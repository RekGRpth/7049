.class Lcom/android/email/activity/MessageListFragment$4;
.super Ljava/lang/Object;
.source "MessageListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessageListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/MessageListFragment;


# direct methods
.method constructor <init>(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isEmptyAndLoading(Landroid/database/Cursor;)Z
    .locals 7
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v3}, Lcom/android/email/activity/MessageListFragment;->access$3600(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/RefreshManager;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v3}, Lcom/android/email/activity/MessageListFragment;->access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_0
    const-string v3, "MessageListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isEmptyAndLoading have null parameter "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v5}, Lcom/android/email/activity/MessageListFragment;->access$3600(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/RefreshManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v5}, Lcom/android/email/activity/MessageListFragment;->access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return v2

    :cond_2
    const/4 v0, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v3}, Lcom/android/email/activity/MessageListFragment;->access$3600(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/RefreshManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v4}, Lcom/android/email/activity/MessageListFragment;->access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v4

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v3, v4, v5}, Lcom/android/email/RefreshManager;->isMessageListRefreshing(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "MessageListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isEmptyAndLoading throw out NPE with mailbox id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v5}, Lcom/android/email/activity/MessageListFragment;->access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v5

    iget-wide v5, v5, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v2, v5}, Lcom/android/email/activity/MessageListFragment;->access$700(Lcom/android/email/activity/MessageListFragment;Z)V

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v2}, Lcom/android/email/activity/MessageListFragment;->access$800(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v3}, Lcom/android/email/activity/MessageListFragment;->access$800(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/email/activity/MessagesAdapter;->createLoader(Landroid/content/Context;Lcom/android/email/MessageListContext;)Landroid/content/Loader;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v0

    sget-boolean v2, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_1

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onCreateLoader(messages) listContext="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v2}, Lcom/android/email/activity/MessageListFragment;->access$900(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/email/MessageListContext;->isRemoteSearch()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v2}, Lcom/android/email/activity/MessageListFragment;->access$900(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2, v5}, Landroid/app/ListFragment;->setListShownNoAnimation(Z)V

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/android/email/activity/MessageListFragment$4$1;

    invoke-direct {v3, p0, v1}, Lcom/android/email/activity/MessageListFragment$4$1;-><init>(Lcom/android/email/activity/MessageListFragment$4;Lcom/android/email/MessageListContext;)V

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/email/activity/MessageListFragment;->access$1002(Lcom/android/email/activity/MessageListFragment;Z)Z

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/email/activity/MessagesAdapter;->createLoader(Landroid/content/Context;Lcom/android/email/MessageListContext;)Landroid/content/Loader;

    move-result-object v2

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 14
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    sget-boolean v9, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v9, :cond_0

    sget-boolean v9, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v9, :cond_0

    const-string v9, "Email"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " onLoadFinished(messages) mailboxId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v11}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v1, p2

    check-cast v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$900(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/email/MessageListContext;->isRemoteSearch()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$300(Lcom/android/email/activity/MessageListFragment;)Landroid/app/Activity;

    move-result-object v9

    check-cast v9, Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v9}, Lcom/android/email/activity/EmailActivity;->getUIController()Lcom/android/email/activity/UIControllerBase;

    move-result-object v8

    if-eqz v8, :cond_1

    const/4 v9, 0x0

    iput-boolean v9, v8, Lcom/android/email/activity/UIControllerBase;->mIsRemoteSearching:Z

    invoke-virtual {v8}, Lcom/android/email/activity/UIControllerBase;->updateRefreshIcon()V

    :cond_1
    if-nez v1, :cond_3

    const-string v9, "MessageListFragment"

    const-string v10, "MessageList onLoadFinished MessagesCursor is null"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v1}, Landroid/database/CursorWrapper;->getCount()I

    move-result v9

    if-lez v9, :cond_4

    sget-boolean v9, Lcom/android/emailcommon/Logging;->LOG_PERFORMANCE:Z

    if-eqz v9, :cond_4

    const-string v9, "Email"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Exchange Download Speed] MessageList:ShowMessages ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_7

    invoke-virtual {v1}, Landroid/database/CursorWrapper;->getCount()I

    move-result v9

    if-lez v9, :cond_7

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const/4 v9, -0x1

    invoke-virtual {v1, v9}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    :goto_1
    invoke-virtual {v1}, Landroid/database/CursorWrapper;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_5

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/database/CursorWrapper;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v9

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Long;

    invoke-interface {v9, v10}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Long;

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_8

    aget-object v9, v0, v2

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v9

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/email/activity/MessagesAdapter;->clearSelection()V

    :cond_8
    iget-boolean v9, v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mIsFound:Z

    if-nez v9, :cond_9

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$500(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessageListFragment$Callback;

    move-result-object v9

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v10}, Lcom/android/email/activity/MessageListFragment;->access$1000(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v10

    invoke-interface {v9, v10}, Lcom/android/email/activity/MessageListFragment$Callback;->onMailboxNotFound(Z)V

    goto/16 :goto_0

    :cond_9
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mAccount:Lcom/android/emailcommon/provider/Account;

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$1302(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Account;)Lcom/android/emailcommon/provider/Account;

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$1402(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-boolean v10, v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mIsEasAccount:Z

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$1502(Lcom/android/email/activity/MessageListFragment;Z)Z

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-boolean v10, v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mIsRefreshable:Z

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$1602(Lcom/android/email/activity/MessageListFragment;Z)Z

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget v10, v1, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mCountTotalAccounts:I

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$1702(Lcom/android/email/activity/MessageListFragment;I)I

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/android/email/activity/UiUtilities;->useTwoPane(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1000(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$900(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/email/MessageListContext;->isSearch()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-virtual {v1}, Landroid/database/CursorWrapper;->getCount()I

    move-result v9

    if-lez v9, :cond_a

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/android/email/activity/UiUtilities;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1800(Lcom/android/email/activity/MessageListFragment;)J

    move-result-wide v9

    const-wide/16 v11, -0x1

    cmp-long v9, v9, v11

    if-nez v9, :cond_a

    invoke-virtual {v1}, Landroid/database/CursorWrapper;->moveToFirst()Z

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v10}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v10

    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Landroid/database/CursorWrapper;->getLong(I)J

    move-result-wide v12

    invoke-static {v9, v10, v11, v12, v13}, Lcom/android/email/activity/MessageListFragment;->access$1900(Lcom/android/email/activity/MessageListFragment;JJ)V

    :cond_a
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$2000(Lcom/android/email/activity/MessageListFragment;Z)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1000(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v9

    if-eqz v9, :cond_b

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v9

    if-eqz v9, :cond_f

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v9

    iget v9, v9, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    const/16 v10, 0x8

    if-ne v9, v10, :cond_f

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v10}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v10

    iget-object v10, v10, Lcom/android/emailcommon/service/SearchParams;->mFilter:Ljava/lang/String;

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v11}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v11

    iget-object v11, v11, Lcom/android/emailcommon/service/SearchParams;->mField:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/android/email/activity/MessagesAdapter;->setQuery(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    check-cast p2, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;

    invoke-virtual/range {p2 .. p2}, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->getSearchedMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$2102(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;

    :goto_3
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$2200(Lcom/android/email/activity/MessageListFragment;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v10

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Lcom/android/email/activity/MessageListFragment;->isCombinedMailbox()Z

    move-result v9

    if-eqz v9, :cond_10

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1700(Lcom/android/email/activity/MessageListFragment;)I

    move-result v9

    const/4 v11, 0x1

    if-le v9, v11, :cond_10

    const/4 v9, 0x1

    :goto_4
    invoke-virtual {v10, v9}, Lcom/android/email/activity/MessagesAdapter;->setShowColorChips(Z)V

    :cond_b
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9, v1}, Lcom/android/email/activity/MessageListFragment;->access$2300(Lcom/android/email/activity/MessageListFragment;Landroid/database/Cursor;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$2400(Lcom/android/email/activity/MessageListFragment;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$2500(Lcom/android/email/activity/MessageListFragment;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Lcom/android/email/activity/MessageListFragment;->updateSelectionMode()V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v10}, Lcom/android/email/activity/MessageListFragment;->access$1000(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v10

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$2600(Lcom/android/email/activity/MessageListFragment;Z)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1000(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v9

    if-eqz v9, :cond_12

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$2700(Lcom/android/email/activity/MessageListFragment;)Landroid/view/ViewGroup;

    move-result-object v9

    const/16 v10, 0x8

    invoke-static {v9, v10}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$2800(Lcom/android/email/activity/MessageListFragment;)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageListFragment$4;->isEmptyAndLoading(Landroid/database/Cursor;)Z

    move-result v9

    if-nez v9, :cond_11

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v10}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    :cond_c
    :goto_5
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$3000(Lcom/android/email/activity/MessageListFragment;)Landroid/os/Parcelable;

    move-result-object v9

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v9

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v10}, Lcom/android/email/activity/MessageListFragment;->access$3000(Lcom/android/email/activity/MessageListFragment;)Landroid/os/Parcelable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/AbsListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$3002(Lcom/android/email/activity/MessageListFragment;Landroid/os/Parcelable;)Landroid/os/Parcelable;

    :cond_d
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$3100(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v9

    if-eqz v9, :cond_e

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Performance test][Email] MessageList FPS test end time ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$3102(Lcom/android/email/activity/MessageListFragment;Z)Z

    :cond_e
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$1002(Lcom/android/email/activity/MessageListFragment;Z)Z

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$3200(Lcom/android/email/activity/MessageListFragment;)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "MessageListFragment"

    const-string v10, "startLocalSearch by onLoadFinished mPendingLocalSearch"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v10}, Lcom/android/email/activity/MessageListFragment;->access$3300(Lcom/android/email/activity/MessageListFragment;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v11}, Lcom/android/email/activity/MessageListFragment;->access$3400(Lcom/android/email/activity/MessageListFragment;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v12}, Lcom/android/email/activity/MessageListFragment;->access$3500(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    move-result-object v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/android/email/activity/MessageListFragment;->startLocalSearch(Ljava/lang/String;Ljava/lang/String;Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;)V

    goto/16 :goto_0

    :cond_f
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v9}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/android/email/activity/MessagesAdapter;->setQuery(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$2102(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;

    goto/16 :goto_3

    :cond_10
    const/4 v9, 0x0

    goto/16 :goto_4

    :cond_11
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$2902(Lcom/android/email/activity/MessageListFragment;Z)Z

    sget-boolean v9, Lcom/android/emailcommon/Logging;->LOG_PERFORMANCE:Z

    if-eqz v9, :cond_c

    const-string v9, "Email"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Exchange Download Speed] MessageList:ShowLoading ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_12
    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v9}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v9

    if-nez v9, :cond_c

    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageListFragment$4;->isEmptyAndLoading(Landroid/database/Cursor;)Z

    move-result v9

    if-nez v9, :cond_c

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/email/activity/MessageListFragment;->access$2902(Lcom/android/email/activity/MessageListFragment;Z)Z

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v10}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    sget-boolean v9, Lcom/android/emailcommon/Logging;->LOG_PERFORMANCE:Z

    if-eqz v9, :cond_c

    const-string v9, "Email"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Exchange Download Speed] MessageList:FisrtShowMessages ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/email/activity/MessageListFragment$4;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onLoaderReset(messages)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v0}, Lcom/android/email/activity/MessageListFragment;->access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v0, v3}, Lcom/android/email/activity/MessageListFragment;->access$1302(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Account;)Lcom/android/emailcommon/provider/Account;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v0, v3}, Lcom/android/email/activity/MessageListFragment;->access$1402(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    invoke-static {v0, v3}, Lcom/android/email/activity/MessageListFragment;->access$2102(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment$4;->this$0:Lcom/android/email/activity/MessageListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/email/activity/MessageListFragment;->access$1702(Lcom/android/email/activity/MessageListFragment;I)I

    return-void
.end method
