.class public Lcom/android/email/activity/MessageListFragment;
.super Landroid/app/ListFragment;
.source "MessageListFragment.java"

# interfaces
.implements Landroid/view/View$OnDragListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/android/email/activity/MessagesAdapter$Callback;
.implements Lcom/android/email/activity/MoveMessageToDialog$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;,
        Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;,
        Lcom/android/email/activity/MessageListFragment$CheckMessageCountTask;,
        Lcom/android/email/activity/MessageListFragment$RefreshListener;,
        Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;,
        Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;,
        Lcom/android/email/activity/MessageListFragment$MessageOpenTask;,
        Lcom/android/email/activity/MessageListFragment$ShadowBuilder;,
        Lcom/android/email/activity/MessageListFragment$EmptyCallback;,
        Lcom/android/email/activity/MessageListFragment$Callback;
    }
.end annotation


# static fields
.field private static final ARG_LIST_CONTEXT:Ljava/lang/String; = "listContext"

.field private static final BUNDLE_KEY_SELECTED_MESSAGE_ID:Ljava/lang/String; = "messageListFragment.state.listState.selected_message_id"

.field private static final BUNDLE_LIST_STATE:Ljava/lang/String; = "MessageListFragment.state.listState"

.field private static final LIST_FOOTER_MODE_LOCAL_SEARCH:I = 0x2

.field private static final LIST_FOOTER_MODE_MORE:I = 0x1

.field private static final LIST_FOOTER_MODE_NONE:I = 0x0

.field private static final LOADER_ID_MESSAGES_LOADER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MessageListFragment"


# instance fields
.field private final LOADER_CALLBACKS:Landroid/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final SEARCH_WARNING_DELAY_MS:I

.field private mAccount:Lcom/android/emailcommon/provider/Account;

.field private mActivity:Landroid/app/Activity;

.field private mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

.field private mController:Lcom/android/email/Controller;

.field private mCountTotalAccounts:I

.field private mDisableCab:Z

.field private mEmptyView:Landroid/view/View;

.field private mHasRestartLoader:Z

.field private mIsEasAccount:Z

.field private mIsFirstLoad:Z

.field private mIsPerformanceLogged:Z

.field private mIsRefreshable:Z

.field private mIsViewCreated:Z

.field private mLastSelectionModeCallback:Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;

.field private mListAdapter:Lcom/android/email/activity/MessagesAdapter;

.field private mListContext:Lcom/android/email/MessageListContext;

.field private mListFooterMode:I

.field private mListFooterProgress:Landroid/view/View;

.field private mListFooterText:Landroid/widget/TextView;

.field private mListFooterView:Landroid/view/View;

.field private mListPanel:Landroid/view/View;

.field private mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

.field private mLocalSearchListContext:Lcom/android/email/MessageListContext;

.field private mMailbox:Lcom/android/emailcommon/provider/Mailbox;

.field private mPendingLocalSearch:Z

.field private mPendingQueryField:Ljava/lang/String;

.field private mPendingQueryTerm:Ljava/lang/String;

.field private final mRefreshListener:Lcom/android/email/activity/MessageListFragment$RefreshListener;

.field private mRefreshManager:Lcom/android/email/RefreshManager;

.field private mRemoteSearchCallback:Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;

.field private mResumed:Z

.field private mSavedListState:Landroid/os/Parcelable;

.field private mSearchHeader:Landroid/view/ViewGroup;

.field private mSearchHeaderCount:Landroid/widget/TextView;

.field private mSearchHeaderText:Landroid/widget/TextView;

.field private mSearchedMailbox:Lcom/android/emailcommon/provider/Mailbox;

.field private mSelectedMessageId:J

.field private mSelectionMode:Landroid/view/ActionMode;

.field private mShowMoveCommand:Z

.field private mShowSendCommand:Z

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mWaitLoading:Z

.field private mWarningContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    new-instance v0, Lcom/android/email/activity/MessageListFragment$RefreshListener;

    invoke-direct {v0, p0, v2}, Lcom/android/email/activity/MessageListFragment$RefreshListener;-><init>(Lcom/android/email/activity/MessageListFragment;Lcom/android/email/activity/MessageListFragment$1;)V

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshListener:Lcom/android/email/activity/MessageListFragment$RefreshListener;

    sget-object v0, Lcom/android/email/activity/MessageListFragment$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MessageListFragment$Callback;

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    iput-boolean v3, p0, Lcom/android/email/activity/MessageListFragment;->mWaitLoading:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mIsPerformanceLogged:Z

    iput-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    iput-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    iput-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mRemoteSearchCallback:Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;

    iput-boolean v3, p0, Lcom/android/email/activity/MessageListFragment;->mPendingLocalSearch:Z

    iput-boolean v3, p0, Lcom/android/email/activity/MessageListFragment;->mHasRestartLoader:Z

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/email/activity/MessageListFragment;->SEARCH_WARNING_DELAY_MS:I

    new-instance v0, Lcom/android/email/activity/MessageListFragment$4;

    invoke-direct {v0, p0}, Lcom/android/email/activity/MessageListFragment$4;-><init>(Lcom/android/email/activity/MessageListFragment;)V

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->LOADER_CALLBACKS:Landroid/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$1000(Lcom/android/email/activity/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mIsFirstLoad:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/android/email/activity/MessageListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mIsFirstLoad:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->onSearchLoadTimeout()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessagesAdapter;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Account;)Lcom/android/emailcommon/provider/Account;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Lcom/android/emailcommon/provider/Account;

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/provider/Mailbox;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Lcom/android/emailcommon/provider/Mailbox;

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/android/email/activity/MessageListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mIsEasAccount:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/android/email/activity/MessageListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mIsRefreshable:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/android/email/activity/MessageListFragment;)I
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget v0, p0, Lcom/android/email/activity/MessageListFragment;->mCountTotalAccounts:I

    return v0
.end method

.method static synthetic access$1702(Lcom/android/email/activity/MessageListFragment;I)I
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # I

    iput p1, p0, Lcom/android/email/activity/MessageListFragment;->mCountTotalAccounts:I

    return p1
.end method

.method static synthetic access$1800(Lcom/android/email/activity/MessageListFragment;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-wide v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    return-wide v0
.end method

.method static synthetic access$1900(Lcom/android/email/activity/MessageListFragment;JJ)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # J
    .param p3    # J

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/email/activity/MessageListFragment;->onMessageOpen(JJ)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/email/activity/MessageListFragment;)Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/email/activity/MessageListFragment;Z)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->adjustMessageNotification(Z)V

    return-void
.end method

.method static synthetic access$2102(Lcom/android/email/activity/MessageListFragment;Lcom/android/emailcommon/provider/Mailbox;)Lcom/android/emailcommon/provider/Mailbox;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Lcom/android/emailcommon/provider/Mailbox;

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchedMailbox:Lcom/android/emailcommon/provider/Mailbox;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->updateMailboxSpecificActions()V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/email/activity/MessageListFragment;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->updateSearchHeader(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->autoRefreshStaleMailbox()V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->updateFooterView()V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/email/activity/MessageListFragment;Z)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->highlightSelectedMessage(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/email/activity/MessageListFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mWarningContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/email/activity/MessageListFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListPanel:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/email/activity/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mWaitLoading:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/android/email/activity/MessageListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mWaitLoading:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/email/activity/MessageListFragment;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/email/activity/MessageListFragment;)Landroid/os/Parcelable;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSavedListState:Landroid/os/Parcelable;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/email/activity/MessageListFragment;Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Landroid/os/Parcelable;

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mSavedListState:Landroid/os/Parcelable;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/android/email/activity/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mIsPerformanceLogged:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/android/email/activity/MessageListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mIsPerformanceLogged:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/android/email/activity/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mPendingLocalSearch:Z

    return v0
.end method

.method static synthetic access$3300(Lcom/android/email/activity/MessageListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mPendingQueryTerm:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/email/activity/MessageListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mPendingQueryField:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/RefreshManager;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/android/email/activity/MessageListFragment;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/android/email/activity/MessageListFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mSelectionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/android/email/activity/MessageListFragment;)I
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->getSelectedCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/email/activity/MessageListFragment;I)I
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->callbackTypeForMailboxType(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$4000(Lcom/android/email/activity/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mShowMoveCommand:Z

    return v0
.end method

.method static synthetic access$4100(Lcom/android/email/activity/MessageListFragment;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->toggleRead(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/email/activity/MessageListFragment;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->toggleFavorite(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/android/email/activity/MessageListFragment;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->deleteMessages(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$4400(Lcom/android/email/activity/MessageListFragment;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->showMoveMessagesDialog(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$4500(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->onDeselectAll()V

    return-void
.end method

.method static synthetic access$4600(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->updateListFooter()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/activity/MessageListFragment$Callback;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/Controller;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mController:Lcom/android/email/Controller;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/email/activity/MessageListFragment;Z)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageListFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListFragment;->showSendCommand(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/email/activity/MessageListFragment;)Lcom/android/email/MessageListContext;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    return-object v0
.end method

.method private adjustMessageNotification(Z)V
    .locals 7
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccountId()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v2

    const-wide/16 v5, -0x2

    cmp-long v5, v2, v5

    if-eqz v5, :cond_0

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-lez v5, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    iget-object v5, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v5, v0, v1}, Lcom/android/emailcommon/utility/Utility;->updateLastSeenMessageKey(Landroid/content/Context;J)Lcom/android/emailcommon/utility/EmailAsyncTask;

    :cond_1
    iget-object v5, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lcom/android/email/NotificationController;->getInstance(Landroid/content/Context;)Lcom/android/email/NotificationController;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/email/activity/MessageListFragment;->mResumed:Z

    invoke-virtual {v4, v5, v0, v1}, Lcom/android/email/NotificationController;->suspendMessageNotification(ZJ)V

    :cond_2
    return-void
.end method

.method private autoRefreshStaleMailbox()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mIsRefreshable:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/email/RefreshManager;->isMailboxStale(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    :cond_2
    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mIsFirstLoad:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    if-eqz v1, :cond_3

    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-object v2, v1, Lcom/android/emailcommon/provider/Account;->mEmailAddress:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v3}, Lcom/android/emailcommon/provider/Account;->isEasAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "com.android.exchange"

    :goto_1
    invoke-direct {v0, v2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.android.email.provider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/email/activity/MessageListFragment;->onRefresh(Z)V

    goto :goto_0

    :cond_4
    const-string v1, "com.android.email"

    goto :goto_1
.end method

.method private callbackTypeForMailboxType(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static clearDeletedSet(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    sget-object v1, Lcom/android/email/activity/MessagesAdapter;->sDeletedSet:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/email/activity/MessagesAdapter;->sDeletedSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/android/email/activity/MessagesAdapter;->sDeletedSet:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/AbstractSet;->removeAll(Ljava/util/Collection;)Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private deleteMessages(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mController:Lcom/android/email/Controller;

    invoke-virtual {v1, v0}, Lcom/android/email/Controller;->deleteMessages(Ljava/util/Set;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v1, v0}, Lcom/android/email/activity/MessagesAdapter;->updateDeletedMessageList(Ljava/util/Set;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0008

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->updateSelectionMode()V

    return-void
.end method

.method private determineFooterMode()I
    .locals 7

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    const/4 v5, 0x4

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    :cond_0
    move v4, v2

    :goto_0
    return v4

    :cond_1
    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v4}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    move v4, v2

    goto :goto_0

    :cond_4
    move-object v4, v0

    check-cast v4, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;

    invoke-virtual {v4}, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->getResultsCount()I

    move-result v3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v1, v3, :cond_5

    const/4 v2, 0x1

    :cond_5
    :goto_1
    const-string v4, "MessageListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "determineFooterMode result "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v2

    goto :goto_0

    :cond_6
    iget-boolean v4, p0, Lcom/android/email/activity/MessageListFragment;->mIsEasAccount:Z

    if-nez v4, :cond_5

    const/4 v2, 0x1

    goto :goto_1
.end method

.method private doFooterClick()V
    .locals 1

    iget v0, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->onLoadMoreMessages()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->startRemoteSearch()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private finishSelectionMode()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->isInSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mLastSelectionModeCallback:Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;->mClosedByUser:Z

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    return-void
.end method

.method private getSelectedCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method private highlightSelectedMessage(Z)V
    .locals 7
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->isViewCreated()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    invoke-virtual {v2}, Landroid/widget/AbsListView;->clearChoices()V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Landroid/widget/AdapterView;->getCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    invoke-virtual {v2, v1}, Landroid/widget/AdapterView;->getItemIdAtPosition(I)J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/android/emailcommon/utility/Utility;->listViewSmoothScrollToPosition(Landroid/app/Activity;Landroid/widget/ListView;I)V

    goto :goto_0
.end method

.method private initSearchHeader()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04003c

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    const v2, 0x7f0f0098

    invoke-static {v1, v2}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeaderText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    const v2, 0x7f0f0097

    invoke-static {v1, v2}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeaderCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method private initializeArgCache()V
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "listContext"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/email/MessageListContext;

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " initializeArgCache mListContext "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isSearchRequestSameAsPrevious(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mPendingQueryField:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/emailcommon/utility/TextUtilities;->stringOrNullEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mPendingQueryTerm:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/emailcommon/utility/TextUtilities;->stringOrNullEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewCreated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mIsViewCreated:Z

    return v0
.end method

.method public static newInstance(Lcom/android/email/MessageListContext;)Lcom/android/email/activity/MessageListFragment;
    .locals 3
    .param p0    # Lcom/android/email/MessageListContext;

    new-instance v1, Lcom/android/email/activity/MessageListFragment;

    invoke-direct {v1}, Lcom/android/email/activity/MessageListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "listContext"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private onDeselectAll()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/MessagesAdapter;->clearSelection()V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->isInSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->finishSelectionMode()V

    :cond_0
    return-void
.end method

.method private onLoadMoreMessages()V
    .locals 6

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/email/activity/ConnectionAlertDialog;->newInstance()Lcom/android/email/activity/ConnectionAlertDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "connectionalertdialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mIsRefreshable:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccountId()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/email/RefreshManager;->loadMoreMessages(JJ)Z

    goto :goto_0
.end method

.method private onMessageOpen(JJ)V
    .locals 8
    .param p1    # J
    .param p3    # J

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v5

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageListFragment;->callbackTypeForMailboxType(I)I

    move-result v7

    move-wide v1, p3

    move-wide v3, p1

    invoke-interface/range {v0 .. v7}, Lcom/android/email/activity/MessageListFragment$Callback;->onMessageOpen(JJJI)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/email/activity/MessageListFragment$MessageOpenTask;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/MessageListFragment$MessageOpenTask;-><init>(Lcom/android/email/activity/MessageListFragment;JJ)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->cancelPreviousAndExecuteParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    goto :goto_0
.end method

.method private onSearchLoadTimeout()V
    .locals 7

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mListPanel:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f04003d

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mWarningContainer:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mWarningContainer:Landroid/view/ViewGroup;

    const v5, 0x7f0f009a

    invoke-static {v4, v5}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mWarningContainer:Landroid/view/ViewGroup;

    const v5, 0x7f0f009b

    invoke-static {v4, v5}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v4, 0x7f0801c1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0801c2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageListFragment;->mWarningContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private showMoveMessagesDialog(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/android/emailcommon/utility/Utility;->toPrimitiveLongArray(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-static {v1, p0}, Lcom/android/email/activity/MoveMessageToDialog;->newInstance([JLandroid/app/Fragment;)Lcom/android/email/activity/MoveMessageToDialog;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showSendCommand(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mShowSendCommand:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mShowSendCommand:Z

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method private startLoading()V
    .locals 5

    const/4 v4, 0x0

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startLoading"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0, v4}, Lcom/android/email/activity/MessageListFragment;->updateSearchHeader(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->LOADER_CALLBACKS:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v4, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method private startRemoteSearch()V
    .locals 12

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v7

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/email/activity/ConnectionAlertDialog;->newInstance()Lcom/android/email/activity/ConnectionAlertDialog;

    move-result-object v0

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "connectionalertdialog"

    invoke-virtual {v0, v10, v11}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccountId()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/android/emailcommon/provider/Account;->isNormalAccount(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-wide v3, v9, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v8

    if-eqz v8, :cond_2

    iget-object v0, v8, Lcom/android/emailcommon/service/SearchParams;->mFilter:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v8, Lcom/android/emailcommon/service/SearchParams;->mField:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "MessageListFragment"

    const-string v10, "Can\'t do remote search for the searchParams is null"

    invoke-static {v0, v10}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v5, v8, Lcom/android/emailcommon/service/SearchParams;->mFilter:Ljava/lang/String;

    iget-object v6, v8, Lcom/android/emailcommon/service/SearchParams;->mField:Ljava/lang/String;

    const-string v0, "MessageListFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Submitting search: ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Fileds "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " in mailboxId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static/range {v0 .. v6}, Lcom/android/email/activity/EmailActivity;->createSearchIntent(Landroid/app/Activity;JJLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private supportRemoteSearch()Z
    .locals 15

    const-wide/16 v13, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccount()Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccountId()J

    move-result-wide v1

    const-wide/high16 v11, 0x1000000000000000L

    cmp-long v11, v1, v11

    if-nez v11, :cond_1

    :cond_0
    :goto_0
    return v10

    :cond_1
    cmp-long v11, v1, v13

    if-lez v11, :cond_2

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v11, v1, v2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_4

    iget v11, v0, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit16 v11, v11, 0x800

    if-eqz v11, :cond_4

    move v3, v9

    :goto_1
    if-eqz v3, :cond_0

    const/4 v7, 0x0

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v11}, Lcom/android/emailcommon/provider/Account;->getProtocol(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v6

    if-nez v6, :cond_3

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v4

    cmp-long v11, v4, v13

    if-lez v11, :cond_3

    iget-object v11, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v11, v4, v5}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v6

    :cond_3
    if-eqz v6, :cond_5

    invoke-virtual {v6, v8}, Lcom/android/emailcommon/provider/Mailbox;->loadsFromServer(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    move v7, v9

    :goto_2
    if-eqz v7, :cond_6

    if-eqz v3, :cond_6

    :goto_3
    move v10, v9

    goto :goto_0

    :cond_4
    move v3, v10

    goto :goto_1

    :cond_5
    move v7, v10

    goto :goto_2

    :cond_6
    move v9, v10

    goto :goto_3
.end method

.method private testMultiple(Ljava/util/Set;IZ)Z
    .locals 7
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;IZ)Z"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v3, v4

    :goto_0
    return v3

    :cond_1
    const/4 v5, -0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    int-to-long v1, v5

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz p3, :cond_3

    move v5, v3

    :goto_1
    if-ne v6, v5, :cond_2

    goto :goto_0

    :cond_3
    move v5, v4

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_0
.end method

.method private toggleFavorite(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/android/email/activity/MessageListFragment$2;

    invoke-direct {v0, p0}, Lcom/android/email/activity/MessageListFragment$2;-><init>(Lcom/android/email/activity/MessageListFragment;)V

    invoke-direct {p0, p1, v0}, Lcom/android/email/activity/MessageListFragment;->toggleMultiple(Ljava/util/Set;Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;)V

    return-void
.end method

.method private toggleMultiple(Ljava/util/Set;Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;)V
    .locals 12
    .param p2    # Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;",
            ")V"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v11, -0x1

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v9}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x1

    invoke-interface {v1, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_2
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    int-to-long v2, v9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {p1, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {p2, v1}, Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;->getField(Landroid/database/Cursor;)Z

    move-result v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_3

    if-eqz v6, :cond_3

    move v0, v7

    :goto_2
    goto :goto_1

    :cond_3
    move v0, v8

    goto :goto_2

    :cond_4
    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_5

    if-nez v0, :cond_6

    move v4, v7

    :goto_3
    invoke-interface {v1, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    new-instance v7, Lcom/android/email/activity/MessageListFragment$3;

    invoke-direct {v7, p0, v5, v4, p2}, Lcom/android/email/activity/MessageListFragment$3;-><init>(Lcom/android/email/activity/MessageListFragment;Ljava/util/HashMap;ZLcom/android/email/activity/MessageListFragment$MultiToggleHelper;)V

    invoke-static {v7}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    :cond_5
    invoke-interface {p1}, Ljava/util/Set;->clear()V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->updateSelectionMode()V

    goto :goto_0

    :cond_6
    move v4, v8

    goto :goto_3
.end method

.method private toggleRead(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/android/email/activity/MessageListFragment$1;

    invoke-direct {v0, p0}, Lcom/android/email/activity/MessageListFragment$1;-><init>(Lcom/android/email/activity/MessageListFragment;)V

    invoke-direct {p0, p1, v0}, Lcom/android/email/activity/MessageListFragment;->toggleMultiple(Ljava/util/Set;Lcom/android/email/activity/MessageListFragment$MultiToggleHelper;)V

    return-void
.end method

.method private toggleSelection(Lcom/android/email/activity/MessageListItem;)V
    .locals 1
    .param p1    # Lcom/android/email/activity/MessageListItem;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/MessagesAdapter;->toggleSelected(Lcom/android/email/activity/MessageListItem;)V

    return-void
.end method

.method private updateFooterView()V
    .locals 6

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->determineFooterMode()I

    move-result v2

    iget v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    if-ne v3, v2, :cond_0

    const-string v3, "MessageListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateFooterView mListFooterMode already is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput v2, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    iget v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    iget v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    iget v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->supportRemoteSearch()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Landroid/widget/AbsListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {p0, v3}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_2
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    const v4, 0x7f0f008c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterProgress:Landroid/view/View;

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    const v4, 0x7f0f008d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterText:Landroid/widget/TextView;

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->updateListFooter()V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_1
.end method

.method private updateListFooter()V
    .locals 6

    const/16 v2, 0x8

    iget v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    if-eqz v3, :cond_0

    const-string v3, "MessageListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateListFooter mListFooterMode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    iget v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/android/email/RefreshManager;->isMessageListRefreshing(J)Z

    move-result v0

    if-eqz v0, :cond_2

    const v1, 0x7f080060

    :goto_1
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterProgress:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const v1, 0x7f08008d

    goto :goto_1

    :pswitch_1
    iget-boolean v3, p0, Lcom/android/email/activity/MessageListFragment;->mIsEasAccount:Z

    if-eqz v3, :cond_3

    const v1, 0x7f08002b

    :goto_2
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterProgress:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const v1, 0x7f080029

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateMailboxSpecificActions()V
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v4

    const-wide/16 v6, -0x6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    const/4 v4, 0x4

    if-ne v1, v4, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageListFragment;->showSendCommand(Z)V

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/emailcommon/provider/Account;->supportsMoveMessages(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/Mailbox;->canHaveMessagesMoved()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_2
    :goto_2
    iput-boolean v2, p0, Lcom/android/email/activity/MessageListFragment;->mShowMoveCommand:Z

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :cond_3
    move v0, v3

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private updateSearchHeader(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    if-eqz v3, :cond_1

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;->onLocalSearchFinished(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->isRemoteSearch()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    invoke-static {v3, v4}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    goto :goto_0

    :cond_3
    move-object v2, p1

    check-cast v2, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;

    invoke-virtual {v2}, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->getResultsCount()I

    move-result v1

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mRemoteSearchCallback:Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;

    if-eqz v3, :cond_4

    if-ltz v1, :cond_4

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mRemoteSearchCallback:Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;

    invoke-interface {v3, v1}, Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;->onRemoteSearchFinished(I)V

    :cond_4
    if-gez v1, :cond_0

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->initSearchHeader()V

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeaderText:Landroid/widget/TextView;

    const v4, 0x7f080028

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private updateSelectionModeView()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    return-void
.end method


# virtual methods
.method public doesSelectionContainNonStarredMessage()Z
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/email/activity/MessageListFragment;->testMultiple(Ljava/util/Set;IZ)Z

    move-result v0

    return v0
.end method

.method public doesSelectionContainReadMessage()Z
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/email/activity/MessageListFragment;->testMultiple(Ljava/util/Set;IZ)Z

    move-result v0

    return v0
.end method

.method public enterLocalSearchMode()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/ListFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public exitLocalSearchMode()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    const v0, 0x7f08008e

    invoke-virtual {p0, v0}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/ListFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public getAccount()Lcom/android/emailcommon/provider/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    return-object v0
.end method

.method public getAccountId()J
    .locals 2

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->initializeArgCache()V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    iget-wide v0, v0, Lcom/android/email/MessageListContext;->mAccountId:J

    return-wide v0
.end method

.method public getListContext()Lcom/android/email/MessageListContext;
    .locals 1

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->initializeArgCache()V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    return-object v0
.end method

.method public getMailbox()Lcom/android/emailcommon/provider/Mailbox;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    return-object v0
.end method

.method public getMailboxId()J
    .locals 2

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->initializeArgCache()V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSearchedMailbox()Lcom/android/emailcommon/provider/Mailbox;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSearchedMailbox:Lcom/android/emailcommon/provider/Mailbox;

    return-object v0
.end method

.method public hasDataLoaded()Z
    .locals 1

    iget v0, p0, Lcom/android/email/activity/MessageListFragment;->mCountTotalAccounts:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCombinedMailbox()Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccountId()J

    move-result-wide v0

    const-wide/high16 v2, 0x1000000000000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInSelectionMode()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInboxList()Z
    .locals 11

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v4

    iget-wide v0, v4, Lcom/android/email/MessageListContext;->mAccountId:J

    const-wide/high16 v7, 0x1000000000000000L

    cmp-long v7, v0, v7

    if-nez v7, :cond_2

    invoke-virtual {v4}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v7

    const-wide/16 v9, -0x2

    cmp-long v7, v7, v9

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    move v5, v6

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->hasDataLoaded()Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v7, v0, v1, v6}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v7

    cmp-long v7, v7, v2

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    if-eqz v7, :cond_0

    :cond_4
    move v5, v6

    goto :goto_0
.end method

.method public isRefreshable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mIsRefreshable:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onActivityCreated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    const/high16 v1, 0x3f000000

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setVelocityScale(F)V

    const v1, 0x3ba3d70a

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFriction(F)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040039

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    const v1, 0x7f08008e

    invoke-virtual {p0, v1}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/ListFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getEmptyView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mEmptyView:Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessageListFragment;->restoreInstanceState(Landroid/os/Bundle;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->startLoading()V

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->installFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method public onAdapterFavoriteChanged(Lcom/android/email/activity/MessageListItem;Z)V
    .locals 5
    .param p1    # Lcom/android/email/activity/MessageListItem;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v1

    const-wide/16 v3, -0x4

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v1, p1}, Lcom/android/email/activity/MessagesAdapter;->isSelected(Lcom/android/email/activity/MessageListItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v1, p1}, Lcom/android/email/activity/MessagesAdapter;->toggleSelected(Lcom/android/email/activity/MessageListItem;)V

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mController:Lcom/android/email/Controller;

    iget-wide v2, p1, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    invoke-virtual {v1, v2, v3, p2}, Lcom/android/email/Controller;->setMessageFavorite(JZ)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public onAdapterSelectedChanged(Lcom/android/email/activity/MessageListItem;ZI)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessageListItem;
    .param p2    # Z
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->updateSelectionMode()V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onAttach"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0, v3}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mController:Lcom/android/email/Controller;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    new-instance v0, Lcom/android/email/activity/MessagesAdapter;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/email/MessageListContext;->isSearch()Z

    move-result v2

    invoke-direct {v0, v1, p0, v2}, Lcom/android/email/activity/MessagesAdapter;-><init>(Landroid/content/Context;Lcom/android/email/activity/MessagesAdapter$Callback;Z)V

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    iput-boolean v3, p0, Lcom/android/email/activity/MessageListFragment;->mIsFirstLoad:Z

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onCreateView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const v1, 0x7f040038

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mIsViewCreated:Z

    const v1, 0x7f0f008b

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListPanel:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->finishSelectionMode()V

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mHasRestartLoader:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDestroyView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v3, p0, Lcom/android/email/activity/MessageListFragment;->mIsViewCreated:Z

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->uninstallFragment(Landroid/app/Fragment;)V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->updateSelectionMode()V

    iput v3, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterMode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSearchHeader:Landroid/view/ViewGroup;

    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    return-void
.end method

.method public onDetach()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDetach"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    return-void
.end method

.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/DragEvent;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/DragEvent;->getResult()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->onDeselectAll()V

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    invoke-interface {v0}, Lcom/android/email/activity/MessageListFragment$Callback;->onDragEnded()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onHidden(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mDisableCab:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/email/activity/MessageListFragment;->mDisableCab:Z

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->updateSelectionMode()V

    goto :goto_0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 15
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    move-object/from16 v0, p2

    if-eq v0, v10, :cond_7

    move-object/from16 v0, p2

    instance-of v10, v0, Lcom/android/email/activity/MessageListItem;

    if-eqz v10, :cond_7

    move-object/from16 v3, p2

    check-cast v3, Lcom/android/email/activity/MessageListItem;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v10, v3}, Lcom/android/email/activity/MessagesAdapter;->isSelected(Lcom/android/email/activity/MessageListItem;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageListFragment;->toggleSelection(Lcom/android/email/activity/MessageListItem;)V

    const/4 v9, 0x1

    :cond_0
    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    invoke-interface {v10}, Lcom/android/email/activity/MessageListFragment$Callback;->onDragStarted()Z

    move-result v10

    if-nez v10, :cond_1

    :goto_0
    return v9

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v4

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-nez v10, :cond_3

    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    :cond_3
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-lez v10, :cond_4

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    invoke-virtual {v10}, Lcom/android/emailcommon/provider/Mailbox;->canHaveMessagesMoved()Z

    move-result v10

    if-nez v10, :cond_4

    const/4 v9, 0x0

    goto :goto_0

    :cond_4
    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "com.android.email.MESSAGE_LIST_ITEMS"

    sget-object v12, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v12

    iget-wide v13, v3, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    invoke-static {v13, v14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, "mailboxId"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v12

    invoke-static {v10, v11, v12}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v1

    iget-object v10, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v10}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-wide v12, v3, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    cmp-long v10, v10, v12

    if-eqz v10, :cond_5

    new-instance v10, Landroid/content/ClipData$Item;

    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v10}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_1

    :cond_6
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    new-instance v10, Lcom/android/email/activity/MessageListFragment$ShadowBuilder;

    invoke-direct {v10, v3, v8}, Lcom/android/email/activity/MessageListFragment$ShadowBuilder;-><init>(Landroid/view/View;I)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v3, v1, v10, v11, v12}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    const/4 v9, 0x1

    goto/16 :goto_0

    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListFooterView:Landroid/view/View;

    if-eq p2, v1, :cond_1

    instance-of v1, p2, Lcom/android/email/activity/MessageListItem;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/android/email/activity/MessageListItem;

    iget-wide v1, v0, Lcom/android/email/activity/MessageListItem;->mMailboxId:J

    invoke-direct {p0, v1, v2, p4, p5}, Lcom/android/email/activity/MessageListFragment;->onMessageOpen(JJ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->doFooterClick()V

    goto :goto_0
.end method

.method public onMoveToMailboxSelected(J[J)V
    .locals 3
    .param p1    # J
    .param p3    # [J

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    invoke-static {p3}, Lcom/android/emailcommon/utility/Utility;->toLongSet([J)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/email/activity/MessageListFragment$Callback;->onAdvancingOpAccepted(Ljava/util/Set;)V

    invoke-static {v0, p1, p2, p3}, Lcom/android/email/activity/ActivityHelper;->moveMessages(Landroid/content/Context;J[J)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->onDeselectAll()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->onSendPendingMessages()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0f00fc
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mResumed:Z

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSavedListState:Landroid/os/Parcelable;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageListFragment;->adjustMessageNotification(Z)V

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1    # Landroid/view/Menu;

    const v1, 0x7f0f00fc

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mShowSendCommand:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public onRefresh(Z)V
    .locals 6
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mIsRefreshable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getAccountId()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v3

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/email/RefreshManager;->refreshMessageList(JJZ)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mResumed:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageListFragment;->adjustMessageNotification(Z)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->updateListFooter()V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshListener:Lcom/android/email/activity/MessageListFragment$RefreshListener;

    invoke-virtual {v0, v1}, Lcom/android/email/RefreshManager;->registerListener(Lcom/android/email/RefreshManager$Listener;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSaveInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/MessagesAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->isViewCreated()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MessageListFragment.state.listState"

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    const-string v0, "messageListFragment.state.listState.selected_message_id"

    iget-wide v1, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method public onSendPendingMessages()V
    .locals 5

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/email/activity/ConnectionAlertDialog;->newInstance()Lcom/android/email/activity/ConnectionAlertDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "connectionalertdialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v1

    const-wide/16 v3, -0x6

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/android/email/RefreshManager;->sendPendingMessagesForAllAccounts()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-wide v1, v1, Lcom/android/emailcommon/provider/Mailbox;->mAccountKey:J

    invoke-virtual {v0, v1, v2}, Lcom/android/email/RefreshManager;->sendPendingMessages(J)Z

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStart"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mRefreshListener:Lcom/android/email/activity/MessageListFragment$RefreshListener;

    invoke-virtual {v0, v1}, Lcom/android/email/RefreshManager;->unregisterListener(Lcom/android/email/RefreshManager$Listener;)V

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0}, Lcom/android/email/activity/MessageListFragment$ShadowBuilder;->access$102(I)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method restoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " restoreInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/MessagesAdapter;->loadState(Landroid/os/Bundle;)V

    const-string v0, "MessageListFragment.state.listState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mSavedListState:Landroid/os/Parcelable;

    const-string v0, "messageListFragment.state.listState.selected_message_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    return-void
.end method

.method public setCallback(Lcom/android/email/activity/MessageListFragment$Callback;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessageListFragment$Callback;

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mCallback:Lcom/android/email/activity/MessageListFragment$Callback;

    return-void

    :cond_0
    sget-object p1, Lcom/android/email/activity/MessageListFragment$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MessageListFragment$Callback;

    goto :goto_0
.end method

.method public setLayout(Lcom/android/email/activity/ThreePaneLayout;)V
    .locals 1
    .param p1    # Lcom/android/email/activity/ThreePaneLayout;

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/email/activity/UiUtilities;->useTwoPane(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/MessagesAdapter;->setLayout(Lcom/android/email/activity/ThreePaneLayout;)V

    :cond_0
    return-void
.end method

.method public setRemoteSearchCallBack(Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;

    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mRemoteSearchCallback:Lcom/android/email/activity/MessageListFragment$RemoteSearchCallback;

    return-void
.end method

.method public setSelectedMessage(J)V
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-wide p1, p0, Lcom/android/email/activity/MessageListFragment;->mSelectedMessageId:J

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListFragment;->mResumed:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageListFragment;->highlightSelectedMessage(Z)V

    goto :goto_0
.end method

.method public startLocalSearch(Ljava/lang/String;Ljava/lang/String;Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageListFragment;->isSearchRequestSameAsPrevious(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mPendingLocalSearch:Z

    if-nez v1, :cond_1

    const-string v1, "MessageListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startLocalSearch same as privous just return"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    invoke-interface {p3, v1}, Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;->onMessageListContextChanged(Lcom/android/email/MessageListContext;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mPendingLocalSearch:Z

    if-eqz v1, :cond_2

    iput-boolean v2, p0, Lcom/android/email/activity/MessageListFragment;->mPendingLocalSearch:Z

    :cond_2
    iput-object p1, p0, Lcom/android/email/activity/MessageListFragment;->mPendingQueryTerm:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/email/activity/MessageListFragment;->mPendingQueryField:Ljava/lang/String;

    if-eqz p3, :cond_3

    iput-object p3, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    iput-object v7, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchCallback:Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;

    if-eqz v1, :cond_5

    invoke-interface {p3, v2}, Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;->onLocalSearchFinished(I)V

    :cond_5
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListContext:Lcom/android/email/MessageListContext;

    invoke-interface {p3, v1}, Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;->onMessageListContextChanged(Lcom/android/email/MessageListContext;)V

    const-string v1, "MessageListFragment"

    const-string v2, "start local Search.with null queryTerm."

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mListAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v1, p1, p2}, Lcom/android/email/activity/MessagesAdapter;->setQuery(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v6

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->LOADER_CALLBACKS:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v6, v5, v7, v1}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    iput-boolean v5, p0, Lcom/android/email/activity/MessageListFragment;->mHasRestartLoader:Z

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    if-eqz v1, :cond_7

    new-instance v0, Lcom/android/emailcommon/service/SearchParams;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-wide v1, v1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/emailcommon/service/SearchParams;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-wide v1, v1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-wide v3, v3, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v1, v2, v3, v4, v0}, Lcom/android/email/MessageListContext;->forSearch(JJLcom/android/emailcommon/service/SearchParams;)Lcom/android/email/MessageListContext;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    iget-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    invoke-interface {p3, v1}, Lcom/android/email/activity/MessageListFragment$LocalSearchCallback;->onMessageListContextChanged(Lcom/android/email/MessageListContext;)V

    const-string v1, "MessageListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " startLocalSearch mListContext "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/email/activity/MessageListFragment;->mLocalSearchListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string v1, "MessageListFragment"

    const-string v2, " start pending local Search.."

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/android/email/activity/MessageListFragment;->mPendingLocalSearch:Z

    goto/16 :goto_0
.end method

.method public updateSelectionMode()V
    .locals 3

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->getSelectedCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/android/email/activity/MessageListFragment;->mDisableCab:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->isViewCreated()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->finishSelectionMode()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/MessageListFragment;->isInSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/email/activity/MessageListFragment;->updateSelectionModeView()V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;-><init>(Lcom/android/email/activity/MessageListFragment;Lcom/android/email/activity/MessageListFragment$1;)V

    iput-object v1, p0, Lcom/android/email/activity/MessageListFragment;->mLastSelectionModeCallback:Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageListFragment;->mLastSelectionModeCallback:Lcom/android/email/activity/MessageListFragment$SelectionModeCallback;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto :goto_0
.end method
