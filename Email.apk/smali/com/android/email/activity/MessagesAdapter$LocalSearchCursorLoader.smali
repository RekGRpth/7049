.class public Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;
.super Lcom/android/email/data/ThrottlingCursorLoader;
.source "MessagesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessagesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalSearchCursorLoader"
.end annotation


# instance fields
.field private final mAccountId:J

.field protected final mContext:Landroid/content/Context;

.field private mIsFirstLoad:Z

.field private final mListContext:Lcom/android/email/MessageListContext;

.field private final mMailboxId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/email/MessageListContext;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/email/MessageListContext;

    const/4 v4, 0x0

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/android/email/activity/MessagesAdapter;->MESSAGE_PROJECTION:[Ljava/lang/String;

    const-string v6, "timeStamp DESC"

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/android/email/data/ThrottlingCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mIsFirstLoad:Z

    iput-object p1, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    iget-wide v0, p2, Lcom/android/email/MessageListContext;->mAccountId:J

    iput-wide v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mAccountId:J

    invoke-virtual {p2}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mMailboxId:J

    iput-object p2, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mListContext:Lcom/android/email/MessageListContext;

    return-void
.end method

.method private loadExtras(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-wide v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mMailboxId:J

    const-wide/16 v8, 0x0

    cmp-long v0, v0, v8

    if-gez v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/android/emailcommon/provider/EmailContent;->count(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v7

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->wrapCursor(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZI)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    iget-wide v8, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mMailboxId:J

    invoke-static {v0, v8, v9}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    iget-wide v8, v4, Lcom/android/emailcommon/provider/Mailbox;->mAccountKey:J

    invoke-static {v0, v8, v9}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Lcom/android/emailcommon/provider/Account;->isEasAccount(Landroid/content/Context;)Z

    move-result v5

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    iget-wide v8, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mMailboxId:J

    invoke-static {v0, v8, v9}, Lcom/android/emailcommon/provider/Mailbox;->isRefreshable(Landroid/content/Context;J)Z

    move-result v6

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 13

    const/4 v12, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v11

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    sput-boolean v12, Lcom/android/emailcommon/provider/EmailContent$Message;->sNewLocalSearchStarted:Z

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    iget-wide v1, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mAccountId:J

    iget-wide v3, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mMailboxId:J

    iget-object v5, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v5}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v5

    iget-object v5, v5, Lcom/android/emailcommon/service/SearchParams;->mFilter:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v6}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v6

    iget-object v6, v6, Lcom/android/emailcommon/service/SearchParams;->mField:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/android/emailcommon/provider/EmailContent$Message;->buildLocalSearchSelection(Landroid/content/Context;JJLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/CursorLoader;->setSelection(Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/emailcommon/provider/EmailContent$Message;->sNewLocalSearchStarted:Z

    if-eqz v0, :cond_0

    const/4 v7, 0x0

    :goto_0
    return-object v7

    :cond_0
    invoke-super {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v10

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v1, Lcom/android/email/activity/MessagesAdapter;->sDeletedSet:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/email/activity/MessagesAdapter;->sDeletedSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v9, 0x1

    :goto_1
    if-eqz v9, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->mContext:Landroid/content/Context;

    invoke-static {v0, v10}, Lcom/android/email/activity/MessagesAdapter;->access$000(Landroid/content/Context;Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v8

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v9, :cond_3

    :goto_2
    invoke-direct {p0, v8}, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->loadExtras(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/Thread;->setPriority(I)V

    goto :goto_0

    :cond_2
    move v9, v12

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object v8, v10

    goto :goto_2
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter$LocalSearchCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected wrapCursor(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZI)Landroid/database/Cursor;
    .locals 9
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Lcom/android/emailcommon/provider/Account;
    .param p4    # Lcom/android/emailcommon/provider/Mailbox;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I

    new-instance v0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;

    const/4 v8, 0x0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;-><init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZILcom/android/email/activity/MessagesAdapter$1;)V

    return-object v0
.end method
