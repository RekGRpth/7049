.class public Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;
.super Lcom/android/email/activity/MessagesAdapter$MessagesCursor;
.source "MessagesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessagesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchResultsCursor"
.end annotation


# instance fields
.field private final mResultsCount:I

.field private final mSearchedMailbox:Lcom/android/emailcommon/provider/Mailbox;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZILcom/android/emailcommon/provider/Mailbox;I)V
    .locals 10
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Lcom/android/emailcommon/provider/Account;
    .param p4    # Lcom/android/emailcommon/provider/Mailbox;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # Lcom/android/emailcommon/provider/Mailbox;
    .param p9    # I

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;-><init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZILcom/android/email/activity/MessagesAdapter$1;)V

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->mSearchedMailbox:Lcom/android/emailcommon/provider/Mailbox;

    move/from16 v0, p9

    iput v0, p0, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->mResultsCount:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZILcom/android/emailcommon/provider/Mailbox;ILcom/android/email/activity/MessagesAdapter$1;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Lcom/android/emailcommon/provider/Account;
    .param p4    # Lcom/android/emailcommon/provider/Mailbox;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # Lcom/android/emailcommon/provider/Mailbox;
    .param p9    # I
    .param p10    # Lcom/android/email/activity/MessagesAdapter$1;

    invoke-direct/range {p0 .. p9}, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;-><init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZILcom/android/emailcommon/provider/Mailbox;I)V

    return-void
.end method


# virtual methods
.method public getResultsCount()I
    .locals 1

    iget v0, p0, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->mResultsCount:I

    return v0
.end method

.method public getSearchedMailbox()Lcom/android/emailcommon/provider/Mailbox;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;->mSearchedMailbox:Lcom/android/emailcommon/provider/Mailbox;

    return-object v0
.end method
