.class Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;
.super Ljava/lang/Object;
.source "MoveMessageToDialog.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MoveMessageToDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MailboxesLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/MoveMessageToDialog;


# direct methods
.method private constructor <init>(Lcom/android/email/activity/MoveMessageToDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/email/activity/MoveMessageToDialog;Lcom/android/email/activity/MoveMessageToDialog$1;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MoveMessageToDialog;
    .param p2    # Lcom/android/email/activity/MoveMessageToDialog$1;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;-><init>(Lcom/android/email/activity/MoveMessageToDialog;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-static {v1}, Lcom/android/email/activity/MoveMessageToDialog;->access$600(Lcom/android/email/activity/MoveMessageToDialog;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-static {v3}, Lcom/android/email/activity/MoveMessageToDialog;->access$700(Lcom/android/email/activity/MoveMessageToDialog;)J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/email/activity/MailboxMoveToAdapter;->createLoader(Landroid/content/Context;JJ)Landroid/content/Loader;

    move-result-object v0

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-static {v1}, Lcom/android/email/activity/MoveMessageToDialog;->access$100(Lcom/android/email/activity/MoveMessageToDialog;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-static {v1}, Lcom/android/email/activity/MoveMessageToDialog;->access$900(Lcom/android/email/activity/MoveMessageToDialog;)Lcom/android/email/activity/MailboxMoveToAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-static {v1}, Lcom/android/email/activity/MoveMessageToDialog;->access$900(Lcom/android/email/activity/MoveMessageToDialog;)Lcom/android/email/activity/MailboxMoveToAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-virtual {v1}, Landroid/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-virtual {v1}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;->this$0:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-static {v0}, Lcom/android/email/activity/MoveMessageToDialog;->access$900(Lcom/android/email/activity/MoveMessageToDialog;)Lcom/android/email/activity/MailboxMoveToAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method
