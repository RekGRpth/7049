.class Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;
.super Landroid/content/AsyncTaskLoader;
.source "MoveMessageToDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MoveMessageToDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageChecker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/AsyncTaskLoader",
        "<",
        "Lcom/android/email/activity/MoveMessageToDialog$IdContainer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mMessageIds:[J


# direct methods
.method public constructor <init>(Landroid/app/Activity;[J)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # [J

    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->mMessageIds:[J

    return-void
.end method


# virtual methods
.method public loadInBackground()Lcom/android/email/activity/MoveMessageToDialog$IdContainer;
    .locals 15

    invoke-virtual {p0}, Landroid/content/Loader;->getContext()Landroid/content/Context;

    move-result-object v7

    const-wide/16 v1, -0x1

    const-wide/16 v3, -0x1

    iget-object v6, p0, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->mMessageIds:[J

    array-length v9, v6

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_2

    aget-wide v11, v6, v8

    invoke-static {v7, v11, v12}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v10

    if-nez v10, :cond_1

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v13, -0x1

    cmp-long v0, v1, v13

    if-nez v0, :cond_4

    iget-wide v1, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v7, v1, v2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/emailcommon/provider/Account;->supportsMoveMessages(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->mActivity:Landroid/app/Activity;

    const v5, 0x7f080063

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    const-wide/16 v1, -0x1

    :cond_2
    :goto_1
    new-instance v0, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;-><init>(JJLcom/android/email/activity/MoveMessageToDialog$1;)V

    return-object v0

    :cond_3
    iget-wide v3, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static {v7, v3, v4}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/emailcommon/provider/Mailbox;->canHaveMessagesMoved()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->mActivity:Landroid/app/Activity;

    const v5, 0x7f080065

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    const-wide/16 v1, -0x1

    const-wide/16 v3, -0x1

    goto :goto_1

    :cond_4
    iget-wide v13, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    cmp-long v0, v13, v1

    if-nez v0, :cond_5

    iget-wide v13, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    cmp-long v0, v13, v3

    if-eqz v0, :cond_0

    :cond_5
    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->mActivity:Landroid/app/Activity;

    const v5, 0x7f080064

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    const-wide/16 v1, -0x1

    const-wide/16 v3, -0x1

    goto :goto_1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;->loadInBackground()Lcom/android/email/activity/MoveMessageToDialog$IdContainer;

    move-result-object v0

    return-object v0
.end method

.method protected onReset()V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Loader;->stopLoading()V

    return-void
.end method

.method protected onStartLoading()V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Loader;->cancelLoad()Z

    invoke-virtual {p0}, Landroid/content/Loader;->forceLoad()V

    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Loader;->cancelLoad()Z

    return-void
.end method
