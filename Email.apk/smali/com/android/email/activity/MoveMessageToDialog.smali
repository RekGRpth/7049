.class public Lcom/android/email/activity/MoveMessageToDialog;
.super Landroid/app/DialogFragment;
.source "MoveMessageToDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MoveMessageToDialog$IdContainer;,
        Lcom/android/email/activity/MoveMessageToDialog$MessageChecker;,
        Lcom/android/email/activity/MoveMessageToDialog$MailboxesLoaderCallbacks;,
        Lcom/android/email/activity/MoveMessageToDialog$MessageCheckerCallback;,
        Lcom/android/email/activity/MoveMessageToDialog$Callback;
    }
.end annotation


# static fields
.field private static final BUNDLE_MESSAGE_IDS:Ljava/lang/String; = "message_ids"

.field private static final LOADER_ID_MOVE_TO_DIALOG_MAILBOX_LOADER:I = 0x1

.field private static final LOADER_ID_MOVE_TO_DIALOG_MESSAGE_CHECKER:I = 0x2

.field static sActiveDialog:Lcom/android/email/activity/MoveMessageToDialog;


# instance fields
.field private mAccountId:J

.field private mAdapter:Lcom/android/email/activity/MailboxMoveToAdapter;

.field private mDestroyed:Z

.field private mMailboxId:J

.field private mMessageIds:[J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/activity/MoveMessageToDialog;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;

    iget-boolean v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/email/activity/MoveMessageToDialog;)[J
    .locals 1
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mMessageIds:[J

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/email/activity/MoveMessageToDialog;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;

    invoke-direct {p0}, Lcom/android/email/activity/MoveMessageToDialog;->dismissAsync()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/email/activity/MoveMessageToDialog;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;

    iget-wide v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAccountId:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/android/email/activity/MoveMessageToDialog;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAccountId:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/android/email/activity/MoveMessageToDialog;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;

    iget-wide v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mMailboxId:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/android/email/activity/MoveMessageToDialog;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/MoveMessageToDialog;->mMailboxId:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/android/email/activity/MoveMessageToDialog;)Lcom/android/email/activity/MailboxMoveToAdapter;
    .locals 1
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog;

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAdapter:Lcom/android/email/activity/MailboxMoveToAdapter;

    return-object v0
.end method

.method private dismissAsync()V
    .locals 2

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/email/activity/MoveMessageToDialog$1;

    invoke-direct {v1, p0}, Lcom/android/email/activity/MoveMessageToDialog$1;-><init>(Lcom/android/email/activity/MoveMessageToDialog;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static newInstance([JLandroid/app/Fragment;)Lcom/android/email/activity/MoveMessageToDialog;
    .locals 3
    .param p0    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Fragment;",
            ":",
            "Lcom/android/email/activity/MoveMessageToDialog$Callback;",
            ">([JTT;)",
            "Lcom/android/email/activity/MoveMessageToDialog;"
        }
    .end annotation

    array-length v2, p0

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_1
    new-instance v1, Lcom/android/email/activity/MoveMessageToDialog;

    invoke-direct {v1}, Lcom/android/email/activity/MoveMessageToDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message_ids"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/app/Fragment;->setTargetFragment(Landroid/app/Fragment;I)V

    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    sget-object v0, Lcom/android/email/activity/MoveMessageToDialog;->sActiveDialog:Lcom/android/email/activity/MoveMessageToDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/email/activity/MoveMessageToDialog;->sActiveDialog:Lcom/android/email/activity/MoveMessageToDialog;

    invoke-direct {v0}, Lcom/android/email/activity/MoveMessageToDialog;->dismissAsync()V

    :cond_0
    sput-object p0, Lcom/android/email/activity/MoveMessageToDialog;->sActiveDialog:Lcom/android/email/activity/MoveMessageToDialog;

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAdapter:Lcom/android/email/activity/MailboxMoveToAdapter;

    invoke-virtual {v2, p2}, Landroid/widget/CursorAdapter;->getItemId(I)J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/android/email/activity/MoveMessageToDialog$Callback;

    iget-object v3, p0, Lcom/android/email/activity/MoveMessageToDialog;->mMessageIds:[J

    invoke-interface {v2, v0, v1, v3}, Lcom/android/email/activity/MoveMessageToDialog$Callback;->onMoveToMailboxSelected(J[J)V

    invoke-virtual {p0}, Landroid/app/DialogFragment;->dismiss()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCreate  target="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mMessageIds:[J

    const/4 v0, 0x0

    const v1, 0x103006e

    invoke-virtual {p0, v0, v1}, Landroid/app/DialogFragment;->setStyle(II)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08005f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/email/activity/MailboxMoveToAdapter;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/email/activity/MailboxMoveToAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAdapter:Lcom/android/email/activity/MailboxMoveToAdapter;

    iget-object v2, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAdapter:Lcom/android/email/activity/MailboxMoveToAdapter;

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x2

    new-instance v4, Lcom/android/email/activity/MoveMessageToDialog$MessageCheckerCallback;

    invoke-direct {v4, p0, v5}, Lcom/android/email/activity/MoveMessageToDialog$MessageCheckerCallback;-><init>(Lcom/android/email/activity/MoveMessageToDialog;Lcom/android/email/activity/MoveMessageToDialog$1;)V

    invoke-virtual {v2, v3, v5, v4}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mDestroyed:Z

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    sget-object v0, Lcom/android/email/activity/MoveMessageToDialog;->sActiveDialog:Lcom/android/email/activity/MoveMessageToDialog;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lcom/android/email/activity/MoveMessageToDialog;->sActiveDialog:Lcom/android/email/activity/MoveMessageToDialog;

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    iget-object v0, p0, Lcom/android/email/activity/MoveMessageToDialog;->mAdapter:Lcom/android/email/activity/MailboxMoveToAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    :cond_0
    return-void
.end method
