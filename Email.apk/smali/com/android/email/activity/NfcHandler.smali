.class public Lcom/android/email/activity/NfcHandler;
.super Ljava/lang/Object;
.source "NfcHandler.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# instance fields
.field final mActivity:Landroid/app/Activity;

.field mCurrentEmail:Ljava/lang/String;

.field final mUiController:Lcom/android/email/activity/UIControllerBase;


# direct methods
.method public constructor <init>(Lcom/android/email/activity/UIControllerBase;Landroid/app/Activity;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/UIControllerBase;
    .param p2    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/email/activity/NfcHandler;->mUiController:Lcom/android/email/activity/UIControllerBase;

    iput-object p2, p0, Lcom/android/email/activity/NfcHandler;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method private static buildMailtoNdef(Ljava/lang/String;)Landroid/nfc/NdefMessage;
    .locals 9
    .param p0    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return-object v4

    :cond_0
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {p0, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    array-length v4, v0

    add-int/lit8 v4, v4, 0x1

    new-array v3, v4, [B

    const/4 v4, 0x6

    aput-byte v4, v3, v7

    array-length v4, v0

    invoke-static {v0, v7, v3, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v2, Landroid/nfc/NdefRecord;

    sget-object v4, Landroid/nfc/NdefRecord;->RTD_URI:[B

    new-array v5, v7, [B

    invoke-direct {v2, v8, v4, v5, v3}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    new-instance v4, Landroid/nfc/NdefMessage;

    new-array v5, v8, [Landroid/nfc/NdefRecord;

    aput-object v2, v5, v7

    invoke-direct {v4, v5}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static register(Lcom/android/email/activity/UIControllerBase;Landroid/app/Activity;)Lcom/android/email/activity/NfcHandler;
    .locals 3
    .param p0    # Lcom/android/email/activity/UIControllerBase;
    .param p1    # Landroid/app/Activity;

    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/android/email/activity/NfcHandler;

    invoke-direct {v1, p0, p1}, Lcom/android/email/activity/NfcHandler;-><init>(Lcom/android/email/activity/UIControllerBase;Landroid/app/Activity;)V

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p1, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 1
    .param p1    # Landroid/nfc/NfcEvent;

    iget-object v0, p0, Lcom/android/email/activity/NfcHandler;->mCurrentEmail:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/NfcHandler;->mCurrentEmail:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/email/activity/NfcHandler;->buildMailtoNdef(Ljava/lang/String;)Landroid/nfc/NdefMessage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccountChanged()V
    .locals 4

    iget-object v3, p0, Lcom/android/email/activity/NfcHandler;->mUiController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v3}, Lcom/android/email/activity/UIControllerBase;->isActualAccountSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/email/activity/NfcHandler;->mUiController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v3}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/email/activity/NfcHandler;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v1, v2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, v0, Lcom/android/emailcommon/provider/Account;->mEmailAddress:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/email/activity/NfcHandler;->mCurrentEmail:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/email/activity/NfcHandler;->mCurrentEmail:Ljava/lang/String;

    goto :goto_0
.end method
