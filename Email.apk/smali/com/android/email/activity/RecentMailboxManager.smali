.class public Lcom/android/email/activity/RecentMailboxManager;
.super Ljava/lang/Object;
.source "RecentMailboxManager.java"


# static fields
.field static final DEFAULT_RECENT_TYPES:[I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final LIMIT_RESULTS:I = 0x5

.field public static final RECENT_MAILBOXES_SORT_ORDER:Ljava/lang/String; = "displayName"

.field private static final RECENT_SELECTION:Ljava/lang/String; = "_id IN ( SELECT _id FROM Mailbox WHERE ( accountKey=?  AND type<64 AND flagVisible=1 AND type!=0 AND lastTouchedTime>0 ) ORDER BY lastTouchedTime DESC LIMIT ? )"

.field private static final RECENT_SELECTION_WITH_EXCLUSIONS:Ljava/lang/String; = "_id IN ( SELECT _id FROM Mailbox WHERE ( accountKey=?  AND type<64 AND flagVisible=1 AND type=1 AND lastTouchedTime>0 ) ORDER BY lastTouchedTime DESC LIMIT ? )"

.field private static final TAG:Ljava/lang/String; = "RecentMailboxManager"

.field static sClock:Lcom/android/email/Clock;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field static sInstance:Lcom/android/email/activity/RecentMailboxManager;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDefaultRecentsInitialized:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/android/email/Clock;->INSTANCE:Lcom/android/email/Clock;

    sput-object v0, Lcom/android/email/activity/RecentMailboxManager;->sClock:Lcom/android/email/Clock;

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/email/activity/RecentMailboxManager;->DEFAULT_RECENT_TYPES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x3
        0x5
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/email/activity/RecentMailboxManager;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/RecentMailboxManager;->mDefaultRecentsInitialized:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/RecentMailboxManager;JJ)V
    .locals 0
    .param p0    # Lcom/android/email/activity/RecentMailboxManager;
    .param p1    # J
    .param p3    # J

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/email/activity/RecentMailboxManager;->ensureDefaultsInitialized(JJ)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/activity/RecentMailboxManager;JJJ)V
    .locals 0
    .param p0    # Lcom/android/email/activity/RecentMailboxManager;
    .param p1    # J
    .param p3    # J
    .param p5    # J

    invoke-direct/range {p0 .. p6}, Lcom/android/email/activity/RecentMailboxManager;->touchMailboxSynchronous(JJJ)V

    return-void
.end method

.method private declared-synchronized ensureDefaultsInitialized(JJ)V
    .locals 12
    .param p1    # J
    .param p3    # J

    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/android/email/activity/RecentMailboxManager;->mDefaultRecentsInitialized:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x2

    :try_start_1
    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v0

    iget-object v0, p0, Lcom/android/email/activity/RecentMailboxManager;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id IN ( SELECT _id FROM Mailbox WHERE ( accountKey=?  AND type<64 AND flagVisible=1 AND type!=0 AND lastTouchedTime>0 ) ORDER BY lastTouchedTime DESC LIMIT ? )"

    invoke-static {v0, v1, v2, v7}, Lcom/android/emailcommon/provider/EmailContent;->count(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v8, Lcom/android/email/activity/RecentMailboxManager;->DEFAULT_RECENT_TYPES:[I

    array-length v10, v8

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v10, :cond_1

    aget v11, v8, v9

    iget-object v0, p0, Lcom/android/email/activity/RecentMailboxManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v11}, Lcom/android/email/Controller;->findOrCreateMailboxOfType(JI)J

    move-result-wide v3

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/email/activity/RecentMailboxManager;->touchMailboxSynchronous(JJJ)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/RecentMailboxManager;->mDefaultRecentsInitialized:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private fireAndForget(JJJ)Lcom/android/emailcommon/utility/EmailAsyncTask;
    .locals 8
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ)",
            "Lcom/android/emailcommon/utility/EmailAsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/email/activity/RecentMailboxManager$1;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p5

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/android/email/activity/RecentMailboxManager$1;-><init>(Lcom/android/email/activity/RecentMailboxManager;JJJ)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/email/activity/RecentMailboxManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/android/email/activity/RecentMailboxManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/email/activity/RecentMailboxManager;->sInstance:Lcom/android/email/activity/RecentMailboxManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/email/activity/RecentMailboxManager;

    invoke-direct {v0, p0}, Lcom/android/email/activity/RecentMailboxManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/email/activity/RecentMailboxManager;->sInstance:Lcom/android/email/activity/RecentMailboxManager;

    :cond_0
    sget-object v0, Lcom/android/email/activity/RecentMailboxManager;->sInstance:Lcom/android/email/activity/RecentMailboxManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private touchMailboxSynchronous(JJJ)V
    .locals 4
    .param p1    # J
    .param p3    # J
    .param p5    # J

    const/4 v3, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "lastTouchedTime"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/android/email/activity/RecentMailboxManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getMostRecent(JZ)Ljava/util/ArrayList;
    .locals 9
    .param p1    # J
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const/4 v8, 0x0

    sget-object v0, Lcom/android/email/activity/RecentMailboxManager;->sClock:Lcom/android/email/Clock;

    invoke-virtual {v0}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/email/activity/RecentMailboxManager;->ensureDefaultsInitialized(JJ)V

    if-eqz p3, :cond_0

    const-string v3, "_id IN ( SELECT _id FROM Mailbox WHERE ( accountKey=?  AND type<64 AND flagVisible=1 AND type=1 AND lastTouchedTime>0 ) ORDER BY lastTouchedTime DESC LIMIT ? )"

    :goto_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/email/activity/RecentMailboxManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x1

    const/4 v8, 0x5

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const-string v5, "displayName"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    const-string v3, "_id IN ( SELECT _id FROM Mailbox WHERE ( accountKey=?  AND type<64 AND flagVisible=1 AND type!=0 AND lastTouchedTime>0 ) ORDER BY lastTouchedTime DESC LIMIT ? )"

    goto :goto_0

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public touch(JJ)Lcom/android/emailcommon/utility/EmailAsyncTask;
    .locals 7
    .param p1    # J
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lcom/android/emailcommon/utility/EmailAsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/android/email/activity/RecentMailboxManager;->sClock:Lcom/android/email/Clock;

    invoke-virtual {v0}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/email/activity/RecentMailboxManager;->fireAndForget(JJJ)Lcom/android/emailcommon/utility/EmailAsyncTask;

    move-result-object v0

    return-object v0
.end method
