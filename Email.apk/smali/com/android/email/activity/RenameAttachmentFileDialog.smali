.class public Lcom/android/email/activity/RenameAttachmentFileDialog;
.super Landroid/app/DialogFragment;
.source "RenameAttachmentFileDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;
    }
.end annotation


# static fields
.field private static final ATTACHMENT_FILE_RENAME_NEWNAME:Ljava/lang/String; = "attachment_file_rename_newname"

.field private static final INVALID_FILENAME_PATTERN:Ljava/lang/String; = ".*[/\\\\:*?\"<>|].*"


# instance fields
.field private mAttachmentInfo:Lcom/android/email/AttachmentInfo;

.field private mCallback:Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;

.field private mDialog:Landroid/app/AlertDialog;

.field private mEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static isFileNameValid(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".*[/\\\\:*?\"<>|].*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static newInstance(Lcom/android/email/AttachmentInfo;Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;)Lcom/android/email/activity/RenameAttachmentFileDialog;
    .locals 1
    .param p0    # Lcom/android/email/AttachmentInfo;
    .param p1    # Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;

    new-instance v0, Lcom/android/email/activity/RenameAttachmentFileDialog;

    invoke-direct {v0}, Lcom/android/email/activity/RenameAttachmentFileDialog;-><init>()V

    iput-object p0, v0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mAttachmentInfo:Lcom/android/email/AttachmentInfo;

    iput-object p1, v0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mCallback:Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1    # Landroid/text/Editable;

    const/4 v3, -0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/email/activity/RenameAttachmentFileDialog;->isFileNameValid(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/email/activity/RenameAttachmentFileDialog;->isFileNameValid(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f080032

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Utility;->showToastShortTime(Landroid/content/Context;I)V

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mAttachmentInfo:Lcom/android/email/AttachmentInfo;

    iget-object v1, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mCallback:Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;

    iget-object v1, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mAttachmentInfo:Lcom/android/email/AttachmentInfo;

    invoke-interface {v0, v1}, Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;->onOkayClicked(Lcom/android/email/AttachmentInfo;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040020

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0f0057

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    if-eqz p1, :cond_1

    const-string v4, "attachment_file_rename_newname"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/TextView;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    const/16 v5, 0x100

    const/4 v6, 0x1

    invoke-static {v4, v1, v5, v6}, Lcom/android/email/activity/UiUtilities;->setupLengthFilter(Landroid/widget/EditText;Landroid/content/Context;IZ)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080031

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1010355

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f08003d

    invoke-virtual {v4, v5, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f08003c

    invoke-virtual {v4, v5, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mDialog:Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v4

    :cond_1
    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mAttachmentInfo:Lcom/android/email/AttachmentInfo;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mAttachmentInfo:Lcom/android/email/AttachmentInfo;

    iget-object v5, v5, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/RenameAttachmentFileDialog;->isFileNameValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "attachment_file_rename_newname"

    iget-object v1, p0, Lcom/android/email/activity/RenameAttachmentFileDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
