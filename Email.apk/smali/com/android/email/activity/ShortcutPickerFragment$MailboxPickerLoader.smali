.class final Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;
.super Landroid/content/CursorLoader;
.source "ShortcutPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/ShortcutPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MailboxPickerLoader"
.end annotation


# instance fields
.field private final mAccountId:J

.field private final mAllowUnread:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # Z

    invoke-direct/range {p0 .. p6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-wide p7, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAccountId:J

    iput-boolean p9, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAllowUnread:Z

    return-void
.end method

.method private getValue(Landroid/database/Cursor;I)Ljava/lang/Object;
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private updateMailboxNames(Landroid/content/Context;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;

    if-nez p2, :cond_1

    const/4 v4, 0x0

    :cond_0
    return-object v4

    :cond_1
    invoke-static {}, Lcom/android/email/FolderProperties;->removeInstance()V

    invoke-static {p1}, Lcom/android/email/FolderProperties;->getInstance(Landroid/content/Context;)Lcom/android/email/FolderProperties;

    move-result-object v2

    new-instance v4, Lcom/android/email/data/ClosingMatrixCursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7, p2}, Lcom/android/email/data/ClosingMatrixCursor;-><init>([Ljava/lang/String;Landroid/database/Cursor;)V

    const/4 v7, -0x1

    invoke-interface {p2, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v6

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    const-string v7, "displayName"

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_2

    if-ne v5, v3, :cond_3

    invoke-virtual {v2, p2}, Lcom/android/email/FolderProperties;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v6, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0, p2, v3}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->getValue(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 13

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v4, Landroid/database/MatrixCursor;

    invoke-static {}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxShortcutPickerFragment;->access$000()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Loader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v5, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAllowUnread:Z

    if-eqz v5, :cond_0

    const v5, 0x7f080076

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v12, [Ljava/lang/Object;

    const-wide/32 v6, 0x7ffffffc

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    const-wide/16 v6, -0x3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v10

    aput-object v3, v5, v11

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    iget-wide v5, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAccountId:J

    const-wide/high16 v7, 0x1000000000000000L

    cmp-long v5, v5, v7

    if-nez v5, :cond_1

    new-instance v0, Landroid/database/MatrixCursor;

    invoke-static {}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxShortcutPickerFragment;->access$000()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    const v5, 0x7f080077

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v12, [Ljava/lang/Object;

    const-wide/32 v6, 0x7ffffffd

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    const-wide/16 v6, -0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v10

    aput-object v3, v5, v11

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    new-instance v5, Landroid/database/MergeCursor;

    new-array v6, v11, [Landroid/database/Cursor;

    aput-object v0, v6, v9

    aput-object v4, v6, v10

    invoke-direct {v5, v6}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    :goto_0
    return-object v5

    :cond_1
    invoke-super {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->updateMailboxNames(Landroid/content/Context;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    new-instance v5, Landroid/database/MergeCursor;

    new-array v6, v11, [Landroid/database/Cursor;

    aput-object v2, v6, v9

    aput-object v4, v6, v10

    invoke-direct {v5, v6}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
