.class public Lcom/android/email/activity/ThreePaneLayout;
.super Landroid/widget/LinearLayout;
.source "ThreePaneLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/ThreePaneLayout$1;,
        Lcom/android/email/activity/ThreePaneLayout$SavedState;,
        Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;,
        Lcom/android/email/activity/ThreePaneLayout$EmptyCallback;,
        Lcom/android/email/activity/ThreePaneLayout$Callback;
    }
.end annotation


# static fields
.field private static final ANIMATION_DEBUG:Z = false

.field private static final ANIMATION_DURATION:I = 0x96

.field private static final INDEX_GONE:I = 0x2

.field private static final INDEX_INVISIBLE:I = 0x1

.field private static final INDEX_VISIBLE:I = 0x0

.field private static final INTERPOLATOR:Landroid/animation/TimeInterpolator;

.field public static final PANE_LEFT:I = 0x4

.field public static final PANE_MIDDLE:I = 0x2

.field public static final PANE_RIGHT:I = 0x1

.field private static final PROP_MAILBOX_LIST_LEFT:Ljava/lang/String; = "mailboxListLeftAnim"

.field private static final PROP_MESSAGE_LIST_WIDTH:Ljava/lang/String; = "messageListWidthAnim"

.field public static final STATE_LEFT_VISIBLE:I = 0x0

.field public static final STATE_MIDDLE_EXPANDED:I = 0x2

.field public static final STATE_RIGHT_VISIBLE:I = 0x1

.field private static final STATE_UNINITIALIZED:I = -0x1


# instance fields
.field private mCallback:Lcom/android/email/activity/ThreePaneLayout$Callback;

.field private mConvViewExpandList:Z

.field private mFirstSizeChangedDone:Z

.field private mInMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

.field private mInitialPaneState:I

.field private mIsSearchResult:Z

.field private mLastAnimator:Landroid/animation/Animator;

.field private mLastAnimatorListener:Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;

.field private mLeftPane:Landroid/view/View;

.field private mMailboxListWidth:I

.field private mMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

.field private mMessageListWidth:I

.field private mMiddlePane:Landroid/view/View;

.field private mPaneState:I

.field private mRightPane:Landroid/view/View;

.field private mShowHideViews:[[[Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fe00000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/android/email/activity/ThreePaneLayout;->INTERPOLATOR:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    iput v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    sget-object v0, Lcom/android/email/activity/ThreePaneLayout$EmptyCallback;->INSTANCE:Lcom/android/email/activity/ThreePaneLayout$Callback;

    iput-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mCallback:Lcom/android/email/activity/ThreePaneLayout$Callback;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    iput v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    sget-object v0, Lcom/android/email/activity/ThreePaneLayout$EmptyCallback;->INSTANCE:Lcom/android/email/activity/ThreePaneLayout$Callback;

    iput-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mCallback:Lcom/android/email/activity/ThreePaneLayout$Callback;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    iput v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    sget-object v0, Lcom/android/email/activity/ThreePaneLayout$EmptyCallback;->INSTANCE:Lcom/android/email/activity/ThreePaneLayout$Callback;

    iput-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mCallback:Lcom/android/email/activity/ThreePaneLayout$Callback;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->initView()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/ThreePaneLayout;)Lcom/android/email/activity/ThreePaneLayout$Callback;
    .locals 1
    .param p0    # Lcom/android/email/activity/ThreePaneLayout;

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mCallback:Lcom/android/email/activity/ThreePaneLayout$Callback;

    return-object v0
.end method

.method private changePaneState(IZ)Z
    .locals 14
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Lcom/android/email/activity/ThreePaneLayout;->isPaneCollapsible()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    :cond_0
    iget-boolean v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mFirstSizeChangedDone:Z

    if-nez v1, :cond_1

    iput p1, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    iget v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    if-ne p1, v1, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_3

    const/16 p2, 0x0

    :cond_3
    invoke-virtual {p0}, Lcom/android/email/activity/ThreePaneLayout;->getVisiblePanes()I

    move-result v6

    iput p1, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getMailboxListWidth()I

    move-result v3

    invoke-direct {p0, v1, v3}, Lcom/android/email/activity/ThreePaneLayout;->setViewWidth(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getMessageListWidth()I

    move-result v3

    sub-int v3, v10, v3

    invoke-direct {p0, v1, v3}, Lcom/android/email/activity/ThreePaneLayout;->setViewWidth(Landroid/view/View;I)V

    iget v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :pswitch_0
    const-string v2, "moving to [mailbox list + message list]"

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getMailboxListWidth()I

    move-result v1

    sub-int v8, v10, v1

    :goto_1
    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    invoke-direct {p0, v1, v8}, Lcom/android/email/activity/ThreePaneLayout;->setViewWidth(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mShowHideViews:[[[Landroid/view/View;

    iget v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    aget-object v9, v1, v3

    new-instance v0, Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;

    const/4 v1, 0x0

    aget-object v3, v9, v1

    const/4 v1, 0x1

    aget-object v4, v9, v1

    const/4 v1, 0x2

    aget-object v5, v9, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;-><init>(Lcom/android/email/activity/ThreePaneLayout;Ljava/lang/String;[Landroid/view/View;[Landroid/view/View;[Landroid/view/View;I)V

    if-eqz p2, :cond_4

    const/16 v1, 0x96

    :goto_2
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    const/4 v4, 0x0

    const-string v5, "mailboxListLeftAnim"

    const/4 v11, 0x2

    new-array v11, v11, [I

    const/4 v12, 0x0

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getCurrentMailboxLeft()I

    move-result v13

    aput v13, v11, v12

    const/4 v12, 0x1

    aput v7, v11, v12

    invoke-static {v5, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "messageListWidthAnim"

    const/4 v11, 0x2

    new-array v11, v11, [I

    const/4 v12, 0x0

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getCurrentMessageListWidth()I

    move-result v13

    aput v13, v11, v12

    const/4 v12, 0x1

    aput v8, v11, v12

    invoke-static {v5, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {p0, v1, v0, v3}, Lcom/android/email/activity/ThreePaneLayout;->startLayoutAnimation(ILcom/android/email/activity/ThreePaneLayout$AnimatorListener;[Landroid/animation/PropertyValuesHolder;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_1
    const-string v2, "moving to [message list + message view]"

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getMailboxListWidth()I

    move-result v1

    neg-int v7, v1

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->getMessageListWidth()I

    move-result v8

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getCurrentMailboxLeft()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    return v0
.end method

.method private getCurrentMessageListWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    return v0
.end method

.method private getMailboxListWidth()I
    .locals 1

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->shouldShowMailboxList()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mMailboxListWidth:I

    goto :goto_0
.end method

.method private getMessageListWidth()I
    .locals 2

    iget-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mConvViewExpandList:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageListWidth:I

    goto :goto_0
.end method

.method private initView()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-void
.end method

.method private onFirstSizeChanged()V
    .locals 3

    const/4 v2, -0x1

    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/email/activity/ThreePaneLayout;->changePaneState(IZ)Z

    iput v2, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    :cond_0
    return-void
.end method

.method private setViewWidth(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method private shouldShowMailboxList()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/UiUtilities;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs startLayoutAnimation(ILcom/android/email/activity/ThreePaneLayout$AnimatorListener;[Landroid/animation/PropertyValuesHolder;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;
    .param p3    # [Landroid/animation/PropertyValuesHolder;

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mLastAnimator:Landroid/animation/Animator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mLastAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mLastAnimatorListener:Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mLastAnimatorListener:Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;

    invoke-virtual {v1}, Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;->cancel()V

    :cond_1
    invoke-static {p0, p3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v1, Lcom/android/email/activity/ThreePaneLayout;->INTERPOLATOR:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_2
    iput-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mLastAnimator:Landroid/animation/Animator;

    iput-object p2, p0, Lcom/android/email/activity/ThreePaneLayout;->mLastAnimatorListener:Lcom/android/email/activity/ThreePaneLayout$AnimatorListener;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method


# virtual methods
.method public getInMessageCommandButtons()Lcom/android/email/activity/MessageCommandButtonView;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mInMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    return-object v0
.end method

.method public getLeftPaneId()I
    .locals 1

    const v0, 0x7f0f00dd

    return v0
.end method

.method public getMessageCommandButtons()Lcom/android/email/activity/MessageCommandButtonView;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    return-object v0
.end method

.method public getMiddlePaneId()I
    .locals 1

    const v0, 0x7f0f00de

    return v0
.end method

.method public getPaneState()I
    .locals 1

    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    return v0
.end method

.method public getRightPaneId()I
    .locals 1

    const v0, 0x7f0f00e0

    return v0
.end method

.method public getVisiblePanes()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    or-int/lit8 v0, v0, 0x4

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_2
    return v0
.end method

.method public isLeftPaneVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMiddlePaneVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaneCollapsible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRightPaneVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 11

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v3, 0x7f0f00dd

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    const v3, 0x7f0f00de

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    const v3, 0x7f0f00df

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/email/activity/MessageCommandButtonView;

    iput-object v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    const v3, 0x7f0f00e1

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/email/activity/MessageCommandButtonView;

    iput-object v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mInMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    const v3, 0x7f0f00e0

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mConvViewExpandList:Z

    new-array v1, v10, [[Landroid/view/View;

    new-array v3, v10, [Landroid/view/View;

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    aput-object v4, v3, v9

    aput-object v3, v1, v7

    new-array v3, v8, [Landroid/view/View;

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    aput-object v4, v3, v7

    aput-object v3, v1, v8

    new-array v3, v8, [Landroid/view/View;

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mInMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    aput-object v4, v3, v7

    aput-object v3, v1, v9

    new-array v2, v10, [[Landroid/view/View;

    new-array v3, v9, [Landroid/view/View;

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mInMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    aput-object v4, v3, v8

    aput-object v3, v2, v7

    new-array v3, v10, [Landroid/view/View;

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    aput-object v4, v3, v9

    aput-object v3, v2, v8

    new-array v3, v7, [Landroid/view/View;

    aput-object v3, v2, v9

    new-array v3, v10, [[[Landroid/view/View;

    new-array v4, v10, [[Landroid/view/View;

    new-array v5, v9, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    aput-object v6, v5, v8

    aput-object v5, v4, v7

    new-array v5, v8, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/email/activity/ThreePaneLayout;->mRightPane:Landroid/view/View;

    aput-object v6, v5, v7

    aput-object v5, v4, v8

    new-array v5, v9, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/email/activity/ThreePaneLayout;->mInMessageCommandButtons:Lcom/android/email/activity/MessageCommandButtonView;

    aput-object v6, v5, v8

    aput-object v5, v4, v9

    aput-object v4, v3, v7

    iget-boolean v4, p0, Lcom/android/email/activity/ThreePaneLayout;->mConvViewExpandList:Z

    if-eqz v4, :cond_0

    :goto_0
    aput-object v1, v3, v8

    new-array v4, v10, [[Landroid/view/View;

    new-array v5, v7, [Landroid/view/View;

    aput-object v5, v4, v7

    new-array v5, v7, [Landroid/view/View;

    aput-object v5, v4, v8

    new-array v5, v7, [Landroid/view/View;

    aput-object v5, v4, v9

    aput-object v4, v3, v9

    iput-object v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mShowHideViews:[[[Landroid/view/View;

    iput v7, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mMailboxListWidth:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/android/email/activity/ThreePaneLayout;->mMessageListWidth:I

    return-void

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/android/email/activity/ThreePaneLayout$SavedState;

    invoke-virtual {v0}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/email/activity/UiUtilities;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    :goto_0
    return-void

    :cond_0
    iget v1, v0, Lcom/android/email/activity/ThreePaneLayout$SavedState;->mPaneState:I

    iput v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/android/email/activity/ThreePaneLayout$SavedState;

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/email/activity/ThreePaneLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    iput v1, v0, Lcom/android/email/activity/ThreePaneLayout$SavedState;->mPaneState:I

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    iget-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mFirstSizeChangedDone:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mFirstSizeChangedDone:Z

    invoke-direct {p0}, Lcom/android/email/activity/ThreePaneLayout;->onFirstSizeChanged()V

    :cond_0
    return-void
.end method

.method public setCallback(Lcom/android/email/activity/ThreePaneLayout$Callback;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/ThreePaneLayout$Callback;

    if-nez p1, :cond_0

    sget-object p1, Lcom/android/email/activity/ThreePaneLayout$EmptyCallback;->INSTANCE:Lcom/android/email/activity/ThreePaneLayout$Callback;

    :cond_0
    iput-object p1, p0, Lcom/android/email/activity/ThreePaneLayout;->mCallback:Lcom/android/email/activity/ThreePaneLayout$Callback;

    return-void
.end method

.method public setIsSearch(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    iput-boolean p1, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    iget-boolean v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mIsSearchResult:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/UiUtilities;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p0, Lcom/android/email/activity/ThreePaneLayout;->mInitialPaneState:I

    iget v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mPaneState:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/email/activity/ThreePaneLayout;->changePaneState(IZ)Z

    :cond_0
    return-void
.end method

.method public setMailboxListLeftAnim(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mLeftPane:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public setMessageListWidthAnim(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/activity/ThreePaneLayout;->mMiddlePane:Landroid/view/View;

    invoke-direct {p0, v0, p1}, Lcom/android/email/activity/ThreePaneLayout;->setViewWidth(Landroid/view/View;I)V

    return-void
.end method

.method public showLeftPane()Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/email/activity/ThreePaneLayout;->changePaneState(IZ)Z

    move-result v0

    return v0
.end method

.method public showRightPane()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, v0}, Lcom/android/email/activity/ThreePaneLayout;->changePaneState(IZ)Z

    move-result v0

    return v0
.end method
