.class Lcom/android/email/activity/UIControllerOnePane;
.super Lcom/android/email/activity/UIControllerBase;
.source "UIControllerOnePane.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/UIControllerOnePane$1;,
        Lcom/android/email/activity/UIControllerOnePane$ActionBarControllerCallback;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_PREVIOUS_FRAGMENT:Ljava/lang/String; = "UIControllerOnePane.PREVIOUS_FRAGMENT"


# instance fields
.field private mPreviousFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Lcom/android/email/activity/EmailActivity;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/EmailActivity;

    invoke-direct {p0, p1}, Lcom/android/email/activity/UIControllerBase;-><init>(Lcom/android/email/activity/EmailActivity;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/UIControllerOnePane;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/UIControllerOnePane;

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->isInboxShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/android/email/activity/UIControllerOnePane;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/UIControllerOnePane;

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->getMailboxId()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$200(Lcom/android/email/activity/UIControllerOnePane;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/UIControllerOnePane;

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->showAllMailboxes()V

    return-void
.end method

.method private getInstalledFragment()Landroid/app/Fragment;
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMailboxListInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageViewFragment()Lcom/android/email/activity/MessageViewFragment;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMailboxId()J
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private isInboxShown()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageListFragment;->isInboxList()Z

    move-result v0

    goto :goto_0
.end method

.method private openMailboxList(J)V
    .locals 3
    .param p1    # J

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/UIControllerBase;->setListContext(Lcom/android/email/MessageListContext;)V

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Lcom/android/email/activity/MailboxListFragment;->newInstance(JJZ)Lcom/android/email/activity/MailboxListFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/email/activity/UIControllerOnePane;->showFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method private openMessage(J)V
    .locals 1
    .param p1    # J

    invoke-static {p1, p2}, Lcom/android/email/activity/MessageViewFragment;->newInstance(J)Lcom/android/email/activity/MessageViewFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/email/activity/UIControllerOnePane;->showFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method private popFromBackStack()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->getInstalledFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/email/activity/UIControllerBase;->removeFragment(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/MailboxListFragment;

    if-eqz v2, :cond_1

    invoke-virtual {p0, v3}, Lcom/android/email/activity/UIControllerBase;->setListContext(Lcom/android/email/MessageListContext;)V

    :goto_1
    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    const/16 v2, 0x2002

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    iput-object v3, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    invoke-virtual {p0, v0}, Lcom/android/email/activity/UIControllerBase;->commitFragmentTransaction(Landroid/app/FragmentTransaction;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/MessageListFragment;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    check-cast v2, Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/email/activity/UIControllerBase;->setListContext(Lcom/android/email/MessageListContext;)V

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Message view should never be in backstack"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private shouldPopFromBackStack(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/MessageViewFragment;

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Message view should never be in backstack"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->getInstalledFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/MessageListFragment;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    check-cast v2, Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2}, Lcom/android/email/activity/MessageListFragment;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/email/MessageListContext;->isRemoteSearch()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v4

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->isInboxShown()Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/MessageListFragment;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    check-cast v2, Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v2}, Lcom/android/email/activity/MessageListFragment;->isInboxList()Z

    move-result v2

    if-nez v2, :cond_5

    instance-of v2, v0, Lcom/android/email/activity/MailboxListFragment;

    if-eqz v2, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_0
.end method

.method private showAllMailboxes()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isAccountSelected()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerOnePane;->getUIAccountId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/email/activity/UIControllerOnePane;->openMailboxList(J)V

    goto :goto_0
.end method

.method private showFragment(Landroid/app/Fragment;)V
    .locals 4
    .param p1    # Landroid/app/Fragment;

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->getInstalledFragment()Landroid/app/Fragment;

    move-result-object v1

    instance-of v3, v1, Lcom/android/email/activity/MessageViewFragment;

    if-eqz v3, :cond_0

    instance-of v3, p1, Lcom/android/email/activity/MessageViewFragment;

    if-eqz v3, :cond_0

    if-eqz v1, :cond_2

    instance-of v3, v1, Lcom/android/email/activity/MessageViewFragment;

    if-eqz v3, :cond_2

    move-object v2, v1

    check-cast v2, Lcom/android/email/activity/MessageViewFragment;

    invoke-virtual {p1}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/email/activity/MessageViewFragment;->setMessageId(Landroid/os/Bundle;)V

    invoke-virtual {v2}, Lcom/android/email/activity/MessageViewFragmentBase;->resetPartialLoading()V

    invoke-virtual {v2}, Lcom/android/email/activity/MessageViewFragmentBase;->loadMessage()V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    invoke-virtual {p0, v0, v3}, Lcom/android/email/activity/UIControllerBase;->removeFragment(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    :cond_1
    if-eqz v1, :cond_2

    instance-of v3, v1, Lcom/android/email/activity/MessageViewFragment;

    if-eqz v3, :cond_3

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_2
    :goto_1
    const v3, 0x7f0f0077

    invoke-virtual {v0, v3, p1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    const/16 v3, 0x1001

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    invoke-virtual {p0, v0}, Lcom/android/email/activity/UIControllerBase;->commitFragmentTransaction(Landroid/app/FragmentTransaction;)V

    goto :goto_0

    :cond_3
    iput-object v1, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    iget-object v3, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1
.end method


# virtual methods
.method protected canStopRefreshIcon(Z)Z
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerOnePane;->isRefreshEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected createActionBarController(Landroid/app/Activity;)Lcom/android/email/activity/ActionBarController;
    .locals 5
    .param p1    # Landroid/app/Activity;

    new-instance v0, Lcom/android/email/activity/ActionBarController;

    invoke-virtual {p1}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Lcom/android/email/activity/UIControllerOnePane$ActionBarControllerCallback;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/android/email/activity/UIControllerOnePane$ActionBarControllerCallback;-><init>(Lcom/android/email/activity/UIControllerOnePane;Lcom/android/email/activity/UIControllerOnePane$1;)V

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/android/email/activity/ActionBarController;-><init>(Landroid/content/Context;Landroid/app/LoaderManager;Landroid/app/ActionBar;Lcom/android/email/activity/ActionBarController$Callback;)V

    return-object v0
.end method

.method public getLayoutId()I
    .locals 1

    const v0, 0x7f04002c

    return v0
.end method

.method protected getMailboxSettingsMailboxId()J
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getUIAccountId()J
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    iget-wide v0, v0, Lcom/android/email/MessageListContext;->mAccountId:J

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMailboxListInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected installMailboxListFragment(Lcom/android/email/activity/MailboxListFragment;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MailboxListFragment;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->stopMessageOrderManager()V

    invoke-super {p0, p1}, Lcom/android/email/activity/UIControllerBase;->installMailboxListFragment(Lcom/android/email/activity/MailboxListFragment;)V

    return-void
.end method

.method protected installMessageListFragment(Lcom/android/email/activity/MessageListFragment;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->stopMessageOrderManager()V

    invoke-super {p0, p1}, Lcom/android/email/activity/UIControllerBase;->installMessageListFragment(Lcom/android/email/activity/MessageListFragment;)V

    return-void
.end method

.method protected isRefreshEnabled()Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isActualAccountSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMailboxListInstalled()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v1}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected isRefreshInProgress()Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerOnePane;->isRefreshEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->getMailboxId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/email/RefreshManager;->isMessageListRefreshing(J)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/email/RefreshManager;->isMailboxListRefreshing(J)Z

    move-result v0

    goto :goto_0
.end method

.method protected navigateToMessage(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/UIControllerOnePane;->openMessage(J)V

    return-void
.end method

.method public onAccountSelected(J)V
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/email/activity/UIControllerBase;->switchAccount(JZ)V

    return-void
.end method

.method public onAdvancingOpAccepted(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onBackPressed(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v0, 0x1

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onBackPressed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v1, p1}, Lcom/android/email/activity/ActionBarController;->onBackPressed(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMailboxListInstalled()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/email/activity/MailboxListFragment;->navigateUp()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_3
    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v1}, Lcom/android/email/activity/ActionBarController;->isRemoteSearchMode()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Lcom/android/email/activity/UIControllerOnePane;->shouldPopFromBackStack(Z)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->popFromBackStack()V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    iget-wide v1, v1, Lcom/android/email/MessageListContext;->mAccountId:J

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v3}, Lcom/android/email/MessageListContext;->getMailboxId()J

    move-result-wide v3

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/email/activity/UIControllerBase;->openMailbox(JJ)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMailboxListInstalled()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/email/activity/UIControllerBase;->switchAccount(JZ)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->isInboxShown()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v1}, Lcom/android/email/activity/ActionBarController;->isRemoteSearchMode()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    iget-wide v1, v1, Lcom/android/email/MessageListContext;->mAccountId:J

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/email/activity/UIControllerBase;->switchAccount(JZ)V

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCalendarLinkClicked(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v0, p1, p2}, Lcom/android/email/activity/ActivityHelper;->openCalendar(Landroid/app/Activity;J)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/MenuInflater;
    .param p2    # Landroid/view/Menu;

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0e0005

    invoke-virtual {p1, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0e0007

    invoke-virtual {p1, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDragEnded()V
    .locals 0

    return-void
.end method

.method public onDragStarted()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onForward()V
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/MessageCompose;->actionForward(Landroid/content/Context;J)V

    return-void
.end method

.method public onLoadMessageError(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onLoadMessageFinished()V
    .locals 0

    return-void
.end method

.method public onLoadMessageStarted()V
    .locals 0

    return-void
.end method

.method public onMailboxSelected(JJZ)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Z

    if-eqz p5, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/email/activity/UIControllerBase;->openMailbox(JJ)V

    goto :goto_0
.end method

.method public onMessageOpen(JJJI)V
    .locals 1
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # I

    const/4 v0, 0x1

    if-ne p7, v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v0, p1, p2}, Lcom/android/email/activity/MessageCompose;->actionEditDraft(Landroid/content/Context;J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/email/activity/UIControllerBase;->open(Lcom/android/email/MessageListContext;J)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lcom/android/email/activity/UIControllerBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->moveToNewer()Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->moveToOlder()Z

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->showAllMailboxes()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f00f6
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onParentMailboxChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->refreshActionBar()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z
    .locals 8
    .param p1    # Landroid/view/MenuInflater;
    .param p2    # Landroid/view/Menu;

    const v7, 0x7f0f00f9

    const v6, 0x7f0f00f8

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1, p2}, Lcom/android/email/activity/UIControllerBase;->onPrepareOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z

    const v3, 0x7f0f00f6

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v3}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v3}, Lcom/android/email/activity/ActionBarController;->isRemoteSearchMode()Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    :goto_0
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageOrderManager()Lcom/android/email/activity/MessageOrderManager;

    move-result-object v2

    invoke-interface {p2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p2, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/android/email/activity/MessageOrderManager;->canMoveToNewer()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_1
    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p2, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/email/activity/MessageOrderManager;->canMoveToOlder()Z

    move-result v6

    if-eqz v6, :cond_1

    move v5, v4

    :cond_1
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    return v4

    :cond_3
    move v3, v5

    goto :goto_0

    :cond_4
    move v3, v5

    goto :goto_1
.end method

.method protected onRefresh()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerOnePane;->isRefreshEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->isRemoteSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v0

    iget-object v0, v0, Lcom/android/emailcommon/service/SearchParams;->mFilter:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v1}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/service/SearchParams;->mField:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/email/activity/UIControllerBase;->onSearchSubmit(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerOnePane;->getMailboxId()J

    move-result-wide v3

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/android/email/RefreshManager;->refreshMessageList(JJZ)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/email/RefreshManager;->refreshMailboxList(J)Z

    goto :goto_0
.end method

.method public onReply()V
    .locals 4

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageId()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/android/email/activity/MessageCompose;->actionReply(Landroid/content/Context;JZ)V

    return-void
.end method

.method public onReplyAll()V
    .locals 4

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageId()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/android/email/activity/MessageCompose;->actionReply(Landroid/content/Context;JZ)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/email/activity/UIControllerBase;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v1, "UIControllerOnePane.PREVIOUS_FRAGMENT"

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/email/activity/UIControllerBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v1, "UIControllerOnePane.PREVIOUS_FRAGMENT"

    iget-object v2, p0, Lcom/android/email/activity/UIControllerOnePane;->mPreviousFragment:Landroid/app/Fragment;

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    :cond_0
    return-void
.end method

.method public onUrlInMessageClicked(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    invoke-static {v0, p1, v1, v2}, Lcom/android/email/activity/ActivityHelper;->openUrlInMessage(Landroid/app/Activity;Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public openInternal(Lcom/android/email/MessageListContext;J)V
    .locals 3
    .param p1    # Lcom/android/email/MessageListContext;
    .param p2    # J

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " open "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " messageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2, p3}, Lcom/android/email/activity/UIControllerOnePane;->openMessage(J)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/android/email/activity/MessageListFragment;->newInstance(Lcom/android/email/MessageListContext;)Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/email/activity/UIControllerOnePane;->showFragment(Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method protected updateNavigationArrows()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->refreshActionBar()V

    return-void
.end method
