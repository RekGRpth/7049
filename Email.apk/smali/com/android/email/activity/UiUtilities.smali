.class public Lcom/android/email/activity/UiUtilities;
.super Ljava/lang/Object;
.source "UiUtilities.java"


# static fields
.field private static sDebugForcedPaneMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/email/activity/UiUtilities;->sDebugForcedPaneMode:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkView(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .param p0    # Landroid/view/View;

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View doesn\'t exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public static formatSize(Landroid/content/Context;J)Ljava/lang/String;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # J

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const-wide/16 v2, 0x400

    const-wide/32 v4, 0x100000

    const-wide/32 v0, 0x40000000

    const-wide/16 v9, 0x400

    cmp-long v9, p1, v9

    if-gez v9, :cond_0

    const v7, 0x7f0d000a

    long-to-int v8, p1

    :goto_0
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    return-object v9

    :cond_0
    const-wide/32 v9, 0x100000

    cmp-long v9, p1, v9

    if-gez v9, :cond_1

    const v7, 0x7f0d000b

    const-wide/16 v9, 0x400

    div-long v9, p1, v9

    long-to-int v8, v9

    goto :goto_0

    :cond_1
    const-wide/32 v9, 0x40000000

    cmp-long v9, p1, v9

    if-gez v9, :cond_2

    const v7, 0x7f0d000c

    const-wide/32 v9, 0x100000

    div-long v9, p1, v9

    long-to-int v8, v9

    goto :goto_0

    :cond_2
    const v7, 0x7f0d000d

    const-wide/32 v9, 0x40000000

    div-long v9, p1, v9

    long-to-int v8, v9

    goto :goto_0
.end method

.method public static getMessageCountForUi(Landroid/content/Context;IZ)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Z

    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x3e7

    if-le p1, v0, :cond_1

    const v0, 0x7f0801bd

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getView(Landroid/app/Activity;I)Landroid/view/View;
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/app/Activity;",
            "I)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/UiUtilities;->checkView(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static getView(Landroid/view/View;I)Landroid/view/View;
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/UiUtilities;->checkView(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static getViewOrNull(Landroid/app/Activity;I)Landroid/view/View;
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/app/Activity;",
            "I)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static getViewOrNull(Landroid/view/View;I)Landroid/view/View;
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static installFragment(Landroid/app/Fragment;)V
    .locals 2
    .param p0    # Landroid/app/Fragment;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/email/activity/FragmentInstallable;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/email/activity/FragmentInstallable;

    invoke-interface {v0, p0}, Lcom/android/email/activity/FragmentInstallable;->onInstallFragment(Landroid/app/Fragment;)V

    :cond_0
    return-void
.end method

.method static setDebugPaneMode(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/email/activity/UiUtilities;->sDebugForcedPaneMode:I

    return-void
.end method

.method public static setVisibilitySafe(Landroid/app/Activity;II)V
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    return-void
.end method

.method public static setVisibilitySafe(Landroid/view/View;I)V
    .locals 0
    .param p0    # Landroid/view/View;
    .param p1    # I

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public static setVisibilitySafe(Landroid/view/View;II)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    return-void
.end method

.method public static setupLengthFilter(Landroid/widget/EditText;Landroid/content/Context;IZ)V
    .locals 3
    .param p0    # Landroid/widget/EditText;
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Z

    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/android/email/activity/UiUtilities$1;

    invoke-direct {v2, p2, p2, p3, p1}, Lcom/android/email/activity/UiUtilities$1;-><init>(IIZLandroid/content/Context;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public static showTwoPaneSearchResults(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public static uninstallFragment(Landroid/app/Fragment;)V
    .locals 2
    .param p0    # Landroid/app/Fragment;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/email/activity/FragmentInstallable;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/email/activity/FragmentInstallable;

    invoke-interface {v0, p0}, Lcom/android/email/activity/FragmentInstallable;->onUninstallFragment(Landroid/app/Fragment;)V

    :cond_0
    return-void
.end method

.method public static useTwoPane(Landroid/content/Context;)Z
    .locals 3
    .param p0    # Landroid/content/Context;

    const/4 v0, 0x1

    sget v1, Lcom/android/email/activity/UiUtilities;->sDebugForcedPaneMode:I

    if-ne v1, v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, Lcom/android/email/activity/UiUtilities;->sDebugForcedPaneMode:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_0
.end method
