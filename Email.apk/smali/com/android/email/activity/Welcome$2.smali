.class Lcom/android/email/activity/Welcome$2;
.super Ljava/lang/Object;
.source "Welcome.java"

# interfaces
.implements Lcom/android/email/activity/MailboxFinder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/Welcome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/Welcome;


# direct methods
.method constructor <init>(Lcom/android/email/activity/Welcome;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private cleanUp()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/email/activity/Welcome;->access$002(Lcom/android/email/activity/Welcome;Lcom/android/email/activity/MailboxFinder;)Lcom/android/email/activity/MailboxFinder;

    return-void
.end method


# virtual methods
.method public onAccountNotFound()V
    .locals 5

    const-wide/16 v3, -0x1

    invoke-direct {p0}, Lcom/android/email/activity/Welcome$2;->cleanUp()V

    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v2, v3, v4}, Lcom/android/email/activity/Welcome;->access$102(Lcom/android/email/activity/Welcome;J)J

    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v2, v3, v4}, Lcom/android/email/activity/Welcome;->access$202(Lcom/android/email/activity/Welcome;J)J

    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/email/activity/Welcome;->access$302(Lcom/android/email/activity/Welcome;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v2}, Lcom/android/email/activity/Welcome;->access$400(Lcom/android/email/activity/Welcome;)Lcom/android/email/activity/Welcome;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v2}, Lcom/android/email/activity/Welcome;->access$500(Lcom/android/email/activity/Welcome;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "MyConnectionAlertDialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/Welcome$MyConnectionAlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v2}, Lcom/android/email/activity/Welcome;->access$400(Lcom/android/email/activity/Welcome;)Lcom/android/email/activity/Welcome;

    move-result-object v2

    invoke-static {v2}, Lcom/android/email/activity/Welcome$MyConnectionAlertDialog;->newInstance(Lcom/android/email/activity/Welcome;)Lcom/android/email/activity/Welcome$MyConnectionAlertDialog;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "MyConnectionAlertDialog"

    invoke-virtual {v2, v0, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v2}, Lcom/android/email/activity/Welcome;->access$600(Lcom/android/email/activity/Welcome;)V

    goto :goto_0
.end method

.method public onAccountSecurityHold(J)V
    .locals 1
    .param p1    # J

    invoke-direct {p0}, Lcom/android/email/activity/Welcome$2;->cleanUp()V

    iget-object v0, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v0, p1, p2}, Lcom/android/email/activity/ActivityHelper;->showSecurityHoldDialog(Landroid/app/Activity;J)V

    iget-object v0, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onMailboxFound(JJ)V
    .locals 6
    .param p1    # J
    .param p3    # J

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/android/email/activity/Welcome$2;->cleanUp()V

    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1, p3, p4}, Lcom/android/email/activity/Welcome;->access$102(Lcom/android/email/activity/Welcome;J)J

    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1}, Lcom/android/email/Email;->setServicesEnabledSync(Landroid/content/Context;)Z

    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1}, Lcom/android/email/activity/Welcome;->access$500(Lcom/android/email/activity/Welcome;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1}, Lcom/android/email/activity/Welcome;->access$700(Lcom/android/email/activity/Welcome;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1, v5}, Lcom/android/email/activity/Welcome;->access$802(Lcom/android/email/activity/Welcome;Z)Z

    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    move-result-object v0

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/email/RefreshManager;->refreshMessageList(JJZ)Z

    iget-object v1, p0, Lcom/android/email/activity/Welcome$2;->this$0:Lcom/android/email/activity/Welcome;

    invoke-static {v1}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/android/email/Preferences;->setLastUsedAccountId(J)V

    goto :goto_0
.end method

.method public onMailboxNotFound(J)V
    .locals 0
    .param p1    # J

    invoke-virtual {p0}, Lcom/android/email/activity/Welcome$2;->onAccountNotFound()V

    return-void
.end method
