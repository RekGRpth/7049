.class Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;
.super Landroid/os/AsyncTask;
.source "AccountSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/setup/AccountSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadAccountListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "[",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/setup/AccountSettings;


# direct methods
.method private constructor <init>(Lcom/android/email/activity/setup/AccountSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/email/activity/setup/AccountSettings;Lcom/android/email/activity/setup/AccountSettings$1;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/setup/AccountSettings;
    .param p2    # Lcom/android/email/activity/setup/AccountSettings$1;

    invoke-direct {p0, p1}, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;-><init>(Lcom/android/email/activity/setup/AccountSettings;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->doInBackground([Ljava/lang/Long;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)[Ljava/lang/Object;
    .locals 21
    .param p1    # [Ljava/lang/Long;

    const/16 v20, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/Account;->CONTENT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    const/16 v16, 0x0

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v15

    new-array v0, v15, [Landroid/preference/PreferenceActivity$Header;

    move-object/from16 v20, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move/from16 v17, v16

    :goto_0
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v2, v8, v12

    if-nez v2, :cond_0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/4 v2, 0x2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    new-instance v19, Landroid/preference/PreferenceActivity$Header;

    invoke-direct/range {v19 .. v19}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    move-object/from16 v0, v19

    iput-wide v8, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    iput-object v0, v1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    iput-object v14, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    const-class v2, Lcom/android/email/activity/setup/AccountSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    invoke-static {v8, v9, v14}, Lcom/android/email/activity/setup/AccountSettingsFragment;->buildArguments(JLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v16, v17, 0x1

    :try_start_2
    aput-object v19, v20, v17
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move/from16 v17, v16

    goto :goto_0

    :cond_1
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v20, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    return-object v2

    :catchall_0
    move-exception v2

    :goto_1
    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    :catchall_1
    move-exception v2

    move/from16 v16, v17

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->onPostExecute([Ljava/lang/Object;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/Object;)V
    .locals 7
    .param p1    # [Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    aget-object v3, p1, v6

    check-cast v3, [Landroid/preference/PreferenceActivity$Header;

    move-object v1, v3

    check-cast v1, [Landroid/preference/PreferenceActivity$Header;

    aget-object v3, p1, v4

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-static {v3, v1}, Lcom/android/email/activity/setup/AccountSettings;->access$402(Lcom/android/email/activity/setup/AccountSettings;[Landroid/preference/PreferenceActivity$Header;)[Landroid/preference/PreferenceActivity$Header;

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-static {v3}, Lcom/android/email/activity/setup/AccountSettings;->access$500(Lcom/android/email/activity/setup/AccountSettings;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-virtual {v3}, Landroid/preference/PreferenceActivity;->invalidateHeaders()V

    :cond_2
    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-static {v3, v4}, Lcom/android/email/activity/setup/AccountSettings;->access$602(Lcom/android/email/activity/setup/AccountSettings;Z)Z

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-virtual {v3}, Landroid/app/Activity;->invalidateOptionsMenu()V

    if-nez v0, :cond_3

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    const-wide/16 v4, -0x1

    invoke-static {v3, v4, v5}, Lcom/android/email/activity/setup/AccountSettings;->access$702(Lcom/android/email/activity/setup/AccountSettings;J)J

    :cond_3
    if-eqz v1, :cond_4

    array-length v3, v1

    if-gtz v3, :cond_0

    :cond_4
    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-static {v3}, Lcom/android/email/activity/setup/AccountSettings;->access$800(Lcom/android/email/activity/setup/AccountSettings;)Landroid/app/DialogFragment;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-static {v3}, Lcom/android/email/activity/setup/AccountSettings;->access$800(Lcom/android/email/activity/setup/AccountSettings;)Landroid/app/DialogFragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-static {v3}, Lcom/android/email/activity/setup/AccountSettings;->access$800(Lcom/android/email/activity/setup/AccountSettings;)Landroid/app/DialogFragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_5
    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/email/activity/setup/AccountSettings;->access$802(Lcom/android/email/activity/setup/AccountSettings;Landroid/app/DialogFragment;)Landroid/app/DialogFragment;

    :cond_6
    const-string v3, "There are no account left, go to account setup!"

    invoke-static {v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/android/email/activity/setup/SetupData;->init(I)V

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    const-class v4, Lcom/android/email/activity/setup/AccountSetupBasics;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v3, 0x8000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-virtual {v3, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;->this$0:Lcom/android/email/activity/setup/AccountSettings;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
.end method
