.class public Lcom/android/email/activity/setup/AccountSettings;
.super Landroid/preference/PreferenceActivity;
.source "AccountSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/setup/AccountSettings$LoginFailedCallback;,
        Lcom/android/email/activity/setup/AccountSettings$LoginWarningDialog;,
        Lcom/android/email/activity/setup/AccountSettings$WaitingRemoveAccountDialog;,
        Lcom/android/email/activity/setup/AccountSettings$UnsavedChangesDialogFragment;,
        Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;,
        Lcom/android/email/activity/setup/AccountSettings$AccountServerSettingsFragmentCallback;,
        Lcom/android/email/activity/setup/AccountSettings$AccountSettingsFragmentCallback;,
        Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;
    }
.end annotation


# static fields
.field private static final EXTRA_ENABLE_DEBUG:Ljava/lang/String; = "AccountSettings.enable_debug"

.field private static final EXTRA_LOGIN_WARNING_FOR_ACCOUNT:Ljava/lang/String; = "AccountSettings.for_account"

.field private static final EXTRA_TITLE:Ljava/lang/String; = "AccountSettings.title"

.field private static final QUICK_RESPONSE_ACCOUNT_KEY:Ljava/lang/String; = "account"

.field private static final SECRET_KEY_CODES:[I

.field protected static final TAG:Ljava/lang/String; = "AccountSettings"


# instance fields
.field private mAccountListHeaders:[Landroid/preference/PreferenceActivity$Header;

.field private mAccountObserver:Landroid/database/ContentObserver;

.field private final mAccountServerSettingsFragmentCallback:Lcom/android/email/activity/setup/AccountSettings$AccountServerSettingsFragmentCallback;

.field private final mAccountSettingsFragmentCallback:Lcom/android/email/activity/setup/AccountSettings$AccountSettingsFragmentCallback;

.field private mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

.field mCurrentFragment:Landroid/app/Fragment;

.field private mDeletingAccountId:J

.field private mFinishedLoadAccount:Z

.field private mGeneratedHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;"
        }
    .end annotation
.end field

.field private mIsBackGround:Z

.field private mLoadAccountListTask:Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

.field private mNumGeneralHeaderClicked:I

.field private mProgressDialog:Landroid/app/DialogFragment;

.field private mRequestedAccountHeader:Landroid/preference/PreferenceActivity$Header;

.field private mRequestedAccountId:J

.field private mSecretKeyCodeIndex:I

.field private mShowDebugMenu:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/email/activity/setup/AccountSettings;->SECRET_KEY_CODES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x20
        0x21
        0x1e
        0x31
        0x23
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    iput v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    iput v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mNumGeneralHeaderClicked:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mDeletingAccountId:J

    new-instance v0, Lcom/android/email/activity/setup/AccountSettings$AccountSettingsFragmentCallback;

    invoke-direct {v0, p0, v2}, Lcom/android/email/activity/setup/AccountSettings$AccountSettingsFragmentCallback;-><init>(Lcom/android/email/activity/setup/AccountSettings;Lcom/android/email/activity/setup/AccountSettings$1;)V

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountSettingsFragmentCallback:Lcom/android/email/activity/setup/AccountSettings$AccountSettingsFragmentCallback;

    new-instance v0, Lcom/android/email/activity/setup/AccountSettings$AccountServerSettingsFragmentCallback;

    invoke-direct {v0, p0, v2}, Lcom/android/email/activity/setup/AccountSettings$AccountServerSettingsFragmentCallback;-><init>(Lcom/android/email/activity/setup/AccountSettings;Lcom/android/email/activity/setup/AccountSettings$1;)V

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountServerSettingsFragmentCallback:Lcom/android/email/activity/setup/AccountSettings$AccountServerSettingsFragmentCallback;

    return-void
.end method

.method static synthetic access$1000(Lcom/android/email/activity/setup/AccountSettings;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->forceBack()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/email/activity/setup/AccountSettings;I)V
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/setup/AccountSettings;->forceSwitchHeader(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/email/activity/setup/AccountSettings;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->updateAccounts()V

    return-void
.end method

.method static synthetic access$402(Lcom/android/email/activity/setup/AccountSettings;[Landroid/preference/PreferenceActivity$Header;)[Landroid/preference/PreferenceActivity$Header;
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;
    .param p1    # [Landroid/preference/PreferenceActivity$Header;

    iput-object p1, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountListHeaders:[Landroid/preference/PreferenceActivity$Header;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/email/activity/setup/AccountSettings;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;

    iget-boolean v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mIsBackGround:Z

    return v0
.end method

.method static synthetic access$602(Lcom/android/email/activity/setup/AccountSettings;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/setup/AccountSettings;->mFinishedLoadAccount:Z

    return p1
.end method

.method static synthetic access$702(Lcom/android/email/activity/setup/AccountSettings;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/setup/AccountSettings;->mDeletingAccountId:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/android/email/activity/setup/AccountSettings;)Landroid/app/DialogFragment;
    .locals 1
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mProgressDialog:Landroid/app/DialogFragment;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/email/activity/setup/AccountSettings;Landroid/app/DialogFragment;)Landroid/app/DialogFragment;
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;
    .param p1    # Landroid/app/DialogFragment;

    iput-object p1, p0, Lcom/android/email/activity/setup/AccountSettings;->mProgressDialog:Landroid/app/DialogFragment;

    return-object p1
.end method

.method static synthetic access$900(Lcom/android/email/activity/setup/AccountSettings;)Landroid/preference/PreferenceActivity$Header;
    .locals 1
    .param p0    # Lcom/android/email/activity/setup/AccountSettings;

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->getAppPreferencesHeader()Landroid/preference/PreferenceActivity$Header;

    move-result-object v0

    return-object v0
.end method

.method public static actionSettings(Landroid/app/Activity;J)V
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # J

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/email/activity/setup/AccountSettings;->createAccountSettingsIntent(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static actionSettingsWithDebug(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/email/activity/setup/AccountSettings;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "AccountSettings.enable_debug"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static createAccountSettingsIntent(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;

    const-string v2, "settings"

    invoke-static {v2}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountId(Landroid/net/Uri$Builder;J)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.EDIT"

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    if-eqz p3, :cond_0

    const-string v2, "AccountSettings.for_account"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v1
.end method

.method private enableDebugMenu()V
    .locals 1

    sget-boolean v0, Lcom/android/email/activity/setup/AccountSetupBasics;->ENTER_DEBUG_SCREEN:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mShowDebugMenu:Z

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->invalidateHeaders()V

    :cond_0
    return-void
.end method

.method private forceBack()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    invoke-virtual {p0}, Lcom/android/email/activity/setup/AccountSettings;->onBackPressed()V

    return-void
.end method

.method private forceSwitchHeader(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    invoke-virtual {p0, p1}, Landroid/app/ListActivity;->setSelection(I)V

    iget-object v1, p0, Lcom/android/email/activity/setup/AccountSettings;->mGeneratedHeaders:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->switchToHeader(Landroid/preference/PreferenceActivity$Header;)V

    return-void
.end method

.method private getAppPreferencesHeader()Landroid/preference/PreferenceActivity$Header;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    if-nez v0, :cond_0

    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    const v1, 0x7f08019e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    iput-object v2, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    const/4 v1, 0x0

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    const-class v1, Lcom/android/email/activity/setup/GeneralPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    iput-object v2, v0, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAppPreferencesHeader:Landroid/preference/PreferenceActivity$Header;

    return-object v0
.end method

.method private onAddNewAccount()V
    .locals 0

    invoke-static {p0}, Lcom/android/email/activity/setup/AccountSetupBasics;->actionNewAccount(Landroid/app/Activity;)V

    return-void
.end method

.method private shouldShowNewAccount()Z
    .locals 1

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->hasHeaders()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/android/email/activity/setup/GeneralPreferences;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/android/email/activity/setup/DebugFragment;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/android/email/activity/setup/AccountSettingsFragment;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateAccounts()V
    .locals 6

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->hasHeaders()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mLoadAccountListTask:Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->cancelTaskInterrupt(Landroid/os/AsyncTask;)V

    new-instance v0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;-><init>(Lcom/android/email/activity/setup/AccountSettings;Lcom/android/email/activity/setup/AccountSettings$1;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mDeletingAccountId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mLoadAccountListTask:Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

    :cond_0
    return-void
.end method


# virtual methods
.method public deleteAccount(Lcom/android/emailcommon/provider/Account;)V
    .locals 4
    .param p1    # Lcom/android/emailcommon/provider/Account;

    invoke-static {}, Lcom/android/email/activity/setup/AccountSettings$WaitingRemoveAccountDialog;->newInstance()Lcom/android/email/activity/setup/AccountSettings$WaitingRemoveAccountDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mProgressDialog:Landroid/app/DialogFragment;

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mProgressDialog:Landroid/app/DialogFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/DialogFragment;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mProgressDialog:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "WaitingRemoveDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v0

    iget-wide v1, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    new-instance v3, Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;

    invoke-direct {v3, p0}, Lcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;-><init>(Lcom/android/email/activity/setup/AccountSettings;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/email/Controller;->deleteAccount(JLcom/android/email/activity/setup/AccountSettings$RemoveAccountCallback;)V

    return-void
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1    # Landroid/app/Fragment;

    invoke-super {p0, p1}, Landroid/app/Activity;->onAttachFragment(Landroid/app/Fragment;)V

    instance-of v2, p1, Lcom/android/email/activity/setup/AccountSettingsFragment;

    if-eqz v2, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/android/email/activity/setup/AccountSettingsFragment;

    iget-object v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountSettingsFragmentCallback:Lcom/android/email/activity/setup/AccountSettings$AccountSettingsFragmentCallback;

    invoke-virtual {v1, v2}, Lcom/android/email/activity/setup/AccountSettingsFragment;->setCallback(Lcom/android/email/activity/setup/AccountSettingsFragment$Callback;)V

    :goto_0
    iput-object p1, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void

    :cond_1
    instance-of v2, p1, Lcom/android/email/activity/setup/AccountServerBaseFragment;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/email/activity/setup/AccountServerBaseFragment;

    iget-object v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountServerSettingsFragmentCallback:Lcom/android/email/activity/setup/AccountSettings$AccountServerSettingsFragmentCallback;

    invoke-virtual {v0, v2}, Lcom/android/email/activity/setup/AccountServerBaseFragment;->setCallback(Lcom/android/email/activity/setup/AccountServerBaseFragment$Callback;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    iget-object v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/setup/AccountServerBaseFragment;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    check-cast v2, Lcom/android/email/activity/setup/AccountServerBaseFragment;

    invoke-virtual {v2}, Lcom/android/email/activity/setup/AccountServerBaseFragment;->haveSettingsChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/email/activity/setup/AccountSettings$UnsavedChangesDialogFragment;->newInstanceForBack()Lcom/android/email/activity/setup/AccountSettings$UnsavedChangesDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "UnsavedChangesDialogFragment"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    const-wide/16 v9, -0x1

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountHeader:Landroid/preference/PreferenceActivity$Header;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->getAppPreferencesHeader()Landroid/preference/PreferenceActivity$Header;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountListHeaders:[Landroid/preference/PreferenceActivity$Header;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountListHeaders:[Landroid/preference/PreferenceActivity$Header;

    array-length v2, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    iget-object v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountListHeaders:[Landroid/preference/PreferenceActivity$Header;

    aget-object v1, v4, v3

    if-eqz v1, :cond_0

    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    cmp-long v4, v4, v9

    if-eqz v4, :cond_0

    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    iget-wide v6, p0, Lcom/android/email/activity/setup/AccountSettings;->mDeletingAccountId:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    iget-wide v6, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    iput-object v1, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountHeader:Landroid/preference/PreferenceActivity$Header;

    iput-wide v9, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountId:J

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mShowDebugMenu:Z

    if-eqz v4, :cond_2

    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    const v4, 0x7f08003a

    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    iput-object v8, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    iput v4, v0, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    const-class v4, Lcom/android/email/activity/setup/DebugFragment;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    iput-object v8, v0, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iput-object p1, p0, Lcom/android/email/activity/setup/AccountSettings;->mGeneratedHeaders:Ljava/util/List;

    return-void
.end method

.method public onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/preference/PreferenceActivity;->onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p2}, Lcom/android/email/activity/setup/AccountSettingsFragment;->getTitleFromArgs(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    if-nez p3, :cond_0

    if-eqz v1, :cond_0

    const-string v2, "AccountSettings.title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public onChangePwd(J)V
    .locals 12
    .param p1    # J

    :try_start_0
    invoke-static {p0}, Lcom/android/email/NotificationController;->getInstance(Landroid/content/Context;)Lcom/android/email/NotificationController;

    move-result-object v9

    invoke-virtual {v9, p1, p2}, Lcom/android/email/NotificationController;->cancelLoginFailedNotification(J)V

    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/android/email/mail/Store;->getInstance(Lcom/android/emailcommon/provider/Account;Landroid/content/Context;)Lcom/android/email/mail/Store;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11}, Lcom/android/email/mail/Store;->getSettingActivityClass()Ljava/lang/Class;

    move-result-object v10

    if-eqz v10, :cond_0

    const-class v0, Lcom/android/email/activity/setup/AccountSetupIncoming;

    invoke-virtual {v10, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    invoke-static {p0, v0, v7}, Lcom/android/email/activity/setup/AccountSetupIncoming;->actionIncomingSettings(Landroid/app/Activity;ILcom/android/emailcommon/provider/Account;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    const-string v0, "Email"

    const-string v1, "Error while trying to invoke store settings."

    invoke-static {v0, v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    :try_start_1
    const-class v0, Lcom/android/email/activity/setup/AccountSetupExchange;

    invoke-virtual {v10, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-static {v0, v7}, Lcom/android/email/activity/setup/SetupData;->init(ILcom/android/emailcommon/provider/Account;)V

    const-class v0, Lcom/android/email/activity/setup/AccountSetupExchangeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, v7, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/setup/AccountSetupExchangeFragment;->getSettingsModeArgs(Ljava/lang/Long;)Landroid/os/Bundle;

    move-result-object v2

    const v3, 0x7f08016b

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x4

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/email/activity/ActivityHelper;->debugSetWindowFlags(Landroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->getAccountIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountId:J

    const-string v4, "AccountSettings.for_account"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-wide v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountId:J

    invoke-static {v2, v4, v5}, Lcom/android/email/activity/setup/AccountSettings$LoginWarningDialog;->newInstance(Ljava/lang/String;J)Lcom/android/email/activity/setup/AccountSettings$LoginWarningDialog;

    move-result-object v0

    new-instance v4, Lcom/android/email/activity/setup/AccountSettings$1;

    invoke-direct {v4, p0}, Lcom/android/email/activity/setup/AccountSettings$1;-><init>(Lcom/android/email/activity/setup/AccountSettings;)V

    invoke-virtual {v0, v4}, Lcom/android/email/activity/setup/AccountSettings$LoginWarningDialog;->setCallback(Lcom/android/email/activity/setup/AccountSettings$LoginFailedCallback;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "loginwarning"

    invoke-virtual {v0, v4, v5}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    const-string v4, "AccountSettings.enable_debug"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mShowDebugMenu:Z

    const-string v4, "AccountSettings.title"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v4, Lcom/android/email/activity/setup/AccountSettings$2;

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/android/email/activity/setup/AccountSettings$2;-><init>(Lcom/android/email/activity/setup/AccountSettings;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountObserver:Landroid/database/ContentObserver;

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f080055

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setTitle(I)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-boolean v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mFinishedLoadAccount:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mLoadAccountListTask:Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->cancelTaskInterrupt(Landroid/os/AsyncTask;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mLoadAccountListTask:Lcom/android/email/activity/setup/AccountSettings$LoadAccountListTask;

    return-void
.end method

.method public onEditQuickResponses(Lcom/android/emailcommon/provider/Account;)V
    .locals 8
    .param p1    # Lcom/android/emailcommon/provider/Account;

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v0, "account"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-class v0, Lcom/android/email/activity/setup/AccountSettingsEditQuickResponsesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f080172

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v0, "Email"

    const-string v1, "Error while trying to invoke edit quick responses."

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onGetNewHeader()Landroid/preference/PreferenceActivity$Header;
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountHeader:Landroid/preference/PreferenceActivity$Header;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/email/activity/setup/AccountSettings;->mRequestedAccountHeader:Landroid/preference/PreferenceActivity$Header;

    return-object v0
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 4
    .param p1    # Landroid/preference/PreferenceActivity$Header;
    .param p2    # I

    iget-object v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v2, v2, Lcom/android/email/activity/setup/AccountServerBaseFragment;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mCurrentFragment:Landroid/app/Fragment;

    check-cast v2, Lcom/android/email/activity/setup/AccountServerBaseFragment;

    invoke-virtual {v2}, Lcom/android/email/activity/setup/AccountServerBaseFragment;->haveSettingsChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/android/email/activity/setup/AccountSettings$UnsavedChangesDialogFragment;->newInstanceForHeader(I)Lcom/android/email/activity/setup/AccountSettings$UnsavedChangesDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "UnsavedChangesDialogFragment"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_2

    iget v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mNumGeneralHeaderClicked:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mNumGeneralHeaderClicked:I

    iget v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mNumGeneralHeaderClicked:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->enableDebugMenu()V

    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mNumGeneralHeaderClicked:I

    goto :goto_1
.end method

.method public onIncomingSettings(Lcom/android/emailcommon/provider/Account;)V
    .locals 10
    .param p1    # Lcom/android/emailcommon/provider/Account;

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/email/mail/Store;->getInstance(Lcom/android/emailcommon/provider/Account;Landroid/content/Context;)Lcom/android/email/mail/Store;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/android/email/mail/Store;->getSettingActivityClass()Ljava/lang/Class;

    move-result-object v8

    if-eqz v8, :cond_0

    const/4 v0, 0x3

    invoke-static {v0, p1}, Lcom/android/email/activity/setup/SetupData;->init(ILcom/android/emailcommon/provider/Account;)V

    const-class v0, Lcom/android/email/activity/setup/AccountSetupIncoming;

    invoke-virtual {v8, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/android/email/activity/setup/AccountSetupIncomingFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/setup/AccountSetupIncomingFragment;->getSettingsModeArgs(Ljava/lang/Long;)Landroid/os/Bundle;

    move-result-object v2

    const v3, 0x7f08016b

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-class v0, Lcom/android/email/activity/setup/AccountSetupExchange;

    invoke-virtual {v8, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/android/email/activity/setup/AccountSetupExchangeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/setup/AccountSetupExchangeFragment;->getSettingsModeArgs(Ljava/lang/Long;)Landroid/os/Bundle;

    move-result-object v2

    const v3, 0x7f08016b

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v0, "Email"

    const-string v1, "Error while trying to invoke store settings."

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sget-object v1, Lcom/android/email/activity/setup/AccountSettings;->SECRET_KEY_CODES:[I

    iget v2, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    aget v1, v1, v2

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    iget v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    sget-object v1, Lcom/android/email/activity/setup/AccountSettings;->SECRET_KEY_CODES:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    iput v3, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->enableDebugMenu()V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :cond_1
    iput v3, p0, Lcom/android/email/activity/setup/AccountSettings;->mSecretKeyCodeIndex:I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/email/activity/setup/AccountSettings;->onBackPressed()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->onAddNewAccount()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f00f3 -> :sswitch_1
    .end sparse-switch
.end method

.method public onOutgoingSettings(Lcom/android/emailcommon/provider/Account;)V
    .locals 10
    .param p1    # Lcom/android/emailcommon/provider/Account;

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/email/mail/Sender;->getInstance(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)Lcom/android/email/mail/Sender;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/android/email/mail/Sender;->getSettingActivityClass()Ljava/lang/Class;

    move-result-object v9

    if-eqz v9, :cond_0

    const/4 v0, 0x3

    invoke-static {v0, p1}, Lcom/android/email/activity/setup/SetupData;->init(ILcom/android/emailcommon/provider/Account;)V

    const-class v0, Lcom/android/email/activity/setup/AccountSetupOutgoing;

    invoke-virtual {v9, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/android/email/activity/setup/AccountSetupOutgoingFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/activity/setup/AccountSetupOutgoingFragment;->getSettingsModeArgs(Ljava/lang/Long;)Landroid/os/Bundle;

    move-result-object v2

    const v3, 0x7f08016d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v0, "Email"

    const-string v1, "Error while trying to invoke sender settings."

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mIsBackGround:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->shouldShowNewAccount()Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mIsBackGround:Z

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mFinishedLoadAccount:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->invalidateHeaders()V

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Account;->NOTIFIER_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/AccountSettings;->updateAccounts()V

    return-void
.end method

.method public onSettingsChanged(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Lcom/android/emailcommon/provider/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;

    const-string v4, "account_description"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/setup/AccountSettings;->mAccountListHeaders:[Landroid/preference/PreferenceActivity$Header;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    iget-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->invalidateHeaders()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
