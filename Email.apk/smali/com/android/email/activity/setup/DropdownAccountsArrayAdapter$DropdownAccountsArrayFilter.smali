.class Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;
.super Landroid/widget/Filter;
.source "DropdownAccountsArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropdownAccountsArrayFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;


# direct methods
.method private constructor <init>(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$1;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;
    .param p2    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$1;

    invoke-direct {p0, p1}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;-><init>(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 17
    .param p1    # Ljava/lang/CharSequence;

    new-instance v7, Landroid/widget/Filter$FilterResults;

    invoke-direct {v7}, Landroid/widget/Filter$FilterResults;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v13}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$100(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v13}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$200(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/lang/Object;

    move-result-object v14

    monitor-enter v14

    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    new-instance v15, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$300(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/List;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v13, v15}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$102(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz p1, :cond_1

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v13

    if-nez v13, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v13}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$200(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/lang/Object;

    move-result-object v14

    monitor-enter v14

    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v13}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$100(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-direct {v4, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iput-object v4, v7, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v13

    iput v13, v7, Landroid/widget/Filter$FilterResults;->count:I

    :goto_0
    return-object v7

    :catchall_0
    move-exception v13

    :try_start_2
    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v13

    :catchall_1
    move-exception v13

    :try_start_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v13

    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v13}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$200(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/lang/Object;

    move-result-object v14

    monitor-enter v14

    :try_start_4
    new-instance v10, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v13}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$100(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_6

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catchall_2
    move-exception v13

    :try_start_5
    monitor-exit v14
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v13

    :cond_4
    const-string v13, " "

    invoke-virtual {v9, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    array-length v11, v12

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v11, :cond_3

    aget-object v13, v12, v3

    invoke-virtual {v13, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    iput-object v5, v7, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    iput v13, v7, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/widget/Filter$FilterResults;

    iget-object v4, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    iget-object v3, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-static {v4, v3}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$302(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Ljava/util/List;)Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v3}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$300(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    iget-object v4, v4, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mUserName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-static {v3, v2}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->access$302(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Ljava/util/List;)Ljava/util/List;

    iget v3, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;->this$0:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    goto :goto_1
.end method
