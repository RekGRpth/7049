.class public Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "DropdownAccountsArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$1;,
        Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field mFilter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;

.field mInflater:Landroid/view/LayoutInflater;

.field private final mLock:Ljava/lang/Object;

.field private mObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOriginalValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mResourceId:I

.field mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mLock:Ljava/lang/Object;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput p2, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mResourceId:I

    iput-object p3, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mOriginalValues:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mOriginalValues:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mFilter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;-><init>(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$1;)V

    iput-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mFilter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mFilter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter$DropdownAccountsArrayFilter;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public bridge synthetic getPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getPosition(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPosition(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_1

    iget-object v10, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v11, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mResourceId:I

    const/4 v12, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v10, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    :goto_0
    move-object v6, v8

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v3

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v7, v10

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getWidth()I

    move-result v10

    add-int/lit8 v9, v10, -0x64

    if-le v7, v9, :cond_0

    const-string v10, "@"

    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    aget-object v11, v5, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v1, v10

    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v11

    sub-int v12, v9, v1

    int-to-float v12, v12

    sget-object v13, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v10, v11, v12, v13}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    aget-object v11, v5, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v8

    :cond_1
    move-object/from16 v8, p2

    goto :goto_0
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mObjects:Ljava/util/List;

    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->mUserName:Ljava/lang/String;

    return-void
.end method
