.class public Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "EmailAccountAutoCompleteTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;
    }
.end annotation


# instance fields
.field private mIMEFullScreenMode:Z

.field private mIsPerformCompletion:Z

.field private mLastHostNameLength:I

.field mPopup:Landroid/widget/ListPopupWindow;

.field mTokenizer:Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIsPerformCompletion:Z

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIsPerformCompletion:Z

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    new-instance v0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    invoke-direct {v0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mTokenizer:Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    iget-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mTokenizer:Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    invoke-virtual {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIsPerformCompletion:Z

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    return-void
.end method

.method private getEditHostNameLength()I
    .locals 4

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public onFilterComplete(I)V
    .locals 2
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onFilterComplete(I)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->getEditHostNameLength()I

    move-result v0

    iget v1, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->replacePartTextIfNeed()V

    :cond_0
    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/os/Parcelable;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->getEditHostNameLength()I

    move-result v0

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    return-void
.end method

.method public replacePartTextIfNeed()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/widget/AutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getCount()I

    move-result v1

    if-ne v1, v6, :cond_0

    iget-boolean v4, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    if-nez v4, :cond_0

    invoke-virtual {p0, v6}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->setPerfomCompletion(Z)V

    invoke-virtual {p0, v2}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v5}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->setPerfomCompletion(Z)V

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/widget/TextView;->clearComposingText()V

    iget-boolean v4, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIsPerformCompletion:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;Z)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-static {v0, v1, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-static {v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->getEditHostNameLength()I

    move-result v2

    iput v2, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastHostNameLength:I

    goto :goto_0
.end method

.method public setPerfomCompletion(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIsPerformCompletion:Z

    return-void
.end method
