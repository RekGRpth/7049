.class public Lcom/android/email/service/EmailExternalOmacpService;
.super Landroid/app/Service;
.source "EmailExternalOmacpService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/service/EmailExternalOmacpService$EmailCapability;
    }
.end annotation


# static fields
.field private static final ACCOUNT_WHERE_HOSTAUTH:Ljava/lang/String; = "hostAuthKeyRecv=?"

.field private static final APPID_KEY:Ljava/lang/String; = "25"

.field private static final APPID_VALUE:Ljava/lang/String; = "25"

.field private static final CONNECT_FAIL:I = -0x1

.field private static final CONNECT_SUCCESS:I = 0x1

.field private static final HOSTAUTH_WHERE_CREDENTIALS:Ljava/lang/String; = "address like ? and login like ? and protocol not like \"smtp\""

.field private static final IMAP_APPID:Ljava/lang/String;

.field private static final IMAP_DEFAULT_PORT_NUM:Ljava/lang/String;

.field private static final IMAP_DEFAULT_SERVICE:Ljava/lang/String;

.field private static final IMAP_SERVER_TYPE:I = 0x3

.field private static final IMAP_SSL_SERVICE:Ljava/lang/String;

.field private static final POP_APPID:Ljava/lang/String;

.field private static final POP_DEFAULT_PORT_NUM:Ljava/lang/String;

.field private static final POP_DEFAULT_SERVICE:Ljava/lang/String;

.field private static final POP_SERVER_TYPE:I = 0x2

.field private static final POP_SSL_SERVICE:Ljava/lang/String;

.field private static final SMTP_APPID:Ljava/lang/String;

.field private static final SMTP_DEAULT_PORT_NUM:Ljava/lang/String;

.field private static final SMTP_DEFAULT_SERVICE:Ljava/lang/String;

.field private static final SMTP_SERVER_TYPE:I = 0x1

.field private static final SMTP_SSL_SERVICE:Ljava/lang/String;

.field private static final STR_SSL:Ljava/lang/String; = "ssl"

.field private static final STR_TLS:Ljava/lang/String; = "tls"

.field private static final SYNC_INTERVAL:I = 0xf

.field private static final TAG:Ljava/lang/String; = "OmacpAddAccountService"


# instance fields
.field private mEmailValidator:Lcom/android/email/EmailAddressValidator;

.field private mFrom:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mProviderId:Ljava/lang/String;

.field private mRtAddr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x8f

    const/16 v2, 0x6e

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_APPID:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_DEAULT_PORT_NUM:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_DEFAULT_SERVICE:Ljava/lang/String;

    const/16 v0, 0x1d1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_SSL_SERVICE:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->POP_DEFAULT_PORT_NUM:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_DEFAULT_PORT_NUM:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->POP_DEFAULT_SERVICE:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_DEFAULT_SERVICE:Ljava/lang/String;

    const/16 v0, 0x3e3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->POP_SSL_SERVICE:Ljava/lang/String;

    const/16 v0, 0x3e1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_SSL_SERVICE:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->POP_APPID:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_APPID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/android/email/EmailAddressValidator;

    invoke-direct {v0}, Lcom/android/email/EmailAddressValidator;-><init>()V

    iput-object v0, p0, Lcom/android/email/service/EmailExternalOmacpService;->mEmailValidator:Lcom/android/email/EmailAddressValidator;

    new-instance v0, Lcom/android/email/service/EmailExternalOmacpService$1;

    invoke-direct {v0, p0}, Lcom/android/email/service/EmailExternalOmacpService$1;-><init>(Lcom/android/email/service/EmailExternalOmacpService;)V

    iput-object v0, p0, Lcom/android/email/service/EmailExternalOmacpService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/service/EmailExternalOmacpService;Lcom/android/emailcommon/provider/Account;)V
    .locals 0
    .param p0    # Lcom/android/email/service/EmailExternalOmacpService;
    .param p1    # Lcom/android/emailcommon/provider/Account;

    invoke-direct {p0, p1}, Lcom/android/email/service/EmailExternalOmacpService;->addAccount(Lcom/android/emailcommon/provider/Account;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/service/EmailExternalOmacpService;Z)V
    .locals 0
    .param p0    # Lcom/android/email/service/EmailExternalOmacpService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/email/service/EmailExternalOmacpService;->sendResultToOmacp(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/email/service/EmailExternalOmacpService;ILcom/android/emailcommon/provider/Account;)V
    .locals 0
    .param p0    # Lcom/android/email/service/EmailExternalOmacpService;
    .param p1    # I
    .param p2    # Lcom/android/emailcommon/provider/Account;

    invoke-direct {p0, p1, p2}, Lcom/android/email/service/EmailExternalOmacpService;->sendMessage(ILcom/android/emailcommon/provider/Account;)V

    return-void
.end method

.method private addAccount(Lcom/android/emailcommon/provider/Account;)V
    .locals 8
    .param p1    # Lcom/android/emailcommon/provider/Account;

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "OmacpAddAccountService"

    const-string v1, "add Account is beginning"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/android/emailcommon/provider/Account;->getFlags()I

    move-result v0

    and-int/lit8 v6, v0, -0x2

    or-int/lit8 v6, v6, 0x1

    invoke-virtual {p1, v6}, Lcom/android/emailcommon/provider/Account;->setFlags(I)V

    iget-object v0, p0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/emailcommon/provider/Account;->setEmailAddress(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/emailcommon/provider/Account;->setSenderName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/service/EmailExternalOmacpService;->mProviderId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/emailcommon/provider/Account;->setDisplayName(Ljava/lang/String;)V

    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lcom/android/emailcommon/provider/Account;->setSyncInterval(I)V

    invoke-virtual {p1, v2}, Lcom/android/emailcommon/provider/Account;->setDefaultAccount(Z)V

    iget v0, p1, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p1, Lcom/android/emailcommon/provider/Account;->mFlags:I

    invoke-virtual {p1}, Lcom/android/emailcommon/provider/EmailContent;->isSaved()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OmacpAddAccountService"

    const-string v1, "update Account send & receive information"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lcom/android/emailcommon/provider/Account;->mHostAuthSend:Lcom/android/emailcommon/provider/HostAuth;

    iget-object v1, p1, Lcom/android/emailcommon/provider/Account;->mHostAuthSend:Lcom/android/emailcommon/provider/HostAuth;

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/HostAuth;->toContentValues()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/emailcommon/provider/EmailContent;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    iget-object v0, p1, Lcom/android/emailcommon/provider/Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/HostAuth;

    iget-object v1, p1, Lcom/android/emailcommon/provider/Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/HostAuth;

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/HostAuth;->toContentValues()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/emailcommon/provider/EmailContent;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    :cond_0
    iget-object v0, p1, Lcom/android/emailcommon/provider/Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/HostAuth;

    if-nez v0, :cond_2

    move-object v7, v5

    :goto_0
    if-eqz v7, :cond_1

    invoke-static {p1, v7}, Lcom/android/email/service/EmailExternalOmacpService;->setFlagsForProtocol(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;)V

    :cond_1
    const-string v0, "OmacpAddAccountService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "add Account with flag : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p1, Lcom/android/emailcommon/provider/Account;->mFlags:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Lcom/android/email/activity/setup/AccountSettingsUtils;->commitSettings(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)V

    const-string v0, "OmacpAddAccountService"

    const-string v1, "AccountSettingsUtils.commitSettings save email Account "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-static/range {v0 .. v5}, Lcom/android/email/service/MailService;->setupAccountManagerAccount(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;ZZZLandroid/accounts/AccountManagerCallback;)V

    const-string v0, "OmacpAddAccountService"

    const-string v1, "MailService.setupAccountManagerAccount save system Account "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/android/email/Email;->setServicesEnabledSync(Landroid/content/Context;)Z

    return-void

    :cond_2
    iget-object v0, p1, Lcom/android/emailcommon/provider/Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/HostAuth;

    iget-object v7, v0, Lcom/android/emailcommon/provider/HostAuth;->mProtocol:Ljava/lang/String;

    goto :goto_0
.end method

.method static buildCapabilityResultToOmacp(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.omacp.capability.result"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "appId"

    const-string v2, "25"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "email"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_provider_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_outbound_addr"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_outbound_port_number"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_outbound_secure"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_outbound_auth_type"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_outbound_user_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_outbound_password"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_from"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_rt_addr"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_inbound_addr"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_inbound_port_number"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_inbound_secure"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_inbound_user_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "email_inbound_password"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "EmailExternalReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return OMACP capability result intent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private checkEmailServerConnect(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/emailcommon/provider/Account;

    new-instance v0, Lcom/android/email/service/EmailExternalOmacpService$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/android/email/service/EmailExternalOmacpService$2;-><init>(Lcom/android/email/service/EmailExternalOmacpService;Lcom/android/emailcommon/provider/Account;Landroid/content/Context;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private getScheme(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne v1, p2, :cond_5

    sget-object v1, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_DEFAULT_SERVICE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "smtp"

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "STARTTLS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "tls"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string v0, "smtp+tls+"

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_SSL_SERVICE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "ssl"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    const-string v0, "smtp+ssl+"

    goto :goto_0

    :cond_5
    const/4 v1, 0x2

    if-ne v1, p2, :cond_a

    sget-object v1, Lcom/android/email/service/EmailExternalOmacpService;->POP_DEFAULT_SERVICE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "pop3"

    goto :goto_0

    :cond_6
    const-string v1, "STARTTLS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "tls"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    const-string v0, "pop3+tls+"

    goto :goto_0

    :cond_8
    sget-object v1, Lcom/android/email/service/EmailExternalOmacpService;->POP_SSL_SERVICE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "ssl"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_9
    const-string v0, "pop3+ssl+"

    goto :goto_0

    :cond_a
    const/4 v1, 0x3

    if-ne v1, p2, :cond_0

    sget-object v1, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_DEFAULT_SERVICE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v0, "imap"

    goto :goto_0

    :cond_b
    const-string v1, "STARTTLS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "tls"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_c
    const-string v0, "imap+tls+"

    goto :goto_0

    :cond_d
    sget-object v1, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_SSL_SERVICE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "ssl"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_e
    const-string v0, "imap+ssl+"

    goto/16 :goto_0
.end method

.method private isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInvalidEmailAddress(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/email/service/EmailExternalOmacpService;->mEmailValidator:Lcom/android/email/EmailAddressValidator;

    invoke-virtual {v0, p1}, Lcom/android/email/EmailAddressValidator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parserOmacpRequest(Landroid/content/Intent;)Lcom/android/emailcommon/provider/Account;
    .locals 41
    .param p1    # Landroid/content/Intent;

    const-string v6, "OmacpAddAccountService"

    const-string v7, "parser Omacp Request is begin"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "email_setting_intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v32

    if-eqz v32, :cond_0

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    const-string v6, "OmacpAddAccountService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "OMACP email_setting_intent is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v21, 0x0

    :cond_1
    :goto_0
    return-object v21

    :cond_2
    new-instance v21, Lcom/android/emailcommon/provider/Account;

    invoke-direct/range {v21 .. v21}, Lcom/android/emailcommon/provider/Account;-><init>()V

    const/4 v5, 0x0

    const/16 v33, 0x0

    const/16 v37, 0x0

    const/4 v3, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/4 v12, 0x0

    const/4 v2, 0x0

    const/16 v25, 0x0

    const/16 v27, 0x0

    :try_start_0
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v31

    move-object/from16 v38, v2

    move-object/from16 v40, v12

    :goto_1
    :try_start_1
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_23

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/content/Intent;

    const-string v6, "APPID"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    const-string v6, "APPADDR"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v23

    if-eqz v23, :cond_4

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    const/16 v30, 0x0

    :goto_2
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v6

    move/from16 v0, v30

    if-ge v0, v6, :cond_5

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Ljava/util/Map;

    if-eqz v6, :cond_3

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Map;

    const-string v6, "ADDR"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    const-string v6, "PORTNBR"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object/from16 v33, v0

    const-string v6, "SERVICE"

    move-object/from16 v0, v22

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object/from16 v37, v0

    :cond_3
    add-int/lit8 v30, v30, 0x1

    goto :goto_2

    :cond_4
    const-string v6, "ADDR"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "OmacpAddAccountService"

    const-string v7, "addr is empty"

    invoke-static {v6, v7}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v6, "APPAUTH"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v24

    const/4 v11, 0x0

    const/16 v20, 0x0

    if-eqz v24, :cond_8

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    const/16 v30, 0x0

    :goto_3
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v6

    move/from16 v0, v30

    if-ge v0, v6, :cond_8

    move-object/from16 v0, v24

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Ljava/util/Map;

    if-eqz v6, :cond_7

    move-object/from16 v0, v24

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/util/Map;

    const-string v6, "AAUTHTYPE"

    move-object/from16 v0, v26

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v6, "AAUTHNAME"

    move-object/from16 v0, v26

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string v6, "AAUTHSECRET"

    move-object/from16 v0, v26

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    :cond_7
    add-int/lit8 v30, v30, 0x1

    goto :goto_3

    :cond_8
    sget-object v6, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_APPID:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    const-string v6, "PROVIDER-ID"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mProviderId:Ljava/lang/String;

    const-string v6, "FROM"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    const-string v6, "RT-ADDR"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    const-string v6, "OmacpAddAccountService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[OMACP] smtp param: PROVIDER-ID="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/email/service/EmailExternalOmacpService;->mProviderId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";ADDR="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";FROM="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";RT-ADDR="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";PORT="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";SERVICE="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHTYPE="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v27

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHNAME="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHSECRET="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/email/service/EmailExternalOmacpService;->isInvalidEmailAddress(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const-string v7, ">"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mFrom:Ljava/lang/String;

    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    if-eqz v6, :cond_c

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/email/service/EmailExternalOmacpService;->isInvalidEmailAddress(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_e

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\""

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;

    :cond_c
    :goto_4
    const/4 v4, 0x0

    const-string v6, "LOGIN"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_d

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    :cond_d
    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_e
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/service/EmailExternalOmacpService;->mRtAddr:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v28

    move-object/from16 v2, v38

    move-object/from16 v12, v40

    :goto_5
    const-string v6, "OmacpAddAccountService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v28

    invoke-static {v6, v7, v0}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_f
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_10
    if-nez v33, :cond_11

    sget-object v33, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_DEAULT_PORT_NUM:Ljava/lang/String;

    :cond_11
    if-nez v37, :cond_12

    sget-object v37, Lcom/android/email/service/EmailExternalOmacpService;->SMTP_DEFAULT_SERVICE:Ljava/lang/String;

    :cond_12
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v6}, Lcom/android/email/service/EmailExternalOmacpService;->getScheme(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_13

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_13
    new-instance v2, Ljava/net/URI;

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/Account;->getOrCreateHostAuthSend(Landroid/content/Context;)Lcom/android/emailcommon/provider/HostAuth;

    move-result-object v35

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-static {v0, v6}, Lcom/android/emailcommon/provider/HostAuth;->setHostAuthFromString(Lcom/android/emailcommon/provider/HostAuth;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v12, v40

    :goto_6
    move-object/from16 v38, v2

    move-object/from16 v40, v12

    goto/16 :goto_1

    :cond_14
    :try_start_4
    sget-object v6, Lcom/android/email/service/EmailExternalOmacpService;->POP_APPID:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    const-string v6, "OmacpAddAccountService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[OMACP] pop param: ADDR="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";PORT="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";SERVICE="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHNAME="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHSECRET="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_15

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_16

    :cond_15
    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_16
    move-object/from16 v0, v21

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v6, p0

    move-object/from16 v7, p0

    move-object v10, v5

    invoke-virtual/range {v6 .. v11}, Lcom/android/email/service/EmailExternalOmacpService;->findDuplicateAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/Account;

    move-result-object v39

    if-eqz v39, :cond_17

    move-object/from16 v0, v39

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, v21

    iput-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v21

    :cond_17
    if-nez v33, :cond_18

    sget-object v33, Lcom/android/email/service/EmailExternalOmacpService;->POP_DEFAULT_PORT_NUM:Ljava/lang/String;

    :cond_18
    if-nez v37, :cond_19

    sget-object v37, Lcom/android/email/service/EmailExternalOmacpService;->POP_DEFAULT_SERVICE:Ljava/lang/String;

    :cond_19
    const/4 v6, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v6}, Lcom/android/email/service/EmailExternalOmacpService;->getScheme(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1a

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_1a
    new-instance v12, Ljava/net/URI;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v13, v3

    move-object v15, v5

    invoke-direct/range {v12 .. v19}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/Account;->getOrCreateHostAuthRecv(Landroid/content/Context;)Lcom/android/emailcommon/provider/HostAuth;

    move-result-object v34

    invoke-virtual {v12}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/android/emailcommon/provider/HostAuth;->setHostAuthFromString(Lcom/android/emailcommon/provider/HostAuth;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    move-object/from16 v2, v38

    goto/16 :goto_6

    :cond_1b
    :try_start_6
    sget-object v6, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_APPID:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_22

    const-string v6, "OmacpAddAccountService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[OMACP] imap param: ADDR="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";PORT="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";SERVICE="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHNAME="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";AAUTHSECRET="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1c

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/email/service/EmailExternalOmacpService;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1d

    :cond_1c
    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_1d
    move-object/from16 v0, v21

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v6, p0

    move-object/from16 v7, p0

    move-object v10, v5

    invoke-virtual/range {v6 .. v11}, Lcom/android/email/service/EmailExternalOmacpService;->findDuplicateAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/Account;

    move-result-object v39

    if-eqz v39, :cond_1e

    move-object/from16 v0, v39

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, v21

    iput-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v21

    :cond_1e
    if-nez v33, :cond_1f

    sget-object v33, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_DEFAULT_PORT_NUM:Ljava/lang/String;

    :cond_1f
    if-nez v37, :cond_20

    sget-object v37, Lcom/android/email/service/EmailExternalOmacpService;->IMAP_DEFAULT_SERVICE:Ljava/lang/String;

    :cond_20
    const/4 v6, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v6}, Lcom/android/email/service/EmailExternalOmacpService;->getScheme(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_21

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_21
    new-instance v12, Ljava/net/URI;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v13, v3

    move-object v15, v5

    invoke-direct/range {v12 .. v19}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    const/4 v6, 0x2

    :try_start_7
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/provider/Account;->setDeletePolicy(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/Account;->getOrCreateHostAuthRecv(Landroid/content/Context;)Lcom/android/emailcommon/provider/HostAuth;

    move-result-object v34

    invoke-virtual {v12}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/android/emailcommon/provider/HostAuth;->setHostAuthFromString(Lcom/android/emailcommon/provider/HostAuth;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    move-object/from16 v2, v38

    goto/16 :goto_6

    :cond_22
    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_23
    :try_start_8
    invoke-virtual/range {v21 .. v21}, Lcom/android/emailcommon/provider/EmailContent;->isSaved()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/Account;->getOrCreateHostAuthSend(Landroid/content/Context;)Lcom/android/emailcommon/provider/HostAuth;

    move-result-object v36

    invoke-virtual/range {v38 .. v38}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v36

    invoke-static {v0, v6}, Lcom/android/emailcommon/provider/HostAuth;->setHostAuthFromString(Lcom/android/emailcommon/provider/HostAuth;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v28

    const-string v6, "OmacpAddAccountService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v28

    invoke-static {v6, v7, v0}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :catch_2
    move-exception v28

    goto/16 :goto_5

    :catch_3
    move-exception v28

    move-object/from16 v12, v40

    goto/16 :goto_5

    :catch_4
    move-exception v28

    move-object/from16 v2, v38

    goto/16 :goto_5
.end method

.method private sendMessage(ILcom/android/emailcommon/provider/Account;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/emailcommon/provider/Account;

    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/android/email/service/EmailExternalOmacpService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/android/email/service/EmailExternalOmacpService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/email/service/EmailExternalOmacpService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private sendResultToOmacp(Z)V
    .locals 3
    .param p1    # Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mediatek.omacp.settings.result"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "appId"

    const-string v2, "25"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static setFlagsForProtocol(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;)V
    .locals 1
    .param p0    # Lcom/android/emailcommon/provider/Account;
    .param p1    # Ljava/lang/String;

    const-string v0, "imap"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/emailcommon/provider/Account;->setDeletePolicy(I)V

    iget v0, p0, Lcom/android/emailcommon/provider/Account;->mFlags:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/android/emailcommon/provider/Account;->mFlags:I

    :cond_0
    return-void
.end method


# virtual methods
.method public findDuplicateAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/Account;
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/HostAuth;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/HostAuth;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "address like ? and login like ? and protocol not like \"smtp\""

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    const/4 v5, 0x1

    aput-object p5, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    sget-object v1, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/Account;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "hostAuthKeyRecv=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    :cond_0
    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    cmp-long v1, v7, p2

    if-eqz v1, :cond_0

    invoke-static {p1, v7, v8}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1

    :catchall_1
    move-exception v1

    :try_start_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-object v6
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    const-string v2, "android.intent.extra.INTENT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/android/email/service/EmailExternalOmacpService;->parserOmacpRequest(Landroid/content/Intent;)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    const-string v2, "OmacpAddAccountService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    invoke-direct {p0, p0, v0}, Lcom/android/email/service/EmailExternalOmacpService;->checkEmailServerConnect(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)V

    :cond_0
    :goto_0
    const/4 v2, 0x2

    return v2

    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/email/service/EmailExternalOmacpService;->sendResultToOmacp(Z)V

    goto :goto_0
.end method
