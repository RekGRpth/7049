.class Lcom/android/email/service/EmailExternalService$ControllerCallbacks;
.super Lcom/android/emailcommon/service/EmailExternalCalls$Stub;
.source "EmailExternalService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/service/EmailExternalService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ControllerCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/service/EmailExternalService;


# direct methods
.method private constructor <init>(Lcom/android/email/service/EmailExternalService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/service/EmailExternalService$ControllerCallbacks;->this$0:Lcom/android/email/service/EmailExternalService;

    invoke-direct {p0}, Lcom/android/emailcommon/service/EmailExternalCalls$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/email/service/EmailExternalService;Lcom/android/email/service/EmailExternalService$1;)V
    .locals 0
    .param p1    # Lcom/android/email/service/EmailExternalService;
    .param p2    # Lcom/android/email/service/EmailExternalService$1;

    invoke-direct {p0, p1}, Lcom/android/email/service/EmailExternalService$ControllerCallbacks;-><init>(Lcom/android/email/service/EmailExternalService;)V

    return-void
.end method


# virtual methods
.method public sendCallback(IJI)V
    .locals 1
    .param p1    # I
    .param p2    # J
    .param p4    # I

    iget-object v0, p0, Lcom/android/email/service/EmailExternalService$ControllerCallbacks;->this$0:Lcom/android/email/service/EmailExternalService;

    invoke-static {v0}, Lcom/android/email/service/EmailExternalService;->access$400(Lcom/android/email/service/EmailExternalService;)Lcom/android/email/service/EmailExternalService$ExternalHandler;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lcom/android/email/service/EmailExternalService$ExternalHandler;->access$500(Lcom/android/email/service/EmailExternalService$ExternalHandler;IJI)V

    return-void
.end method

.method public updateCallback(IJJ)V
    .locals 6
    .param p1    # I
    .param p2    # J
    .param p4    # J

    iget-object v0, p0, Lcom/android/email/service/EmailExternalService$ControllerCallbacks;->this$0:Lcom/android/email/service/EmailExternalService;

    invoke-static {v0}, Lcom/android/email/service/EmailExternalService;->access$400(Lcom/android/email/service/EmailExternalService;)Lcom/android/email/service/EmailExternalService$ExternalHandler;

    move-result-object v0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/email/service/EmailExternalService$ExternalHandler;->access$600(Lcom/android/email/service/EmailExternalService$ExternalHandler;IJJ)V

    return-void
.end method
