.class public Lcom/android/email/view/CertificateSelector;
.super Landroid/widget/RelativeLayout;
.source "CertificateSelector.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/view/CertificateSelector$1;,
        Lcom/android/email/view/CertificateSelector$SavedState;,
        Lcom/android/email/view/CertificateSelector$HostCallback;
    }
.end annotation


# instance fields
.field private mAliasText:Landroid/widget/TextView;

.field private mHost:Lcom/android/email/view/CertificateSelector$HostCallback;

.field private mSelectButton:Landroid/widget/Button;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public getCertificate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/view/CertificateSelector;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public hasCertificate()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/view/CertificateSelector;->mValue:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/email/view/CertificateSelector;->mSelectButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/email/view/CertificateSelector;->mHost:Lcom/android/email/view/CertificateSelector$HostCallback;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/view/CertificateSelector;->hasCertificate()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/email/view/CertificateSelector;->setCertificate(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/email/view/CertificateSelector;->mHost:Lcom/android/email/view/CertificateSelector$HostCallback;

    invoke-interface {v0}, Lcom/android/email/view/CertificateSelector$HostCallback;->onCertificateRequested()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0f005a

    invoke-static {p0, v0}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/email/view/CertificateSelector;->mAliasText:Landroid/widget/TextView;

    const v0, 0x7f0f0058

    invoke-static {p0, v0}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/email/view/CertificateSelector;->mSelectButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/email/view/CertificateSelector;->mSelectButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/email/view/CertificateSelector;->setCertificate(Ljava/lang/String;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/android/email/view/CertificateSelector$SavedState;

    invoke-virtual {v0}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, v0, Lcom/android/email/view/CertificateSelector$SavedState;->mValue:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/email/view/CertificateSelector;->setCertificate(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Lcom/android/email/view/CertificateSelector$SavedState;

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/email/view/CertificateSelector;->getCertificate()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/email/view/CertificateSelector$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;)V

    return-object v0
.end method

.method public setCertificate(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object p1, p0, Lcom/android/email/view/CertificateSelector;->mValue:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/email/view/CertificateSelector;->mAliasText:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const v1, 0x7f080121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/email/view/CertificateSelector;->mSelectButton:Landroid/widget/Button;

    if-nez p1, :cond_1

    const v1, 0x7f08011e

    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    const v1, 0x7f080120

    goto :goto_1
.end method

.method public setDelegate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setHostActivity(Lcom/android/email/view/CertificateSelector$HostCallback;)V
    .locals 0
    .param p1    # Lcom/android/email/view/CertificateSelector$HostCallback;

    iput-object p1, p0, Lcom/android/email/view/CertificateSelector;->mHost:Lcom/android/email/view/CertificateSelector$HostCallback;

    return-void
.end method
