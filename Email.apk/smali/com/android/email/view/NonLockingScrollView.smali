.class public Lcom/android/email/view/NonLockingScrollView;
.super Landroid/widget/ScrollView;
.source "NonLockingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;
    }
.end annotation


# static fields
.field private static final INTERCEPT_MAX_VELOCITY:I = 0x7d0

.field private static final OVERSCROLLED_MSG:I = 0xc8

.field private static final TIME_ELAPSED:J = 0x1f4L

.field private static final sHitFrame:Landroid/graphics/Rect;


# instance fields
.field private mBottomHeight:I

.field private final mChildrenNeedingAllTouches:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mContentLen:I

.field private mElapsedOverScrolled:J

.field private mHandler:Landroid/os/Handler;

.field private mInCustomDrag:Z

.field private mInterceptXMotion:Z

.field private mIsScrollFreeze:Z

.field private mMaximumVelocity:I

.field private mMsg:Landroid/os/Message;

.field private mOnOverScrollListener:Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;

.field private mToggleLoading:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mViewHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/email/view/NonLockingScrollView;->sHitFrame:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput v2, p0, Lcom/android/email/view/NonLockingScrollView;->mContentLen:I

    iput v2, p0, Lcom/android/email/view/NonLockingScrollView;->mViewHeight:I

    iput v2, p0, Lcom/android/email/view/NonLockingScrollView;->mBottomHeight:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/email/view/NonLockingScrollView;->mElapsedOverScrolled:J

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mToggleLoading:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mMsg:Landroid/os/Message;

    new-instance v0, Lcom/android/email/view/NonLockingScrollView$1;

    invoke-direct {v0, p0}, Lcom/android/email/view/NonLockingScrollView$1;-><init>(Lcom/android/email/view/NonLockingScrollView;)V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mHandler:Landroid/os/Handler;

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInterceptXMotion:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v3, p0, Lcom/android/email/view/NonLockingScrollView;->mContentLen:I

    iput v3, p0, Lcom/android/email/view/NonLockingScrollView;->mViewHeight:I

    iput v3, p0, Lcom/android/email/view/NonLockingScrollView;->mBottomHeight:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/email/view/NonLockingScrollView;->mElapsedOverScrolled:J

    iput-boolean v3, p0, Lcom/android/email/view/NonLockingScrollView;->mToggleLoading:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/email/view/NonLockingScrollView;->mMsg:Landroid/os/Message;

    new-instance v1, Lcom/android/email/view/NonLockingScrollView$1;

    invoke-direct {v1, p0}, Lcom/android/email/view/NonLockingScrollView$1;-><init>(Lcom/android/email/view/NonLockingScrollView;)V

    iput-object v1, p0, Lcom/android/email/view/NonLockingScrollView;->mHandler:Landroid/os/Handler;

    iput-boolean v3, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    iput-boolean v3, p0, Lcom/android/email/view/NonLockingScrollView;->mInterceptXMotion:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/android/email/view/NonLockingScrollView;->mMaximumVelocity:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v2, p0, Lcom/android/email/view/NonLockingScrollView;->mContentLen:I

    iput v2, p0, Lcom/android/email/view/NonLockingScrollView;->mViewHeight:I

    iput v2, p0, Lcom/android/email/view/NonLockingScrollView;->mBottomHeight:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/email/view/NonLockingScrollView;->mElapsedOverScrolled:J

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mToggleLoading:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mMsg:Landroid/os/Message;

    new-instance v0, Lcom/android/email/view/NonLockingScrollView$1;

    invoke-direct {v0, p0}, Lcom/android/email/view/NonLockingScrollView$1;-><init>(Lcom/android/email/view/NonLockingScrollView;)V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mHandler:Landroid/os/Handler;

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInterceptXMotion:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/view/NonLockingScrollView;)J
    .locals 2
    .param p0    # Lcom/android/email/view/NonLockingScrollView;

    iget-wide v0, p0, Lcom/android/email/view/NonLockingScrollView;->mElapsedOverScrolled:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/android/email/view/NonLockingScrollView;J)J
    .locals 0
    .param p0    # Lcom/android/email/view/NonLockingScrollView;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/view/NonLockingScrollView;->mElapsedOverScrolled:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/android/email/view/NonLockingScrollView;)Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;
    .locals 1
    .param p0    # Lcom/android/email/view/NonLockingScrollView;

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mOnOverScrollListener:Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/email/view/NonLockingScrollView;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/view/NonLockingScrollView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/view/NonLockingScrollView;->mToggleLoading:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/email/view/NonLockingScrollView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/email/view/NonLockingScrollView;

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private canViewReceivePointerEvents(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private excludeChildrenFromInterceptions(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    instance-of v4, p1, Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/email/view/NonLockingScrollView;->excludeChildrenFromInterceptions(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private initVelocityTrackerIfNotExists()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method private isEventOverChild(Landroid/view/MotionEvent;Ljava/util/ArrayList;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MotionEvent;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v6

    int-to-float v6, v6

    add-float v3, v5, v6

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v6

    int-to-float v6, v6

    add-float v4, v5, v6

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/android/email/view/NonLockingScrollView;->canViewReceivePointerEvents(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/email/view/NonLockingScrollView;->sHitFrame:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    sget-object v5, Lcom/android/email/view/NonLockingScrollView;->sHitFrame:Landroid/graphics/Rect;

    float-to-int v6, v3

    float-to-int v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private recycleVelocityTracker()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method


# virtual methods
.method public getToggleLoading()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mToggleLoading:Z

    return v0
.end method

.method public isScrollFreeze()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mIsScrollFreeze:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    invoke-direct {p0, p0}, Lcom/android/email/view/NonLockingScrollView;->excludeChildrenFromInterceptions(Landroid/view/View;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v2}, Lcom/android/email/view/NonLockingScrollView;->isEventOverChild(Landroid/view/MotionEvent;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_1
    return v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/email/view/NonLockingScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInterceptXMotion:Z

    goto :goto_1
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # Z

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onOverScrolled(IIZZ)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/email/view/NonLockingScrollView;->mContentLen:I

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/email/view/NonLockingScrollView;->mViewHeight:I

    if-eqz p4, :cond_1

    if-gtz p2, :cond_0

    iget v0, p0, Lcom/android/email/view/NonLockingScrollView;->mContentLen:I

    iget v1, p0, Lcom/android/email/view/NonLockingScrollView;->mViewHeight:I

    if-gt v0, v1, :cond_1

    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mMsg:Landroid/os/Message;

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mMsg:Landroid/os/Message;

    const/16 v1, 0xc8

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/email/view/NonLockingScrollView;->mMsg:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1    # Landroid/view/MotionEvent;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/android/email/view/NonLockingScrollView;->mIsScrollFreeze:Z

    invoke-direct/range {p0 .. p0}, Lcom/android/email/view/NonLockingScrollView;->initVelocityTrackerIfNotExists()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v4, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getScrollY()I

    move-result v11

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    new-instance v10, Landroid/graphics/Rect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct {v10, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v6, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v12, v8, Landroid/webkit/WebView;

    if-eqz v12, :cond_0

    move-object v6, v8

    check-cast v6, Landroid/webkit/WebView;

    invoke-virtual {v6, v10}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v9

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz v6, :cond_2

    if-le v11, v9, :cond_4

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v12

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v13

    invoke-virtual {v6, v12, v13, v4, v9}, Landroid/webkit/WebView;->setVisibleParameterForEmail(IIZI)V

    :cond_2
    and-int/lit16 v12, v1, 0xff

    packed-switch v12, :pswitch_data_0

    :cond_3
    :goto_2
    invoke-super/range {p0 .. p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v12

    return v12

    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v13, 0x3e8

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/email/view/NonLockingScrollView;->mMaximumVelocity:I

    int-to-float v14, v14

    invoke-virtual {v12, v13, v14}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/email/view/NonLockingScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v12

    float-to-int v7, v12

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v12

    const/16 v13, 0x7d0

    if-le v12, v13, :cond_3

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/android/email/view/NonLockingScrollView;->mInterceptXMotion:Z

    goto :goto_2

    :pswitch_1
    invoke-direct/range {p0 .. p0}, Lcom/android/email/view/NonLockingScrollView;->recycleVelocityTracker()V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/android/email/view/NonLockingScrollView;->mInterceptXMotion:Z

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public scrollFreeze()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mIsScrollFreeze:Z

    return-void
.end method

.method public scrollTo(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mIsScrollFreeze:Z

    if-eqz v0, :cond_0

    invoke-super {p0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0
.end method

.method public setOnOverScrollListener(Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;)V
    .locals 0
    .param p1    # Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;

    iput-object p1, p0, Lcom/android/email/view/NonLockingScrollView;->mOnOverScrollListener:Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;

    return-void
.end method
