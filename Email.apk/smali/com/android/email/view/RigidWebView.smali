.class public Lcom/android/email/view/RigidWebView;
.super Landroid/webkit/WebView;
.source "RigidWebView.java"


# static fields
.field private static final MAX_RESIZE_INTERVAL:I = 0x12c

.field private static final MIN_RESIZE_INTERVAL:I = 0xc8


# instance fields
.field private final mClock:Lcom/android/email/Clock;

.field private mIgnoreNext:Z

.field private mLastSizeChangeTime:J

.field private mRealHeight:I

.field private mRealWidth:I

.field private final mThrottle:Lcom/android/email/Throttle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/android/email/Clock;->INSTANCE:Lcom/android/email/Clock;

    iput-object v0, p0, Lcom/android/email/view/RigidWebView;->mClock:Lcom/android/email/Clock;

    new-instance v0, Lcom/android/email/Throttle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/email/view/RigidWebView$1;

    invoke-direct {v2, p0}, Lcom/android/email/view/RigidWebView$1;-><init>(Lcom/android/email/view/RigidWebView;)V

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xc8

    const/16 v5, 0x12c

    invoke-direct/range {v0 .. v5}, Lcom/android/email/Throttle;-><init>(Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;II)V

    iput-object v0, p0, Lcom/android/email/view/RigidWebView;->mThrottle:Lcom/android/email/Throttle;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/view/RigidWebView;->mLastSizeChangeTime:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/android/email/Clock;->INSTANCE:Lcom/android/email/Clock;

    iput-object v0, p0, Lcom/android/email/view/RigidWebView;->mClock:Lcom/android/email/Clock;

    new-instance v0, Lcom/android/email/Throttle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/email/view/RigidWebView$1;

    invoke-direct {v2, p0}, Lcom/android/email/view/RigidWebView$1;-><init>(Lcom/android/email/view/RigidWebView;)V

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xc8

    const/16 v5, 0x12c

    invoke-direct/range {v0 .. v5}, Lcom/android/email/Throttle;-><init>(Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;II)V

    iput-object v0, p0, Lcom/android/email/view/RigidWebView;->mThrottle:Lcom/android/email/Throttle;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/view/RigidWebView;->mLastSizeChangeTime:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/android/email/Clock;->INSTANCE:Lcom/android/email/Clock;

    iput-object v0, p0, Lcom/android/email/view/RigidWebView;->mClock:Lcom/android/email/Clock;

    new-instance v0, Lcom/android/email/Throttle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/email/view/RigidWebView$1;

    invoke-direct {v2, p0}, Lcom/android/email/view/RigidWebView$1;-><init>(Lcom/android/email/view/RigidWebView;)V

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xc8

    const/16 v5, 0x12c

    invoke-direct/range {v0 .. v5}, Lcom/android/email/Throttle;-><init>(Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;II)V

    iput-object v0, p0, Lcom/android/email/view/RigidWebView;->mThrottle:Lcom/android/email/Throttle;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/view/RigidWebView;->mLastSizeChangeTime:J

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/view/RigidWebView;)V
    .locals 0
    .param p0    # Lcom/android/email/view/RigidWebView;

    invoke-direct {p0}, Lcom/android/email/view/RigidWebView;->performSizeChangeDelayed()V

    return-void
.end method

.method private performSizeChange(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/android/email/view/RigidWebView;->mRealWidth:I

    iget v1, p0, Lcom/android/email/view/RigidWebView;->mRealHeight:I

    invoke-super {p0, v0, v1, p1, p2}, Landroid/webkit/WebView;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/android/email/view/RigidWebView;->mClock:Lcom/android/email/Clock;

    invoke-virtual {v0}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/view/RigidWebView;->mLastSizeChangeTime:J

    return-void
.end method

.method private performSizeChangeDelayed()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/view/RigidWebView;->mIgnoreNext:Z

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/email/view/RigidWebView;->performSizeChange(II)V

    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iput p1, p0, Lcom/android/email/view/RigidWebView;->mRealWidth:I

    iput p2, p0, Lcom/android/email/view/RigidWebView;->mRealHeight:I

    iget-object v4, p0, Lcom/android/email/view/RigidWebView;->mClock:Lcom/android/email/Clock;

    invoke-virtual {v4}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/android/email/view/RigidWebView;->mLastSizeChangeTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0xc8

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    const/4 v2, 0x1

    :goto_0
    iget-boolean v4, p0, Lcom/android/email/view/RigidWebView;->mIgnoreNext:Z

    if-eqz v4, :cond_2

    iput-boolean v3, p0, Lcom/android/email/view/RigidWebView;->mIgnoreNext:Z

    if-eqz v2, :cond_2

    sget-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "Email"

    const-string v4, "Supressing size change in RigidWebView"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/android/email/view/RigidWebView;->mThrottle:Lcom/android/email/Throttle;

    invoke-virtual {v3}, Lcom/android/email/Throttle;->onEvent()V

    goto :goto_1

    :cond_3
    invoke-direct {p0, p3, p4}, Lcom/android/email/view/RigidWebView;->performSizeChange(II)V

    goto :goto_1
.end method
