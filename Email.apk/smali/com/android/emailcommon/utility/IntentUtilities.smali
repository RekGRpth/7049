.class public final Lcom/android/emailcommon/utility/IntentUtilities;
.super Ljava/lang/Object;
.source "IntentUtilities.java"


# static fields
.field private static final ACCOUNT_ID_PARAM:Ljava/lang/String; = "ACCOUNT_ID"

.field private static final ACCOUNT_UUID_PARAM:Ljava/lang/String; = "ACCOUNT_UUID"

.field private static final ACTIVITY_INTENT_HOST:Ljava/lang/String; = "ui.email.android.com"

.field private static final ACTIVITY_INTENT_SCHEME:Ljava/lang/String; = "content"

.field private static final MAILBOX_ID_PARAM:Ljava/lang/String; = "MAILBOX_ID"

.field private static final MESSAGE_ID_PARAM:Ljava/lang/String; = "MESSAGE_ID"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "ui.email.android.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    return-object v0
.end method

.method public static createRestartAppIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/IntentUtilities;->prepareRestartAppIntent(Landroid/content/Intent;)V

    return-object v0
.end method

.method public static createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/IntentUtilities;->prepareRestartAppIntent(Landroid/content/Intent;)V

    return-object v0
.end method

.method public static getAccountIdFromIntent(Landroid/content/Intent;)J
    .locals 2
    .param p0    # Landroid/content/Intent;

    const-string v0, "ACCOUNT_ID"

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getLongFromIntent(Landroid/content/Intent;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getAccountUuidFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    const-string v3, "ACCOUNT_UUID"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v2

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method private static getLongFromIntent(Landroid/content/Intent;Ljava/lang/String;)J
    .locals 5
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;

    const-wide/16 v0, -0x1

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-static {v2, p1, v3, v4}, Lcom/android/emailcommon/utility/IntentUtilities;->getLongParamFromUri(Landroid/net/Uri;Ljava/lang/String;J)J

    move-result-wide v0

    :cond_0
    return-wide v0
.end method

.method private static getLongParamFromUri(Landroid/net/Uri;Ljava/lang/String;J)J
    .locals 2
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    :cond_0
    :goto_0
    return-wide p2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static getMailboxIdFromIntent(Landroid/content/Intent;)J
    .locals 2
    .param p0    # Landroid/content/Intent;

    const-string v0, "MAILBOX_ID"

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getLongFromIntent(Landroid/content/Intent;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getMessageIdFromIntent(Landroid/content/Intent;)J
    .locals 2
    .param p0    # Landroid/content/Intent;

    const-string v0, "MESSAGE_ID"

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getLongFromIntent(Landroid/content/Intent;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static prepareRestartAppIntent(Landroid/content/Intent;)V
    .locals 1
    .param p0    # Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.category.LAUNCHER"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x4000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-void
.end method

.method public static setAccountId(Landroid/net/Uri$Builder;J)V
    .locals 2
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # J

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const-string v0, "ACCOUNT_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    return-void
.end method

.method public static setAccountUuid(Landroid/net/Uri$Builder;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const-string v0, "ACCOUNT_UUID"

    invoke-virtual {p0, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    return-void
.end method

.method public static setMailboxId(Landroid/net/Uri$Builder;J)V
    .locals 2
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # J

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const-string v0, "MAILBOX_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    return-void
.end method

.method public static setMessageId(Landroid/net/Uri$Builder;J)V
    .locals 2
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # J

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const-string v0, "MESSAGE_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    return-void
.end method
