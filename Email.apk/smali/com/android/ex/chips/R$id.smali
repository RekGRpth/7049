.class public final Lcom/android/ex/chips/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ANCHOR_MENU:I = 0x7f0f0112

.field public static final EMAIL_MENU:I = 0x7f0f010c

.field public static final GEO_MENU:I = 0x7f0f010f

.field public static final HELP_FEEDBACK_GROUP:I = 0x7f0f0116

.field public static final PHONE_MENU:I = 0x7f0f0107

.field public static final accept:I = 0x7f0f00c2

.field public static final account_background_attachments:I = 0x7f0f0036

.field public static final account_background_attachments_divider:I = 0x7f0f003e

.field public static final account_check_frequency:I = 0x7f0f002e

.field public static final account_default:I = 0x7f0f0016

.field public static final account_default_divider:I = 0x7f0f0039

.field public static final account_default_divider_1:I = 0x7f0f0018

.field public static final account_default_divider_2:I = 0x7f0f0019

.field public static final account_default_divider_upper:I = 0x7f0f0038

.field public static final account_delete_policy:I = 0x7f0f0027

.field public static final account_delete_policy_label:I = 0x7f0f0026

.field public static final account_description:I = 0x7f0f002a

.field public static final account_description_label:I = 0x7f0f002d

.field public static final account_email:I = 0x7f0f0014

.field public static final account_name:I = 0x7f0f002c

.field public static final account_name_label:I = 0x7f0f002b

.field public static final account_notify:I = 0x7f0f0032

.field public static final account_notify_divider:I = 0x7f0f003a

.field public static final account_password:I = 0x7f0f0015

.field public static final account_port:I = 0x7f0f0020

.field public static final account_require_login:I = 0x7f0f003f

.field public static final account_require_login_settings:I = 0x7f0f0040

.field public static final account_require_login_settings_2:I = 0x7f0f0041

.field public static final account_security_type:I = 0x7f0f0025

.field public static final account_server:I = 0x7f0f001c

.field public static final account_server_label:I = 0x7f0f0024

.field public static final account_settings:I = 0x7f0f00fb

.field public static final account_settings_quick_responses_list:I = 0x7f0f0003

.field public static final account_spinner:I = 0x7f0f0049

.field public static final account_spinner_container:I = 0x7f0f0043

.field public static final account_ssl:I = 0x7f0f001d

.field public static final account_sync_calendar:I = 0x7f0f0034

.field public static final account_sync_calendar_divider:I = 0x7f0f003c

.field public static final account_sync_contacts:I = 0x7f0f0033

.field public static final account_sync_contacts_divider:I = 0x7f0f003b

.field public static final account_sync_email:I = 0x7f0f0035

.field public static final account_sync_email_divider:I = 0x7f0f003d

.field public static final account_sync_window:I = 0x7f0f0031

.field public static final account_sync_window_label:I = 0x7f0f0030

.field public static final account_sync_window_row:I = 0x7f0f002f

.field public static final account_trust_certificates:I = 0x7f0f001e

.field public static final account_trust_certificates_divider:I = 0x7f0f0022

.field public static final account_username:I = 0x7f0f001b

.field public static final add_attachment:I = 0x7f0f005d

.field public static final add_cc_bcc:I = 0x7f0f005c

.field public static final add_contact_context_menu_id:I = 0x7f0f010a

.field public static final add_new_account:I = 0x7f0f00f3

.field public static final add_star:I = 0x7f0f0104

.field public static final address_divider:I = 0x7f0f00b5

.field public static final addresses:I = 0x7f0f00c6

.field public static final always_show_pictures_button:I = 0x7f0f00ac

.field public static final attachment_container:I = 0x7f0f0083

.field public static final attachment_delete:I = 0x7f0f0088

.field public static final attachment_icon:I = 0x7f0f009c

.field public static final attachment_info:I = 0x7f0f009d

.field public static final attachment_name:I = 0x7f0f0053

.field public static final attachment_size:I = 0x7f0f0054

.field public static final attachment_thumbnail:I = 0x7f0f0052

.field public static final attachments:I = 0x7f0f0082

.field public static final attachments_scroll:I = 0x7f0f00b2

.field public static final badge:I = 0x7f0f00bb

.field public static final bcc:I = 0x7f0f0068

.field public static final bcc_content:I = 0x7f0f0066

.field public static final bcc_label:I = 0x7f0f0067

.field public static final bcc_row:I = 0x7f0f00a6

.field public static final body_text:I = 0x7f0f006a

.field public static final buttons_wrapper:I = 0x7f0f0007

.field public static final cancel:I = 0x7f0f0001

.field public static final cc:I = 0x7f0f0065

.field public static final cc_bcc_container:I = 0x7f0f00dc

.field public static final cc_bcc_wrapper:I = 0x7f0f0062

.field public static final cc_content:I = 0x7f0f0063

.field public static final cc_label:I = 0x7f0f0064

.field public static final cc_row:I = 0x7f0f00a5

.field public static final certificate_alias:I = 0x7f0f005a

.field public static final checkmark:I = 0x7f0f008f

.field public static final clear_webview_cache:I = 0x7f0f0072

.field public static final client_certificate_divider:I = 0x7f0f0023

.field public static final client_certificate_selector:I = 0x7f0f001f

.field public static final color_chip:I = 0x7f0f004f

.field public static final common:I = 0x7f0f0010

.field public static final compose:I = 0x7f0f00f4

.field public static final compose_buttons_spacer:I = 0x7f0f005b

.field public static final compose_recipients_wrapper:I = 0x7f0f005e

.field public static final compose_scroll:I = 0x7f0f0081

.field public static final compose_scrollview:I = 0x7f0f0084

.field public static final composearea_tap_trap_bottom:I = 0x7f0f0087

.field public static final connection_error_text:I = 0x7f0f006d

.field public static final content:I = 0x7f0f0085

.field public static final copy_geo_context_menu_id:I = 0x7f0f0111

.field public static final copy_link_context_menu_id:I = 0x7f0f0114

.field public static final copy_mail_context_menu_id:I = 0x7f0f010e

.field public static final copy_phone_context_menu_id:I = 0x7f0f010b

.field public static final create_new:I = 0x7f0f0005

.field public static final date:I = 0x7f0f0093

.field public static final date_container:I = 0x7f0f0091

.field public static final date_row:I = 0x7f0f00a3

.field public static final datetime:I = 0x7f0f00c7

.field public static final debug_disable_graphics_acceleration:I = 0x7f0f0073

.field public static final debug_enable_strict_mode:I = 0x7f0f0075

.field public static final debug_force_one_minute_refresh:I = 0x7f0f0074

.field public static final debug_logging:I = 0x7f0f006f

.field public static final decline:I = 0x7f0f00c4

.field public static final delete:I = 0x7f0f0100

.field public static final delete_icon:I = 0x7f0f00cf

.field public static final device_id:I = 0x7f0f0021

.field public static final dial_context_menu_id:I = 0x7f0f0108

.field public static final discard:I = 0x7f0f00fd

.field public static final display_name:I = 0x7f0f004d

.field public static final divider_bar:I = 0x7f0f00d7

.field public static final divider_bar_1:I = 0x7f0f00d5

.field public static final done:I = 0x7f0f0002

.field public static final edit_quoted_text:I = 0x7f0f00d6

.field public static final email_address:I = 0x7f0f004e

.field public static final email_context_menu_id:I = 0x7f0f010d

.field public static final email_password_table:I = 0x7f0f0017

.field public static final empty_view:I = 0x7f0f0004

.field public static final error_message:I = 0x7f0f0076

.field public static final exchange:I = 0x7f0f000a

.field public static final exchange_file_logging:I = 0x7f0f0071

.field public static final exchange_logging:I = 0x7f0f0070

.field public static final favorite:I = 0x7f0f00b6

.field public static final feedback_menu_item:I = 0x7f0f0118

.field public static final folder_expanded_icon:I = 0x7f0f007d

.field public static final folder_icon:I = 0x7f0f007b

.field public static final forward:I = 0x7f0f00ba

.field public static final fragment_placeholder:I = 0x7f0f0077

.field public static final fragment_wrapper:I = 0x7f0f0006

.field public static final from:I = 0x7f0f006b

.field public static final from_address:I = 0x7f0f00be

.field public static final from_name:I = 0x7f0f00bc

.field public static final headline:I = 0x7f0f000b

.field public static final help_info_menu_item:I = 0x7f0f0117

.field public static final icon:I = 0x7f0f00ca

.field public static final imap:I = 0x7f0f0009

.field public static final imap_path_prefix:I = 0x7f0f0029

.field public static final imap_path_prefix_section:I = 0x7f0f0028

.field public static final include_quoted_text:I = 0x7f0f00d3

.field public static final include_quoted_text_content:I = 0x7f0f00d4

.field public static final info:I = 0x7f0f009f

.field public static final inmessage_command_buttons:I = 0x7f0f00e1

.field public static final instructions:I = 0x7f0f000e

.field public static final invite_info:I = 0x7f0f00c1

.field public static final invite_link:I = 0x7f0f00c0

.field public static final invite_scroll:I = 0x7f0f00b1

.field public static final invite_section:I = 0x7f0f00bf

.field public static final label:I = 0x7f0f00db

.field public static final left_pane:I = 0x7f0f00dd

.field public static final line_item:I = 0x7f0f00ce

.field public static final list_panel:I = 0x7f0f008b

.field public static final load:I = 0x7f0f009e

.field public static final loading_progress:I = 0x7f0f00a7

.field public static final loading_text:I = 0x7f0f00f2

.field public static final mailbox_name:I = 0x7f0f007c

.field public static final mailbox_settings:I = 0x7f0f00fa

.field public static final main_content:I = 0x7f0f000d

.field public static final main_panel:I = 0x7f0f00a8

.field public static final main_text:I = 0x7f0f008d

.field public static final manual_setup:I = 0x7f0f0012

.field public static final map_context_menu_id:I = 0x7f0f0110

.field public static final mark_as_unread:I = 0x7f0f0106

.field public static final mark_read:I = 0x7f0f0102

.field public static final mark_unread:I = 0x7f0f0103

.field public static final maybe:I = 0x7f0f00c3

.field public static final message_command_buttons:I = 0x7f0f00df

.field public static final message_content:I = 0x7f0f0089

.field public static final message_count:I = 0x7f0f007a

.field public static final message_file_view_fragment:I = 0x7f0f008a

.field public static final message_list:I = 0x7f0f00e9

.field public static final message_position:I = 0x7f0f007f

.field public static final message_tabs_section:I = 0x7f0f00aa

.field public static final message_title:I = 0x7f0f009a

.field public static final message_view_header_upper:I = 0x7f0f00b4

.field public static final message_view_subheader:I = 0x7f0f00a9

.field public static final message_warning:I = 0x7f0f009b

.field public static final middle_pane:I = 0x7f0f00de

.field public static final more:I = 0x7f0f00b9

.field public static final move:I = 0x7f0f0101

.field public static final move_to_newer_button:I = 0x7f0f007e

.field public static final move_to_older_button:I = 0x7f0f0080

.field public static final msg_remain:I = 0x7f0f00b0

.field public static final msg_remain_btn:I = 0x7f0f00cb

.field public static final msg_remain_loading_progress:I = 0x7f0f00cd

.field public static final msg_remain_loading_text:I = 0x7f0f00cc

.field public static final newer:I = 0x7f0f00f8

.field public static final next:I = 0x7f0f0013

.field public static final older:I = 0x7f0f00f9

.field public static final open:I = 0x7f0f00a1

.field public static final open_context_menu_id:I = 0x7f0f0113

.field public static final padding_view:I = 0x7f0f0011

.field public static final paperclip:I = 0x7f0f0092

.field public static final pop:I = 0x7f0f0008

.field public static final presence:I = 0x7f0f00bd

.field public static final previous:I = 0x7f0f000f

.field public static final progress:I = 0x7f0f008c

.field public static final progress_circular:I = 0x7f0f00da

.field public static final quick_response_text:I = 0x7f0f0079

.field public static final quoted_text:I = 0x7f0f00d8

.field public static final quoted_text_area:I = 0x7f0f00d0

.field public static final quoted_text_bar:I = 0x7f0f00d1

.field public static final quoted_text_row:I = 0x7f0f00d2

.field public static final refresh:I = 0x7f0f00f7

.field public static final remote_result_count:I = 0x7f0f0045

.field public static final remote_search_title:I = 0x7f0f0044

.field public static final remove_attachment:I = 0x7f0f0055

.field public static final remove_star:I = 0x7f0f0105

.field public static final rename_edittext:I = 0x7f0f0057

.field public static final rename_label:I = 0x7f0f0056

.field public static final reply:I = 0x7f0f00b7

.field public static final reply_all:I = 0x7f0f00b8

.field public static final reply_state:I = 0x7f0f008e

.field public static final result_count:I = 0x7f0f0046

.field public static final right_pane:I = 0x7f0f00e0

.field public static final save:I = 0x7f0f00a0

.field public static final search:I = 0x7f0f00f5

.field public static final search_count:I = 0x7f0f0097

.field public static final search_header:I = 0x7f0f0096

.field public static final search_header_text:I = 0x7f0f0098

.field public static final search_view:I = 0x7f0f0047

.field public static final select_button:I = 0x7f0f0058

.field public static final send:I = 0x7f0f00fc

.field public static final senders:I = 0x7f0f0094

.field public static final settings:I = 0x7f0f00ff

.field public static final settings_fragment:I = 0x7f0f0000

.field public static final setup_fragment:I = 0x7f0f001a

.field public static final share_link_context_menu_id:I = 0x7f0f0115

.field public static final shortcut_list:I = 0x7f0f0042

.field public static final show_all_mailboxes:I = 0x7f0f00f6

.field public static final show_attachments:I = 0x7f0f00af

.field public static final show_details:I = 0x7f0f00a2

.field public static final show_invite:I = 0x7f0f00ae

.field public static final show_message:I = 0x7f0f00ad

.field public static final show_pictures:I = 0x7f0f00ab

.field public static final show_quick_text_list_dialog:I = 0x7f0f00fe

.field public static final sms_context_menu_id:I = 0x7f0f0109

.field public static final spinner:I = 0x7f0f0099

.field public static final spinner_count:I = 0x7f0f0048

.field public static final spinner_frame:I = 0x7f0f004c

.field public static final spinner_line_1:I = 0x7f0f004a

.field public static final spinner_line_2:I = 0x7f0f004b

.field public static final spinners_table:I = 0x7f0f0037

.field public static final star:I = 0x7f0f0090

.field public static final sub_header_contents_collapsed:I = 0x7f0f00c5

.field public static final sub_header_contents_expanded:I = 0x7f0f00c8

.field public static final subject:I = 0x7f0f0069

.field public static final subject_divider:I = 0x7f0f00b3

.field public static final switch_account_icon:I = 0x7f0f006c

.field public static final tap_to_configure:I = 0x7f0f00ea

.field public static final text1:I = 0x7f0f00c9

.field public static final text2:I = 0x7f0f00d9

.field public static final three_pane:I = 0x7f0f0078

.field public static final thumbnail:I = 0x7f0f0051

.field public static final title:I = 0x7f0f0059

.field public static final to:I = 0x7f0f0061

.field public static final to_content:I = 0x7f0f005f

.field public static final to_label:I = 0x7f0f0060

.field public static final to_row:I = 0x7f0f00a4

.field public static final top_divider:I = 0x7f0f000c

.field public static final unread_count:I = 0x7f0f0050

.field public static final unread_state:I = 0x7f0f0095

.field public static final version:I = 0x7f0f006e

.field public static final waiting_for_sync_message:I = 0x7f0f00e2

.field public static final widget_attachment:I = 0x7f0f00ef

.field public static final widget_compose:I = 0x7f0f00e8

.field public static final widget_count:I = 0x7f0f00e7

.field public static final widget_date:I = 0x7f0f00f0

.field public static final widget_from:I = 0x7f0f00ed

.field public static final widget_header:I = 0x7f0f00e3

.field public static final widget_invite:I = 0x7f0f00ee

.field public static final widget_logo:I = 0x7f0f00e4

.field public static final widget_message:I = 0x7f0f00eb

.field public static final widget_subject:I = 0x7f0f00f1

.field public static final widget_tap:I = 0x7f0f00e6

.field public static final widget_title:I = 0x7f0f00e5

.field public static final widget_unread_state:I = 0x7f0f00ec

.field public static final wrapper:I = 0x7f0f0086


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
