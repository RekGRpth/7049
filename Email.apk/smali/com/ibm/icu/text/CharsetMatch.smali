.class public Lcom/ibm/icu/text/CharsetMatch;
.super Ljava/lang/Object;
.source "CharsetMatch.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/ibm/icu/text/CharsetMatch;",
        ">;"
    }
.end annotation


# static fields
.field public static final BOM:I = 0x2

.field public static final DECLARED_ENCODING:I = 0x4

.field public static final ENCODING_SCHEME:I = 0x1

.field public static final LANG_STATISTICS:I = 0x8


# instance fields
.field private fConfidence:I

.field private fInputStream:Ljava/io/InputStream;

.field private fRawInput:[B

.field private fRawLength:I

.field private fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/CharsetDetector;Lcom/ibm/icu/text/CharsetRecognizer;I)V
    .locals 1
    .param p1    # Lcom/ibm/icu/text/CharsetDetector;
    .param p2    # Lcom/ibm/icu/text/CharsetRecognizer;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/ibm/icu/text/CharsetMatch;->fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;

    iput p3, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    iget v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    iput v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawLength:I

    :cond_0
    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/ibm/icu/text/CharsetMatch;)I
    .locals 3
    .param p1    # Lcom/ibm/icu/text/CharsetMatch;

    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    iget v2, p1, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    iget v2, p1, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    if-ge v1, v2, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/ibm/icu/text/CharsetMatch;

    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/CharsetMatch;->compareTo(Lcom/ibm/icu/text/CharsetMatch;)I

    move-result v0

    return v0
.end method

.method public getConfidence()I
    .locals 1

    iget v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CharsetRecognizer;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMatchType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CharsetRecognizer;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReader()Ljava/io/Reader;
    .locals 5

    iget-object v1, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    const/4 v3, 0x0

    iget v4, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawLength:I

    invoke-direct {v1, v2, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Lcom/ibm/icu/text/CharsetMatch;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/CharsetMatch;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v8, 0x400

    const/4 v7, 0x0

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-array v0, v8, [C

    invoke-virtual {p0}, Lcom/ibm/icu/text/CharsetMatch;->getReader()Ljava/io/Reader;

    move-result-object v3

    if-gez p1, :cond_0

    const v2, 0x7fffffff

    :goto_0
    const/4 v1, 0x0

    :goto_1
    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v3, v0, v7, v6}, Ljava/io/Reader;->read([CII)I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {v5, v0, v7, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    sub-int/2addr v2, v1

    goto :goto_1

    :cond_0
    move v2, p1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/io/Reader;->close()V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_2
    return-object v6

    :cond_2
    new-instance v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    invoke-virtual {p0}, Lcom/ibm/icu/text/CharsetMatch;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    move-object v6, v4

    goto :goto_2
.end method
