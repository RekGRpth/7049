.class abstract Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_32;
.super Lcom/ibm/icu/text/CharsetRecog_Unicode;
.source "CharsetRecog_Unicode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CharsetRecog_Unicode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "CharsetRecog_UTF_32"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/ibm/icu/text/CharsetRecog_Unicode;-><init>()V

    return-void
.end method


# virtual methods
.method abstract getChar([BI)I
.end method

.method abstract getName()Ljava/lang/String;
.end method

.method match(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 10
    .param p1    # Lcom/ibm/icu/text/CharsetDetector;

    const/4 v8, 0x0

    iget-object v4, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    iget v9, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    div-int/lit8 v9, v9, 0x4

    mul-int/lit8 v5, v9, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-nez v5, :cond_0

    :goto_0
    return v8

    :cond_0
    invoke-virtual {p0, v4, v8}, Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_32;->getChar([BI)I

    move-result v8

    const v9, 0xfeff

    if-ne v8, v9, :cond_1

    const/4 v2, 0x1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_4

    invoke-virtual {p0, v4, v3}, Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_32;->getChar([BI)I

    move-result v0

    if-ltz v0, :cond_2

    const v8, 0x10ffff

    if-ge v0, v8, :cond_2

    const v8, 0xd800

    if-lt v0, v8, :cond_3

    const v8, 0xdfff

    if-gt v0, v8, :cond_3

    :cond_2
    add-int/lit8 v6, v6, 0x1

    :goto_2
    add-int/lit8 v3, v3, 0x4

    goto :goto_1

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_4
    if-eqz v2, :cond_6

    if-nez v6, :cond_6

    const/16 v1, 0x64

    :cond_5
    :goto_3
    move v8, v1

    goto :goto_0

    :cond_6
    if-eqz v2, :cond_7

    mul-int/lit8 v8, v6, 0xa

    if-le v7, v8, :cond_7

    const/16 v1, 0x50

    goto :goto_3

    :cond_7
    const/4 v8, 0x3

    if-le v7, v8, :cond_8

    if-nez v6, :cond_8

    const/16 v1, 0x64

    goto :goto_3

    :cond_8
    if-lez v7, :cond_9

    if-nez v6, :cond_9

    const/16 v1, 0x50

    goto :goto_3

    :cond_9
    mul-int/lit8 v8, v6, 0xa

    if-le v7, v8, :cond_5

    const/16 v1, 0x19

    goto :goto_3
.end method
