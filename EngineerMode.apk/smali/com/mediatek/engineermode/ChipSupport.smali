.class public Lcom/mediatek/engineermode/ChipSupport;
.super Ljava/lang/Object;
.source "ChipSupport.java"


# static fields
.field public static final HAVE_MATV_FEATURE:I = 0x5

.field public static final MTK_6516_SUPPORT:I = 0x2

.field public static final MTK_6573_SUPPORT:I = 0x1

.field public static final MTK_6575_SUPPORT:I = 0x4

.field public static final MTK_6577_SUPPORT:I = 0x8

.field public static final MTK_AGPS_APP:I = 0x3

.field public static final MTK_BT_SUPPORT:I = 0x6

.field public static final MTK_FM_SUPPORT:I = 0x0

.field public static final MTK_FM_TX_SUPPORT:I = 0x1

.field public static final MTK_GPS_SUPPORT:I = 0x4

.field public static final MTK_NFC_SUPPORT:I = 0x9

.field public static final MTK_RADIO_SUPPORT:I = 0x2

.field public static final MTK_TTY_SUPPORT:I = 0x8

.field public static final MTK_UNKNOWN_SUPPORT:I = 0x0

.field public static final MTK_WLAN_SUPPORT:I = 0x7


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "em_support_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getChip()I
.end method

.method public static native isFeatureSupported(I)Z
.end method
