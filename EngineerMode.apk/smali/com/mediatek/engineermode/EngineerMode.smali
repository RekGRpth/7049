.class public Lcom/mediatek/engineermode/EngineerMode;
.super Landroid/app/Activity;
.source "EngineerMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/EngineerMode$MyPagerAdapter;
    }
.end annotation


# static fields
.field private static final TAB_COUNT:I = 0x6

.field private static final TAB_TITLE_IDS:[I

.field private static final TAG:Ljava/lang/String; = "EM/MainView"


# instance fields
.field private mTabs:[Lcom/mediatek/engineermode/PrefsFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/engineermode/EngineerMode;->TAB_TITLE_IDS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f080005
        0x7f080006
        0x7f080007
        0x7f080008
        0x7f080009
        0x7f08000a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mediatek/engineermode/PrefsFragment;

    iput-object v0, p0, Lcom/mediatek/engineermode/EngineerMode;->mTabs:[Lcom/mediatek/engineermode/PrefsFragment;

    return-void
.end method

.method static synthetic access$000()[I
    .locals 1

    sget-object v0, Lcom/mediatek/engineermode/EngineerMode;->TAB_TITLE_IDS:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/EngineerMode;)[Lcom/mediatek/engineermode/PrefsFragment;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/EngineerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/EngineerMode;->mTabs:[Lcom/mediatek/engineermode/PrefsFragment;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const v8, 0x7f0b014b

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f030032

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const-string v6, "EM/MainView"

    const-string v7, "new fregments"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    const/4 v6, 0x6

    if-ge v1, v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/engineermode/EngineerMode;->mTabs:[Lcom/mediatek/engineermode/PrefsFragment;

    new-instance v7, Lcom/mediatek/engineermode/PrefsFragment;

    invoke-direct {v7}, Lcom/mediatek/engineermode/PrefsFragment;-><init>()V

    aput-object v7, v6, v1

    iget-object v6, p0, Lcom/mediatek/engineermode/EngineerMode;->mTabs:[Lcom/mediatek/engineermode/PrefsFragment;

    aget-object v6, v6, v1

    invoke-virtual {v6, v1}, Lcom/mediatek/engineermode/PrefsFragment;->setResource(I)V

    iget-object v6, p0, Lcom/mediatek/engineermode/EngineerMode;->mTabs:[Lcom/mediatek/engineermode/PrefsFragment;

    aget-object v6, v6, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v8, v6, v7}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    iget-object v6, p0, Lcom/mediatek/engineermode/EngineerMode;->mTabs:[Lcom/mediatek/engineermode/PrefsFragment;

    aget-object v6, v6, v1

    invoke-virtual {v4, v6}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    const v6, 0x7f0b014c

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/PagerTabStrip;

    const v6, 0x1060012

    invoke-virtual {v3, v6}, Landroid/support/v4/view/PagerTabStrip;->setTabIndicatorColorResource(I)V

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    new-instance v2, Lcom/mediatek/engineermode/EngineerMode$MyPagerAdapter;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/EngineerMode$MyPagerAdapter;-><init>(Lcom/mediatek/engineermode/EngineerMode;)V

    invoke-virtual {v5, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void
.end method
