.class public Lcom/mediatek/engineermode/GPRS;
.super Landroid/app/Activity;
.source "GPRS.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final ATTACH_MODE_ALWAYS:I = 0x1

.field public static final ATTACH_MODE_NOT_SPECIFY:I = -0x1

.field public static final ATTACH_MODE_WHEN_NEEDED:I = 0x0

.field private static final EVENT_GPRS_ATTACHED:I = 0x1

.field private static final EVENT_GPRS_ATTACH_TYPE:I = 0x9

.field private static final EVENT_GPRS_DETACHED:I = 0x2

.field private static final EVENT_GPRS_FD:I = 0x8

.field private static final EVENT_GPRS_INTERNAL_AT:I = 0x6

.field private static final EVENT_PDP_ACTIVATE:I = 0x3

.field private static final EVENT_PDP_DEACTIVATE:I = 0x4

.field private static final EVENT_SEND_DATA:I = 0x5

.field private static final EVENT_WRITE_IMEI:I = 0x7

.field static final LOG_TAG:Ljava/lang/String; = "GPRS EN"

.field private static final PDP_CONTEXT_MAX:I = 0xf

.field public static final PREFERENCE_GPRS:Ljava/lang/String; = "com.mtk.GPRS"

.field public static final PREF_ATTACH_MODE:Ljava/lang/String; = "ATTACH_MODE"

.field public static final PREF_ATTACH_MODE_SIM:Ljava/lang/String; = "ATTACH_MODE_SIM"

.field private static final SCRI_DEFAULT_TIMEOUT:I = 0x14


# instance fields
.field private mAlive:Z

.field private mBtnActivate:Landroid/widget/Button;

.field private mBtnAttached:Landroid/widget/Button;

.field private mBtnAttachedContinue:Landroid/widget/Button;

.field private mBtnConfigFD:Landroid/widget/Button;

.field private mBtnDeactivate:Landroid/widget/Button;

.field private mBtnDetached:Landroid/widget/Button;

.field private mBtnDetachedContinue:Landroid/widget/Button;

.field private mBtnFastDormancy:Landroid/widget/Button;

.field private mBtnImei:Landroid/widget/Button;

.field private mBtnNotSpecify:Landroid/widget/Button;

.field private mBtnSendData:Landroid/widget/Button;

.field private mBtnSendFdOffTimer:Landroid/widget/Button;

.field private mBtnSendFdOnTimer:Landroid/widget/Button;

.field private mBtnSim1:Landroid/widget/Button;

.field private mBtnSim2:Landroid/widget/Button;

.field mContextCmdStringArray:[Ljava/lang/String;

.field private mEditDataLen:Landroid/widget/EditText;

.field private mEditGprsFdOffTimer:Landroid/widget/EditText;

.field private mEditGprsFdOnTimer:Landroid/widget/EditText;

.field private mEditImeiValue:Landroid/widget/EditText;

.field private mFlag:Z

.field private mGprstAttachSelect:Landroid/widget/RadioGroup;

.field private mPDPContextIndex:I

.field private mPDPSelect:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mRaBtnSIM1Enabled:Landroid/widget/RadioButton;

.field private mRaBtnSIM2Enabled:Landroid/widget/RadioButton;

.field private mRaGpDefSIMSelect:Landroid/widget/RadioGroup;

.field private mRaGpPDPSelect:Landroid/widget/RadioGroup;

.field private mRaGpUsageSelect:Landroid/widget/RadioGroup;

.field private mResponseHander:Landroid/os/Handler;

.field private mSPinnerPDPContext:Landroid/widget/Spinner;

.field private mSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTextDefSIMSelect:Landroid/widget/TextView;

.field private mUsageSelect:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "3,128,128,0,0,1,1500,\"1e3\",\"4e3\",1,0,0"

    aput-object v1, v0, v3

    const-string v1, "3,128,128,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "3,128,128,0,0,1,1500,\"1e3\",\"4e3\",1,0,0"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "3,256,256,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "3,128,128,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "3,256,256,0,0,1,1500,\"1e3\",\"4e3\",1,0,0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "3,256,256,0,0,1,1500,\"1e3\",\"4e3\",1,0,0"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "3,128,128,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "3,128,128,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "3,128,128,0,0,1,1500,\"1e3\",\"4e3\",1,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "3,128,128,0,0,1,1500,\"1e6\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3,128,128,0,0,1,1500,\"1e6\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "3,128,128,0,0,1,1500,\"1e6\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "3,128,128,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "3,256,256,0,0,1,1500,\"1e3\",\"4e3\",1,0,0"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "3,512,512,0,0,1,1500,\"1e4\",\"1e5\",0,0,0"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/engineermode/GPRS;->mContextCmdStringArray:[Ljava/lang/String;

    iput-boolean v4, p0, Lcom/mediatek/engineermode/GPRS;->mFlag:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    iput v3, p0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    iput v3, p0, Lcom/mediatek/engineermode/GPRS;->mUsageSelect:I

    iput v3, p0, Lcom/mediatek/engineermode/GPRS;->mPDPContextIndex:I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/GPRS;->mAlive:Z

    new-instance v0, Lcom/mediatek/engineermode/GPRS$6;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/GPRS$6;-><init>(Lcom/mediatek/engineermode/GPRS;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/GPRS;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/GPRS;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/GPRS;->mPDPContextIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/GPRS;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/GPRS;

    iget-object v0, p0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/GPRS;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/GPRS;

    iget-object v0, p0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/GPRS;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/GPRS;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/GPRS;)Landroid/widget/RadioGroup;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/GPRS;

    iget-object v0, p0, Lcom/mediatek/engineermode/GPRS;->mRaGpUsageSelect:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/engineermode/GPRS;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/GPRS;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/GPRS;->mUsageSelect:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/GPRS;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/GPRS;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/GPRS;->mAlive:Z

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/GPRS;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/GPRS;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/GPRS;->mFlag:Z

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/engineermode/GPRS;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/GPRS;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/GPRS;->mFlag:Z

    return p1
.end method

.method private rebootAlert()V
    .locals 4

    new-instance v1, Lcom/mediatek/engineermode/GPRS$7;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/GPRS$7;-><init>(Lcom/mediatek/engineermode/GPRS;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "Reboot?"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Yes"

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "No"

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Warning"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showDefaultSim()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getMySimId()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim1:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim2:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v0, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim1:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim2:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateAttachModeMMI()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v2, "com.mtk.GPRS"

    invoke-virtual {p0, v2, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "ATTACH_MODE"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttachedContinue:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetachedContinue:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnNotSpecify:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttachedContinue:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetachedContinue:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnNotSpecify:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttachedContinue:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetachedContinue:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/GPRS;->mBtnNotSpecify:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 23
    .param p1    # Landroid/view/View;

    const-string v19, "GPRS EN"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "onClick:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnImei:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v13, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+EGMR=1,"

    aput-object v20, v13, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v13, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getMySimId()I

    move-result v16

    if-nez v16, :cond_17

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "AT+EGMR=1,7,\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    :cond_0
    :goto_0
    const-string v19, "GPRS EN"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "IMEI String:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x0

    aget-object v21, v13, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x7

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v13, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnSim1:Landroid/widget/Button;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_3

    :try_start_0
    const-string v19, "phone"

    invoke-static/range {v19 .. v19}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v11

    if-nez v11, :cond_2

    const-string v19, "GPRS EN"

    const-string v20, "clocwork worked..."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v11, v0}, Lcom/android/internal/telephony/ITelephony;->setDefaultPhone(I)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    const-string v19, "GPRS EN"

    const-string v20, "SIM 1"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/engineermode/GPRS;->showDefaultSim()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v19, "GPRS EN"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "IMEI 1: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnSim2:Landroid/widget/Button;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_5

    :try_start_1
    const-string v19, "phone"

    invoke-static/range {v19 .. v19}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v11

    if-nez v11, :cond_4

    const-string v19, "GPRS EN"

    const-string v20, "clocwork worked..."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v11, v0}, Lcom/android/internal/telephony/ITelephony;->setDefaultPhone(I)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    const-string v19, "GPRS EN"

    const-string v20, "SIM 2"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/engineermode/GPRS;->showDefaultSim()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v19, "GPRS EN"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "IMEI 2: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnAttached:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v4, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+CGATT=1"

    aput-object v20, v4, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v4, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v4, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnDetached:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v7, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+CGATT=0"

    aput-object v20, v7, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v7, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v7, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_7
    const-string v19, "com.mtk.GPRS"

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnAttachedContinue:Landroid/widget/Button;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_18

    const-string v19, "persist.radio.gprs.attach.type"

    const-string v20, "1"

    invoke-static/range {v19 .. v20}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v5, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+EGTYPE=1,1"

    aput-object v20, v5, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v5, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x9

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v5, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const-string v19, "ATTACH_MODE"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_8
    :goto_3
    const-string v19, "ATTACH_MODE_SIM"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getMySimId()I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/engineermode/GPRS;->updateAttachModeMMI()V

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnFastDormancy:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v10, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+ESCRI"

    aput-object v20, v10, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v10, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v10, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnConfigFD:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    new-instance v19, Landroid/content/Intent;

    const-class v20, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnActivate:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/engineermode/GPRS;->mFlag:Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    move/from16 v19, v0

    if-nez v19, :cond_b

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v3, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+CGQMIN=1"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    const-string v20, "AT+CGQREQ=1"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    const-string v20, "AT+CGDCONT=1,\"IP\",\"internet\",\"192.168.1.1\",0,0"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "AT+CGEQREQ=1,"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mContextCmdStringArray:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPContextIndex:I

    move/from16 v22, v0

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    const-string v20, "AT+ACTTEST=1,1"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_b
    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v3, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+CGQMIN=2"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    const-string v20, "AT+CGQREQ=2"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mUsageSelect:I

    move/from16 v19, v0

    if-nez v19, :cond_c

    const/16 v19, 0x0

    const-string v20, "AT+CGDCONT=2,\"IP\",\"internet\",\"192.168.1.1\",0,0"

    aput-object v20, v3, v19

    :cond_c
    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mUsageSelect:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_d

    const/16 v19, 0x0

    const-string v20, "AT+CGDSCONT=2,1,0,0"

    aput-object v20, v3, v19

    :cond_d
    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "AT+CGEQREQ=2,"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mContextCmdStringArray:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPContextIndex:I

    move/from16 v22, v0

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const/16 v19, 0x0

    const-string v20, "AT+ACTTEST=1,2"

    aput-object v20, v3, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v3, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnDeactivate:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_11

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/engineermode/GPRS;->mFlag:Z

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v6, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    move/from16 v19, v0

    if-nez v19, :cond_f

    const/16 v19, 0x0

    const-string v20, "AT+ACTTEST=0,1"

    aput-object v20, v6, v19

    :cond_f
    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    const/16 v19, 0x0

    const-string v20, "AT+ACTTEST=0,2"

    aput-object v20, v6, v19

    :cond_10
    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v6, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v6, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnSendData:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditDataLen:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v15, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    move/from16 v19, v0

    if-nez v19, :cond_12

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "AT+CGSDATA="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",1"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v15, v19

    :cond_12
    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/GPRS;->mPDPSelect:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_13

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "AT+CGSDATA="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",2"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v15, v19

    :cond_13
    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v15, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x5

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_14
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnSendFdOnTimer:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditGprsFdOnTimer:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "persist.radio.fd.counter"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnSendFdOffTimer:Landroid/widget/Button;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getId()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditGprsFdOffTimer:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "persist.radio.fd.off.counter"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    return-void

    :cond_17
    const/16 v19, 0x1

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "AT+EGMR=1,10,\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    goto/16 :goto_0

    :catch_0
    move-exception v8

    const-string v19, "GPRS EN"

    const-string v20, "RemoteException in ITelephony.Stub.asInterface"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_1
    move-exception v8

    const-string v19, "GPRS EN"

    const-string v20, "RemoteException in ITelephony.Stub.asInterface"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnDetachedContinue:Landroid/widget/Button;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_19

    const-string v19, "persist.radio.gprs.attach.type"

    const-string v20, "0"

    invoke-static/range {v19 .. v20}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v5, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, "AT+EGTYPE=0,1"

    aput-object v20, v5, v19

    const/16 v19, 0x1

    const-string v20, ""

    aput-object v20, v5, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mResponseHander:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x9

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v5, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const-string v19, "ATTACH_MODE"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3

    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/GPRS;->mBtnNotSpecify:Landroid/widget/Button;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_8

    const-string v19, "ATTACH_MODE"

    const/16 v20, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x1

    const/16 v8, 0x8

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f03002a

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    const-string v5, "GPRS EN"

    const-string v6, "onCreate"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v9, p0, Lcom/mediatek/engineermode/GPRS;->mAlive:Z

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mPhone:Lcom/android/internal/telephony/Phone;

    new-instance v5, Landroid/widget/ArrayAdapter;

    const v6, 0x1090008

    invoke-direct {v5, p0, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v6, 0x1090009

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x1

    :goto_0
    const/16 v5, 0xf

    if-ge v1, v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PDP Context "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const-string v6, "PDP Context 30"

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const-string v6, "PDP Context 31"

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const v5, 0x7f0b0106

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim1:Landroid/widget/Button;

    const v5, 0x7f0b0107

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim2:Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim1:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim2:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    const-string v5, "GPRS EN"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Default IMEI:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v5, 0x7f0b0108

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f0b0109

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnImei:Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnImei:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnImei:Landroid/widget/Button;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f0b010e

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttached:Landroid/widget/Button;

    const v5, 0x7f0b010f

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetached:Landroid/widget/Button;

    const v5, 0x7f0b0115

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttachedContinue:Landroid/widget/Button;

    const v5, 0x7f0b0116

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetachedContinue:Landroid/widget/Button;

    const v5, 0x7f0b0117

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnNotSpecify:Landroid/widget/Button;

    const v5, 0x7f0b0110

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnFastDormancy:Landroid/widget/Button;

    const v5, 0x7f0b0111

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnConfigFD:Landroid/widget/Button;

    const v5, 0x7f0b011c

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioGroup;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mRaGpPDPSelect:Landroid/widget/RadioGroup;

    const v5, 0x7f0b0120

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioGroup;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mRaGpUsageSelect:Landroid/widget/RadioGroup;

    const v5, 0x7f0b0123

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSPinnerPDPContext:Landroid/widget/Spinner;

    const v5, 0x7f0b0124

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnActivate:Landroid/widget/Button;

    const v5, 0x7f0b0125

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDeactivate:Landroid/widget/Button;

    const v5, 0x7f0b0126

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditDataLen:Landroid/widget/EditText;

    const v5, 0x7f0b0127

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSendData:Landroid/widget/Button;

    const v5, 0x7f0b0118

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditGprsFdOnTimer:Landroid/widget/EditText;

    const v5, 0x7f0b011a

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditGprsFdOffTimer:Landroid/widget/EditText;

    const v5, 0x7f0b0119

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSendFdOnTimer:Landroid/widget/Button;

    const v5, 0x7f0b011b

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSendFdOffTimer:Landroid/widget/Button;

    const-string v5, "persist.radio.fd.counter"

    const-string v6, "20"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditGprsFdOnTimer:Landroid/widget/EditText;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "persist.radio.fd.off.counter"

    const-string v6, "20"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditGprsFdOffTimer:Landroid/widget/EditText;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f0b0112

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioGroup;

    iput-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mGprstAttachSelect:Landroid/widget/RadioGroup;

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttached:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetached:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnAttachedContinue:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDetachedContinue:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnNotSpecify:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnFastDormancy:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnConfigFD:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSPinnerPDPContext:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/GPRS;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnActivate:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnDeactivate:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSendData:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSendFdOnTimer:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSendFdOffTimer:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mSPinnerPDPContext:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/GPRS$2;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/GPRS$2;-><init>(Lcom/mediatek/engineermode/GPRS;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mGprstAttachSelect:Landroid/widget/RadioGroup;

    new-instance v6, Lcom/mediatek/engineermode/GPRS$3;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/GPRS$3;-><init>(Lcom/mediatek/engineermode/GPRS;)V

    invoke-virtual {v5, v6}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mRaGpPDPSelect:Landroid/widget/RadioGroup;

    new-instance v6, Lcom/mediatek/engineermode/GPRS$4;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/GPRS$4;-><init>(Lcom/mediatek/engineermode/GPRS;)V

    invoke-virtual {v5, v6}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mRaGpUsageSelect:Landroid/widget/RadioGroup;

    new-instance v6, Lcom/mediatek/engineermode/GPRS$5;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/GPRS$5;-><init>(Lcom/mediatek/engineermode/GPRS;)V

    invoke-virtual {v5, v6}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mRaGpPDPSelect:Landroid/widget/RadioGroup;

    const v6, 0x7f0b011d

    invoke-virtual {v5, v6}, Landroid/widget/RadioGroup;->check(I)V

    const-string v5, "persist.radio.gprs.attach.type"

    invoke-static {v5, v9}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v9, :cond_2

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mGprstAttachSelect:Landroid/widget/RadioGroup;

    const v6, 0x7f0b0113

    invoke-virtual {v5, v6}, Landroid/widget/RadioGroup;->check(I)V

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/GPRS;->showDefaultSim()V

    const-string v5, "ro.build.type"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GPRS EN"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "USE MODE"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "user"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim1:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnSim2:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mBtnImei:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mEditImeiValue:Landroid/widget/EditText;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v5, p0, Lcom/mediatek/engineermode/GPRS;->mGprstAttachSelect:Landroid/widget/RadioGroup;

    const v6, 0x7f0b0114

    invoke-virtual {v5, v6}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/GPRS;->mAlive:Z

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/GPRS;->updateAttachModeMMI()V

    return-void
.end method
