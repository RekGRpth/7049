.class public final Lcom/mediatek/engineermode/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final af_mode_dialog:I = 0x7f06002b

.field public static final anti_flicker_dialog:I = 0x7f06002e

.field public static final audio_functions:I = 0x7f060023

.field public static final bt_baudrate:I = 0x7f060002

.field public static final bt_ble_test_pattern:I = 0x7f060004

.field public static final bt_chip_eco:I = 0x7f060001

.field public static final bt_chip_id:I = 0x7f060000

.field public static final bt_uart:I = 0x7f060003

.field public static final camera_items:I = 0x7f06002a

.field public static final capture_mode_dialog:I = 0x7f06002c

.field public static final command_data:I = 0x7f060036

.field public static final device_mgr_entries:I = 0x7f06001f

.field public static final device_mgr_values:I = 0x7f060020

.field public static final gsm_umts_network_preferences_choices:I = 0x7f06001d

.field public static final gsm_umts_network_preferences_values:I = 0x7f06001e

.field public static final hopping_bit:I = 0x7f060034

.field public static final hopping_time:I = 0x7f060035

.field public static final host_data:I = 0x7f060033

.field public static final host_type:I = 0x7f060032

.field public static final iso_dialog:I = 0x7f06002f

.field public static final log_filter_tags:I = 0x7f060017

.field public static final mPreferredNetworkLabels:I = 0x7f060030

.field public static final mTddPreferredNetworkLabels:I = 0x7f060031

.field public static final mode_fir:I = 0x7f060028

.field public static final mode_level:I = 0x7f060027

.field public static final mode_type0:I = 0x7f060024

.field public static final mode_type1:I = 0x7f060025

.field public static final mode_type2:I = 0x7f060026

.field public static final modem_test_cta_options:I = 0x7f06001a

.field public static final modem_test_iot_options:I = 0x7f06001c

.field public static final modem_test_operator_options:I = 0x7f06001b

.field public static final msdc_select:I = 0x7f060037

.field public static final nsrx_Pocket_type:I = 0x7f060019

.field public static final nsrx_pattern:I = 0x7f060018

.field public static final raw_type_dialog:I = 0x7f06002d

.field public static final rx_Pocket_type:I = 0x7f060005

.field public static final speech_enhance_mode:I = 0x7f060029

.field public static final tvout_page_list:I = 0x7f060022

.field public static final tvout_system_list:I = 0x7f060021

.field public static final tx_Pocket_type:I = 0x7f060008

.field public static final tx_channels:I = 0x7f060007

.field public static final tx_pattern:I = 0x7f060006

.field public static final working_mode_audio_channels:I = 0x7f060009

.field public static final working_mode_audio_channels_values:I = 0x7f06000a

.field public static final working_mode_multiplex_level:I = 0x7f06000f

.field public static final working_mode_multiplex_level_values:I = 0x7f060010

.field public static final working_mode_terminal_type:I = 0x7f060015

.field public static final working_mode_terminal_type_values:I = 0x7f060016

.field public static final working_mode_use_wnsrp:I = 0x7f060013

.field public static final working_mode_use_wnsrp_values:I = 0x7f060014

.field public static final working_mode_video_channels:I = 0x7f06000b

.field public static final working_mode_video_channels_reverse:I = 0x7f06000d

.field public static final working_mode_video_channels_reverse_values:I = 0x7f06000e

.field public static final working_mode_video_channels_values:I = 0x7f06000c

.field public static final working_mode_video_codec_preference:I = 0x7f060011

.field public static final working_mode_video_codec_preference_values:I = 0x7f060012


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
