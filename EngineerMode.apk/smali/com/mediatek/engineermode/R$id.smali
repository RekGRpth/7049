.class public final Lcom/mediatek/engineermode/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Activate:I = 0x7f0b0124

.field public static final Attached:I = 0x7f0b010e

.field public static final Audio_A2DPOtpt_Dump:I = 0x7f0b0004

.field public static final Audio_CTM4WAYLogger_Enable:I = 0x7f0b0023

.field public static final Audio_CTM4WAYLogger_EnableText:I = 0x7f0b0022

.field public static final Audio_DebugInfo_Button:I = 0x7f0b000b

.field public static final Audio_DebugInfo_EditText:I = 0x7f0b000a

.field public static final Audio_DebugInfo_ParameterText:I = 0x7f0b0007

.field public static final Audio_DebugInfo_Spinner:I = 0x7f0b0008

.field public static final Audio_DebugInfo_ValueText:I = 0x7f0b0009

.field public static final Audio_Fir_Spinner:I = 0x7f0b000e

.field public static final Audio_Fir_Title:I = 0x7f0b000d

.field public static final Audio_Level:I = 0x7f0b0010

.field public static final Audio_MaxVol_Edit:I = 0x7f0b0014

.field public static final Audio_MaxVol_Set:I = 0x7f0b0015

.field public static final Audio_MixerBuf_Dump:I = 0x7f0b0002

.field public static final Audio_ModeSetting:I = 0x7f0b000f

.field public static final Audio_ModeSetting_Button:I = 0x7f0b0013

.field public static final Audio_ModeSetting_EditText:I = 0x7f0b0012

.field public static final Audio_ModeSetting_TextView:I = 0x7f0b0011

.field public static final Audio_SpEnhancement_Button:I = 0x7f0b001a

.field public static final Audio_SpEnhancement_EditText:I = 0x7f0b0019

.field public static final Audio_SpEnhancement_ModeType:I = 0x7f0b0016

.field public static final Audio_SpEnhancement_ParaType:I = 0x7f0b0017

.field public static final Audio_SpEnhancement_TextView:I = 0x7f0b0018

.field public static final Audio_SpeechLogger_EPL:I = 0x7f0b001f

.field public static final Audio_SpeechLogger_Enable:I = 0x7f0b001d

.field public static final Audio_SpeechLogger_Normalvm:I = 0x7f0b0020

.field public static final Audio_StrmInpt_Dump:I = 0x7f0b0005

.field public static final Audio_StrmOtpt_Dump:I = 0x7f0b0001

.field public static final Audio_TrackBuf_Dump:I = 0x7f0b0003

.field public static final Audio_VOIPLogger_Enable:I = 0x7f0b0021

.field public static final Audio_View1:I = 0x7f0b0024

.field public static final BLEAddToWhiteList:I = 0x7f0b008f

.field public static final BLEAddressType:I = 0x7f0b0065

.field public static final BLEAddressTypeSpinner:I = 0x7f0b008e

.field public static final BLEAdvertise:I = 0x7f0b0062

.field public static final BLEAdvertiseData:I = 0x7f0b006e

.field public static final BLEAdvertiseSet:I = 0x7f0b006f

.field public static final BLEAdvertiseStart:I = 0x7f0b0070

.field public static final BLEAdvertiseStop:I = 0x7f0b0071

.field public static final BLEAdvertiseTypeSpinner:I = 0x7f0b0066

.field public static final BLEChannel:I = 0x7f0b009a

.field public static final BLEChannel37Checkbox:I = 0x7f0b006a

.field public static final BLEChannel38Checkbox:I = 0x7f0b006b

.field public static final BLEChannel39Checkbox:I = 0x7f0b006c

.field public static final BLEChannelSpinner:I = 0x7f0b009b

.field public static final BLEClearWhiteList:I = 0x7f0b0091

.field public static final BLEContiniousTx:I = 0x7f0b009e

.field public static final BLEDirectAddress:I = 0x7f0b0069

.field public static final BLEDirectAddressTypeSpinner:I = 0x7f0b0068

.field public static final BLEFilterPolicySpinner:I = 0x7f0b006d

.field public static final BLEHCIReset:I = 0x7f0b0092

.field public static final BLEHopping:I = 0x7f0b0098

.field public static final BLEHoppingSingle:I = 0x7f0b0097

.field public static final BLEInitiate:I = 0x7f0b0072

.field public static final BLEInitiateFilterSpinner:I = 0x7f0b0075

.field public static final BLEInitiateIntervalMax:I = 0x7f0b007a

.field public static final BLEInitiateIntervalMin:I = 0x7f0b0079

.field public static final BLEInitiateOwnAddressTypeSpinner:I = 0x7f0b0077

.field public static final BLEInitiatePeerAddressTypeSpinner:I = 0x7f0b0076

.field public static final BLEInitiateScanInterval:I = 0x7f0b0073

.field public static final BLEInitiateScanWindow:I = 0x7f0b0074

.field public static final BLEInitiateStart:I = 0x7f0b007f

.field public static final BLEInitiateStop:I = 0x7f0b0080

.field public static final BLEIntervalMax:I = 0x7f0b0064

.field public static final BLEIntervalMin:I = 0x7f0b0063

.field public static final BLELatency:I = 0x7f0b007b

.field public static final BLEMACAddress:I = 0x7f0b008d

.field public static final BLEMaxCELength:I = 0x7f0b007e

.field public static final BLEMinCELength:I = 0x7f0b007d

.field public static final BLEOwnAddressTypeSpinner:I = 0x7f0b0067

.field public static final BLEPattern:I = 0x7f0b009c

.field public static final BLEPatternSpinner:I = 0x7f0b009d

.field public static final BLEPeerAddress:I = 0x7f0b0078

.field public static final BLERemoveWhiteList:I = 0x7f0b0090

.field public static final BLEResult_Text:I = 0x7f0b00a1

.field public static final BLEScan:I = 0x7f0b0081

.field public static final BLEScanFilterDuplicate:I = 0x7f0b0087

.field public static final BLEScanInterval:I = 0x7f0b0084

.field public static final BLEScanOwnAddressTypeSpinner:I = 0x7f0b0086

.field public static final BLEScanResponseData:I = 0x7f0b0088

.field public static final BLEScanSet:I = 0x7f0b0089

.field public static final BLEScanStart:I = 0x7f0b008a

.field public static final BLEScanStop:I = 0x7f0b008b

.field public static final BLEScanType:I = 0x7f0b0082

.field public static final BLEScanTypeSpinner:I = 0x7f0b0083

.field public static final BLEScanWindow:I = 0x7f0b0085

.field public static final BLESingle:I = 0x7f0b0099

.field public static final BLEStart:I = 0x7f0b009f

.field public static final BLEStop:I = 0x7f0b00a0

.field public static final BLESupervisionTimeout:I = 0x7f0b007c

.field public static final BLETestMode:I = 0x7f0b0093

.field public static final BLETestModeRx:I = 0x7f0b0096

.field public static final BLETestModeTx:I = 0x7f0b0095

.field public static final BLETxRx:I = 0x7f0b0094

.field public static final BLEWhiteList:I = 0x7f0b008c

.field public static final BTRxTitle:I = 0x7f0b01d7

.field public static final BTTestMode_edit:I = 0x7f0b0218

.field public static final BTTestMode_edit_Text:I = 0x7f0b0217

.field public static final BandSel_Btn_Set:I = 0x7f0b004d

.field public static final BandSel_GSM_DCS1800:I = 0x7f0b0040

.field public static final BandSel_GSM_EGSM900:I = 0x7f0b003f

.field public static final BandSel_GSM_GSM850:I = 0x7f0b0042

.field public static final BandSel_GSM_PCS1900:I = 0x7f0b0041

.field public static final BandSel_UMTS_BAND_I:I = 0x7f0b0043

.field public static final BandSel_UMTS_BAND_II:I = 0x7f0b0044

.field public static final BandSel_UMTS_BAND_III:I = 0x7f0b0045

.field public static final BandSel_UMTS_BAND_IV:I = 0x7f0b0046

.field public static final BandSel_UMTS_BAND_IX:I = 0x7f0b004b

.field public static final BandSel_UMTS_BAND_V:I = 0x7f0b0047

.field public static final BandSel_UMTS_BAND_VI:I = 0x7f0b0048

.field public static final BandSel_UMTS_BAND_VII:I = 0x7f0b0049

.field public static final BandSel_UMTS_BAND_VIII:I = 0x7f0b004a

.field public static final BandSel_UMTS_BAND_X:I = 0x7f0b004c

.field public static final Baseband_Addr:I = 0x7f0b0050

.field public static final Baseband_Info:I = 0x7f0b0056

.field public static final Baseband_Len:I = 0x7f0b0053

.field public static final Baseband_Read:I = 0x7f0b0054

.field public static final Baseband_Val:I = 0x7f0b0051

.field public static final Baseband_Write:I = 0x7f0b0055

.field public static final Baseband_addr_text:I = 0x7f0b004e

.field public static final Baseband_len_text:I = 0x7f0b0052

.field public static final Baseband_val_text:I = 0x7f0b004f

.field public static final Button01:I = 0x7f0b01b0

.field public static final Button02:I = 0x7f0b01b1

.field public static final ChannelsSpinner:I = 0x7f0b0222

.field public static final Check_Enable:I = 0x7f0b01c9

.field public static final ConfigFD:I = 0x7f0b0111

.field public static final ContextNumber:I = 0x7f0b0123

.field public static final CpuFreq_BtnCurrentTest:I = 0x7f0b00d0

.field public static final CpuFreq_BtnStartTest:I = 0x7f0b00ce

.field public static final CpuFreq_BtnStopTest:I = 0x7f0b00cf

.field public static final CpuFreq_Info:I = 0x7f0b00d2

.field public static final DataLength:I = 0x7f0b0126

.field public static final Deactivate:I = 0x7f0b0125

.field public static final DefSIM:I = 0x7f0b010b

.field public static final DefSIMText:I = 0x7f0b010a

.field public static final Detached:I = 0x7f0b010f

.field public static final Display_Edit_Value:I = 0x7f0b00e8

.field public static final Display_lcd_Text:I = 0x7f0b00ea

.field public static final Display_lcd_off:I = 0x7f0b00ec

.field public static final Display_lcd_on:I = 0x7f0b00eb

.field public static final Display_lcm_Text:I = 0x7f0b00ed

.field public static final Display_lcm_off:I = 0x7f0b00ef

.field public static final Display_lcm_on:I = 0x7f0b00ee

.field public static final Display_set:I = 0x7f0b00e9

.field public static final Display_set_Text:I = 0x7f0b00e7

.field public static final Dump_Audio_DebgInfo:I = 0x7f0b0006

.field public static final Dump_Speech_DbgInfo:I = 0x7f0b0025

.field public static final EINT_edit:I = 0x7f0b00f2

.field public static final EINT_edit_Text:I = 0x7f0b00f1

.field public static final EINT_polarity:I = 0x7f0b00f7

.field public static final EINT_polarity_Text:I = 0x7f0b00f6

.field public static final EINT_query:I = 0x7f0b00f3

.field public static final EINT_sensitivity:I = 0x7f0b00f5

.field public static final EINT_sensitivity_Text:I = 0x7f0b00f4

.field public static final EditText01:I = 0x7f0b01a5

.field public static final EditText02:I = 0x7f0b01a7

.field public static final EditText03:I = 0x7f0b01a9

.field public static final EditText04:I = 0x7f0b01ab

.field public static final EditText05:I = 0x7f0b01ad

.field public static final EditText06:I = 0x7f0b01af

.field public static final FastDormancy:I = 0x7f0b0110

.field public static final FirstPDP:I = 0x7f0b011d

.field public static final FrameLayout01:I = 0x7f0b01b6

.field public static final FreqText:I = 0x7f0b0223

.field public static final GPIO_Data_High:I = 0x7f0b0104

.field public static final GPIO_Data_Low:I = 0x7f0b0105

.field public static final GPIO_Data_Text:I = 0x7f0b0103

.field public static final GPIO_Direction_In:I = 0x7f0b0101

.field public static final GPIO_Direction_Out:I = 0x7f0b0102

.field public static final GPIO_Direction_Text:I = 0x7f0b0100

.field public static final GPIO_Edit_Value:I = 0x7f0b00fe

.field public static final GPIO_Setno_Text:I = 0x7f0b00fd

.field public static final GPIO_Setnomax_Text:I = 0x7f0b00ff

.field public static final GprsAlwaysAttach:I = 0x7f0b0113

.field public static final GprsAttachType:I = 0x7f0b0112

.field public static final GprsFdOffTimer:I = 0x7f0b011a

.field public static final GprsFdOnTimer:I = 0x7f0b0118

.field public static final GprsWhenNeeded:I = 0x7f0b0114

.field public static final IMEI:I = 0x7f0b0109

.field public static final IMEI_VALUE:I = 0x7f0b0108

.field public static final LinearLayout:I = 0x7f0b000c

.field public static final LinerLayout_pmu_info_text:I = 0x7f0b01b7

.field public static final LinerLayout_pmu_reg:I = 0x7f0b01b9

.field public static final ListView_Audio:I = 0x7f0b0000

.field public static final ListView_Camera:I = 0x7f0b00b4

.field public static final ListView_Io:I = 0x7f0b014a

.field public static final ListView_Power:I = 0x7f0b01b5

.field public static final ListView_SimSelect:I = 0x7f0b01ec

.field public static final ListView_TP_Verification:I = 0x7f0b021f

.field public static final ListView_TVOut:I = 0x7f0b0220

.field public static final ListView_TouchScreen:I = 0x7f0b021e

.field public static final ListView_USB:I = 0x7f0b0228

.field public static final ListView_WiFi:I = 0x7f0b0246

.field public static final ListView_dualtalk_networkinfo:I = 0x7f0b00f0

.field public static final ListView_internalmainmenu:I = 0x7f0b0149

.field public static final ListView_msdcSelect:I = 0x7f0b016e

.field public static final ListView_syslogmenu:I = 0x7f0b01f1

.field public static final Log_Record:I = 0x7f0b0061

.field public static final Log_Record_Interval:I = 0x7f0b0060

.field public static final MSDC_Clk_pu_spinner:I = 0x7f0b0196

.field public static final MSDC_Current_cmd_spininer:I = 0x7f0b0165

.field public static final MSDC_Current_data_spininer:I = 0x7f0b0166

.field public static final MSDC_Get:I = 0x7f0b0167

.field public static final MSDC_HopSet_Get:I = 0x7f0b016c

.field public static final MSDC_HopSet_HOST_sppiner:I = 0x7f0b0169

.field public static final MSDC_HopSet_Set:I = 0x7f0b016d

.field public static final MSDC_Host_spininer:I = 0x7f0b0164

.field public static final MSDC_Set:I = 0x7f0b0168

.field public static final MSDC_clk_pd_spinner:I = 0x7f0b0197

.field public static final MSDC_cmd_pd_spinner:I = 0x7f0b0199

.field public static final MSDC_cmd_pu_spinner:I = 0x7f0b0198

.field public static final MSDC_data_pd_spinner:I = 0x7f0b019b

.field public static final MSDC_data_pu_spinner:I = 0x7f0b019a

.field public static final MSDC_hopping_bit_spinner:I = 0x7f0b016a

.field public static final MSDC_hopping_time_spinner:I = 0x7f0b016b

.field public static final Modem_dcm:I = 0x7f0b0158

.field public static final Modem_memorydump:I = 0x7f0b015b

.field public static final Modem_sleepmode:I = 0x7f0b0155

.field public static final NEW_MSDC_Get:I = 0x7f0b019c

.field public static final NEW_MSDC_HOST_sppiner:I = 0x7f0b0195

.field public static final NEW_MSDC_Set:I = 0x7f0b019d

.field public static final NSRX_PatternSpinner:I = 0x7f0b01cb

.field public static final NSRX_PocketTypeSpinner:I = 0x7f0b01cd

.field public static final NSRX_StartTest:I = 0x7f0b01d2

.field public static final NSRX_StrBitErrRate:I = 0x7f0b01d6

.field public static final NSRX_StrPackByteCnt:I = 0x7f0b01d5

.field public static final NSRX_StrPackCnt:I = 0x7f0b01d3

.field public static final NSRX_StrPackErrRate:I = 0x7f0b01d4

.field public static final NSRX_editFrequency:I = 0x7f0b01cf

.field public static final NSRX_editTesterAddr:I = 0x7f0b01d1

.field public static final NetworkInfo_3GCsceEMServCellSStatusInd:I = 0x7f0b0183

.field public static final NetworkInfo_3GCsceEmInfoMultiPlmn:I = 0x7f0b0184

.field public static final NetworkInfo_3GHandoverSequenceIndStuct:I = 0x7f0b018c

.field public static final NetworkInfo_3GMemeEmInfoHServCellInd:I = 0x7f0b0187

.field public static final NetworkInfo_3GMemeEmInfoUmtsCellStatus:I = 0x7f0b0189

.field public static final NetworkInfo_3GMemeEmPeriodicBlerReportInd:I = 0x7f0b0185

.field public static final NetworkInfo_3GMmEmInfo:I = 0x7f0b0181

.field public static final NetworkInfo_3GSlceEmPsDataRateStatusInd:I = 0x7f0b018a

.field public static final NetworkInfo_3GTcmMmiEmInfo:I = 0x7f0b0182

.field public static final NetworkInfo_3GUl2EmAdmPoolStatusIndStruct:I = 0x7f0b018d

.field public static final NetworkInfo_3GUl2EmPeriodicBlerReportInd:I = 0x7f0b0191

.field public static final NetworkInfo_3GUl2EmPsDataRateStatusIndStruct:I = 0x7f0b018e

.field public static final NetworkInfo_3GUl2EmUrlcEventStatusIndStruct:I = 0x7f0b0190

.field public static final NetworkInfo_3GUrrUmtsSrncId:I = 0x7f0b0186

.field public static final NetworkInfo_3Gul2EmHsdschReconfigStatusIndStruct:I = 0x7f0b018f

.field public static final NetworkInfo_BLK:I = 0x7f0b017c

.field public static final NetworkInfo_Ca:I = 0x7f0b0178

.field public static final NetworkInfo_Cell:I = 0x7f0b0171

.field public static final NetworkInfo_Ch:I = 0x7f0b0172

.field public static final NetworkInfo_Control:I = 0x7f0b0179

.field public static final NetworkInfo_Ctrl:I = 0x7f0b0173

.field public static final NetworkInfo_GPRS:I = 0x7f0b017e

.field public static final NetworkInfo_Info:I = 0x7f0b0194

.field public static final NetworkInfo_LAI:I = 0x7f0b0175

.field public static final NetworkInfo_MI:I = 0x7f0b017b

.field public static final NetworkInfo_Meas:I = 0x7f0b0177

.field public static final NetworkInfo_PageDown:I = 0x7f0b0193

.field public static final NetworkInfo_PageUp:I = 0x7f0b0192

.field public static final NetworkInfo_RACH:I = 0x7f0b0174

.field public static final NetworkInfo_Radio:I = 0x7f0b0176

.field public static final NetworkInfo_SI2Q:I = 0x7f0b017a

.field public static final NetworkInfo_TBF:I = 0x7f0b017d

.field public static final NetworkInfo_xGCsceEMNeighCellSStatusInd:I = 0x7f0b017f

.field public static final PDPSelect:I = 0x7f0b011c

.field public static final PatternSpinner:I = 0x7f0b0221

.field public static final PockLenText:I = 0x7f0b0226

.field public static final PocketTypeSpinner:I = 0x7f0b0225

.field public static final Primary:I = 0x7f0b0121

.field public static final RadioGroup1:I = 0x7f0b001e

.field public static final RxAccessCode:I = 0x7f0b01e0

.field public static final RxChannel:I = 0x7f0b01da

.field public static final RxChannelSpinner:I = 0x7f0b01db

.field public static final RxDataLength:I = 0x7f0b01e1

.field public static final RxFrequency:I = 0x7f0b01dc

.field public static final RxPacketType:I = 0x7f0b01dd

.field public static final RxPacketTypeSpinner:I = 0x7f0b01de

.field public static final RxPattern:I = 0x7f0b01d8

.field public static final RxPatternSpinner:I = 0x7f0b01d9

.field public static final RxPollPeriod:I = 0x7f0b01df

.field public static final RxPowerControl:I = 0x7f0b01e2

.field public static final RxPowerControlSpinner:I = 0x7f0b01e3

.field public static final RxSetPwr:I = 0x7f0b01e4

.field public static final RxStart:I = 0x7f0b01e6

.field public static final RxStop:I = 0x7f0b01e7

.field public static final RxWritten:I = 0x7f0b01e5

.field public static final SIM1Enabled:I = 0x7f0b010c

.field public static final SIM2Enabled:I = 0x7f0b010d

.field public static final SSPDebugModeCb:I = 0x7f0b01ee

.field public static final SSPDebugModetxv:I = 0x7f0b01ed

.field public static final ScrollView01:I = 0x7f0b00d1

.field public static final SecondPDP:I = 0x7f0b011e

.field public static final Secondary:I = 0x7f0b0122

.field public static final SendData:I = 0x7f0b0127

.field public static final SendFdOffTimer:I = 0x7f0b011b

.field public static final SendFdOnTimer:I = 0x7f0b0119

.field public static final Sim1:I = 0x7f0b0106

.field public static final Sim2:I = 0x7f0b0107

.field public static final SpacerTop:I = 0x7f0b00ca

.field public static final TDD_Btn_Set:I = 0x7f0b0214

.field public static final TDD_GSM_DCS1800:I = 0x7f0b020b

.field public static final TDD_GSM_EGSM900:I = 0x7f0b020a

.field public static final TDD_GSM_GSM850:I = 0x7f0b020d

.field public static final TDD_GSM_PCS1900:I = 0x7f0b020c

.field public static final TDD_UMTS_BAND_I:I = 0x7f0b020e

.field public static final TDD_UMTS_BAND_II:I = 0x7f0b020f

.field public static final TDD_UMTS_BAND_III:I = 0x7f0b0210

.field public static final TDD_UMTS_BAND_IV:I = 0x7f0b0211

.field public static final TDD_UMTS_BAND_V:I = 0x7f0b0212

.field public static final TDD_UMTS_BAND_VI:I = 0x7f0b0213

.field public static final TableLayout:I = 0x7f0b011f

.field public static final TableLayout01:I = 0x7f0b001b

.field public static final TableLayout02:I = 0x7f0b0037

.field public static final TableRow:I = 0x7f0b0209

.field public static final TableRow01:I = 0x7f0b001c

.field public static final TableRow02:I = 0x7f0b0038

.field public static final TableRow03:I = 0x7f0b01ce

.field public static final TableRow04:I = 0x7f0b01cc

.field public static final TableRow05:I = 0x7f0b01d0

.field public static final TestModeCb:I = 0x7f0b0216

.field public static final TestModetxv:I = 0x7f0b0215

.field public static final TextView01:I = 0x7f0b01a4

.field public static final TextView02:I = 0x7f0b01a6

.field public static final TextView03:I = 0x7f0b01a8

.field public static final TextView04:I = 0x7f0b01aa

.field public static final TextView05:I = 0x7f0b01ac

.field public static final TextView06:I = 0x7f0b01ae

.field public static final TextView_extmd:I = 0x7f0b0203

.field public static final TouchScreen_Settings_Spinner:I = 0x7f0b021a

.field public static final TouchScreen_Settings_TextSet:I = 0x7f0b021d

.field public static final TouchScreen_Settings_TextSpinner:I = 0x7f0b0219

.field public static final TouchScreen_Settings_TextValue:I = 0x7f0b021b

.field public static final TouchScreen_Settings_Value:I = 0x7f0b021c

.field public static final USB_EX_ITEM1_Start_ID:I = 0x7f0b0236

.field public static final USB_EX_ITEM1_Stop_ID:I = 0x7f0b0237

.field public static final USB_EX_ITEM2_Start_ID:I = 0x7f0b0238

.field public static final USB_EX_ITEM2_Stop_ID:I = 0x7f0b0239

.field public static final USB_EX_ITEM3_Start_ID:I = 0x7f0b023a

.field public static final USB_EX_ITEM3_Stop_ID:I = 0x7f0b023b

.field public static final USB_EX_ITEM4_Start_ID:I = 0x7f0b023c

.field public static final USB_EX_ITEM4_Stop_ID:I = 0x7f0b023d

.field public static final USB_EX_ITEM5_Start_ID:I = 0x7f0b023e

.field public static final USB_EX_ITEM5_Stop_ID:I = 0x7f0b023f

.field public static final USB_EX_ITEM6_Start_ID:I = 0x7f0b0240

.field public static final USB_EX_ITEM6_Stop_ID:I = 0x7f0b0241

.field public static final USB_EX_ITEM7_Start_ID:I = 0x7f0b0242

.field public static final USB_EX_ITEM7_Stop_ID:I = 0x7f0b0243

.field public static final USB_IF_Elec_DeSRP_Start_ID:I = 0x7f0b022f

.field public static final USB_IF_Elec_DeSRP_Stop_ID:I = 0x7f0b0230

.field public static final USB_IF_Elec_DeVBUS_Start_ID:I = 0x7f0b022b

.field public static final USB_IF_Elec_DeVBUS_Stop_ID:I = 0x7f0b022c

.field public static final USB_IF_Elec_EnSRP_Start_ID:I = 0x7f0b022d

.field public static final USB_IF_Elec_EnSRP_Stop_ID:I = 0x7f0b022e

.field public static final USB_IF_Elec_EnVBUS_Start_ID:I = 0x7f0b0229

.field public static final USB_IF_Elec_EnVBUS_Stop_ID:I = 0x7f0b022a

.field public static final USB_IF_Proto_AUUT_Start_ID:I = 0x7f0b0231

.field public static final USB_IF_Proto_AUUT_Stop_ID:I = 0x7f0b0232

.field public static final USB_IF_Proto_BUUT_Start_ID:I = 0x7f0b0233

.field public static final USB_IF_Proto_BUUT_Stop_ID:I = 0x7f0b0234

.field public static final USB_IF_Proto_BUUT_TD5_9_ID:I = 0x7f0b0235

.field public static final UsageSelect:I = 0x7f0b0120

.field public static final VTAutoAnswer_Edit:I = 0x7f0b0244

.field public static final VTAutoAnswer_Set_Btn:I = 0x7f0b0245

.field public static final View_2G:I = 0x7f0b0170

.field public static final View_3G_COMMON:I = 0x7f0b0180

.field public static final View_3G_FDD:I = 0x7f0b0188

.field public static final View_3G_TDD:I = 0x7f0b018b

.field public static final WiFi_ALC:I = 0x7f0b0289

.field public static final WiFi_ALC_Rx:I = 0x7f0b0270

.field public static final WiFi_Bandwidth:I = 0x7f0b0273

.field public static final WiFi_Bandwidth_Spinner:I = 0x7f0b0274

.field public static final WiFi_Channel_Spinner:I = 0x7f0b027a

.field public static final WiFi_Channel_Text:I = 0x7f0b0268

.field public static final WiFi_DPD_Calibration:I = 0x7f0b024b

.field public static final WiFi_DPD_Import:I = 0x7f0b024e

.field public static final WiFi_DPD_Info:I = 0x7f0b024f

.field public static final WiFi_DPD_Read:I = 0x7f0b024c

.field public static final WiFi_DPD_Save:I = 0x7f0b024d

.field public static final WiFi_DPD_Text:I = 0x7f0b024a

.field public static final WiFi_FCS_Content:I = 0x7f0b026b

.field public static final WiFi_FCS_Text:I = 0x7f0b026a

.field public static final WiFi_Go:I = 0x7f0b028a

.field public static final WiFi_Go_Rx:I = 0x7f0b0271

.field public static final WiFi_Guard_Interval_Spinner:I = 0x7f0b028f

.field public static final WiFi_MCR_Addr_Content:I = 0x7f0b0263

.field public static final WiFi_MCR_Addr_Text:I = 0x7f0b0262

.field public static final WiFi_MCR_ReadBtn:I = 0x7f0b0266

.field public static final WiFi_MCR_Text:I = 0x7f0b0261

.field public static final WiFi_MCR_Value_Content:I = 0x7f0b0265

.field public static final WiFi_MCR_Value_Text:I = 0x7f0b0264

.field public static final WiFi_MCR_WriteBtn:I = 0x7f0b0267

.field public static final WiFi_Misc_ClearBtn:I = 0x7f0b0278

.field public static final WiFi_Misc_GoBtn:I = 0x7f0b0276

.field public static final WiFi_Misc_StopBtn:I = 0x7f0b0277

.field public static final WiFi_Misc_TempShow:I = 0x7f0b0279

.field public static final WiFi_Misc_Text:I = 0x7f0b0275

.field public static final WiFi_Mode_Spinner:I = 0x7f0b0284

.field public static final WiFi_Mode_Text:I = 0x7f0b0283

.field public static final WiFi_PER_Content:I = 0x7f0b026f

.field public static final WiFi_PER_Text:I = 0x7f0b026e

.field public static final WiFi_Pkt_Edit:I = 0x7f0b027c

.field public static final WiFi_Pkt_Text:I = 0x7f0b027b

.field public static final WiFi_Pktcnt_Edit:I = 0x7f0b027e

.field public static final WiFi_Pktcnt_Text:I = 0x7f0b027d

.field public static final WiFi_Preamble:I = 0x7f0b028d

.field public static final WiFi_Preamble_Spinner:I = 0x7f0b028e

.field public static final WiFi_RX_Channel_Spinner:I = 0x7f0b0269

.field public static final WiFi_Rate_Spinner:I = 0x7f0b0282

.field public static final WiFi_Rate_Text:I = 0x7f0b0281

.field public static final WiFi_Read_String:I = 0x7f0b025e

.field public static final WiFi_Read_Word:I = 0x7f0b0255

.field public static final WiFi_Rx_Content:I = 0x7f0b026d

.field public static final WiFi_Rx_Text:I = 0x7f0b026c

.field public static final WiFi_ShowWindow:I = 0x7f0b0260

.field public static final WiFi_Stop:I = 0x7f0b028b

.field public static final WiFi_Stop_Rx:I = 0x7f0b0272

.field public static final WiFi_StringAccess_Text:I = 0x7f0b0257

.field public static final WiFi_Tx_Edit:I = 0x7f0b0280

.field public static final WiFi_Tx_Gain_Edit:I = 0x7f0b028c

.field public static final WiFi_Tx_Text:I = 0x7f0b027f

.field public static final WiFi_WordAccess_Text:I = 0x7f0b0250

.field public static final WiFi_Write_String:I = 0x7f0b025f

.field public static final WiFi_Write_Word:I = 0x7f0b0256

.field public static final WiFi_XtalTrim_Edit:I = 0x7f0b0286

.field public static final WiFi_XtalTrim_Read:I = 0x7f0b0287

.field public static final WiFi_XtalTrim_Text:I = 0x7f0b0285

.field public static final WiFi_XtalTrim_Write:I = 0x7f0b0288

.field public static final WiFi_addr_Content:I = 0x7f0b0252

.field public static final WiFi_addr_Content_String:I = 0x7f0b0259

.field public static final WiFi_addr_Text:I = 0x7f0b0251

.field public static final WiFi_addr_Text_String:I = 0x7f0b0258

.field public static final WiFi_length_Content_String:I = 0x7f0b025b

.field public static final WiFi_length_Text_String:I = 0x7f0b025a

.field public static final WiFi_value_Content:I = 0x7f0b0254

.field public static final WiFi_value_Content_String:I = 0x7f0b025d

.field public static final WiFi_value_Text:I = 0x7f0b0253

.field public static final WiFi_value_Text_String:I = 0x7f0b025c

.field public static final address6575_text:I = 0x7f0b01bb

.field public static final address_text:I = 0x7f0b01c2

.field public static final always_mode_continue:I = 0x7f0b0115

.field public static final apmcu_btn:I = 0x7f0b013a

.field public static final apmcu_ca9_result:I = 0x7f0b0138

.field public static final apmcu_ca9_result_1:I = 0x7f0b0139

.field public static final apmcu_ca9_test:I = 0x7f0b0137

.field public static final apmcu_l2c_result:I = 0x7f0b0133

.field public static final apmcu_l2c_test:I = 0x7f0b0132

.field public static final apmcu_loopcount:I = 0x7f0b0131

.field public static final apmcu_neon_result:I = 0x7f0b0135

.field public static final apmcu_neon_result_1:I = 0x7f0b0136

.field public static final apmcu_neon_test:I = 0x7f0b0134

.field public static final apmcu_result:I = 0x7f0b013b

.field public static final audio_mal_supported_format_spinner:I = 0x7f0b019f

.field public static final autoanswer_switch:I = 0x7f0b0026

.field public static final band:I = 0x7f0b0027

.field public static final bandmodesim1_Btn_Set:I = 0x7f0b0036

.field public static final bandmodesim1_GSM_DCS1800:I = 0x7f0b0029

.field public static final bandmodesim1_GSM_EGSM900:I = 0x7f0b0028

.field public static final bandmodesim1_GSM_GSM850:I = 0x7f0b002b

.field public static final bandmodesim1_GSM_PCS1900:I = 0x7f0b002a

.field public static final bandmodesim1_UMTS_BAND_I:I = 0x7f0b002c

.field public static final bandmodesim1_UMTS_BAND_II:I = 0x7f0b002d

.field public static final bandmodesim1_UMTS_BAND_III:I = 0x7f0b002e

.field public static final bandmodesim1_UMTS_BAND_IV:I = 0x7f0b002f

.field public static final bandmodesim1_UMTS_BAND_IX:I = 0x7f0b0034

.field public static final bandmodesim1_UMTS_BAND_V:I = 0x7f0b0030

.field public static final bandmodesim1_UMTS_BAND_VI:I = 0x7f0b0031

.field public static final bandmodesim1_UMTS_BAND_VII:I = 0x7f0b0032

.field public static final bandmodesim1_UMTS_BAND_VIII:I = 0x7f0b0033

.field public static final bandmodesim1_UMTS_BAND_X:I = 0x7f0b0035

.field public static final bandmodesim2_Btn_Set:I = 0x7f0b003d

.field public static final bandmodesim2_GSM_DCS1800:I = 0x7f0b003a

.field public static final bandmodesim2_GSM_EGSM900:I = 0x7f0b0039

.field public static final bandmodesim2_GSM_GSM850:I = 0x7f0b003c

.field public static final bandmodesim2_GSM_PCS1900:I = 0x7f0b003b

.field public static final battery_charge_info_text:I = 0x7f0b0057

.field public static final button1:I = 0x7f0b00b3

.field public static final camera_preview:I = 0x7f0b00b7

.field public static final capture_btn:I = 0x7f0b00b9

.field public static final cfu_default_radio:I = 0x7f0b00bb

.field public static final cfu_off_radio:I = 0x7f0b00bd

.field public static final cfu_on_radio:I = 0x7f0b00bc

.field public static final cfu_radio_group:I = 0x7f0b00ba

.field public static final cfu_set_button:I = 0x7f0b00be

.field public static final chipId:I = 0x7f0b00a4

.field public static final clockswitch_btn_start:I = 0x7f0b0140

.field public static final clockswitch_btn_switch1g:I = 0x7f0b0142

.field public static final clockswitch_btn_switch26:I = 0x7f0b0141

.field public static final clockswitch_debug_enable:I = 0x7f0b013d

.field public static final clockswitch_debug_view:I = 0x7f0b013c

.field public static final clockswitch_timeout_ns:I = 0x7f0b013f

.field public static final clockswitch_timeout_s:I = 0x7f0b013e

.field public static final cmmb_external_edit:I = 0x7f0b00cc

.field public static final cmmb_external_ft:I = 0x7f0b00cd

.field public static final cmmb_internal_ft:I = 0x7f0b00cb

.field public static final cmmb_mbbms30_off_radio:I = 0x7f0b00c9

.field public static final cmmb_mbbms30_on_radio:I = 0x7f0b00c8

.field public static final cmmb_mbbms30_radiagroup:I = 0x7f0b00c7

.field public static final cmmb_memset_off_radio:I = 0x7f0b00c3

.field public static final cmmb_memset_on_radio:I = 0x7f0b00c2

.field public static final cmmb_mts_off_radio:I = 0x7f0b00c5

.field public static final cmmb_mts_on_radio:I = 0x7f0b00c4

.field public static final cmmb_save_off_radio:I = 0x7f0b00c1

.field public static final cmmb_save_on_radio:I = 0x7f0b00c0

.field public static final cmmb_textView_mbbms30:I = 0x7f0b00c6

.field public static final comm_info:I = 0x7f0b0150

.field public static final comm_info_view:I = 0x7f0b014f

.field public static final current_shot_num:I = 0x7f0b00b8

.field public static final dcm_off_radio:I = 0x7f0b015a

.field public static final dcm_on_radio:I = 0x7f0b0159

.field public static final desense_back_light_duty:I = 0x7f0b00d8

.field public static final desense_back_light_duty_btn:I = 0x7f0b00da

.field public static final desense_back_light_duty_desc_textview:I = 0x7f0b00db

.field public static final desense_back_light_duty_edit:I = 0x7f0b00d9

.field public static final desense_back_light_freq_desc_textview:I = 0x7f0b00d7

.field public static final desense_back_light_frequency:I = 0x7f0b00d4

.field public static final desense_back_light_frequency_btn:I = 0x7f0b00d6

.field public static final desense_back_light_frequency_edit:I = 0x7f0b00d5

.field public static final desense_classd_toggle_btn:I = 0x7f0b00bf

.field public static final desense_lcd_desc_textview:I = 0x7f0b00e0

.field public static final desense_lcd_down_btn:I = 0x7f0b00dd

.field public static final desense_lcd_set_btn:I = 0x7f0b00e1

.field public static final desense_lcd_title_textview:I = 0x7f0b00dc

.field public static final desense_lcd_up_btn:I = 0x7f0b00df

.field public static final desense_lcd_value_edit:I = 0x7f0b00de

.field public static final desense_listview:I = 0x7f0b00d3

.field public static final desense_pll_detail_edit:I = 0x7f0b00e3

.field public static final desense_pll_detail_set_btn:I = 0x7f0b00e4

.field public static final desense_pll_detail_title_textview:I = 0x7f0b00e2

.field public static final desense_sdlog_toggle_btn:I = 0x7f0b00e6

.field public static final ecoVersion:I = 0x7f0b00a7

.field public static final edtFrequency:I = 0x7f0b0224

.field public static final edtPocketLength:I = 0x7f0b0227

.field public static final fd_off_radio:I = 0x7f0b00fa

.field public static final fd_on_radio:I = 0x7f0b00f9

.field public static final fd_radio_group:I = 0x7f0b00f8

.field public static final fd_set_button:I = 0x7f0b00fb

.field public static final file_list:I = 0x7f0b00fc

.field public static final file_sys_info:I = 0x7f0b0152

.field public static final file_sys_view:I = 0x7f0b0151

.field public static final frame:I = 0x7f0b00b6

.field public static final frame_layout:I = 0x7f0b00b5

.field public static final health:I = 0x7f0b005b

.field public static final hqa_cpu_main_checkbox:I = 0x7f0b0130

.field public static final hqa_cpu_main_radiogroup:I = 0x7f0b012b

.field public static final hqa_cpu_main_raidobutton_default:I = 0x7f0b012c

.field public static final hqa_cpu_main_raidobutton_dual:I = 0x7f0b012f

.field public static final hqa_cpu_main_raidobutton_single:I = 0x7f0b012e

.field public static final hqa_cpu_main_raidobutton_test:I = 0x7f0b012d

.field public static final level:I = 0x7f0b0059

.field public static final linearLayout1:I = 0x7f0b00ae

.field public static final linearLayout2:I = 0x7f0b00b0

.field public static final list_memory_item:I = 0x7f0b014d

.field public static final listview_bandmode_sim_select:I = 0x7f0b003e

.field public static final listview_hqa_cpu_main:I = 0x7f0b012a

.field public static final media_loopback_layout:I = 0x7f0b0293

.field public static final media_loopback_radiogroup:I = 0x7f0b0294

.field public static final media_loopback_stack:I = 0x7f0b0295

.field public static final media_loopback_transceiver:I = 0x7f0b0296

.field public static final memory_dump_off_radio:I = 0x7f0b015d

.field public static final memory_dump_on_radio:I = 0x7f0b015c

.field public static final memory_flash_tabs:I = 0x7f0b014e

.field public static final menu_discard:I = 0x7f0b029d

.field public static final menu_done:I = 0x7f0b029c

.field public static final modem_test_cta_btn:I = 0x7f0b0160

.field public static final modem_test_fta_btn:I = 0x7f0b0161

.field public static final modem_test_iot_btn:I = 0x7f0b0162

.field public static final modem_test_none_btn:I = 0x7f0b015f

.field public static final modem_test_operator_btn:I = 0x7f0b0163

.field public static final modem_test_textview:I = 0x7f0b015e

.field public static final network_loopback_radiogroup:I = 0x7f0b0298

.field public static final network_loopback_service:I = 0x7f0b029a

.field public static final network_loopback_stack:I = 0x7f0b0299

.field public static final not_specify:I = 0x7f0b0117

.field public static final pagertitle:I = 0x7f0b014c

.field public static final partition_info:I = 0x7f0b0154

.field public static final partition_view:I = 0x7f0b0153

.field public static final patchDate:I = 0x7f0b00ad

.field public static final patchSize:I = 0x7f0b00aa

.field public static final peer_audio_recorder_btn:I = 0x7f0b01a0

.field public static final peer_audio_recorder_service_checkbox:I = 0x7f0b019e

.field public static final peer_video_recorder_btn:I = 0x7f0b01a3

.field public static final peer_video_recorder_service_checkbox:I = 0x7f0b01a1

.field public static final pll_menu_listview:I = 0x7f0b00e5

.field public static final plls_list_item_btn:I = 0x7f0b01b4

.field public static final plls_list_item_edit:I = 0x7f0b01b3

.field public static final plls_list_item_name:I = 0x7f0b01b2

.field public static final pmu_bank_spinner:I = 0x7f0b01ba

.field public static final pmu_btn_get:I = 0x7f0b01bf

.field public static final pmu_btn_set:I = 0x7f0b01c0

.field public static final pmu_edit_addr:I = 0x7f0b01bd

.field public static final pmu_edit_val:I = 0x7f0b01be

.field public static final pmu_info_text:I = 0x7f0b01b8

.field public static final preferredNetworkType:I = 0x7f0b01ca

.field public static final relativeLayout1:I = 0x7f0b00a2

.field public static final relativeLayout2:I = 0x7f0b00a5

.field public static final relativeLayout3:I = 0x7f0b00a8

.field public static final relativeLayout4:I = 0x7f0b00ab

.field public static final save_file_name:I = 0x7f0b016f

.field public static final scale:I = 0x7f0b005a

.field public static final settings_fs_extralarge_edit:I = 0x7f0b01ea

.field public static final settings_fs_large_edit:I = 0x7f0b01e9

.field public static final settings_fs_ok:I = 0x7f0b01eb

.field public static final settings_fs_small_edit:I = 0x7f0b01e8

.field public static final sim3g:I = 0x7f0b01c6

.field public static final simIcon:I = 0x7f0b01c4

.field public static final simNum:I = 0x7f0b01c7

.field public static final simStatus:I = 0x7f0b01c5

.field public static final sleep_mode_off_radio:I = 0x7f0b0157

.field public static final sleep_mode_on_radio:I = 0x7f0b0156

.field public static final spinner1:I = 0x7f0b00af

.field public static final spinner2:I = 0x7f0b00b2

.field public static final status:I = 0x7f0b0058

.field public static final swla_assert_btn:I = 0x7f0b01ef

.field public static final swla_swla_btn:I = 0x7f0b01f0

.field public static final swvideo_btn:I = 0x7f0b0147

.field public static final swvideo_iteration:I = 0x7f0b0144

.field public static final swvideo_iteration_result:I = 0x7f0b0145

.field public static final swvideo_iteration_result_1:I = 0x7f0b0146

.field public static final swvideo_loopcount:I = 0x7f0b0143

.field public static final swvideo_result:I = 0x7f0b0148

.field public static final syslogger_btn_clear_log:I = 0x7f0b01ff

.field public static final syslogger_btn_start:I = 0x7f0b01fd

.field public static final syslogger_btn_taglog:I = 0x7f0b01fe

.field public static final syslogger_extmodemlog_status_image:I = 0x7f0b01fa

.field public static final syslogger_extmodemlog_status_text:I = 0x7f0b01fb

.field public static final syslogger_hq_all:I = 0x7f0b0206

.field public static final syslogger_hq_extmd:I = 0x7f0b0204

.field public static final syslogger_hq_info:I = 0x7f0b0208

.field public static final syslogger_hq_md:I = 0x7f0b0200

.field public static final syslogger_hq_mobile:I = 0x7f0b0201

.field public static final syslogger_hq_network:I = 0x7f0b0202

.field public static final syslogger_hq_sd:I = 0x7f0b0205

.field public static final syslogger_hq_tab:I = 0x7f0b0207

.field public static final syslogger_log_path:I = 0x7f0b01f2

.field public static final syslogger_mobilelog_status_image:I = 0x7f0b01f4

.field public static final syslogger_mobilelog_status_text:I = 0x7f0b01f5

.field public static final syslogger_modemlog_status_image:I = 0x7f0b01f6

.field public static final syslogger_modemlog_status_text:I = 0x7f0b01f7

.field public static final syslogger_networklog_status_image:I = 0x7f0b01f8

.field public static final syslogger_networklog_status_text:I = 0x7f0b01f9

.field public static final technology:I = 0x7f0b005e

.field public static final temperature:I = 0x7f0b005d

.field public static final textView1:I = 0x7f0b00a3

.field public static final textView2:I = 0x7f0b00b1

.field public static final textView21:I = 0x7f0b00a6

.field public static final textView31:I = 0x7f0b00a9

.field public static final textView41:I = 0x7f0b00ac

.field public static final text_layout:I = 0x7f0b01c8

.field public static final textview_help:I = 0x7f0b0129

.field public static final textview_title:I = 0x7f0b0128

.field public static final uptime:I = 0x7f0b005f

.field public static final value6575_text:I = 0x7f0b01bc

.field public static final value_text:I = 0x7f0b01c3

.field public static final video_mal_supported_format_spinner:I = 0x7f0b01a2

.field public static final view_blank1:I = 0x7f0b01f3

.field public static final view_blank2:I = 0x7f0b01fc

.field public static final viewpager:I = 0x7f0b014b

.field public static final voltage:I = 0x7f0b005c

.field public static final vt_set_to_default:I = 0x7f0b029e

.field public static final warning_text:I = 0x7f0b01c1

.field public static final when_needed_continue:I = 0x7f0b0116

.field public static final wifi_ctia_switcher:I = 0x7f0b0248

.field public static final wifi_version:I = 0x7f0b0249

.field public static final wifi_wfa_switcher:I = 0x7f0b0247

.field public static final working_mode_media_loopback:I = 0x7f0b0292

.field public static final working_mode_network_loopback:I = 0x7f0b0297

.field public static final working_mode_normal:I = 0x7f0b0291

.field public static final working_mode_radiogroup:I = 0x7f0b0290

.field public static final working_mode_test_file:I = 0x7f0b029b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
