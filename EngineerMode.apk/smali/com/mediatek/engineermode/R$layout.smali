.class public final Lcom/mediatek/engineermode/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final audio:I = 0x7f030000

.field public static final audio_audiologger:I = 0x7f030001

.field public static final audio_debuginfo:I = 0x7f030002

.field public static final audio_modesetting:I = 0x7f030003

.field public static final audio_speechenhancement:I = 0x7f030004

.field public static final audio_speechloggerx:I = 0x7f030005

.field public static final auto_answer:I = 0x7f030006

.field public static final band_mode:I = 0x7f030007

.field public static final bandmodesim1:I = 0x7f030008

.field public static final bandmodesim2:I = 0x7f030009

.field public static final bandmodesimselect:I = 0x7f03000a

.field public static final bandselect:I = 0x7f03000b

.field public static final baseband:I = 0x7f03000c

.field public static final battery_charge:I = 0x7f03000d

.field public static final battery_info:I = 0x7f03000e

.field public static final ble_normal_mode_advertise:I = 0x7f03000f

.field public static final ble_normal_mode_initiate:I = 0x7f030010

.field public static final ble_normal_mode_scan:I = 0x7f030011

.field public static final ble_normal_mode_whitelist:I = 0x7f030012

.field public static final ble_test_mode:I = 0x7f030013

.field public static final bt_chip_info:I = 0x7f030014

.field public static final bt_relayer_mode:I = 0x7f030015

.field public static final btlist:I = 0x7f030016

.field public static final camera:I = 0x7f030017

.field public static final camera_preview:I = 0x7f030018

.field public static final cfu_activity:I = 0x7f030019

.field public static final class_d_activity:I = 0x7f03001a

.field public static final cmmb_activity:I = 0x7f03001b

.field public static final cpufreq_test:I = 0x7f03001c

.field public static final desense_activity:I = 0x7f03001d

.field public static final desense_back_light:I = 0x7f03001e

.field public static final desense_lcd_activity:I = 0x7f03001f

.field public static final desense_pll_detail_activity:I = 0x7f030020

.field public static final desense_plls_activity:I = 0x7f030021

.field public static final desense_sdlog_activity:I = 0x7f030022

.field public static final devicemgr:I = 0x7f030023

.field public static final display:I = 0x7f030024

.field public static final dualtalk_networkinfo:I = 0x7f030025

.field public static final eint:I = 0x7f030026

.field public static final fastdormancy:I = 0x7f030027

.field public static final file_name_list:I = 0x7f030028

.field public static final gpio:I = 0x7f030029

.field public static final gprs:I = 0x7f03002a

.field public static final help_page:I = 0x7f03002b

.field public static final hqa_cpustress:I = 0x7f03002c

.field public static final hqa_cpustress_apmcu:I = 0x7f03002d

.field public static final hqa_cpustress_clockswitch:I = 0x7f03002e

.field public static final hqa_cpustress_swvideo:I = 0x7f03002f

.field public static final internalmain:I = 0x7f030030

.field public static final io:I = 0x7f030031

.field public static final main:I = 0x7f030032

.field public static final memory:I = 0x7f030033

.field public static final memory_nand_tabs:I = 0x7f030034

.field public static final modem_activity:I = 0x7f030035

.field public static final modem_test_activity:I = 0x7f030036

.field public static final msdc:I = 0x7f030037

.field public static final msdc_hopset:I = 0x7f030038

.field public static final msdc_select:I = 0x7f030039

.field public static final mydialog:I = 0x7f03003a

.field public static final networkinfo:I = 0x7f03003b

.field public static final networkinfo_info:I = 0x7f03003c

.field public static final new_msdc:I = 0x7f03003d

.field public static final peer_audio_recorder:I = 0x7f03003e

.field public static final peer_video_recorder:I = 0x7f03003f

.field public static final phone_auto_test_tool:I = 0x7f030040

.field public static final plls_list_item:I = 0x7f030041

.field public static final power:I = 0x7f030042

.field public static final power_pmu6575_tabs:I = 0x7f030043

.field public static final power_pmu_tabs:I = 0x7f030044

.field public static final preference_sim_info:I = 0x7f030045

.field public static final radio_info:I = 0x7f030046

.field public static final rx_nosig_test:I = 0x7f030047

.field public static final rx_only_test:I = 0x7f030048

.field public static final settings_fontsize:I = 0x7f030049

.field public static final sim1_networkinfo:I = 0x7f03004a

.field public static final sim2_networkinfo:I = 0x7f03004b

.field public static final simselect:I = 0x7f03004c

.field public static final ssp_debug_mode:I = 0x7f03004d

.field public static final swla_activity:I = 0x7f03004e

.field public static final systemlogger:I = 0x7f03004f

.field public static final systemlogger_list:I = 0x7f030050

.field public static final systemlogger_main:I = 0x7f030051

.field public static final systemloggerhq:I = 0x7f030052

.field public static final tddbandselect:I = 0x7f030053

.field public static final test_mode:I = 0x7f030054

.field public static final touch_settings:I = 0x7f030055

.field public static final touchscreen:I = 0x7f030056

.field public static final touchscreen_verification:I = 0x7f030057

.field public static final tv_out:I = 0x7f030058

.field public static final tx_only_test:I = 0x7f030059

.field public static final usb:I = 0x7f03005a

.field public static final usb_test:I = 0x7f03005b

.field public static final usb_test_ex:I = 0x7f03005c

.field public static final vt_auto_answer:I = 0x7f03005d

.field public static final vt_log_filter:I = 0x7f03005e

.field public static final wifi:I = 0x7f03005f

.field public static final wifi_dpd_calibration:I = 0x7f030060

.field public static final wifi_eeprom:I = 0x7f030061

.field public static final wifi_mcr:I = 0x7f030062

.field public static final wifi_rx:I = 0x7f030063

.field public static final wifi_rx_6620:I = 0x7f030064

.field public static final wifi_temperature_sensor:I = 0x7f030065

.field public static final wifi_tx:I = 0x7f030066

.field public static final wifi_tx_6620:I = 0x7f030067

.field public static final working_mode:I = 0x7f030068


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
