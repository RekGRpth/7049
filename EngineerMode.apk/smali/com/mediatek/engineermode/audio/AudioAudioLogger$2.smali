.class Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;
.super Ljava/lang/Object;
.source "AudioAudioLogger.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/audio/AudioAudioLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/16 v6, 0x67

    const/16 v5, 0x65

    const/16 v4, 0x63

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioAudioLogger;->access$000(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioAudioLogger;->access$100(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p2, :cond_2

    invoke-static {v4, v1}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    :cond_1
    :goto_1
    const/4 v3, -0x1

    if-ne v0, v3, :cond_c

    const-string v3, "EM/Audio"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "set"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "parameter failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    const v4, 0x7f08018b

    invoke-static {v3, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    if-nez p2, :cond_b

    :goto_2
    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_2
    invoke-static {v4, v2}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioAudioLogger;->access$200(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz p2, :cond_4

    invoke-static {v5, v1}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {v5, v2}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioAudioLogger;->access$300(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz p2, :cond_6

    invoke-static {v6, v1}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto :goto_1

    :cond_6
    invoke-static {v6, v2}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioAudioLogger;->access$400(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    if-eqz p2, :cond_8

    const/16 v3, 0x69

    invoke-static {v3, v1}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto :goto_1

    :cond_8
    const/16 v3, 0x69

    invoke-static {v3, v2}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto/16 :goto_1

    :cond_9
    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioAudioLogger$2;->this$0:Lcom/mediatek/engineermode/audio/AudioAudioLogger;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioAudioLogger;->access$500(Lcom/mediatek/engineermode/audio/AudioAudioLogger;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p2, :cond_a

    const/16 v3, 0x6b

    invoke-static {v3, v1}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto/16 :goto_1

    :cond_a
    const/16 v3, 0x6b

    invoke-static {v3, v2}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v0

    goto/16 :goto_1

    :cond_b
    move v1, v2

    goto :goto_2

    :cond_c
    const-string v1, "EM/Audio"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "parameter success"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
