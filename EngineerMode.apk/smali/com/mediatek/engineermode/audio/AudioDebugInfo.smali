.class public Lcom/mediatek/engineermode/audio/AudioDebugInfo;
.super Landroid/app/Activity;
.source "AudioDebugInfo.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static final AUDIO_RECORD_PREFER:Ljava/lang/String; = "audio_record"

.field private static final DATA_SIZE:I = 0x5a4

.field private static final DIALOG_ID_GET_DATA_ERROR:I = 0x1

.field private static final DIALOG_ID_SET_ERROR:I = 0x3

.field private static final DIALOG_ID_SET_SUCCESS:I = 0x2

.field private static final LONGEST_NUM_LEN:I = 0x5

.field private static final MAGIC_NUMBER_256:I = 0x100

.field private static final MAGIC_NUMBER_65535:I = 0xffff

.field private static final SPINNER_COUNT:I = 0x10

.field private static final VOLUME_SPEECH_SIZE:I = 0x136


# instance fields
.field private mBtnSet:Landroid/widget/Button;

.field private mData:[B

.field private mDebugValue:Landroid/widget/EditText;

.field private mSpinnerIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const-wide/16 v9, 0x100

    const v8, 0x7f080190

    const/4 v7, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    const v5, 0x7f080189

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v5, v6, :cond_2

    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    invoke-static {p0, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/32 v5, 0xffff

    cmp-long v5, v1, v5

    if-lez v5, :cond_4

    invoke-static {p0, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    div-long v5, v1, v9

    long-to-int v0, v5

    rem-long v5, v1, v9

    long-to-int v3, v5

    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    iget v6, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    mul-int/lit8 v6, v6, 0x2

    add-int/lit16 v6, v6, 0x136

    int-to-byte v7, v3

    aput-byte v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    iget v6, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    mul-int/lit8 v6, v6, 0x2

    add-int/lit16 v6, v6, 0x136

    add-int/lit8 v6, v6, 0x1

    int-to-byte v7, v0

    aput-byte v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    const/16 v6, 0x5a4

    invoke-static {v5, v6}, Landroid/media/AudioSystem;->SetEMParameter([BI)I

    move-result v4

    if-nez v4, :cond_5

    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_5
    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    const-string v5, "EM/Audio"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SetEMParameter return value is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/16 v10, 0x5a4

    const/4 v11, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v8, 0x7f030002

    invoke-virtual {p0, v8}, Landroid/app/Activity;->setContentView(I)V

    const v8, 0x7f0b0008

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    const v8, 0x7f0b000a

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iput-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    const v8, 0x7f0b000b

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mBtnSet:Landroid/widget/Button;

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v8, 0x1090008

    invoke-direct {v2, p0, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v8, 0x1090009

    invoke-virtual {v2, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v0, 0x0

    :goto_0
    const/16 v8, 0x10

    if-ge v0, v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f080195

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v4, p0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-array v8, v10, [B

    iput-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v10, :cond_1

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    aput-byte v11, v8, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    invoke-static {v8, v10}, Landroid/media/AudioSystem;->GetEMParameter([BI)I

    move-result v7

    if-eqz v7, :cond_2

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Landroid/app/Activity;->showDialog(I)V

    const-string v8, "EM/Audio"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Audio_DebugInfo GetEMParameter return value is : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v8, "audio_record"

    invoke-virtual {p0, v8, v11}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v8, "NUM"

    invoke-interface {v5, v8, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    iget v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    invoke-virtual {v4, v8}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    iget v9, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    mul-int/lit8 v9, v9, 0x2

    add-int/lit16 v9, v9, 0x136

    add-int/lit8 v9, v9, 0x1

    aget-byte v8, v8, v9

    mul-int/lit16 v8, v8, 0x100

    iget-object v9, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    iget v10, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit16 v10, v10, 0x136

    aget-byte v9, v9, v10

    add-int v1, v8, v9

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const v3, 0x104000a

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080180

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080181

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/audio/AudioDebugInfo$1;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/audio/AudioDebugInfo$1;-><init>(Lcom/mediatek/engineermode/audio/AudioDebugInfo;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080184

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080191

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080186

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080192

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iput p3, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    iget v4, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    mul-int/lit8 v4, v4, 0x2

    add-int/lit16 v4, v4, 0x136

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v3, v4

    mul-int/lit16 v3, v3, 0x100

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mData:[B

    iget v5, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    mul-int/lit8 v5, v5, 0x2

    add-int/lit16 v5, v5, 0x136

    aget-byte v4, v4, v5

    add-int v1, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mDebugValue:Landroid/widget/EditText;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v3, "audio_record"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "NUM"

    iget v4, p0, Lcom/mediatek/engineermode/audio/AudioDebugInfo;->mSpinnerIndex:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "EM/Audio"

    const-string v1, "do noting..."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
