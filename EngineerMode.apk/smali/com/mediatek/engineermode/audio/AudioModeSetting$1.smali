.class Lcom/mediatek/engineermode/audio/AudioModeSetting$1;
.super Ljava/lang/Object;
.source "AudioModeSetting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/engineermode/audio/AudioModeSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$000(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Value is 0~255"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    const/16 v1, 0xff

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$102(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$200(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v0

    if-nez v0, :cond_4

    if-eqz p3, :cond_0

    if-ne p3, v3, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    add-int/lit8 v1, p3, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$302(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I

    :goto_0
    const-string v0, "EM/Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mModeIndex is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v2}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v0

    if-eq v0, v5, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$400(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$500(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v0

    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$000(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08019b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    const/16 v1, 0xa0

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$102(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$900(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v2}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$600(Lcom/mediatek/engineermode/audio/AudioModeSetting;)[B

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v3}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$200(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$700(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$800(Lcom/mediatek/engineermode/audio/AudioModeSetting;[BIII)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$1000(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V

    const-string v0, "EM/Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v2}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$200(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v2}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v2}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$700(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    add-int/lit8 v1, p3, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$302(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$200(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I

    move-result v0

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    add-int/lit8 v1, p3, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$302(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$302(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$400(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;->this$0:Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-static {v0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->access$500(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "EM/Audio"

    const-string v1, "noting selected."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
