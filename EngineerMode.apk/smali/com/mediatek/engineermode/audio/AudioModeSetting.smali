.class public Lcom/mediatek/engineermode/audio/AudioModeSetting;
.super Landroid/app/Activity;
.source "AudioModeSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final AUDIO_COMMAND_PARAM0:I = 0x20

.field private static final AUDIO_COMMAND_PARAM1:I = 0x21

.field private static final AUDIO_COMMAND_PARAM2:I = 0x22

.field private static final CONSTANT_0XFF:I = 0xff

.field private static final CONSTANT_256:I = 0x100

.field private static final DIALOG_ID_GET_DATA_ERROR:I = 0x1

.field private static final DIALOG_ID_SET_ERROR:I = 0x3

.field private static final DIALOG_ID_SET_SUCCESS:I = 0x2

.field private static final GET_CUSTOMD_DATASIZE:I = 0x5

.field private static final GET_CUSTOM_DATA:I = 0x7

.field private static final MAX_VOL_CATEGORY:I = 0x3

.field private static final MAX_VOL_LEVEL:I = 0x7

.field private static final MAX_VOL_TYPE:I = 0x8

.field private static final MODE_MIC_INDEX:I = 0x2

.field private static final MODE_SID_INDEX:I = 0x5

.field private static final MODE_SPH_INDEX:I = 0x4

.field private static final OFFEST:[I

.field private static final SET_CUSTOM_DATA:I = 0x6

.field private static final STRUCT_SIZE:I = 0xa8

.field private static final TYPE_MEDIA:I = 0x6

.field private static final VALUE_RANGE_160:I = 0xa0

.field private static final VALUE_RANGE_255:I = 0xff


# instance fields
.field private mBtnSet:Landroid/widget/Button;

.field private mBtnSetMaxVol:Landroid/widget/Button;

.field private mCurrentCategory:I

.field private mData:[B

.field private mEditMaxVol:Landroid/widget/EditText;

.field private mFirsummary:Landroid/widget/TextView;

.field private mIsFirstFirSet:Z

.field private mLevelIndex:I

.field private mModeIndex:I

.field private mValText:Landroid/widget/TextView;

.field private mValueEdit:Landroid/widget/EditText;

.field private mValueRange:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->OFFEST:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x15
        0x2a
        0x3f
        0x54
        0x69
        0x7e
        0x93
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0xff

    iput v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueRange:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mIsFirstFirSet:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    invoke-direct {p0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setMaxVolEdit()V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueRange:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mIsFirstFirSet:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/engineermode/audio/AudioModeSetting;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mIsFirstFirSet:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mFirsummary:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mModeIndex:I

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mModeIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSetMaxVol:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/audio/AudioModeSetting;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/audio/AudioModeSetting;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mLevelIndex:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/engineermode/audio/AudioModeSetting;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mLevelIndex:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/audio/AudioModeSetting;[BIII)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/audio/AudioModeSetting;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioModeSetting;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueEdit:Landroid/widget/EditText;

    return-object v0
.end method

.method private checkEditNumber(Landroid/widget/EditText;I)Z
    .locals 7
    .param p1    # Landroid/widget/EditText;
    .param p2    # I

    const v6, 0x7f080188

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const v4, 0x7f080189

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v4, p2, :cond_2

    add-int v4, v6, p2

    const/4 v5, 0x1

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    add-int v4, v6, p2

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private getValue([BIII)I
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    if-ge p2, v0, :cond_0

    const/16 v0, 0x8

    if-ge p3, v0, :cond_0

    const/4 v0, 0x7

    if-lt p4, v0, :cond_1

    :cond_0
    const-string v0, "EM/Audio"

    const-string v1, "assert! Check the setting value."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    mul-int/lit8 v0, p2, 0x7

    add-int/2addr v0, p4

    sget-object v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->OFFEST:[I

    aget v1, v1, p3

    add-int/2addr v0, v1

    aget-byte v0, p1, v0

    add-int/lit16 v0, v0, 0x100

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private setAudioData()V
    .locals 4

    const/4 v1, 0x6

    const/16 v2, 0xa8

    iget-object v3, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-static {v1, v2, v3}, Landroid/media/AudioSystem;->SetAudioData(II[B)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    const-string v1, "EM/Audio"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioModeSetting SetAudioData return value is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMaxVolData(B)V
    .locals 6
    .param p1    # B

    const/4 v2, 0x0

    const/4 v3, 0x6

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    const/4 v4, 0x4

    move-object v0, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setValue([BIIIB)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    const/4 v4, 0x5

    move-object v0, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setValue([BIIIB)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    move-object v0, p0

    move v4, v3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setValue([BIIIB)V

    goto :goto_0
.end method

.method private setMaxVolEdit()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x6

    const-string v0, "EM/Audio"

    const-string v1, "Set max vol Edit."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-direct {p0, v1, v4, v3, v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "EM/Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0 is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-direct {p0, v2, v4, v3, v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-direct {p0, v1, v4, v3, v6}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "EM/Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "1 is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-direct {p0, v2, v4, v3, v6}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-direct {p0, v1, v4, v3, v3}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "EM/Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2 is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    invoke-direct {p0, v2, v4, v3, v3}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "EM/Audio"

    const-string v1, "error is 0"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setValue([BIIIB)V
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # B

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    if-ge p2, v0, :cond_0

    const/16 v0, 0x8

    if-ge p3, v0, :cond_0

    const/4 v0, 0x7

    if-lt p4, v0, :cond_1

    :cond_0
    const-string v0, "EM/Audio"

    const-string v1, "assert! Check the setting value."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    mul-int/lit8 v0, p2, 0x7

    add-int/2addr v0, p4

    sget-object v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->OFFEST:[I

    aget v1, v1, p3

    add-int/2addr v0, v1

    aput-byte p5, p1, v0

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueEdit:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueRange:I

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->checkEditNumber(Landroid/widget/EditText;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-byte v5, v6

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    iget v2, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    iget v3, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mModeIndex:I

    iget v4, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mLevelIndex:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setValue([BIIIB)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setAudioData()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSetMaxVol:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    const/16 v1, 0xa0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->checkEditNumber(Landroid/widget/EditText;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-byte v5, v6

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setMaxVolData(B)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setAudioData()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 22
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v17, 0x7f030003

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v17, "CurrentMode"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    const/16 v17, 0x5

    invoke-static/range {v17 .. v17}, Landroid/media/AudioSystem;->GetAudioCommand(I)I

    move-result v6

    const/16 v17, 0xa8

    move/from16 v0, v17

    if-eq v6, v0, :cond_0

    const-string v17, "EM/Audio"

    const-string v18, "assert! Check the structure size!"

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-array v0, v6, [B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    const/4 v14, 0x0

    :goto_0
    if-ge v14, v6, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-byte v18, v17, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    const/16 v17, 0x7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    move-object/from16 v18, v0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v6, v1}, Landroid/media/AudioSystem;->GetAudioData(II[B)I

    move-result v16

    if-eqz v16, :cond_2

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    const-string v17, "EM/Audio"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "AudioModeSetting GetAudioData return value is : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const v17, 0x7f0b0013

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSet:Landroid/widget/Button;

    const v17, 0x7f0b0012

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueEdit:Landroid/widget/EditText;

    const v17, 0x7f0b0015

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSetMaxVol:Landroid/widget/Button;

    const v17, 0x7f0b0014

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mEditMaxVol:Landroid/widget/EditText;

    const v17, 0x7f0b000f

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Spinner;

    const v17, 0x7f0b000e

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Spinner;

    const v17, 0x7f0b0010

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Spinner;

    const v17, 0x7f0b000d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mFirsummary:Landroid/widget/TextView;

    const v17, 0x7f0b0011

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValText:Landroid/widget/TextView;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    move/from16 v17, v0

    if-nez v17, :cond_3

    const v17, 0x7f060024

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :goto_1
    new-instance v12, Landroid/widget/ArrayAdapter;

    const v17, 0x1090008

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v12, v0, v1, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v17, 0x1090009

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v13, v12}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v17, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting$1;-><init>(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v10, Landroid/widget/ArrayAdapter;

    const v17, 0x1090008

    const v18, 0x7f060027

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v10, v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v17, 0x1090009

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v11, v10}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v17, Lcom/mediatek/engineermode/audio/AudioModeSetting$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting$2;-><init>(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v8, Landroid/widget/ArrayAdapter;

    const v17, 0x1090008

    const v18, 0x7f060028

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v8, v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v17, 0x1090009

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v9, v8}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v17, Lcom/mediatek/engineermode/audio/AudioModeSetting$3;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/engineermode/audio/AudioModeSetting$3;-><init>(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSet:Landroid/widget/Button;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mBtnSetMaxVol:Landroid/widget/Button;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mValueEdit:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mData:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mModeIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mLevelIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->getValue([BIII)I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/engineermode/audio/AudioModeSetting;->setMaxVolEdit()V

    return-void

    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/audio/AudioModeSetting;->mCurrentCategory:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    const v17, 0x7f060025

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_4
    const v17, 0x7f060026

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const v3, 0x104000a

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080180

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080181

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/audio/AudioModeSetting$4;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/audio/AudioModeSetting$4;-><init>(Lcom/mediatek/engineermode/audio/AudioModeSetting;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080184

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080185

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080186

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080187

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
