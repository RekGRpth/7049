.class Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;
.super Ljava/lang/Object;
.source "AudioSpeechLoggerX.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 11
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/16 v10, 0x60

    const/4 v9, -0x1

    const v8, 0x7f08018b

    const/4 v6, 0x0

    const/4 v7, 0x1

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    const-string v5, "engineermode_audiolog_preferences"

    invoke-virtual {v4, v5, v7}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$000(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v0, p2}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$100(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;Landroid/content/SharedPreferences$Editor;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$200(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz p2, :cond_2

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$300(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)[B

    move-result-object v4

    const/16 v5, 0x5a0

    aget-byte v6, v4, v5

    or-int/lit8 v6, v6, 0x2

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$476(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)I

    const-string v4, "EM/Audio"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "E mVmLogState "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v6}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$400(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$300(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)[B

    move-result-object v4

    const/16 v5, 0x5a4

    invoke-static {v4, v5}, Landroid/media/AudioSystem;->SetEMParameter([BI)I

    move-result v1

    if-eqz v1, :cond_0

    const-string v4, "EM/Audio"

    const-string v5, "set CTM4WAY parameter failed"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$300(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)[B

    move-result-object v4

    const/16 v5, 0x5a0

    aget-byte v6, v4, v5

    and-int/lit8 v6, v6, -0x3

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    const/4 v5, -0x3

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$472(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)I

    const-string v4, "EM/Audio"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "D mVmLogState "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v6}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$400(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$500(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz p2, :cond_4

    const-string v4, "EM/Audio"

    const-string v5, "mCKVOIPLogger checked"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v7}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$600(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)Z

    goto/16 :goto_0

    :cond_4
    const-string v4, "EM/Audio"

    const-string v5, "mCKVOIPLogger Unchecked"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v6}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$600(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)Z

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$700(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    if-eqz p2, :cond_7

    const-string v4, "EM/Audio"

    const-string v5, "mCKBEPL checked"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10, v7}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v3

    if-ne v3, v9, :cond_6

    const-string v4, "EM/Audio"

    const-string v5, "set mCKBEPL parameter failed"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_6
    const-string v4, "epl_status"

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    :cond_7
    const-string v4, "EM/Audio"

    const-string v5, "mCKBEPL unchecked"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$800(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_b

    const-string v4, "EM/Audio"

    const-string v5, "mCKBNormalVm checked"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$900(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v6}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->access$902(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;Z)Z

    goto/16 :goto_0

    :cond_9
    const-string v4, "EM/Audio"

    const-string v5, "mCKBNormalVm checked ok"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10, v6}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v3

    if-ne v3, v9, :cond_a

    const-string v4, "EM/Audio"

    const-string v5, "set mCKBNormalVm parameter failed"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;->this$0:Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    invoke-static {v4, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_a
    const-string v4, "epl_status"

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    :cond_b
    const-string v4, "EM/Audio"

    const-string v5, "mCKBNormalVm unchecked"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
