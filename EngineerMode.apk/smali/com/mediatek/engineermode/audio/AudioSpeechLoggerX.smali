.class public Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
.super Landroid/app/Activity;
.source "AudioSpeechLoggerX.java"


# static fields
.field private static final CONSTANT_0XFF:I = 0xff

.field private static final CONSTANT_256:I = 0x100

.field private static final DATA_SIZE:I = 0x5a4

.field private static final DIALOG_GET_DATA_ERROR:I = 0x0

.field private static final DIALOG_ID_NO_SDCARD:I = 0x1

.field private static final DIALOG_ID_SDCARD_BUSY:I = 0x2

.field public static final ENGINEER_MODE_PREFERENCE:Ljava/lang/String; = "engineermode_audiolog_preferences"

.field public static final EPL_STATUS:Ljava/lang/String; = "epl_status"

.field private static final SET_DUMP_SPEECH_DEBUG_INFO:I = 0x61

.field private static final SET_SPEECH_VM_ENABLE:I = 0x60

.field private static final VM_LOG_POS:I = 0x5a0


# instance fields
.field private mCKCTM4WAY:Landroid/widget/CheckBox;

.field private mCKSpeechLogger:Landroid/widget/CheckBox;

.field private mCKVOIPLogger:Landroid/widget/CheckBox;

.field private final mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mData:[B

.field private mForRefresh:Z

.field private mRadioBtnBEPL:Landroid/widget/RadioButton;

.field private mRadioBtnBNormalVm:Landroid/widget/RadioButton;

.field private mVmLogState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mForRefresh:Z

    new-instance v0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$1;-><init>(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKSpeechLogger:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;Landroid/content/SharedPreferences$Editor;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
    .param p1    # Landroid/content/SharedPreferences$Editor;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->onClickSpeechLogger(Landroid/content/SharedPreferences$Editor;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKCTM4WAY:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    return v0
.end method

.method static synthetic access$472(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    return v0
.end method

.method static synthetic access$476(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKVOIPLogger:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;I)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->setVOIP(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-object v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mForRefresh:Z

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mForRefresh:Z

    return p1
.end method

.method private checkSDCardIsAvaliable()Ljava/lang/Boolean;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EM/Audio"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Environment.getExternalStorageState() is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "removed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "shared"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method private getVOIP()I
    .locals 5

    const/4 v2, 0x0

    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "/system/bin/sh"

    aput-object v3, v0, v2

    const/4 v3, 0x1

    const-string v4, "-c"

    aput-object v4, v0, v3

    const/4 v3, 0x2

    const-string v4, "cat /data/data/com.mediatek.engineermode/sharefile/audio_voip"

    aput-object v4, v0, v3

    :try_start_0
    invoke-static {v0}, Lcom/mediatek/engineermode/ShellExe;->execCommand([Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :catch_0
    move-exception v1

    const-string v3, "EM/Audio"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method private onClickSpeechLogger(Landroid/content/SharedPreferences$Editor;Z)V
    .locals 9
    .param p1    # Landroid/content/SharedPreferences$Editor;
    .param p2    # Z

    const/16 v8, 0x5a4

    const/16 v7, 0x5a0

    const v6, 0x7f08018b

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p2, :cond_2

    const-string v2, "EM/Audio"

    const-string v3, "mCKSpeechLogger checked"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->checkSDCardIsAvaliable()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "EM/Audio"

    const-string v3, "mCKSpeechLogger checked 111"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKSpeechLogger:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iput-boolean v5, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mForRefresh:Z

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    aget-byte v3, v2, v7

    or-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    aput-byte v3, v2, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    invoke-static {v2, v8}, Landroid/media/AudioSystem;->SetEMParameter([BI)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "EM/Audio"

    const-string v3, "set mAutoVM parameter failed"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const-string v2, "EM/Audio"

    const-string v3, "mCKSpeechLogger unchecked"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_4
    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    const/16 v2, 0x60

    invoke-static {v2, v4}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_5

    const-string v2, "EM/Audio"

    const-string v3, "set mCKBEPL parameter failed 1"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_5
    const-string v2, "epl_status"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    aget-byte v3, v2, v7

    and-int/lit8 v3, v3, -0x2

    int-to-byte v3, v3

    aput-byte v3, v2, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    invoke-static {v2, v8}, Landroid/media/AudioSystem;->SetEMParameter([BI)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "EM/Audio"

    const-string v3, "set mAutoVM parameter failed"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private setVOIP(I)Z
    .locals 8
    .param p1    # I

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v5, "/system/bin/sh"

    aput-object v5, v0, v3

    const-string v5, "-c"

    aput-object v5, v0, v4

    const-string v5, "mkdir /data/data/com.mediatek.engineermode/sharefile"

    aput-object v5, v0, v7

    :try_start_0
    invoke-static {v0}, Lcom/mediatek/engineermode/ShellExe;->execCommand([Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v2

    const-string v4, "EM/Audio"

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-array v1, v6, [Ljava/lang/String;

    const-string v5, "/system/bin/sh"

    aput-object v5, v1, v3

    const-string v5, "-c"

    aput-object v5, v1, v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "echo "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " > /data/data/com.mediatek.engineermode/sharefile/audio_voip "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v7

    :try_start_1
    invoke-static {v1}, Lcom/mediatek/engineermode/ShellExe;->execCommand([Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-nez v5, :cond_0

    move v3, v4

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v4, "EM/Audio"

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private shortToInt(BB)I
    .locals 4
    .param p1    # B
    .param p2    # B

    add-int/lit16 v3, p2, 0x100

    and-int/lit16 v2, v3, 0xff

    mul-int/lit16 v0, v2, 0x100

    add-int/lit16 v3, p1, 0x100

    and-int/lit16 v1, v3, 0xff

    add-int v3, v0, v1

    return v3
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/16 v12, 0x5a4

    const/16 v9, 0x60

    const/16 v8, 0x8

    const/4 v11, 0x0

    const/4 v10, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v7, 0x7f030005

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setContentView(I)V

    const v7, 0x7f0b001d

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKSpeechLogger:Landroid/widget/CheckBox;

    const v7, 0x7f0b0021

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKVOIPLogger:Landroid/widget/CheckBox;

    const v7, 0x7f0b0023

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKCTM4WAY:Landroid/widget/CheckBox;

    const v7, 0x7f0b0022

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v7, 0x7f0b001f

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    const v7, 0x7f0b0020

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    const v7, 0x7f0b0025

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v7, 0x7f0b0024

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v8}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKCTM4WAY:Landroid/widget/CheckBox;

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const-string v7, "engineermode_audiolog_preferences"

    invoke-virtual {p0, v7, v10}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v7, "epl_status"

    invoke-interface {v3, v7, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v4, 0x0

    if-ne v2, v10, :cond_3

    invoke-static {v9, v10}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v4

    :goto_0
    const/4 v7, -0x1

    if-ne v4, v7, :cond_1

    const-string v7, "EM/Audio"

    const-string v8, "init mCKBEPL parameter failed"

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-array v7, v12, [B

    iput-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    invoke-static {v7, v12}, Landroid/media/AudioSystem;->GetEMParameter([BI)I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0, v11}, Landroid/app/Activity;->showDialog(I)V

    const-string v7, "EM/Audio"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Audio_SpeechLogger GetEMParameter return value is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    const/16 v8, 0x5a0

    aget-byte v7, v7, v8

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mData:[B

    const/16 v9, 0x5a1

    aget-byte v8, v8, v9

    invoke-direct {p0, v7, v8}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->shortToInt(BB)I

    move-result v7

    iput v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    const-string v7, "EM/Audio"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Audio_SpeechLogger GetEMParameter return value is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKSpeechLogger:Landroid/widget/CheckBox;

    invoke-virtual {v7, v11}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v7, v11}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v7, v11}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    iget v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mVmLogState:I

    and-int/lit8 v7, v7, 0x2

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKCTM4WAY:Landroid/widget/CheckBox;

    invoke-virtual {v7, v11}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->getVOIP()I

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKVOIPLogger:Landroid/widget/CheckBox;

    invoke-virtual {v7, v11}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_3
    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKSpeechLogger:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKVOIPLogger:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKCTM4WAY:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    iget-object v8, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v7, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$2;

    invoke-direct {v7, p0}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$2;-><init>(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_3
    invoke-static {v9, v11}, Landroid/media/AudioSystem;->SetAudioCommand(II)I

    move-result v4

    goto/16 :goto_0

    :cond_4
    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKSpeechLogger:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    if-ne v2, v10, :cond_5

    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBEPL:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mRadioBtnBNormalVm:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_6
    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKCTM4WAY:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_7
    iget-object v7, p0, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;->mCKVOIPLogger:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_3
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    const v3, 0x104000a

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080180

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080181

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$3;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX$3;-><init>(Lcom/mediatek/engineermode/audio/AudioSpeechLoggerX;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08018c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f08018d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08018e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f08018f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
