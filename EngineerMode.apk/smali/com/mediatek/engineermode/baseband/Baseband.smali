.class public Lcom/mediatek/engineermode/baseband/Baseband;
.super Landroid/app/Activity;
.source "Baseband.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final MAX_VALUE:I = 0x400

.field private static final PARA_NUM:I = 0x4

.field private static final RADIX_LENGTH_SIXTEEN:I = 0x10

.field private static final RADIX_LENGTH_TEN:I = 0xa

.field private static final READ:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EM-Baseband"

.field private static final WRITE:I = 0x1


# instance fields
.field private mBtnRead:Landroid/widget/Button;

.field private mBtnWrite:Landroid/widget/Button;

.field private mEditAddr:Landroid/widget/EditText;

.field private mEditLen:Landroid/widget/EditText;

.field private mEditVal:Landroid/widget/EditText;

.field private mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

.field private mInfo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public checkValue(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    const-wide/16 v0, 0x0

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const/16 v8, 0x10

    :try_start_0
    invoke-static {p1, v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    const/16 v8, 0xa

    invoke-static {p2, v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    const-wide/16 v8, 0x0

    cmp-long v8, v3, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x400

    cmp-long v8, v3, v8

    if-gtz v8, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public functionCall(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v1, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const/16 v2, 0x2711

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const-string v2, "r"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1, p2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1, p3}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    :goto_1
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const-string v2, "w"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1, p4}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;

    const v11, 0x7f080425

    const/4 v10, -0x1

    const/4 v9, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    iget-object v8, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v8

    if-ne v7, v8, :cond_4

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditAddr:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditLen:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditVal:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v0, v2}, Lcom/mediatek/engineermode/baseband/Baseband;->checkValue(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const v7, 0x7f080426

    invoke-static {p0, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v7, 0x10

    :try_start_0
    invoke-static {v0, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    const/16 v7, 0xa

    invoke-static {v2, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v0, v2, v8}, Lcom/mediatek/engineermode/baseband/Baseband;->functionCall(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const v7, 0x7f080428

    invoke-static {p0, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {p0, v11, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v7}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v3

    iget-object v7, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_1
    iget v7, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-ne v7, v10, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    const v8, 0x7f080429

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    iget-object v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v7, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-eq v7, v9, :cond_2

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    iget-object v8, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v8

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditAddr:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditLen:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditVal:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v0, v2}, Lcom/mediatek/engineermode/baseband/Baseband;->checkValue(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    const v7, 0x7f080426

    invoke-static {p0, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    const/16 v7, 0x10

    :try_start_1
    invoke-static {v0, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    const/16 v7, 0xa

    invoke-static {v2, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {p0, v9, v0, v2, v6}, Lcom/mediatek/engineermode/baseband/Baseband;->functionCall(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    const v7, 0x7f080428

    invoke-static {p0, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-static {p0, v11, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_6
    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v7}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v3

    iget-object v7, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    :goto_2
    iget v7, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-ne v7, v10, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    const v8, 0x7f080429

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v7, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    iget-object v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v7, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-eq v7, v9, :cond_6

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0054

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnRead:Landroid/widget/Button;

    const v0, 0x7f0b0055

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnWrite:Landroid/widget/Button;

    const v0, 0x7f0b0050

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditAddr:Landroid/widget/EditText;

    const v0, 0x7f0b0053

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditLen:Landroid/widget/EditText;

    const v0, 0x7f0b0051

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditVal:Landroid/widget/EditText;

    const v0, 0x7f0b0056

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
