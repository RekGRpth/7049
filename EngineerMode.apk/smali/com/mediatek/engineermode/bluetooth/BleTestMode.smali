.class public Lcom/mediatek/engineermode/bluetooth/BleTestMode;
.super Landroid/app/Activity;
.source "BleTestMode.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;
    }
.end annotation


# static fields
.field private static final ACTIVITY_EXIT:I = 0x14

.field private static final CHANNEL_NUM:I = 0x28

.field private static final CHECK_BT_DEVEICE:I = 0x3

.field private static final CHECK_BT_STATE:I = 0x1

.field private static final CHECK_STOP:I = 0x2

.field private static final RENTURN_SUCCESS:I = 0x0

.field private static final STOP_FINISH:I = 0xf

.field private static final TAG:Ljava/lang/String; = "BLETestMode"

.field private static final TEST_FAILED:I = 0xe

.field private static final TEST_START:I = 0xb

.field private static final TEST_STOP:I = 0xc

.field private static final TEST_SUCCESS:I = 0xd


# instance fields
.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtInited:Z

.field private mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

.field private mBtnStart:Landroid/widget/Button;

.field private mBtnStop:Landroid/widget/Button;

.field private mChannelSpn:Landroid/widget/Spinner;

.field private mChannelValue:B

.field private mContinune:Landroid/widget/CheckBox;

.field private mIniting:Z

.field private mPatternSpn:Landroid/widget/Spinner;

.field private mPatternValue:B

.field private mRBtnHopping:Landroid/widget/RadioButton;

.field private mRBtnRx:Landroid/widget/RadioButton;

.field private mRBtnSingle:Landroid/widget/RadioButton;

.field private mRBtnTx:Landroid/widget/RadioButton;

.field private mResultStr:Ljava/lang/String;

.field private mResultText:Landroid/widget/TextView;

.field private mTestStared:Z

.field private mTxTest:Z

.field private mUiHandler:Landroid/os/Handler;

.field private mWorkHandler:Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStop:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mResultText:Landroid/widget/TextView;

    const-string v0, "R:"

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mResultStr:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnTx:Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnRx:Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnHopping:Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnSingle:Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mContinune:Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelSpn:Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternSpn:Landroid/widget/Spinner;

    iput-byte v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelValue:B

    iput-byte v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternValue:B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTxTest:Z

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    iput-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTestStared:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mIniting:Z

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mWorkHandler:Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;

    new-instance v0, Lcom/mediatek/engineermode/bluetooth/BleTestMode$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode$3;-><init>(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/bluetooth/BleTestMode;B)B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;
    .param p1    # B

    iput-byte p1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelValue:B

    return p1
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/bluetooth/BleTestMode;B)B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;
    .param p1    # B

    iput-byte p1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternValue:B

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->handleStopBtnClick()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->uninitBtTestOjbect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mResultStr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mResultText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/engineermode/bluetooth/BleTestMode;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTestStared:Z

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/bluetooth/BleTestMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->setViewState(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->initBtTestOjbect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->handleStartBtnClick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BleTestMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private handleRxTestStart()Z
    .locals 12

    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v7, "BLETestMode"

    const-string v8, "-->handleRxTestStart"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x5

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v5, v0, v6

    const/16 v7, 0x1d

    aput-char v7, v0, v5

    const/16 v7, 0x20

    aput-char v7, v0, v11

    const/4 v7, 0x3

    aput-char v5, v0, v7

    const/4 v7, 0x4

    iget-byte v8, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelValue:B

    int-to-char v8, v8

    aput-char v8, v0, v7

    iget-object v7, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v7, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v7, v3

    if-ge v2, v7, :cond_1

    const-string v7, "response[%d] = 0x%x"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    aget-char v9, v3, v2

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "BLETestMode"

    invoke-static {v7, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "BLETestMode"

    const-string v7, "HCICommandRun failed"

    invoke-static {v5, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    :goto_1
    return v5

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private handleRxTestStop()V
    .locals 12

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v11, 0x0

    const-string v5, "BLETestMode"

    const-string v6, "-->handleRxTestStop"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v9, v0, v11

    const/16 v5, 0x1f

    aput-char v5, v0, v9

    const/16 v5, 0x20

    aput-char v5, v0, v10

    const/4 v5, 0x3

    aput-char v11, v0, v5

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    const-string v5, "response[%d] = 0x%x"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    aget-char v7, v3, v2

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "BLETestMode"

    invoke-static {v5, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "***Packet Count: %d"

    new-array v6, v9, [Ljava/lang/Object;

    const/16 v7, 0x8

    aget-char v7, v3, v7

    int-to-long v7, v7

    const-wide/16 v9, 0x100

    mul-long/2addr v7, v9

    const/4 v9, 0x7

    aget-char v9, v3, v9

    int-to-long v9, v9

    add-long/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mResultStr:Ljava/lang/String;

    const/4 v3, 0x0

    :goto_1
    return-void

    :cond_1
    const-string v5, "BLETestMode"

    const-string v6, "HCICommandRun failed"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private handleStartBtnClick()Z
    .locals 12

    const/4 v8, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v5, 0x0

    const-string v6, "BLETestMode"

    const-string v7, "-->handleStartBtnClick"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v10, v0, v5

    aput-char v8, v0, v10

    const/16 v6, 0xc

    aput-char v6, v0, v11

    aput-char v5, v0, v8

    iget-object v6, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v6, v3

    if-ge v2, v6, :cond_1

    const-string v6, "response[%d] = 0x%x"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    aget-char v8, v3, v2

    int-to-long v8, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "BLETestMode"

    invoke-static {v6, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v6, "BLETestMode"

    const-string v7, "HCICommandRun failed"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v5

    :cond_1
    const/4 v3, 0x0

    iget-boolean v5, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTxTest:Z

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->handleTxTestStart()Z

    move-result v5

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->handleRxTestStart()Z

    move-result v5

    goto :goto_1
.end method

.method private handleStopBtnClick()V
    .locals 2

    const-string v0, "BLETestMode"

    const-string v1, "-->handleStopBtnClick"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTxTest:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->handleTxTestStop()V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTestStared:Z

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->handleRxTestStop()V

    goto :goto_0
.end method

.method private handleTxTestStart()Z
    .locals 12

    const/4 v9, 0x3

    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v7, "BLETestMode"

    const-string v8, "-->handleTxTestStart"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x7

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v5, v0, v6

    const/16 v7, 0x1e

    aput-char v7, v0, v5

    const/16 v7, 0x20

    aput-char v7, v0, v11

    aput-char v9, v0, v9

    const/4 v7, 0x4

    iget-byte v8, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelValue:B

    int-to-char v8, v8

    aput-char v8, v0, v7

    const/4 v7, 0x5

    const/16 v8, 0x25

    aput-char v8, v0, v7

    const/4 v7, 0x6

    iget-byte v8, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternValue:B

    int-to-char v8, v8

    aput-char v8, v0, v7

    iget-object v7, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v7, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v7, v3

    if-ge v2, v7, :cond_1

    const-string v7, "response[%d] = 0x%x"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    aget-char v9, v3, v2

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "BLETestMode"

    invoke-static {v7, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "BLETestMode"

    const-string v7, "HCICommandRun failed"

    invoke-static {v5, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    :goto_1
    return v5

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private handleTxTestStop()V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-string v5, "BLETestMode"

    const-string v6, "-->handleTxTestStop"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v10, v0, v9

    const/16 v5, 0x1f

    aput-char v5, v0, v10

    const/16 v5, 0x20

    aput-char v5, v0, v11

    const/4 v5, 0x3

    aput-char v9, v0, v5

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_1

    const-string v5, "response[%d] = 0x%x"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    aget-char v7, v3, v2

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "BLETestMode"

    invoke-static {v5, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "BLETestMode"

    const-string v6, "HCICommandRun failed"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private initBtTestOjbect()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    const-string v1, "BLETestMode"

    const-string v2, "-->initBtTestOjbect"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mIniting:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-nez v1, :cond_2

    new-instance v1, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    if-nez v1, :cond_3

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mIniting:Z

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->init()I

    move-result v1

    if-eqz v1, :cond_4

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    const-string v1, "BLETestMode"

    const-string v2, "mBT initialization failed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mIniting:Z

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->runHCIResetCmd()V

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    goto :goto_1
.end method

.method private runHCIResetCmd()V
    .locals 12

    const/4 v7, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-string v5, "BLETestMode"

    const-string v6, "-->runHCIResetCmd"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v10, v0, v9

    aput-char v7, v0, v10

    const/16 v5, 0xc

    aput-char v5, v0, v11

    aput-char v9, v0, v7

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_1

    const-string v5, "response[%d] = 0x%x"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    aget-char v7, v3, v2

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "BLETestMode"

    invoke-static {v5, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "BLETestMode"

    const-string v6, "HCICommandRun failed"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v3, 0x0

    return-void
.end method

.method private setViewState(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnTx:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnRx:Landroid/widget/RadioButton;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnHopping:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnSingle:Landroid/widget/RadioButton;

    if-nez p1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mContinune:Landroid/widget/CheckBox;

    if-nez p1, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelSpn:Landroid/widget/Spinner;

    if-nez p1, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternSpn:Landroid/widget/Spinner;

    if-nez p1, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStart:Landroid/widget/Button;

    if-nez p1, :cond_6

    :goto_6
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6
.end method

.method private uninitBtTestOjbect()Z
    .locals 3

    const/4 v2, 0x0

    const-string v0, "BLETestMode"

    const-string v1, "-->uninitBtTestOjbect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTestStared:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->runHCIResetCmd()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/bluetooth/BtTest;->unInit()I

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "BLETestMode"

    const-string v1, "mBT un-initialization failed"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    iput-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtInited:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTestStared:Z

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "BLETestMode"

    const-string v1, "-->onClick"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->setViewState(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mWorkHandler:Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mWorkHandler:Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnRx:Landroid/widget/RadioButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTxTest:Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnTx:Landroid/widget/RadioButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTxTest:Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const v8, 0x1090009

    const v7, 0x1090008

    const/4 v6, 0x1

    const-string v4, "BLETestMode"

    const-string v5, "-->onCreate"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f030013

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f0b009f

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStart:Landroid/widget/Button;

    const v4, 0x7f0b00a0

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStop:Landroid/widget/Button;

    const v4, 0x7f0b00a1

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mResultText:Landroid/widget/TextView;

    const v4, 0x7f0b0095

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnTx:Landroid/widget/RadioButton;

    const v4, 0x7f0b0096

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnRx:Landroid/widget/RadioButton;

    const v4, 0x7f0b0098

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnHopping:Landroid/widget/RadioButton;

    const v4, 0x7f0b0099

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnSingle:Landroid/widget/RadioButton;

    const v4, 0x7f0b009e

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mContinune:Landroid/widget/CheckBox;

    const v4, 0x7f0b009b

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelSpn:Landroid/widget/Spinner;

    const v4, 0x7f0b009d

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternSpn:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnTx:Landroid/widget/RadioButton;

    invoke-virtual {v4, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iput-boolean v6, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mTxTest:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnTx:Landroid/widget/RadioButton;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnRx:Landroid/widget/RadioButton;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnSingle:Landroid/widget/RadioButton;

    invoke-virtual {v4, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnSingle:Landroid/widget/RadioButton;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mRBtnHopping:Landroid/widget/RadioButton;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-direct {v1, p0, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v0, 0x0

    :goto_0
    const/16 v4, 0x28

    if-ge v0, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0802a9

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelSpn:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mChannelSpn:Landroid/widget/Spinner;

    new-instance v5, Lcom/mediatek/engineermode/bluetooth/BleTestMode$1;

    invoke-direct {v5, p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode$1;-><init>(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)V

    invoke-virtual {v4, v5}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-direct {v2, p0, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mPatternSpn:Landroid/widget/Spinner;

    new-instance v5, Lcom/mediatek/engineermode/bluetooth/BleTestMode$2;

    invoke-direct {v5, p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode$2;-><init>(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)V

    invoke-virtual {v4, v5}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->setViewState(Z)V

    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "BLETestMode"

    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    new-instance v4, Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6}, Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;-><init>(Lcom/mediatek/engineermode/bluetooth/BleTestMode;Landroid/os/Looper;Lcom/mediatek/engineermode/bluetooth/BleTestMode$1;)V

    iput-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mWorkHandler:Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1    # I

    const v6, 0x7f08025e

    const v5, 0x7f08025d

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "BLETestMode"

    const-string v2, "-->onCreateDialog"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_0

    const v1, 0x7f080262

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const-string v1, "BLETestMode"

    const-string v2, "new ProgressDialog succeed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "BLETestMode"

    const-string v2, "new ProgressDialog failed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    if-ne p1, v4, :cond_2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080260

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/BleTestMode$4;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode$4;-><init>(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080261

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/BleTestMode$5;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/bluetooth/BleTestMode$5;-><init>(Lcom/mediatek/engineermode/bluetooth/BleTestMode;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "BLETestMode"

    const-string v1, "-->onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mWorkHandler:Lcom/mediatek/engineermode/bluetooth/BleTestMode$WorkHandler;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "BLETestMode"

    const-string v1, "-->onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BleTestMode;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method
