.class public Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;
.super Landroid/app/Activity;
.source "BtRelayerModeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity$FunctionTask;
    }
.end annotation


# static fields
.field private static final END_TEST:I = 0x2

.field private static final EXIT_SUCCESS:I = 0xa

.field private static final PARA_INDEX:I = 0x0

.field private static final RENTURN_SUCCESS:I = 0x0

.field private static final START_TEST:I = 0x1

.field private static final TAG:Ljava/lang/String; = "EM/BT/RelayerMode"


# instance fields
.field private mBauSpinner:Landroid/widget/Spinner;

.field private mBaudrate:I

.field private mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

.field private mPortNumber:I

.field private mStartBtn:Landroid/widget/Button;

.field private mStartFlag:Z

.field private mUartSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartFlag:Z

    const/16 v0, 0x2580

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBaudrate:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mPortNumber:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;)Lcom/mediatek/engineermode/bluetooth/BtTest;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;Lcom/mediatek/engineermode/bluetooth/BtTest;)Lcom/mediatek/engineermode/bluetooth/BtTest;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;
    .param p1    # Lcom/mediatek/engineermode/bluetooth/BtTest;

    iput-object p1, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;

    iget v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mPortNumber:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;

    iget v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBaudrate:I

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartFlag:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartBtn:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v3, "EM/BT/RelayerMode"

    const-string v4, "-->onClick"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "EM/BT/RelayerMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mStartFlag--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartFlag:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    if-ne v3, v4, :cond_0

    const-string v3, "EM/BT/RelayerMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mBtRelayerModeSpinner.getSelectedItem()--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBauSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBauSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBaudrate:I

    const-string v3, "EM/BT/RelayerMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mBaudrate--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBaudrate:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mUartSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v3

    iput v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mPortNumber:I

    const-string v3, "EM/BT/RelayerMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mPortNumber--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mPortNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity$FunctionTask;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity$FunctionTask;-><init>(Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartFlag:Z

    if-eqz v3, :cond_1

    new-array v3, v6, [Ljava/lang/Integer;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartBtn:Landroid/widget/Button;

    const-string v4, "Start"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v3, "EM/BT/RelayerMode"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v6}, Landroid/app/Activity;->showDialog(I)V

    new-array v3, v6, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const v5, 0x1090009

    const v4, 0x1090008

    const-string v2, "EM/BT/RelayerMode"

    const-string v3, "-->onCreate"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030015

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b00af

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mBauSpinner:Landroid/widget/Spinner;

    const v2, 0x7f0b00b2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mUartSpinner:Landroid/widget/Spinner;

    const v2, 0x7f0b00b3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartBtn:Landroid/widget/Button;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-direct {v0, p0, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-direct {v1, p0, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtRelayerModeActivity;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const-string v1, "EM/BT/RelayerMode"

    const-string v2, "-->onCreateDialog"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v3, :cond_1

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_0

    const v1, 0x7f080263

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const-string v1, "EM/BT/RelayerMode"

    const-string v2, "new ProgressDialog succeed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "EM/BT/RelayerMode"

    const-string v2, "new ProgressDialog failed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
