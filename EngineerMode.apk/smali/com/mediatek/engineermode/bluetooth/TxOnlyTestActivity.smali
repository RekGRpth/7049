.class public Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;
.super Landroid/app/Activity;
.source "TxOnlyTestActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$WorkRunnable;
    }
.end annotation


# static fields
.field private static final BT_TEST_0:I = 0x0

.field private static final BT_TEST_3:I = 0x3

.field private static final CHECK_BT_DEVEICE:I = 0x2

.field private static final CHECK_BT_STATE:I = 0x1

.field public static final DLGID_OP_IN_PROCESS:I = 0x1

.field private static final MAP_TO_CHANNELS:I = 0x1

.field private static final MAP_TO_FREQ:I = 0x3

.field private static final MAP_TO_PATTERN:I = 0x0

.field private static final MAP_TO_POCKET_TYPE:I = 0x2

.field private static final MAP_TO_POCKET_TYPE_LEN:I = 0x4

.field public static final OP_FINISH:I = 0x0

.field public static final OP_IN_PROCESS:I = 0x2

.field public static final OP_TX_FAIL:I = 0x4

.field private static final RETURN_FAIL:I = -0x1

.field private static final TAG:Ljava/lang/String; = "TxOnlyTest"

.field private static final TEST_TX:I = 0x3

.field private static sWorkHandler:Landroid/os/Handler;


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

.field private mChannels:Landroid/widget/Spinner;

.field private mDoneTest:Z

.field private mHasInit:Z

.field private mIniting:Z

.field private mNonModulate:Z

.field private mPattern:Landroid/widget/Spinner;

.field private mPktTypes:Landroid/widget/Spinner;

.field private mPocketType:Z

.field private mStateBt:I

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->sWorkHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPattern:Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mChannels:Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPktTypes:Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mIniting:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mNonModulate:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPocketType:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mDoneTest:Z

    new-instance v0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$1;-><init>(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mDoneTest:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->doSendCommandAction()Z

    move-result v0

    return v0
.end method

.method private doRevertAction()Z
    .locals 1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method private doSendCommandAction()Z
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getBtState()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->enableBluetooth(Z)V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getValuesAndSend()V

    const/4 v0, 0x1

    return v0
.end method

.method private enableBluetooth(Z)V
    .locals 3
    .param p1    # Z

    const-string v1, "TxOnlyTest"

    const-string v2, "Enter EnableBluetooth()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "TxOnlyTest"

    const-string v2, "we can not find a bluetooth adapter."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "TxOnlyTest"

    const-string v2, "Bluetooth is enabled"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    :goto_1
    const-string v1, "TxOnlyTest"

    const-string v2, "Leave EnableBluetooth()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "TxOnlyTest"

    const-string v2, "Bluetooth is disabled"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_1
.end method

.method private getBtState()V
    .locals 3

    const-string v1, "TxOnlyTest"

    const-string v2, "Enter GetBtState()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "TxOnlyTest"

    const-string v2, "we can not find a bluetooth adapter."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mUiHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mStateBt:I

    const-string v1, "TxOnlyTest"

    const-string v2, "Leave GetBtState()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getEditBoxValue(II)Z
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const/4 v4, 0x0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_0
    if-eqz v4, :cond_1

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    move v1, v0

    :goto_0
    return v1

    :cond_2
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    const/4 v6, 0x3

    if-ne v6, p2, :cond_4

    iget-object v6, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v6, v3}, Lcom/mediatek/engineermode/bluetooth/BtTest;->setFreq(I)V

    const/4 v0, 0x1

    :cond_3
    :goto_1
    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v6, "TxOnlyTest"

    const-string v7, "parseInt failed--invalid number!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    goto :goto_0

    :cond_4
    const/4 v6, 0x4

    if-ne v6, p2, :cond_3

    iget-object v6, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v6, v3}, Lcom/mediatek/engineermode/bluetooth/BtTest;->setPocketTypeLen(I)V

    const/4 v0, 0x1

    goto :goto_1
.end method

.method private getSpinnerValue(Landroid/widget/Spinner;I)Z
    .locals 4
    .param p1    # Landroid/widget/Spinner;
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    if-gez v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_1
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v3, v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->setPatter(I)V

    goto :goto_1

    :pswitch_1
    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v3, v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->setChannels(I)V

    goto :goto_1

    :pswitch_2
    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v3, v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->setPocketType(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleNonModulated()V
    .locals 14

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    const-string v5, "TxOnlyTest"

    const-string v6, "-->handleNonModulated TX first"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x5

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    aput-char v9, v0, v10

    const/16 v5, 0x15

    aput-char v5, v0, v9

    const/16 v5, 0xfc

    aput-char v5, v0, v11

    aput-char v9, v0, v12

    aput-char v10, v0, v13

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_1

    const-string v5, "response[%d] = 0x%x"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    aget-char v7, v3, v2

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "TxOnlyTest"

    invoke-static {v5, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "TxOnlyTest"

    const-string v6, "HCICommandRun failed"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v3, 0x0

    const-string v5, "TxOnlyTest"

    const-string v6, "-->handleNonModulated TX second"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x5

    aput-char v9, v0, v10

    const/16 v5, 0xd5

    aput-char v5, v0, v9

    const/16 v5, 0xfc

    aput-char v5, v0, v11

    aput-char v9, v0, v12

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5}, Lcom/mediatek/engineermode/bluetooth/BtTest;->getFreq()I

    move-result v5

    int-to-char v5, v5

    aput-char v5, v0, v13

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_1
    array-length v5, v3

    if-ge v2, v5, :cond_3

    const-string v5, "response[%d] = 0x%x"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    aget-char v7, v3, v2

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "TxOnlyTest"

    invoke-static {v5, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const-string v5, "TxOnlyTest"

    const-string v6, "HCICommandRun failed"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v3, 0x0

    return-void
.end method

.method private initBtTestOjbect()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    const-string v1, "TxOnlyTest"

    const-string v2, "-->initBtTestOjbect"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mIniting:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-nez v1, :cond_2

    new-instance v1, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    if-nez v1, :cond_3

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mIniting:Z

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->init()I

    move-result v1

    if-eqz v1, :cond_4

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    const-string v1, "TxOnlyTest"

    const-string v2, "mBtTest initialization failed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mIniting:Z

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->runHCIResetCmd()V

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mHasInit:Z

    goto :goto_1
.end method

.method private runHCIResetCmd()V
    .locals 12

    const/4 v7, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v5, "TxOnlyTest"

    const-string v6, "-->runHCIResetCmd"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    aput-char v10, v0, v9

    aput-char v7, v0, v10

    const/16 v5, 0xc

    aput-char v5, v0, v11

    aput-char v9, v0, v7

    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-nez v5, :cond_0

    new-instance v5, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v5}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    iput-object v5, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    :cond_0
    iget-object v5, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v5, v0, v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->hciCommandRun([CI)[C

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_2

    const-string v5, "response[%d] = 0x%x"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    aget-char v7, v3, v2

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "TxOnlyTest"

    invoke-static {v5, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v5, "TxOnlyTest"

    const-string v6, "HCICommandRun failed"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v3, 0x0

    return-void
.end method


# virtual methods
.method public getValuesAndSend()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "TxOnlyTest"

    const-string v1, "Enter GetValuesAndSend()."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v0}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPattern:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v3}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getSpinnerValue(Landroid/widget/Spinner;I)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mChannels:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v4}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getSpinnerValue(Landroid/widget/Spinner;I)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPktTypes:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getSpinnerValue(Landroid/widget/Spinner;I)Z

    const v0, 0x7f0b0224

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getEditBoxValue(II)Z

    const v0, 0x7f0b0227

    invoke-direct {p0, v0, v5}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->getEditBoxValue(II)Z

    const-string v0, "TxOnlyTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PocketType().+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->getPocketType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TxOnlyTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "edtFrequency+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->getFreq()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->getPocketType()I

    move-result v1

    if-ne v0, v1, :cond_2

    const-string v0, "TxOnlyTest"

    const-string v1, "enter handleNonModulated(mBtTest)"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TxOnlyTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mbIsNonModulate--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mNonModulate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   mbIsPocketType--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPocketType:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPocketType:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->runHCIResetCmd()V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->initBtTestOjbect()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v4, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mNonModulate:Z

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPocketType:Z

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->handleNonModulated()V

    :cond_1
    :goto_0
    const-string v0, "TxOnlyTest"

    const-string v1, "Leave getValuesAndSend()."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mNonModulate:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->runHCIResetCmd()V

    iput-boolean v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mNonModulate:Z

    :cond_3
    iput-boolean v4, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPocketType:Z

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v1, v3}, Lcom/mediatek/engineermode/bluetooth/BtTest;->doBtTest(I)I

    move-result v1

    if-ne v0, v1, :cond_1

    const-string v0, "TxOnlyTest"

    const-string v1, "transmit data failed."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xb

    iget v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mStateBt:I

    if-eq v0, v1, :cond_4

    const/16 v0, 0xc

    iget v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mStateBt:I

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->enableBluetooth(Z)V

    :cond_5
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_6
    const-string v0, "TxOnlyTest"

    const-string v1, "We cannot find BtTest mBtTestect."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    const-string v0, "TxOnlyTest"

    const-string v1, "-->onCancel"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v1, "TxOnlyTest"

    const-string v2, "-->onCreate"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030059

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->setValuesSpinner()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "TxOnlyTest"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->sWorkHandler:Landroid/os/Handler;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1    # I

    const v6, 0x7f08025e

    const v5, 0x7f08025d

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "TxOnlyTest"

    const-string v2, "-->onCreateDialog"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_0

    const v1, 0x7f080262

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const-string v1, "TxOnlyTest"

    const-string v2, "new ProgressDialog succeed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "TxOnlyTest"

    const-string v2, "new ProgressDialog failed"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    if-ne p1, v4, :cond_2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080260

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$2;-><init>(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080261

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$3;-><init>(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x3

    const-string v0, "TxOnlyTest"

    const-string v1, "TXOnlyTest onDestroy."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->removeDialog(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->doBtTest(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, "TxOnlyTest"

    const-string v1, "stop failed."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void

    :cond_1
    const-string v0, "TxOnlyTest"

    const-string v1, "BtTest does not start yet."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const-string v0, "TxOnlyTest"

    const-string v1, "menu_done is clicked."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mDoneTest:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->sWorkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$WorkRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$WorkRunnable;-><init>(Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    const-string v0, "TxOnlyTest"

    const-string v1, "menu_done is handled."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "TxOnlyTest"

    const-string v1, "last click is not finished yet."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->doRevertAction()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b029c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mDoneTest:Z

    if-nez v1, :cond_0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/Menu;->close()V

    :goto_0
    return v3

    :cond_0
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    const-string v1, "TxOnlyTest"

    const-string v2, "menu_done is not found."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "TxOnlyTest"

    const-string v1, "-->onStart"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method protected setValuesSpinner()V
    .locals 6

    const v5, 0x1090009

    const v4, 0x1090008

    const v3, 0x7f0b0221

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPattern:Landroid/widget/Spinner;

    const v3, 0x7f060006

    invoke-static {p0, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPattern:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v3, 0x7f0b0222

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mChannels:Landroid/widget/Spinner;

    const v3, 0x7f060007

    invoke-static {p0, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mChannels:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v3, 0x7f0b0225

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPktTypes:Landroid/widget/Spinner;

    const v3, 0x7f060008

    invoke-static {p0, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/TxOnlyTestActivity;->mPktTypes:Landroid/widget/Spinner;

    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method
