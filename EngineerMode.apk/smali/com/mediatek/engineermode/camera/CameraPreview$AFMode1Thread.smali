.class Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;
.super Ljava/lang/Thread;
.source "CameraPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera/CameraPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AFMode1Thread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera/CameraPreview;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/camera/CameraPreview;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/16 v9, 0x3e9

    const/16 v8, 0x32

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v2, "EM/Camera"

    const-string v3, "mAFMode1Thread"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2, v6}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1700(Lcom/mediatek/engineermode/camera/CameraPreview;Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    sput-boolean v7, Lcom/mediatek/engineermode/camera/CameraPreview;->sCanBack:Z

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    const-string v4, "manual"

    invoke-static {v2, v3, v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1900(Lcom/mediatek/engineermode/camera/CameraPreview;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3ee

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2202(Lcom/mediatek/engineermode/camera/CameraPreview;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2300(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$AutoFocusCallback;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    :goto_1
    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2400(Lcom/mediatek/engineermode/camera/CameraPreview;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2808(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    const-string v4, "manual"

    invoke-static {v2, v3, v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1900(Lcom/mediatek/engineermode/camera/CameraPreview;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/hardware/Camera$Parameters;->setFocusEngMode(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2800(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    move-result v3

    add-int/lit8 v3, v3, -0x18

    iget-object v4, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2900(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    move-result v4

    mul-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setFocusEngStep(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2500(Lcom/mediatek/engineermode/camera/CameraPreview;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3200(Lcom/mediatek/engineermode/camera/CameraPreview;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3300(Lcom/mediatek/engineermode/camera/CameraPreview;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3400(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3500(Lcom/mediatek/engineermode/camera/CameraPreview;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3102(Lcom/mediatek/engineermode/camera/CameraPreview;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    const-string v3, "rawfname"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v5}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3100(Lcom/mediatek/engineermode/camera/CameraPreview;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".raw"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    sput-boolean v6, Lcom/mediatek/engineermode/camera/CameraPreview;->sCanBack:Z

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    const-wide/16 v3, 0x7d0

    invoke-static {v2, v3, v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1600(Lcom/mediatek/engineermode/camera/CameraPreview;J)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2800(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    move-result v2

    if-ge v2, v8, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2800(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    move-result v2

    if-lt v2, v8, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1002(Lcom/mediatek/engineermode/camera/CameraPreview;Z)Z

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    const-string v4, "manual"

    invoke-static {v2, v3, v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1900(Lcom/mediatek/engineermode/camera/CameraPreview;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$3000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3ef

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_4
    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1100(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$ProgressDlgHandler;

    move-result-object v2

    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode1Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v2, v6}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2202(Lcom/mediatek/engineermode/camera/CameraPreview;I)I

    goto/16 :goto_1
.end method
