.class Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;
.super Ljava/lang/Thread;
.source "CameraPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera/CameraPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AFMode2Thread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera/CameraPreview;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/camera/CameraPreview;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/16 v5, 0x64

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "EM/Camera"

    const-string v1, "mAFMode2Thread"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1700(Lcom/mediatek/engineermode/camera/CameraPreview;Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1002(Lcom/mediatek/engineermode/camera/CameraPreview;Z)Z

    sput-boolean v3, Lcom/mediatek/engineermode/camera/CameraPreview;->sCanBack:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2702(Lcom/mediatek/engineermode/camera/CameraPreview;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "fullscan"

    invoke-static {v0, v1, v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1900(Lcom/mediatek/engineermode/camera/CameraPreview;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2700(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusEngMode(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2900(Lcom/mediatek/engineermode/camera/CameraPreview;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusEngStep(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v1, "focus-mode"

    const-string v2, "fullscan"

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0, v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2202(Lcom/mediatek/engineermode/camera/CameraPreview;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2300(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$AutoFocusCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1100(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$ProgressDlgHandler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    sput-boolean v4, Lcom/mediatek/engineermode/camera/CameraPreview;->sCanBack:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2400(Lcom/mediatek/engineermode/camera/CameraPreview;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2500(Lcom/mediatek/engineermode/camera/CameraPreview;)V

    sput-boolean v3, Lcom/mediatek/engineermode/camera/CameraPreview;->sCanBack:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0, v4}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1002(Lcom/mediatek/engineermode/camera/CameraPreview;Z)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    const-wide/16 v1, 0x7d0

    invoke-static {v0, v1, v2}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1600(Lcom/mediatek/engineermode/camera/CameraPreview;J)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1100(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$ProgressDlgHandler;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    const-string v0, "EM/Camera"

    const-string v1, "AFMode2Thread does not support fullscan mode."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1100(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$ProgressDlgHandler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2100(Lcom/mediatek/engineermode/camera/CameraPreview;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2000(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1800(Lcom/mediatek/engineermode/camera/CameraPreview;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$2202(Lcom/mediatek/engineermode/camera/CameraPreview;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/camera/CameraPreview$AFMode2Thread;->this$0:Lcom/mediatek/engineermode/camera/CameraPreview;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera/CameraPreview;->access$1100(Lcom/mediatek/engineermode/camera/CameraPreview;)Lcom/mediatek/engineermode/camera/CameraPreview$ProgressDlgHandler;

    move-result-object v0

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
