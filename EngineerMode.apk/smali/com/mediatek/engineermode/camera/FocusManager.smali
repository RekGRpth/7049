.class public Lcom/mediatek/engineermode/camera/FocusManager;
.super Ljava/lang/Object;
.source "FocusManager.java"


# instance fields
.field private mAeAwbLock:Z

.field private mFocusAreaSupported:Z

.field private mLockAeAwbNeeded:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getAeAwbLock()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mAeAwbLock:Z

    return v0
.end method

.method public initializeParameters(Landroid/hardware/Camera$Parameters;)V
    .locals 5
    .param p1    # Landroid/hardware/Camera$Parameters;

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object v0, p1

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v1

    if-lez v1, :cond_2

    const-string v1, "auto"

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/camera/FocusManager;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mFocusAreaSupported:Z

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    iput-boolean v3, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mLockAeAwbNeeded:Z

    return-void

    :cond_2
    move v1, v3

    goto :goto_0
.end method

.method public ismFocusAreaSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mFocusAreaSupported:Z

    return v0
.end method

.method public ismLockAeAwbNeeded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mLockAeAwbNeeded:Z

    return v0
.end method

.method public setAeAwbLock(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mAeAwbLock:Z

    return-void
.end method

.method public setmFocusAreaSupported(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mFocusAreaSupported:Z

    return-void
.end method

.method public setmLockAeAwbNeeded(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera/FocusManager;->mLockAeAwbNeeded:Z

    return-void
.end method
