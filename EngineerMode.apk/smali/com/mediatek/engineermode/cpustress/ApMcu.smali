.class public Lcom/mediatek/engineermode/cpustress/ApMcu;
.super Landroid/app/Activity;
.source "ApMcu.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;


# static fields
.field public static final BIT_DUAL_CORE:I = 0x1f

.field private static final DIALOG_WAIT:I = 0x3e9

.field private static final INDEX_UPDATE_RESULT:I = 0x1

.field public static final MASK_CA9:I = 0x4

.field public static final MASK_CA9_0:I = 0x5

.field public static final MASK_CA9_1:I = 0x6

.field public static final MASK_CB:I = 0x7

.field public static final MASK_L2C:I = 0x0

.field public static final MASK_NEON:I = 0x1

.field public static final MASK_NEON_0:I = 0x2

.field public static final MASK_NEON_1:I = 0x3

.field private static final PERCENT:D = 100.0

.field private static final TAG:Ljava/lang/String; = "EM/CpuStress_ApMcu"


# instance fields
.field private mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

.field private mBtnStart:Landroid/widget/Button;

.field private mCbCa9:Landroid/widget/CheckBox;

.field private mCbL2C:Landroid/widget/CheckBox;

.field private mCbNeon:Landroid/widget/CheckBox;

.field private mEtLoopCount:Landroid/widget/EditText;

.field private final mHandler:Landroid/os/Handler;

.field private mTvResult:Landroid/widget/TextView;

.field private mTvResultCA9:Landroid/widget/TextView;

.field private mTvResultCA91:Landroid/widget/TextView;

.field private mTvResultL2C:Landroid/widget/TextView;

.field private mTvResultNeon:Landroid/widget/TextView;

.field private mTvResultNeon1:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultL2C:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon1:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA91:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    new-instance v0, Lcom/mediatek/engineermode/cpustress/ApMcu$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/ApMcu$1;-><init>(Lcom/mediatek/engineermode/cpustress/ApMcu;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/ApMcu;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ApMcu;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateTestResult()V

    return-void
.end method

.method private updateStartUi()V
    .locals 3

    const v2, 0x7f080108

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f080119

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultL2C:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA91:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateTestCount(IIIIII)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const-string v1, "EM/CpuStress_ApMcu"

    const-string v2, "Enter updateTestResult: %1$d, %2$d, %3$d, %4$d, %5$d, %6$d"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz p1, :cond_0

    const v1, 0x7f08011b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L

    mul-double/2addr v4, v6

    int-to-double v6, p1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    if-eqz p3, :cond_1

    const v1, 0x7f08011c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    int-to-double v4, p4

    const-wide/high16 v6, 0x4059000000000000L

    mul-double/2addr v4, v6

    int-to-double v6, p3

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    if-eqz p5, :cond_2

    const v1, 0x7f08011d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    int-to-double v4, p6

    const-wide/high16 v6, 0x4059000000000000L

    mul-double/2addr v4, v6

    int-to-double v6, p5

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTestResult()V
    .locals 19

    const-string v1, "EM/CpuStress_ApMcu"

    const-string v2, "Enter updateTestResult"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->getData()Landroid/os/Bundle;

    move-result-object v17

    if-eqz v17, :cond_0

    const-string v1, "result_l2c"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v1, "result_neon"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v1, "result_ca9"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v1, "run"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v1, "loopcount"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v1, "mask"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v1, "result"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateTestUi(ZJIIIII)V

    const-string v1, "result_pass_l2c"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    const-string v1, "result_pass_neon"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    const-string v1, "result_pass_ca9"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v10, p0

    move v11, v7

    move v13, v8

    move v15, v9

    invoke-direct/range {v10 .. v16}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateTestCount(IIIIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v18

    const-string v1, "EM/CpuStress_ApMcu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateTestUI NullPointerException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateTestUi(ZJIIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    const-string v0, "EM/CpuStress_ApMcu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateTestUI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    if-nez p1, :cond_9

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    if-eqz p1, :cond_a

    const v0, 0x7f080119

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    and-int/lit8 v0, p4, 0xe

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    and-int/lit8 v0, p4, 0x70

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    if-gtz p6, :cond_0

    if-gtz p7, :cond_0

    if-lez p8, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultL2C:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x1

    if-nez v0, :cond_e

    const v0, 0x7f08010a

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    const/high16 v0, -0x80000000

    and-int/2addr v0, p5

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_15

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_6
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultL2C:Landroid/widget/TextView;

    const v1, 0x7f080108

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_3
    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    const v1, 0x7f080108

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon1:Landroid/widget/TextView;

    const v1, 0x7f080108

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_5
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    const v1, 0x7f080108

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA91:Landroid/widget/TextView;

    const v1, 0x7f080108

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_7
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    if-nez v0, :cond_8

    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->removeDialog(I)V

    :cond_8
    return-void

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_a
    const v0, 0x7f080118

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_e
    const v0, 0x7f080109

    goto :goto_5

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x2

    if-nez v0, :cond_f

    const v0, 0x7f08010a

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x10

    if-nez v0, :cond_10

    const v0, 0x7f08010a

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :cond_f
    const v0, 0x7f080109

    goto :goto_7

    :cond_10
    const v0, 0x7f080109

    goto :goto_8

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x4

    if-nez v0, :cond_11

    const v0, 0x7f08010a

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x20

    if-nez v0, :cond_12

    const v0, 0x7f08010a

    :goto_a
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon1:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x8

    if-nez v0, :cond_13

    const v0, 0x7f08010a

    :goto_b
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA91:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x40

    if-nez v0, :cond_14

    const v0, 0x7f08010a

    :goto_c
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_6

    :cond_11
    const v0, 0x7f080109

    goto :goto_9

    :cond_12
    const v0, 0x7f080109

    goto :goto_a

    :cond_13
    const v0, 0x7f080109

    goto :goto_b

    :cond_14
    const v0, 0x7f080109

    goto :goto_c

    :cond_15
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x2

    if-nez v0, :cond_16

    const v0, 0x7f08010a

    :goto_d
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x10

    if-nez v0, :cond_17

    const v0, 0x7f08010a

    :goto_e
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_6

    :cond_16
    const v0, 0x7f080109

    goto :goto_d

    :cond_17
    const v0, 0x7f080109

    goto :goto_e

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0xe

    :goto_1
    or-int/2addr v2, v4

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x70

    :cond_0
    or-int v1, v2, v3

    const-string v2, "EM/CpuStress_ApMcu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCheckChanged, mask: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "mask"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v2, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateData(Landroid/os/Bundle;)Landroid/os/Bundle;

    :cond_1
    return-void

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    move v4, v3

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    if-ne v4, p1, :cond_4

    const-string v4, "EM/CpuStress_ApMcu"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is clicked"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f080118

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-wide/16 v0, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v4, "loopcount"

    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v7, "mask"

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_2

    const/16 v6, 0xe

    :goto_1
    or-int/2addr v4, v6

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x70

    :cond_0
    or-int/2addr v4, v5

    invoke-virtual {v2, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v4, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->startTest(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateStartUi()V

    :goto_2
    return-void

    :catch_0
    move-exception v3

    const-string v4, "EM/CpuStress_ApMcu"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Loopcount string: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v4, 0x7f080104

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_2

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    move v6, v5

    goto :goto_1

    :cond_3
    const/16 v4, 0x3e9

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->stopTest()V

    goto :goto_2

    :cond_4
    const-string v4, "EM/CpuStress_ApMcu"

    const-string v5, "Unknown event"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0131

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    const v0, 0x7f0b0132

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    const v0, 0x7f0b0134

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    const v0, 0x7f0b0137

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    const v0, 0x7f0b0133

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultL2C:Landroid/widget/TextView;

    const v0, 0x7f0b0135

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon:Landroid/widget/TextView;

    const v0, 0x7f0b0138

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA9:Landroid/widget/TextView;

    const v0, 0x7f0b0136

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultNeon1:Landroid/widget/TextView;

    const v0, 0x7f0b0139

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResultCA91:Landroid/widget/TextView;

    const v0, 0x7f0b013a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    const v0, 0x7f0b013b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbL2C:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbNeon:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbCa9:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    const/16 v1, 0x3e9

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0800fe

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f0800ff

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080100

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f080101

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "EM/CpuStress_ApMcu"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    invoke-virtual {p2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;->getService()Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iput-object p0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "EM/CpuStress_ApMcu"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p0, v1}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-virtual {p0, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onUpdateTestResult()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
