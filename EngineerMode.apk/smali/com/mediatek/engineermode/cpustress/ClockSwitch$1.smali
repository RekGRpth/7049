.class Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;
.super Landroid/os/Handler;
.source "ClockSwitch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/cpustress/ClockSwitch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v0, "EM/CpuStress_ClockSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHandler receive message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "EM/CpuStress_ClockSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msg.what: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msg.obj: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-static {v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$100(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->removeDialog(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$000()[Ljava/lang/String;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    rem-int/lit8 v3, v3, 0xa

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-static {v0, v4}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$176(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)I

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$176(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$200(Lcom/mediatek/engineermode/cpustress/ClockSwitch;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$176(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$300(Lcom/mediatek/engineermode/cpustress/ClockSwitch;Z)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$176(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-static {v0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$400(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-static {v0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$400(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;->this$0:Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-static {v0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->access$400(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
