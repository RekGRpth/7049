.class Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;
.super Landroid/os/Handler;
.source "CpuStressTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/cpustress/CpuStressTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "EM/CpuStress"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mHandler receive message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/io/File;

    const-string v3, "/etc/.tp/.ht120.mtc"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    sput-boolean v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    sget-object v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/CheckBox;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalDisabled:Z

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/CheckBox;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v2, :pswitch_data_1

    :goto_1
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$500(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->isTestRun()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->updateRadioGroup(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :pswitch_3
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :pswitch_4
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v2, :pswitch_data_2

    goto/16 :goto_1

    :pswitch_5
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_1

    :pswitch_6
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_1
    move v0, v1

    goto/16 :goto_2

    :pswitch_7
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;->this$0:Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    invoke-static {v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->access$500(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->isTestRun()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_3
    invoke-virtual {v2, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->updateRadioGroup(Z)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
