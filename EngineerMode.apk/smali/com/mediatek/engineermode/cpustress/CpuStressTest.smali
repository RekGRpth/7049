.class public Lcom/mediatek/engineermode/cpustress/CpuStressTest;
.super Landroid/app/Activity;
.source "CpuStressTest.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;


# static fields
.field private static final DIALOG_WAIT:I = 0x3e9

.field private static final ERROR:Ljava/lang/String; = "ERROR"

.field private static final HQA_CPU_STRESS_TEST_ITEMS:[Ljava/lang/String;

.field public static final INDEX_DEFAULT:I = 0x0

.field public static final INDEX_DUAL:I = 0x3

.field public static final INDEX_SINGLE:I = 0x2

.field public static final INDEX_TEST:I = 0x1

.field private static final INDEX_UPDATE_RADIOBTN:I = 0x1

.field private static final INDEX_UPDATE_RADIOGROUP:I = 0x2

.field private static final ITEM_COUNT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "EM/CpuStress"

.field public static final TEST_BACKUP:I = 0x14

.field public static final TEST_BACKUP_DUAL:I = 0x17

.field public static final TEST_BACKUP_SINGLE:I = 0x16

.field public static final TEST_BACKUP_TEST:I = 0x15

.field public static final TEST_RESTORE:I = 0x18

.field public static final TEST_RESTORE_DUAL:I = 0x1b

.field public static final TEST_RESTORE_SINGLE:I = 0x1a

.field public static final TEST_RESTORE_TEST:I = 0x19

.field private static final TYPE_LOAD_ENG:Ljava/lang/String; = "eng"


# instance fields
.field private mCbThermal:Landroid/widget/CheckBox;

.field private final mHandler:Landroid/os/Handler;

.field private mListCpuTestItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRbDefault:Landroid/widget/RadioButton;

.field private mRbDual:Landroid/widget/RadioButton;

.field private mRbSingle:Landroid/widget/RadioButton;

.field private mRbTest:Landroid/widget/RadioButton;

.field private mRgRadioGroup:Landroid/widget/RadioGroup;

.field private mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->HQA_CPU_STRESS_TEST_ITEMS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRgRadioGroup:Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDefault:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbTest:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbSingle:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDual:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    new-instance v0, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTest$1;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDefault:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbTest:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbSingle:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDual:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/cpustress/CpuStressTest;)Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    return-object v0
.end method

.method private doThermalDisable(Z)V
    .locals 9
    .param p1    # Z

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-string v4, "EM/CpuStress"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Enter doThermalDisable: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean p1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalDisabled:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    const v4, 0x9c44

    invoke-virtual {v1, v4}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v3

    invoke-virtual {v1, v6}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    if-eqz p1, :cond_2

    move v4, v5

    :goto_0
    invoke-virtual {v1, v4}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamInt(I)Z

    if-eqz v3, :cond_4

    :cond_0
    invoke-virtual {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v2

    iget-object v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_1
    iget v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_1

    const-string v4, "EM/CpuStress"

    const-string v6, "AFMFunctionCallEx: RESULT_IO_ERR"

    invoke-static {v4, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    const-string v6, "ERROR"

    invoke-virtual {v0, v5, v4, v6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    const-string v4, "EM/CpuStress"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doThermalDisable response: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    move v4, v6

    goto :goto_0

    :cond_3
    iget-object v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-eq v4, v6, :cond_0

    goto :goto_1

    :cond_4
    const-string v4, "EM/CpuStress"

    const-string v5, "AFMFunctionCallEx return false"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "ERROR"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_1

    const-string v0, "EM/CpuStress"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check box checked: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p2}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->doThermalDisable(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "EM/CpuStress"

    const-string v1, "Unknown checkbox"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    const-string v0, "EM/CpuStress"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter onCheckedChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDefault:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->setIndexMode(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbTest:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->setIndexMode(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbSingle:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->setIndexMode(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDual:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->setIndexMode(I)V

    goto :goto_0

    :cond_3
    const-string v0, "EM/CpuStress"

    const-string v1, "Unknown checked Id"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f03002c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const-string v2, "eng"

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x7f0800fd

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    const-string v2, "EM/CpuStress"

    const-string v3, "Not eng load, finish"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->HQA_CPU_STRESS_TEST_ITEMS:[Ljava/lang/String;

    const v3, 0x7f080112

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v2, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->HQA_CPU_STRESS_TEST_ITEMS:[Ljava/lang/String;

    const v3, 0x7f08012a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    sget-object v2, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->HQA_CPU_STRESS_TEST_ITEMS:[Ljava/lang/String;

    const/4 v3, 0x2

    const v4, 0x7f08011e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const v2, 0x7f0b012a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const v2, 0x7f0b012b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRgRadioGroup:Landroid/widget/RadioGroup;

    const v2, 0x7f0b012c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDefault:Landroid/widget/RadioButton;

    const v2, 0x7f0b012d

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbTest:Landroid/widget/RadioButton;

    const v2, 0x7f0b012e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbSingle:Landroid/widget/RadioButton;

    const v2, 0x7f0b012f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDual:Landroid/widget/RadioButton;

    const v2, 0x7f0b0130

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    new-instance v2, Ljava/util/ArrayList;

    sget-object v3, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->HQA_CPU_STRESS_TEST_ITEMS:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    const v3, 0x7f080423

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x1090003

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRgRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0, v5}, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->updateRadioGroup(Z)V

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-string v2, "EM/CpuStress"

    const-string v3, "start cpu test service"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    const/16 v1, 0x3e9

    if-ne v1, p1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0800fe

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f0800ff

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080100

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f080101

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const v5, 0x7f080423

    const/4 v4, 0x0

    const/4 v0, 0x0

    const-string v2, "EM/CpuStress"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User select: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/FeatureHelpPage;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "helpTitle"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "helpMessage"

    const v2, 0x7f080132

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    sget v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    if-nez v1, :cond_1

    const v1, 0x7f080102

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v1, "EM/CpuStress"

    const-string v2, "Not select mode"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const v2, 0x7f080112

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/ApMcu;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_2
    :goto_1
    if-nez v0, :cond_5

    const v1, 0x7f080103

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v1, "EM/CpuStress"

    const-string v2, "Select error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const v2, 0x7f08012a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mListCpuTestItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const v2, 0x7f08011e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    if-le v1, v2, :cond_2

    sget-object v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f080105

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "EM/CpuStress"

    const-string v1, "Enter onServiceConnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    invoke-virtual {p2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;->getService()Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iput-object p0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "EM/CpuStress"

    const-string v1, "Enter onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mTestService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p0, v1}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-virtual {p0, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onUpdateTestResult()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected updateRadioGroup(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDefault:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbTest:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbSingle:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mRbDual:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    if-eqz p1, :cond_0

    sget-boolean v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTest;->mCbThermal:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->removeDialog(I)V

    return-void
.end method
