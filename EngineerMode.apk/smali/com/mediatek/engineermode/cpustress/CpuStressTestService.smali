.class public Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
.super Landroid/app/Service;
.source "CpuStressTestService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;
    }
.end annotation


# static fields
.field private static final CPU_1_ONLINE_PATH:Ljava/lang/String; = "/sys/devices/system/cpu/cpu1/online"

.field protected static final DUALCORE_MASK:I = -0x80000000

.field private static final FAIL:Ljava/lang/String; = "FAIL"

.field private static final HANDLER_THREAD_NAME_APMCU:Ljava/lang/String; = "ApMcu"

.field private static final HANDLER_THREAD_NAME_BACKUPRESTORE:Ljava/lang/String; = "BackupRestore"

.field private static final HANDLER_THREAD_NAME_VIDEO:Ljava/lang/String; = "VideoCodec"

.field private static final INDEX_TEST_APMCU:I = 0x1

.field private static final INDEX_TEST_BACKUP:I = 0x3

.field private static final INDEX_TEST_RESTORE:I = 0x4

.field private static final INDEX_TEST_VIDEOCODEC:I = 0x2

.field private static final LOOPCOUNT_DEFAULT_VALUE:J = 0x5f5e0ffL

.field private static final PASS:Ljava/lang/String; = "PASS"

.field public static final RESULT_CA9:Ljava/lang/String; = "result_ca9"

.field private static final RESULT_ERROR:Ljava/lang/String; = "ERROR"

.field public static final RESULT_L2C:Ljava/lang/String; = "result_l2c"

.field public static final RESULT_NEON:Ljava/lang/String; = "result_neon"

.field public static final RESULT_PASS_CA9:Ljava/lang/String; = "result_pass_ca9"

.field public static final RESULT_PASS_L2C:Ljava/lang/String; = "result_pass_l2c"

.field public static final RESULT_PASS_NEON:Ljava/lang/String; = "result_pass_neon"

.field public static final RESULT_PASS_VIDEOCODEC:Ljava/lang/String; = "result_pass_video_codec"

.field private static final RESULT_SEPARATE:Ljava/lang/String; = ";"

.field public static final RESULT_VIDEOCODEC:Ljava/lang/String; = "result_video_codec"

.field private static final SKIP:Ljava/lang/String; = "CPU1 is powered off"

.field private static final TAG:Ljava/lang/String; = "EM/CpuStressTestService"

.field protected static final THERMAL_ETC_FILE:Ljava/lang/String; = "/etc/.tp/.ht120.mtc"

.field private static final TIME_DELAYED:I = 0x64

.field public static final VALUE_ITERATION:Ljava/lang/String; = "iteration"

.field public static final VALUE_LOOPCOUNT:Ljava/lang/String; = "loopcount"

.field public static final VALUE_MASK:Ljava/lang/String; = "mask"

.field public static final VALUE_RESULT:Ljava/lang/String; = "result"

.field public static final VALUE_RUN:Ljava/lang/String; = "run"

.field protected static sIndexMode:I

.field protected static sIsDualCore:Ljava/lang/Boolean;

.field protected static sIsThermalDisabled:Z

.field protected static sIsThermalSupport:Z


# instance fields
.field private final mBinder:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

.field private mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

.field private mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

.field private final mHandlerThreadApMcu:Landroid/os/HandlerThread;

.field private final mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

.field private final mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

.field private mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

.field private mIterationVideoCodec:I

.field private mLoopCountApMcu:J

.field private mLoopCountVideoCodec:J

.field private mResultApMcu:I

.field private mResultPassCa9:I

.field private mResultPassL2C:I

.field private mResultPassNeon:I

.field private mResultPassVideoCodec:I

.field private mResultTotalCa9:I

.field private mResultTotalL2C:I

.field private mResultTotalNeon:I

.field private mResultTotalVideoCodec:I

.field private mResultVideoCodec:I

.field private mTestApMcuMask:I

.field private mTestApMcuRunning:Z

.field protected mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

.field private mTestClockSwitchRunning:Z

.field private mTestVideoCodecRunning:Z

.field private mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

.field protected mWantStopApmcu:Z

.field protected mWantStopSwCodec:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    sput-boolean v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    sput-boolean v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalDisabled:Z

    sput v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const-wide/32 v3, 0x5f5e0ff

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-wide v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    iput-wide v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mIterationVideoCodec:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    new-instance v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mBinder:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ApMcu"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "VideoCodec"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BackupRestore"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    return-wide p1
.end method

.method static synthetic access$010(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 4
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    return-wide p1
.end method

.method static synthetic access$310(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 4
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doVideoCodecTest()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doBackupRestore(I)V

    return-void
.end method

.method private backup(I)V
    .locals 4
    .param p1    # I

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter backup: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    add-int/lit8 v1, p1, 0x14

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private coreNum()I
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/system/cpu/cpu1/online"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private dataGenerator(I)Landroid/os/Bundle;
    .locals 4
    .param p1    # I

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dataGenerator index is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v1, "run"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "loopcount"

    iget-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "mask"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_l2c"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_l2c"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_neon"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_neon"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_ca9"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_ca9"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_1
    const-string v1, "run"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "loopcount"

    iget-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "iteration"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mIterationVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_video_codec"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_video_codec"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private doApMcuTest()V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x1

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "enter doApMpuTest"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_0
    sget-object v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    const-string v0, "EM/CpuStressTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "iResultApMpu is 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_0
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_2
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_3
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_5
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private doApMcuTest(I)V
    .locals 14
    .param p1    # I

    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v11, 0x1

    const-string v8, "EM/CpuStressTestService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doApMpuTest index is: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    const v8, 0x9c41

    invoke-virtual {v2, v8}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v5

    invoke-virtual {v2, v11}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    invoke-virtual {v2, p1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamInt(I)Z

    if-eqz v5, :cond_4

    :cond_0
    invoke-virtual {v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v3

    iget-object v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_0
    iget v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_1

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "AFMFunctionCallEx: RESULT_IO_ERR"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    const-string v9, "ERROR"

    invoke-virtual {v1, v12, v8, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v8, "EM/CpuStressTestService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doApMcuTest response: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v4, :cond_5

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-eq v8, v11, :cond_0

    goto :goto_0

    :cond_4
    const-string v8, "EM/CpuStressTestService"

    const-string v9, "AFMFunctionCallEx return false"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "ERROR"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    :pswitch_1
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    const-string v8, "PASS"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    goto :goto_2

    :cond_6
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x2

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_2

    :pswitch_2
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    const-string v8, "PASS"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x2

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_2

    :cond_7
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x3

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_2

    :pswitch_3
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    const-string v8, "PASS"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x10

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_2

    :cond_8
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x11

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto/16 :goto_2

    :pswitch_4
    const-string v8, ";"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    array-length v8, v6

    if-ne v13, v8, :cond_c

    const/4 v0, 0x1

    aget-object v8, v6, v12

    const-string v9, "PASS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x4

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_3
    aget-object v8, v6, v11

    const-string v9, "PASS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x8

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_4
    if-eqz v0, :cond_2

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    goto/16 :goto_2

    :cond_9
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x5

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    aget-object v8, v6, v11

    const-string v9, "CPU1 is powered off"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "NEON test, CPU1 OFFLINE, skip"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x8

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_4

    :cond_b
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x9

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const/4 v0, 0x0

    goto :goto_4

    :cond_c
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x5

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x9

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto/16 :goto_2

    :pswitch_5
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    const-string v8, ";"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    if-ne v13, v8, :cond_10

    const/4 v0, 0x1

    aget-object v8, v7, v12

    const-string v9, "PASS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x20

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_5
    aget-object v8, v7, v11

    const-string v9, "PASS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_e

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x40

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_6
    if-eqz v0, :cond_2

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    goto/16 :goto_2

    :cond_d
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x21

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const/4 v0, 0x0

    goto :goto_5

    :cond_e
    aget-object v8, v7, v11

    const-string v9, "CPU1 is powered off"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_f

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "CA9 test, CPU1 OFFLINE, skip"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/lit8 v8, v8, 0x40

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_6

    :cond_f
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x41

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const/4 v0, 0x0

    goto :goto_6

    :cond_10
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x21

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    and-int/lit8 v8, v8, -0x41

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private doBackupRestore(I)V
    .locals 8
    .param p1    # I

    const/4 v7, 0x1

    const-string v4, "EM/CpuStressTestService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Enter doBackupRestore: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    const v4, 0x9c43

    invoke-virtual {v1, v4}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v3

    invoke-virtual {v1, v7}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    invoke-virtual {v1, p1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamInt(I)Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_3

    :cond_0
    invoke-virtual {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v2

    iget-object v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_0
    iget v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    const-string v4, "EM/CpuStressTestService"

    const-string v5, "AFMFunctionCallEx: RESULT_IO_ERR"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const-string v6, "ERROR"

    invoke-virtual {v0, v4, v5, v6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    const-string v4, "EM/CpuStressTestService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doBackupRestore: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iget-object v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v2, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-eq v4, v7, :cond_0

    goto :goto_0

    :cond_3
    const-string v4, "EM/CpuStressTestService"

    const-string v5, "AFMFunctionCallEx return false"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "ERROR"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private doVideoCodecTest()V
    .locals 14

    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x1

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "enter doVideoCodecTest"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, ""

    const/4 v7, -0x1

    sget-object v8, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_1

    sget v8, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v8, :pswitch_data_0

    :goto_0
    if-ne v10, v7, :cond_2

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    const/4 v7, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v7, 0x2

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    const v8, 0x9c42

    invoke-virtual {v2, v8}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v5

    invoke-virtual {v2, v13}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    invoke-virtual {v2, v7}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamInt(I)Z

    invoke-virtual {v2, v11}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamInt(I)Z

    if-eqz v5, :cond_6

    :cond_3
    invoke-virtual {v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v3

    iget-object v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    :goto_2
    iget v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-ne v8, v10, :cond_4

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "AFMFunctionCallEx: RESULT_IO_ERR"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    const-string v9, "ERROR"

    invoke-virtual {v1, v12, v8, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    :goto_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v8, "EM/CpuStressTestService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doVideoCodecTest response: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v8, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_c

    sget v8, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v8, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    const-string v8, ";"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v8, v6

    if-ne v13, v8, :cond_b

    const/4 v0, 0x1

    aget-object v8, v6, v12

    const-string v9, "PASS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/lit8 v8, v8, 0x4

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    :goto_4
    aget-object v8, v6, v11

    const-string v9, "PASS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/lit8 v8, v8, 0x8

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    :goto_5
    if-eqz v0, :cond_0

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    goto/16 :goto_1

    :cond_5
    iget-object v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v3, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    if-eq v8, v11, :cond_3

    goto/16 :goto_2

    :cond_6
    const-string v8, "EM/CpuStressTestService"

    const-string v9, "AFMFunctionCallEx return false"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "ERROR"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :pswitch_3
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    const-string v8, "PASS"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/lit8 v8, v8, 0x2

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    goto/16 :goto_1

    :cond_7
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    and-int/lit8 v8, v8, -0x3

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    goto/16 :goto_1

    :cond_8
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    and-int/lit8 v8, v8, -0x5

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const/4 v0, 0x0

    goto :goto_4

    :cond_9
    aget-object v8, v6, v11

    const-string v9, "CPU1 is powered off"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_a

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "SWCODEC test, CPU1 OFFLINE, skip"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/lit8 v8, v8, 0x8

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    goto :goto_5

    :cond_a
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    and-int/lit8 v8, v8, -0x9

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const/4 v0, 0x0

    goto :goto_5

    :cond_b
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    and-int/lit8 v8, v8, -0x5

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    and-int/lit8 v8, v8, -0x9

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    goto/16 :goto_1

    :cond_c
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    const-string v8, "PASS"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    goto/16 :goto_1

    :cond_d
    iget v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    and-int/lit8 v8, v8, -0x2

    iput v8, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private restore(I)V
    .locals 4
    .param p1    # I

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter restore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    add-int/lit8 v1, p1, 0x18

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public getData()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateData(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public isClockSwitchRun()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    return v0
.end method

.method public isTestRun()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mBinder:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 6

    const/high16 v4, -0x80000000

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->coreNum()I

    move-result v0

    if-eq v0, v3, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    sget-object v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    :cond_0
    new-instance v2, Ljava/io/File;

    const-string v4, "/etc/.tp/.ht120.mtc"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    sput-boolean v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;
    :try_end_0
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "EM/CpuStressTestService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Core Number: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "EM/CpuStressTestService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Handler thread IllegalThreadStateException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f080111

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->restore(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;->release()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "Enter onStartCommand"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    return v0
.end method

.method public setIndexMode(I)V
    .locals 3
    .param p1    # I

    const-string v0, "EM/CpuStressTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIndexMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sIndexMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->backup(I)V

    :goto_1
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    if-nez p1, :cond_2

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->restore(I)V

    goto :goto_1

    :cond_2
    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->restore(I)V

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->backup(I)V

    goto :goto_1
.end method

.method public startTest(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/high16 v4, -0x80000000

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "Enter startTest"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_3

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "startTest for ApMcu"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    if-eqz v0, :cond_1

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "ApMpu test is running"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "loopcount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    const-string v0, "mask"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    sget-object v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    or-int/2addr v0, v4

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :cond_2
    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_6

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "startTest for SwVideoCodec"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    if-eqz v0, :cond_4

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "VideoCodec test is running"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v0, "loopcount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    const-string v0, "iteration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mIterationVideoCodec:I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    sget-object v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsDualCore:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    or-int/2addr v0, v4

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    :cond_5
    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    if-eqz v0, :cond_0

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "startTest for ClockSwitch"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    goto/16 :goto_0
.end method

.method public stopTest()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "EM/CpuStressTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter stopTest, testObject is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_1

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "stopTest for ApMcu"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_2

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "stopTest for SwVideoCodec"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    if-eqz v0, :cond_0

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "stopTest for ClockSwitch"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    goto :goto_0
.end method

.method public updateData(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    const-string v2, "EM/CpuStressTestService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateData, data is null ? "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->dataGenerator(I)Landroid/os/Bundle;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->dataGenerator(I)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_4

    const-string v0, "mask"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    :cond_3
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_3

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "VideoCodec test not need to update config"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public updateWakeLock()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->isTestRun()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-virtual {v0, p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;->acquire(Landroid/content/Context;)V

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
