.class public Lcom/mediatek/engineermode/cpustress/SwVideoCodec;
.super Landroid/app/Activity;
.source "SwVideoCodec.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;


# static fields
.field private static final DIALOG_WAIT:I = 0x3f3

.field private static final INDEX_UPDATE_RESULT:I = 0xb

.field private static final PERCENT:D = 100.0

.field public static final SWCODEC_MASK_FORCE_DUAL_0:I = 0x2

.field public static final SWCODEC_MASK_FORCE_DUAL_1:I = 0x3

.field public static final SWCODEC_MASK_FORCE_SINGLE:I = 0x1

.field public static final SWCODEC_MASK_SINGLE:I = 0x0

.field public static final SWCODEC_TEST_FORCE_DUAL:I = 0x2

.field public static final SWCODEC_TEST_FORCE_SINGLE:I = 0x1

.field public static final SWCODEC_TEST_SINGLE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EM/CpuStress_SwVideoCodec"


# instance fields
.field private mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

.field private mBtnStart:Landroid/widget/Button;

.field private mEtIteration:Landroid/widget/EditText;

.field private mEtLoopCount:Landroid/widget/EditText;

.field private final mHandler:Landroid/os/Handler;

.field private mTvResult:Landroid/widget/TextView;

.field private mTvStatus0:Landroid/widget/TextView;

.field private mTvStatus1:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus1:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    new-instance v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec$1;-><init>(Lcom/mediatek/engineermode/cpustress/SwVideoCodec;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/SwVideoCodec;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateTestResult()V

    return-void
.end method

.method private updateStartUI()V
    .locals 3

    const/4 v1, 0x0

    const v2, 0x7f080108

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateTestCount(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const-string v1, "EM/CpuStress_SwVideoCodec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter updateTestResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz p1, :cond_0

    const v1, 0x7f080131

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L

    mul-double/2addr v4, v6

    int-to-double v6, p1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTestResult()V
    .locals 8

    const-string v0, "EM/CpuStress_SwVideoCodec"

    const-string v1, "Enter updateTestResult"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->getData()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v0, "result_video_codec"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v0, "run"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v0, "loopcount"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "iteration"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "result"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateTestUi(ZJIII)V

    const-string v0, "result_pass_video_codec"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v6, v0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateTestCount(II)V

    :cond_0
    return-void
.end method

.method private updateTestUi(ZJIII)V
    .locals 8
    .param p1    # Z
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v1, 0x0

    const/high16 v7, -0x80000000

    const v2, 0x7f08010a

    const v3, 0x7f080109

    const v6, 0x7f080108

    const-string v0, "EM/CpuStress_SwVideoCodec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateTestUI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    if-nez p1, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    if-lez p6, :cond_1

    and-int v0, p5, v7

    if-ne v0, v7, :cond_8

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    if-eqz p1, :cond_a

    const v0, 0x7f080130

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus1:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    if-nez v0, :cond_3

    const/16 v0, 0x3f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->removeDialog(I)V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x2

    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus1:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_3

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    and-int/lit8 v0, p5, 0x4

    if-nez v0, :cond_6

    move v0, v2

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus1:Landroid/widget/TextView;

    and-int/lit8 v1, p5, 0x8

    if-nez v1, :cond_7

    :goto_5
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_4

    :cond_7
    move v2, v3

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    and-int/lit8 v1, p5, 0x1

    if-nez v1, :cond_9

    :goto_6
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus1:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_9
    move v2, v3

    goto :goto_6

    :cond_a
    const v0, 0x7f08012f

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    if-ne v5, p1, :cond_1

    const-string v5, "EM/CpuStress_SwVideoCodec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is clicked"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08012f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-wide/16 v0, 0x0

    const/4 v3, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v5, "loopcount"

    invoke-virtual {v2, v5, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "iteration"

    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v5, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->startTest(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateStartUI()V

    :goto_0
    return-void

    :catch_0
    move-exception v4

    const-string v5, "EM/CpuStress_SwVideoCodec"

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v5, 0x7f080104

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    const/16 v5, 0x3f3

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v5}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->stopTest()V

    goto :goto_0

    :cond_1
    const-string v5, "EM/CpuStress_SwVideoCodec"

    const-string v6, "Unknown event"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0147

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    const v0, 0x7f0b0143

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    const v0, 0x7f0b0144

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    const v0, 0x7f0b0145

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus0:Landroid/widget/TextView;

    const v0, 0x7f0b0146

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvStatus1:Landroid/widget/TextView;

    const v0, 0x7f0b0148

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    const/16 v1, 0x3f3

    if-ne v1, p1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0800fe

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f0800ff

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080100

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f080101

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "EM/CpuStress_SwVideoCodec"

    const-string v1, "Enter onServiceConnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    invoke-virtual {p2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;->getService()Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iput-object p0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "EM/CpuStress_SwVideoCodec"

    const-string v1, "Enter onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/16 v0, 0x3f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p0, v1}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-virtual {p0, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onUpdateTestResult()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
