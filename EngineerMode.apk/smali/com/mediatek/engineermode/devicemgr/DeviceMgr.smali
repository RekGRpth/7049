.class public Lcom/mediatek/engineermode/devicemgr/DeviceMgr;
.super Landroid/preference/PreferenceActivity;
.source "DeviceMgr.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final KEY_SMS_AUTO_REG:Ljava/lang/String; = "sms_auto_reg"

.field private static final STR_DISABLED:Ljava/lang/String; = "Disabled"

.field private static final STR_DMAGENT:Ljava/lang/String; = "DMAgent"

.field private static final STR_ENABLED:Ljava/lang/String; = "Enabled"

.field private static final TAG:Ljava/lang/String; = "EM/devmgr"


# instance fields
.field private mAgent:Lcom/mediatek/common/dm/DMAgent;

.field private mListPreferSmsAutoReg:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private getSavedCTA()I
    .locals 6

    iget-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v3, :cond_0

    const-string v3, "EM/devmgr"

    const-string v4, "get CTA failed, agent is null!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    invoke-interface {v3}, Lcom/mediatek/common/dm/DMAgent;->readCTA()[B

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    :cond_1
    :goto_1
    const-string v3, "EM/devmgr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get savedCTA = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "EM/devmgr"

    const-string v4, "get cta cmcc switch failed, readCTA failed!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v3, "EM/devmgr"

    const-string v4, "number format exception. "

    invoke-static {v3, v4, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private setSavedCTA(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v1, :cond_0

    const-string v1, "EM/devmgr"

    const-string v2, "save CTA switch value failed, agent is null!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/mediatek/common/dm/DMAgent;->writeCTA([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v1, "EM/devmgr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "save CTA ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "EM/devmgr"

    const-string v2, "save CTA switch failed, writeCTA failed!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030023

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    const-string v3, "DMAgent"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/common/dm/DMAgent$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/common/dm/DMAgent;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    const-string v3, "sms_auto_reg"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    iput-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mListPreferSmsAutoReg:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mListPreferSmsAutoReg:Landroid/preference/ListPreference;

    invoke-virtual {v3, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->getSavedCTA()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const-string v2, "Enabled"

    :goto_0
    iget-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mListPreferSmsAutoReg:Landroid/preference/ListPreference;

    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mListPreferSmsAutoReg:Landroid/preference/ListPreference;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v2, "Disabled"

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v5, "sms_auto_reg"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :try_start_0
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->setSavedCTA(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->getSavedCTA()I

    move-result v5

    if-ne v5, v1, :cond_1

    :goto_1
    iget-object v6, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mListPreferSmsAutoReg:Landroid/preference/ListPreference;

    if-eqz v1, :cond_2

    const-string v5, "1"

    :goto_2
    invoke-virtual {v6, v5}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    if-eqz v1, :cond_3

    const-string v3, "Enabled"

    :goto_3
    iget-object v5, p0, Lcom/mediatek/engineermode/devicemgr/DeviceMgr;->mListPreferSmsAutoReg:Landroid/preference/ListPreference;

    invoke-virtual {v5, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return v4

    :catch_0
    move-exception v0

    const-string v5, "EM/devmgr"

    const-string v6, "set exception. "

    invoke-static {v5, v6, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    move v1, v4

    goto :goto_1

    :cond_2
    const-string v5, "0"

    goto :goto_2

    :cond_3
    const-string v3, "Disabled"

    goto :goto_3
.end method
