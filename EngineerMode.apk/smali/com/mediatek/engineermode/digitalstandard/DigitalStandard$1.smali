.class Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;
.super Landroid/content/BroadcastReceiver;
.source "DigitalStandard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    const-string v3, "state"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$002(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;Z)Z

    const-string v2, "DigitalStandard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "airplane mode changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-static {v4}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$000(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$100(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$200(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$300(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$400(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v2, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    const/4 v3, 0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "DigitalStandard"

    const-string v3, "Receiver: network mode change failed! restore the old value."

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "preferred_network_mode"

    const-string v4, "com.android.phone.OLD_NETWORK_MODE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_1
    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->access$300(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->updateSummary()V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    const/16 v3, 0x3f0

    invoke-virtual {v2, v3}, Landroid/app/Activity;->removeDialog(I)V

    goto :goto_0

    :cond_3
    const-string v2, "DigitalStandard"

    const-string v3, "Receiver: network mode change succeed! set the new value."

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "preferred_network_mode"

    const-string v4, "NEW_NETWORK_MODE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1
.end method
