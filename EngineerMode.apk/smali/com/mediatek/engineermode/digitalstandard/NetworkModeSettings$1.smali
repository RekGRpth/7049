.class Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkModeSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    const/4 v4, -0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "slotId"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "state"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v3, "NetworkModeSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive notification of  sim slot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v2, :cond_0

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;

    invoke-static {v3}, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->access$000(Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;)Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->setStatus(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "NetworkModeSettings"

    const-string v4, "BroadcastReceiver: network mode change failed! restore the old value."

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    const-string v5, "com.android.phone.OLD_NETWORK_MODE"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_1
    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;

    const/16 v4, 0x3f0

    invoke-virtual {v3, v4}, Landroid/app/Activity;->removeDialog(I)V

    goto :goto_0

    :cond_2
    const-string v3, "NetworkModeSettings"

    const-string v4, "BroadcastReceiver: network mode change succeed! set the new value."

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;->this$0:Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    const-string v5, "NEW_NETWORK_MODE"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1
.end method
