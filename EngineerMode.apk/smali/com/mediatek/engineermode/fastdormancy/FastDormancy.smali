.class public Lcom/mediatek/engineermode/fastdormancy/FastDormancy;
.super Landroid/app/Activity;
.source "FastDormancy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/fastdormancy/FastDormancy$ButtonClickListener;
    }
.end annotation


# static fields
.field private static final EVENT_FD_QUERY:I = 0x0

.field private static final EVENT_FD_SET:I = 0x1

.field private static final FD_OFF:I = 0x800000

.field private static final FD_ON:I = 0x7fffff

.field private static final FORE_CMD:Ljava/lang/String; = "+EPCT:"

.field private static final QUERY_FD:[Ljava/lang/String;

.field private static final SET_FAILED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EM_FD"


# instance fields
.field private mFdValue:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mRadioGroup:Landroid/widget/RadioGroup;

.field private mResponseHander:Landroid/os/Handler;

.field private mReturnData:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AT+EPCT?"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "+EPCT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->QUERY_FD:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mFdValue:I

    new-instance v0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/fastdormancy/FastDormancy$1;-><init>(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mResponseHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)Landroid/widget/RadioGroup;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mRadioGroup:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;

    iget v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mFdValue:I

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mFdValue:I

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mResponseHander:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/fastdormancy/FastDormancy;
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->parseData([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 4

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mPhone:Lcom/android/internal/telephony/Phone;

    sget-object v1, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->QUERY_FD:[Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mResponseHander:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    return-void
.end method

.method private parseData([Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1    # [Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "EM_FD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseData() content[0]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    aget-object v1, p1, v4

    if-eqz v1, :cond_0

    aget-object v1, p1, v4

    const-string v2, "+EPCT:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    aget-object v1, p1, v4

    const-string v2, "+EPCT:"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const-string v1, "EM_FD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "EM_FD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    aget-object v1, p1, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-object v0, p1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030027

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f0b00f8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0b00fb

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/engineermode/fastdormancy/FastDormancy$ButtonClickListener;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/fastdormancy/FastDormancy$ButtonClickListener;-><init>(Lcom/mediatek/engineermode/fastdormancy/FastDormancy;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->init()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v1

    :pswitch_0
    const-string v2, "SCRI/FD Set"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "SCRI/FD Set failed."

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "OK"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    aget-object v0, v0, v2

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0b00fa

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mReturnData:[Ljava/lang/String;

    aget-object v0, v0, v2

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/fastdormancy/FastDormancy;->mRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0b00f9

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :cond_1
    const-string v0, "EM_FD"

    const-string v1, "returnData is null "

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
