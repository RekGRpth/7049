.class public Lcom/mediatek/engineermode/io/Msdc;
.super Landroid/app/Activity;
.source "Msdc.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DATA_BIT:I = 0xffff

.field private static final EVENT_GET_FAIL_ID:I = 0x6e

.field private static final EVENT_SET_FAIL_ID:I = 0x65

.field private static final EVENT_SET_OK_ID:I = 0x64

.field private static final OFFSET_BIT:I = 0x10


# instance fields
.field private mBtnGet:Landroid/widget/Button;

.field private mBtnSet:Landroid/widget/Button;

.field private mCurrentCmdIndex:I

.field private mCurrentCmdSpinner:Landroid/widget/Spinner;

.field private mCurrentDataIndex:I

.field private mCurrentDataSpinner:Landroid/widget/Spinner;

.field private mHostIndex:I

.field private mHostSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdIndex:I

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/io/Msdc;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/Msdc;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostIndex:I

    return p1
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/io/Msdc;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/Msdc;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataIndex:I

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/io/Msdc;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/Msdc;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdIndex:I

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const v6, 0xffff

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/engineermode/io/Msdc;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostIndex:I

    invoke-static {v4}, Lcom/mediatek/engineermode/io/EmGpio;->getCurrent(I)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    const/16 v4, 0x6e

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    and-int v1, v2, v6

    shr-int/lit8 v4, v2, 0x10

    and-int v0, v4, v6

    iget-object v4, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/AbsSpinner;->setSelection(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/engineermode/io/Msdc;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    iget v4, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostIndex:I

    iget v5, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataIndex:I

    iget v6, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdIndex:I

    invoke-static {v4, v5, v6}, Lcom/mediatek/engineermode/io/EmGpio;->setCurrent(III)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v4, 0x64

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    const/16 v4, 0x65

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const v6, 0x1090009

    const v5, 0x1090008

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030037

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0b0167

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mBtnGet:Landroid/widget/Button;

    const v3, 0x7f0b0168

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mBtnSet:Landroid/widget/Button;

    const v3, 0x7f0b0164

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b0166

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b0165

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f060032

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/Msdc$1;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/Msdc$1;-><init>(Lcom/mediatek/engineermode/io/Msdc;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x7f060033

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentDataSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/Msdc$2;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/Msdc$2;-><init>(Lcom/mediatek/engineermode/io/Msdc;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mCurrentCmdSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/Msdc$3;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/Msdc$3;-><init>(Lcom/mediatek/engineermode/io/Msdc;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/Msdc;->mHostSpinner:Landroid/widget/Spinner;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const v2, 0x7f080409

    const/16 v1, 0x64

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080405

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080406

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :cond_0
    const/16 v1, 0x65

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080407

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080408

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080403

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080404

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-static {}, Lcom/mediatek/engineermode/io/EmGpio;->gpioUnInit()Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
