.class public Lcom/mediatek/engineermode/io/MsdcDrivSet;
.super Landroid/app/Activity;
.source "MsdcDrivSet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DATA_BIT:I = 0xf

.field private static final EVENT_GET_FAIL_ID:I = 0x6e

.field private static final EVENT_SET_FAIL_ID:I = 0x65

.field private static final EVENT_SET_OK_ID:I = 0x64

.field private static final OFFSET_EIGHT_BIT:I = 0x8

.field private static final OFFSET_FOUR_BIT:I = 0x4

.field private static final OFFSET_SIXTEEN_BIT:I = 0x10

.field private static final OFFSET_TWELVE_BIT:I = 0xc

.field private static final OFFSET_TWENTY_BIT:I = 0x14

.field private static final TAG:Ljava/lang/String; = "MSDC_IOCTL"


# instance fields
.field private mBtnGet:Landroid/widget/Button;

.field private mBtnSet:Landroid/widget/Button;

.field private mClkPdIndex:I

.field private mClkPdSpinner:Landroid/widget/Spinner;

.field private mClkPuIndex:I

.field private mClkPuSpinner:Landroid/widget/Spinner;

.field private mCmdPdIndex:I

.field private mCmdPdSpinner:Landroid/widget/Spinner;

.field private mCmdPuIndex:I

.field private mCmdPuSpinner:Landroid/widget/Spinner;

.field private mDataPdSpinner:Landroid/widget/Spinner;

.field private mDataPdndex:I

.field private mDataPuIndex:I

.field private mDataPuSpinner:Landroid/widget/Spinner;

.field private mHostIndex:I

.field private mHostSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdndex:I

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    return p1
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuIndex:I

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdIndex:I

    return p1
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuIndex:I

    return p1
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdIndex:I

    return p1
.end method

.method static synthetic access$502(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuIndex:I

    return p1
.end method

.method static synthetic access$602(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdndex:I

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 19
    .param p1    # Landroid/view/View;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_2

    const-string v1, "MSDC_IOCTL"

    const-string v2, "SD_IOCTL: click GetCurrent"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/io/EmGpio;->newGetCurrent(II)I

    move-result v17

    const/4 v1, -0x1

    move/from16 v0, v17

    if-eq v0, v1, :cond_1

    and-int/lit8 v12, v17, 0xf

    shr-int/lit8 v1, v17, 0x4

    and-int/lit8 v11, v1, 0xf

    shr-int/lit8 v1, v17, 0x8

    and-int/lit8 v14, v1, 0xf

    shr-int/lit8 v1, v17, 0xc

    and-int/lit8 v13, v1, 0xf

    shr-int/lit8 v1, v17, 0x10

    and-int/lit8 v16, v1, 0xf

    shr-int/lit8 v1, v17, 0x14

    and-int/lit8 v15, v1, 0xf

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v12}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v11}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v14}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v13}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v15}, Landroid/widget/AbsSpinner;->setSelection(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x6e

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuIndex:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdIndex:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuIndex:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdIndex:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuIndex:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdndex:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/mediatek/engineermode/io/EmGpio;->newSetCurrent(IIIIIIIIII)Z

    move-result v18

    if-eqz v18, :cond_3

    const/16 v1, 0x64

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    const/16 v1, 0x65

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const v6, 0x1090009

    const v5, 0x1090008

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03003d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0b019c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnGet:Landroid/widget/Button;

    const v3, 0x7f0b019d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnSet:Landroid/widget/Button;

    const v3, 0x7f0b0195

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b0196

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b0197

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b0198

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b0199

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b019a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    const v3, 0x7f0b019b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f060032

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x7f060036

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$2;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$2;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$3;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$3;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$4;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$4;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$5;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$5;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$6;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$6;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/mediatek/engineermode/io/MsdcDrivSet$7;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$7;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    invoke-virtual {v3, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const v2, 0x7f080409

    const/16 v1, 0x64

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080405

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080406

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :cond_0
    const/16 v1, 0x65

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080407

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080408

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080403

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080404

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
