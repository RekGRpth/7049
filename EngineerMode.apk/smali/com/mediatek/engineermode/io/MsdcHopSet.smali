.class public Lcom/mediatek/engineermode/io/MsdcHopSet;
.super Landroid/app/Activity;
.source "MsdcHopSet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DATA_BIT:I = 0xf

.field private static final EVENT_GET_FAIL_ID:I = 0x6e

.field private static final EVENT_SET_FAIL_ID:I = 0x65

.field private static final EVENT_SET_OK_ID:I = 0x64

.field private static final OFFSET_HOP_BIT:I = 0x18

.field private static final OFFSET_TIME_BIT:I = 0x1c

.field private static final TAG:Ljava/lang/String; = "MSDC_HOPSET_IOCTL"


# instance fields
.field private mBtnGet:Landroid/widget/Button;

.field private mBtnSet:Landroid/widget/Button;

.field private mHoppingBitIndex:I

.field private mHoppingBitSpinner:Landroid/widget/Spinner;

.field private mHoppingTimeIndex:I

.field private mHoppingTimeSpinner:Landroid/widget/Spinner;

.field private mHostIndex:I

.field private mHostSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeIndex:I

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/io/MsdcHopSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcHopSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostIndex:I

    return p1
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/io/MsdcHopSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcHopSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitIndex:I

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/io/MsdcHopSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcHopSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeIndex:I

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1    # Landroid/view/View;

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostIndex:I

    invoke-static {v0, v9}, Lcom/mediatek/engineermode/io/EmGpio;->newGetCurrent(II)I

    move-result v10

    const-string v0, "MSDC_HOPSET_IOCTL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get Data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-eq v10, v0, :cond_1

    shr-int/lit8 v0, v10, 0x18

    and-int/lit8 v11, v0, 0xf

    shr-int/lit8 v0, v10, 0x1c

    and-int/lit8 v12, v0, 0xf

    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v11}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v12}, Landroid/widget/AbsSpinner;->setSelection(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x6e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostIndex:I

    iget v7, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitIndex:I

    iget v8, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeIndex:I

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-static/range {v0 .. v9}, Lcom/mediatek/engineermode/io/EmGpio;->newSetCurrent(IIIIIIIIII)Z

    move-result v13

    if-eqz v13, :cond_3

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const v7, 0x1090009

    const v6, 0x1090008

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f030038

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f0b016c

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mBtnGet:Landroid/widget/Button;

    const v4, 0x7f0b016d

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mBtnSet:Landroid/widget/Button;

    const v4, 0x7f0b0169

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostSpinner:Landroid/widget/Spinner;

    const v4, 0x7f0b016a

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitSpinner:Landroid/widget/Spinner;

    const v4, 0x7f0b016b

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060032

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v2, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostSpinner:Landroid/widget/Spinner;

    new-instance v5, Lcom/mediatek/engineermode/io/MsdcHopSet$1;

    invoke-direct {v5, p0}, Lcom/mediatek/engineermode/io/MsdcHopSet$1;-><init>(Lcom/mediatek/engineermode/io/MsdcHopSet;)V

    invoke-virtual {v4, v5}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingBitSpinner:Landroid/widget/Spinner;

    new-instance v5, Lcom/mediatek/engineermode/io/MsdcHopSet$2;

    invoke-direct {v5, p0}, Lcom/mediatek/engineermode/io/MsdcHopSet$2;-><init>(Lcom/mediatek/engineermode/io/MsdcHopSet;)V

    invoke-virtual {v4, v5}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHoppingTimeSpinner:Landroid/widget/Spinner;

    new-instance v5, Lcom/mediatek/engineermode/io/MsdcHopSet$3;

    invoke-direct {v5, p0}, Lcom/mediatek/engineermode/io/MsdcHopSet$3;-><init>(Lcom/mediatek/engineermode/io/MsdcHopSet;)V

    invoke-virtual {v4, v5}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/io/MsdcHopSet;->mHostSpinner:Landroid/widget/Spinner;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const v2, 0x7f080409

    const/16 v1, 0x64

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080405

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080406

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :cond_0
    const/16 v1, 0x65

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080407

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080408

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080403

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080404

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method
