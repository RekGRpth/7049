.class public Lcom/mediatek/engineermode/memory/NandFlash;
.super Landroid/app/TabActivity;
.source "NandFlash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/memory/NandFlash$MmcCid;
    }
.end annotation


# static fields
.field private static final EMMC_ID_HEADER:Ljava/lang/String; = "emmc ID: "

.field private static final FILE_CID:Ljava/lang/String; = "/sys/block/mmcblk0/device/cid"

.field private static final FILE_DUMCHAR_INFO:Ljava/lang/String; = "/proc/dumchar_info"

.field protected static final FILE_EMMC:Ljava/lang/String; = "/proc/emmc"

.field protected static final FILE_MOUNTS:Ljava/lang/String; = "/proc/mounts"

.field protected static final FILE_MTD:Ljava/lang/String; = "/proc/mtd"

.field protected static final FILE_NAND:Ljava/lang/String; = "/proc/driver/nand"

.field private static final READ_COMMAND:Ljava/lang/String; = "cat "

.field private static final TAG:Ljava/lang/String; = "EM/Memory_flash"


# instance fields
.field private mCommonTabName:Ljava/lang/String;

.field private mFileSysTabName:Ljava/lang/String;

.field private mHaveEmmc:Z

.field private mPartitionTabName:Ljava/lang/String;

.field private mTabId:Ljava/lang/String;

.field private mTvCommInfo:Landroid/widget/TextView;

.field private mTvFSInfo:Landroid/widget/TextView;

.field private mTvPartInfo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mHaveEmmc:Z

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvCommInfo:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvFSInfo:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvPartInfo:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mCommonTabName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mFileSysTabName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mPartitionTabName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTabId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/memory/NandFlash;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/memory/NandFlash;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTabId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/memory/NandFlash;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/memory/NandFlash;

    invoke-direct {p0}, Lcom/mediatek/engineermode/memory/NandFlash;->showTabContent()V

    return-void
.end method

.method private getEmmcCommon()Ljava/lang/String;
    .locals 6

    const-string v3, "/sys/block/mmcblk0/device/cid"

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/memory/NandFlash;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "EM/Memory_flash"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "emmcId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "emmc ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/mediatek/engineermode/memory/NandFlash$MmcCid;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/memory/NandFlash$MmcCid;-><init>(Lcom/mediatek/engineermode/memory/NandFlash;)V

    invoke-virtual {v0, v1}, Lcom/mediatek/engineermode/memory/NandFlash$MmcCid;->parse(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/engineermode/memory/NandFlash$MmcCid;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/proc/dumchar_info"

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/memory/NandFlash;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cat "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const v3, 0x7f08009e

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "EM/Memory_flash"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private showTabContent()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTabId:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mCommonTabName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mHaveEmmc:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvCommInfo:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/mediatek/engineermode/memory/NandFlash;->getEmmcCommon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvCommInfo:Landroid/widget/TextView;

    const-string v1, "/proc/driver/nand"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/memory/NandFlash;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTabId:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mFileSysTabName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvFSInfo:Landroid/widget/TextView;

    const-string v1, "/proc/mounts"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/memory/NandFlash;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTabId:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mPartitionTabName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mHaveEmmc:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvPartInfo:Landroid/widget/TextView;

    const-string v1, "/proc/emmc"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/memory/NandFlash;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvPartInfo:Landroid/widget/TextView;

    const-string v1, "/proc/mtd"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/memory/NandFlash;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "HAVE_EMMC"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mHaveEmmc:Z

    const v1, 0x7f08009b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mCommonTabName:Ljava/lang/String;

    const v1, 0x7f08009c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mFileSysTabName:Ljava/lang/String;

    const v1, 0x7f08009d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mPartitionTabName:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/TabActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030034

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mCommonTabName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mCommonTabName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const v2, 0x7f0b014f

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mFileSysTabName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mFileSysTabName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const v2, 0x7f0b0151

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mPartitionTabName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mPartitionTabName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const v2, 0x7f0b0153

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    const v1, 0x7f0b0150

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvCommInfo:Landroid/widget/TextView;

    const v1, 0x7f0b0152

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvFSInfo:Landroid/widget/TextView;

    const v1, 0x7f0b0154

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTvPartInfo:Landroid/widget/TextView;

    new-instance v1, Lcom/mediatek/engineermode/memory/NandFlash$1;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/memory/NandFlash$1;-><init>(Lcom/mediatek/engineermode/memory/NandFlash;)V

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mCommonTabName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/engineermode/memory/NandFlash;->mTabId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/engineermode/memory/NandFlash;->showTabContent()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/ActivityGroup;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/ActivityGroup;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/memory/NandFlash;->showTabContent()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/ActivityGroup;->onStop()V

    return-void
.end method
