.class public Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
.super Landroid/app/Activity;
.source "ModemTestActivity.java"


# static fields
.field private static final CMD_LENGTH:I = 0x6

.field private static final CTA_DIALOG:I = 0x1

.field private static final EVENT_MODEM_CTA:I = 0x1

.field private static final EVENT_MODEM_FTA:I = 0x2

.field private static final EVENT_MODEM_IOT:I = 0x3

.field private static final EVENT_MODEM_NONE:I = 0x0

.field private static final EVENT_MODEM_OPERATOR:I = 0x5

.field private static final EVENT_MODEM_QUERY:I = 0x4

.field private static final EVENT_QUERY_PREFERRED_TYPE_DONE:I = 0x3e8

.field private static final EVENT_SET_PREFERRED_TYPE_DONE:I = 0x3e9

.field private static final IOT_DIALOG:I = 0x3

.field private static final MODE_LENGTH:I = 0x3

.field private static final NETWORK_TYPE:I = 0x3

.field private static final OPERATOR_DIALOG:I = 0x5

.field private static final REBOOT_DIALOG:I = 0x2

.field public static final TAG:Ljava/lang/String; = "ModemTest"


# instance fields
.field private mATCmdHander:Landroid/os/Handler;

.field private mCtaBtn:Landroid/widget/Button;

.field private mCtaOption:I

.field private mCtaOptionsArray:[Ljava/lang/String;

.field private mFtaBtn:Landroid/widget/Button;

.field private mIotBtn:Landroid/widget/Button;

.field private mIotOption:I

.field private mModemFlag:Z

.field private mNoneBtn:Landroid/widget/Button;

.field private mOperatorBtn:Landroid/widget/Button;

.field private mOperatorOption:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iput v1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOption:I

    iput v1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mIotOption:I

    iput v1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mOperatorOption:I

    new-instance v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$2;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mATCmdHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->sendATCommad(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mModemFlag:Z

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mModemFlag:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->writePreferred(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mATCmdHander:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;[Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->handleQuery([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    invoke-direct {p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->disableAllButton()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    iget v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOption:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOption:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    iget v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mIotOption:I

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mIotOption:I

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;

    iget v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mOperatorOption:I

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/modemtest/ModemTestActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mOperatorOption:I

    return p1
.end method

.method private checkNetworkType()V
    .locals 3

    const-string v0, "ModemTest"

    const-string v1, "TcheckNetworkType"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mATCmdHander:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    return-void
.end method

.method private disableAllButton()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mNoneBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mFtaBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mIotBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mOperatorBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method

.method private handleQuery([Ljava/lang/String;)V
    .locals 17
    .param p1    # [Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "data length is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v4, 0x0

    move-object/from16 v1, p1

    array-length v7, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v9, v1, v5

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "data["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] is : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v14, 0x0

    aget-object v14, p1, v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x6

    if-le v14, v15, :cond_a

    const/4 v14, 0x0

    aget-object v14, p1, v14

    const/4 v15, 0x7

    const/16 v16, 0x0

    aget-object v16, p1, v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mode is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x3

    if-lt v14, v15, :cond_9

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-virtual {v8, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v14, 0x2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v8, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "subMode is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "subCtaMode is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "0"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const-string v15, "The current mode is none"

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string v14, "1"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const-string v15, "The current mode is CTA"

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOptionsArray:[Ljava/lang/String;

    array-length v2, v14

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ctaLength is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "val is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "The current mode is CTA: "

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v2, :cond_5

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "j ==== "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "(val & (1 << j)) is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x1

    shl-int v16, v16, v6

    and-int v16, v16, v13

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v14, 0x1

    shl-int/2addr v14, v6

    and-int/2addr v14, v13

    if-eqz v14, :cond_4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOptionsArray:[Ljava/lang/String;

    aget-object v15, v15, v6

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_5
    const/4 v14, 0x0

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v12, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v14, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v14, "ModemTest"

    const-string v15, "Exception when transfer subCtaMode"

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    const-string v14, "2"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const-string v15, "The current mode is FTA."

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    const-string v14, "3"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const-string v15, "The current mode is IOT."

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    const-string v14, "4"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const-string v15, "The current mode is OPERATOR."

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_9
    const-string v14, "ModemTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mode len is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    const-string v14, "ModemTest"

    const-string v15, "The data returned is not right."

    invoke-static {v14, v15}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private sendATCommad(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AT+EPCT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mATCmdHander:Landroid/os/Handler;

    invoke-virtual {v2, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    return-void
.end method

.method private writePreferred(I)V
    .locals 4
    .param p1    # I

    const-string v2, "RATMode"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ModeType"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030036

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    new-instance v1, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$1;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    const v2, 0x7f0b015e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const v2, 0x7f0b015f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mNoneBtn:Landroid/widget/Button;

    const v2, 0x7f0b0160

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaBtn:Landroid/widget/Button;

    const v2, 0x7f0b0161

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mFtaBtn:Landroid/widget/Button;

    const v2, 0x7f0b0162

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mIotBtn:Landroid/widget/Button;

    const v2, 0x7f0b0163

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mOperatorBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mNoneBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mFtaBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mIotBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mOperatorBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mTextView:Landroid/widget/TextView;

    const-string v3, "The current mode is unknown"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOptionsArray:[Ljava/lang/String;

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "AT+EPCT?"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "+EPCT:"

    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v3, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mATCmdHander:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    const/16 v2, 0x9

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "MODEM TEST"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f06001a

    new-array v2, v2, [Z

    fill-array-data v2, :array_0

    new-instance v3, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$5;

    invoke-direct {v3, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$5;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems(I[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Send"

    new-instance v2, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$4;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$4;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Cancel"

    new-instance v2, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$3;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "MODEM TEST"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Please reboot the phone!"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "MODEM TEST"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f06001c

    new-array v2, v2, [Z

    fill-array-data v2, :array_1

    new-instance v3, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$8;

    invoke-direct {v3, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$8;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems(I[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Send"

    new-instance v2, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$7;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$7;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Cancel"

    new-instance v2, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$6;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$6;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "MODEM TEST"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f06001b

    new-array v2, v2, [Z

    fill-array-data v2, :array_2

    new-instance v3, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$11;

    invoke-direct {v3, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$11;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems(I[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Send"

    new-instance v2, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$10;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$10;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Cancel"

    new-instance v2, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$9;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity$9;-><init>(Lcom/mediatek/engineermode/modemtest/ModemTestActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->mCtaOption:I

    invoke-direct {p0}, Lcom/mediatek/engineermode/modemtest/ModemTestActivity;->checkNetworkType()V

    return-void
.end method
