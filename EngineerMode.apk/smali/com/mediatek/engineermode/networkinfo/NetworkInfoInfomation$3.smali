.class Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;
.super Landroid/os/Handler;
.source "NetworkInfoInfomation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    packed-switch v12, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    iget-object v12, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v12, [Ljava/lang/String;

    move-object v2, v12

    check-cast v2, [Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v12

    new-array v4, v12, [B

    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    const-string v13, "NetworkInfo.urc"

    invoke-virtual {v12, v13}, Landroid/content/ContextWrapper;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v6

    const/4 v12, 0x0

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v13

    invoke-virtual {v6, v4, v12, v13}, Ljava/io/FileInputStream;->read([BII)I

    move-result v7

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/lang/String;-><init>([B)V

    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Ret Type: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x0

    aget-object v14, v2, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Ret Data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v2, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    aget-object v12, v2, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    sparse-switch v12, :sswitch_data_0

    :goto_1
    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->getModemType()I

    move-result v8

    const/4 v12, 0x1

    if-ne v8, v12, :cond_3

    const/4 v12, 0x0

    aget-object v12, v2, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    sparse-switch v12, :sswitch_data_1

    :cond_1
    :goto_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    const-string v13, "NetworkInfo.urc"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/ContextWrapper;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v12}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v12

    const/4 v13, 0x0

    aget-object v13, v2, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    if-ne v12, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    new-instance v13, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {v13, v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;-><init>(Landroid/content/Context;)V

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3502(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v12}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Landroid/widget/TextView;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "<"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ">\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v15}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)[I

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v16

    aget v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;->getInfo(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :catch_0
    move-exception v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    const/16 v13, 0x64

    invoke-virtual {v12, v13}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :catch_1
    move-exception v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    const/16 v13, 0x65

    invoke-virtual {v12, v13}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :sswitch_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_4
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_6
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_7
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_8
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_9
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_a
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_b
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_c
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_d
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_e
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_f
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_10
    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "data[1].length()="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v2, v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "start offset "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "end offset "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_11
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_12
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_13
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_14
    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "data[1].length()="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v2, v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "NetworkInfo"

    const-string v13, "2G size should be 912"

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "start offset "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "NetworkInfo"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "end offset "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x1

    aget-object v12, v2, v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    const/16 v13, 0x390

    if-lt v12, v13, :cond_2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_2
    const/4 v12, 0x1

    aget-object v12, v2, v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    rsub-int v12, v12, 0x390

    new-array v11, v12, [C

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :sswitch_15
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :sswitch_16
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :cond_3
    const/4 v12, 0x2

    if-ne v8, v12, :cond_1

    const/4 v12, 0x0

    aget-object v12, v2, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    packed-switch v12, :pswitch_data_1

    :pswitch_1
    goto/16 :goto_2

    :pswitch_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$1900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :pswitch_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$2900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :pswitch_4
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :pswitch_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :pswitch_6
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v13}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v13

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$500()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :pswitch_7
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v14}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v14

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :catch_2
    move-exception v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    const/16 v13, 0x64

    invoke-virtual {v12, v13}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :catch_3
    move-exception v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    const/16 v13, 0x65

    invoke-virtual {v12, v13}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x15 -> :sswitch_e
        0x1b -> :sswitch_f
        0x2f -> :sswitch_10
        0x30 -> :sswitch_14
        0x34 -> :sswitch_11
        0x3d -> :sswitch_12
        0x3f -> :sswitch_13
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x35 -> :sswitch_15
        0x40 -> :sswitch_16
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x40
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
