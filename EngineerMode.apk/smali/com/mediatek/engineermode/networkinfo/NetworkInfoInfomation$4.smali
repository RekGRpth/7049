.class Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;
.super Landroid/os/Handler;
.source "NetworkInfoInfomation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    new-instance v1, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {v1, v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3502(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v3}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v4}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v4

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;->getInfo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    new-instance v1, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {v1, v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3502(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v3}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v4}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v4

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;->getInfo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    new-instance v1, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {v1, v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3502(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v3}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;->this$0:Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-static {v4}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I

    move-result v4

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;->getInfo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
