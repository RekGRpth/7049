.class public Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;
.super Landroid/app/Activity;
.source "NetworkInfoInfomation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final BUF_SIZE_2G:I = 0x89c

.field private static final BUF_SIZE_3G_FDD:I = 0x890

.field private static final BUF_SIZE_3G_TDD:I = 0x16c

.field private static final BUF_SIZE_XG_IDX48:I = 0x390

.field private static final EVENT_MENU_INFO:I = 0x2

.field private static final EVENT_NW_INFO:I = 0x1

.field private static final EVENT_NW_INFO_AT:I = 0x2

.field private static final EVENT_NW_INFO_CLOSE_AT:I = 0x4

.field private static final EVENT_NW_INFO_INFO_AT:I = 0x3

.field private static final EVENT_PAGEDOWN_INFO:I = 0x1

.field private static final EVENT_PAGEUP_INFO:I = 0x0

.field private static final FILE_NOT_FOUND:I = 0x64

.field private static final FLAG_DATA_BIT:I = 0x8

.field private static final FLAG_OFFSET_BIT:I = 0x8

.field private static final FLAG_OR_DATA:I = 0xf7

.field private static final IO_EXCEPTION_ID:I = 0x65

.field private static final NW_INFO_FILE_NAME:Ljava/lang/String; = "NetworkInfo.urc"

.field private static final TAG:Ljava/lang/String; = "NetworkInfo"

.field private static final TOTAL_TIMER:I = 0x3e8

.field private static sTotalBufSize:I


# instance fields
.field private mATCmdHander:Landroid/os/Handler;

.field private mBLKInfoSize:I

.field private mCaListSize:I

.field private mCellSelSize:I

.field private mChDscrSize:I

.field private mControlMsgSize:I

.field private mCsceEMNeighCellSStatusIndStructSizexG:I

.field private mCsceEMServCellsStatusIndSize3G:I

.field private mCsceEmInfoMultiPlmnSize3G:I

.field private mCtrlchanSize:I

.field private mCurrentItem:I

.field private mFlag:I

.field private mGHandoverSequenceIndStuctSize3G:I

.field private mGPRSGenSize:I

.field private mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field private mInfo:Landroid/widget/TextView;

.field private mIsChecked:[I

.field private mItem:[I

.field private mItemCount:I

.field private mLAIInfoSize:I

.field private mMIInfoSize:I

.field private mMeasRepSize:I

.field private mMemeEmInfoHServCellIndSize3G:I

.field private mMemeEmInfoUmtsCellStatusSize3G:I

.field private mMemeEmPeriodicBlerReportIndSize3G:I

.field private mMmEmInfoSize3G:I

.field private mNetworkUrcParse:Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

.field private mPageDown:Landroid/widget/Button;

.field private mPageUp:Landroid/widget/Button;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mRACHCtrlSize:I

.field private mRadioLinkSize:I

.field private mResponseHander:Landroid/os/Handler;

.field private mSI2QInfoSize:I

.field private mSimType:I

.field private mSlceEmPsDataRateStatusIndSize3G:I

.field private mTBFInfoSize:I

.field private mTcmMmiEmInfoSize3G:I

.field private mTimer:Ljava/util/Timer;

.field private mUl2EmAdmPoolStatusIndStructSize3G:I

.field private mUl2EmHsdschReconfigStatusIndStructSize3G:I

.field private mUl2EmPeriodicBlerReportIndSize3G:I

.field private mUl2EmPsDataRateStatusIndStructSize3G:I

.field private mUl2EmUrlcEventStatusIndStructSize3G:I

.field private mUpUiHandler:Landroid/os/Handler;

.field private mUrrUmtsSrncIdSize3G:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sTotalBufSize:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    iput-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mNetworkUrcParse:Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iput-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPhone:Lcom/android/internal/telephony/Phone;

    iput-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCellSelSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mChDscrSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCtrlchanSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRACHCtrlSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mLAIInfoSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRadioLinkSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMeasRepSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCaListSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mControlMsgSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSI2QInfoSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMIInfoSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mBLKInfoSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTBFInfoSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGPRSGenSize:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMmEmInfoSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTcmMmiEmInfoSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEmInfoMultiPlmnSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoUmtsCellStatusSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmPeriodicBlerReportIndSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUrrUmtsSrncIdSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSlceEmPsDataRateStatusIndSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoHServCellIndSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGHandoverSequenceIndStuctSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmAdmPoolStatusIndStructSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPsDataRateStatusIndStructSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmHsdschReconfigStatusIndStructSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmUrlcEventStatusIndStructSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPeriodicBlerReportIndSize3G:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMNeighCellSStatusIndStructSizexG:I

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    new-instance v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$1;-><init>(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mATCmdHander:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$3;-><init>(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mResponseHander:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$4;-><init>(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUpUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mLAIInfoSize:I

    return v0
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRadioLinkSize:I

    return v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMeasRepSize:I

    return v0
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCaListSize:I

    return v0
.end method

.method static synthetic access$1400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mControlMsgSize:I

    return v0
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSI2QInfoSize:I

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMIInfoSize:I

    return v0
.end method

.method static synthetic access$1700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mBLKInfoSize:I

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTBFInfoSize:I

    return v0
.end method

.method static synthetic access$1900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGPRSGenSize:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)[I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItem:[I

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMNeighCellSStatusIndStructSizexG:I

    return v0
.end method

.method static synthetic access$2100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMmEmInfoSize3G:I

    return v0
.end method

.method static synthetic access$2200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTcmMmiEmInfoSize3G:I

    return v0
.end method

.method static synthetic access$2300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    return v0
.end method

.method static synthetic access$2400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEmInfoMultiPlmnSize3G:I

    return v0
.end method

.method static synthetic access$2500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoUmtsCellStatusSize3G:I

    return v0
.end method

.method static synthetic access$2600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmPeriodicBlerReportIndSize3G:I

    return v0
.end method

.method static synthetic access$2700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUrrUmtsSrncIdSize3G:I

    return v0
.end method

.method static synthetic access$2800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSlceEmPsDataRateStatusIndSize3G:I

    return v0
.end method

.method static synthetic access$2900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGHandoverSequenceIndStuctSize3G:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;[Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sendATCommand([Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmAdmPoolStatusIndStructSize3G:I

    return v0
.end method

.method static synthetic access$3100(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPsDataRateStatusIndStructSize3G:I

    return v0
.end method

.method static synthetic access$3200(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmHsdschReconfigStatusIndStructSize3G:I

    return v0
.end method

.method static synthetic access$3300(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmUrlcEventStatusIndStructSize3G:I

    return v0
.end method

.method static synthetic access$3400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    return v0
.end method

.method static synthetic access$3500(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mNetworkUrcParse:Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;)Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;
    .param p1    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iput-object p1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mNetworkUrcParse:Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUpUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    sget v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sTotalBufSize:I

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCellSelSize:I

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mChDscrSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCtrlchanSize:I

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRACHCtrlSize:I

    return v0
.end method

.method private calcOffset()V
    .locals 2

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCellSelSize:I

    add-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCellSelSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mChDscrSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCellSelSize:I

    add-int/lit16 v1, v1, 0x134

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mChDscrSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCtrlchanSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mChDscrSize:I

    add-int/lit8 v1, v1, 0xe

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCtrlchanSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRACHCtrlSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCtrlchanSize:I

    add-int/lit8 v1, v1, 0xe

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRACHCtrlSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mLAIInfoSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRACHCtrlSize:I

    add-int/lit8 v1, v1, 0x1c

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mLAIInfoSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRadioLinkSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mLAIInfoSize:I

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRadioLinkSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMeasRepSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mRadioLinkSize:I

    add-int/lit16 v1, v1, 0x558

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMeasRepSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCaListSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMeasRepSize:I

    add-int/lit16 v1, v1, 0x104

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCaListSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mControlMsgSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCaListSize:I

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mControlMsgSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSI2QInfoSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mControlMsgSize:I

    add-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSI2QInfoSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMIInfoSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSI2QInfoSize:I

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMIInfoSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mBLKInfoSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMIInfoSize:I

    add-int/lit8 v1, v1, 0x50

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mBLKInfoSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTBFInfoSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mBLKInfoSize:I

    add-int/lit8 v1, v1, 0x38

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTBFInfoSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGPRSGenSize:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTBFInfoSize:I

    add-int/lit8 v1, v1, 0x20

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGPRSGenSize:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMNeighCellSStatusIndStructSizexG:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGPRSGenSize:I

    add-int/lit16 v1, v1, 0x390

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMNeighCellSStatusIndStructSizexG:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMmEmInfoSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMNeighCellSStatusIndStructSizexG:I

    add-int/lit8 v1, v1, 0x34

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMmEmInfoSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTcmMmiEmInfoSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMmEmInfoSize3G:I

    add-int/lit8 v1, v1, 0xe

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTcmMmiEmInfoSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTcmMmiEmInfoSize3G:I

    add-int/lit8 v1, v1, 0x58

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEmInfoMultiPlmnSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    add-int/lit8 v1, v1, 0x4a

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEmInfoMultiPlmnSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoUmtsCellStatusSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEmInfoMultiPlmnSize3G:I

    add-int/lit16 v1, v1, 0x3ee

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoUmtsCellStatusSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmPeriodicBlerReportIndSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoUmtsCellStatusSize3G:I

    add-int/lit16 v1, v1, 0xc8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmPeriodicBlerReportIndSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUrrUmtsSrncIdSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmPeriodicBlerReportIndSize3G:I

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUrrUmtsSrncIdSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSlceEmPsDataRateStatusIndSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUrrUmtsSrncIdSize3G:I

    add-int/lit16 v1, v1, 0xc8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSlceEmPsDataRateStatusIndSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoHServCellIndSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSlceEmPsDataRateStatusIndSize3G:I

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mMemeEmInfoHServCellIndSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGHandoverSequenceIndStuctSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMNeighCellSStatusIndStructSizexG:I

    add-int/lit8 v1, v1, 0x20

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGHandoverSequenceIndStuctSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmAdmPoolStatusIndStructSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGHandoverSequenceIndStuctSize3G:I

    add-int/lit8 v1, v1, 0x40

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmAdmPoolStatusIndStructSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPsDataRateStatusIndStructSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmAdmPoolStatusIndStructSize3G:I

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPsDataRateStatusIndStructSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmHsdschReconfigStatusIndStructSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPsDataRateStatusIndStructSize3G:I

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmHsdschReconfigStatusIndStructSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmUrlcEventStatusIndStructSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmHsdschReconfigStatusIndStructSize3G:I

    add-int/lit8 v1, v1, 0x24

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmUrlcEventStatusIndStructSize3G:I

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPeriodicBlerReportIndSize3G:I

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmUrlcEventStatusIndStructSize3G:I

    add-int/lit16 v1, v1, 0xc8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mUl2EmPeriodicBlerReportIndSize3G:I

    return-void
.end method

.method private registerNetwork()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "AT+EINFO?"

    aput-object v1, v0, v6

    const-string v1, "+EINFO"

    aput-object v1, v0, v3

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSimType:I

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mResponseHander:Landroid/os/Handler;

    invoke-interface {v1, v2, v3, v5}, Lcom/android/internal/telephony/Phone;->registerForNetworkInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    invoke-direct {p0, v0, v4}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sendATCommand([Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSimType:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mResponseHander:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->registerForNetworkInfoGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    invoke-direct {p0, v0, v4}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sendATCommand([Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mResponseHander:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3, v5, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->registerForNetworkInfoGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    invoke-direct {p0, v0, v4}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sendATCommand([Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private sendATCommand([Ljava/lang/String;I)V
    .locals 3
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x1

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSimType:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mATCmdHander:Landroid/os/Handler;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSimType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mATCmdHander:Landroid/os/Handler;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mATCmdHander:Landroid/os/Handler;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method


# virtual methods
.method public calcBufferSize()I
    .locals 1

    const/4 v0, 0x0

    const/16 v0, 0x1628

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPageUp:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->updateUI(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPageDown:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->updateUI(I)V

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/16 v9, 0x64

    const/4 v10, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v7, 0x7f03003c

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->calcBufferSize()I

    move-result v7

    sput v7, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sTotalBufSize:I

    sget v7, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sTotalBufSize:I

    new-array v2, v7, [B

    const/4 v6, 0x0

    :goto_0
    sget v7, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sTotalBufSize:I

    if-ge v6, v7, :cond_0

    const/16 v7, 0x30

    aput-byte v7, v2, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    :try_start_0
    const-string v7, "NetworkInfo.urc"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/content/ContextWrapper;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->getModemType()I

    move-result v4

    const/4 v7, 0x2

    if-ne v4, v7, :cond_1

    iget v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    add-int/lit8 v7, v7, -0x8

    iput v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCsceEMServCellsStatusIndSize3G:I

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->calcOffset()V

    const-string v7, "NetworkInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The total data size is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mGPRSGenSize:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v7, 0x48

    new-array v7, v7, [I

    iput-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItem:[I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v7, "mChecked"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    iput-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mIsChecked:[I

    const-string v7, "mSimType"

    invoke-virtual {v3, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mSimType:I

    const/4 v1, 0x0

    :goto_1
    iget-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mIsChecked:[I

    array-length v7, v7

    if-ge v1, v7, :cond_3

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mIsChecked:[I

    aget v8, v8, v1

    if-ne v7, v8, :cond_2

    iget-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItem:[I

    iget v8, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    aput v1, v7, v8

    iget v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {p0, v9}, Landroid/app/Activity;->showDialog(I)V

    :goto_2
    return-void

    :catch_1
    move-exception v0

    invoke-virtual {p0, v9}, Landroid/app/Activity;->removeDialog(I)V

    const/16 v7, 0x65

    invoke-virtual {p0, v7}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->registerNetwork()V

    const v7, 0x7f0b0194

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mInfo:Landroid/widget/TextView;

    const v7, 0x7f0b0192

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPageUp:Landroid/widget/Button;

    const v7, 0x7f0b0193

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPageDown:Landroid/widget/Button;

    iget-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPageUp:Landroid/widget/Button;

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPageDown:Landroid/widget/Button;

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v7, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    invoke-direct {v7, p0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mNetworkUrcParse:Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v7, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mInfo:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItemCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mNetworkUrcParse:Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;

    iget-object v10, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mItem:[I

    iget v11, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mCurrentItem:I

    aget v10, v10, v11

    invoke-virtual {v9, v10}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoUrcParser;->getInfo(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const v2, 0x7f080412

    const/16 v1, 0x64

    if-ne v1, p1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08040e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f08040f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :cond_1
    const/16 v1, 0x65

    if-ne v1, p1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080410

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080411

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->updateUI(I)V

    return-void
.end method

.method public onStop()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mResponseHander:Landroid/os/Handler;

    invoke-interface {v1, v2}, Lcom/android/internal/telephony/Phone;->unregisterForNetworkInfo(Landroid/os/Handler;)V

    iget v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    and-int/lit16 v1, v1, 0xf7

    iput v1, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    const-string v1, "NetworkInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The close flag is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AT+EINFO="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mFlag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->sendATCommand([Ljava/lang/String;I)V

    return-void
.end method

.method public updateUI(I)V
    .locals 7
    .param p1    # I

    const-wide/16 v2, 0x3e8

    move v6, p1

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$2;

    invoke-direct {v1, p0, v6}, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation$2;-><init>(Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;I)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method
