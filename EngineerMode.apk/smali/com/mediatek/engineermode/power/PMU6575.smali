.class public Lcom/mediatek/engineermode/power/PMU6575;
.super Landroid/app/TabActivity;
.source "PMU6575.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/power/PMU6575$RunThread;
    }
.end annotation


# static fields
.field private static final EVENT_UPDATE:I = 0x1

.field private static final MAGIC_TEN:F = 10.0f

.field private static final RADIX_HEX:I = 0x10

.field private static final TAB_INFO:I = 0x2

.field private static final TAB_REG:I = 0x1

.field private static final UPDATE_INTERVAL:I = 0x5dc


# instance fields
.field private mBankAdatper:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBankIndex:I

.field private mBankSpinner:Landroid/widget/Spinner;

.field private mBankType:[Ljava/lang/String;

.field private mBtnGetRegister:Landroid/widget/Button;

.field private mBtnSetRegister:Landroid/widget/Button;

.field private mEditAddr:Landroid/widget/EditText;

.field private mEditVal:Landroid/widget/EditText;

.field private mFiles:[[Ljava/lang/String;

.field private mInfo:Landroid/widget/TextView;

.field private mPromptSw:Ljava/lang/String;

.field private mPromptUnit:Ljava/lang/String;

.field private mRun:Z

.field public mUpdateHandler:Landroid/os/Handler;

.field private mWhichTab:I


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mInfo:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnGetRegister:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnSetRegister:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditAddr:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditVal:Landroid/widget/EditText;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Bank0"

    aput-object v1, v0, v4

    const-string v1, "Bank1"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankType:[Ljava/lang/String;

    iput v4, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankIndex:I

    iput v5, p0, Lcom/mediatek/engineermode/power/PMU6575;->mWhichTab:I

    new-instance v0, Lcom/mediatek/engineermode/power/PMU6575$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/power/PMU6575$3;-><init>(Lcom/mediatek/engineermode/power/PMU6575;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mUpdateHandler:Landroid/os/Handler;

    const-string v0, "0/1=off/on"

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    const-string v0, "mV"

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    const/16 v0, 0x37

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "BUCK_VAPROC_STATUS"

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "BUCK_VCORE_STATUS"

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "BUCK_VIO18_STATUS"

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    const/4 v1, 0x3

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VPA_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VRF18_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SEP"

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VA1_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VA2_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAM_AF_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAM_IO_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAMA_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAMD_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VGP_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VGP2_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VIBR_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VIO28_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VM12_1_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VM12_2_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VM12_INT_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VMC_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VMCH_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VRF_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VRTC_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VSIM_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VSIM2_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VTCXO_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VUSB_STATUS"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptSw:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SEP"

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VAPROC_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VCORE_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VIO18_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VPA_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "BUCK_VRF18_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SEP"

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VA1_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VA2_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAM_AF_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAM_IO_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAMA_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VCAMD_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VGP_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VGP2_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VIBR_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VIO28_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VM12_1_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VM12_2_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VM12_INT_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VMC_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x30

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VMCH_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x31

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VRF_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x32

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VRTC_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VSIM_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VSIM2_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x35

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VTCXO_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x36

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "LDO_VUSB_VOLTAGE"

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/power/PMU6575;->mPromptUnit:Ljava/lang/String;

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mFiles:[[Ljava/lang/String;

    iput-boolean v4, p0, Lcom/mediatek/engineermode/power/PMU6575;->mRun:Z

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/power/PMU6575;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/power/PMU6575;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;

    invoke-direct {p0}, Lcom/mediatek/engineermode/power/PMU6575;->onTabInfo()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/power/PMU6575;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;

    invoke-direct {p0}, Lcom/mediatek/engineermode/power/PMU6575;->onTabReg()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/power/PMU6575;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;

    iget-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/power/PMU6575;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mRun:Z

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/power/PMU6575;)[[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;

    iget-object v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mFiles:[[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/power/PMU6575;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/power/PMU6575;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/power/PMU6575;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkAddr(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v2, :cond_1

    :cond_0
    move v2, v3

    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    const/16 v4, 0x10

    :try_start_0
    invoke-static {v1, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v2, v3

    goto :goto_0
.end method

.method private checkVal(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    const-string v2, "EM-PMU"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s.length() is wrong!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v2, v3

    :goto_0
    return v2

    :cond_2
    move-object v1, p1

    const/16 v4, 0x10

    :try_start_0
    invoke-static {v1, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v2, v3

    goto :goto_0
.end method

.method private getInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v4, 0x3

    :try_start_0
    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "/system/bin/sh"

    aput-object v5, v0, v4

    const/4 v4, 0x1

    const-string v5, "-c"

    aput-object v5, v0, v4

    const/4 v4, 0x2

    aput-object p1, v0, v4

    invoke-static {v0}, Lcom/mediatek/engineermode/ShellExe;->execCommand([Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "EM-PMU"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ERR.JE"

    goto :goto_0
.end method

.method private onTabInfo()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mWhichTab:I

    return-void
.end method

.method private onTabReg()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mWhichTab:I

    return-void
.end method

.method private setLayout()V
    .locals 3

    const v1, 0x7f0b01b8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mInfo:Landroid/widget/TextView;

    const v1, 0x7f0b01bf

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnGetRegister:Landroid/widget/Button;

    const v1, 0x7f0b01c0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnSetRegister:Landroid/widget/Button;

    const v1, 0x7f0b01bd

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditAddr:Landroid/widget/EditText;

    const v1, 0x7f0b01be

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditVal:Landroid/widget/EditText;

    const v1, 0x7f0b01ba

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mInfo:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnGetRegister:Landroid/widget/Button;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnSetRegister:Landroid/widget/Button;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditAddr:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditVal:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankSpinner:Landroid/widget/Spinner;

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "PMU"

    const-string v2, "clocwork worked..."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnGetRegister:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnSetRegister:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090008

    invoke-direct {v1, p0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankAdatper:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankAdatper:Landroid/widget/ArrayAdapter;

    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankType:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankAdatper:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankType:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankAdatper:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/engineermode/power/PMU6575$1;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/power/PMU6575$1;-><init>(Lcom/mediatek/engineermode/power/PMU6575;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const/4 v10, 0x1

    const/4 v4, 0x0

    iget v7, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankIndex:I

    if-nez v7, :cond_1

    const-string v4, "/sys/devices/platform/mt-pmic/pmic_access_bank0"

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    iget-object v8, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnGetRegister:Landroid/widget/Button;

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v8

    if-ne v7, v8, :cond_4

    iget-object v7, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditAddr:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/power/PMU6575;->checkAddr(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "echo "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " > "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/power/PMU6575;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cat "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/power/PMU6575;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "EM-PMU"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addr:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "out :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "EM-PMU"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addr:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "text :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditVal:Landroid/widget/EditText;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v7, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBankIndex:I

    if-ne v7, v10, :cond_2

    const-string v4, "/sys/devices/platform/mt-pmic/pmic_access_bank1"

    goto/16 :goto_0

    :cond_2
    const-string v7, "Internal error. bankX too large."

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Please check return value :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_3
    const-string v7, "Please check address."

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    iget-object v8, p0, Lcom/mediatek/engineermode/power/PMU6575;->mBtnSetRegister:Landroid/widget/Button;

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v8

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditAddr:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/mediatek/engineermode/power/PMU6575;->mEditVal:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/power/PMU6575;->checkAddr(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/power/PMU6575;->checkVal(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "echo "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " > "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/power/PMU6575;->getInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v7, "EM-PMU"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addr:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "val:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "out :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {p0, v3, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_5
    const-string v7, "Please check address or value."

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const v6, 0x7f0800ae

    const v5, 0x7f0800ad

    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/TabActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030043

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const v2, 0x7f0b01b7

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    const v2, 0x7f0b01b9

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/power/PMU6575;->setLayout()V

    new-instance v1, Lcom/mediatek/engineermode/power/PMU6575$2;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/power/PMU6575$2;-><init>(Lcom/mediatek/engineermode/power/PMU6575;)V

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/ActivityGroup;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mRun:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/ActivityGroup;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/power/PMU6575;->mRun:Z

    new-instance v0, Lcom/mediatek/engineermode/power/PMU6575$RunThread;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/power/PMU6575$RunThread;-><init>(Lcom/mediatek/engineermode/power/PMU6575;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/ActivityGroup;->onStop()V

    return-void
.end method
