.class Lcom/mediatek/engineermode/ratmode/RadioInfo$1;
.super Landroid/os/Handler;
.source "RadioInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/ratmode/RadioInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/ratmode/RadioInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v2, :cond_1

    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, [I

    check-cast v2, [I

    aget v1, v2, v6

    const-string v2, "EM/RATMode_RadioInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Get Preferred Type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2, v6, v5}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    const v3, 0x7f0803fc

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$100(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-virtual {p0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getPreferredNetworkTypeGemini(Landroid/os/Message;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
