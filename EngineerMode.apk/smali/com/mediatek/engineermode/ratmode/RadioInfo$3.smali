.class Lcom/mediatek/engineermode/ratmode/RadioInfo$3;
.super Ljava/lang/Object;
.source "RadioInfo.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/ratmode/RadioInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/ratmode/RadioInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p1    # Landroid/widget/AdapterView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    const v7, 0x7f0803fd

    const/4 v6, -0x1

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$400(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$500(Lcom/mediatek/engineermode/ratmode/RadioInfo;)I

    move-result v1

    if-eq v1, v6, :cond_0

    const-string v1, "EM/RATMode_RadioInfo"

    const-string v2, "GSM/WCDMA(auto) 3"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-virtual {v1, v5}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->writePreferred(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$100(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v1

    invoke-virtual {v1, v5, v0, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkTypeGemini(ILandroid/os/Message;I)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1, v5, v3}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1, v7, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    const-string v1, "EM/RATMode_RadioInfo"

    const-string v2, "WCDMA Preferred 0"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$500(Lcom/mediatek/engineermode/ratmode/RadioInfo;)I

    move-result v1

    if-eq v1, v6, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-virtual {v1, v4}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->writePreferred(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$100(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v1

    invoke-virtual {v1, v4, v0, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkTypeGemini(ILandroid/os/Message;I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1, v5, v3}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1, v7, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$500(Lcom/mediatek/engineermode/ratmode/RadioInfo;)I

    move-result v1

    if-eq v1, v6, :cond_2

    const-string v1, "EM/RATMode_RadioInfo"

    const-string v2, "WCDMA only 2"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->writePreferred(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$100(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkTypeGemini(ILandroid/os/Message;I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1, v5, v3}, Landroid/widget/AbsSpinner;->setSelection(IZ)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1, v7, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-virtual {v1, v3}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->writePreferred(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    invoke-static {v1}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->access$100(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v1

    invoke-virtual {v1, v3, v0, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkTypeGemini(ILandroid/os/Message;I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;->this$0:Lcom/mediatek/engineermode/ratmode/RadioInfo;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->writePreferred(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .param p1    # Landroid/widget/AdapterView;

    return-void
.end method
