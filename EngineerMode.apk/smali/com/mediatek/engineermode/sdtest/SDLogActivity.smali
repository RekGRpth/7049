.class public Lcom/mediatek/engineermode/sdtest/SDLogActivity;
.super Landroid/app/Activity;
.source "SDLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;,
        Lcom/mediatek/engineermode/sdtest/SDLogActivity$ButtonClickListener;
    }
.end annotation


# static fields
.field private static final AVAILABLESPACE:I = 0x3ffc18

.field private static final COUNT:I = 0xa

.field private static final FILECOUNT_MAX:I = 0xc8

.field private static final FILENAME:Ljava/lang/String; = "EM_SDLOG_TESTFILE"

.field private static final FODERNAME:Ljava/lang/String; = "EM_SDLog"

.field private static final LONG_TIME:I = 0x1f4

.field private static final OPERATOR_TYPE:I = 0x3

.field private static final PRE_FILE_SIZE:I = 0x100

.field private static final SDLOG_TEXT:Ljava/lang/String; = "Copyright Statement:This software/firmware and related documentation MediaTek Softwareare* protected under relevant copyright laws. The information contained herein* is confidential and proprietary to MediaTek Inc. and/or its licensors.* Without the prior written permission of MediaTek inc. and/or its licensors,* any reproduction, modification, use or disclosure of MediaTek Software,* and information contained herein, in whole or in part, shall be strictly prohibited. MediaTek Inc. (C) 2010. All rights reserved** BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS (MEDIATEK SOFTWARE)* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON* AN AS-IS BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED OF* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES* THAT IT IS RECEIVER\'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEKSOFTWARE RELEASES MADE TO RECEIVER\'S SPECIFICATION OR TO CONFORM TO A PARTICULARSTANDARD OR OPEN FORUM. RECEIVER\'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK\'S ENTIRE ANCUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,AT MEDIATEK\'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,OR REFUND ANY LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TOMEDIATEK FOR SUCH MEDIATEK  AT ISSUE.The following software/firmware and/or related documentation have been modified by MediaTek Inc. All revisions are subject to any receiver\'sapplicable license agreements with MediaTek Inc."

.field private static final SHORT_TIME:I = 0x32

.field private static final TAG:Ljava/lang/String; = "SD Log"


# instance fields
.field private mFileCount:I

.field private mFileList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRandom:Ljava/util/Random;

.field private mState:Z

.field private mThread:Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;

.field private mThreadState:Z

.field private mToggleButton:Landroid/widget/ToggleButton;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThreadState:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/sdtest/SDLogActivity;I)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getRandom(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->readFile()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->deleteFile()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mToggleButton:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mState:Z

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/sdtest/SDLogActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mState:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThread:Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->createAndWriteFile()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThreadState:Z

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    iget v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/engineermode/sdtest/SDLogActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/sdtest/SDLogActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->emptyForder(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)Ljava/util/Vector;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/sdtest/SDLogActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    return-object v0
.end method

.method private checkSDCard()V
    .locals 4

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->isSdMounted()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Warning!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Please insert SD card!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/mediatek/engineermode/sdtest/SDLogActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity$1;-><init>(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->isSdWriteable()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Warning!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "SD card isn\'t writeable!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/mediatek/engineermode/sdtest/SDLogActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity$2;-><init>(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdAvailableSpace()J

    move-result-wide v0

    const-wide/32 v2, 0x3ffc18

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Warning!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "SD card space < 4M!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/mediatek/engineermode/sdtest/SDLogActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity$3;-><init>(Lcom/mediatek/engineermode/sdtest/SDLogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private createAndWriteFile()V
    .locals 8

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdAvailableSpace()J

    move-result-wide v4

    const-wide/32 v6, 0x3ffc18

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->emptyForder(Z)V

    :cond_0
    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->isSdWriteable()Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "EM_SDLog"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "EM_SDLOG_TESTFILE"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    const-string v4, "SD Log"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CreateAndWriteFile :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EM_SDLOG_TESTFILE"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    const/4 v1, 0x0

    :goto_1
    const/16 v4, 0xa

    if-ge v1, v4, :cond_2

    :try_start_2
    const-string v4, "Copyright Statement:This software/firmware and related documentation MediaTek Softwareare* protected under relevant copyright laws. The information contained herein* is confidential and proprietary to MediaTek Inc. and/or its licensors.* Without the prior written permission of MediaTek inc. and/or its licensors,* any reproduction, modification, use or disclosure of MediaTek Software,* and information contained herein, in whole or in part, shall be strictly prohibited. MediaTek Inc. (C) 2010. All rights reserved** BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS (MEDIATEK SOFTWARE)* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON* AN AS-IS BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED OF* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES* THAT IT IS RECEIVER\'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEKSOFTWARE RELEASES MADE TO RECEIVER\'S SPECIFICATION OR TO CONFORM TO A PARTICULARSTANDARD OR OPEN FORUM. RECEIVER\'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK\'S ENTIRE ANCUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,AT MEDIATEK\'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,OR REFUND ANY LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TOMEDIATEK FOR SUCH MEDIATEK  AT ISSUE.The following software/firmware and/or related documentation have been modified by MediaTek Inc. All revisions are subject to any receiver\'sapplicable license agreements with MediaTek Inc."

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_3
    :goto_2
    return-void

    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method private createFileForder()V
    .locals 4

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->isSdMounted()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "EM_SDLog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    const-string v1, "SD Log"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createFileForder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private deleteFile()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "EM_SDLog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    iget-object v3, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getRandom(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, "SD Log"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteFile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iget-object v1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const-string v1, "SD Log"

    const-string v2, "deleteFile doesn\'t exist!"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->createAndWriteFile()V

    goto :goto_0
.end method

.method private emptyForder(Z)V
    .locals 9
    .param p1    # Z

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "EM_SDLog"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    const-string v6, "SD Log"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Delete File :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    :cond_2
    return-void
.end method

.method private getRandom(I)I
    .locals 1
    .param p1    # I

    if-gtz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mRandom:Ljava/util/Random;

    invoke-virtual {v0, p1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    goto :goto_0
.end method

.method private static getSdAvailableSpace()J
    .locals 8

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->isSdMounted()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/os/StatFs;

    invoke-direct {v3, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v6, v6

    mul-long v0, v4, v6

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static getSdPath()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mState:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThreadState:Z

    iput v1, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileCount:I

    iget-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mRandom:Ljava/util/Random;

    new-instance v0, Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;-><init>(Lcom/mediatek/engineermode/sdtest/SDLogActivity;Lcom/mediatek/engineermode/sdtest/SDLogActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThread:Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;

    return-void
.end method

.method private static isSdMounted()Z
    .locals 2

    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mounted_ro"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSdWriteable()Z
    .locals 2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private readFile()V
    .locals 8

    iget-object v5, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-lez v5, :cond_2

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getSdPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "EM_SDLog"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v5, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    iget-object v7, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->getRandom(I)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v5, "SD Log"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "readFile: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v5, 0x100

    new-array v0, v5, [B
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    :goto_0
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_1
    const-string v5, "SD Log"

    const-string v6, "readFile doesn\'t exist!"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->createAndWriteFile()V

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030022

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b00e6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mToggleButton:Landroid/widget/ToggleButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mToggleButton:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/engineermode/sdtest/SDLogActivity$ButtonClickListener;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/engineermode/sdtest/SDLogActivity$ButtonClickListener;-><init>(Lcom/mediatek/engineermode/sdtest/SDLogActivity;Lcom/mediatek/engineermode/sdtest/SDLogActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mFileList:Ljava/util/Vector;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mRandom:Ljava/util/Random;

    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->checkSDCard()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->createFileForder()V

    new-instance v0, Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;-><init>(Lcom/mediatek/engineermode/sdtest/SDLogActivity;Lcom/mediatek/engineermode/sdtest/SDLogActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThread:Lcom/mediatek/engineermode/sdtest/SDLogActivity$ActionThread;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iput-boolean v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mState:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->mThreadState:Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->emptyForder(Z)V

    const-string v0, "SD Log"

    const-string v1, "DesenceSDLogActivity onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onRestart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/sdtest/SDLogActivity;->init()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "SD Log"

    const-string v1, "DesenceSDLogActivity onStop()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
