.class public Lcom/mediatek/engineermode/swla/SwlaActivity;
.super Landroid/app/Activity;
.source "SwlaActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/swla/SwlaActivity$ButtonListener;
    }
.end annotation


# static fields
.field private static final MSG_ASSERT:I = 0x1

.field private static final MSG_SWLA_ENABLE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SWLA"


# instance fields
.field private mATCmdHander:Landroid/os/Handler;

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/swla/SwlaActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    new-instance v0, Lcom/mediatek/engineermode/swla/SwlaActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/swla/SwlaActivity$1;-><init>(Lcom/mediatek/engineermode/swla/SwlaActivity;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/swla/SwlaActivity;->mATCmdHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/swla/SwlaActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/swla/SwlaActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/swla/SwlaActivity;->sendATCommad(Ljava/lang/String;I)V

    return-void
.end method

.method private sendATCommad(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v4, 0x0

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AT+ESWLA="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/swla/SwlaActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/mediatek/engineermode/swla/SwlaActivity;->mATCmdHander:Landroid/os/Handler;

    invoke-virtual {v2, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const-string v1, "SWLA"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send ATCmd : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f03004e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b01ef

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v2, 0x7f0b01f0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/engineermode/swla/SwlaActivity$ButtonListener;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/swla/SwlaActivity$ButtonListener;-><init>(Lcom/mediatek/engineermode/swla/SwlaActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/mediatek/engineermode/swla/SwlaActivity$ButtonListener;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/swla/SwlaActivity$ButtonListener;-><init>(Lcom/mediatek/engineermode/swla/SwlaActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/swla/SwlaActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    return-void
.end method
