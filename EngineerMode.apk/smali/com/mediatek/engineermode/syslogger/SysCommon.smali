.class public Lcom/mediatek/engineermode/syslogger/SysCommon;
.super Landroid/app/Activity;
.source "SysCommon.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final BROADCAST_ACTION:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field private static final BROADCAST_KEY_COMMAND:Ljava/lang/String; = "Command"

.field private static final BROADCAST_KEY_RETURN:Ljava/lang/String; = "Return"

.field private static final BROADCAST_KEY_SELFTEST:Ljava/lang/String; = "Satan"

.field private static final BROADCAST_KEY_SRC_FROM:Ljava/lang/String; = "From"

.field private static final BROADCAST_KEY_SRC_TO:Ljava/lang/String; = "To"

.field private static final BROADCAST_VAL_COMMAND_START:Ljava/lang/String; = "Start"

.field private static final BROADCAST_VAL_COMMAND_STOP:Ljava/lang/String; = "Stop"

.field private static final BROADCAST_VAL_COMMAND_UNK:Ljava/lang/String; = "Unknown"

.field private static final BROADCAST_VAL_RETURN_ERR:Ljava/lang/String; = "Error"

.field private static final BROADCAST_VAL_RETURN_OK:Ljava/lang/String; = "Normal"

.field private static final BROADCAST_VAL_SELFTEST:Ljava/lang/String; = "Angel"

.field private static final BROADCAST_VAL_SRC_EXTMD:Ljava/lang/String; = "ExtModemLog"

.field private static final BROADCAST_VAL_SRC_HQ:Ljava/lang/String; = "CommonUI"

.field private static final BROADCAST_VAL_SRC_MD:Ljava/lang/String; = "ModemLog"

.field private static final BROADCAST_VAL_SRC_MOBILE:Ljava/lang/String; = "MobileLog"

.field private static final BROADCAST_VAL_SRC_NETWORK:Ljava/lang/String; = "ActivityNetworkLog"

.field private static final BROADCAST_VAL_SRC_UNK:Ljava/lang/String; = "Unknown"

.field private static final CHECK_INTERVAL:I = 0x3e8

.field private static final EVENT_OP_ERR:I = 0x68

.field private static final EVENT_OP_EXCEPTION:I = 0x69

.field private static final EVENT_OP_EXTMODEM_START:I = 0x6e

.field private static final EVENT_OP_EXTMODEM_STOP:I = 0x6f

.field private static final EVENT_OP_MSG:I = 0x6b

.field private static final EVENT_OP_SEARCH_FIN:I = 0x67

.field private static final EVENT_OP_SEARCH_START:I = 0x65

.field private static final EVENT_OP_TIMEOUT:I = 0x6a

.field private static final EVENT_OP_UPDATE_CK:I = 0x6c

.field private static final EVENT_TICK:I = 0x6d

.field private static final EXTERNALSDPATH:Ljava/lang/String; = "/mnt/sdcard2"

.field private static final EXTERNALSDTEXT:Ljava/lang/String; = "External SD card"

.field private static final INTERNALSDTEXT:Ljava/lang/String; = "Internal SD card"

.field private static final PER_LOG2SD:Ljava/lang/String; = "persist.radio.log2sd.path"

.field private static final PROP_EXTMD:Ljava/lang/String; = "com.mtk.extmdlogger.Running"

.field private static final PROP_MD:Ljava/lang/String; = "debug.mdlogger.Running"

.field private static final PROP_MOBILE:Ljava/lang/String; = "debug.MB.running"

.field private static final PROP_NETWORK:Ljava/lang/String; = "persist.radio.netlog.Running"

.field private static final PROP_OFF:Ljava/lang/String; = "0"

.field private static final PROP_ON:Ljava/lang/String; = "1"

.field private static final STARTEXTMODEM:[Ljava/lang/String;

.field private static final STOPEXTMODEM:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "Syslog"

.field private static final TIMEOUT:I = 0x1388


# instance fields
.field private mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;

.field private mCKAll:Landroid/widget/CheckBox;

.field private mCKExtModem:Landroid/widget/CheckBox;

.field private mCKMobile:Landroid/widget/CheckBox;

.field private mCKModem:Landroid/widget/CheckBox;

.field private mCKNetwork:Landroid/widget/CheckBox;

.field private mDialogSearchProgress:Landroid/app/ProgressDialog;

.field private mExtModemReply:Z

.field mHandler:Landroid/os/Handler;

.field private mInClickProcess:Z

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIsExtModem:Z

.field private mMsg:Ljava/lang/StringBuilder;

.field private mPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field private mSDcardPath:Landroid/widget/TextView;

.field private mTInfo:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "AT+ETSTLP=4,4"

    aput-object v1, v0, v2

    const-string v1, ""

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/engineermode/syslogger/SysCommon;->STARTEXTMODEM:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "AT+ETSTLP=0,0"

    aput-object v1, v0, v2

    const-string v1, ""

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/engineermode/syslogger/SysCommon;->STOPEXTMODEM:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mMsg:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mDialogSearchProgress:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/mediatek/engineermode/syslogger/SysCommon$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/syslogger/SysCommon$1;-><init>(Lcom/mediatek/engineermode/syslogger/SysCommon;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/syslogger/SysCommon;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/SysCommon;->sendMsghToHandler(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/syslogger/SysCommon;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SysCommon;->setCheckBoxStatus()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/syslogger/SysCommon;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mDialogSearchProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/syslogger/SysCommon;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mDialogSearchProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/syslogger/SysCommon;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mInClickProcess:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/syslogger/SysCommon;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mMsg:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/syslogger/SysCommon;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mTInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/syslogger/SysCommon;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/syslogger/SysCommon;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/syslogger/SysCommon;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/syslogger/SysCommon;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SysCommon;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private sendMsghToHandler(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/16 v2, 0x6b

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MSG"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setCheckBoxStatus()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mInClickProcess:Z

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v2, "debug.mdlogger.Running"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    const-string v2, "debug.MB.running"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const-string v2, "Syslog"

    const-string v3, "Mobile CHECKED  setCheckBoxStatus"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const-string v2, "persist.radio.netlog.Running"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const-string v2, "Syslog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NETWORK status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    const-string v2, "persist.radio.log2sd.path"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v2, "/mnt/sdcard2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Syslog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SD path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mSDcardPath:Landroid/widget/TextView;

    const-string v3, "External SD card"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const-string v2, "Syslog"

    const-string v3, "Mobile UN-CHECKED  setCheckBoxStatus"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const-string v2, "Syslog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NETWORK status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mSDcardPath:Landroid/widget/TextView;

    const-string v3, "Internal SD card"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const-wide/16 v9, 0x1388

    const/16 v8, 0x67

    const/16 v7, 0x65

    const/4 v6, 0x1

    instance-of v2, p1, Landroid/widget/CheckBox;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mIsExtModem:Z

    move-object v0, p1

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mediatek.syslogger.action"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "Satan"

    const-string v3, "Angel"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "From"

    const-string v3, "CommonUI"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    if-ne v0, v2, :cond_1

    const-string v2, "To"

    const-string v3, "ModemLog"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog"

    const-string v3, "BROADCAST_KEY_SRC_TO :ModemLog"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "Command"

    const-string v3, "Start"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog"

    const-string v3, "BROADCAST_KEY_COMMAND :Start"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mIsExtModem:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8, v9, v10}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "Syslog"

    const-string v3, "sendBroadcast over!"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    if-ne v0, v2, :cond_2

    const-string v2, "To"

    const-string v3, "MobileLog"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog"

    const-string v3, "BROADCAST_KEY_SRC_TO :MobileLog"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    if-ne v0, v2, :cond_3

    const-string v2, "To"

    const-string v3, "ActivityNetworkLog"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog"

    const-string v3, "BROADCAST_KEY_SRC_TO :ActivityNetworkLog"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    if-ne v0, v2, :cond_4

    iput-boolean v6, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mIsExtModem:Z

    const-string v2, "To"

    const-string v3, "ExtModemLog"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog"

    const-string v3, "BROADCAST_KEY_SRC_TO :ExtModemLog"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "Syslog"

    const-string v3, "$90000"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v2, "Command"

    const-string v3, "Stop"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog"

    const-string v3, "BROADCAST_KEY_COMMAND :Stop"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    sget-object v3, Lcom/mediatek/engineermode/syslogger/SysCommon;->STARTEXTMODEM:[Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x6e

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    :goto_3
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8, v9, v10}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    sget-object v3, Lcom/mediatek/engineermode/syslogger/SysCommon;->STOPEXTMODEM:[Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x6f

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    goto :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/16 v2, 0x8

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030052

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0200

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    const v0, 0x7f0b0201

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    const v0, 0x7f0b0202

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    const v0, 0x7f0b0206

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKAll:Landroid/widget/CheckBox;

    const v0, 0x7f0b0204

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    const v0, 0x7f0b0208

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mTInfo:Landroid/widget/TextView;

    const v0, 0x7f0b0205

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mSDcardPath:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKAll:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mTInfo:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "Syslog"

    const-string v1, "clocwork worked..."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKModem:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKMobile:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKNetwork:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKAll:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mCKExtModem:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;-><init>(Lcom/mediatek/engineermode/syslogger/SysCommon;Lcom/mediatek/engineermode/syslogger/SysCommon$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mIntentFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mMsg:Ljava/lang/StringBuilder;

    const-string v1, "Standby...\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6d

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mInClickProcess:Z

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SysCommon;->setCheckBoxStatus()V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6d

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mTInfo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mMsg:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SysCommon$SYSLogBroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SysCommon;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
