.class public abstract Lcom/mediatek/engineermode/syslogger/SysUtils;
.super Ljava/lang/Object;
.source "SysUtils.java"


# static fields
.field public static final ACTION_EXP_HAPPENED:Ljava/lang/String; = "com.mediatek.log2server.EXCEPTION_HAPPEND"

.field public static final ACTION_MDLOG_READY:Ljava/lang/String; = "com.mediatek.mdlogger.MEMORYDUMP_DONE"

.field public static final ACTION_TAGLOG_SWITCH:Ljava/lang/String; = "com.mediatek.taglog.switch"

.field public static final ACTION_TAGLOG_TO_LOG2SERVER:Ljava/lang/String; = "com.mediatek.syslogger.taglog"

.field public static final BROADCAST_ACTION:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field public static final BROADCAST_KEY_COMMAND:Ljava/lang/String; = "Command"

.field public static final BROADCAST_KEY_MDEXP_LOGPATH:Ljava/lang/String; = "LogPath"

.field public static final BROADCAST_KEY_MDLOG_PATH:Ljava/lang/String; = "ModemLogPath"

.field public static final BROADCAST_KEY_MOBILELOG_PATH:Ljava/lang/String; = "MobileLogPath"

.field public static final BROADCAST_KEY_NETLOG_PATH:Ljava/lang/String; = "NetLogPath"

.field public static final BROADCAST_KEY_RETURN:Ljava/lang/String; = "Return"

.field public static final BROADCAST_KEY_SELFTEST:Ljava/lang/String; = "Satan"

.field public static final BROADCAST_KEY_SRC_FROM:Ljava/lang/String; = "From"

.field public static final BROADCAST_KEY_SRC_TO:Ljava/lang/String; = "To"

.field public static final BROADCAST_KEY_TAGLOG_RESULT:Ljava/lang/String; = "TaglogResult"

.field public static final BROADCAST_VAL_COMMAND_START:Ljava/lang/String; = "Start"

.field public static final BROADCAST_VAL_COMMAND_STOP:Ljava/lang/String; = "Stop"

.field public static final BROADCAST_VAL_COMMAND_UNK:Ljava/lang/String; = "Unknown"

.field public static final BROADCAST_VAL_LOGTOOL_STOPPED:Ljava/lang/String; = "LogToolStopped"

.field public static final BROADCAST_VAL_RETURN_ERR:Ljava/lang/String; = "Error"

.field public static final BROADCAST_VAL_RETURN_OK:Ljava/lang/String; = "Normal"

.field public static final BROADCAST_VAL_SELFTEST:Ljava/lang/String; = "Angel"

.field public static final BROADCAST_VAL_SRC_EXTMD:Ljava/lang/String; = "ExtModemLog"

.field public static final BROADCAST_VAL_SRC_HQ:Ljava/lang/String; = "CommonUI"

.field public static final BROADCAST_VAL_SRC_MD:Ljava/lang/String; = "ModemLog"

.field public static final BROADCAST_VAL_SRC_MOBILE:Ljava/lang/String; = "MobileLog"

.field public static final BROADCAST_VAL_SRC_NETWORK:Ljava/lang/String; = "ActivityNetworkLog"

.field public static final BROADCAST_VAL_SRC_UNK:Ljava/lang/String; = "Unknown"

.field public static final BROADCAST_VAL_TAGLOG_CANCEL:Ljava/lang/String; = "Cancel"

.field public static final BROADCAST_VAL_TAGLOG_FAILED:Ljava/lang/String; = "Failed"

.field public static final BROADCAST_VAL_TAGLOG_SUCCESS:Ljava/lang/String; = "Successful"

.field public static final CALIBRATION_DATA:Ljava/lang/String; = "calibration_data"

.field public static final CALIBRATION_DATA_KEY:Ljava/lang/String; = "calibrationData"

.field public static final DATAPATH:Ljava/lang/String; = "/data"

.field public static final DIALOG_ALL_LOGTOOL_STOPED:I = 0x12e

.field public static final DIALOG_INPUT:I = 0x12d

.field public static final DIALOG_LOCK_OF_SDSPACE:I = 0x12f

.field public static final DIALOG_START_PROGRESS:I = 0x132

.field public static final DIALOG_ZIP_LOG_FAIL:I = 0x131

.field public static final DIALOG_ZIP_LOG_SUCCESS:I = 0x130

.field public static final EVENT_ALL_LOGTOOL_STOPED:I = 0xcd

.field public static final EVENT_CHECK_INPUTDIALOG_TIMEOUT:I = 0xd1

.field public static final EVENT_CREATE_INPUTDIALOG:I = 0xcb

.field public static final EVENT_GET_EXCEPTION_TYPE:I = 0xc9

.field public static final EVENT_LOCK_OF_SDSPACE:I = 0xd0

.field public static final EVENT_OP_ERR:I = 0x68

.field public static final EVENT_OP_EXCEPTION:I = 0x69

.field public static final EVENT_OP_MSG:I = 0x6b

.field public static final EVENT_OP_SEARCH_FIN:I = 0x67

.field public static final EVENT_OP_SEARCH_START:I = 0x65

.field public static final EVENT_OP_TIMEOUT:I = 0x6a

.field public static final EVENT_OP_UPDATE_CK:I = 0x6c

.field public static final EVENT_TICK:I = 0x6d

.field public static final EVENT_ZIP_ALL_LOG:I = 0xca

.field public static final EVENT_ZIP_LOG_FAIL:I = 0xcf

.field public static final EVENT_ZIP_LOG_SUCCESS:I = 0xce

.field public static final EXCP_TAGLOG_DESCRIPTION:Ljava/lang/String; = ""

.field public static final EXCP_TAGLOG_PROCESS:Ljava/lang/String; = "manual dump"

.field public static final EXCP_TAGLOG_TYPE:Ljava/lang/String; = "Kernel Module"

.field public static final EXTERNALSDPATH:Ljava/lang/String; = "/mnt/sdcard2"

.field public static final EXTERNALSDTEXT:Ljava/lang/String; = "External SD card"

.field public static final EXTMODEM_LOG_FOLDER:Ljava/lang/String; = "mtklog/extmdlog"

.field public static final EXTRA_KEY_EXP_NAME:Ljava/lang/String; = "db_filename"

.field public static final EXTRA_KEY_EXP_PATH:Ljava/lang/String; = "path"

.field public static final EXTRA_KEY_EXP_ZZ:Ljava/lang/String; = "zz_filename"

.field public static final INTERNALSDPATH:Ljava/lang/String; = "/mnt/sdcard"

.field public static final INTERNALSDTEXT:Ljava/lang/String; = "Internal SD card"

.field private static final KEY_BASEBAND_VERSION:Ljava/lang/String; = "gsm.version.baseband"

.field private static final KEY_BUILD_NUMBER:Ljava/lang/String; = "ro.build.display.id"

.field private static final KEY_BUILD_PRODUCT:Ljava/lang/String; = "ro.build.product"

.field private static final KEY_BUILD_TYPE:Ljava/lang/String; = "ro.build.type"

.field public static final LOG_QUANTITY:I = 0x3

.field public static final MANUAL_SAVE_LOG:Ljava/lang/String; = "SaveLogManually"

.field public static final MOBILE_LOG_FOLDER:Ljava/lang/String; = "mtklog/mobilelog"

.field public static final MODEM_LOG_FOLDER:Ljava/lang/String; = "mtklog/mdlog"

.field public static final MSG_ALL_LOGTOOL_STOPED:Ljava/lang/String; = "Want you start all log tools?"

.field public static final MSG_LOCK_OF_SDSPACE:Ljava/lang/String; = "Please release some space"

.field public static final MSG_SD_NOT_EXIST:Ljava/lang/String; = "The SD Card doesn\'t exist"

.field public static final MSG_SD_NOT_WRITABLE:Ljava/lang/String; = "The SD Card is not writtable"

.field public static final MSG_ZIP_LOG_FAIL:Ljava/lang/String; = "Zip Tag log failed!"

.field public static final NET_LOG_FOLDER:Ljava/lang/String; = "mtklog/netlog"

.field public static final PATH_SEPARATER:Ljava/lang/String; = "/"

.field public static final PER_LOG2SD:Ljava/lang/String; = "persist.radio.log2sd.path"

.field public static final PROP_EXTMD:Ljava/lang/String; = "com.mtk.extmdlogger.Running"

.field public static final PROP_MD:Ljava/lang/String; = "debug.mdlogger.Running"

.field public static final PROP_MOBILE:Ljava/lang/String; = "debug.MB.running"

.field public static final PROP_NETWORK:Ljava/lang/String; = "persist.radio.netlog.Running"

.field public static final PROP_OFF:Ljava/lang/String; = "0"

.field public static final PROP_ON:Ljava/lang/String; = "1"

.field public static final SD_LOCK_OF_SPACE:I = 0x192

.field public static final SD_NORMAL:I = 0x191

.field public static final SD_NOT_EXIST:I = 0x193

.field public static final SD_NOT_WRITABLE:I = 0x194

.field private static final TAG:Ljava/lang/String; = "Syslog"

.field public static final TAGLOG_SWITCH:Ljava/lang/String; = "taglog_switch"

.field public static final TAGLOG_SWITCH_KEY:Ljava/lang/String; = "taglog_switch_key"

.field public static final TITLE_ALL_LOGTOOL_STOPED:Ljava/lang/String; = "All log tools are stopped!"

.field public static final TITLE_INPUT_TAG:Ljava/lang/String; = "[Tag Log] Please input tag (letters&numbers&\'_\'&\' \' only):"

.field public static final TITLE_LOCK_OF_SDSPACE:Ljava/lang/String; = "[Tag Log] Lack of SDCard space!"

.field public static final TITLE_WARNING:Ljava/lang/String; = "[Tag Log] Warning!"

.field public static final TITLE_ZIP_LOG_FAIL:Ljava/lang/String; = "[Tag Log] Failed!"

.field public static final ZIP_LOG_SUFFIX:Ljava/lang/String; = ".zip"

.field public static final ZIP_TAG_LOG_FOLDER:Ljava/lang/String; = "mtklog/taglog"

.field public static final ZZ_INTERNAL_LENGTH:I = 0xa


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBuildNumber()Ljava/lang/String;
    .locals 8

    const-string v5, "ro.build.display.id"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "ro.build.product"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "ro.build.type"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "gsm.version.baseband"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "===Build Version Information==="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\nBuild Number: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\nThe production is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " with "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " build"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\nAnd the baseband version is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "Syslog"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Build number is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getIMEI(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    const-string v0, "The device IMEI is "

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SIM1: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SIM2: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Syslog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Flashed IMEI? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public static getIVSR(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;

    const-string v3, "ivsr_setting"

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-static {v4, v3, v5, v6}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The IVSR is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Syslog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IVSR enable status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2
.end method

.method public static hasCalibrationData(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x0

    const-string v2, "calibration_data"

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "calibrationData"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "Syslog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Calibration data is written? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public static writeCheckSOPToFile(Landroid/content/Context;Ljava/io/File;)Z
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/io/File;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_1
    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SysUtils;->hasCalibrationData(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SysUtils;->getIVSR(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SysUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/mediatek/engineermode/syslogger/SysUtils;->getBuildNumber()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Calibration data is downloaded: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuffer;

    const-string v9, "Check SOP result:\n"

    invoke-direct {v3, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v8, Ljava/io/OutputStreamWriter;

    invoke-direct {v8, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/OutputStreamWriter;->flush()V

    invoke-virtual {v8}, Ljava/io/OutputStreamWriter;->close()V

    const-string v9, "Syslog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "The check sop string is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "Syslog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Write the check sop to file: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    const/4 v9, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method
