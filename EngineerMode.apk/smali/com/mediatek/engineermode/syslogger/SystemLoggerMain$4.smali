.class Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;
.super Landroid/os/Handler;
.source "SystemLoggerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const-wide/16 v5, 0xbb8

    const/4 v4, 0x1

    const/16 v2, 0x67

    const/4 v3, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Syslog"

    const-string v1, "Create progress dialog."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1002(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08014f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x7f08014e

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_1
    const-string v0, "Syslog"

    const-string v1, "new mDialogSearchProgress failed"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1100(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Syslog"

    const-string v1, "EVENT_WAITING_CHECK_STOP: checklog status return false"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1200(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1300(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_2
    const-string v0, "Syslog"

    const-string v1, "EVENT_WAITING_CHECK_STOP: re-check"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$300(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x70

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$300(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)I

    move-result v0

    const/16 v1, 0x222

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1400(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/ToggleButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1200(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1300(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "Syslog"

    const-string v1, "Stop waiting dialog"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1002(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1100(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Z

    const/16 v0, 0x6d

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    invoke-static {v1, v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1500(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/os/AsyncResult;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    invoke-static {v1, v0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$1600(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/os/AsyncResult;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_3
    .end packed-switch
.end method
