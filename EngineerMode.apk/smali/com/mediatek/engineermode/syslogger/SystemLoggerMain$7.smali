.class Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;
.super Ljava/lang/Object;
.source "SystemLoggerMain.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->clearLog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iput-object p2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;->val$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v0, "Syslog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delete tag log folder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-static {v2}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$2100(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;->val$file:Ljava/io/File;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->deleteFolder(Ljava/io/File;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
