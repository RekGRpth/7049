.class Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SystemLoggerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SystemLoggerBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p2    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$1;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "Syslog"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SystemLoggerBroadcastReceiver: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "Satan"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v5, "Angel"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Syslog"

    const-string v6, "Receive loop message."

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v5, "From"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Return"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v4, :cond_1

    if-nez v2, :cond_2

    :cond_1
    const-string v5, "Syslog"

    const-string v6, "Received null source or status"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    const-string v5, "Normal"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "Syslog"

    const-string v6, "Receive Nomal"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_1
    const-string v5, "ModemLog"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v6, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    if-eqz v1, :cond_5

    const/16 v5, 0x100

    :goto_2
    invoke-static {v6, v5}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$376(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;I)I

    :goto_3
    const-string v5, "Syslog"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "src: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const-string v5, "Syslog"

    const-string v6, "SystemLoggerBroadcastReceiver end"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/16 v5, 0x200

    goto :goto_2

    :cond_6
    const-string v5, "MobileLog"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v6, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    if-eqz v1, :cond_7

    const/16 v5, 0x10

    :goto_4
    invoke-static {v6, v5}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$376(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;I)I

    goto :goto_3

    :cond_7
    const/16 v5, 0x20

    goto :goto_4

    :cond_8
    const-string v5, "ActivityNetworkLog"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v6, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    if-eqz v1, :cond_9

    const/4 v5, 0x1

    :goto_5
    invoke-static {v6, v5}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->access$376(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;I)I

    goto :goto_3

    :cond_9
    const/4 v5, 0x2

    goto :goto_5

    :cond_a
    const-string v5, "Syslog"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unknow source: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
