.class public Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
.super Landroid/app/Activity;
.source "SystemLoggerMain.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final AT_CMD_START_EXTMODEM:[Ljava/lang/String;

.field private static final AT_CMD_STOP_EXTMODEM:[Ljava/lang/String;

.field private static final BROADCAST_ACTION:Ljava/lang/String; = "com.mediatek.syslogger.action"

.field private static final BROADCAST_KEY_COMMAND:Ljava/lang/String; = "Command"

.field private static final BROADCAST_KEY_RETURN:Ljava/lang/String; = "Return"

.field private static final BROADCAST_KEY_SELFTEST:Ljava/lang/String; = "Satan"

.field private static final BROADCAST_KEY_SRC_FROM:Ljava/lang/String; = "From"

.field private static final BROADCAST_KEY_SRC_TO:Ljava/lang/String; = "To"

.field private static final BROADCAST_VAL_COMMAND_START:Ljava/lang/String; = "Start"

.field private static final BROADCAST_VAL_COMMAND_STOP:Ljava/lang/String; = "Stop"

.field private static final BROADCAST_VAL_COMMAND_UNK:Ljava/lang/String; = "Unknown"

.field private static final BROADCAST_VAL_RETURN_ERR:Ljava/lang/String; = "Error"

.field private static final BROADCAST_VAL_RETURN_OK:Ljava/lang/String; = "Normal"

.field private static final BROADCAST_VAL_SELFTEST:Ljava/lang/String; = "Angel"

.field private static final BROADCAST_VAL_SRC_EXTMODEMLOG:Ljava/lang/String; = "ExtModemLog"

.field private static final BROADCAST_VAL_SRC_MOBILELOG:Ljava/lang/String; = "MobileLog"

.field private static final BROADCAST_VAL_SRC_MODEMLOG:Ljava/lang/String; = "ModemLog"

.field private static final BROADCAST_VAL_SRC_NETWORKLOG:Ljava/lang/String; = "ActivityNetworkLog"

.field private static final BROADCAST_VAL_SRC_SYSLOGGER:Ljava/lang/String; = "CommonUI"

.field private static final BROADCAST_VAL_SRC_UNKNOWN:Ljava/lang/String; = "Unknown"

.field private static final CREATE_WAITING_DIALOG:I = 0x0

.field private static final DIALOG_INPUT_PASSWORD:I = 0x1

.field private static final EVENT_CHECK_STATUS:I = 0x6d

.field private static final EVENT_ERR:I = 0x68

.field private static final EVENT_EXCEPTION:I = 0x69

.field private static final EVENT_EXTMODEM_START:I = 0x6e

.field private static final EVENT_EXTMODEM_STOP:I = 0x6f

.field private static final EVENT_SHOW_MESSEG:I = 0x6b

.field private static final EVENT_UPDATE_STATUS:I = 0x6c

.field private static final EVENT_WAITING_CHECK_START:I = 0x70

.field private static final EVENT_WAITING_CHECK_STOP:I = 0x66

.field private static final EVENT_WAITING_END:I = 0x67

.field private static final EVENT_WAITING_START:I = 0x65

.field private static final EVENT_WAITING_TIMEOUT:I = 0x6a

.field private static final EXTERNAL_SD_PATH:Ljava/lang/String; = "/mnt/sdcard2"

.field private static final INTERNAL_SD_PATH:Ljava/lang/String; = "/mnt/sdcard"

.field private static final MTKLOG_FOLDER:Ljava/lang/String; = "/mtklog"

.field private static final PASSWORD:Ljava/lang/String; = "3646633"

.field private static final PROPERTY_EXTMODEMLOG:Ljava/lang/String; = "com.mtk.extmdlogger.Running"

.field private static final PROPERTY_LOG2SD:Ljava/lang/String; = "persist.radio.log2sd.path"

.field private static final PROPERTY_LOG_OFF:Ljava/lang/String; = "0"

.field private static final PROPERTY_LOG_ON:Ljava/lang/String; = "1"

.field private static final PROPERTY_MOBILELOG:Ljava/lang/String; = "debug.MB.running"

.field private static final PROPERTY_MODEMLOG:Ljava/lang/String; = "debug.mdlogger.Running"

.field private static final PROPERTY_NETWORKLOG:Ljava/lang/String; = "persist.radio.netlog.Running"

.field private static final STATUS_START:I = 0x0

.field private static final STOP_WAITING_DIALOG:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Syslog"

.field private static final TAGLOG_FOLDER:Ljava/lang/String; = "/taglog"


# instance fields
.field private mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;

.field private mButtonClearLog:Landroid/widget/Button;

.field private mButtonStart:Landroid/widget/ToggleButton;

.field private mButtonTagLog:Landroid/widget/Button;

.field private mDialogSearchProgress:Landroid/app/ProgressDialog;

.field private mExtModemReply:Z

.field private mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field mHandler:Landroid/os/Handler;

.field private mHasExtModem:Z

.field private mImageFlagExtModemLog:Landroid/widget/ImageView;

.field private mImageFlagMobileLog:Landroid/widget/ImageView;

.field private mImageFlagModemLog:Landroid/widget/ImageView;

.field private mImageFlagNetworkLog:Landroid/widget/ImageView;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIsExtModemLogRunning:Z

.field private mIsMobileLogRunning:Z

.field private mIsModemLogRunning:Z

.field private mIsNetworkLogRunning:Z

.field private mIsRunning:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mReceivedStatus:I

.field private mSync:Ljava/lang/Object;

.field private mTagLogPath:Ljava/lang/String;

.field private mTextLogPath:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "AT+ETSTLP=4,4"

    aput-object v1, v0, v2

    const-string v1, ""

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->AT_CMD_START_EXTMODEM:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "AT+ETSTLP=0,0"

    aput-object v1, v0, v2

    const-string v1, ""

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->AT_CMD_STOP_EXTMODEM:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mReceivedStatus:I

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mSync:Ljava/lang/Object;

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mDialogSearchProgress:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$4;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->showConfirmDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mDialogSearchProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mDialogSearchProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->checkLogStatus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonTagLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonClearLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonStart:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p1    # Landroid/os/AsyncResult;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->startExtModemLog(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p1    # Landroid/os/AsyncResult;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->stopExtModemLog(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->stopMobileLog()V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->stopModemLog()V

    return-void
.end method

.method static synthetic access$1900(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->stopNetworkLog()V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->stopExtModemLogByATCmd()V

    return-void
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsRunning:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mReceivedStatus:I

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mReceivedStatus:I

    return p1
.end method

.method static synthetic access$376(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;I)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mReceivedStatus:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mReceivedStatus:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->startMobileLog()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->startModemLog()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->startNetworkLog()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHasExtModem:Z

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->startExtModemLogByATCmd()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->clearLog()V

    return-void
.end method

.method private checkLogStatus()Z
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v4, "debug.mdlogger.Running"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iput-boolean v8, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsModemLogRunning:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagModemLog:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->setLogRunningImage(Landroid/widget/ImageView;)V

    :goto_0
    const-string v4, "debug.MB.running"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    iput-boolean v8, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsMobileLogRunning:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagMobileLog:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->setLogRunningImage(Landroid/widget/ImageView;)V

    :goto_1
    const-string v4, "persist.radio.netlog.Running"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    iput-boolean v8, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsNetworkLogRunning:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagNetworkLog:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->setLogRunningImage(Landroid/widget/ImageView;)V

    :goto_2
    const/4 v3, 0x0

    const-string v4, "storage"

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    const-string v4, "persist.radio.log2sd.path"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v4, "/mnt/sdcard2"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Syslog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get sd path from extternal path:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/mtklog"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/taglog"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTextLogPath:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsMobileLogRunning:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsModemLogRunning:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsNetworkLogRunning:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsExtModemLogRunning:Z

    if-eqz v4, :cond_8

    :cond_1
    iput-boolean v8, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsRunning:Z

    :goto_4
    iget-boolean v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsRunning:Z

    return v4

    :cond_2
    iput-boolean v7, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsModemLogRunning:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagModemLog:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->setLogStopImage(Landroid/widget/ImageView;)V

    goto/16 :goto_0

    :cond_3
    iput-boolean v7, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsMobileLogRunning:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagMobileLog:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->setLogStopImage(Landroid/widget/ImageView;)V

    goto/16 :goto_1

    :cond_4
    iput-boolean v7, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsNetworkLogRunning:Z

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagNetworkLog:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->setLogStopImage(Landroid/widget/ImageView;)V

    goto/16 :goto_2

    :cond_5
    const-string v4, "/mnt/sdcard"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getInternalStoragePath()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Syslog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get sd path is null from internal path: may be not emmc"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_6
    const-string v4, "Syslog"

    const-string v5, "Get SD path from System property is empty, set default internal sd path."

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getInternalStoragePath()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Syslog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get sd path is null from internal path: may be not emmc"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_7
    const-string v3, ""

    goto/16 :goto_3

    :cond_8
    iput-boolean v7, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsRunning:Z

    goto :goto_4
.end method

.method private clearLog()V
    .locals 6

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v3, "Syslog"

    const-string v4, "Log path is null or empty"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    const-string v3, "Syslog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " doesn\'t exist! Error"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v3, "Syslog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete the folder "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTagLogPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f08014c

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08014d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    new-instance v5, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;

    invoke-direct {v5, p0, v2}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$7;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Ljava/io/File;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040009

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private prepareIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "Satan"

    const-string v2, "Angel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "From"

    const-string v2, "CommonUI"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private setLogRunningImage(Landroid/widget/ImageView;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;

    const v0, 0x7f020003

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private setLogStopImage(Landroid/widget/ImageView;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;

    const v0, 0x7f020002

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private showConfirmDialog()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08014a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08014b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x1040013

    new-instance v2, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$5;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$5;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x1040009

    new-instance v2, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$6;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$6;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private startExtModemLog(Landroid/os/AsyncResult;)V
    .locals 3
    .param p1    # Landroid/os/AsyncResult;

    const/4 v2, 0x0

    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_0

    const-string v1, "Change port success!"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "Satan"

    const-string v2, "Angel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "From"

    const-string v2, "CommonUI"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "To"

    const-string v2, "ExtModemLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "Syslog"

    const-string v2, "sendBroadcast to extModem!"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Syslog"

    const-string v2, "To:ExtModemLog"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Syslog"

    const-string v2, "Command:Start"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "Change port failed!"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private startExtModemLogByATCmd()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    sget-object v1, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->AT_CMD_START_EXTMODEM:[Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x6e

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    return-void
.end method

.method private startMobileLog()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "To"

    const-string v2, "MobileLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private startModemLog()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "To"

    const-string v2, "ModemLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private startNetworkLog()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "To"

    const-string v2, "ActivityNetworkLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private stopExtModemLog(Landroid/os/AsyncResult;)V
    .locals 3
    .param p1    # Landroid/os/AsyncResult;

    const/4 v2, 0x0

    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_0

    const-string v1, "Change port success!"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "Satan"

    const-string v2, "Angel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "From"

    const-string v2, "CommonUI"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "To"

    const-string v2, "ExtModemLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Stop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "Syslog"

    const-string v2, "sendBroadcast to extModem!"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Syslog"

    const-string v2, "To:ExtModemLog"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Syslog"

    const-string v2, "Command:Stop"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "Change port failed!"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private stopExtModemLogByATCmd()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    sget-object v1, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->AT_CMD_STOP_EXTMODEM:[Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x6f

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    return-void
.end method

.method private stopMobileLog()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "To"

    const-string v2, "MobileLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Stop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private stopModemLog()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "To"

    const-string v2, "ModemLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Stop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private stopNetworkLog()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->prepareIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "To"

    const-string v2, "ActivityNetworkLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Command"

    const-string v2, "Stop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030051

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b01fd

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonStart:Landroid/widget/ToggleButton;

    const v0, 0x7f0b01fe

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonTagLog:Landroid/widget/Button;

    const v0, 0x7f0b01ff

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonClearLog:Landroid/widget/Button;

    const v0, 0x7f0b01f4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagMobileLog:Landroid/widget/ImageView;

    const v0, 0x7f0b01f6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagModemLog:Landroid/widget/ImageView;

    const v0, 0x7f0b01f8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagNetworkLog:Landroid/widget/ImageView;

    const v0, 0x7f0b01fa

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagExtModemLog:Landroid/widget/ImageView;

    const v0, 0x7f0b01f2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mTextLogPath:Landroid/widget/TextView;

    new-instance v0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIntentFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsMobileLogRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsModemLogRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsNetworkLogRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIsExtModemLogRunning:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonStart:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$1;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$1;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonTagLog:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$2;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$2;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonClearLog:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$3;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$3;-><init>(Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mImageFlagExtModemLog:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f0b01fb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHasExtModem:Z

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->checkLogStatus()Z

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonStart:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonTagLog:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mButtonClearLog:Landroid/widget/Button;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6d

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mBroadcastReceiver:Lcom/mediatek/engineermode/syslogger/SystemLoggerMain$SystemLoggerBroadcastReceiver;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/SystemLoggerMain;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
