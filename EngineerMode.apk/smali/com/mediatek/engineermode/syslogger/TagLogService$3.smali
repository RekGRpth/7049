.class Lcom/mediatek/engineermode/syslogger/TagLogService$3;
.super Ljava/lang/Object;
.source "TagLogService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/engineermode/syslogger/TagLogService;->createDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

.field final synthetic val$inputText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/syslogger/TagLogService;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    iput-object p2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->val$inputText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->val$inputText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$902(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->val$inputText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1702(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1700(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const-string v2, "taglog"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1702(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    const-string v1, "Syslog_taglog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Input tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v3}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1700(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$800(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    move-result-object v1

    const/16 v2, 0x132

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$3;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2400(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    move-result-object v1

    const/16 v2, 0xca

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
