.class Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;
.super Landroid/os/Handler;
.source "TagLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/syslogger/TagLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FunctionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/syslogger/TagLogService;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const-string v9, "Syslog_taglog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " SubHandler handleMessage --> start "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Landroid/os/Message;->what:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v9, p1, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_0

    :goto_0
    const-string v9, "Syslog_taglog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " SubHandler handleMessage --> end "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Landroid/os/Message;->what:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :pswitch_0
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1300(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1400(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x3

    new-array v1, v9, [Ljava/io/File;

    invoke-static {v6, v5}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->checkSdCardSpace(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    const/16 v9, 0x191

    if-eq v7, v9, :cond_0

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$800(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_0
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v10, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$600(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$502(Lcom/mediatek/engineermode/syslogger/TagLogService;[Z)[Z

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1500(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)V

    const/4 v3, 0x0

    :goto_2
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v9

    array-length v9, v9

    if-ge v3, v9, :cond_2

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v9

    aget-boolean v9, v9, v3

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    aget-object v10, v5, v3

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1600(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    aput-object v9, v1, v3

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1500(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)V

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1700(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->createTagLogFolder(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v10, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v10, v1, v8}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1900(Lcom/mediatek/engineermode/syslogger/TagLogService;[Ljava/io/File;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1802(Lcom/mediatek/engineermode/syslogger/TagLogService;[Ljava/lang/String;)[Ljava/lang/String;

    const/4 v3, 0x0

    :goto_3
    const/4 v9, 0x3

    if-ge v3, v9, :cond_4

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v3

    if-nez v9, :cond_3

    const-string v9, "Syslog_taglog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mLogPathInTagLog["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "= null"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    const-string v9, "Syslog_taglog"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mLogPathInTagLog["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v11}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v11

    aget-object v11, v11, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_4
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$200(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$300(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v8}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->writeFolderToTagFolder(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$400(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2000(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v9

    if-eqz v9, :cond_a

    const-string v9, "Syslog_taglog"

    const-string v10, "Modem Log is Ready"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    new-instance v12, Ljava/io/File;

    iget-object v13, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v12, v8}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2200(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    :cond_6
    :goto_5
    new-instance v0, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "checksop.txt"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_7
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_6
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9, v0}, Lcom/mediatek/engineermode/syslogger/SysUtils;->writeCheckSOPToFile(Landroid/content/Context;Ljava/io/File;)Z

    :cond_8
    const/4 v4, 0x1

    const/4 v3, 0x0

    :goto_7
    const/4 v9, 0x3

    if-ge v3, v9, :cond_c

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v9

    aget-boolean v9, v9, v3

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v3

    if-nez v9, :cond_9

    const/4 v4, 0x0

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_a
    :try_start_1
    const-string v9, "Syslog_taglog"

    const-string v10, "Modem Log is not Ready , wait 1 min"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v9, 0x3e8

    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_8
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2000(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v9

    if-eqz v9, :cond_b

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    new-instance v12, Ljava/io/File;

    iget-object v13, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v12, v8}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2200(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    goto :goto_5

    :catch_0
    move-exception v2

    const-string v9, "Syslog_taglog"

    const-string v10, "Catch InterruptedException"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :cond_b
    const-string v9, "Syslog_taglog"

    const-string v10, "Modem Log is not Ready , search modem log from SD card!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v12, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v13, 0x0

    aget-object v13, v5, v13

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2300(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    invoke-static {v11, v12, v8}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2200(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    goto/16 :goto_5

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_6

    :cond_c
    if-eqz v4, :cond_d

    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$800(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    move-result-object v9

    const/16 v10, 0xce

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_d
    iget-object v9, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v9}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$800(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    move-result-object v9

    const/16 v10, 0xcf

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_0
    .end packed-switch
.end method
