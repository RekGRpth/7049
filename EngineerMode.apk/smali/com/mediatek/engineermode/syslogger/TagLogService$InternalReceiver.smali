.class Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TagLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/syslogger/TagLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "com.mediatek.mdlogger.MEMORYDUMP_DONE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const-string v0, "Syslog_taglog"

    const-string v1, "InternalReceiver --> com.mediatek.mdlogger.MEMORYDUMP_DONE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2600(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Syslog_taglog"

    const-string v1, "This broadcast have received!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2602(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2002(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const-string v1, "LogPath"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2102(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "Syslog_taglog"

    const-string v1, " mModemLogPath is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "Syslog_taglog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mModemLogPath is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$2100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
