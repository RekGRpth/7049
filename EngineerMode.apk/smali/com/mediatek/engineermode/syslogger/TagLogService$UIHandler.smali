.class Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;
.super Landroid/os/Handler;
.source "TagLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/syslogger/TagLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UIHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/syslogger/TagLogService;Lcom/mediatek/engineermode/syslogger/TagLogService$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p2    # Lcom/mediatek/engineermode/syslogger/TagLogService$1;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1    # Landroid/os/Message;

    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " MyHandler handleMessage --> start "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    iget v14, v0, Landroid/os/Message;->what:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    sparse-switch v12, :sswitch_data_0

    :cond_0
    :goto_0
    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " MyHandler handleMessage --> end "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    iget v14, v0, Landroid/os/Message;->what:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "path"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v12, "Syslog_taglog"

    const-string v13, "params are not valid! exp_path is null"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-virtual {v12}, Landroid/app/Service;->stopSelf()V

    goto :goto_1

    :cond_1
    const-string v12, "SaveLogManually"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$202(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z

    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$600(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$502(Lcom/mediatek/engineermode/syslogger/TagLogService;[Z)[Z

    const/4 v9, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v12

    array-length v12, v12

    if-ge v9, v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$400(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v12

    if-nez v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v12

    aget-boolean v12, v12, v9

    if-eqz v12, :cond_a

    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x12d

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$800(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    move-result-object v12

    const/16 v13, 0xd1

    const-wide/32 v14, 0x1d4c0

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$202(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "db_filename"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "zz_filename"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v3, :cond_5

    if-nez v10, :cond_6

    :cond_5
    const-string v12, "Syslog_taglog"

    const-string v13, "params are not valid! exp_file name is null"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-virtual {v12}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12, v6}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$302(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v5, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;

    invoke-direct {v5}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;-><init>()V

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "exp_path: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "exp_file: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5, v4}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->setmPath(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v5, v11}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->initFieldsFromZZ(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v5}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->getmType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->getmDiscription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->getmProcess()Ljava/lang/String;

    move-result-object v7

    if-eqz v8, :cond_7

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "exp_info.getmType(): "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    if-eqz v2, :cond_8

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "exp_info.getmDiscription(): "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    if-eqz v7, :cond_9

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "exp_info.getmProcess(): "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    if-eqz v8, :cond_2

    const-string v12, "Externel (EE)"

    invoke-virtual {v8, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "expType == External (EE) "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$402(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z

    goto/16 :goto_2

    :catch_0
    move-exception v1

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "fail to init exception info:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-virtual {v12}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z

    move-result-object v12

    array-length v12, v12

    add-int/lit8 v12, v12, -0x1

    if-ne v9, v12, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x12e

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    goto/16 :goto_1

    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$900(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "Syslog_taglog"

    const-string v13, "time out"

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const-string v13, "Cancel"

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1002(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-virtual {v12}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x12f

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    goto/16 :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1100(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x193

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    goto/16 :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1100(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x194

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    goto/16 :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x12e

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    goto/16 :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1100(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const-string v13, "Successful"

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1002(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const v13, 0x7f080421

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-virtual {v12}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1100(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    const/16 v13, 0x131

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V

    goto/16 :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-static {v12}, Lcom/mediatek/engineermode/syslogger/TagLogService;->access$1200(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0xcb -> :sswitch_0
        0xcd -> :sswitch_5
        0xce -> :sswitch_6
        0xcf -> :sswitch_7
        0xd1 -> :sswitch_1
        0x132 -> :sswitch_8
        0x192 -> :sswitch_2
        0x193 -> :sswitch_3
        0x194 -> :sswitch_4
    .end sparse-switch
.end method
