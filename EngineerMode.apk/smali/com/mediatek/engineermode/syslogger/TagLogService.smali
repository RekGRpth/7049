.class public Lcom/mediatek/engineermode/syslogger/TagLogService;
.super Landroid/app/Service;
.source "TagLogService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;,
        Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;,
        Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;
    }
.end annotation


# static fields
.field private static final EXT_MODEM_INDEX:I = 0x3

.field private static final INPUT_TIMEOUT:I = 0x1d4c0

.field private static final LOGPATHKEY:[Ljava/lang/String;

.field private static final LOGTOOLFOLDER:[Ljava/lang/String;

.field private static final MOBILE_INDEX:I = 0x1

.field private static final MODEM_INDEX:I = 0x0

.field private static final NET_INDEX:I = 0x2

.field private static final PAST_TIME:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "Syslog_taglog"

.field private static final TIMEAWAY:J = 0x927c0L

.field private static final WAIT_MODEM_DUMP:I = 0x2710

.field private static final WAIT_MODEM_DUMP_MAX:I = 0x927c0

.field private static final WAIT_MODEM_INTENT:I = 0x3e8


# instance fields
.field private mDataFromAee:Landroid/os/Bundle;

.field private mDbPathFromAee:Ljava/lang/String;

.field private mDialog:Landroid/app/AlertDialog;

.field private mFunctionHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

.field private mInternalReceicer:Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;

.field private mIsFirstReceive:Z

.field private mIsInputDialogClicked:Z

.field private mIsModemExp:Z

.field private mIsModemLogReady:Z

.field private mIsNormalExcep:Z

.field private mIsStartService:Z

.field private mLogPathInTagLog:[Ljava/lang/String;

.field private mLogToolStatus:[Z

.field private mManualSaveLog:Z

.field private mModemLogPath:Ljava/lang/String;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mTag:Ljava/lang/String;

.field private mTagLogResult:Ljava/lang/String;

.field private mUIHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mtklog/mdlog"

    aput-object v1, v0, v2

    const-string v1, "mtklog/mobilelog"

    aput-object v1, v0, v3

    const-string v1, "mtklog/netlog"

    aput-object v1, v0, v4

    const-string v1, "mtklog/extmdlog"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/engineermode/syslogger/TagLogService;->LOGTOOLFOLDER:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "ModemLogPath"

    aput-object v1, v0, v2

    const-string v1, "MobileLogPath"

    aput-object v1, v0, v3

    const-string v1, "NetLogPath"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/engineermode/syslogger/TagLogService;->LOGPATHKEY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTag:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mManualSaveLog:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsFirstReceive:Z

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTagLogResult:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/syslogger/TagLogService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/syslogger/TagLogService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->createProgressDialog()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getStoragePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getLogPath()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->startOrStopAllLogTool(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getCurrentLogFolderFromPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTag:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/mediatek/engineermode/syslogger/TagLogService;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/mediatek/engineermode/syslogger/TagLogService;[Ljava/io/File;Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # [Ljava/io/File;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->zipAllLogAndDelete([Ljava/io/File;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mManualSaveLog:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsModemLogReady:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsModemLogReady:Z

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mManualSaveLog:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mModemLogPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mModemLogPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->zipLogAndDelete(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getMdLogFromSDcard(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mFunctionHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/engineermode/syslogger/TagLogService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->startAllLogTool()V

    return-void
.end method

.method static synthetic access$2600(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsFirstReceive:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsFirstReceive:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/syslogger/TagLogService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDbPathFromAee:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/syslogger/TagLogService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDbPathFromAee:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsModemExp:Z

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsModemExp:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/engineermode/syslogger/TagLogService;[Z)[Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # [Z

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/syslogger/TagLogService;)[Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getLogToolStatus()[Z

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/syslogger/TagLogService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->createDialog(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/syslogger/TagLogService;)Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mUIHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/syslogger/TagLogService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsInputDialogClicked:Z

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/engineermode/syslogger/TagLogService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/syslogger/TagLogService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsInputDialogClicked:Z

    return p1
.end method

.method private checkMDLogDumpDone(Ljava/io/File;)V
    .locals 16
    .param p1    # Ljava/io/File;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    if-nez v11, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v9, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :goto_1
    move-object v0, v11

    array-length v8, v0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v8, :cond_1

    aget-object v2, v0, v5

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Memory"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    move-object v9, v2

    :cond_1
    if-nez v9, :cond_3

    const-string v12, "Syslog_taglog"

    const-string v13, "memoryLogFile don\'t exit!"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v12, ".bin.bin"

    invoke-virtual {v10, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "Syslog_taglog"

    const-string v13, "memoryLogFile end with .bin.bin"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v12, v6, v3

    const-wide/32 v14, 0x927c0

    cmp-long v12, v12, v14

    if-ltz v12, :cond_4

    const-string v12, "Syslog_taglog"

    const-string v13, "Modem log dump time more than 10 min, please check it is normal! "

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-wide/16 v12, 0x2710

    :try_start_0
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v12, "Syslog_taglog"

    const-string v13, "Catch InterruptedException"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    const-string v12, ".bin"

    invoke-virtual {v10, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    const-string v12, "Syslog_taglog"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Modem Log Dump done: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    const-string v12, "Syslog_taglog"

    const-string v13, "memoryLogFile don\'t end with .bin!"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private clearRedScreen()V
    .locals 4

    const-string v2, "Syslog_taglog"

    const-string v3, "--> Clear Red Screen"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    const-string v2, "aee -c dal"

    invoke-virtual {v1, v2}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Syslog_taglog"

    const-string v3, "--> Clear Red Screen catch IOException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "Syslog_taglog"

    const-string v3, "--> Clear Red Screen catch SecurityException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private createDialog(I)V
    .locals 8
    .param p1    # I

    const v4, 0x7f080417

    const/high16 v7, 0x1040000

    const/4 v6, 0x0

    const v3, 0x7f080418

    const v5, 0x104000a

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3, v6}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    return-void

    :sswitch_0
    iput-boolean v6, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsInputDialogClicked:Z

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/mediatek/engineermode/syslogger/TagLogService$1;

    invoke-direct {v3, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$1;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f080419

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$3;

    invoke-direct {v4, p0, v1}, Lcom/mediatek/engineermode/syslogger/TagLogService$3;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;Landroid/widget/EditText;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$2;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$2;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v7, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :sswitch_1
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f08041a

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$5;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$5;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$4;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$4;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v7, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f08041b

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$6;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$6;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f08041c

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$7;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$7;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f08041d

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$8;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$8;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f08041e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/engineermode/syslogger/TagLogService$9;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$9;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x12e -> :sswitch_1
        0x12f -> :sswitch_2
        0x131 -> :sswitch_3
        0x193 -> :sswitch_4
        0x194 -> :sswitch_5
    .end sparse-switch
.end method

.method private createProgressDialog()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f080417

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08041f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Syslog_taglog"

    const-string v2, "new mProgressDialog failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private deInit()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mInternalReceicer:Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsStartService:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDialog:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mFunctionHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mFunctionHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mFunctionHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mUIHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mUIHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    :cond_3
    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->sendIntentToLog2Server()V

    iput-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    return-void
.end method

.method private dismissProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private getConfigStoragePath()Ljava/lang/String;
    .locals 8

    const/4 v4, 0x0

    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    const-string v5, "/system/etc/mtklog-config.prop"

    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v5, "persist.radio.log2sd.path"

    invoke-virtual {v3, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    :goto_0
    return-object v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move-object v1, v2

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_1
    const/4 v4, 0x0

    :try_start_3
    const-string v5, "Syslog_taglog"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File Not Found Exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    :goto_2
    const/4 v4, 0x0

    :try_start_5
    const-string v5, "Syslog_taglog"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v5

    :goto_3
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :goto_4
    throw v5

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    :catchall_1
    move-exception v5

    move-object v1, v2

    goto :goto_3

    :catch_6
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_7
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private getCurrentLogFolder([Ljava/io/File;)Ljava/io/File;
    .locals 18
    .param p1    # [Ljava/io/File;

    const-string v14, "Syslog_taglog"

    const-string v15, "-->getCurrentLogFolder()"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/32 v12, 0x927c0

    const/4 v7, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Current time="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v8, v9}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v1, p1

    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v14

    if-eqz v14, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getFolderLastModifyTime(Ljava/io/File;)J

    move-result-wide v5

    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Loop log folder:  name="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", modified time="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sub-long v14, v8, v5

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v14

    cmp-long v14, v14, v12

    if-gez v14, :cond_0

    sub-long v14, v8, v5

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    move-object v7, v2

    goto :goto_1

    :cond_2
    if-eqz v7, :cond_3

    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Selected log folder name=["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const-string v14, "Syslog_taglog"

    const-string v15, "<--getCurrentLogFolder()"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v7

    :cond_3
    const-string v14, "Syslog_taglog"

    const-string v15, "Could not get needed log folder."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v10, 0x0

    move-object/from16 v1, p1

    array-length v4, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v4, :cond_5

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    cmp-long v14, v5, v10

    if-lez v14, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-eqz v14, :cond_4

    move-wide v10, v5

    move-object v7, v2

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    if-eqz v7, :cond_6

    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Selected log folder name=["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "], last modified time="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    const-string v14, "Syslog_taglog"

    const-string v15, "There is no folder"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private getCurrentLogFolderFromPath(Ljava/lang/String;)Ljava/io/File;
    .locals 21
    .param p1    # Ljava/lang/String;

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "-->get currentLog folder in "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_0
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_1

    const-string v17, "Syslog_taglog"

    const-string v18, "getCurrentLogFolder() the folder isn\'t exist!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    if-nez v7, :cond_2

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "No promission to access "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    array-length v0, v7

    move/from16 v17, v0

    if-gtz v17, :cond_3

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "There is no folder in "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0

    :cond_3
    const/4 v10, 0x0

    const-wide/32 v15, 0x927c0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Current time="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {v11, v12}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v7

    array-length v5, v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_6

    aget-object v3, v2, v4

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v17

    if-eqz v17, :cond_5

    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getFolderLastModifyTime(Ljava/io/File;)J

    move-result-wide v8

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Loop log folder:  name="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", modified time="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {v8, v9}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sub-long v17, v11, v8

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->abs(J)J

    move-result-wide v17

    cmp-long v17, v17, v15

    if-gez v17, :cond_4

    sub-long v17, v11, v8

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->abs(J)J

    move-result-wide v15

    move-object v10, v3

    goto :goto_2

    :cond_6
    if-eqz v10, :cond_7

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Selected log folder name=["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    const-string v17, "Syslog_taglog"

    const-string v18, "<--getCurrentLogFolder()"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    const-string v17, "Syslog_taglog"

    const-string v18, "Could not get needed log folder."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v13, 0x0

    move-object v2, v7

    array-length v5, v2

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v5, :cond_9

    aget-object v3, v2, v4

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v17, v8, v13

    if-lez v17, :cond_8

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_8

    move-wide v13, v8

    move-object v10, v3

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_9
    if-eqz v10, :cond_a

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Selected log folder name=["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "], last modified time="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_a
    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "There is no folder in "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private getFolderLastModifyTime(Ljava/io/File;)J
    .locals 18
    .param p1    # Ljava/io/File;

    const-string v15, "Syslog_taglog"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "-->getFolderLastModifyTime(), path="

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    if-nez p1, :cond_1

    const-string v14, "NUll"

    :goto_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v15, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v7, 0x0

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_2

    :cond_0
    const-string v14, "Syslog_taglog"

    const-string v15, "Given file not exist."

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v9, v7

    :goto_1
    return-wide v9

    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v14

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    move-result v14

    if-eqz v14, :cond_4

    const-string v14, "Syslog_taglog"

    const-string v15, "You should give me a folder. But still can work here."

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " modified at "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v12, v13}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sub-long v14, v2, v12

    const-wide/16 v16, -0x2710

    cmp-long v14, v14, v16

    if-lez v14, :cond_3

    move-wide v7, v12

    :cond_3
    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "<--getFolderLastModifyTime(), time="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v7, v8}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v9, v7

    goto :goto_1

    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    if-nez v4, :cond_5

    const-string v14, "Syslog_taglog"

    const-string v15, "No promission to access "

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v9, v7

    goto :goto_1

    :cond_5
    move-object v1, v4

    array-length v6, v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v6, :cond_3

    aget-object v11, v1, v5

    const-wide/16 v12, 0x0

    invoke-virtual {v11}, Ljava/io/File;->isFile()Z

    move-result v14

    if-eqz v14, :cond_8

    const-string v14, "Syslog_taglog"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " modified at "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    sub-long v14, v2, v12

    const-wide/16 v16, -0x2710

    cmp-long v14, v14, v16

    if-gez v14, :cond_6

    const-wide/16 v12, 0x0

    :cond_6
    :goto_3
    sub-long v14, v12, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v14

    sub-long v16, v7, v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_7

    move-wide v7, v12

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getFolderLastModifyTime(Ljava/io/File;)J

    move-result-wide v12

    goto :goto_3
.end method

.method private getLogPath()[Ljava/lang/String;
    .locals 6

    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getStoragePath()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    const-string v3, "Syslog_taglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSDCardPath :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/engineermode/syslogger/TagLogService;->LOGTOOLFOLDER:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getLogToolStatus()[Z
    .locals 5

    const/4 v4, 0x1

    const-string v2, "Syslog_taglog"

    const-string v3, "Check log tool status"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x3

    new-array v0, v2, [Z

    const-string v2, "debug.mdlogger.Running"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsModemExp:Z

    if-nez v2, :cond_0

    const-string v2, "Syslog_taglog"

    const-string v3, "getLogToolStatus: ModemLog is running"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    aput-boolean v4, v0, v2

    :cond_0
    const-string v2, "debug.MB.running"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Syslog_taglog"

    const-string v3, "getLogToolStatus: MobileLog is running"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aput-boolean v4, v0, v4

    :cond_1
    const-string v2, "persist.radio.netlog.Running"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Syslog_taglog"

    const-string v3, "getLogToolStatus: NetLog is running"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    aput-boolean v4, v0, v2

    :cond_2
    return-object v0
.end method

.method private getMdLogFromSDcard(Ljava/lang/String;)Ljava/io/File;
    .locals 21
    .param p1    # Ljava/lang/String;

    const-string v17, "Syslog_taglog"

    const-string v18, "-->getMdLogFromSDcard()"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_0

    const-string v17, "Syslog_taglog"

    const-string v18, "getMdLogFromSDcard() the folder isn\'t exist!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_0
    const-wide/32 v15, 0x927c0

    const/4 v10, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Current time="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {v11, v12}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "No promission to access "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0

    :cond_1
    move-object v2, v6

    array-length v5, v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v3, v2, v4

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v17

    if-eqz v17, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "EE"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getFolderLastModifyTime(Ljava/io/File;)J

    move-result-wide v8

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Loop log folder:  name="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", modified time="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {v8, v9}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sub-long v17, v11, v8

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->abs(J)J

    move-result-wide v17

    cmp-long v17, v17, v15

    if-gez v17, :cond_2

    sub-long v17, v11, v8

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->abs(J)J

    move-result-wide v15

    move-object v10, v3

    goto :goto_2

    :cond_4
    if-eqz v10, :cond_5

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Selected modem log folder name=["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/mediatek/engineermode/syslogger/TagLogService;->checkMDLogDumpDone(Ljava/io/File;)V

    :goto_3
    const-string v17, "Syslog_taglog"

    const-string v18, "<--getMdLogFromSDcard()"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-string v17, "Syslog_taglog"

    const-string v18, "Could not get needed log folder."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v13, 0x0

    move-object v2, v6

    array-length v5, v2

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v5, :cond_7

    aget-object v3, v2, v4

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v17, v8, v13

    if-lez v17, :cond_6

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_6

    move-wide v13, v8

    move-object v10, v3

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_7
    if-eqz v10, :cond_8

    const-string v17, "Syslog_taglog"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Selected log folder name=["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "], last modified time="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Lcom/mediatek/engineermode/syslogger/ZipManager;->translateTime(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_8
    const-string v17, "Syslog_taglog"

    const-string v18, "There is no folder"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private getStoragePath()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const-string v2, "persist.radio.log2sd.path"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/mnt/sdcard2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->getExternalStoragePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->getInternalStoragePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get storage path is null from external path: may be emmc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get storage path from external path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v2, "/mnt/sdcard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->getInternalStoragePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->getExternalStoragePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get storage path is null from internal path: may be not emmc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get storage path from internal path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v2, "/data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "Syslog_taglog"

    const-string v3, "The storage path is /data."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "/data"

    goto :goto_0

    :cond_5
    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getStoragePath --> other :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getConfigStoragePath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get storage path from config file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->getInternalStoragePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->getExternalStoragePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get storage path is null from internal path: may be not emmc "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private init()V
    .locals 4

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.mdlogger.MEMORYDUMP_DONE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mInternalReceicer:Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mInternalReceicer:Lcom/mediatek/engineermode/syslogger/TagLogService$InternalReceiver;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsStartService:Z

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    new-instance v2, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;Lcom/mediatek/engineermode/syslogger/TagLogService$1;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mUIHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "taglog_function"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v2, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;-><init>(Lcom/mediatek/engineermode/syslogger/TagLogService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mFunctionHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$FunctionHandler;

    return-void
.end method

.method private initExceptionInfo()Lcom/mediatek/engineermode/syslogger/ExceptionInfo;
    .locals 11

    const/4 v7, 0x0

    new-instance v3, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;

    invoke-direct {v3}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;-><init>()V

    iget-object v8, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    const-string v9, "path"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    const-string v9, "db_filename"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    const-string v9, "zz_filename"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    if-nez v6, :cond_1

    :cond_0
    const-string v8, "Syslog_taglog"

    const-string v9, "params are not valid!"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v7

    :goto_0
    return-object v3

    :cond_1
    iput-object v4, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDbPathFromAee:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "Syslog_taglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "expPath: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "Syslog_taglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "exp_file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3, v1}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->setmPath(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v3, v5}, Lcom/mediatek/engineermode/syslogger/ExceptionInfo;->initFieldsFromZZ(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v8, "Syslog_taglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fail to init exception info:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v7

    goto :goto_0
.end method

.method private playTipSound(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;

    const-string v1, "Syslog_taglog"

    const-string v2, "play sound tip."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-static {p1, p2}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/Ringtone;->play()V

    :cond_0
    return-void
.end method

.method private sendIntentToLog2Server()V
    .locals 5

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.mediatek.syslogger.taglog"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTagLogResult:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "TaglogResult"

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTagLogResult:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TaglogResult = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mTagLogResult:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    sget-object v2, Lcom/mediatek/engineermode/syslogger/TagLogService;->LOGPATHKEY:[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/mediatek/engineermode/syslogger/TagLogService;->LOGPATHKEY:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogPathInTagLog:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const-string v2, "Syslog_taglog"

    const-string v3, "mTagLogResult is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v2, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mLogPathInTagLog["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "= null!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    const-string v2, "Syslog_taglog"

    const-string v3, "mLogPathInTagLog is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :goto_3
    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "Syslog_taglog"

    const-string v3, "send intent to Log2Server"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    const-string v2, "Syslog_taglog"

    const-string v3, "Data From Aee is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private startAllLogTool()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->startOrStopLogTool(ZI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startOrStopAllLogTool(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->startOrStopLogTool(ZI)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private startOrStopLogTool(ZI)V
    .locals 3
    .param p1    # Z
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.syslogger.action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "Satan"

    const-string v2, "Angel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "From"

    const-string v2, "CommonUI"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "Command"

    const-string v2, "Start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    packed-switch p2, :pswitch_data_0

    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v1, "Command"

    const-string v2, "Stop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_0
    const-string v1, "To"

    const-string v2, "ModemLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_1
    const-string v1, "To"

    const-string v2, "MobileLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_2
    const-string v1, "To"

    const-string v2, "ActivityNetworkLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_3
    const-string v1, "To"

    const-string v2, "ExtModemLog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private zipAllLogAndDelete([Ljava/io/File;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1    # [Ljava/io/File;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, 0x3

    new-array v1, v6, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mLogToolStatus:[Z

    aget-boolean v3, v3, v2

    if-eqz v3, :cond_1

    aget-object v3, p1, v2

    if-eqz v3, :cond_1

    aget-object v3, p1, v2

    invoke-direct {p0, v3, p2}, Lcom/mediatek/engineermode/syslogger/TagLogService;->zipLogAndDelete(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    aput-object v7, v1, v2

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    aput-object v0, v1, v2

    const-string v3, "Syslog_taglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "currentTaglogPaths["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    aput-object v7, v1, v2

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private zipLogAndDelete(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;

    const-string v3, "Syslog_taglog"

    const-string v4, "-->zipLog()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Syslog_taglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "targetLogFileName :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/mediatek/engineermode/syslogger/ZipManager;->zipFileOrFolder(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v3, "Syslog_taglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail to zip log folder: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    const-string v3, "Syslog_taglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Zip log success, target log file="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v0

    invoke-static {p1}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->deleteFolder(Ljava/io/File;)V

    :goto_1
    const-string v3, "Syslog_taglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<--zipLog(), zipResultPath="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    goto :goto_0

    :cond_1
    const-string v3, "Syslog_taglog"

    const-string v4, "Needed log folder path is null!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private zipLogAndDelete(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Syslog_taglog"

    const-string v8, "-->zipLog()"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    if-nez p1, :cond_1

    const-string v7, "Syslog_taglog"

    const-string v8, "LogFolderPath is null!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v7, "Syslog_taglog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No promission to access "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    array-length v7, v1

    if-nez v7, :cond_3

    const-string v7, "Syslog_taglog"

    const-string v8, "Found no detail log in log folder"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/syslogger/TagLogService;->getCurrentLogFolder([Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".zip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "Syslog_taglog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "targetLogFileName :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/mediatek/engineermode/syslogger/ZipManager;->zipFileOrFolder(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v7, "Syslog_taglog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Fail to zip log folder: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v6, "Syslog_taglog"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Zip log success, target log file="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v3

    invoke-static {v2}, Lcom/mediatek/engineermode/syslogger/SDCardUtils;->deleteFolder(Ljava/io/File;)V

    :goto_1
    const-string v6, "Syslog_taglog"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<--zipLog(), zipResultPath="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    goto/16 :goto_0

    :cond_5
    const-string v6, "Syslog_taglog"

    const-string v7, "Fail to get needed log folder"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "Syslog_taglog"

    const-string v1, "TagLogService-->onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "Syslog_taglog"

    const-string v1, "TagLogService-->onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->init()V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "Syslog_taglog"

    const-string v1, "TagLogService-->onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/syslogger/TagLogService;->deInit()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "Syslog_taglog"

    const-string v1, "TagLogService-->onStartCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "Syslog_taglog"

    const-string v1, "Intent is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsStartService:Z

    if-eqz v0, :cond_1

    const-string v0, "Syslog_taglog"

    const-string v1, "The service is runing!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f080420

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mIsStartService:Z

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mDataFromAee:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/mediatek/engineermode/syslogger/TagLogService;->mUIHandler:Lcom/mediatek/engineermode/syslogger/TagLogService$UIHandler;

    const/16 v1, 0xcb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "Syslog_taglog"

    const-string v1, "TagLogService-->onStartCommand End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    goto :goto_0
.end method
