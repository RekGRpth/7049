.class Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;
.super Ljava/lang/Object;
.source "TagLogSwitch.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/engineermode/syslogger/TagLogSwitch;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/syslogger/TagLogSwitch;

.field final synthetic val$chk:Landroid/preference/CheckBoxPreference;

.field final synthetic val$edit:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/syslogger/TagLogSwitch;Landroid/preference/CheckBoxPreference;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;->this$0:Lcom/mediatek/engineermode/syslogger/TagLogSwitch;

    iput-object p2, p0, Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;->val$chk:Landroid/preference/CheckBoxPreference;

    iput-object p3, p0, Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;->val$edit:Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5
    .param p1    # Landroid/preference/Preference;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;->val$chk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    const-string v1, "Syslog_taglog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tag Log Switch--check box is checked:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;->val$edit:Landroid/content/SharedPreferences$Editor;

    const-string v4, "taglog_switch_key"

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/mediatek/engineermode/syslogger/TagLogSwitch$1;->val$edit:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return v2

    :cond_0
    move v1, v2

    goto :goto_0
.end method
