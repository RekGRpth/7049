.class public Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;
.super Ljava/lang/Object;
.source "TsPointDataStruct.java"


# instance fields
.field public mAction:I

.field public mCoordinateX:I

.field public mCoordinateY:I

.field public mFatSize:F

.field public mPid:I

.field public mPressure:F

.field public mTimeStamp:J

.field public mVelocityX:F

.field public mVelocityY:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mAction:I

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mCoordinateX:I

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mCoordinateY:I

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mPressure:F

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mVelocityX:F

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mVelocityY:F

    const v0, 0x3c23d70a

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mFatSize:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mTimeStamp:J

    return-void
.end method


# virtual methods
.method public getmAction()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mAction:I

    return v0
.end method

.method public getmCoordinateX()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mCoordinateX:I

    return v0
.end method

.method public getmCoordinateY()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mCoordinateY:I

    return v0
.end method

.method public getmFatSize()F
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mFatSize:F

    return v0
.end method

.method public getmPid()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mPid:I

    return v0
.end method

.method public getmPressure()F
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mPressure:F

    return v0
.end method

.method public getmTimeStamp()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mTimeStamp:J

    return-wide v0
.end method

.method public getmVelocityX()F
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mVelocityX:F

    return v0
.end method

.method public getmVelocityY()F
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mVelocityY:F

    return v0
.end method

.method public setTimeStamp()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mTimeStamp:J

    return-void
.end method

.method public setmAction(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mAction:I

    return-void
.end method

.method public setmCoordinateX(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mCoordinateX:I

    return-void
.end method

.method public setmCoordinateY(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mCoordinateY:I

    return-void
.end method

.method public setmFatSize(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mFatSize:F

    return-void
.end method

.method public setmPid(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mPid:I

    return-void
.end method

.method public setmPressure(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mPressure:F

    return-void
.end method

.method public setmTimeStamp(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mTimeStamp:J

    return-void
.end method

.method public setmVelocityX(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mVelocityX:F

    return-void
.end method

.method public setmVelocityY(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->mVelocityY:F

    return-void
.end method
