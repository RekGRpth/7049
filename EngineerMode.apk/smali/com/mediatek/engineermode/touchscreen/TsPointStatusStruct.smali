.class public Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;
.super Ljava/lang/Object;
.source "TsPointStatusStruct.java"


# instance fields
.field public mDown:Z

.field public mNewLine:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->mDown:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->mNewLine:Z

    return-void
.end method


# virtual methods
.method public ismDown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->mDown:Z

    return v0
.end method

.method public ismNewLine()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->mNewLine:Z

    return v0
.end method

.method public setmDown(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->mDown:Z

    return-void
.end method

.method public setmNewLine(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->mNewLine:Z

    return-void
.end method
