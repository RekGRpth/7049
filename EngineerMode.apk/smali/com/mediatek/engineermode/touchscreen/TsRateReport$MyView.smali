.class public Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;
.super Landroid/view/View;
.source "TsRateReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/touchscreen/TsRateReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;
    }
.end annotation


# instance fields
.field private mMinPtrId:I

.field private mPointerNumDetected:I

.field private mPtsStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/engineermode/touchscreen/TsRateReport;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/touchscreen/TsRateReport;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsRateReport;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPointerNumDetected:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    return-void
.end method

.method private calcMinId(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    if-ge v0, p1, :cond_1

    iget p1, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    :cond_1
    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method getPaint(II)Landroid/graphics/Paint;
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const/16 v5, 0xff

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v1, 0x7

    if-ge p1, v1, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->access$200()[[I

    move-result-object v1

    aget-object v1, v1, p1

    aget v1, v1, v2

    invoke-static {}, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->access$200()[[I

    move-result-object v2

    aget-object v2, v2, p1

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {}, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->access$200()[[I

    move-result-object v3

    aget-object v3, v3, p1

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    :goto_0
    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-object v0

    :cond_0
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    const-string v7, "EM/TouchScreen/RR"

    const-string v8, "-->onDraw"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v4, 0xf

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Pointer number detected: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPointerNumDetected:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/high16 v8, 0x40400000

    const/16 v9, 0x19

    int-to-float v9, v9

    const/4 v10, 0x4

    invoke-virtual {p0, v10, v4}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->getPaint(II)Landroid/graphics/Paint;

    move-result-object v10

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->setUTimeStamp()V

    invoke-virtual {v1}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->calculateRate()V

    const-string v7, "pid=%2d, X=%3d, Y=%3d."

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mPid:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget v10, v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mLastX:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget v10, v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mLastY:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "Rate=%dHz, Count=%d, Time=%dms"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mRate:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget v10, v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mCnt:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget v10, v1, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mMills:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x3

    mul-int/lit8 v7, v0, 0x3

    mul-int/2addr v7, v4

    add-int/lit8 v6, v7, 0x37

    int-to-float v7, v5

    int-to-float v8, v6

    invoke-virtual {p0, v0, v4}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->getPaint(II)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {p1, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    int-to-float v7, v5

    add-int v8, v6, v4

    int-to-float v8, v8

    invoke-virtual {p0, v0, v4}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->getPaint(II)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {p1, v3, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    const-string v8, "EM/TouchScreen/RR"

    const-string v9, "-->onTouchEvent"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    shr-int/lit8 v7, v0, 0x8

    const/4 v8, 0x5

    if-eq v1, v8, :cond_0

    if-nez v1, :cond_5

    :cond_0
    const/4 v6, 0x0

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v7, v8, :cond_4

    new-instance v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;

    invoke-direct {v6, p0, v12}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;-><init>(Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;Lcom/mediatek/engineermode/touchscreen/TsRateReport$1;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v6}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->clean()V

    iput v7, v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mPid:I

    invoke-virtual {v6}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->setDTimeStamp()V

    invoke-static {v6, v11}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->access$102(Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;Z)Z

    :cond_1
    :goto_1
    if-ne v1, v11, :cond_2

    iput v10, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPointerNumDetected:I

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-ne v1, v11, :cond_8

    iput v10, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPointerNumDetected:I

    :goto_2
    const-string v8, "EM/TouchScreen/RR"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Pointer counts = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mPtsStatus.size()= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v5, :cond_9

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_3

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->calcMinId(I)V

    iget v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mMinPtrId:I

    sub-int v4, v3, v8

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;

    iget v8, v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mCnt:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mCnt:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mLastX:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->mLastY:I

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;

    goto :goto_0

    :cond_5
    const/4 v8, 0x6

    if-eq v1, v8, :cond_6

    if-ne v1, v11, :cond_1

    :cond_6
    const/4 v6, 0x0

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v7, v8, :cond_7

    new-instance v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;

    invoke-direct {v6, p0, v12}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;-><init>(Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;Lcom/mediatek/engineermode/touchscreen/TsRateReport$1;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-virtual {v6}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->setUTimeStamp()V

    invoke-static {v6, v10}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;->access$102(Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;Z)Z

    goto/16 :goto_1

    :cond_7
    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView$PointerData;

    goto :goto_4

    :cond_8
    iput v5, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;->mPointerNumDetected:I

    goto/16 :goto_2

    :cond_9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return v11
.end method
