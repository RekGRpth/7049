.class public Lcom/mediatek/engineermode/touchscreen/TsRateReport;
.super Landroid/app/Activity;
.source "TsRateReport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/touchscreen/TsRateReport$1;,
        Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;
    }
.end annotation


# static fields
.field private static final RGB:[[I

.field private static final TAG:Ljava/lang/String; = "EM/TouchScreen/RR"


# instance fields
.field mMetrick:Landroid/util/DisplayMetrics;

.field mView:Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x7

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v3

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->RGB:[[I

    return-void

    :array_0
    .array-data 4
        0xff
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0xff
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x0
        0xff
    .end array-data

    :array_3
    .array-data 4
        0xff
        0xff
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x0
        0xff
        0xff
    .end array-data

    :array_5
    .array-data 4
        0xff
        0x0
        0xff
    .end array-data

    :array_6
    .array-data 4
        0xff
        0xff
        0xff
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->mView:Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->mMetrick:Landroid/util/DisplayMetrics;

    return-void
.end method

.method static synthetic access$200()[[I
    .locals 1

    sget-object v0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->RGB:[[I

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/16 v1, 0x400

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    new-instance v0, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;

    invoke-direct {v0, p0, p0}, Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;-><init>(Lcom/mediatek/engineermode/touchscreen/TsRateReport;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->mView:Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->mView:Lcom/mediatek/engineermode/touchscreen/TsRateReport$MyView;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    const-string v0, "EM/TouchScreen/RR"

    const-string v1, "-->onPause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "EM/TouchScreen/RR"

    const-string v1, "-->onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsRateReport;->mMetrick:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    return-void
.end method
