.class public Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;
.super Landroid/app/Activity;
.source "TsVerifyLine.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;
    }
.end annotation


# static fields
.field public static final CALCULATE_ID:I = 0x1

.field public static final NEXTLINE_ID:I = 0x2

.field private static final TAG:Ljava/lang/String; = "EM/TouchScreen/VL"


# instance fields
.field public mDiversity:D

.field public mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

.field public mInput:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field public mLineIndex:I

.field public mPts1:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private mRectHeight:I

.field private mRectWidth:I

.field public mRun:Z

.field private mZoom:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRun:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversity:D

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mPts1:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mZoom:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mZoom:I

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    return v0
.end method


# virtual methods
.method public calculateDiversity()V
    .locals 10

    const/4 v5, 0x0

    const/high16 v9, 0x3f800000

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v5, v5}, Landroid/graphics/Point;-><init>(II)V

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/16 v1, 0x0

    iget v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    int-to-float v5, v5

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    int-to-float v6, v6

    div-float v4, v5, v6

    iget v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    add-int/lit8 v5, v5, -0x1

    packed-switch v5, :pswitch_data_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    int-to-double v5, v5

    div-double v5, v1, v5

    iput-wide v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversity:D

    goto :goto_0

    :pswitch_0
    const/4 v3, 0x0

    :goto_1
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-double v5, v5

    add-double/2addr v1, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_1
    const/4 v3, 0x0

    :goto_2
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget v5, v0, Landroid/graphics/Point;->y:I

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-double v5, v5

    add-double/2addr v1, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :pswitch_2
    const/4 v3, 0x0

    :goto_3
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    mul-float/2addr v5, v4

    iget v6, v0, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v5, v5

    mul-float v7, v4, v4

    add-float/2addr v7, v9

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    div-double/2addr v5, v7

    add-double/2addr v1, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :pswitch_3
    const/4 v3, 0x0

    :goto_4
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v5, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    mul-float/2addr v5, v4

    iget v6, v0, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v5, v5

    mul-float v7, v4, v4

    add-float/2addr v7, v9

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    div-double/2addr v5, v7

    add-double/2addr v1, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0x320

    const/16 v2, 0x1e0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    iget v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    if-ne v2, v1, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    if-eq v3, v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    if-ne v3, v1, :cond_2

    iget v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    if-ne v2, v1, :cond_2

    :cond_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mZoom:I

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->readPoints(I)Ljava/util/Vector;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mPts1:Ljava/util/Vector;

    iget v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    new-instance v1, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

    invoke-direct {v1, p0, p0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;-><init>(Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const-string v1, "EM/TouchScreen/VL"

    const-string v2, "Oncreate"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "Calculate"

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x2

    const-string v1, "NextLine"

    invoke-interface {p1, v2, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    return v3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->calculateDiversity()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->readPoints(I)Ljava/util/Vector;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mPts1:Ljava/util/Vector;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    const/4 v0, 0x4

    iget v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversity:D

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    const-string v0, "EM/TouchScreen/VL"

    const-string v1, "-->onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyLine$DiversityCanvas;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mInput:Ljava/util/Vector;

    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public readPoints(I)Ljava/util/Vector;
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    int-to-float v6, v6

    iget v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    int-to-float v7, v7

    div-float v2, v6, v7

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mLineIndex:I

    packed-switch v6, :pswitch_data_0

    :cond_0
    return-object v3

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    if-ge v0, v6, :cond_0

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    div-int/lit8 v4, v6, 0x2

    move v5, v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v3, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    :goto_1
    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    if-ge v0, v6, :cond_0

    move v4, v0

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectHeight:I

    div-int/lit8 v5, v6, 0x2

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v3, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x0

    :goto_2
    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    if-ge v0, v6, :cond_0

    move v4, v0

    int-to-float v6, v0

    mul-float/2addr v6, v2

    float-to-int v5, v6

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v3, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_3
    const/4 v0, 0x0

    :goto_3
    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    if-ge v0, v6, :cond_0

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyLine;->mRectWidth:I

    sub-int v4, v6, v0

    int-to-float v6, v0

    mul-float/2addr v6, v2

    float-to-int v5, v6

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v3, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
