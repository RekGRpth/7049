.class public Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;
.super Landroid/app/Activity;
.source "TsVerifyPoint.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EM/TouchScreen"


# instance fields
.field public mBitmap:Landroid/graphics/Bitmap;

.field public mBitmapPad:I

.field public mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;

.field public mPointError:D

.field public mPrePoint:Landroid/graphics/Point;

.field public mRand:Ljava/util/Random;

.field private mRectHeight:I

.field private mRectWidth:I

.field public mRun:Z

.field private mZoom:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRun:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPointError:D

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mBitmapPad:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mZoom:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mZoom:I

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectWidth:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectHeight:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/16 v4, 0x320

    const/16 v3, 0x1e0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectWidth:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectHeight:I

    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectWidth:I

    if-ne v3, v2, :cond_0

    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectHeight:I

    if-eq v4, v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectWidth:I

    if-ne v4, v2, :cond_2

    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectHeight:I

    if-ne v3, v2, :cond_2

    :cond_1
    const/4 v2, 0x2

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mZoom:I

    :cond_2
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRand:Ljava/util/Random;

    new-instance v2, Landroid/graphics/Point;

    iget v3, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectWidth:I

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectHeight:I

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    new-instance v2, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;

    invoke-direct {v2, p0, p0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;-><init>(Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint$DiversityCanvas;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020004

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mBitmapPad:I

    :cond_3
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "EM/TouchScreen"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The PrePoint.x value is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "EM/TouchScreen"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The PrePoint.y value is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v3, v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v5, v6

    const-string v6, "EM/TouchScreen"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The xTouch value is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "EM/TouchScreen"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The yTouch value is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    sub-int v6, v3, v6

    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    sub-int v7, v3, v7

    mul-int v0, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    sub-int v6, v5, v6

    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    sub-int v7, v5, v7

    mul-int v1, v6, v7

    add-int v6, v0, v1

    int-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPointError:D

    iget-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRand:Ljava/util/Random;

    iget v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectWidth:I

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    iget-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRand:Ljava/util/Random;

    iget v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mRectHeight:I

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6, v2, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyPoint;->mPrePoint:Landroid/graphics/Point;

    :cond_0
    const/4 v6, 0x1

    return v6
.end method
