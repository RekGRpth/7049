.class Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;
.super Ljava/lang/Thread;
.source "TsVerifyShaking.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DiversityThread"
.end annotation


# instance fields
.field private mCrossPaint:Landroid/graphics/Paint;

.field private mRect:Landroid/graphics/Rect;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mTextPaint:Landroid/graphics/Paint;

.field final synthetic this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;Landroid/view/SurfaceHolder;Landroid/content/Context;)V
    .locals 5
    .param p2    # Landroid/view/SurfaceHolder;
    .param p3    # Landroid/content/Context;

    const/4 v0, 0x0

    const/16 v4, 0xff

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mTextPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRectPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mCrossPaint:Landroid/graphics/Paint;

    iput-object p2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mTextPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mTextPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41100000

    iget-object v2, p1, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v2}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$100(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p1, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$200(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I

    move-result v1

    iget-object v2, p1, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v2}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$300(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRectPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mCrossPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mCrossPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4, v4, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    return-void
.end method

.method private doDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$400(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$400(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v1, v1, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v2, v2, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v2}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$600(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v2, v2, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v2}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v3, v3, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v3}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$600(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Average shaking error : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v1, v1, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$700(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x41a00000

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v2, v2, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v2}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$300(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0xf

    int-to-float v1, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0xf

    int-to-float v2, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0xf

    int-to-float v3, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0xf

    int-to-float v4, v0

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mCrossPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, -0xf

    int-to-float v1, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, 0xf

    int-to-float v2, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v0, 0xf

    int-to-float v3, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v0, v0, -0xf

    int-to-float v4, v0

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mCrossPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public run()V
    .locals 3

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->this$1:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v1, v1, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-static {v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$000(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_1

    :try_start_1
    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->doDraw(Landroid/graphics/Canvas;)V

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_2
    throw v1

    :cond_3
    return-void
.end method
