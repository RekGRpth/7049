.class Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;
.super Landroid/view/SurfaceView;
.source "TsVerifyShaking.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DiversityCanvas"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;
    }
.end annotation


# instance fields
.field mThread:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;

.field final synthetic this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->mThread:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;

    invoke-virtual {p0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$002(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;Z)Z

    new-instance v0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;-><init>(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;Landroid/view/SurfaceHolder;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->mThread:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->mThread:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas$DiversityThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;->this$0:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->access$002(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;Z)Z

    return-void
.end method
