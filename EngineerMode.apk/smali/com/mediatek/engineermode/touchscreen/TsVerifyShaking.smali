.class public Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;
.super Landroid/app/Activity;
.source "TsVerifyShaking.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;
    }
.end annotation


# instance fields
.field private mAverageShakingError:D

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPad:I

.field private mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

.field private mInputPoint:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private mPrePoint:Landroid/graphics/Point;

.field private mRand:Ljava/util/Random;

.field private mRectHeight:I

.field private mRectWidth:I

.field private mRun:Z

.field private mZoom:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v3, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRun:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x81

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    iput v3, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmapPad:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mZoom:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRun:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRun:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mZoom:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectWidth:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)Landroid/graphics/Point;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmapPad:I

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;)D
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;

    iget-wide v0, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    return-wide v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/16 v4, 0x320

    const/16 v3, 0x1e0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectWidth:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectHeight:I

    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectWidth:I

    if-ne v3, v2, :cond_0

    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectHeight:I

    if-eq v4, v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectWidth:I

    if-ne v4, v2, :cond_2

    iget v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectHeight:I

    if-ne v3, v2, :cond_2

    :cond_1
    const/4 v2, 0x2

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mZoom:I

    :cond_2
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRand:Ljava/util/Random;

    new-instance v2, Landroid/graphics/Point;

    iget v3, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectWidth:I

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectHeight:I

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    new-instance v2, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    invoke-direct {v2, p0, p0}, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;-><init>(Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mDiversityCanvas:Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking$DiversityCanvas;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020004

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mBitmapPad:I

    :cond_3
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v9, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-ne v5, v6, :cond_2

    :cond_0
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    new-instance v6, Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v8, v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v5, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return v9

    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v9, v5, :cond_1

    const-wide/16 v5, 0x0

    iput-wide v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    sub-int v6, v5, v6

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    sub-int/2addr v5, v7

    mul-int v0, v6, v5

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    sub-int v6, v5, v6

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v7

    mul-int v1, v6, v5

    iget-wide v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    add-int v7, v0, v1

    int-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    add-double/2addr v5, v7

    iput-wide v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-wide v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    iget-object v7, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    int-to-double v7, v7

    div-double/2addr v5, v7

    iput-wide v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mAverageShakingError:D

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mInputPoint:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->clear()V

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRand:Ljava/util/Random;

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectWidth:I

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    iget-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRand:Ljava/util/Random;

    iget v6, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mRectHeight:I

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v5, p0, Lcom/mediatek/engineermode/touchscreen/TsVerifyShaking;->mPrePoint:Landroid/graphics/Point;

    goto/16 :goto_0
.end method
