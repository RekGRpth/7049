.class public Lcom/mediatek/engineermode/usb/UsbEx;
.super Landroid/app/Activity;
.source "UsbEx.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;
    }
.end annotation


# static fields
.field private static final DLG_ERROR_MSG:I = 0x4

.field private static final DLG_MSG:I = 0x2

.field private static final DLG_STOP:I = 0x1

.field private static final DLG_UNKNOW_MSG:I = 0x3

.field private static final ERROR_MSG:I = 0xd

.field private static final GET_DESCRIPTOR:I = 0xc

.field private static final GET_MSG:I = 0x14

.field private static final OP_FINISH:I = 0xb

.field private static final OP_IN_PROCESS:I = 0xa

.field private static final SET_FEATURE:I = 0xd

.field private static final START_TEST:I = 0x15

.field private static final SUSPEND_RESUME:I = 0xb

.field private static final TAG:Ljava/lang/String; = "USBIF"

.field private static final TEST_J:I = 0x8

.field private static final TEST_K:I = 0x9

.field private static final TEST_PACKET:I = 0xa

.field private static final TEST_SE0_NAK:I = 0x7

.field private static final UPDATAT_MSG:I = 0xc

.field private static sWorkHandler:Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;


# instance fields
.field private mBtn1Start:Landroid/widget/Button;

.field private mBtn1Stop:Landroid/widget/Button;

.field private mBtn2Start:Landroid/widget/Button;

.field private mBtn2Stop:Landroid/widget/Button;

.field private mBtn3Start:Landroid/widget/Button;

.field private mBtn3Stop:Landroid/widget/Button;

.field private mBtn4Start:Landroid/widget/Button;

.field private mBtn4Stop:Landroid/widget/Button;

.field private mBtn5Start:Landroid/widget/Button;

.field private mBtn5Stop:Landroid/widget/Button;

.field private mBtn6Start:Landroid/widget/Button;

.field private mBtn6Stop:Landroid/widget/Button;

.field private mBtn7Start:Landroid/widget/Button;

.field private mBtn7Stop:Landroid/widget/Button;

.field private mBtnList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field private mCommand:I

.field private mMsg:I

.field private mRun:Z

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/engineermode/usb/UsbEx;->sWorkHandler:Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iput v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    iput v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mMsg:I

    iput-boolean v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mRun:Z

    new-instance v0, Lcom/mediatek/engineermode/usb/UsbEx$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/usb/UsbEx$1;-><init>(Lcom/mediatek/engineermode/usb/UsbEx;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/usb/UsbEx;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbEx;

    iget v0, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mMsg:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/usb/UsbEx;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/usb/UsbEx;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mMsg:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/usb/UsbEx;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbEx;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mRun:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/usb/UsbEx;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbEx;

    iget-object v0, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/usb/UsbEx;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbEx;

    iget v0, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    return v0
.end method

.method private makeOneBtnEnable(Landroid/widget/Button;)V
    .locals 3
    .param p1    # Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-ne v0, p1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateAllBtn(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/16 v8, 0xa

    const/16 v7, 0x9

    const/16 v6, 0x8

    const/4 v5, 0x7

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_2

    iput v5, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Stop:Landroid/widget/Button;

    :cond_0
    :goto_0
    if-eqz v0, :cond_f

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbEx;->sWorkHandler:Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;

    const/16 v3, 0x15

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/usb/UsbEx;->makeOneBtnEnable(Landroid/widget/Button;)V

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbEx;->sWorkHandler:Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_3

    iput v6, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Stop:Landroid/widget/Button;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_4

    iput v7, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Stop:Landroid/widget/Button;

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_5

    iput v8, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Stop:Landroid/widget/Button;

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_6

    const/16 v2, 0xb

    iput v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Stop:Landroid/widget/Button;

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_7

    const/16 v2, 0xc

    iput v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Stop:Landroid/widget/Button;

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Start:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_8

    const/16 v2, 0xd

    iput v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Stop:Landroid/widget/Button;

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_9

    iput v5, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_a

    iput v6, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_b

    iput v7, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_c

    iput v8, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_d

    const/16 v2, 0xb

    iput v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_e

    const/16 v2, 0xc

    iput v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Stop:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/16 v2, 0xd

    iput v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_f
    iget-boolean v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mRun:Z

    if-eqz v2, :cond_1

    iput-boolean v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mRun:Z

    iget v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mCommand:I

    invoke-static {v2}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeStopTest(I)Z

    move-result v2

    if-nez v2, :cond_10

    const v2, 0x7f08032c

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeCleanMsg()Z

    :cond_10
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/usb/UsbEx;->updateAllBtn(Z)V

    goto/16 :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03005c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0b0236

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Start:Landroid/widget/Button;

    const v3, 0x7f0b0237

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Stop:Landroid/widget/Button;

    const v3, 0x7f0b0238

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Start:Landroid/widget/Button;

    const v3, 0x7f0b0239

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Stop:Landroid/widget/Button;

    const v3, 0x7f0b023a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Start:Landroid/widget/Button;

    const v3, 0x7f0b023b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Stop:Landroid/widget/Button;

    const v3, 0x7f0b023c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Start:Landroid/widget/Button;

    const v3, 0x7f0b023d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Stop:Landroid/widget/Button;

    const v3, 0x7f0b023e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Start:Landroid/widget/Button;

    const v3, 0x7f0b023f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Stop:Landroid/widget/Button;

    const v3, 0x7f0b0240

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Start:Landroid/widget/Button;

    const v3, 0x7f0b0241

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Stop:Landroid/widget/Button;

    const v3, 0x7f0b0242

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Start:Landroid/widget/Button;

    const v3, 0x7f0b0243

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Stop:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn1Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn2Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn3Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn4Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn5Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn6Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Start:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtn7Stop:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeInit()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "USBIF"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v3, Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;-><init>(Lcom/mediatek/engineermode/usb/UsbEx;Landroid/os/Looper;Lcom/mediatek/engineermode/usb/UsbEx$1;)V

    sput-object v3, Lcom/mediatek/engineermode/usb/UsbEx;->sWorkHandler:Lcom/mediatek/engineermode/usb/UsbEx$WorkHandler;

    :goto_1
    return-void

    :cond_1
    const v3, 0x7f08032b

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const v5, 0x7f08032a

    const v4, 0x7f08025d

    const/4 v3, 0x0

    const-string v1, "USBIF"

    const-string v2, "-->onCreateDialog"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v6, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080325

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f080338

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbDriver;->MSG:[Ljava/lang/String;

    iget v3, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mMsg:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbEx$2;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/usb/UsbEx$2;-><init>(Lcom/mediatek/engineermode/usb/UsbEx;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/usb/UsbEx;->mMsg:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbEx$3;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/usb/UsbEx$3;-><init>(Lcom/mediatek/engineermode/usb/UsbEx;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080327

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbEx$4;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/usb/UsbEx$4;-><init>(Lcom/mediatek/engineermode/usb/UsbEx;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeDeInit()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
