.class final Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;
.super Landroid/os/Handler;
.source "UsbIf.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/usb/UsbIf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/usb/UsbIf;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/usb/UsbIf;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/usb/UsbIf;Landroid/os/Looper;Lcom/mediatek/engineermode/usb/UsbIf$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/usb/UsbIf;
    .param p2    # Landroid/os/Looper;
    .param p3    # Lcom/mediatek/engineermode/usb/UsbIf$1;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;-><init>(Lcom/mediatek/engineermode/usb/UsbIf;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x14

    if-ne v1, v2, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {v1}, Lcom/mediatek/engineermode/usb/UsbIf;->access$200(Lcom/mediatek/engineermode/usb/UsbIf;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeGetMsg()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/usb/UsbIf;->access$102(Lcom/mediatek/engineermode/usb/UsbIf;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {v1}, Lcom/mediatek/engineermode/usb/UsbIf;->access$100(Lcom/mediatek/engineermode/usb/UsbIf;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {v1}, Lcom/mediatek/engineermode/usb/UsbIf;->access$300(Lcom/mediatek/engineermode/usb/UsbIf;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const-wide/16 v1, 0xc8

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "USBIF"

    const-string v2, "sleep 200 error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x15

    if-ne v1, v2, :cond_3

    const-string v1, "USBIF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "command--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {v3}, Lcom/mediatek/engineermode/usb/UsbIf;->access$400(Lcom/mediatek/engineermode/usb/UsbIf;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {v1}, Lcom/mediatek/engineermode/usb/UsbIf;->access$400(Lcom/mediatek/engineermode/usb/UsbIf;)I

    move-result v1

    invoke-static {v1}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeStartTest(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbIf$WorkHandler;->this$0:Lcom/mediatek/engineermode/usb/UsbIf;

    invoke-static {v1}, Lcom/mediatek/engineermode/usb/UsbIf;->access$300(Lcom/mediatek/engineermode/usb/UsbIf;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    const-string v1, "USBIF"

    const-string v2, "Task finish"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method
