.class public Lcom/mediatek/engineermode/videotelephone/Configuration;
.super Landroid/preference/PreferenceActivity;
.source "Configuration.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final AUDIO_CHANNEL_ADAPTATION:Ljava/lang/String; = "audio_channel_adaptation"

.field private static final MULTIPLE_LEVEL:Ljava/lang/String; = "multiplex_level"

.field private static final TAG:Ljava/lang/String; = "Configuration"

.field private static final TERMINAL_TYPE:Ljava/lang/String; = "terminal_type"

.field private static final USE_WNSRP:Ljava/lang/String; = "use_wnsrp"

.field private static final VIDEO_CHANNEL_ADAPTATION:Ljava/lang/String; = "video_channel_adaptation"

.field private static final VIDEO_CHANNEL_REV:Ljava/lang/String; = "video_channel_reverse_data_type"

.field private static final VIDEO_CODEC:Ljava/lang/String; = "video_codec_preference"


# instance fields
.field private mAudioArr:[Ljava/lang/String;

.field private mAudioChanneAdaptPref:Landroid/preference/ListPreference;

.field private mMultiArr:[Ljava/lang/String;

.field private mMultiPref:Landroid/preference/ListPreference;

.field private mTerminalArr:[Ljava/lang/String;

.field private mTerminalTypePref:Landroid/preference/ListPreference;

.field private mUseArr:[Ljava/lang/String;

.field private mUseWnsrpPref:Landroid/preference/ListPreference;

.field private mVideoArr:[Ljava/lang/String;

.field private mVideoCdecPref:Landroid/preference/ListPreference;

.field private mVideoChanneAdaptlPref:Landroid/preference/ListPreference;

.field private mVideoChanneRevPref:Landroid/preference/ListPreference;

.field private mVideoCodecArr:[Ljava/lang/String;

.field private mVideoRevArr:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private initSummary()V
    .locals 14

    const-string v0, "AL2 WithSequenceNumber"

    const-string v10, "AL2 WithSequenceNumber"

    const-string v9, "No Change"

    const-string v3, "MuxLevel 2"

    const-string v8, "MPEG4_H263"

    const-string v7, "ON"

    const-string v6, "Normal"

    :try_start_0
    const-string v11, "engineermode_vt_preferences"

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "0"

    const-string v11, "config_audio_channel_adapt"

    const-string v12, "1"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioArr:[Ljava/lang/String;

    aget-object v0, v11, v2

    const-string v11, "config_video_channel_adapt"

    const-string v12, "1"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoArr:[Ljava/lang/String;

    aget-object v10, v11, v2

    const-string v11, "config_video_channel_reverse"

    const-string v12, "0"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoRevArr:[Ljava/lang/String;

    aget-object v9, v11, v2

    const-string v11, "config_multiplex_level"

    const-string v12, "4"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiArr:[Ljava/lang/String;

    aget-object v3, v11, v2

    const-string v11, "config_video_codec_preference"

    const-string v12, "1"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCodecArr:[Ljava/lang/String;

    aget-object v8, v11, v2

    const-string v11, "config_use_wnsrp"

    const-string v12, "2"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseArr:[Ljava/lang/String;

    aget-object v7, v11, v2

    const-string v11, "config_terminal_type"

    const-string v12, "1"

    invoke-interface {v4, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalArr:[Ljava/lang/String;

    aget-object v6, v11, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioChanneAdaptPref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneAdaptlPref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v10}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneRevPref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiPref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCdecPref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v8}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseWnsrpPref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v7}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalTypePref:Landroid/preference/ListPreference;

    invoke-virtual {v11, v6}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :catch_0
    move-exception v1

    const-string v11, "Configuration"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v11, "Configuration"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setPreferenceSummary(Ljava/lang/String;Ljava/lang/String;Landroid/preference/ListPreference;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/preference/ListPreference;

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v0, "Configuration"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSummary : listPreference.getValue() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3, p1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v0, "Configuration"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSummary : value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040001

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060009

    :try_start_0
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioArr:[Ljava/lang/String;

    const v2, 0x7f06000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoArr:[Ljava/lang/String;

    const v2, 0x7f06000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoRevArr:[Ljava/lang/String;

    const v2, 0x7f06000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiArr:[Ljava/lang/String;

    const v2, 0x7f060011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCodecArr:[Ljava/lang/String;

    const v2, 0x7f060013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseArr:[Ljava/lang/String;

    const v2, 0x7f060015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalArr:[Ljava/lang/String;

    const-string v2, "audio_channel_adaptation"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioChanneAdaptPref:Landroid/preference/ListPreference;

    const-string v2, "video_channel_adaptation"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneAdaptlPref:Landroid/preference/ListPreference;

    const-string v2, "video_channel_reverse_data_type"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneRevPref:Landroid/preference/ListPreference;

    const-string v2, "multiplex_level"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiPref:Landroid/preference/ListPreference;

    const-string v2, "video_codec_preference"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCdecPref:Landroid/preference/ListPreference;

    const-string v2, "use_wnsrp"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseWnsrpPref:Landroid/preference/ListPreference;

    const-string v2, "terminal_type"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalTypePref:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioChanneAdaptPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneAdaptlPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneRevPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCdecPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseWnsrpPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalTypePref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/videotelephone/Configuration;->initSummary()V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Configuration"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotFoundException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 12
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v9, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v8, 0x0

    :goto_0
    return v8

    :cond_0
    const-string v8, "engineermode_vt_preferences"

    invoke-virtual {p0, v8, v9}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v8, "Configuration"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "enter onPreferenceChange key is:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, ""

    const-string v6, "0"

    const-string v4, ""

    const-string v8, "Configuration"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "newValue = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, p2

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v8, "Configuration"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "preference = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "audio_channel_adaptation"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioChanneAdaptPref:Landroid/preference/ListPreference;

    const-string v5, "config_audio_channel_adapt"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mAudioArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    :goto_1
    move-object v6, p2

    check-cast v6, Ljava/lang/String;

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0, v4, v1, v2}, Lcom/mediatek/engineermode/videotelephone/Configuration;->setPreferenceSummary(Ljava/lang/String;Ljava/lang/String;Landroid/preference/ListPreference;)V

    move v8, v9

    goto/16 :goto_0

    :cond_1
    const-string v8, "video_channel_adaptation"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneAdaptlPref:Landroid/preference/ListPreference;

    const-string v5, "config_video_channel_adapt"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    goto :goto_1

    :cond_2
    const-string v8, "video_channel_reverse_data_type"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoChanneRevPref:Landroid/preference/ListPreference;

    const-string v5, "config_video_channel_reverse"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoRevArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    goto :goto_1

    :cond_3
    const-string v8, "multiplex_level"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiPref:Landroid/preference/ListPreference;

    const-string v5, "config_multiplex_level"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mMultiArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    goto :goto_1

    :cond_4
    const-string v8, "video_codec_preference"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCdecPref:Landroid/preference/ListPreference;

    const-string v5, "config_video_codec_preference"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mVideoCodecArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    goto :goto_1

    :cond_5
    const-string v8, "use_wnsrp"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseWnsrpPref:Landroid/preference/ListPreference;

    const-string v5, "config_use_wnsrp"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mUseArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalTypePref:Landroid/preference/ListPreference;

    const-string v5, "config_terminal_type"

    iget-object v8, p0, Lcom/mediatek/engineermode/videotelephone/Configuration;->mTerminalArr:[Ljava/lang/String;

    aget-object v4, v8, v3

    goto :goto_1
.end method
