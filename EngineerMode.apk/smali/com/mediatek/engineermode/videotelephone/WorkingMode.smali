.class public Lcom/mediatek/engineermode/videotelephone/WorkingMode;
.super Landroid/app/Activity;
.source "WorkingMode.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EM/WorkingMode"


# instance fields
.field private final mListener:Landroid/view/View$OnClickListener;

.field private mMediaLoopRadiogroup:Landroid/widget/RadioGroup;

.field private mNetworkLoopRadiogroup:Landroid/widget/RadioGroup;

.field private mNetworkLoopStackRadio:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/engineermode/videotelephone/WorkingMode$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/videotelephone/WorkingMode$1;-><init>(Lcom/mediatek/engineermode/videotelephone/WorkingMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/videotelephone/WorkingMode;)Landroid/widget/RadioGroup;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/videotelephone/WorkingMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mMediaLoopRadiogroup:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/videotelephone/WorkingMode;)Landroid/widget/RadioGroup;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/videotelephone/WorkingMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mNetworkLoopRadiogroup:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/videotelephone/WorkingMode;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/videotelephone/WorkingMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->onClicked()V

    return-void
.end method

.method private onClicked()V
    .locals 0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v9, 0x7f030068

    invoke-virtual {p0, v9}, Landroid/app/Activity;->setContentView(I)V

    const v9, 0x7f0b0291

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    const v9, 0x7f0b0292

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const v9, 0x7f0b0295

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const v9, 0x7f0b0296

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    const v9, 0x7f0b0297

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    const v9, 0x7f0b0299

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    iput-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mNetworkLoopStackRadio:Landroid/widget/RadioButton;

    const v9, 0x7f0b029a

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    const v9, 0x7f0b029b

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    const v9, 0x7f0b0294

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioGroup;

    iput-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mMediaLoopRadiogroup:Landroid/widget/RadioGroup;

    const v9, 0x7f0b0298

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioGroup;

    iput-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mNetworkLoopRadiogroup:Landroid/widget/RadioGroup;

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mNetworkLoopStackRadio:Landroid/widget/RadioButton;

    iget-object v10, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v9, "engineermode_vt_preferences"

    const/4 v10, 0x1

    invoke-virtual {p0, v9, v10}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v9, "sdcard_flag"

    const/4 v10, 0x0

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_0

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v9, p0, Lcom/mediatek/engineermode/videotelephone/WorkingMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
