.class Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;
.super Landroid/os/Handler;
.source "WiFiRx6620.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/wifi/WiFiRx6620;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    sget-boolean v8, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Landroid/app/Activity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v8, 0x2

    iget v9, p1, Landroid/os/Message;->what:I

    if-ne v8, v9, :cond_2

    const/4 v8, 0x2

    new-array v1, v8, [J

    const-wide/16 v4, -0x1

    const-wide/16 v2, -0x1

    const-wide/16 v6, -0x1

    const-string v8, "EM/WiFi_Rx"

    const-string v9, "The Handle event is : HANDLER_EVENT_RX"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-static {v8}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->access$000(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    :goto_1
    const/4 v8, 0x2

    invoke-static {v1, v8}, Lcom/mediatek/engineermode/wifi/EMWifi;->getPacketRxStatus([JI)I

    const-string v8, "EM/WiFi_Rx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "after rx test: rx ok = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-wide v10, v1, v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "EM/WiFi_Rx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "after rx test: fcs error = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x1

    aget-wide v10, v1, v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    aget-wide v4, v1, v8

    const/4 v8, 0x1

    aget-wide v2, v1, v8

    add-long v8, v2, v4

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    const-wide/16 v8, 0x64

    mul-long/2addr v8, v2

    add-long v10, v2, v4

    div-long v6, v8, v10

    :cond_1
    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-static {v8}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->access$100(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;

    move-result-object v8

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-static {v8}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->access$200(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;

    move-result-object v8

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-static {v8}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->access$000(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;

    move-result-object v8

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;->this$0:Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-static {v8}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->access$300(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x2

    const-wide/16 v10, 0x3e8

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v8, "EM/WiFi_Rx"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Long.parseLong NumberFormatException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
