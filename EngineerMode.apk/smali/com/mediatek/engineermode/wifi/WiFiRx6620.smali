.class public Lcom/mediatek/engineermode/wifi/WiFiRx6620;
.super Landroid/app/Activity;
.source "WiFiRx6620.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final ATPARAM_INDEX_BANDWIDTH:I = 0xf

.field private static final ATPARAM_INDEX_COMMAND:I = 0x1

.field private static final ATPARAM_INDEX_TEMP_COMPENSATION:I = 0x9

.field private static final BANDWIDTH_INDEX_40:I = 0x1

.field private static final CHANNEL_0:I = 0x0

.field private static final CHANNEL_1:I = 0x1

.field private static final CHANNEL_11:I = 0xb

.field private static final CHANNEL_12:I = 0xc

.field private static final CHANNEL_13:I = 0xd

.field private static final CHANNEL_14:I = 0xe

.field private static final DIALOG_WIFI_ERROR:I = 0x3

.field private static final DIALOG_WIFI_FAIL:I = 0x2

.field private static final HANDLER_EVENT_RX:I = 0x2

.field protected static final HANDLER_RX_DELAY_TIME:J = 0x3e8L

.field private static final PERCENT:J = 0x64L

.field private static final TAG:Ljava/lang/String; = "EM/WiFi_Rx"

.field private static final TEXT_ZERO:Ljava/lang/String; = "0"

.field private static final WAIT_COUNT:I = 0xa


# instance fields
.field private final mBandwidth:[Ljava/lang/String;

.field private mBandwidthAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBandwidthIndex:I

.field private mBandwidthSpinner:Landroid/widget/Spinner;

.field private mBtnGo:Landroid/widget/Button;

.field private mBtnStop:Landroid/widget/Button;

.field private mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

.field private mChannelAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChannelSpinner:Landroid/widget/Spinner;

.field private mChipID:I

.field private final mHandler:Landroid/os/Handler;

.field private mInitData:[J

.field private mTvFcs:Landroid/widget/TextView;

.field private mTvPer:Landroid/widget/TextView;

.field private mTvRx:Landroid/widget/TextView;

.field private mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "20MHz"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "40MHz"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "U20MHz"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "L20MHz"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidth:[Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvFcs:Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvRx:Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvPer:Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnGo:Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnStop:Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelSpinner:Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    iput v4, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthIndex:I

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iput v4, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChipID:I

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iput-object v3, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mInitData:[J

    new-instance v0, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620$1;-><init>(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvPer:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvFcs:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvRx:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Lcom/mediatek/engineermode/wifi/ChannelInfo;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthIndex:I

    return v0
.end method

.method static synthetic access$602(Lcom/mediatek/engineermode/wifi/WiFiRx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthIndex:I

    return p1
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidth:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->updateWifiChannel()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)Lcom/mediatek/engineermode/wifi/WiFiStateManager;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiRx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    return-object v0
.end method

.method private checkWiFiChipState()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/wifi/WiFiStateManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    invoke-virtual {v1, p0}, Lcom/mediatek/engineermode/wifi/WiFiStateManager;->checkState(Landroid/content/Context;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_1
        0x5920 -> :sswitch_0
        0x6620 -> :sswitch_0
    .end sparse-switch
.end method

.method private onClickBtnRxGo()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mInitData:[J

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/wifi/EMWifi;->getPacketRxStatus([JI)I

    const-string v2, "EM/WiFi_Rx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "before rx test: rx ok = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mInitData:[J

    aget-wide v4, v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EM/WiFi_Rx"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "before rx test: fcs error = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mInitData:[J

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const-wide/16 v2, 0x9

    int-to-long v4, v0

    invoke-static {v2, v3, v4, v5}, Lcom/mediatek/engineermode/wifi/EMWifi;->setATParam(JJ)I

    const-wide/16 v2, 0xf

    iget v4, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthIndex:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Lcom/mediatek/engineermode/wifi/EMWifi;->setATParam(JJ)I

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/mediatek/engineermode/wifi/EMWifi;->setATParam(JJ)I

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvFcs:Landroid/widget/TextView;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvRx:Landroid/widget/TextView;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvPer:Landroid/widget/TextView;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->setViewEnabled(Z)V

    return-void
.end method

.method private onClickBtnRxStop()V
    .locals 14

    const-wide/16 v12, 0x0

    const/4 v8, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v2, -0x1

    const-wide/16 v4, -0x1

    new-array v1, v8, [J

    new-array v6, v11, [J

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    :goto_0
    const/16 v7, 0xa

    if-ge v0, v7, :cond_0

    const-wide/16 v7, 0x1

    invoke-static {v7, v8, v12, v13}, Lcom/mediatek/engineermode/wifi/EMWifi;->setATParam(JJ)I

    move-result v7

    int-to-long v7, v7

    aput-wide v7, v6, v10

    aget-wide v7, v6, v10

    cmp-long v7, v7, v12

    if-nez v7, :cond_1

    :cond_0
    invoke-direct {p0, v11}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->setViewEnabled(Z)V

    return-void

    :cond_1
    const-wide/16 v7, 0xa

    invoke-static {v7, v8}, Landroid/os/SystemClock;->sleep(J)V

    const-string v7, "EM/WiFi_Rx"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "stop Rx test failed at the "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "times try"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setViewEnabled(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnGo:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnStop:Landroid/widget/Button;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateWifiChannel()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v1, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelIndex()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v2, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mChannelSelect:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelFreq()I

    move-result v0

    int-to-long v1, v0

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/wifi/EMWifi;->setChannel(J)I

    const-string v1, "EM/WiFi_Rx"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The channel freq ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/AbsSpinner;->setSelection(I)V

    goto :goto_0

    :cond_1
    const-string v1, "EM/WiFi_Rx"

    const-string v2, "Wifi is not initialized"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    sget-boolean v0, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnGo:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->onClickBtnRxGo()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->onClickBtnRxStop()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const v10, 0x1090009

    const v9, 0x1090008

    const v6, 0x7f0801c0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030064

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    const v5, 0x7f0b026b

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvFcs:Landroid/widget/TextView;

    const v5, 0x7f0b026d

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvRx:Landroid/widget/TextView;

    const v5, 0x7f0b026f

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvPer:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvFcs:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvRx:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mTvPer:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    const v5, 0x7f0b0271

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnGo:Landroid/widget/Button;

    const v5, 0x7f0b0272

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnStop:Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnGo:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v5, 0x2

    new-array v5, v5, [J

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mInitData:[J

    new-instance v5, Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-direct {v5}, Lcom/mediatek/engineermode/wifi/ChannelInfo;-><init>()V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    const v5, 0x7f0b0269

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelSpinner:Landroid/widget/Spinner;

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v1, 0x1

    :goto_0
    int-to-long v5, v1

    sget-object v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    const/4 v8, 0x0

    aget-wide v7, v7, v8

    cmp-long v5, v5, v7

    if-gtz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v0, v5, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Channel "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    aget-wide v6, v6, v1

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mChannelSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiRx6620$2;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620$2;-><init>(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v5, 0x7f0b0274

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x0

    :goto_2
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidth:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidth:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiRx6620$3;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620$3;-><init>(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->setViewEnabled(Z)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f0801bf

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v2, "EM/WiFi_Rx"

    const-string v3, "error dialog ID"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801b8

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801b9

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/mediatek/engineermode/wifi/WiFiRx6620$4;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620$4;-><init>(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801ba

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801bb

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/mediatek/engineermode/wifi/WiFiRx6620$5;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620$5;-><init>(Lcom/mediatek/engineermode/wifi/WiFiRx6620;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    sget-boolean v0, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/engineermode/wifi/EMWifi;->setATParam(JJ)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiRx6620;->checkWiFiChipState()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
