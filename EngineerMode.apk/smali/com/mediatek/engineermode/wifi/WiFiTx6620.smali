.class public Lcom/mediatek/engineermode/wifi/WiFiTx6620;
.super Landroid/app/Activity;
.source "WiFiTx6620.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/wifi/WiFiTx6620$EventHandler;,
        Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;
    }
.end annotation


# static fields
.field private static final ATPARAM_INDEX_ANTENNA:I = 0x5

.field private static final ATPARAM_INDEX_BANDWIDTH:I = 0xf

.field private static final ATPARAM_INDEX_COMMAND:I = 0x1

.field private static final ATPARAM_INDEX_CWMODE:I = 0x41

.field private static final ATPARAM_INDEX_GI:I = 0x10

.field private static final ATPARAM_INDEX_PACKCONTENT:I = 0xc

.field private static final ATPARAM_INDEX_PACKCOUNT:I = 0x7

.field private static final ATPARAM_INDEX_PACKINTERVAL:I = 0x8

.field private static final ATPARAM_INDEX_PACKLENGTH:I = 0x6

.field private static final ATPARAM_INDEX_POWER:I = 0x2

.field private static final ATPARAM_INDEX_PREAMBLE:I = 0x4

.field private static final ATPARAM_INDEX_QOS_QUEUE:I = 0xe

.field private static final ATPARAM_INDEX_RATE:I = 0x3

.field private static final ATPARAM_INDEX_RETRY_LIMIT:I = 0xd

.field private static final ATPARAM_INDEX_TEMP_COMPENSATION:I = 0x9

.field private static final ATPARAM_INDEX_TRANSMITCOUNT:I = 0x20

.field private static final ATPARAM_INDEX_TXOP_LIMIT:I = 0xa

.field private static final BANDWIDTH_40MHZ_MASK:I = 0x8000

.field private static final BANDWIDTH_INDEX_40:I = 0x1

.field private static final BIT_8_MASK:I = 0xff

.field private static final CCK_RATE_NUMBER:I = 0x4

.field private static final CHANNEL_0:I = 0x0

.field private static final CHANNEL_1:I = 0x1

.field private static final CHANNEL_11:I = 0xb

.field private static final CHANNEL_12:I = 0xc

.field private static final CHANNEL_13:I = 0xd

.field private static final CHANNEL_14:I = 0xe

.field private static final COMMAND_INDEX_CARRIER:I = 0x6

.field private static final COMMAND_INDEX_CARRIER_NEW:I = 0xa

.field private static final COMMAND_INDEX_LOCALFREQ:I = 0x5

.field private static final COMMAND_INDEX_OUTPUTPOWER:I = 0x4

.field private static final COMMAND_INDEX_STARTTX:I = 0x1

.field private static final COMMAND_INDEX_STOPTEST:I = 0x0

.field private static final CWMODE_CCKPI:I = 0x5

.field private static final CWMODE_OFDMLTF:I = 0x2

.field private static final DEFAULT_PKT_CNT:I = 0xbb8

.field private static final DEFAULT_PKT_LEN:I = 0x400

.field private static final DEFAULT_TX_GAIN:I = 0x0

.field private static final DIALOG_WIFI_ERROR:I = 0x3

.field private static final DIALOG_WIFI_FAIL:I = 0x2

.field private static final HANDLER_EVENT_FINISH:I = 0x4

.field private static final HANDLER_EVENT_GO:I = 0x1

.field private static final HANDLER_EVENT_STOP:I = 0x2

.field private static final HANDLER_EVENT_TIMER:I = 0x3

.field private static final LENGTH_3:I = 0x3

.field private static final MAX_HIGH_RATE_NUMBER:I = 0x15

.field private static final MAX_LOWER_RATE_NUMBER:I = 0xc

.field private static final MAX_VALUE:J = -0x1L

.field private static final MIN_VALUE:I = 0x0

.field private static final ONE_SENCOND:I = 0x3e8

.field public static final PACKCONTENT_BUFFER:[J

.field private static final RATE_MCS_INDEX:I = 0x20

.field private static final RATE_MODE_MASK:I = 0x1f

.field private static final RATE_NOT_MCS_INDEX:I = 0x7

.field private static final TAG:Ljava/lang/String; = "EM/WiFi_Tx"

.field private static final TEST_MODE_CARRIER:I = 0x2

.field private static final TEST_MODE_DUTY:I = 0x1

.field private static final TEST_MODE_LEAKAGE:I = 0x3

.field private static final TEST_MODE_POWEROFF:I = 0x4

.field private static final TEST_MODE_TX:I = 0x0

.field private static final TXOP_LIMIT_VALUE:I = 0x20000


# instance fields
.field private mAntenna:I

.field mBandwidth:[Ljava/lang/String;

.field private mBandwidthAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBandwidthIndex:I

.field private mBandwidthSpinner:Landroid/widget/Spinner;

.field private mBtnGo:Landroid/widget/Button;

.field private mBtnStop:Landroid/widget/Button;

.field private mCCKRateSelected:Z

.field private mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

.field private mChannelAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChannelSpinner:Landroid/widget/Spinner;

.field private mChipID:J

.field private mCntNum:J

.field private mEtPkt:Landroid/widget/EditText;

.field private mEtPktCnt:Landroid/widget/EditText;

.field private mEtTxGain:Landroid/widget/EditText;

.field private mEventHandler:Landroid/os/Handler;

.field mGuardInterval:[Ljava/lang/String;

.field private mGuardIntervalAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mGuardIntervalIndex:I

.field private mGuardIntervalSpinner:Landroid/widget/Spinner;

.field private final mHandler:Landroid/os/Handler;

.field private mHighRateSelected:Z

.field private mLastBandwidth:I

.field private mLastRateGroup:I

.field mMode:[Ljava/lang/String;

.field private mModeAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mModeIndex:I

.field private mModeSpinner:Landroid/widget/Spinner;

.field private mPktLenNum:J

.field mPreamble:[Ljava/lang/String;

.field private mPreambleAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPreambleIndex:I

.field private mPreambleSpinner:Landroid/widget/Spinner;

.field private mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

.field private mRateAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRateSpinner:Landroid/widget/Spinner;

.field private mTestInPorcess:Z

.field private mTestThread:Landroid/os/HandlerThread;

.field private mTxGainVal:J

.field private mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->PACKCONTENT_BUFFER:[J

    return-void

    nop

    :array_0
    .array-data 8
        -0xddfffc
        0x33440006
        0x55660008
        0x55550019
        -0x5555ffe5
        -0x4444ffe3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mHighRateSelected:Z

    iput-boolean v4, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mCCKRateSelected:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChipID:J

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastRateGroup:I

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastBandwidth:I

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelSpinner:Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalSpinner:Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleSpinner:Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPkt:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPktCnt:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtTxGain:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateSpinner:Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeSpinner:Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnGo:Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnStop:Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateAdapter:Landroid/widget/ArrayAdapter;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeAdapter:Landroid/widget/ArrayAdapter;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleAdapter:Landroid/widget/ArrayAdapter;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalAdapter:Landroid/widget/ArrayAdapter;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleIndex:I

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthIndex:I

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalIndex:I

    iput v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mAntenna:I

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    const-wide/16 v0, 0x400

    iput-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPktLenNum:J

    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mCntNum:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    iput-boolean v3, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestInPorcess:Z

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestThread:Landroid/os/HandlerThread;

    new-instance v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$1;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mHandler:Landroid/os/Handler;

    iput-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "continuous packet tx"

    aput-object v1, v0, v3

    const-string v1, "100% duty cycle"

    aput-object v1, v0, v4

    const-string v1, "carrier suppression"

    aput-object v1, v0, v5

    const-string v1, "local leakage"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "enter power off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mMode:[Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Normal"

    aput-object v1, v0, v3

    const-string v1, "CCK short"

    aput-object v1, v0, v4

    const-string v1, "802.11n mixed mode"

    aput-object v1, v0, v5

    const-string v1, "802.11n green field"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreamble:[Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "20MHz"

    aput-object v1, v0, v3

    const-string v1, "40MHz"

    aput-object v1, v0, v4

    const-string v1, "U20MHz"

    aput-object v1, v0, v5

    const-string v1, "L20MHz"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidth:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "800ns"

    aput-object v1, v0, v3

    const-string v1, "400ns"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardInterval:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/wifi/WiFiTx6620;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->setViewEnabled(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPktCnt:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mCCKRateSelected:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/engineermode/wifi/WiFiTx6620;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mCCKRateSelected:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->updateChannels()V

    return-void
.end method

.method static synthetic access$1402(Lcom/mediatek/engineermode/wifi/WiFiTx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastRateGroup:I

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/engineermode/wifi/WiFiTx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    return p1
.end method

.method static synthetic access$1508(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)I
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthIndex:I

    return v0
.end method

.method static synthetic access$1602(Lcom/mediatek/engineermode/wifi/WiFiTx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthIndex:I

    return p1
.end method

.method static synthetic access$1702(Lcom/mediatek/engineermode/wifi/WiFiTx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastBandwidth:I

    return p1
.end method

.method static synthetic access$1800(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalIndex:I

    return v0
.end method

.method static synthetic access$1802(Lcom/mediatek/engineermode/wifi/WiFiTx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalIndex:I

    return p1
.end method

.method static synthetic access$1900(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Lcom/mediatek/engineermode/wifi/WiFiStateManager;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Lcom/mediatek/engineermode/wifi/ChannelInfo;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    return-wide v0
.end method

.method static synthetic access$2100(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mAntenna:I

    return v0
.end method

.method static synthetic access$2200(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPktLenNum:J

    return-wide v0
.end method

.method static synthetic access$2300(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-wide v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mCntNum:J

    return-wide v0
.end method

.method static synthetic access$2400(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestInPorcess:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/mediatek/engineermode/wifi/WiFiTx6620;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestInPorcess:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->uiUpdateTxPower()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mHighRateSelected:Z

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/engineermode/wifi/WiFiTx6620;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mHighRateSelected:Z

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleIndex:I

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/engineermode/wifi/WiFiTx6620;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTx6620;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleIndex:I

    return p1
.end method

.method private checkWiFiChipState()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/wifi/WiFiStateManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    invoke-virtual {v1, p0}, Lcom/mediatek/engineermode/wifi/WiFiStateManager;->checkState(Landroid/content/Context;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_1
        0x5920 -> :sswitch_0
        0x6620 -> :sswitch_0
    .end sparse-switch
.end method

.method private onClickBtnTxGo()V
    .locals 18

    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtTxGain:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    const/high16 v10, 0x40000000

    mul-float/2addr v10, v7

    float-to-long v8, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtTxGain:Landroid/widget/EditText;

    const v11, 0x7f0801d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    long-to-double v14, v8

    const-wide/high16 v16, 0x4000000000000000L

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    const-wide/16 v12, 0xff

    cmp-long v10, v10, v12

    if-lez v10, :cond_0

    const-wide/16 v10, 0xff

    :goto_0
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-gez v10, :cond_1

    const-wide/16 v10, 0x0

    :goto_1
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    const-string v10, "EM/WiFi_Tx"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Wifi Tx Test : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mMode:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    aget-object v12, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    packed-switch v10, :pswitch_data_0

    :goto_2
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    if-nez v10, :cond_2

    const-string v10, "EM/WiFi_Tx"

    const-string v11, "eventHandler = null"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    return-void

    :catch_0
    move-exception v3

    const-string v10, "invalid input value"

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto :goto_3

    :cond_0
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTxGainVal:J

    goto :goto_1

    :pswitch_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPkt:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPktCnt:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPktLenNum:J

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mCntNum:J

    goto :goto_2

    :catch_1
    move-exception v3

    const-string v10, "invalid input value"

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto :goto_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->setViewEnabled(Z)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onClickBtnTxStop()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    const-string v0, "EM/WiFi_Tx"

    const-string v1, "eventHandler = null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeIndex:I

    sparse-switch v0, :sswitch_data_0

    invoke-static {}, Lcom/mediatek/engineermode/wifi/EMWifi;->setStandBy()I

    :goto_1
    :sswitch_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :sswitch_1
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/wifi/EMWifi;->setPnpPower(J)I

    invoke-static {}, Lcom/mediatek/engineermode/wifi/EMWifi;->setTestMode()I

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelFreq()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/wifi/EMWifi;->setChannel(J)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->uiUpdateTxPower()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
    .end sparse-switch
.end method

.method private setViewEnabled(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPkt:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPktCnt:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtTxGain:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnGo:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnStop:Landroid/widget/Button;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private uiUpdateTxPower()V
    .locals 15

    const/4 v11, 0x1

    const/4 v5, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x0

    const-wide/16 v7, 0x0

    new-array v4, v5, [J

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelIndex()I

    move-result v6

    iget v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthIndex:I

    if-ne v0, v11, :cond_1

    const v0, 0x8000

    :goto_0
    or-int/2addr v6, v0

    const-string v0, "EM/WiFi_Tx"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "channelIdx "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rateIdx "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    iget v2, v2, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->mRateIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " gain "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v4}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Len "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    int-to-long v0, v6

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    iget v2, v2, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->mRateIndex:I

    int-to-long v2, v2

    invoke-static/range {v0 .. v5}, Lcom/mediatek/engineermode/wifi/EMWifi;->readTxPowerFromEEPromEx(JJ[JI)I

    move-result v0

    if-nez v0, :cond_0

    aget-wide v7, v4, v10

    const-string v0, "EM/WiFi_Tx"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "i4TxPwrGain from uiUpdateTxPower is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0xff

    and-long/2addr v0, v7

    long-to-int v0, v0

    int-to-short v9, v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtTxGain:Landroid/widget/EditText;

    const v1, 0x7f0801d0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v11, [Ljava/lang/Object;

    int-to-double v11, v9

    const-wide/high16 v13, 0x4000000000000000L

    div-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    move v0, v10

    goto/16 :goto_0
.end method

.method private updateChannels()V
    .locals 11

    const/4 v1, 0x0

    sget-boolean v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sHas14Ch:Z

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastRateGroup:I

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-virtual {v8}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->getUcRateGroupEep()I

    move-result v8

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-virtual {v7}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->getUcRateGroupEep()I

    move-result v7

    if-nez v7, :cond_4

    sget-boolean v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sHasUpper14Ch:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xe

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v4

    const/4 v7, -0x1

    if-ne v7, v4, :cond_2

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xd

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const/4 v1, 0x1

    :cond_0
    :goto_0
    sget-boolean v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sHasUpper14Ch:Z

    if-eqz v7, :cond_7

    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastRateGroup:I

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-virtual {v8}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->getUcRateGroupEep()I

    move-result v8

    if-eq v7, v8, :cond_7

    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastRateGroup:I

    if-nez v7, :cond_6

    const/4 v2, 0x1

    :goto_1
    int-to-long v7, v2

    sget-object v9, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    const/4 v10, 0x0

    aget-wide v9, v9, v10

    cmp-long v7, v7, v9

    if-gtz v7, :cond_7

    sget-object v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    aget-wide v7, v7, v2

    const-wide/16 v9, 0xe

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v0, v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_1

    aget-object v6, v0, v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Channel "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    aget-wide v8, v8, v2

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v7, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const/4 v1, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xd

    aget-object v8, v8, v9

    invoke-virtual {v7, v8, v4}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xd

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastRateGroup:I

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xd

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-virtual {v7}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->getUcRateGroupEep()I

    move-result v7

    if-nez v7, :cond_7

    const/16 v2, 0xe

    :goto_3
    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v7, v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    array-length v7, v7

    if-ge v2, v7, :cond_7

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    aget-object v8, v8, v2

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    const/4 v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastBandwidth:I

    iget v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthIndex:I

    if-eq v7, v8, :cond_b

    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthIndex:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xb

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xc

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    const/4 v1, 0x1

    :cond_8
    iget v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mLastBandwidth:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_b

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->isContains(I)Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xb

    aget-object v8, v8, v9

    const/16 v9, 0xb

    invoke-virtual {v7, v8, v9}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    :cond_9
    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    const/16 v8, 0xd

    invoke-virtual {v7, v8}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->isContains(I)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v8, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v8, v8, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    const/16 v9, 0xc

    aget-object v8, v8, v9

    const/16 v9, 0xc

    invoke-virtual {v7, v8, v9}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    :cond_a
    const/4 v1, 0x1

    :cond_b
    if-eqz v1, :cond_c

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->updateWifiChannel()V

    :cond_c
    return-void
.end method

.method private updateWifiChannel()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v1, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-nez v1, :cond_0

    const-string v1, "EM/WiFi_Tx"

    const-string v2, "Wifi is not initialized"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelIndex()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v2, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mChannelSelect:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelFreq()I

    move-result v0

    int-to-long v1, v0

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/wifi/EMWifi;->setChannel(J)I

    const-string v1, "EM/WiFi_Tx"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The channel freq ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->uiUpdateTxPower()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/AbsSpinner;->setSelection(I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    sget-boolean v0, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-nez v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "EM/WiFi_Tx"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "view_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnGo:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->onClickBtnTxGo()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnStop:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->onClickBtnTxStop()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const v10, 0x1090009

    const v9, 0x1090008

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030067

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    const v5, 0x7f0b027a

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelSpinner:Landroid/widget/Spinner;

    const v5, 0x7f0b028e

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleSpinner:Landroid/widget/Spinner;

    const v5, 0x7f0b027c

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPkt:Landroid/widget/EditText;

    const v5, 0x7f0b027e

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPktCnt:Landroid/widget/EditText;

    const v5, 0x7f0b028c

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtTxGain:Landroid/widget/EditText;

    const v5, 0x7f0b0282

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateSpinner:Landroid/widget/Spinner;

    const v5, 0x7f0b0284

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeSpinner:Landroid/widget/Spinner;

    const v5, 0x7f0b028a

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnGo:Landroid/widget/Button;

    const v5, 0x7f0b028b

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnStop:Landroid/widget/Button;

    const v5, 0x7f0b0274

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    const v5, 0x7f0b028f

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalSpinner:Landroid/widget/Spinner;

    new-instance v5, Landroid/os/HandlerThread;

    const-string v6, "Wifi Tx Test"

    invoke-direct {v5, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestThread:Landroid/os/HandlerThread;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    new-instance v5, Lcom/mediatek/engineermode/wifi/WiFiTx6620$EventHandler;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestThread:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$EventHandler;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;Landroid/os/Looper;)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnGo:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/mediatek/engineermode/wifi/ChannelInfo;

    invoke-direct {v5}, Lcom/mediatek/engineermode/wifi/ChannelInfo;-><init>()V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    new-instance v5, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-direct {v5, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEtPktCnt:Landroid/widget/EditText;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$2;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$2;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x1

    :goto_0
    int-to-long v5, v1

    sget-object v7, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    const/4 v8, 0x0

    aget-wide v7, v7, v8

    cmp-long v5, v5, v7

    if-gtz v5, :cond_0

    sget-object v5, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    aget-wide v5, v5, v1

    const-wide/16 v7, 0xe

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$3;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$3;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-virtual {v5}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->getRateNumber()I

    move-result v5

    if-ge v1, v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRate:Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;

    invoke-static {v6}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;->access$500(Lcom/mediatek/engineermode/wifi/WiFiTx6620$RateInfo;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannel:Lcom/mediatek/engineermode/wifi/ChannelInfo;

    iget-object v0, v5, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mFullChannelName:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Channel "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/mediatek/engineermode/wifi/ChannelInfo;->sChannels:[J

    aget-wide v6, v6, v1

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mChannelAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mRateSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$4;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$4;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x0

    :goto_3
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mMode:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mMode:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mModeSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$5;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$5;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x0

    :goto_4
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreamble:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreamble:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mPreambleSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$6;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$6;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x0

    :goto_5
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidth:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidth:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mBandwidthSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$7;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$7;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, p0, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const/4 v1, 0x0

    :goto_6
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardInterval:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardInterval:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_8
    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mGuardIntervalSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/mediatek/engineermode/wifi/WiFiTx6620$8;

    invoke-direct {v6, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$8;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->setViewEnabled(Z)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f0801bf

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v2, "EM/WiFi_Tx"

    const-string v3, "error dialog ID"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801b8

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801b9

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/mediatek/engineermode/wifi/WiFiTx6620$9;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$9;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801ba

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801bb

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/mediatek/engineermode/wifi/WiFiTx6620$10;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620$10;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTx6620;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mEventHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestInPorcess:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/engineermode/wifi/EMWifi;->setATParam(JJ)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestInPorcess:Z

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->mTestThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/wifi/WiFiTx6620;->checkWiFiChipState()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
