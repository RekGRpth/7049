.class public Lcom/android/simmelock/LockSetting;
.super Landroid/app/Activity;
.source "LockSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field private static final LOCK_ICC_SML_COMPLETE:I = 0x78


# instance fields
.field final DIALOG_LOCKFAIL:I

.field final DIALOG_LOCKSUCCEED:I

.field final DIALOG_PASSWORDLENGTHINCORRECT:I

.field final DIALOG_PASSWORDWRONG:I

.field bundle:Landroid/os/Bundle;

.field private clickFlag:Z

.field et:Landroid/widget/EditText;

.field intSIMNumber:I

.field lockCategory:I

.field private lockName:Ljava/lang/String;

.field private lockPassword:Ljava/lang/String;

.field private lockPwdCorrect:Z

.field private mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field phone:Lcom/android/internal/telephony/PhoneBase;

.field re_et:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/simmelock/LockSetting$7;

    invoke-direct {v0, p0}, Lcom/android/simmelock/LockSetting$7;-><init>(Lcom/android/simmelock/LockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/LockSetting;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/simmelock/LockSetting$8;

    invoke-direct {v0, p0}, Lcom/android/simmelock/LockSetting$8;-><init>(Lcom/android/simmelock/LockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/LockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object v1, p0, Lcom/android/simmelock/LockSetting;->et:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/simmelock/LockSetting;->re_et:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/simmelock/LockSetting;->lockName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/simmelock/LockSetting;->lockPassword:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/simmelock/LockSetting;->lockPwdCorrect:Z

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/simmelock/LockSetting;->DIALOG_LOCKFAIL:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/simmelock/LockSetting;->DIALOG_PASSWORDLENGTHINCORRECT:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/simmelock/LockSetting;->DIALOG_LOCKSUCCEED:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/simmelock/LockSetting;->DIALOG_PASSWORDWRONG:I

    iput v2, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    iput v2, p0, Lcom/android/simmelock/LockSetting;->intSIMNumber:I

    iput-object v1, p0, Lcom/android/simmelock/LockSetting;->bundle:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/android/simmelock/LockSetting;->phone:Lcom/android/internal/telephony/PhoneBase;

    iput-boolean v2, p0, Lcom/android/simmelock/LockSetting;->clickFlag:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/simmelock/LockSetting;)Z
    .locals 1
    .param p0    # Lcom/android/simmelock/LockSetting;

    iget-boolean v0, p0, Lcom/android/simmelock/LockSetting;->clickFlag:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/simmelock/LockSetting;Z)Z
    .locals 0
    .param p0    # Lcom/android/simmelock/LockSetting;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/simmelock/LockSetting;->clickFlag:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/simmelock/LockSetting;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/simmelock/LockSetting;

    iget-object v0, p0, Lcom/android/simmelock/LockSetting;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLockName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/16 v9, 0x81

    const/16 v8, 0x8

    const/4 v7, 0x0

    const/4 v6, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f030001

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    iput-object v4, p0, Lcom/android/simmelock/LockSetting;->bundle:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->bundle:Landroid/os/Bundle;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->bundle:Landroid/os/Bundle;

    const-string v5, "LockCategory"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    :cond_0
    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    if-ne v4, v6, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    invoke-direct {p0, v4}, Lcom/android/simmelock/LockSetting;->getLockName(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/simmelock/LockSetting;->lockName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->lockName:Ljava/lang/String;

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const v4, 0x7f070010

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/simmelock/LockSetting;->et:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->et:Landroid/widget/EditText;

    if-nez v4, :cond_2

    const-string v4, "X"

    const-string v5, "clocwork worked..."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->et:Landroid/widget/EditText;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->et:Landroid/widget/EditText;

    invoke-static {v4, v8}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/android/simmelock/LockSetting;->lockCategory:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_6

    :cond_3
    const v4, 0x7f070011

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-nez v3, :cond_4

    const-string v4, "X"

    const-string v5, "clocwork worked..."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    const v4, 0x7f070012

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/simmelock/LockSetting;->re_et:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->re_et:Landroid/widget/EditText;

    if-nez v4, :cond_5

    const-string v4, "X"

    const-string v5, "clocwork worked..."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->re_et:Landroid/widget/EditText;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->re_et:Landroid/widget/EditText;

    invoke-static {v4, v8}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    iget-object v4, p0, Lcom/android/simmelock/LockSetting;->re_et:Landroid/widget/EditText;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    const v4, 0x7f070013

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-nez v1, :cond_7

    const-string v4, "X"

    const-string v5, "clocwork worked..."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    new-instance v4, Lcom/android/simmelock/LockSetting$1;

    invoke-direct {v4, p0}, Lcom/android/simmelock/LockSetting$1;-><init>(Lcom/android/simmelock/LockSetting;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f070014

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v4, Lcom/android/simmelock/LockSetting$2;

    invoke-direct {v4, p0}, Lcom/android/simmelock/LockSetting$2;-><init>(Lcom/android/simmelock/LockSetting;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/simmelock/LockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showAlertDialog(I)V
    .locals 6
    .param p1    # I

    const v5, 0x7f06000f

    const v4, 0x7f06000c

    const v3, 0x1080027

    const/4 v2, 0x0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060010

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/LockSetting$3;

    invoke-direct {v2, p0}, Lcom/android/simmelock/LockSetting$3;-><init>(Lcom/android/simmelock/LockSetting;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060020

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/LockSetting$4;

    invoke-direct {v2, p0}, Lcom/android/simmelock/LockSetting$4;-><init>(Lcom/android/simmelock/LockSetting;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060011

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/LockSetting$5;

    invoke-direct {v2, p0}, Lcom/android/simmelock/LockSetting$5;-><init>(Lcom/android/simmelock/LockSetting;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060029

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/LockSetting$6;

    invoke-direct {v2, p0}, Lcom/android/simmelock/LockSetting$6;-><init>(Lcom/android/simmelock/LockSetting;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
