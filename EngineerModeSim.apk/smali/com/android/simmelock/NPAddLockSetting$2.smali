.class Lcom/android/simmelock/NPAddLockSetting$2;
.super Ljava/lang/Object;
.source "NPAddLockSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/simmelock/NPAddLockSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/NPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/NPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x4

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    const-string v0, "SIMMELOCK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clickFlag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-static {v4}, Lcom/android/simmelock/NPAddLockSetting;->access$000(Lcom/android/simmelock/NPAddLockSetting;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/NPAddLockSetting;->access$000(Lcom/android/simmelock/NPAddLockSetting;)Z

    move-result v0

    if-ne v0, v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/NPAddLockSetting;->access$000(Lcom/android/simmelock/NPAddLockSetting;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-static {v0, v6}, Lcom/android/simmelock/NPAddLockSetting;->access$002(Lcom/android/simmelock/NPAddLockSetting;Z)Z

    :cond_1
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NPAddLockSetting;->mbMCCMNCReadSIM:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NPAddLockSetting;->mbMCCMNCReadSIM1:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NPAddLockSetting;->mbMCCMNCReadSIM2:Z

    if-nez v0, :cond_3

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-gt v0, v3, :cond_2

    const/4 v0, 0x6

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-virtual {v0, v6}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, v9, :cond_4

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v3, 0x8

    if-le v0, v3, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-virtual {v0, v9}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/NPAddLockSetting;->access$100(Lcom/android/simmelock/NPAddLockSetting;)Landroid/os/Handler;

    move-result-object v0

    const/16 v3, 0x78

    invoke-static {v0, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    check-cast v8, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->bundle:Landroid/os/Bundle;

    const-string v4, "SIMNo"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/simmelock/NPAddLockSetting;->intSIMNumber:I

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/NPAddLockSetting;->intSIMNumber:I

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NPAddLockSetting;->mbMCCMNCReadSIM1:Z

    if-nez v0, :cond_7

    invoke-virtual {v8, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v8, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NPAddLockSetting;->mbMCCMNCReadSIM2:Z

    if-nez v0, :cond_9

    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NPAddLockSetting$2;->this$0:Lcom/android/simmelock/NPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0
.end method
