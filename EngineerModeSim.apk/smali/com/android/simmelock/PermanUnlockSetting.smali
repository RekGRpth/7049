.class public Lcom/android/simmelock/PermanUnlockSetting;
.super Landroid/app/Activity;
.source "PermanUnlockSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field static DIALOG_PERMANUNLOCK:I = 0x0

.field private static final PERREMOVELOCK_ICC_SML_COMPLETE:I = 0x78


# instance fields
.field bundle:Landroid/os/Bundle;

.field intSIMNumber:I

.field lockCategory:I

.field private lockName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerFinish:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    sput v0, Lcom/android/simmelock/PermanUnlockSetting;->DIALOG_PERMANUNLOCK:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/simmelock/PermanUnlockSetting$1;

    invoke-direct {v0, p0}, Lcom/android/simmelock/PermanUnlockSetting$1;-><init>(Lcom/android/simmelock/PermanUnlockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mHandlerFinish:Landroid/os/Handler;

    new-instance v0, Lcom/android/simmelock/PermanUnlockSetting$4;

    invoke-direct {v0, p0}, Lcom/android/simmelock/PermanUnlockSetting$4;-><init>(Lcom/android/simmelock/PermanUnlockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/simmelock/PermanUnlockSetting$5;

    invoke-direct {v0, p0}, Lcom/android/simmelock/PermanUnlockSetting$5;-><init>(Lcom/android/simmelock/PermanUnlockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockName:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->intSIMNumber:I

    iput-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->bundle:Landroid/os/Bundle;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockCategory:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/simmelock/PermanUnlockSetting;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/simmelock/PermanUnlockSetting;

    iget-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mHandlerFinish:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/simmelock/PermanUnlockSetting;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/simmelock/PermanUnlockSetting;

    iget-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLockName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030004

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->bundle:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->bundle:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->bundle:Landroid/os/Bundle;

    const-string v2, "LockCategory"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockCategory:I

    :cond_0
    iget v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockCategory:I

    if-ne v1, v3, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockCategory:I

    invoke-direct {p0, v1}, Lcom/android/simmelock/PermanUnlockSetting;->getLockName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/simmelock/PermanUnlockSetting;->lockName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f06000c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f06000e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060003

    new-instance v3, Lcom/android/simmelock/PermanUnlockSetting$3;

    invoke-direct {v3, p0}, Lcom/android/simmelock/PermanUnlockSetting$3;-><init>(Lcom/android/simmelock/PermanUnlockSetting;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060004

    new-instance v3, Lcom/android/simmelock/PermanUnlockSetting$2;

    invoke-direct {v3, p0}, Lcom/android/simmelock/PermanUnlockSetting$2;-><init>(Lcom/android/simmelock/PermanUnlockSetting;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/simmelock/PermanUnlockSetting;->mHandlerFinish:Landroid/os/Handler;

    const/16 v1, 0x29a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
