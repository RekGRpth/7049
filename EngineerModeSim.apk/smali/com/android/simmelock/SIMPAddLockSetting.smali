.class public Lcom/android/simmelock/SIMPAddLockSetting;
.super Landroid/app/Activity;
.source "SIMPAddLockSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field private static final ADDLOCK_ICC_SML_COMPLETE:I = 0x78


# instance fields
.field final DIALOG_ADDLOCKFAIL:I

.field final DIALOG_ADDLOCKSUCCEED:I

.field final DIALOG_IMSILENGTHINCORRECT:I

.field final DIALOG_PASSWORDLENGTHINCORRECT:I

.field final DIALOG_PASSWORDWRONG:I

.field bundle:Landroid/os/Bundle;

.field private clickFlag:Z

.field etIMSI:Landroid/widget/EditText;

.field etPwd:Landroid/widget/EditText;

.field etPwdConfirm:Landroid/widget/EditText;

.field intSIMNumber:I

.field private lockCategory:I

.field private lockName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mbIMSICorrect:Z

.field mbIMSIReadSIM:Z

.field mbIMSIReadSIM1:Z

.field mbIMSIReadSIM2:Z

.field miSIMState:I

.field msSIM1IMSI:Ljava/lang/String;

.field msSIM2IMSI:Ljava/lang/String;

.field msSIMIMSI:Ljava/lang/String;

.field s1:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/simmelock/SIMPAddLockSetting$9;

    invoke-direct {v0, p0}, Lcom/android/simmelock/SIMPAddLockSetting$9;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/simmelock/SIMPAddLockSetting$10;

    invoke-direct {v0, p0}, Lcom/android/simmelock/SIMPAddLockSetting$10;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->bundle:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwd:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockName:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockCategory:I

    iput v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->intSIMNumber:I

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIM1IMSI:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIM2IMSI:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIMIMSI:Ljava/lang/String;

    iput v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->miSIMState:I

    iput-boolean v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM1:Z

    iput-boolean v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM2:Z

    iput-boolean v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM:Z

    iput-boolean v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSICorrect:Z

    iput-boolean v2, p0, Lcom/android/simmelock/SIMPAddLockSetting;->clickFlag:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->DIALOG_IMSILENGTHINCORRECT:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->DIALOG_ADDLOCKFAIL:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->DIALOG_ADDLOCKSUCCEED:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->DIALOG_PASSWORDLENGTHINCORRECT:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->DIALOG_PASSWORDWRONG:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/simmelock/SIMPAddLockSetting;)Z
    .locals 1
    .param p0    # Lcom/android/simmelock/SIMPAddLockSetting;

    iget-boolean v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->clickFlag:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/simmelock/SIMPAddLockSetting;Z)Z
    .locals 0
    .param p0    # Lcom/android/simmelock/SIMPAddLockSetting;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->clickFlag:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/simmelock/SIMPAddLockSetting;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/simmelock/SIMPAddLockSetting;

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLockName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/16 v9, 0x81

    const/16 v8, 0x8

    const/4 v7, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030006

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->bundle:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->bundle:Landroid/os/Bundle;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->bundle:Landroid/os/Bundle;

    const-string v6, "LockCategory"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockCategory:I

    :cond_0
    iget v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockCategory:I

    if-ne v5, v7, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockCategory:I

    invoke-direct {p0, v5}, Lcom/android/simmelock/SIMPAddLockSetting;->getLockName(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockName:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->lockName:Ljava/lang/String;

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const v5, 0x7f070029

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    if-nez v5, :cond_2

    const-string v5, "X"

    const-string v6, "clocwork worked..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const v5, 0x7f070028

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->s1:Landroid/widget/Spinner;

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->s1:Landroid/widget/Spinner;

    if-nez v5, :cond_3

    const-string v5, "X"

    const-string v6, "clocwork worked..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/high16 v5, 0x7f050000

    const v6, 0x1090008

    invoke-static {p0, v5, v6}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    const v5, 0x1090009

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->s1:Landroid/widget/Spinner;

    invoke-virtual {v5, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v4, Lcom/android/simmelock/SIMPAddLockSetting$1;

    invoke-direct {v4, p0}, Lcom/android/simmelock/SIMPAddLockSetting$1;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->s1:Landroid/widget/Spinner;

    invoke-virtual {v5, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v5, 0x7f07002b

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwd:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwd:Landroid/widget/EditText;

    if-nez v5, :cond_4

    const-string v5, "X"

    const-string v6, "clocwork worked..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-static {v5, v8}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    const v5, 0x7f07002d

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    if-nez v5, :cond_5

    const-string v5, "X"

    const-string v6, "clocwork worked..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-static {v5, v8}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    const v5, 0x7f07002e

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    if-nez v2, :cond_6

    const-string v5, "X"

    const-string v6, "clocwork worked..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v5, Lcom/android/simmelock/SIMPAddLockSetting$2;

    invoke-direct {v5, p0}, Lcom/android/simmelock/SIMPAddLockSetting$2;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f07002f

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-nez v1, :cond_7

    const-string v5, "X"

    const-string v6, "clocwork worked..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    new-instance v5, Lcom/android/simmelock/SIMPAddLockSetting$3;

    invoke-direct {v5, p0}, Lcom/android/simmelock/SIMPAddLockSetting$3;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f06000f

    const v3, 0x7f06000c

    const v2, 0x1080027

    const/4 v1, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :pswitch_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060022

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SIMPAddLockSetting$4;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SIMPAddLockSetting$4;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060020

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SIMPAddLockSetting$5;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SIMPAddLockSetting$5;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060026

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SIMPAddLockSetting$6;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SIMPAddLockSetting$6;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060023

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SIMPAddLockSetting$7;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SIMPAddLockSetting$7;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060029

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SIMPAddLockSetting$8;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SIMPAddLockSetting$8;-><init>(Lcom/android/simmelock/SIMPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getSubscriberIdGemini(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIM1IMSI:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getSubscriberIdGemini(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIM2IMSI:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIM1IMSI:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "SIMMELOCK"

    const-string v2, "Fail to read SIM1 IMSI!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting;->msSIM2IMSI:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "SIMMELOCK"

    const-string v2, "Fail to read SIM2 IMSI!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    const-string v1, "SIMMELOCK"

    const-string v2, "Succeed to read SIM1 IMSI!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "SIMMELOCK"

    const-string v2, "Succeed to read SIM2 IMSI!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
