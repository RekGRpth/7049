.class public Lcom/android/simmelock/TabLockList;
.super Landroid/app/TabActivity;
.source "TabLockList.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# static fields
.field private static final DBG:Z = true

.field private static final INTENT_SIM1_INT_EXTRA:I = 0x0

.field private static final INTENT_SIM2_INT_EXTRA:I = 0x1

.field private static final TAB_SIM_1:I = 0x0

.field private static final TAB_SIM_2:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Gemini_Simme Lock"


# instance fields
.field private Sim1State:Z

.field private Sim2State:Z

.field private mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field private mTabHost:Landroid/widget/TabHost;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    iput-boolean v0, p0, Lcom/android/simmelock/TabLockList;->Sim1State:Z

    iput-boolean v0, p0, Lcom/android/simmelock/TabLockList;->Sim2State:Z

    return-void
.end method

.method private SetCurrentTab()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/simmelock/TabLockList;->Sim1State:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/simmelock/TabLockList;->Sim2State:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0
.end method

.method private SetupSIM1Tab()V
    .locals 6

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/android/simmelock/LockList;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "Setting SIM Number"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    iget-object v2, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    const-string v3, "SIM1"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const-string v3, "SIM1"

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    return-void
.end method

.method private SetupSIM2Tab()V
    .locals 6

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/android/simmelock/LockList;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "Setting SIM Number"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    iget-object v2, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    const-string v3, "SIM2"

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const-string v3, "SIM2"

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "Gemini_Simme Lock"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v0, p0, Lcom/android/simmelock/TabLockList;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/TabLockList;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/simmelock/TabLockList;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isRadioOnGemini(I)Z

    move-result v1

    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/simmelock/TabLockList;->Sim1State:Z

    iget-object v0, p0, Lcom/android/simmelock/TabLockList;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/simmelock/TabLockList;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isRadioOnGemini(I)Z

    move-result v1

    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/simmelock/TabLockList;->Sim2State:Z

    invoke-virtual {p0}, Landroid/app/TabActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v0

    iput-object v0, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    iget-object v0, p0, Lcom/android/simmelock/TabLockList;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    invoke-direct {p0}, Lcom/android/simmelock/TabLockList;->SetupSIM1Tab()V

    invoke-direct {p0}, Lcom/android/simmelock/TabLockList;->SetupSIM2Tab()V

    invoke-direct {p0}, Lcom/android/simmelock/TabLockList;->SetCurrentTab()V

    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/ActivityGroup;->getLocalActivityManager()Landroid/app/LocalActivityManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "Gemini_Simme Lock"

    const-string v2, "clocwork worked..."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    return-void
.end method
