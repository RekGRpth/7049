.class Lcom/android/simmelock/UnlockSetting$1;
.super Ljava/lang/Object;
.source "UnlockSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/simmelock/UnlockSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/UnlockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/UnlockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/16 v6, 0x78

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clickFlag: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v3}, Lcom/android/simmelock/UnlockSetting;->access$000(Lcom/android/simmelock/UnlockSetting;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$000(Lcom/android/simmelock/UnlockSetting;)Z

    move-result v0

    if-ne v0, v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$000(Lcom/android/simmelock/UnlockSetting;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0, v5}, Lcom/android/simmelock/UnlockSetting;->access$002(Lcom/android/simmelock/UnlockSetting;Z)Z

    :cond_1
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v0, v0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v0, v0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x8

    if-le v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-virtual {v0, v5}, Lcom/android/simmelock/UnlockSetting;->showAlertDialog(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v1, v1, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    const-string v3, "SIMNo"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[btnconfirm]intSIMNumber: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v3, v3, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    check-cast v8, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v0, v0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$100(Lcom/android/simmelock/UnlockSetting;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v8, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v1, v1, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    iget-object v3, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v3, v3, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$100(Lcom/android/simmelock/UnlockSetting;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v1, v1, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    iget-object v3, p0, Lcom/android/simmelock/UnlockSetting$1;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v3, v3, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0
.end method
