.class public Lcom/android/emailcommon/Configuration;
.super Ljava/lang/Object;
.source "Configuration.java"


# static fields
.field public static final EAS_DEFAULT_PORT:I = 0x50

.field public static final EAS_DEFAULT_SSL_PORT:I = 0x1bb

.field public static final EXCHANGE_CONFIGURATION_USE_ALTERNATE_STRINGS:Ljava/lang/String; = "com.android.email.EXCHANGE_CONFIGURATION_USE_ALTERNATE_STRINGS"

.field public static final IMAP_DEFAULT_PORT:I = 0x8f

.field public static final IMAP_DEFAULT_SSL_PORT:I = 0x3e1

.field public static final POP3_DEFAULT_PORT:I = 0x6e

.field public static final POP3_DEFAULT_SSL_PORT:I = 0x3e3

.field public static final SMTP_DEFAULT_PORT:I = 0x19

.field public static final SMTP_DEFAULT_SSL_PORT:I = 0x1d1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
