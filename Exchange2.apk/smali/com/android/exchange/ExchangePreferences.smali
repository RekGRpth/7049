.class public Lcom/android/exchange/ExchangePreferences;
.super Ljava/lang/Object;
.source "ExchangePreferences.java"


# static fields
.field private static final BAD_SYNC_KEY_MAILBOX_ID:Ljava/lang/String; = "badSyncKeyMailboxId"

.field private static final LOW_STORAGE:Ljava/lang/String; = "isLowStorage"

.field public static final PREFERENCES_FILE:Ljava/lang/String; = "AndroidExchange.Main"

.field private static final REMOVE_STALE_MAILS:Ljava/lang/String; = "isRemovedStaleMails"

.field private static sPreferences:Lcom/android/exchange/ExchangePreferences;

.field private static sStorageOkSize:J


# instance fields
.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/exchange/ExchangePreferences;->sStorageOkSize:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "AndroidExchange.Main"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static declared-synchronized getPreferences(Landroid/content/Context;)Lcom/android/exchange/ExchangePreferences;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/android/exchange/ExchangePreferences;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangePreferences;->sPreferences:Lcom/android/exchange/ExchangePreferences;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/exchange/ExchangePreferences;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangePreferences;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/exchange/ExchangePreferences;->sPreferences:Lcom/android/exchange/ExchangePreferences;

    :cond_0
    sget-object v0, Lcom/android/exchange/ExchangePreferences;->sPreferences:Lcom/android/exchange/ExchangePreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/exchange/ExchangePreferences;->getPreferences(Landroid/content/Context;)Lcom/android/exchange/ExchangePreferences;

    move-result-object v0

    iget-object v0, v0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method public checkLowStorage()V
    .locals 14

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v8, Landroid/os/StatFs;

    invoke-direct {v8, v9}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v10

    int-to-long v0, v10

    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v4, v10

    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockCount()I

    move-result v10

    int-to-long v2, v10

    mul-long v6, v0, v4

    const-wide/16 v10, 0x0

    sget-wide v12, Lcom/android/exchange/ExchangePreferences;->sStorageOkSize:J

    cmp-long v10, v10, v12

    if-nez v10, :cond_0

    mul-long v10, v2, v4

    const-wide/16 v12, 0xa

    div-long/2addr v10, v12

    sput-wide v10, Lcom/android/exchange/ExchangePreferences;->sStorageOkSize:J

    :cond_0
    sget-boolean v10, Lcom/android/emailcommon/Logging;->LOGD:Z

    if-eqz v10, :cond_1

    const-string v10, "Email"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "AvailableBlocks: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " BlockCount: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " BolckSize: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Data file system remaining size is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-wide/16 v12, 0x400

    div-long v12, v6, v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " KBytes"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sget-wide v10, Lcom/android/exchange/ExchangePreferences;->sStorageOkSize:J

    cmp-long v10, v6, v10

    if-lez v10, :cond_2

    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/android/exchange/ExchangePreferences;->setLowStorage(Z)V

    :goto_0
    return-void

    :cond_2
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/android/exchange/ExchangePreferences;->setLowStorage(Z)V

    goto :goto_0
.end method

.method public getBadSyncKeyMailboxId()J
    .locals 4

    iget-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "badSyncKeyMailboxId"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLowStorage()Z
    .locals 3

    iget-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "isLowStorage"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRemovedStaleMails()Z
    .locals 3

    iget-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "isRemovedStaleMails"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setBadSyncKeyMailboxId(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "badSyncKeyMailboxId"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setLowStorage(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isLowStorage"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setRemovedStaleMails(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/exchange/ExchangePreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isRemovedStaleMails"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
