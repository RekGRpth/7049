.class public Lcom/android/exchange/PartRequest;
.super Lcom/android/exchange/Request;
.source "PartRequest.java"


# instance fields
.field public final mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

.field public volatile mCancelled:Z

.field public final mContentUriString:Ljava/lang/String;

.field public final mDestination:Ljava/lang/String;

.field public final mLocation:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-wide v0, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    invoke-direct {p0, v0, v1}, Lcom/android/exchange/Request;-><init>(J)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/PartRequest;->mCancelled:Z

    iput-object p1, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v0, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/PartRequest;->mLocation:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/exchange/PartRequest;->mDestination:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/exchange/PartRequest;->mContentUriString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/android/exchange/PartRequest;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/android/exchange/PartRequest;

    iget-object v1, p1, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v1, v1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    iget-object v3, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v3, v3, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    long-to-int v0, v0

    return v0
.end method
