.class Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;
.super Ljava/lang/Object;
.source "FolderSyncParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/FolderSyncParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SyncOptions"
.end annotation


# instance fields
.field private final mInterval:I

.field private final mLookback:I


# direct methods
.method private constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mInterval:I

    iput p2, p0, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mLookback:I

    return-void
.end method

.method synthetic constructor <init>(IILcom/android/exchange/adapter/FolderSyncParser$1;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/android/exchange/adapter/FolderSyncParser$1;

    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;-><init>(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;)I
    .locals 1
    .param p0    # Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;

    iget v0, p0, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mInterval:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;)I
    .locals 1
    .param p0    # Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;

    iget v0, p0, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mLookback:I

    return v0
.end method
