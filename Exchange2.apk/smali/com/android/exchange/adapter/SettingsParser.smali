.class public Lcom/android/exchange/adapter/SettingsParser;
.super Lcom/android/exchange/adapter/Parser;
.source "SettingsParser.java"


# instance fields
.field private final mService:Lcom/android/exchange/EasSyncService;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .param p2    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lcom/android/exchange/adapter/SettingsParser;->mService:Lcom/android/exchange/EasSyncService;

    return-void
.end method


# virtual methods
.method public parse()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v2

    const/16 v3, 0x485

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    iget v2, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v3, 0x486

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->getValueInt()I

    move-result v1

    iget-object v2, p0, Lcom/android/exchange/adapter/SettingsParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v3, "Settings status = "

    invoke-virtual {v2, v3, v1}, Lcom/android/exchange/AbstractSyncService;->userLog(Ljava/lang/String;I)V

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v3, 0x496

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/android/exchange/adapter/SettingsParser;->parseDeviceInformation()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->skipTag()V

    goto :goto_0

    :cond_4
    return v0
.end method

.method public parseDeviceInformation()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    const/16 v0, 0x496

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v1, 0x488

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/exchange/adapter/SettingsParser;->parseSet()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->skipTag()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public parseSet()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    const/16 v0, 0x488

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v1, 0x486

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/exchange/adapter/SettingsParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v1, "Set status = "

    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->getValueInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/AbstractSyncService;->userLog(Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->skipTag()V

    goto :goto_0

    :cond_1
    return-void
.end method
