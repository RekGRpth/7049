.class public Lcom/android/exchange/provider/GalResult$GalData;
.super Ljava/lang/Object;
.source "GalResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/provider/GalResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GalData"
.end annotation


# static fields
.field public static final ALIAS:Ljava/lang/String; = "alias"

.field public static final COMPANY:Ljava/lang/String; = "company"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field public static final EMAIL_ADDRESS:Ljava/lang/String; = "emailAddress"

.field public static final FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final HOME_PHONE:Ljava/lang/String; = "homePhone"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final LAST_NAME:Ljava/lang/String; = "lastName"

.field public static final MOBILE_PHONE:Ljava/lang/String; = "mobilePhone"

.field public static final OFFICE:Ljava/lang/String; = "office"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final WORK_PHONE:Ljava/lang/String; = "workPhone"


# instance fields
.field public _id:J

.field builder:Lcom/android/emailcommon/mail/PackedString$Builder;

.field public displayName:Ljava/lang/String;

.field public emailAddress:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/emailcommon/mail/PackedString$Builder;

    invoke-direct {v0}, Lcom/android/emailcommon/mail/PackedString$Builder;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->builder:Lcom/android/emailcommon/mail/PackedString$Builder;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->_id:J

    return-void
.end method

.method private constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/emailcommon/mail/PackedString$Builder;

    invoke-direct {v0}, Lcom/android/emailcommon/mail/PackedString$Builder;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->builder:Lcom/android/emailcommon/mail/PackedString$Builder;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->_id:J

    const-string v0, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/provider/GalResult$GalData;->put(Ljava/lang/String;Ljava/lang/String;)V

    iput-wide p1, p0, Lcom/android/exchange/provider/GalResult$GalData;->_id:J

    const-string v0, "displayName"

    invoke-virtual {p0, v0, p3}, Lcom/android/exchange/provider/GalResult$GalData;->put(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/android/exchange/provider/GalResult$GalData;->displayName:Ljava/lang/String;

    const-string v0, "emailAddress"

    invoke-virtual {p0, v0, p4}, Lcom/android/exchange/provider/GalResult$GalData;->put(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/android/exchange/provider/GalResult$GalData;->emailAddress:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(JLjava/lang/String;Ljava/lang/String;Lcom/android/exchange/provider/GalResult$1;)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/android/exchange/provider/GalResult$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/exchange/provider/GalResult$GalData;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->builder:Lcom/android/emailcommon/mail/PackedString$Builder;

    invoke-virtual {v0, p1}, Lcom/android/emailcommon/mail/PackedString$Builder;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->builder:Lcom/android/emailcommon/mail/PackedString$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toPackedString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/exchange/provider/GalResult$GalData;->builder:Lcom/android/emailcommon/mail/PackedString$Builder;

    invoke-virtual {v0}, Lcom/android/emailcommon/mail/PackedString$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
