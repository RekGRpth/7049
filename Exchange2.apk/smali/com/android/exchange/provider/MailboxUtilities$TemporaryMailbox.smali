.class Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;
.super Ljava/lang/Object;
.source "MailboxUtilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/provider/MailboxUtilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TemporaryMailbox"
.end annotation


# instance fields
.field mFlags:I

.field mId:J

.field mParentKey:J

.field mParentServerId:Ljava/lang/String;

.field mServerId:Ljava/lang/String;

.field mType:I


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JII)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # I
    .param p8    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;->mId:J

    iput-object p3, p0, Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;->mServerId:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;->mParentServerId:Ljava/lang/String;

    iput-wide p5, p0, Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;->mParentKey:J

    iput p7, p0, Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;->mType:I

    iput p8, p0, Lcom/android/exchange/provider/MailboxUtilities$TemporaryMailbox;->mFlags:I

    return-void
.end method
