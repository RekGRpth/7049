.class public Lcom/android/exchange/service/EmailExternalEas;
.super Lcom/android/exchange/EasSyncService;
.source "EmailExternalEas.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmailExternalEas"


# instance fields
.field private mCallback:Lcom/android/emailcommon/service/EmailExternalCalls;

.field private mInputStream:Ljava/io/InputStream;

.field private mSaveInSent:Z

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/Mailbox;Landroid/net/Uri;Lcom/android/emailcommon/service/EmailExternalCalls;Z)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/emailcommon/provider/Mailbox;
    .param p3    # Landroid/net/Uri;
    .param p4    # Lcom/android/emailcommon/service/EmailExternalCalls;
    .param p5    # Z

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/Mailbox;)V

    iput-object v1, p0, Lcom/android/exchange/service/EmailExternalEas;->mInputStream:Ljava/io/InputStream;

    iput-object v1, p0, Lcom/android/exchange/service/EmailExternalEas;->mCallback:Lcom/android/emailcommon/service/EmailExternalCalls;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/service/EmailExternalEas;->mSaveInSent:Z

    iput-object v1, p0, Lcom/android/exchange/service/EmailExternalEas;->mUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lcom/android/exchange/service/EmailExternalEas;->mUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/android/exchange/service/EmailExternalEas;->mCallback:Lcom/android/emailcommon/service/EmailExternalCalls;

    iput-boolean p5, p0, Lcom/android/exchange/service/EmailExternalEas;->mSaveInSent:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v1, 0x1

    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->setupService()Z

    invoke-virtual {p0}, Lcom/android/exchange/service/EmailExternalEas;->sendMessage()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ": sync finished"

    aput-object v3, v2, v5

    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/exchange/AbstractSyncService;->userLog([Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {p0, v2, v1}, Lcom/android/exchange/service/EmailExternalEas;->sendCallback(II)V

    const-string v2, "Exception caught in EmailExternalEas"

    invoke-virtual {p0, v2, v0}, Lcom/android/exchange/AbstractSyncService;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ": sync finished"

    aput-object v3, v2, v5

    goto :goto_0

    :catchall_0
    move-exception v2

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, ": sync finished"

    aput-object v4, v3, v5

    invoke-virtual {p0, v3}, Lcom/android/exchange/AbstractSyncService;->userLog([Ljava/lang/String;)V

    throw v2
.end method

.method public sendCallback(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    iget-object v3, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iget-wide v0, v3, Lcom/android/emailcommon/provider/Mailbox;->mAccountKey:J

    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/service/EmailExternalEas;->mCallback:Lcom/android/emailcommon/service/EmailExternalCalls;

    invoke-interface {v3, p1, v0, v1, p2}, Lcom/android/emailcommon/service/EmailExternalCalls;->sendCallback(IJI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v3, "EmailExternalEas"

    const-string v4, "RemoteException in sendCallback method"

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendMessage()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Start send message ... for Uri "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/exchange/service/EmailExternalEas;->mUri:Landroid/net/Uri;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v11, p0, Lcom/android/exchange/service/EmailExternalEas;->mUri:Landroid/net/Uri;

    invoke-virtual {v10, v11}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v10

    iput-object v10, p0, Lcom/android/exchange/service/EmailExternalEas;->mInputStream:Ljava/io/InputStream;

    iget-object v10, p0, Lcom/android/exchange/service/EmailExternalEas;->mInputStream:Ljava/io/InputStream;

    if-nez v10, :cond_1

    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Send Message Failed in sendMessage() method , Can\'t get InputStream from the given uri: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/exchange/service/EmailExternalEas;->mUri:Landroid/net/Uri;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x1

    invoke-virtual {p0, v7, v8}, Lcom/android/exchange/service/EmailExternalEas;->sendCallback(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v10, p0, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v10, "eas_"

    const-string v11, "tmp"

    invoke-static {v10, v11, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/service/EmailExternalEas;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {p0, v10, v9}, Lcom/android/exchange/service/EmailExternalEas;->writToFile(Ljava/io/InputStream;Ljava/io/File;)V

    :try_start_0
    iget-object v10, p0, Lcom/android/exchange/service/EmailExternalEas;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    :try_start_1
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v5, Lorg/apache/http/entity/InputStreamEntity;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-direct {v5, v4, v10, v11}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    const-string v1, "SendMail&SaveInSent=F"

    iget-boolean v10, p0, Lcom/android/exchange/service/EmailExternalEas;->mSaveInSent:Z

    if-eqz v10, :cond_2

    const-string v1, "SendMail&SaveInSent=T"

    :cond_2
    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Send cmd: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const v10, 0xdbba0

    invoke-virtual {p0, v1, v5, v10}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v2

    const/16 v10, 0xc8

    if-ne v2, v10, :cond_3

    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EAS Message sending success, code:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/android/exchange/service/EmailExternalEas;->sendCallback(II)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v10, "EmailExternalEas"

    const-string v11, "Closes inputstream fail."

    invoke-static {v10, v11, v3}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    :try_start_2
    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EAS Message sending failed, code:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v7, 0x1

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    :goto_2
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_4
    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EAS send Message feedback result = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " resultType = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v7, v8}, Lcom/android/exchange/service/EmailExternalEas;->sendCallback(II)V

    goto/16 :goto_0

    :catch_1
    move-exception v3

    :try_start_3
    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EAS SendMessage FileNotFoundException "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v7, 0x1

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    goto :goto_2

    :catch_2
    move-exception v3

    :try_start_4
    const-string v10, "EmailExternalEas"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EAS SendMessage Exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v7, 0x1

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    goto :goto_2

    :catchall_0
    move-exception v10

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_5
    throw v10
.end method

.method writToFile(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 5
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/BufferedOutputStream;

    const/16 v4, 0x400

    invoke-direct {v2, v0, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/io/OutputStreamWriter;->write(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    return-void
.end method
