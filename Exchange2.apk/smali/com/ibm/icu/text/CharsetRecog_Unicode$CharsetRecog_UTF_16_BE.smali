.class Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_16_BE;
.super Lcom/ibm/icu/text/CharsetRecog_Unicode;
.source "CharsetRecog_Unicode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CharsetRecog_Unicode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CharsetRecog_UTF_16_BE"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/ibm/icu/text/CharsetRecog_Unicode;-><init>()V

    return-void
.end method


# virtual methods
.method getName()Ljava/lang/String;
    .locals 1

    const-string v0, "UTF-16BE"

    return-object v0
.end method

.method match(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 4
    .param p1    # Lcom/ibm/icu/text/CharsetDetector;

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    array-length v2, v0

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    aget-byte v2, v0, v1

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xfe

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xff

    if-ne v2, v3, :cond_0

    const/16 v1, 0x64

    :cond_0
    return v1
.end method
