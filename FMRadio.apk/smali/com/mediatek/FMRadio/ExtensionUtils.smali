.class public Lcom/mediatek/FMRadio/ExtensionUtils;
.super Ljava/lang/Object;
.source "ExtensionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getExtension(Landroid/content/Context;)Lcom/mediatek/FMRadio/ext/IProjectStringExt;
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v7, 0x0

    const-class v5, Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Landroid/content/pm/Signature;

    invoke-static {p0, v5, v6}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v7}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {v3}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/FMRadio/ext/IProjectStringExt;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v2, v1

    :goto_1
    return-object v2

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/FMRadio/ext/DefaultProjectStringExt;

    invoke-direct {v1}, Lcom/mediatek/FMRadio/ext/DefaultProjectStringExt;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/mediatek/FMRadio/ext/DefaultProjectStringExt;

    invoke-direct {v1}, Lcom/mediatek/FMRadio/ext/DefaultProjectStringExt;-><init>()V

    :goto_2
    move-object v2, v1

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/mediatek/FMRadio/ext/DefaultProjectStringExt;

    invoke-direct {v1}, Lcom/mediatek/FMRadio/ext/DefaultProjectStringExt;-><init>()V

    goto :goto_2
.end method
