.class Lcom/mediatek/FMRadio/FMRadioActivity$1;
.super Ljava/lang/Object;
.source "FMRadioActivity.java"

# interfaces
.implements Lcom/mediatek/FMRadio/FMRadioListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/FMRadioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFmExit()V
    .locals 2

    const/16 v1, 0xb

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onPowerUpChanged(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x6

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$002(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onPsChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$302(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onRdsStationChanged(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$202(Lcom/mediatek/FMRadio/FMRadioActivity;I)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onRecordError(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x8

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onRecordModeChanged(Z)V
    .locals 5
    .param p1    # Z

    const/16 v4, 0x9

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/4 v3, -0x1

    invoke-virtual {v2, v4, v1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onRecordStateChanged(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x7

    const-string v1, "FmRx/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FMRadioActivity.onRecordStateChanged : recordState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v4, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onRtChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$402(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$1;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
