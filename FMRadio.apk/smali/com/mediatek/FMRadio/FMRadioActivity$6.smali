.class Lcom/mediatek/FMRadio/FMRadioActivity$6;
.super Landroid/os/Handler;
.source "FMRadioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/FMRadioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x1

    const-string v5, "FmRx/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mHandler.handleMessage: what = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    const-string v5, "FmRx/Activity"

    const-string v6, "invalid message"

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v5, "FmRx/Activity"

    const-string v6, "handleMessage"

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_1
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4500(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;

    move-result-object v6

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v5

    if-eqz v5, :cond_1

    const v5, 0x7f020036

    :goto_1
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5200(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4700(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4500(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5300(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-virtual {v5, v3}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshPopupMenu(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v5, "FmRx/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Performance test][FMRadio] power up end ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "FmRx/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Performance test][FMRadio] power down end ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "FmRx/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateFMState: FMRadio is powerup = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v7}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    const v5, 0x7f020026

    goto/16 :goto_1

    :pswitch_2
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5400(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3200(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/FMRadioStation;->setCurrentStation(Landroid/content/Context;I)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5500(Lcom/mediatek/FMRadio/FMRadioActivity;I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5200(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4500(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5300(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-virtual {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->showNoAntennaDialog()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5300(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_6
    iget v4, p1, Landroid/os/Message;->arg1:I

    const-string v5, "FmRx/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FMRadioActivity.mHandler: recorderState = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5, v4}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5600(Lcom/mediatek/FMRadio/FMRadioActivity;I)V

    goto/16 :goto_0

    :pswitch_7
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5700(Lcom/mediatek/FMRadio/FMRadioActivity;I)V

    goto/16 :goto_0

    :pswitch_8
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-lez v5, :cond_2

    :goto_2
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5, v3}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5800(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V

    goto/16 :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_9
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5900(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/PopupMenu;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5900(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/PopupMenu;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v5

    const v6, 0x7f060046

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v6

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5900(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/PopupMenu;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v5

    const v6, 0x7f060048

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v6}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v6

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :pswitch_a
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
