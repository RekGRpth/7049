.class Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;
.super Ljava/lang/Thread;
.source "FMRadioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/FMRadioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InitialThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "FmRx/Activity"

    const-string v2, "opendev succeed."

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4200(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4102(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$1500(Lcom/mediatek/FMRadio/FMRadioActivity;)Lcom/mediatek/FMRadio/FMRadioService;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mediatek/FMRadio/FMRadioService;->switchAntenna(I)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "FmRx/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to switch to short antenna: errorcode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "FmRx/Activity"

    const-string v2, "Antenna is unavailable. Ask if plug in antenna."

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1, v4}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5102(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/FMRadio/FMRadioUtils;->computeFrequency(I)F

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6200(Lcom/mediatek/FMRadio/FMRadioActivity;F)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/FMRadio/FMRadioUtils;->computeFrequency(I)F

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6200(Lcom/mediatek/FMRadio/FMRadioActivity;F)Z

    goto :goto_0

    :cond_2
    const-string v1, "FmRx/Activity"

    const-string v2, "Error: opendev failed."

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
