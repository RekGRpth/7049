.class Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;
.super Landroid/os/AsyncTask;
.source "FMRadioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/FMRadioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayFMAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p2    # Lcom/mediatek/FMRadio/FMRadioActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1    # [Ljava/lang/Void;

    const-string v0, "FmRx/Activity"

    const-string v1, "doInBackground: Play FM"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I

    move-result v1

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioUtils;->computeFrequency(I)F

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6200(Lcom/mediatek/FMRadio/FMRadioActivity;F)Z

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1    # Ljava/lang/Void;

    const/4 v2, 0x1

    const-string v0, "FmRx/Activity"

    const-string v1, "enable all other buttons!"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$5200(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3700(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-virtual {v0, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshPopupMenu(Z)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "disable all other buttons and start animation when start play FM!"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3600(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3700(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V

    return-void
.end method
