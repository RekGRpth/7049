.class Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;
.super Landroid/os/AsyncTask;
.source "FMRadioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/FMRadioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[I>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p2    # Lcom/mediatek/FMRadio/FMRadioActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->doInBackground([Ljava/lang/Void;)[I

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[I
    .locals 13
    .param p1    # [Ljava/lang/Void;

    const/4 v12, 0x1

    const/4 v11, 0x0

    const-string v8, "FmRx/Activity"

    const-string v9, "doInBackground: begin scan"

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v8, v12}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4302(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v8}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3200(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/FMRadio/FMRadioStation;->cleanSearchedStations(Landroid/content/Context;)V

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v8}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$7000(Lcom/mediatek/FMRadio/FMRadioActivity;)[I

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1}, Ljava/util/Arrays;->sort([I)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    const v9, 0x7f040015

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    :goto_0
    array-length v8, v1

    if-ge v5, v8, :cond_3

    aget v8, v1, v5

    invoke-static {v8}, Lcom/mediatek/FMRadio/FMRadioUtils;->isValidStation(I)Z

    move-result v8

    if-eqz v8, :cond_2

    if-nez v4, :cond_0

    aget v4, v1, v5

    :cond_0
    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v8}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3200(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/content/Context;

    move-result-object v8

    aget v9, v1, v5

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/FMRadioStation;->isFavoriteStation(Landroid/content/Context;I)Z

    move-result v8

    if-nez v8, :cond_1

    sget-object v8, Lcom/mediatek/FMRadio/FMRadioStation$Station;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v9, "COLUMN_STATION_NAME"

    invoke-virtual {v8, v9, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v9, "COLUMN_STATION_FREQ"

    aget v10, v1, v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v9, "COLUMN_STATION_TYPE"

    const/4 v10, 0x3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    :try_start_0
    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v8}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$3200(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "com.mediatek.FMRadio.FMRadioContentProvider"

    invoke-virtual {v8, v9, v6}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_4
    :goto_1
    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4302(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z

    const-string v8, "FmRx/Activity"

    const-string v9, "doInBackground: end scan"

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x2

    new-array v7, v8, [I

    aput v4, v7, v11

    aput v0, v7, v12

    return-object v7

    :catch_0
    move-exception v3

    const-string v8, "FmRx/Activity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception when applyBatch searched stations "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v8, "FmRx/Activity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception when applyBatch searched stations "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onCancelled()V
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "onCancelled: dismiss search progress dialog and stop scan!"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$7200(Lcom/mediatek/FMRadio/FMRadioActivity;Z)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$7300(Lcom/mediatek/FMRadio/FMRadioActivity;Z)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$1500(Lcom/mediatek/FMRadio/FMRadioActivity;)Lcom/mediatek/FMRadio/FMRadioService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->resumeFMAudio()V

    :cond_0
    const-string v0, "FmRx/Activity"

    const-string v1, "onCancelled"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [I

    invoke-virtual {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->onPostExecute([I)V

    return-void
.end method

.method protected onPostExecute([I)V
    .locals 8
    .param p1    # [I

    const/4 v7, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v4, "FmRx/Activity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Performance test][FMRadio] scan channel end ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6900()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "FmRx/Activity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onPostExecute: mIsStopScanCalled: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6900()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    aget v3, p1, v4

    aget v0, p1, v7

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v4, v7}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6802(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z

    if-nez v3, :cond_1

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v4}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    const v6, 0x7f040023

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$700(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/CharSequence;)V

    :goto_1
    const-string v4, "FmRx/Activity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onPostExecute: search finished, tune to station : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v4, v3}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$2400(Lcom/mediatek/FMRadio/FMRadioActivity;I)V

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v4}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$4400(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-static {v4}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$7100(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    const v7, 0x7f040022

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$700(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->access$6902(Z)Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;->this$0:Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->showSearchDialog()V

    return-void
.end method
