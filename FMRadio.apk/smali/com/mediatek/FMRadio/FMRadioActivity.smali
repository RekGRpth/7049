.class public Lcom/mediatek/FMRadio/FMRadioActivity;
.super Landroid/app/Activity;
.source "FMRadioActivity.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnDismissListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;
.implements Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;
.implements Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;
.implements Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;,
        Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;,
        Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;,
        Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;,
        Lcom/mediatek/FMRadio/FMRadioActivity$InitialThread;
    }
.end annotation


# static fields
.field private static final MSGID_ANTENNA_UNAVAILABE:I = 0x3

.field private static final MSGID_FM_EXIT:I = 0xb

.field private static final MSGID_OPEN_DEVICE:I = 0xa

.field private static final MSGID_POWERUP:I = 0x6

.field private static final MSGID_RECORD_ERROR:I = 0x8

.field private static final MSGID_RECORD_MODE_CHANED:I = 0x9

.field private static final MSGID_RECORD_STATE_CHANGED:I = 0x7

.field private static final MSGID_REFRESH:I = 0x4

.field private static final MSGID_SHOW_TOAST_FINISH:I = 0x5

.field private static final MSGID_UPDATE_CURRENT_STATION:I = 0x2

.field private static final MSGID_UPDATE_RDS:I = 0x1

.field private static final NO_ANTENNA:Ljava/lang/String; = "NoAntenna"

.field private static final RDS_SETTING:Ljava/lang/String; = "RdsSetting"

.field private static final REFS_NAME:Ljava/lang/String; = "FMRecord"

.field private static final REQUEST_CODE_FAVORITE:I = 0x1

.field private static final SAVE_RECORDINGD:Ljava/lang/String; = "SaveRecording"

.field private static final SEARCH:Ljava/lang/String; = "Search"

.field private static final START_PLAY_TIME:Ljava/lang/String; = "startPlayTime"

.field private static final START_RECORD_TIME:Ljava/lang/String; = "startRecordTime"

.field public static final TAG:Ljava/lang/String; = "FmRx/Activity"

.field private static final TOAST_TIMER_DELAY:J = 0x7d0L

.field private static sFragmentManager:Landroid/app/FragmentManager;

.field private static sIsStopScanCalled:Z


# instance fields
.field private mAnimImage:Landroid/widget/ImageView;

.field private mAnimation:Landroid/view/animation/Animation;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonAddToFavorite:Landroid/widget/ImageButton;

.field private mButtonClickListener:Landroid/view/View$OnClickListener;

.field private mButtonDecrease:Landroid/widget/ImageButton;

.field private mButtonIncrease:Landroid/widget/ImageButton;

.field private mButtonNextStation:Landroid/widget/ImageButton;

.field private mButtonPlayStop:Landroid/widget/ImageButton;

.field private mButtonPlayback:Landroid/widget/ImageButton;

.field private mButtonPrevStation:Landroid/widget/ImageButton;

.field private mButtonRecord:Landroid/widget/ImageButton;

.field private mButtonStop:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mCurrentStation:I

.field private mFMRadioListener:Lcom/mediatek/FMRadio/FMRadioListener;

.field private mHandler:Landroid/os/Handler;

.field private mIsAFEnabled:Z

.field private mIsActivityForeground:Z

.field private mIsExitPressed:Z

.field private mIsFreshRecordingStatus:Z

.field private mIsInRecordingMode:Z

.field private mIsInitialDone:Z

.field private mIsMenuItemChanged:Z

.field private mIsPSRTEnabled:Z

.field private mIsPlaying:Z

.field private mIsRDSSupported:Z

.field private mIsRecordingCardEject:Z

.field private mIsSDListenerRegistered:Z

.field private mIsSearchFinishToTune:Z

.field private mIsSearching:Z

.field private mIsSeeking:Z

.field private mIsServiceBinded:Z

.field private mIsServiceStarted:Z

.field private mIsUserCancelSearch:Z

.field private mLRTextString:Ljava/lang/String;

.field private mMenuItemChannelList:Landroid/view/MenuItem;

.field private mMenuItemOverflow:Landroid/view/MenuItem;

.field private mPSString:Ljava/lang/String;

.field mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

.field private mPlayStartTime:J

.field private mPopupMenu:Landroid/widget/PopupMenu;

.field private mPrevRecorderState:I

.field private mProjectStringExt:Lcom/mediatek/FMRadio/ext/IProjectStringExt;

.field private mRLRecordInfo:Landroid/widget/RelativeLayout;

.field private mRecordStartTime:J

.field private mRecordState:I

.field private mRefresher:Landroid/os/Handler;

.field private mSDDirectory:Ljava/lang/String;

.field private mSDListener:Landroid/content/BroadcastReceiver;

.field mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

.field mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

.field private mService:Lcom/mediatek/FMRadio/FMRadioService;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mTextFM:Landroid/widget/TextView;

.field private mTextMHz:Landroid/widget/TextView;

.field private mTextRDS:Landroid/widget/TextView;

.field private mTextStationName:Landroid/widget/TextView;

.field private mTextStationValue:Landroid/widget/TextView;

.field private mToast:Landroid/widget/Toast;

.field mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

.field private mTxtRecInfoLeft:Landroid/widget/TextView;

.field private mTxtRecInfoRight:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/FMRadio/FMRadioActivity;->sIsStopScanCalled:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceStarted:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceBinded:Z

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsUserCancelSearch:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsExitPressed:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPSString:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mLRTextString:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSearching:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSeeking:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSearchFinishToTune:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInitialDone:Z

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mToast:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextRDS:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextFM:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextMHz:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonDecrease:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPrevStation:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonNextStation:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonIncrease:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mMenuItemChannelList:Landroid/view/MenuItem;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mMenuItemOverflow:Landroid/view/MenuItem;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRLRecordInfo:Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoRight:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimation:Landroid/view/animation/Animation;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPrevRecorderState:I

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDDirectory:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRecordingCardEject:Z

    const/16 v0, 0x38f

    iput v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAudioManager:Landroid/media/AudioManager;

    iput-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsActivityForeground:Z

    iput v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsFreshRecordingStatus:Z

    iput-wide v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordStartTime:J

    iput-wide v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayStartTime:J

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mProjectStringExt:Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRadioActivity$1;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mFMRadioListener:Lcom/mediatek/FMRadio/FMRadioListener;

    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRadioActivity$2;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$3;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRadioActivity$3;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRadioActivity$4;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$5;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRadioActivity$5;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$6;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRadioActivity$6;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/FMRadio/FMRadioActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    return v0
.end method

.method static synthetic access$1100(Lcom/mediatek/FMRadio/FMRadioActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-wide v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordStartTime:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/mediatek/FMRadio/FMRadioActivity;J)J
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordStartTime:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/mediatek/FMRadio/FMRadioActivity;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isHaveAvailableStorage()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/mediatek/FMRadio/FMRadioActivity;)Lcom/mediatek/FMRadio/FMRadioService;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioService;)Lcom/mediatek/FMRadio/FMRadioService;
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Lcom/mediatek/FMRadio/FMRadioService;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/mediatek/FMRadio/FMRadioActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-wide v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayStartTime:J

    return-wide v0
.end method

.method static synthetic access$1602(Lcom/mediatek/FMRadio/FMRadioActivity;J)J
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayStartTime:J

    return-wide p1
.end method

.method static synthetic access$1700(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoRight:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/FMRadio/FMRadioActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    return v0
.end method

.method static synthetic access$2002(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDDirectory:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/mediatek/FMRadio/FMRadioActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    return p1
.end method

.method static synthetic access$2100(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/String;J)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/FMRadio/FMRadioActivity;->editSharedPreferences(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->updateFavoriteStation()V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/FMRadio/FMRadioActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->tuneToStation(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/mediatek/FMRadio/FMRadioActivity;IZ)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/FMRadio/FMRadioActivity;->seekStation(IZ)V

    return-void
.end method

.method static synthetic access$2600(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onPauseFM()V

    return-void
.end method

.method static synthetic access$2700(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onPlayFM()V

    return-void
.end method

.method static synthetic access$2800(Lcom/mediatek/FMRadio/FMRadioActivity;)Lcom/mediatek/FMRadio/FMRadioListener;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mFMRadioListener:Lcom/mediatek/FMRadio/FMRadioListener;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isServiceInit()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/mediatek/FMRadio/FMRadioActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->initService(I)V

    return-void
.end method

.method static synthetic access$302(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPSString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    return p1
.end method

.method static synthetic access$3400(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->enablePSRT(Z)V

    return-void
.end method

.method static synthetic access$3500(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->enableAF(Z)V

    return-void
.end method

.method static synthetic access$3600(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->startAnimation()V

    return-void
.end method

.method static synthetic access$3700(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshButtonOnGoing(Z)V

    return-void
.end method

.method static synthetic access$3800(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isDeviceOpen()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->updateCurrentStation()V

    return-void
.end method

.method static synthetic access$4000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isPowerUp()Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mLRTextString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    return v0
.end method

.method static synthetic access$4102(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isRDSSupported()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4300(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSearching:Z

    return v0
.end method

.method static synthetic access$4302(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSearching:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->dismissSearchDialog()V

    return-void
.end method

.method static synthetic access$4500(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSeeking:Z

    return v0
.end method

.method static synthetic access$4602(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSeeking:Z

    return p1
.end method

.method static synthetic access$4700(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshButtonStatus(Z)V

    return-void
.end method

.method static synthetic access$4800(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->updateRds()V

    return-void
.end method

.method static synthetic access$4900(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->changeRecordingMode(Z)V

    return-void
.end method

.method static synthetic access$500()Landroid/app/FragmentManager;
    .locals 1

    sget-object v0, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/mediatek/FMRadio/FMRadioActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshRecordingStatus(I)V

    return-void
.end method

.method static synthetic access$5100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInitialDone:Z

    return v0
.end method

.method static synthetic access$5102(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInitialDone:Z

    return p1
.end method

.method static synthetic access$5200(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->stopAnimation()V

    return-void
.end method

.method static synthetic access$5300(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->showRDS()V

    return-void
.end method

.method static synthetic access$5500(Lcom/mediatek/FMRadio/FMRadioActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshStationUI(I)V

    return-void
.end method

.method static synthetic access$5600(Lcom/mediatek/FMRadio/FMRadioActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->updateRecordingState(I)V

    return-void
.end method

.method static synthetic access$5700(Lcom/mediatek/FMRadio/FMRadioActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->updateRecorderError(I)V

    return-void
.end method

.method static synthetic access$5800(Lcom/mediatek/FMRadio/FMRadioActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->exitRecordingMode(Z)V

    return-void
.end method

.method static synthetic access$5900(Lcom/mediatek/FMRadio/FMRadioActivity;)Landroid/widget/PopupMenu;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->dismissSaveRecordingDialog()V

    return-void
.end method

.method static synthetic access$6000(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->openDevice()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6100(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isAntennaAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6200(Lcom/mediatek/FMRadio/FMRadioActivity;F)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->powerUp(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$6700(Lcom/mediatek/FMRadio/FMRadioActivity;F)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->tune(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$6800(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSearchFinishToTune:Z

    return v0
.end method

.method static synthetic access$6802(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSearchFinishToTune:Z

    return p1
.end method

.method static synthetic access$6900()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/FMRadio/FMRadioActivity;->sIsStopScanCalled:Z

    return v0
.end method

.method static synthetic access$6902(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/FMRadio/FMRadioActivity;->sIsStopScanCalled:Z

    return p0
.end method

.method static synthetic access$700(Lcom/mediatek/FMRadio/FMRadioActivity;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->showToast(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$7000(Lcom/mediatek/FMRadio/FMRadioActivity;)[I
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->startScan()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7100(Lcom/mediatek/FMRadio/FMRadioActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->enterChannelList()V

    return-void
.end method

.method static synthetic access$7200(Lcom/mediatek/FMRadio/FMRadioActivity;Z)I
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->setRDS(Z)I

    move-result v0

    return v0
.end method

.method static synthetic access$7300(Lcom/mediatek/FMRadio/FMRadioActivity;Z)I
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->setMute(Z)I

    move-result v0

    return v0
.end method

.method static synthetic access$7400(Lcom/mediatek/FMRadio/FMRadioActivity;Z)I
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->rdsSet(Z)I

    move-result v0

    return v0
.end method

.method static synthetic access$7500(Lcom/mediatek/FMRadio/FMRadioActivity;FZ)F
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # F
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/FMRadio/FMRadioActivity;->seek(FZ)F

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRecordingCardEject:Z

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRecordingCardEject:Z

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/FMRadio/FMRadioActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/FMRadio/FMRadioActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRadioActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    return p1
.end method

.method private changeRecordingMode(Z)V
    .locals 9
    .param p1    # Z

    const/16 v5, 0x8

    const/4 v6, 0x0

    const-string v4, "FmRx/Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "changeRecordingMode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-ne v4, p1, :cond_1

    const-string v5, "FmRx/Activity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FM already "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz p1, :cond_0

    const-string v4, "in"

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "recording mode!"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    const-string v4, "NOT in"

    goto :goto_0

    :cond_1
    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    if-nez v4, :cond_2

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "file"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    :cond_2
    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v4, p1}, Lcom/mediatek/FMRadio/FMRadioService;->setRecordingMode(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    if-eqz p1, :cond_3

    const v4, 0x7f04002c

    :goto_2
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setTitle(I)V

    const v4, 0x7f060036

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    const v4, 0x7f060030

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    if-eqz p1, :cond_4

    move v4, v5

    :goto_3
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_5

    move v4, v6

    :goto_4
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    if-eqz p1, :cond_6

    :goto_5
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/high16 v4, 0x7f040000

    goto :goto_2

    :cond_4
    move v4, v6

    goto :goto_3

    :cond_5
    move v4, v5

    goto :goto_4

    :cond_6
    move v5, v6

    goto :goto_5
.end method

.method private dismissSaveRecordingDialog()V
    .locals 4

    sget-object v2, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const-string v3, "SaveRecording"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_0
    return-void
.end method

.method private dismissSearchDialog()V
    .locals 3

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "Search"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    return-void
.end method

.method private editSharedPreferences(Ljava/lang/String;J)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const-string v2, "FMRecord"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private enableAF(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->enableAF(Z)V

    :cond_0
    return-void
.end method

.method private enablePSRT(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->enablePSRT(Z)V

    :cond_0
    return-void
.end method

.method private enterChannelList()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/mediatek/FMRadio/FMRadioFavorite;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private exitRecordingMode(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    invoke-direct {p0, v3}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshButtonStatus(Z)V

    if-nez p1, :cond_1

    iput-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    iget-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    const/high16 v3, 0x7f040000

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    const v3, 0x7f060036

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const v3, 0x7f060030

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_1
    return-void
.end method

.method private exitService()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceBinded:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    iput-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceBinded:Z

    :cond_0
    iget-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsExitPressed:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceStarted:Z

    if-eqz v1, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/FMRadio/FMRadioService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "FmRx/Activity"

    const-string v2, "Error: Cannot stop the FM service."

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iput-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceStarted:Z

    :cond_2
    return-void
.end method

.method private getFrequency()I
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.getFrequency"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->getFrequency()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getLRText()Ljava/lang/String;
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.getLRText"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->getLRText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPS()Ljava/lang/String;
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.getPS"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->getPS()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTimeString(I)Ljava/lang/String;
    .locals 14
    .param p1    # I

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/16 v3, 0xe10

    div-int/lit16 v0, p1, 0xe10

    const/16 v2, 0x3c

    div-int/lit8 v8, p1, 0x3c

    rem-int/lit8 v1, v8, 0x3c

    rem-int/lit8 v4, p1, 0x3c

    const/4 v7, 0x0

    if-lez v0, :cond_0

    const-string v5, "%02d:%02d:%02d"

    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v13

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_0
    const-string v6, "%02d:%02d"

    const-string v8, "%02d:%02d"

    new-array v9, v13, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method private initService(I)V
    .locals 3
    .param p1    # I

    const-string v0, "FmRx/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FMRadioActivity.initService: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->initService(I)V

    :cond_0
    return-void
.end method

.method private isAFEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isAFEnabled()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAntennaAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method private isDeviceOpen()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.isDeviceOpen"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isDeviceOpen()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEarphoneUsed()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.isEarphoneUsed"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isEarphoneUsed()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isHaveAvailableStorage()Z
    .locals 10

    new-instance v4, Landroid/os/StatFs;

    iget-object v7, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDDirectory:Ljava/lang/String;

    invoke-direct {v4, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v7

    int-to-long v2, v7

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v0, v7

    mul-long v5, v2, v0

    const-string v7, "FmRx/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkRemainingStorage: available space="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/32 v7, 0x80000

    cmp-long v7, v5, v7

    if-lez v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    return v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private isPSRTEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isPSRTEnabled()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPowerUp()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.isPowerUp"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isPowerUp()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRDSSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isRDSSupported()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRecordFileExist()Z
    .locals 5

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v3}, Lcom/mediatek/FMRadio/FMRadioService;->getRecordingName()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDDirectory:Ljava/lang/String;

    const-string v4, "FM Recording"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".3gpp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private isServiceInit()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.isServiceInit"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isServiceInit()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPauseFM()V
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "start onPauseFM"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->powerDown()Z

    const-string v0, "FmRx/Activity"

    const-string v1, "end onPauseFM"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onPlayFM()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isAntennaAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040024

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "FmRx/Activity"

    const-string v1, "start onPlayFM"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    :cond_1
    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    invoke-direct {v0, p0, v3}, Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayFMAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$PlayFMAsyncTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const-string v0, "FmRx/Activity"

    const-string v1, "end onPlayFM"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onUseEarphone()V
    .locals 5

    const-string v2, "FmRx/Activity"

    const-string v3, "onUseEarphone"

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->useEarphone(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "FmRx/Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Performance test][FMRadio] switch earphone end ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onUseLoudspeaker()V
    .locals 5

    const-string v2, "FmRx/Activity"

    const-string v3, "onUseLoudspeaker"

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->useEarphone(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "FmRx/Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Performance test][FMRadio] switch speaker end ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private openDevice()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.openDevice"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->openDevice()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private powerDown()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.powerDown"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->powerDown()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private powerUp(F)Z
    .locals 2
    .param p1    # F

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.powerUp"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->powerUp(F)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rdsSet(Z)I
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->setRDS(Z)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private refreshButtonOnGoing(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshCommonButton(Z)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshPopupMenu(Z)V

    return-void
.end method

.method private refreshButtonStatus(Z)V
    .locals 4
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshCommonButton(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v2}, Lcom/mediatek/FMRadio/FMRadioService;->isFMOverBTActive()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f060046

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v2, 0x7f060047

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void

    :cond_1
    move v0, p1

    goto :goto_0
.end method

.method private refreshCommonButton(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonDecrease:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPrevStation:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonNextStation:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonIncrease:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private refreshRecordingStatus(I)V
    .locals 9
    .param p1    # I

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-nez v2, :cond_0

    const-string v2, "FmRx/Activity"

    const-string v3, "mService is null when refreshRecordingStatus()"

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v2}, Lcom/mediatek/FMRadio/FMRadioService;->getRecorderState()I

    move-result v0

    :goto_1
    const-string v2, "FmRx/Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refreshRecordingStatus: state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v2, "FmRx/Activity"

    const-string v3, "invalid record status"

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iput v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPrevRecorderState:I

    goto :goto_0

    :cond_1
    move v0, p1

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v2}, Lcom/mediatek/FMRadio/FMRadioService;->getRecordTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isRecordFileExist()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    :cond_2
    const/4 v2, 0x6

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPrevRecorderState:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v2}, Lcom/mediatek/FMRadio/FMRadioService;->getRecordingName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/FMRadio/FMRadioActivity;->showSaveRecordingDialog(Ljava/lang/String;)V

    :cond_3
    :goto_3
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRLRecordInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_3

    :pswitch_2
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoRight:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRLRecordInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_3
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v2}, Lcom/mediatek/FMRadio/FMRadioService;->getRecordingName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    const-string v1, ""

    :cond_5
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoRight:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRLRecordInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :pswitch_4
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRLRecordInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private refreshStationUI(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationValue:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/mediatek/FMRadio/FMRadioUtils;->formatStation(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/mediatek/FMRadio/FMRadioStation;->isFavoriteStation(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-static {v1, p1, v2}, Lcom/mediatek/FMRadio/FMRadioStation;->getStationName(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private seek(FZ)F
    .locals 2
    .param p1    # F
    .param p2    # Z

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.seek"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/FMRadio/FMRadioService;->seek(FZ)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private seekStation(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    invoke-virtual {v2}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    invoke-virtual {v2, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    :cond_0
    new-instance v2, Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    invoke-direct {v2, p0, v3}, Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioActivity$1;)V

    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSeekAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SeekAsyncTask;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    if-eqz p2, :cond_1

    move v0, v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private setMute(Z)I
    .locals 2
    .param p1    # Z

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.setMute"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->setMute(Z)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private setRDS(Z)I
    .locals 2
    .param p1    # Z

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.setRDS"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->setRDS(Z)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private showRDS()V
    .locals 4

    const-string v0, ""

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPSString:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPSString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPSString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mLRTextString:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mLRTextString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mLRTextString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextRDS:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextRDS:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    const-string v1, "FmRx/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FMRadioActivity.showRDS: RDS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private showToast(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mToast:Landroid/widget/Toast;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "FmRx/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FMRadioActivity.showToast: toast = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private startAnimation()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private startScan()[I
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.startScan"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->startScan()[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopAnimation()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private stopScan()Z
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.stopScan"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->stopScan()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tune(F)Z
    .locals 2
    .param p1    # F

    const-string v0, "FmRx/Activity"

    const-string v1, "FMRadioActivity.tune"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->tune(F)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tuneToStation(I)V
    .locals 4
    .param p1    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    :cond_0
    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTuneAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$TuneAsyncTask;

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateCurrentStation()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->getFrequency()I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioUtils;->isValidStation(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    if-eq v1, v0, :cond_0

    const-string v1, "FmRx/Activity"

    const-string v2, "frequency in service isn\'t same as in database"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/FMRadioStation;->setCurrentStation(Landroid/content/Context;I)V

    iget v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-direct {p0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshStationUI(I)V

    :cond_0
    return-void
.end method

.method private updateFavoriteStation()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/FMRadioStation;->isFavoriteStation(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v3, v4}, Lcom/mediatek/FMRadio/FMRadioStation;->getStationName(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v1, v5, v3}, Lcom/mediatek/FMRadio/FMRadioStation;->updateStationToDB(Landroid/content/Context;Ljava/lang/String;II)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v3, 0x7f02000b

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mProjectStringExt:Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f04001e

    const v5, 0x7f04001f

    invoke-interface {v2, v3, v4, v5}, Lcom/mediatek/FMRadio/ext/IProjectStringExt;->getProjectString(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->showToast(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x7d0

    invoke-virtual {v2, v6, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/mediatek/FMRadio/FMRadioStation;->getStationCount(Landroid/content/Context;I)I

    move-result v2

    if-lt v2, v6, :cond_1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mProjectStringExt:Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f04001a

    const v5, 0x7f04001b

    invoke-interface {v2, v3, v4, v5}, Lcom/mediatek/FMRadio/ext/IProjectStringExt;->getProjectString(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v3, v5}, Lcom/mediatek/FMRadio/FMRadioStation;->getStationName(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v3, v5}, Lcom/mediatek/FMRadio/FMRadioStation;->isStationExist(Landroid/content/Context;II)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v1, v4, v3}, Lcom/mediatek/FMRadio/FMRadioStation;->updateStationToDB(Landroid/content/Context;Ljava/lang/String;II)V

    :goto_1
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v3, 0x7f02000e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mProjectStringExt:Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f040020

    const v5, 0x7f040021

    invoke-interface {v2, v3, v4, v5}, Lcom/mediatek/FMRadio/ext/IProjectStringExt;->getProjectString(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2, v1, v3, v4}, Lcom/mediatek/FMRadio/FMRadioStation;->insertStationToDB(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_1
.end method

.method private updateRds()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    if-eqz v0, :cond_0

    const-string v0, "FmRx/Activity"

    const-string v1, "ServiceConnection: RDS is supported"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isPSRTEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isAFEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->getPS()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPSString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->getLRText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mLRTextString:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private updateRecorderError(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v1, "FmRx/Activity"

    const-string v2, "invalid recorder error"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->showToast(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_1
    const v1, 0x7f04002f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v1, 0x7f040030

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v1, 0x7f040031

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v1, 0x7f040032

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateRecordingState(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x4

    iput p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsActivityForeground:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshRecordingStatus(I)V

    :goto_0
    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsFreshRecordingStatus:Z

    goto :goto_0

    :pswitch_0
    const v0, 0x7f04002e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->showToast(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private useEarphone(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "FmRx/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FMRadioActivity.useEarphone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->useEarphone(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public cancelSearch()V
    .locals 2

    const/4 v1, 0x1

    sput-boolean v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sIsStopScanCalled:Z

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->stopScan()Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "FmRx/Activity"

    const-string v1, "cancelSearch: mSearchAsyncTask is null!"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->setMute(Z)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->resumeFMAudio()V

    :cond_1
    const-string v0, "FmRx/Activity"

    const-string v1, "onCancelled"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isRecordingCardUnmount(Landroid/content/Intent;)Z
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FmRx/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unmount sd card file path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDDirectory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public noAntennaCancel()V
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, " onClick Negative"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsExitPressed:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public noAntennaContinue()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isAntennaAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onPlayFM()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mediatek/FMRadio/FMRadioService;->switchAntenna(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onPlayFM()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, -0x1

    if-ne v1, p2, :cond_1

    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    const-string v1, "ACTIVITY_RESULT"

    iget v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->tuneToStation(I)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "FmRx/Activity"

    const-string v2, "Error: Invalid requestcode."

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/FMRadioStation;->isFavoriteStation(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v2, 0x7f02000e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lcom/mediatek/FMRadio/FMRadioStation;->getStationName(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const-string v1, "FmRx/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The activity for requestcode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not return any data."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v2, 0x7f02000b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "begin FMRadioActivity.onBackPressed"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->changeRecordingMode(Z)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020036

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshButtonStatus(Z)V

    :goto_1
    const-string v0, "FmRx/Activity"

    const-string v1, "end FMRadioActivity.onBackPressed"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const v0, 0x7f020026

    goto :goto_0

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "FmRx/Activity"

    const-string v1, "start FMRadioActivity.onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v0, "FmRx/Activity"

    const-string v1, "end FMRadioActivity.onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "FmRx/Activity"

    const-string v2, "FMRadioActivity.onCreate start"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sput-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setVolumeControlStream(I)V

    const v1, 0x7f030005

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioStation;->initFMDatabase(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/FMRadio/ExtensionUtils;->getExtension(Landroid/content/Context;)Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mProjectStringExt:Lcom/mediatek/FMRadio/ext/IProjectStringExt;

    const v1, 0x7f06002e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextRDS:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextRDS:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f06002c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextFM:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextFM:Landroid/widget/TextView;

    const-string v2, "FM"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f06002d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextMHz:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextMHz:Landroid/widget/TextView;

    const-string v2, "MHz"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f06002b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationValue:Landroid/widget/TextView;

    const v1, 0x7f06003b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoLeft:Landroid/widget/TextView;

    const v1, 0x7f06003c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTxtRecInfoRight:Landroid/widget/TextView;

    const v1, 0x7f06003a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRLRecordInfo:Landroid/widget/RelativeLayout;

    const v1, 0x7f060037

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    const v1, 0x7f060038

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    const v1, 0x7f060039

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    const v1, 0x7f06002f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v1, 0x7f06002a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    const v1, 0x7f060032

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonDecrease:Landroid/widget/ImageButton;

    const v1, 0x7f060034

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonIncrease:Landroid/widget/ImageButton;

    const v1, 0x7f060031

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPrevStation:Landroid/widget/ImageButton;

    const v1, 0x7f060035

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonNextStation:Landroid/widget/ImageButton;

    const v1, 0x7f060033

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonRecord:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonStop:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonDecrease:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonIncrease:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPrevStation:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonNextStation:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayStop:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    if-eqz v1, :cond_1

    const v1, 0x7f020036

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioStation;->getCurrentStation(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/FMRadioStation;->isFavoriteStation(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v2, 0x7f02000e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lcom/mediatek/FMRadio/FMRadioStation;->getStationName(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mTextStationValue:Landroid/widget/TextView;

    iget v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mCurrentStation:I

    invoke-static {v2}, Lcom/mediatek/FMRadio/FMRadioUtils;->formatStation(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v1, 0x7f020000

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimation:Landroid/view/animation/Animation;

    const v1, 0x7f06003e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAnimImage:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    invoke-direct {p0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshButtonStatus(Z)V

    const-string v1, "FmRx/Activity"

    const-string v2, "FMRadioActivity.onCreate end"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const v1, 0x7f020026

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    const v2, 0x7f02000b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const-string v1, "FmRx/Activity"

    const-string v2, "start FMRadioActivity.onCreateOptionsMenu"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f060042

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mMenuItemChannelList:Landroid/view/MenuItem;

    const v1, 0x7f060043

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mMenuItemOverflow:Landroid/view/MenuItem;

    const-string v1, "FmRx/Activity"

    const-string v2, "end FMRadioActivity.onCreateOptionsMenu"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    return v1
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "FmRx/Activity"

    const-string v1, "start FMRadioActivity.onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSDListenerRegistered:Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    sput-object v2, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mFMRadioListener:Lcom/mediatek/FMRadio/FMRadioListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/FMRadio/FMRadioService;->unregisterFMRadioListener(Lcom/mediatek/FMRadio/FMRadioListener;)V

    :cond_1
    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mFMRadioListener:Lcom/mediatek/FMRadio/FMRadioListener;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    :cond_2
    const-string v0, "FmRx/Activity"

    const-string v1, "end FMRadioActivity.onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDismiss(Landroid/widget/PopupMenu;)V
    .locals 3
    .param p1    # Landroid/widget/PopupMenu;

    const-string v0, "FmRx/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "popmenu dismiss listener:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "FmRx/Activity"

    const-string v1, "invalid menu item"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    :cond_0
    new-instance v0, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    invoke-direct {v0, p0, v3}, Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;-><init>(Lcom/mediatek/FMRadio/FMRadioActivity;Lcom/mediatek/FMRadio/FMRadioActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSearchAsyncTask:Lcom/mediatek/FMRadio/FMRadioActivity$SearchAsyncTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->showRdsSettingDialog()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/mediatek/FMRadio/FMRadioActivity;->changeRecordingMode(Z)V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshRecordingStatus(I)V

    goto :goto_0

    :pswitch_3
    iput-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsExitPressed:Z

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->exitService()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isEarphoneUsed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onUseLoudspeaker()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onUseEarphone()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f060044
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1    # Landroid/view/MenuItem;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v3, "FmRx/Activity"

    const-string v4, "start FMRadioActivity.onOptionsItemSelected"

    invoke-static {v3, v4}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    const-string v3, "FmRx/Activity"

    const-string v4, "Error: Invalid options menu item."

    invoke-static {v3, v4}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v3, "FmRx/Activity"

    const-string v4, "end FMRadioActivity.onOptionsItemSelected"

    invoke-static {v3, v4}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    :sswitch_0
    invoke-virtual {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->enterChannelList()V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isEarphoneUsed()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onUseLoudspeaker()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->onUseEarphone()V

    goto :goto_0

    :sswitch_3
    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    new-instance v3, Landroid/widget/PopupMenu;

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const v5, 0x7f060043

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    const v4, 0x7f050001

    invoke-virtual {v3, v4, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3, p0}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v3}, Lcom/mediatek/FMRadio/FMRadioService;->isDeviceOpen()Z

    move-result v0

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v3}, Lcom/mediatek/FMRadio/FMRadioService;->isFMOverBTActive()Z

    move-result v1

    :cond_1
    const v3, 0x7f060048

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInitialDone:Z

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v3, 0x7f060046

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsRDSSupported:Z

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    if-eqz v3, :cond_2

    if-nez v1, :cond_2

    const v3, 0x7f060047

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    if-eqz v0, :cond_3

    const v3, 0x7f060045

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_3
    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->show()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f060042 -> :sswitch_1
        0x7f060043 -> :sswitch_3
        0x7f060044 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "FmRx/Activity"

    const-string v1, "start FMRadioActivity.onPause"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsActivityForeground:Z

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsExitPressed:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    if-eqz v0, :cond_1

    const-string v0, "FmRx/Activity"

    const-string v1, "FM is Playing. So stop it."

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsSeeking:Z

    if-eqz v0, :cond_0

    const-string v0, "FmRx/Activity"

    const-string v1, "stop begin!"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->stopScan()Z

    const-string v0, "FmRx/Activity"

    const-string v1, "stop end!"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->powerDown()Z

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    :cond_1
    const-string v0, "FmRx/Activity"

    const-string v1, "end FMRadioActivity.onPause"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const v5, 0x7f060043

    const v4, 0x7f060042

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "FmRx/Activity"

    const-string v3, "start FMRadioActivity.onPrepareOptionsMenu"

    invoke-static {v0, v3}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const-string v0, "FmRx/Activity"

    const-string v3, "end FMRadioActivity.onPrepareOptionsMenu"

    invoke-static {v0, v3}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-nez v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public onRecordingDialogClick(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0, p1}, Lcom/mediatek/FMRadio/FMRadioService;->saveRecording(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v0}, Lcom/mediatek/FMRadio/FMRadioService;->isSearching()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FmRx/Activity"

    const-string v1, "ServiceConnection: mIsSearching is false"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->dismissSearchDialog()V

    :cond_0
    const-string v0, "FmRx/Activity"

    const-string v1, "start FMRadioActivity.onResume"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsActivityForeground:Z

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsFreshRecordingStatus:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->refreshRecordingStatus(I)V

    iput-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsFreshRecordingStatus:Z

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->isRecordFileExist()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonPlayback:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v0, "FmRx/Activity"

    const-string v1, "end FMRadioActivity.onResume"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    const-string v0, "FmRx/Activity"

    const-string v1, "doesn\'t need to refresh recording status"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 5

    const/4 v2, 0x4

    new-instance v0, Landroid/os/Bundle;

    const/4 v3, 0x4

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    const/4 v1, 0x0

    const-string v3, "isInRecordingMode"

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "mPrevRecorderState"

    iget v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPrevRecorderState:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "mIsFreshRecordingStatus"

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsFreshRecordingStatus:Z

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "mRecordState"

    iget v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "mIsMenuItemChanged"

    iget-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public onStart()V
    .locals 15

    const-wide/16 v13, 0x0

    const/4 v12, 0x1

    const/16 v9, 0x8

    const/4 v10, 0x0

    const-string v8, "FmRx/Activity"

    const-string v11, "begin FMRadioActivity.onStart"

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v8, Landroid/content/Intent;

    const-class v11, Lcom/mediatek/FMRadio/FMRadioService;

    invoke-direct {v8, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v8}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v8, "FmRx/Activity"

    const-string v11, "Error: Cannot start FM service"

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceBinded:Z

    if-nez v8, :cond_1

    const-string v8, "FmRx/Activity"

    const-string v9, "Error: Cannot bind FM service"

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :cond_0
    const-string v8, "FmRx/Activity"

    const-string v11, "Start FM service successfully."

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v12, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceStarted:Z

    new-instance v8, Landroid/content/Intent;

    const-class v11, Lcom/mediatek/FMRadio/FMRadioService;

    invoke-direct {v8, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v11, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v8, v11, v12}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsServiceBinded:Z

    goto :goto_0

    :cond_1
    const-string v8, "FmRx/Activity"

    const-string v11, "Bind FM service successfully."

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_3

    const-string v8, "FmRx/Activity"

    const-string v11, "Configration changes,activity restart,need to reset UI!"

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mSDDirectory:Ljava/lang/String;

    if-eqz v2, :cond_3

    const-string v8, "mPrevRecorderState"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPrevRecorderState:I

    const-string v8, "mRecordState"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    const-string v8, "mIsFreshRecordingStatus"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsFreshRecordingStatus:Z

    const-string v8, "FMRecord"

    invoke-virtual {p0, v8, v10}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "startRecordTime"

    invoke-interface {v7, v8, v13, v14}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    iput-wide v11, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordStartTime:J

    const-string v8, "startPlayTime"

    invoke-interface {v7, v8, v13, v14}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    iput-wide v11, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPlayStartTime:J

    const-string v8, "isInRecordingMode"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    const-string v8, "FmRx/Activity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "isInRecordingMode = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ";mPrevRecorderState = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPrevRecorderState:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "mIsMenuItemChanged"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    const v8, 0x7f060036

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    const v8, 0x7f060030

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v8, 0x7f06003a

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_4

    move v8, v9

    :goto_2
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    if-eqz v4, :cond_5

    move v8, v10

    :goto_3
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v11, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mButtonAddToFavorite:Landroid/widget/ImageButton;

    if-eqz v4, :cond_6

    move v8, v9

    :goto_4
    invoke-virtual {v11, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v8, 0x6

    iget v11, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    if-eq v8, v11, :cond_2

    const/4 v8, 0x7

    iget v11, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRecordState:I

    if-ne v8, v11, :cond_7

    :cond_2
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    iget-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    iget-boolean v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsInRecordingMode:Z

    if-eqz v8, :cond_8

    const v8, 0x7f04002c

    :goto_6
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mRefresher:Landroid/os/Handler;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    const-string v8, "FmRx/Activity"

    const-string v9, "end FMRadioActivity.onStart"

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    move v8, v10

    goto :goto_2

    :cond_5
    move v8, v9

    goto :goto_3

    :cond_6
    move v8, v10

    goto :goto_4

    :cond_7
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_8
    const/high16 v8, 0x7f040000

    goto :goto_6
.end method

.method public onStop()V
    .locals 2

    const-string v0, "FmRx/Activity"

    const-string v1, "start FMRadioActivity.onStop"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRadioActivity;->exitService()V

    const-string v0, "FmRx/Activity"

    const-string v1, "end FMRadioActivity.onStop"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public refreshPopupMenu(Z)V
    .locals 4
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mService:Lcom/mediatek/FMRadio/FMRadioService;

    invoke-virtual {v2}, Lcom/mediatek/FMRadio/FMRadioService;->isFMOverBTActive()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f060047

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsMenuItemChanged:Z

    and-int/2addr v3, v0

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v2, 0x7f060045

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPlaying:Z

    goto :goto_0
.end method

.method public selectRds(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    const/4 v2, 0x1

    if-nez p1, :cond_1

    iput-boolean p2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    invoke-static {v0, v2, v1}, Lcom/mediatek/FMRadio/FMRadioStation;->setRDSEnabled(Landroid/content/Context;IZ)V

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsPSRTEnabled:Z

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->enablePSRT(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v2, p1, :cond_0

    iput-boolean p2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    invoke-static {v0, v1, v2}, Lcom/mediatek/FMRadio/FMRadioStation;->setRDSEnabled(Landroid/content/Context;IZ)V

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRadioActivity;->mIsAFEnabled:Z

    invoke-direct {p0, v0}, Lcom/mediatek/FMRadio/FMRadioActivity;->enableAF(Z)V

    goto :goto_0
.end method

.method public showNoAntennaDialog()V
    .locals 3

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sput-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    :cond_0
    invoke-static {}, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;->newInstance()Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;

    move-result-object v0

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "NoAntenna"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showRdsSettingDialog()V
    .locals 3

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sput-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    :cond_0
    invoke-static {}, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;->newInstance()Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;

    move-result-object v0

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "RdsSetting"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showSaveRecordingDialog(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sput-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    :cond_0
    new-instance v0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    invoke-direct {v0, p1}, Lcom/mediatek/FMRadio/FMRecordDialogFragment;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "SaveRecording"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showSearchDialog()V
    .locals 3

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sput-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    :cond_0
    invoke-static {}, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;->newInstance()Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;

    move-result-object v0

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "Search"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    sget-object v1, Lcom/mediatek/FMRadio/FMRadioActivity;->sFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    return-void
.end method
