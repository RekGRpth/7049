.class Lcom/mediatek/FMRadio/FMRadioEMActivity$6;
.super Ljava/lang/Thread;
.source "FMRadioEMActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/FMRadio/FMRadioEMActivity;->tuneToStation(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

.field final synthetic val$iStation:I


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/FMRadioEMActivity;I)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iput p2, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->val$iStation:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iget v1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->val$iStation:I

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioUtils;->computeFrequency(I)F

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$4400(Lcom/mediatek/FMRadio/FMRadioEMActivity;F)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "FmRx/EM"

    const-string v1, "Tune to the station succeeded."

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iget v1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->val$iStation:I

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$402(Lcom/mediatek/FMRadio/FMRadioEMActivity;I)I

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$200(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$400(Lcom/mediatek/FMRadio/FMRadioEMActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioStation;->setCurrentStation(Landroid/content/Context;I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$6;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$700(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
