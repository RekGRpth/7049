.class Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;
.super Ljava/lang/Thread;
.source "FMRadioEMActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/FMRadioEMActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InitialThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/FMRadio/FMRadioEMActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$4200(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$700(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$2000(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$1902(Lcom/mediatek/FMRadio/FMRadioEMActivity;Z)Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$1800(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$002(Lcom/mediatek/FMRadio/FMRadioEMActivity;Z)Z

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$000(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$700(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    const-string v0, "FmRx/EM"

    const-string v1, "opendev succeed."

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "FmRx/EM"

    const-string v1, "InitialThread terminated."

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$InitialThread;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$400(Lcom/mediatek/FMRadio/FMRadioEMActivity;)I

    move-result v1

    invoke-static {v1}, Lcom/mediatek/FMRadio/FMRadioUtils;->computeFrequency(I)F

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$4300(Lcom/mediatek/FMRadio/FMRadioEMActivity;F)Z

    goto :goto_0
.end method
