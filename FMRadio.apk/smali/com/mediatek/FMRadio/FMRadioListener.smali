.class public interface abstract Lcom/mediatek/FMRadio/FMRadioListener;
.super Ljava/lang/Object;
.source "FMRadioListener.java"


# static fields
.field public static final LISTEN_FM_EXIT:I = 0x1000

.field public static final LISTEN_POWERUP_CHANGED:I = 0x1

.field public static final LISTEN_PS_CHANGED:I = 0x11

.field public static final LISTEN_RDSSTATION_CHANGED:I = 0x10

.field public static final LISTEN_RECORDERROR:I = 0x110

.field public static final LISTEN_RECORDMODE_CHANGED:I = 0x111

.field public static final LISTEN_RECORDSTATE_CHANGED:I = 0x101

.field public static final LISTEN_RT_CHANGED:I = 0x100


# virtual methods
.method public abstract onFmExit()V
.end method

.method public abstract onPowerUpChanged(Z)V
.end method

.method public abstract onPsChanged(Ljava/lang/String;)V
.end method

.method public abstract onRdsStationChanged(I)V
.end method

.method public abstract onRecordError(I)V
.end method

.method public abstract onRecordModeChanged(Z)V
.end method

.method public abstract onRecordStateChanged(I)V
.end method

.method public abstract onRtChanged(Ljava/lang/String;)V
.end method
