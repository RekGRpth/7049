.class Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;
.super Ljava/lang/Object;
.source "FMRecordDialogFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/FMRadio/FMRecordDialogFragment;->setTextChangedCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRecordDialogFragment;


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;->this$0:Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;->this$0:Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    invoke-static {v0, v2}, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->access$002(Lcom/mediatek/FMRadio/FMRecordDialogFragment;Z)Z

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".*[/\\\\:*?\"<>|].*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;->this$0:Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->access$100(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;->this$0:Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->access$100(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
