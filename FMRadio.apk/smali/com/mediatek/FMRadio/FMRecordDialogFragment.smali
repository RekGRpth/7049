.class public Lcom/mediatek/FMRadio/FMRecordDialogFragment;
.super Landroid/app/DialogFragment;
.source "FMRecordDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;
    }
.end annotation


# static fields
.field private static final RECORDING_CHECKFILENAME:Ljava/lang/String; = "recordingcheckfileName"

.field private static final RECORDING_DEFAULTNAME:Ljava/lang/String; = "recordingName"

.field private static final RECORDING_TOSAVANAME:Ljava/lang/String; = "recordingToSavaName"

.field private static final TAG:Ljava/lang/String; = "FmRx/RecordDlg"


# instance fields
.field private mButtonDiscard:Landroid/widget/Button;

.field private mButtonOnClickListener:Landroid/view/View$OnClickListener;

.field private mButtonSave:Landroid/widget/Button;

.field private mDefaultRecordingName:Ljava/lang/String;

.field private mDivider:Landroid/view/View;

.field private mIsNeedCheckFilenameExist:Z

.field private mListener:Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;

.field private mRecordingNameEditText:Landroid/widget/EditText;

.field private mRecordingNameToSave:Ljava/lang/String;

.field private mStorageWarningTextView:Landroid/widget/TextView;

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonSave:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonDiscard:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDivider:Landroid/view/View;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mTitleTextView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mStorageWarningTextView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mListener:Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mIsNeedCheckFilenameExist:Z

    new-instance v0, Lcom/mediatek/FMRadio/FMRecordDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment$2;-><init>(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonSave:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonDiscard:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDivider:Landroid/view/View;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mTitleTextView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mStorageWarningTextView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mListener:Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mIsNeedCheckFilenameExist:Z

    new-instance v0, Lcom/mediatek/FMRadio/FMRecordDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment$2;-><init>(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonOnClickListener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Z
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    iget-boolean v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mIsNeedCheckFilenameExist:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/FMRadio/FMRecordDialogFragment;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mIsNeedCheckFilenameExist:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonSave:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/FMRadio/FMRecordDialogFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/FMRecordDialogFragment;

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mListener:Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;

    return-object v0
.end method

.method private checkRemainingStorage()Z
    .locals 11

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v2, v8

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v0, v8

    mul-long v6, v2, v0

    const-string v8, "FmRx/RecordDlg"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkRemainingStorage: available space="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/32 v8, 0x80000

    cmp-long v8, v6, v8

    if-lez v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    return v8

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private setTextChangedCallback()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment$1;-><init>(Lcom/mediatek/FMRadio/FMRecordDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    :try_start_0
    check-cast p1, Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mListener:Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "FmRx/RecordDlg"

    const-string v1, ">>onCreate()"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/DialogFragment;->setStyle(II)V

    if-eqz p1, :cond_0

    const-string v0, "recordingName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    const-string v0, "recordingToSavaName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    const-string v0, "recordingcheckfileName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mIsNeedCheckFilenameExist:Z

    :cond_0
    const-string v0, "FmRx/RecordDlg"

    const-string v1, "<<onCreate()"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v3, 0x7f030003

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonSave:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonSave:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f060012

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonDiscard:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonDiscard:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f060010

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mStorageWarningTextView:Landroid/widget/TextView;

    const v3, 0x7f060011

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    const v3, 0x7f06000e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDivider:Landroid/view/View;

    const v3, 0x7f06000d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDivider:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    return-object v2
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "FmRx/RecordDlg"

    const-string v1, ">>onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mListener:Lcom/mediatek/FMRadio/FMRecordDialogFragment$OnRecordingDialogClickListener;

    const-string v0, "FmRx/RecordDlg"

    const-string v1, "<<onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "FmRx/RecordDlg"

    const-string v1, ">>onResume()"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->checkRemainingStorage()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mStorageWarningTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mButtonSave:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-direct {p0}, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->setTextChangedCallback()V

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    const-string v0, "FmRx/RecordDlg"

    const-string v1, "<<onResume()"

    invoke-static {v0, v1}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    iget-object v0, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v1, "FmRx/RecordDlg"

    const-string v2, ">>onSaveInstanceState()"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "recordingName"

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "recordingcheckfileName"

    iget-boolean v2, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mIsNeedCheckFilenameExist:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "FmRx/RecordDlg"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDefaultRecordingName is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mDefaultRecordingName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    const-string v1, "recordingToSavaName"

    iget-object v2, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "FmRx/RecordDlg"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRecordingNameToSave is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/FMRadio/FMRecordDialogFragment;->mRecordingNameToSave:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "FmRx/RecordDlg"

    const-string v2, "<<onSaveInstanceState()"

    invoke-static {v1, v2}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
