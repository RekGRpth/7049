.class public Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;
.super Landroid/app/DialogFragment;
.source "RdsSettingDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;)Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;

    iget-object v0, p0, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;

    return-object v0
.end method

.method public static newInstance()Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;
    .locals 1

    new-instance v0, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;

    invoke-direct {v0}, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    :try_start_0
    check-cast p1, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;

    iput-object p1, p0, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$SelectRdsListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v6, 0x1

    new-array v2, v4, [Ljava/lang/CharSequence;

    const v3, 0x7f040028

    invoke-virtual {p0, v3}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const v3, 0x7f040029

    invoke-virtual {p0, v3}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    new-array v0, v4, [Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/mediatek/FMRadio/FMRadioStation;->getRDSEnabled(Landroid/content/Context;I)Z

    move-result v3

    aput-boolean v3, v0, v5

    invoke-static {v1, v4}, Lcom/mediatek/FMRadio/FMRadioStation;->getRDSEnabled(Landroid/content/Context;I)Z

    move-result v3

    aput-boolean v3, v0, v6

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f040027

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$1;

    invoke-direct {v4, p0}, Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog$1;-><init>(Lcom/mediatek/FMRadio/dialogs/RdsSettingDialog;)V

    invoke-virtual {v3, v2, v0, v4}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f04000e

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method
