.class Lcom/android/facelock/Draw$2;
.super Ljava/lang/Object;
.source "Draw.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/Draw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/Draw;


# direct methods
.method constructor <init>(Lcom/android/facelock/Draw;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/Draw$2;->this$0:Lcom/android/facelock/Draw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/facelock/Draw$2;->this$0:Lcom/android/facelock/Draw;

    const/16 v1, 0xff

    # setter for: Lcom/android/facelock/Draw;->mCheckAlpha:I
    invoke-static {v0, v1}, Lcom/android/facelock/Draw;->access$102(Lcom/android/facelock/Draw;I)I

    iget-object v0, p0, Lcom/android/facelock/Draw$2;->this$0:Lcom/android/facelock/Draw;

    invoke-virtual {v0}, Lcom/android/facelock/Draw;->invalidate()V

    iget-object v0, p0, Lcom/android/facelock/Draw$2;->this$0:Lcom/android/facelock/Draw;

    iget-object v1, p0, Lcom/android/facelock/Draw$2;->this$0:Lcom/android/facelock/Draw;

    iget v1, v1, Lcom/android/facelock/Draw;->DRAW_COMPLETE:I

    # setter for: Lcom/android/facelock/Draw;->mState:I
    invoke-static {v0, v1}, Lcom/android/facelock/Draw;->access$202(Lcom/android/facelock/Draw;I)I

    iget-object v0, p0, Lcom/android/facelock/Draw$2;->this$0:Lcom/android/facelock/Draw;

    # getter for: Lcom/android/facelock/Draw;->mListener:Lcom/android/facelock/Draw$DrawListener;
    invoke-static {v0}, Lcom/android/facelock/Draw;->access$300(Lcom/android/facelock/Draw;)Lcom/android/facelock/Draw$DrawListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/facelock/Draw$DrawListener;->onDrawComplete()V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method
