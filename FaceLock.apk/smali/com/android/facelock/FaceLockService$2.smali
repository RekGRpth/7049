.class Lcom/android/facelock/FaceLockService$2;
.super Ljava/lang/Object;
.source "FaceLockService.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/FaceLockService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/FaceLockService;


# direct methods
.method constructor <init>(Lcom/android/facelock/FaceLockService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/FaceLockService$2;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$2;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$600(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/FaceLockEventLog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->cancelPressed()V

    # getter for: Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/facelock/FaceLockService;->access$700()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/facelock/FaceLockService$2;->this$0:Lcom/android/facelock/FaceLockService;

    const/4 v2, 0x1

    # setter for: Lcom/android/facelock/FaceLockService;->mDone:Z
    invoke-static {v0, v2}, Lcom/android/facelock/FaceLockService;->access$802(Lcom/android/facelock/FaceLockService;Z)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$2;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$2;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$900(Lcom/android/facelock/FaceLockService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$2;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$600(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/FaceLockEventLog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
