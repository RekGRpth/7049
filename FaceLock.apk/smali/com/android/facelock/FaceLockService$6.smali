.class Lcom/android/facelock/FaceLockService$6;
.super Ljava/lang/Object;
.source "FaceLockService.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/FaceLockService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/FaceLockService;


# direct methods
.method constructor <init>(Lcom/android/facelock/FaceLockService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5
    .param p1    # Landroid/animation/ValueAnimator;

    const/high16 v4, 0x3f800000

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mBackground:Landroid/view/View;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1500(Lcom/android/facelock/FaceLockService;)Landroid/view/View;

    move-result-object v1

    sub-float v2, v4, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mIntroStopSize:F
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1600(Lcom/android/facelock/FaceLockService;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    iget-object v2, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v2}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v2

    iget v2, v2, Lcom/android/facelock/Spotlight;->mSize:F

    # setter for: Lcom/android/facelock/FaceLockService;->mIntroStopSize:F
    invoke-static {v1, v2}, Lcom/android/facelock/FaceLockService;->access$1602(Lcom/android/facelock/FaceLockService;F)F

    :cond_0
    iget-object v1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v1

    iget-object v2, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mIntroStopSize:F
    invoke-static {v2}, Lcom/android/facelock/FaceLockService;->access$1600(Lcom/android/facelock/FaceLockService;)F

    move-result v2

    iget-object v3, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mIntroStopSize:F
    invoke-static {v3}, Lcom/android/facelock/FaceLockService;->access$1600(Lcom/android/facelock/FaceLockService;)F

    move-result v3

    sub-float v3, v4, v3

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lcom/android/facelock/Spotlight;->mSize:F

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v1

    const v2, 0x3f4ccccd

    sub-float v3, v4, v0

    mul-float/2addr v2, v3

    const v3, 0x3f666666

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/android/facelock/Spotlight;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$6;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/facelock/Spotlight;->invalidate()V

    return-void
.end method
