.class Lcom/android/facelock/FaceLockService$8;
.super Lcom/android/internal/policy/IFaceLockInterface$Stub;
.source "FaceLockService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/facelock/FaceLockService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/FaceLockService;


# direct methods
.method constructor <init>(Lcom/android/facelock/FaceLockService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Lcom/android/internal/policy/IFaceLockInterface$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public registerCallback(Lcom/android/internal/policy/IFaceLockCallback;)V
    .locals 1
    .param p1    # Lcom/android/internal/policy/IFaceLockCallback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    iget-object v0, v0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method

.method public startUi(Landroid/os/IBinder;IIIIZ)V
    .locals 4
    .param p1    # Landroid/os/IBinder;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Z

    const-string v1, "FULFaceLockService"

    const-string v2, "startUi() reached in service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v1, "FULFaceLockService"

    const-string v2, "containingWindowToken is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$900(Lcom/android/facelock/FaceLockService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # setter for: Lcom/android/facelock/FaceLockService;->mUseLiveliness:Z
    invoke-static {v1, p6}, Lcom/android/facelock/FaceLockService;->access$1802(Lcom/android/facelock/FaceLockService;Z)Z

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mUseLiveliness:Z
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1800(Lcom/android/facelock/FaceLockService;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$600(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/FaceLockEventLog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/facelock/FaceLockEventLog;->useLiveliness()V

    :cond_1
    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1900(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/ProcessingThread;

    move-result-object v1

    iget-object v2, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mUseLiveliness:Z
    invoke-static {v2}, Lcom/android/facelock/FaceLockService;->access$1800(Lcom/android/facelock/FaceLockService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/facelock/ProcessingThread;->setUseLiveliness(Z)V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x3e8

    invoke-direct {v2, v3}, Landroid/view/WindowManager$LayoutParams;-><init>(I)V

    # setter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1, v2}, Lcom/android/facelock/FaceLockService;->access$1102(Lcom/android/facelock/FaceLockService;Landroid/view/WindowManager$LayoutParams;)Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const v2, 0x1000008

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput-object p1, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput p2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput p3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput p4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput p5, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const-string v2, "FaceUnlock"

    iput-object v2, v1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/facelock/FaceLockService;->access$900(Lcom/android/facelock/FaceLockService;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/android/facelock/FaceLockService;->access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0
.end method

.method public stopUi()V
    .locals 2

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$900(Lcom/android/facelock/FaceLockService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public unregisterCallback(Lcom/android/internal/policy/IFaceLockCallback;)V
    .locals 1
    .param p1    # Lcom/android/internal/policy/IFaceLockCallback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$8;->this$0:Lcom/android/facelock/FaceLockService;

    iget-object v0, v0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method
