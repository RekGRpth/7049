.class public Lcom/android/facelock/FaceLockService;
.super Landroid/app/Service;
.source "FaceLockService.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/facelock/FaceLockService$InitializeState;
    }
.end annotation


# static fields
.field private static CANCEL_WAIT:I

.field private static DEFAULT_AWAKE_INTERVAL_MS:I

.field private static mBlackScreenTimeout:I

.field private static mDetectionTimeout:I

.field private static mEstimatedMaxProcessingTime:I

.field private static final mInitializeErrorLock:Ljava/lang/Object;

.field private static mLivelinessTimeout:I

.field private static final mLock:Ljava/lang/Object;

.field private static mRecognitionTimeout:I

.field private static mWatchdogTimeout:I

.field private static final sInitializeLock:Ljava/lang/Object;

.field private static sInitialized:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final binder:Lcom/android/internal/policy/IFaceLockInterface$Stub;

.field private mBackground:Landroid/view/View;

.field final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/android/internal/policy/IFaceLockCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraOpenStartTime:J

.field private mCameraStartAnimation:Landroid/animation/ValueAnimator;

.field private mCancelButton:Landroid/widget/ImageButton;

.field private volatile mClosingDown:Z

.field private mDone:Z

.field private mEventLog:Lcom/android/facelock/FaceLockEventLog;

.field private mFirstFrameTime:J

.field private mFirstNonBlackFrameTime:J

.field private mFoundFace:Z

.field private mFrontCameraId:I

.field private mGotFirstFrame:Z

.field private mHandler:Landroid/os/Handler;

.field private mInfoText:Landroid/widget/TextView;

.field private mInfoTextCrossFade:Landroid/animation/ValueAnimator;

.field private mInitializeErrorResource:I

.field private mInitializeFailed:Z

.field private mIntroStopSize:F

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mLivelinessStartTime:J

.field private mLivelinessStarted:Z

.field private volatile mModelsLoaded:Z

.field private mNeedFrame:Z

.field private mOnCreateStartTime:J

.field private mPreferences:Lcom/android/facelock/FaceLockPreferences;

.field private mPreview:Lcom/android/facelock/Preview;

.field private mPreviewShowing:Z

.field private mReachedNonBlackFrame:Z

.field private mReportUnsuccessfulAttempt:Z

.field private mShutdownFader:Landroid/animation/ValueAnimator;

.field private mSpotlight:Lcom/android/facelock/Spotlight;

.field private mThread:Lcom/android/facelock/ProcessingThread;

.field private mTimeToFirstFrame:I

.field private mUseIntroAnimation:Z

.field private mUseLiveliness:Z

.field private mView:Landroid/view/View;

.field private mViewCreated:Z

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x2ee

    sput v0, Lcom/android/facelock/FaceLockService;->CANCEL_WAIT:I

    const/16 v0, 0x2710

    sput v0, Lcom/android/facelock/FaceLockService;->DEFAULT_AWAKE_INTERVAL_MS:I

    const/16 v0, 0x5dc

    sput v0, Lcom/android/facelock/FaceLockService;->mBlackScreenTimeout:I

    const/16 v0, 0x7d0

    sput v0, Lcom/android/facelock/FaceLockService;->mDetectionTimeout:I

    const/16 v0, 0xfa0

    sput v0, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    const/16 v0, 0xdac

    sput v0, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    const/16 v0, 0xbb8

    sput v0, Lcom/android/facelock/FaceLockService;->mEstimatedMaxProcessingTime:I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/facelock/FaceLockService;->sInitialized:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/facelock/FaceLockService;->sInitializeLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/facelock/FaceLockService;->mInitializeErrorLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, "FULFaceLockService"

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    iput v1, p0, Lcom/android/facelock/FaceLockService;->mFrontCameraId:I

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mModelsLoaded:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mInitializeFailed:Z

    iput v1, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mGotFirstFrame:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mReachedNonBlackFrame:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStarted:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/facelock/FaceLockService;->mIntroStopSize:F

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    new-instance v0, Lcom/android/facelock/FaceLockService$8;

    invoke-direct {v0, p0}, Lcom/android/facelock/FaceLockService$8;-><init>(Lcom/android/facelock/FaceLockService;)V

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->binder:Lcom/android/internal/policy/IFaceLockInterface$Stub;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/facelock/FaceLockService;->sInitializeLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/android/facelock/FaceLockService;->sInitialized:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/android/facelock/FaceLockService;)Z
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/facelock/FaceLockService;->sInitialized:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/facelock/FaceLockService;Landroid/view/WindowManager$LayoutParams;)Landroid/view/WindowManager$LayoutParams;
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;
    .param p1    # Landroid/view/WindowManager$LayoutParams;

    iput-object p1, p0, Lcom/android/facelock/FaceLockService;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/facelock/FaceLockService;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/facelock/FaceLockService;)Landroid/view/WindowManager;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/facelock/FaceLockService;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/facelock/FaceLockService;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mBackground:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/facelock/FaceLockService;)F
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget v0, p0, Lcom/android/facelock/FaceLockService;->mIntroStopSize:F

    return v0
.end method

.method static synthetic access$1602(Lcom/android/facelock/FaceLockService;F)F
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;
    .param p1    # F

    iput p1, p0, Lcom/android/facelock/FaceLockService;->mIntroStopSize:F

    return p1
.end method

.method static synthetic access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/facelock/FaceLockService;)Z
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mUseLiveliness:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/android/facelock/FaceLockService;Z)Z
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/facelock/FaceLockService;->mUseLiveliness:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/ProcessingThread;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/facelock/FaceLockService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->getFilesPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/facelock/FaceLockService;I)I
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;
    .param p1    # I

    iput p1, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    return p1
.end method

.method static synthetic access$400(Lcom/android/facelock/FaceLockService;Lcom/android/facelock/FaceLockService$InitializeState;)V
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;
    .param p1    # Lcom/android/facelock/FaceLockService$InitializeState;

    invoke-direct {p0, p1}, Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/facelock/FaceLockService;)V
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->loadGallery()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/FaceLockEventLog;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/facelock/FaceLockService;Z)Z
    .locals 0
    .param p0    # Lcom/android/facelock/FaceLockService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/facelock/FaceLockService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/facelock/FaceLockService;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private blackFrameTimeout(J)Z
    .locals 2
    .param p1    # J

    sget v0, Lcom/android/facelock/FaceLockService;->mBlackScreenTimeout:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private closeCamera()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    sget-object v2, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v1, v3}, Lcom/android/facelock/Preview;->setCamera(Landroid/hardware/Camera;)V

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private detectionTimeout(J)Z
    .locals 2
    .param p1    # J

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    if-nez v0, :cond_0

    sget v0, Lcom/android/facelock/FaceLockService;->mDetectionTimeout:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->detectionTimeout()V

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V
    .locals 5
    .param p1    # Lcom/android/facelock/FaceLockService$InitializeState;

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v3, Lcom/android/facelock/FaceLockService;->mInitializeErrorLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    sget-object v4, Lcom/android/facelock/FaceLockService$InitializeState;->INITIALIZE_FAILED:Lcom/android/facelock/FaceLockService$InitializeState;

    if-ne p1, v4, :cond_3

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/facelock/FaceLockService;->mInitializeFailed:Z

    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/android/facelock/FaceLockService;->mViewCreated:Z

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockService;->mInitializeFailed:Z

    if-eqz v4, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mViewCreated:Z

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->closeCamera()V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    return-void

    :cond_3
    :try_start_1
    sget-object v4, Lcom/android/facelock/FaceLockService$InitializeState;->SET_VIEW:Lcom/android/facelock/FaceLockService$InitializeState;

    if-ne p1, v4, :cond_0

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/facelock/FaceLockService;->mViewCreated:Z

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private displayInfoText(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoTextCrossFade:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    return-void
.end method

.method private doneStatusString()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-eqz v0, :cond_0

    const-string v0, " (DONE)"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private getFilesPath()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "FULFaceLockService"

    const-string v2, "IOException in getFilesPath()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initializeIfNecessary()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mModelsLoaded:Z

    new-instance v0, Lcom/android/facelock/FaceLockService$1;

    invoke-direct {v0, p0}, Lcom/android/facelock/FaceLockService$1;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockService$1;->start()V

    return-void
.end method

.method private livelinessTimeout()Z
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStartTime:J

    sub-long v0, v2, v4

    iget-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mUseLiveliness:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStarted:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v2}, Lcom/android/facelock/FaceLockEventLog;->livelinessTimeout()V

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private loadGallery()V
    .locals 2

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->unlockReadGallery()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "FULFaceLockService"

    const-string v1, "Read gallery failed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f060022

    iput v0, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    sget-object v0, Lcom/android/facelock/FaceLockService$InitializeState;->INITIALIZE_FAILED:Lcom/android/facelock/FaceLockService$InitializeState;

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->unlockLoadRecognitionModels()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mModelsLoaded:Z

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mModelsLoaded:Z

    if-nez v0, :cond_0

    const-string v0, "FULFaceLockService"

    const-string v1, "Model load failed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/facelock/FaceLockService$InitializeState;->INITIALIZE_FAILED:Lcom/android/facelock/FaceLockService$InitializeState;

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V

    goto :goto_0
.end method

.method private recognitionTimeout(J)Z
    .locals 2
    .param p1    # J

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStarted:Z

    if-nez v0, :cond_0

    sget v0, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->recognitionTimeout()V

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private removeView()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    return-void
.end method

.method private startShutdown()V
    .locals 2

    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateSettings()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "ro.facelock.black_timeout"

    sget v3, Lcom/android/facelock/FaceLockService;->mBlackScreenTimeout:I

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mBlackScreenTimeout:I

    const-string v0, "ro.facelock.det_timeout"

    sget v3, Lcom/android/facelock/FaceLockService;->mDetectionTimeout:I

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mDetectionTimeout:I

    const-string v0, "ro.facelock.rec_timeout"

    sget v3, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    const-string v0, "ro.facelock.lively_timeout"

    sget v3, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    const-string v0, "ro.facelock.est_max_time"

    sget v3, Lcom/android/facelock/FaceLockService;->mEstimatedMaxProcessingTime:I

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mEstimatedMaxProcessingTime:I

    const-string v0, "ro.facelock.use_intro_anim"

    iget-boolean v3, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v0, "facelock_black_screen_timeout"

    sget v4, Lcom/android/facelock/FaceLockService;->mBlackScreenTimeout:I

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mBlackScreenTimeout:I

    const-string v0, "facelock_detetection_timeout"

    sget v4, Lcom/android/facelock/FaceLockService;->mDetectionTimeout:I

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mDetectionTimeout:I

    const-string v0, "facelock_recognition_timeout"

    sget v4, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    const-string v0, "facelock_liveliness_timeout"

    sget v4, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    const-string v0, "facelock_est_max_time"

    sget v4, Lcom/android/facelock/FaceLockService;->mEstimatedMaxProcessingTime:I

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/facelock/FaceLockService;->mEstimatedMaxProcessingTime:I

    const-string v4, "facelock_use_intro_animation"

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    sget v0, Lcom/android/facelock/FaceLockService;->mRecognitionTimeout:I

    sget v1, Lcom/android/facelock/FaceLockService;->mLivelinessTimeout:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/facelock/FaceLockService;->mEstimatedMaxProcessingTime:I

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    sput v0, Lcom/android/facelock/FaceLockService;->mWatchdogTimeout:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private watchdogTimeout(J)Z
    .locals 2
    .param p1    # J

    sget v0, Lcom/android/facelock/FaceLockService;->mWatchdogTimeout:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->watchdogTimeout()V

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public doCallback(I)V
    .locals 6
    .param p1    # I

    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    packed-switch p1, :pswitch_data_0

    :try_start_0
    const-string v4, "FULFaceLockService"

    const-string v5, "Unknown callback type"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/android/internal/policy/IFaceLockCallback;

    invoke-interface {v4}, Lcom/android/internal/policy/IFaceLockCallback;->unlock()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v4, "FULFaceLockService"

    const-string v5, "Remote exception during unlock"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    :try_start_1
    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/android/internal/policy/IFaceLockCallback;

    invoke-interface {v4}, Lcom/android/internal/policy/IFaceLockCallback;->cancel()V

    goto :goto_1

    :pswitch_2
    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/android/internal/policy/IFaceLockCallback;

    invoke-interface {v4}, Lcom/android/internal/policy/IFaceLockCallback;->reportFailedAttempt()V

    goto :goto_1

    :pswitch_3
    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/android/internal/policy/IFaceLockCallback;

    invoke-interface {v4}, Lcom/android/internal/policy/IFaceLockCallback;->exposeFallback()V

    goto :goto_1

    :pswitch_4
    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/android/internal/policy/IFaceLockCallback;

    sget v5, Lcom/android/facelock/FaceLockService;->DEFAULT_AWAKE_INTERVAL_MS:I

    invoke-interface {v4, v5}, Lcom/android/internal/policy/IFaceLockCallback;->pokeWakelock(I)V

    goto :goto_1

    :pswitch_5
    const/16 v0, 0x12c

    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/android/internal/policy/IFaceLockCallback;

    sget v5, Lcom/android/facelock/FaceLockService;->mWatchdogTimeout:I

    add-int/lit16 v5, v5, 0x12c

    invoke-interface {v4, v5}, Lcom/android/internal/policy/IFaceLockCallback;->pokeWakelock(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_0
    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "FULFaceLockService"

    const-string v2, "Invalid message"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->interruptThread()V

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->startShutdown()V

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    const-string v0, "FULFaceLockService"

    const-string v3, "add view"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "FULFaceLockService"

    const-string v1, "View could not open"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    iput-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mGotFirstFrame:Z

    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/android/facelock/FaceLockService$InitializeState;->SET_VIEW:Lcom/android/facelock/FaceLockService$InitializeState;

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :pswitch_1
    const-string v0, "FULFaceLockService"

    const-string v1, "remove view"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->removeView()V

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :pswitch_2
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_4
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "black screen timeout"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    const v0, 0x7f06001f

    const v3, 0x7f050004

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_2
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto :goto_1

    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_2

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :pswitch_3
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_6
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "detection timeout"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    const v0, 0x7f06001b

    const v3, 0x7f050001

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_3
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_5
    :try_start_7
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_3

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :pswitch_4
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_8
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recognition timeout"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    const v0, 0x7f06001c

    const v3, 0x7f050002

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_4
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_6
    :try_start_9
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_4

    :catchall_4
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :pswitch_5
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_a
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "liveliness timeout"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    const v0, 0x7f06001e

    const v3, 0x7f050002

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_5
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_7
    :try_start_b
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_5

    :catchall_5
    move-exception v0

    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v0

    :pswitch_6
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_c
    const-string v0, "FULFaceLockService"

    const-string v3, "watchdog timeout"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->startShutdown()V

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :catchall_6
    move-exception v0

    :try_start_d
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v0

    :pswitch_7
    const-string v0, "FULFaceLockService"

    const-string v3, "initialize error"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    iget v3, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    :goto_6
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    const v3, 0x7f050003

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    sget v3, Lcom/android/facelock/FaceLockService;->CANCEL_WAIT:I

    int-to-long v3, v3

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    const v3, 0x7f060020

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :pswitch_8
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_e
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "need frame ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/facelock/FaceLockUtil;->sUnlockStateString:[Ljava/lang/String;

    iget v5, p1, Landroid/os/Message;->arg2:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v2, :cond_9

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    :cond_9
    monitor-exit v1

    goto/16 :goto_1

    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    throw v0

    :pswitch_9
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_f
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "allow access"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_b

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->loginComplete()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/android/facelock/Preview;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    :cond_a
    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->closeCamera()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_7
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_b
    :try_start_10
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_7

    :catchall_8
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    throw v0

    :pswitch_a
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_11
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deny access"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_c

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    const v0, 0x7f06001c

    const v3, 0x7f050002

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_8
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_c
    :try_start_12
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_8

    :catchall_9
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_9

    throw v0

    :pswitch_b
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_13
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "liveliness no face"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->livelinessNoFace()V

    const v0, 0x7f06001c

    const v3, 0x7f050002

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_9
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_d
    :try_start_14
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_9

    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    throw v0

    :pswitch_c
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_15
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "liveliness motion"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_e

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    :goto_a
    monitor-exit v1

    goto/16 :goto_1

    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_b

    throw v0

    :cond_e
    :try_start_16
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_b

    goto :goto_a

    :pswitch_d
    const-string v0, "FULFaceLockService"

    const-string v1, "unlock state error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/facelock/Preview;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->startShutdown()V

    goto/16 :goto_1

    :pswitch_e
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_17
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "liveliness start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_f

    const v0, 0x7f06001d

    const v3, 0x7f050005

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStarted:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStartTime:J

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    :goto_b
    monitor-exit v1

    goto/16 :goto_1

    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    throw v0

    :cond_f
    :try_start_18
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_c

    goto :goto_b

    :pswitch_f
    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_19
    const-string v0, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "liveliness deny"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->doneStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-nez v0, :cond_10

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->livelinessFail()V

    const v0, 0x7f06001c

    const v3, 0x7f050002

    invoke-direct {p0, v0, v3}, Lcom/android/facelock/FaceLockService;->displayInfoText(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    invoke-virtual {p0}, Lcom/android/facelock/FaceLockService;->sendCancel()V

    :goto_c
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_d

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->writeEvent()V

    goto/16 :goto_1

    :cond_10
    :try_start_1a
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->doneNeeded()V

    goto :goto_c

    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_d

    throw v0

    :pswitch_10
    const-string v3, "FULFaceLockService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send unlock"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mClosingDown:Z

    if-eqz v0, :cond_11

    const-string v0, " (CLOSING DOWN)"

    :goto_d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mClosingDown:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    goto/16 :goto_1

    :cond_11
    const-string v0, ""

    goto :goto_d

    :pswitch_11
    const-string v0, "FULFaceLockService"

    const-string v1, "send cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->startShutdown()V

    goto/16 :goto_1

    :pswitch_12
    const-string v0, "FULFaceLockService"

    const-string v1, "send close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->removeView()V

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mClosingDown:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->closeCamera()V

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mLivelinessStarted:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mReportUnsuccessfulAttempt:Z

    if-eqz v0, :cond_12

    invoke-virtual {p0, v3}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    :cond_12
    invoke-virtual {p0, v2}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    goto/16 :goto_1

    :pswitch_13
    const-string v0, "FULFaceLockService"

    const-string v1, "send poke wakelock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_b
        :pswitch_c
        :pswitch_10
        :pswitch_11
        :pswitch_13
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_12
    .end packed-switch
.end method

.method public interruptThread()V
    .locals 4

    sget-object v2, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    :cond_0
    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    invoke-virtual {v0}, Lcom/android/facelock/ProcessingThread;->quit()Z

    :cond_1
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 8
    .param p1    # Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mInitializeFailed:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->binder:Lcom/android/internal/policy/IFaceLockInterface$Stub;

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mGotFirstFrame:Z

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/facelock/FaceLockService;->mTimeToFirstFrame:I

    const-string v2, "FULFaceLockService"

    const-string v3, "Camera.open()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/facelock/FaceLockService;->mCameraOpenStartTime:J

    iget v2, p0, Lcom/android/facelock/FaceLockService;->mFrontCameraId:I

    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    const-string v2, "FULFaceLockService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Camera.open() elapsed time (ms): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/facelock/FaceLockService;->mCameraOpenStartTime:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    iget-object v3, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v3}, Lcom/android/facelock/Preview;->setCamera(Landroid/hardware/Camera;)V

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    iget v2, p0, Lcom/android/facelock/FaceLockService;->mFrontCameraId:I

    invoke-static {v2, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v2, v0}, Lcom/android/facelock/Preview;->setCameraInfo(Landroid/hardware/Camera$CameraInfo;)V

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v2, p0}, Lcom/android/facelock/Preview;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    const-string v2, "FULFaceLockService"

    const-string v3, "start preview"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v2}, Lcom/android/facelock/Preview;->start()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->binder:Lcom/android/internal/policy/IFaceLockInterface$Stub;

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "FULFaceLockService"

    const-string v3, "Runtime exception during camera.open"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f060021

    iput v2, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    sget-object v2, Lcom/android/facelock/FaceLockService$InitializeState;->INITIALIZE_FAILED:Lcom/android/facelock/FaceLockService$InitializeState;

    invoke-direct {p0, v2}, Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onCreate()V
    .locals 15

    const/4 v14, 0x0

    const-wide/16 v12, 0x12c

    const/4 v11, 0x2

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/facelock/FaceLockService;->mOnCreateStartTime:J

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mInitializeFailed:Z

    iput v2, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mClosingDown:Z

    iput-object v14, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mViewCreated:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mGotFirstFrame:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mFoundFace:Z

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->updateSettings()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/facelock/FaceLockEventLog;

    iget-wide v1, p0, Lcom/android/facelock/FaceLockService;->mOnCreateStartTime:J

    invoke-direct {v0, v1, v2}, Lcom/android/facelock/FaceLockEventLog;-><init>(J)V

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    new-instance v0, Lcom/android/facelock/ProcessingThread;

    const-string v1, "ProcessingThread"

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    iget-wide v4, p0, Lcom/android/facelock/FaceLockService;->mOnCreateStartTime:J

    invoke-direct/range {v0 .. v5}, Lcom/android/facelock/ProcessingThread;-><init>(Ljava/lang/String;Landroid/os/Handler;Lcom/android/facelock/FaceLockEventLog;J)V

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    invoke-virtual {v0}, Lcom/android/facelock/ProcessingThread;->start()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    invoke-virtual {v0}, Lcom/android/facelock/ProcessingThread;->waitUntilReady()V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/android/facelock/FaceLockService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    const v0, 0x7f040001

    invoke-static {p0, v0, v14}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/facelock/Preview;

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mWindowManager:Landroid/view/WindowManager;

    invoke-virtual {v0, v1}, Lcom/android/facelock/Preview;->setWindowManager(Landroid/view/WindowManager;)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    const v1, 0x7f080009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    const v1, 0x7f080007

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/facelock/Spotlight;

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mBackground:Landroid/view/View;

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mBackground:Landroid/view/View;

    const v1, 0x7f020005

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mView:Landroid/view/View;

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mCancelButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCancelButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/facelock/FaceLockService$2;

    invoke-direct {v1, p0}, Lcom/android/facelock/FaceLockService$2;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v0, v11, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/facelock/FaceLockService$3;

    invoke-direct {v1, p0}, Lcom/android/facelock/FaceLockService$3;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/facelock/FaceLockService$4;

    invoke-direct {v1, p0}, Lcom/android/facelock/FaceLockService$4;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mShutdownFader:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-array v0, v11, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoTextCrossFade:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoTextCrossFade:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mInfoTextCrossFade:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/facelock/FaceLockService$5;

    invoke-direct {v1, p0}, Lcom/android/facelock/FaceLockService$5;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-array v0, v11, [F

    fill-array-data v0, :array_2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mCameraStartAnimation:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCameraStartAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCameraStartAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCameraStartAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/facelock/FaceLockService$6;

    invoke-direct {v1, p0}, Lcom/android/facelock/FaceLockService$6;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCameraStartAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/facelock/FaceLockService$7;

    invoke-direct {v1, p0}, Lcom/android/facelock/FaceLockService$7;-><init>(Lcom/android/facelock/FaceLockService;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Lcom/android/facelock/FaceLockPreferences;

    invoke-direct {v0, p0}, Lcom/android/facelock/FaceLockPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreferences:Lcom/android/facelock/FaceLockPreferences;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreferences:Lcom/android/facelock/FaceLockPreferences;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockPreferences;->getCameraDelay()I

    move-result v6

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mUseIntroAnimation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;

    iget-object v0, v0, Lcom/android/facelock/Spotlight;->mIntroAnimation:Landroid/animation/ValueAnimator;

    int-to-long v1, v6

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    :cond_0
    const-string v0, "FULFaceLockService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stored camera delay (ms): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v10

    new-instance v7, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v7}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v10, :cond_1

    invoke-static {v9, v7}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v0, v7, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iput v9, p0, Lcom/android/facelock/FaceLockService;->mFrontCameraId:I

    const/4 v8, 0x1

    :cond_1
    if-nez v8, :cond_4

    const-string v0, "FULFaceLockService"

    const-string v1, "Front camera not found"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f060021

    iput v0, p0, Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I

    sget-object v0, Lcom/android/facelock/FaceLockService$InitializeState;->INITIALIZE_FAILED:Lcom/android/facelock/FaceLockService$InitializeState;

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;

    invoke-virtual {v0}, Lcom/android/facelock/Spotlight;->disableAnimation()V

    goto/16 :goto_0

    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->initializeIfNecessary()V

    goto :goto_2

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000
    .end array-data
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 7

    const/4 v5, 0x1

    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mDone:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v0}, Lcom/android/facelock/Preview;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mGotFirstFrame:Z

    if-nez v1, :cond_2

    const-string v1, "FULFaceLockService"

    const-string v2, "got first frame"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/android/facelock/FaceLockService;->mGotFirstFrame:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/facelock/FaceLockService;->mFirstFrameTime:J

    iget-wide v1, p0, Lcom/android/facelock/FaceLockService;->mFirstFrameTime:J

    iget-wide v3, p0, Lcom/android/facelock/FaceLockService;->mCameraOpenStartTime:J

    sub-long/2addr v1, v3

    long-to-int v1, v1

    iput v1, p0, Lcom/android/facelock/FaceLockService;->mTimeToFirstFrame:I

    const-string v1, "FULFaceLockService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Before camera open to first preview frame elapsed time (ms): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/facelock/FaceLockService;->mTimeToFirstFrame:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    :cond_2
    iget-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mReachedNonBlackFrame:Z

    if-nez v1, :cond_4

    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-static {p1, v1, v2}, Lcom/android/facelock/FaceLockUtil;->unlockIsBlackScreen([BII)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v0}, Lcom/android/facelock/FaceLockEventLog;->blackFrame()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/facelock/FaceLockService;->mFirstFrameTime:J

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/android/facelock/FaceLockService;->blackFrameTimeout(J)Z

    goto :goto_0

    :cond_3
    iput-boolean v5, p0, Lcom/android/facelock/FaceLockService;->mReachedNonBlackFrame:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/facelock/FaceLockService;->mFirstNonBlackFrameTime:J

    const-string v1, "FULFaceLockService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "First frame to first non-black frame elapsed time (ms): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/facelock/FaceLockService;->mFirstNonBlackFrameTime:J

    iget-wide v5, p0, Lcom/android/facelock/FaceLockService;->mFirstFrameTime:J

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/android/facelock/FaceLockService;->doCallback(I)V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mCameraStartAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    iget-object v1, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    invoke-virtual {v1}, Lcom/android/facelock/ProcessingThread;->getThreadId()I

    move-result v1

    const/4 v2, -0x2

    invoke-static {v1, v2}, Landroid/os/Process;->setThreadPriority(II)V

    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/facelock/FaceLockService;->mFirstNonBlackFrameTime:J

    sub-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lcom/android/facelock/FaceLockService;->watchdogTimeout(J)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/facelock/FaceLockService;->mModelsLoaded:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    if-eqz v2, :cond_9

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/facelock/FaceLockService;->mFirstNonBlackFrameTime:J

    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/android/facelock/FaceLockService;->detectionTimeout(J)Z

    move-result v4

    if-eqz v4, :cond_5

    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    :try_start_1
    invoke-direct {p0, v2, v3}, Lcom/android/facelock/FaceLockService;->recognitionTimeout(J)Z

    move-result v4

    if-eqz v4, :cond_6

    monitor-exit v1

    goto/16 :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->livelinessTimeout()Z

    move-result v4

    if-eqz v4, :cond_7

    monitor-exit v1

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0, v2, v3}, Lcom/android/facelock/FaceLockService;->watchdogTimeout(J)Z

    move-result v2

    if-eqz v2, :cond_8

    monitor-exit v1

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mNeedFrame:Z

    iget-object v2, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    iget-object v3, p0, Lcom/android/facelock/FaceLockService;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v3}, Lcom/android/facelock/Preview;->getPreviewRotation()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/facelock/ProcessingThread;->setRotation(I)V

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v2, v3

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    invoke-static {p1, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/android/facelock/FaceLockService;->mThread:Lcom/android/facelock/ProcessingThread;

    invoke-virtual {v3}, Lcom/android/facelock/ProcessingThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v4, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_9
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockService;->mClosingDown:Z

    invoke-direct {p0}, Lcom/android/facelock/FaceLockService;->closeCamera()V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/facelock/FaceLockService;->mPreviewShowing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    iget v0, p0, Lcom/android/facelock/FaceLockService;->mTimeToFirstFrame:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mPreferences:Lcom/android/facelock/FaceLockPreferences;

    iget v1, p0, Lcom/android/facelock/FaceLockService;->mTimeToFirstFrame:I

    invoke-virtual {v0, v1}, Lcom/android/facelock/FaceLockPreferences;->updateCameraDelay(I)V

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public sendCancel()V
    .locals 4

    sget-object v1, Lcom/android/facelock/FaceLockService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/facelock/FaceLockService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    sget v2, Lcom/android/facelock/FaceLockService;->CANCEL_WAIT:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
