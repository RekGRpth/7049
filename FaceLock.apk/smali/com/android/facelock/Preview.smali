.class public Lcom/android/facelock/Preview;
.super Landroid/view/ViewGroup;
.source "Preview.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field private static sSupportedPreviewSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraInfo:Landroid/hardware/Camera$CameraInfo;

.field private mCameraParameters:Landroid/hardware/Camera$Parameters;

.field private mContext:Landroid/content/Context;

.field private mFade:Landroid/animation/ObjectAnimator;

.field private mLock:Ljava/lang/Object;

.field private mMeteringAreas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

.field private mPreviewSize:Landroid/hardware/Camera$Size;

.field private mPreviewStarted:Z

.field private mRotation:I

.field private mStartRequested:Z

.field private mSurfaceAvailable:Z

.field private mTextureView:Landroid/view/TextureView;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/facelock/Preview;->sSupportedPreviewSizes:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/16 v5, 0xc8

    const/16 v4, -0xc8

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v1, "FULPreview"

    iput-object v1, p0, Lcom/android/facelock/Preview;->TAG:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/facelock/Preview;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/facelock/Preview;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/view/TextureView;

    invoke-direct {v1, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/facelock/Preview;->mTextureView:Landroid/view/TextureView;

    iget-object v1, p0, Lcom/android/facelock/Preview;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v1, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object v1, p0, Lcom/android/facelock/Preview;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {p0, v1}, Lcom/android/facelock/Preview;->addView(Landroid/view/View;)V

    iput-boolean v2, p0, Lcom/android/facelock/Preview;->mSurfaceAvailable:Z

    iput-boolean v2, p0, Lcom/android/facelock/Preview;->mPreviewStarted:Z

    iput-boolean v2, p0, Lcom/android/facelock/Preview;->mStartRequested:Z

    iget-object v1, p0, Lcom/android/facelock/Preview;->mTextureView:Landroid/view/TextureView;

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/facelock/Preview;->mFade:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/facelock/Preview;->mFade:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/facelock/Preview;->mMeteringAreas:Ljava/util/List;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v4, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lcom/android/facelock/Preview;->mMeteringAreas:Ljava/util/List;

    new-instance v2, Landroid/hardware/Camera$Area;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/android/facelock/Preview;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/facelock/Preview;

    iget-object v0, p0, Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/facelock/Preview;)Z
    .locals 1
    .param p0    # Lcom/android/facelock/Preview;

    iget-boolean v0, p0, Lcom/android/facelock/Preview;->mSurfaceAvailable:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/facelock/Preview;)Landroid/hardware/Camera;
    .locals 1
    .param p0    # Lcom/android/facelock/Preview;

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/facelock/Preview;)Landroid/view/TextureView;
    .locals 1
    .param p0    # Lcom/android/facelock/Preview;

    iget-object v0, p0, Lcom/android/facelock/Preview;->mTextureView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/facelock/Preview;)V
    .locals 0
    .param p0    # Lcom/android/facelock/Preview;

    invoke-direct {p0}, Lcom/android/facelock/Preview;->setupPreview()V

    return-void
.end method

.method private getCameraParameters()Landroid/hardware/Camera$Parameters;
    .locals 1

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/Preview;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    :cond_0
    iget-object v0, p0, Lcom/android/facelock/Preview;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method private getOptimalPreviewSize(II)Landroid/hardware/Camera$Size;
    .locals 9
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Lcom/android/facelock/Preview;->isPortraitMode()Z

    move-result v7

    if-eqz v7, :cond_0

    move v6, p1

    move p1, p2

    move p2, v6

    :cond_0
    sget-object v7, Lcom/android/facelock/Preview;->sSupportedPreviewSizes:Ljava/util/List;

    if-nez v7, :cond_2

    const/4 v4, 0x0

    :cond_1
    return-object v4

    :cond_2
    const/4 v4, 0x0

    const-wide v2, 0x7fefffffffffffffL

    sget-object v7, Lcom/android/facelock/Preview;->sSupportedPreviewSizes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/Camera$Size;

    iget v7, v5, Landroid/hardware/Camera$Size;->width:I

    sub-int/2addr v7, p1

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    iget v8, v5, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v8, p2

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    add-int v0, v7, v8

    int-to-double v7, v0

    cmpg-double v7, v7, v2

    if-gez v7, :cond_3

    move-object v4, v5

    int-to-double v2, v0

    goto :goto_0
.end method

.method private isPortraitMode()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/facelock/Preview;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v0, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne v0, v2, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method private setupPreview()V
    .locals 7

    iget-object v5, p0, Lcom/android/facelock/Preview;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v4

    const/4 v1, 0x0

    packed-switch v4, :pswitch_data_0

    const-string v5, "FULPreview"

    const-string v6, "Bad rotation value in setupPreview()"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v5, p0, Lcom/android/facelock/Preview;->mCameraInfo:Landroid/hardware/Camera$CameraInfo;

    iget v5, v5, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v5, v1

    rem-int/lit16 v0, v5, 0x168

    iget-object v5, p0, Lcom/android/facelock/Preview;->mCameraInfo:Landroid/hardware/Camera$CameraInfo;

    iget v5, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    rsub-int v5, v0, 0x168

    rem-int/lit16 v0, v5, 0x168

    :cond_0
    iget-object v5, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v5, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    div-int/lit8 v5, v0, 0x5a

    iput v5, p0, Lcom/android/facelock/Preview;->mRotation:I

    invoke-direct {p0}, Lcom/android/facelock/Preview;->getCameraParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    iget-object v5, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget v5, v5, Landroid/hardware/Camera$Size;->width:I

    iget-object v6, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget v6, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v5, v6}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v5

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/android/facelock/Preview;->mMeteringAreas:Ljava/util/List;

    invoke-virtual {v3, v5}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    :cond_1
    iget-object v5, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v5, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v5, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    iget-object v6, p0, Lcom/android/facelock/Preview;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    invoke-virtual {v5, v6}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v5, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v5}, Landroid/hardware/Camera;->startPreview()V

    return-void

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/16 v1, 0x5a

    goto :goto_0

    :pswitch_2
    const/16 v1, 0xb4

    goto :goto_0

    :pswitch_3
    const/16 v1, 0x10e

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private startPreviewIfReady()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/android/facelock/Preview;->mStartRequested:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/android/facelock/Preview;->mPreviewStarted:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/android/facelock/Preview;->mSurfaceAvailable:Z

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v3, p0, Lcom/android/facelock/Preview;->mPreviewStarted:Z

    or-int/2addr v3, v0

    iput-boolean v3, p0, Lcom/android/facelock/Preview;->mPreviewStarted:Z

    monitor-exit v2

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    new-instance v2, Lcom/android/facelock/Preview$1;

    invoke-direct {v2, p0}, Lcom/android/facelock/Preview$1;-><init>(Lcom/android/facelock/Preview;)V

    invoke-virtual {v2}, Lcom/android/facelock/Preview$1;->start()V

    iput-boolean v1, p0, Lcom/android/facelock/Preview;->mStartRequested:Z

    goto :goto_1
.end method


# virtual methods
.method public fade()V
    .locals 1

    iget-object v0, p0, Lcom/android/facelock/Preview;->mFade:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public getPreviewRotation()I
    .locals 1

    iget v0, p0, Lcom/android/facelock/Preview;->mRotation:I

    return v0
.end method

.method public getPreviewSize()Landroid/hardware/Camera$Size;
    .locals 1

    iget-object v0, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/android/facelock/Preview;->getChildCount()I

    move-result v10

    if-lez v10, :cond_2

    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/android/facelock/Preview;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sub-int v9, p4, p2

    sub-int v2, p5, p3

    iget-object v10, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    if-nez v10, :cond_0

    invoke-direct {p0, v9, v2}, Lcom/android/facelock/Preview;->getOptimalPreviewSize(II)Landroid/hardware/Camera$Size;

    move-result-object v10

    iput-object v10, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    :cond_0
    iget-object v10, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget v4, v10, Landroid/hardware/Camera$Size;->width:I

    iget-object v10, p0, Lcom/android/facelock/Preview;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget v3, v10, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {p0}, Lcom/android/facelock/Preview;->isPortraitMode()Z

    move-result v10

    if-eqz v10, :cond_1

    move v7, v4

    move v4, v3

    move v3, v7

    :cond_1
    mul-int v10, v9, v3

    mul-int v11, v2, v4

    if-le v10, v11, :cond_3

    mul-int v10, v9, v3

    div-int v5, v10, v4

    sub-int v8, v2, v5

    const/4 v10, 0x0

    add-int v11, v5, v8

    invoke-virtual {v0, v10, v8, v9, v11}, Landroid/view/View;->layout(IIII)V

    :goto_0
    invoke-direct {p0}, Lcom/android/facelock/Preview;->startPreviewIfReady()V

    :cond_2
    return-void

    :cond_3
    mul-int v10, v2, v4

    div-int v6, v10, v3

    sub-int v10, v9, v6

    div-int/lit8 v1, v10, 0x2

    const/4 v10, 0x0

    add-int v11, v6, v1

    invoke-virtual {v0, v1, v10, v11, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/facelock/Preview;->mSurfaceAvailable:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/android/facelock/Preview;->start()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/Preview;->mSurfaceAvailable:Z

    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;

    return-void
.end method

.method public setCamera(Landroid/hardware/Camera;)V
    .locals 2
    .param p1    # Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/Preview;->mPreviewStarted:Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/facelock/Preview;->sSupportedPreviewSizes:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/facelock/Preview;->getCameraParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/facelock/Preview;->sSupportedPreviewSizes:Ljava/util/List;

    :cond_1
    invoke-virtual {p0}, Lcom/android/facelock/Preview;->requestLayout()V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setCameraInfo(Landroid/hardware/Camera$CameraInfo;)V
    .locals 0
    .param p1    # Landroid/hardware/Camera$CameraInfo;

    iput-object p1, p0, Lcom/android/facelock/Preview;->mCameraInfo:Landroid/hardware/Camera$CameraInfo;

    return-void
.end method

.method public setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 2
    .param p1    # Landroid/hardware/Camera$PreviewCallback;

    iput-object p1, p0, Lcom/android/facelock/Preview;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/android/facelock/Preview;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    :cond_0
    return-void
.end method

.method public setWindowManager(Landroid/view/WindowManager;)V
    .locals 0
    .param p1    # Landroid/view/WindowManager;

    iput-object p1, p0, Lcom/android/facelock/Preview;->mWindowManager:Landroid/view/WindowManager;

    return-void
.end method

.method public start()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/Preview;->mStartRequested:Z

    invoke-direct {p0}, Lcom/android/facelock/Preview;->startPreviewIfReady()V

    return-void
.end method
