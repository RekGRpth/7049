.class public final Lcom/android/facelock/ProcessingThread;
.super Landroid/os/HandlerThread;
.source "ProcessingThread.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final mLock:Ljava/lang/Object;


# instance fields
.field private mDoingLiveliness:Z

.field private mEventLog:Lcom/android/facelock/FaceLockEventLog;

.field private mHandler:Landroid/os/Handler;

.field private mOnCreateStartTime:J

.field private mRotation:I

.field private final mServiceHandler:Landroid/os/Handler;

.field private mUseLiveliness:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/facelock/ProcessingThread;->mLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;Lcom/android/facelock/FaceLockEventLog;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Handler;
    .param p3    # Lcom/android/facelock/FaceLockEventLog;
    .param p4    # J

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput v0, p0, Lcom/android/facelock/ProcessingThread;->mRotation:I

    iput-boolean v0, p0, Lcom/android/facelock/ProcessingThread;->mUseLiveliness:Z

    iput-boolean v0, p0, Lcom/android/facelock/ProcessingThread;->mDoingLiveliness:Z

    iput-object p2, p0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    iput-wide p4, p0, Lcom/android/facelock/ProcessingThread;->mOnCreateStartTime:J

    return-void
.end method

.method private compareImage([BII)V
    .locals 20
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/facelock/ProcessingThread;->mDoingLiveliness:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    new-array v7, v2, [F

    const/4 v2, 0x1

    new-array v8, v2, [F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v17

    sget-object v9, Lcom/android/facelock/ProcessingThread;->mLock:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/facelock/ProcessingThread;->mUseLiveliness:Z

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/facelock/ProcessingThread;->mRotation:I

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-static/range {v2 .. v8}, Lcom/android/facelock/FaceLockUtil;->unlockCompareFace([BIIZI[F[F)I

    move-result v19

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v15

    const-string v2, "FULProcessingThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "comparison time (ms): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v15, v17

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v19, :pswitch_data_0

    :pswitch_0
    const-string v2, "FULProcessingThread"

    const-string v3, "Invalid return value"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v7, v3

    const/4 v4, 0x0

    aget v4, v8, v4

    invoke-virtual {v2, v3, v4}, Lcom/android/facelock/FaceLockEventLog;->updateConfidences(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v7, v3

    const/4 v4, 0x0

    aget v4, v8, v4

    invoke-virtual {v2, v3, v4}, Lcom/android/facelock/FaceLockEventLog;->updateConfidences(FF)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/facelock/ProcessingThread;->mDoingLiveliness:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v7, v3

    const/4 v4, 0x0

    aget v4, v8, v4

    invoke-virtual {v2, v3, v4}, Lcom/android/facelock/FaceLockEventLog;->updateConfidences(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v7, v3

    const/4 v4, 0x0

    aget v4, v8, v4

    invoke-virtual {v2, v3, v4}, Lcom/android/facelock/FaceLockEventLog;->updateConfidences(FF)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/android/facelock/ProcessingThread;->sendNeedFrameMessage(ZI)V

    goto :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v8, v3

    invoke-virtual {v2, v3}, Lcom/android/facelock/FaceLockEventLog;->updateDetectionConfidence(F)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/android/facelock/ProcessingThread;->sendNeedFrameMessage(ZI)V

    goto :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v2}, Lcom/android/facelock/FaceLockEventLog;->noTemplate()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v8, v3

    invoke-virtual {v2, v3}, Lcom/android/facelock/FaceLockEventLog;->updateDetectionConfidence(F)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/android/facelock/ProcessingThread;->sendNeedFrameMessage(ZI)V

    goto/16 :goto_0

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    const/4 v3, 0x0

    aget v3, v8, v3

    invoke-virtual {v2, v3}, Lcom/android/facelock/FaceLockEventLog;->updateDetectionConfidence(F)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/android/facelock/ProcessingThread;->sendNeedFrameMessage(ZI)V

    goto/16 :goto_0

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_0
    const/4 v2, 0x1

    new-array v13, v2, [Z

    const/4 v2, 0x1

    new-array v14, v2, [Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v17

    sget-object v3, Lcom/android/facelock/ProcessingThread;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/facelock/ProcessingThread;->mRotation:I

    move-object/from16 v9, p1

    move/from16 v10, p2

    move/from16 v11, p3

    invoke-static/range {v9 .. v14}, Lcom/android/facelock/FaceLockUtil;->unlockDetectLiveliness([BIII[Z[Z)I

    move-result v19

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v15

    const-string v2, "FULProcessingThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "liveliness time (ms): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v15, v17

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v19, :pswitch_data_1

    :pswitch_9
    const-string v2, "FULProcessingThread"

    const-string v3, "Invalid return value"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_c
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/android/facelock/ProcessingThread;->sendNeedFrameMessage(ZI)V

    goto/16 :goto_0

    :pswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mEventLog:Lcom/android/facelock/FaceLockEventLog;

    invoke-virtual {v2}, Lcom/android/facelock/FaceLockEventLog;->motionTriggered()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_9
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private sendNeedFrameMessage(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    iget-object v2, p0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v3, v1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/facelock/ProcessingThread;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/facelock/ProcessingThread;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/facelock/ProcessingThread;->compareImage([BII)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setRotation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/facelock/ProcessingThread;->mRotation:I

    return-void
.end method

.method public setUseLiveliness(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/facelock/ProcessingThread;->mUseLiveliness:Z

    return-void
.end method

.method public declared-synchronized waitUntilReady()V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/android/facelock/ProcessingThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/facelock/ProcessingThread;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
