.class Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;
.super Ljava/lang/Thread;
.source "SetupFaceLock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->onPreviewFrame([BLandroid/hardware/Camera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;


# direct methods
.method constructor <init>(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPreviewData:[B
    invoke-static {}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$300()[B

    move-result-object v5

    iget-object v6, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;
    invoke-static {v6}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$400(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Lcom/android/facelock/Preview;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/facelock/Preview;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v6

    iget v6, v6, Landroid/hardware/Camera$Size;->width:I

    iget-object v7, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;
    invoke-static {v7}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$400(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Lcom/android/facelock/Preview;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/facelock/Preview;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v7

    iget v7, v7, Landroid/hardware/Camera$Size;->height:I

    iget-object v8, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;
    invoke-static {v8}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$400(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Lcom/android/facelock/Preview;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/facelock/Preview;->getPreviewRotation()I

    move-result v8

    invoke-static {v5, v6, v7, v8}, Lcom/android/facelock/FaceLockUtil;->setupEnroll([BIII)I

    move-result v5

    # setter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollState:I
    invoke-static {v4, v5}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$202(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;I)I

    const-string v4, "FULSetupFaceLock"

    sget-object v5, Lcom/android/facelock/FaceLockUtil;->sEnrollStateString:[Ljava/lang/String;

    iget-object v6, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollState:I
    invoke-static {v6}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$200(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)I

    move-result v6

    aget-object v5, v5, v6

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v2

    const-string v4, "FULSetupFaceLock"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "enroll time (ms): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollState:I
    invoke-static {v4}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$200(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    const-wide/16 v5, 0x0

    # setter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLastFrameTime:J
    invoke-static {v4, v5, v6}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$502(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;J)J

    :cond_0
    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPaused:Z
    invoke-static {}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$600()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$800(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;->this$0:Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    # getter for: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$700(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x1

    # invokes: Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->setFinalize(ZZ)Z
    invoke-static {v4, v5}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->access$900(ZZ)Z

    return-void
.end method
