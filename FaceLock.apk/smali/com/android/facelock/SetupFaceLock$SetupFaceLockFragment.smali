.class public Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;
.super Landroid/preference/PreferenceFragment;
.source "SetupFaceLock.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/facelock/Draw$DrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/facelock/SetupFaceLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SetupFaceLockFragment"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static mRotation:I

.field private static volatile sCount:I

.field private static volatile sEnrollmentComplete:Z

.field private static volatile sFinalize:Z

.field private static sFoundId:Z

.field private static sFrontCameraId:I

.field private static volatile sInitialized:Z

.field private static volatile sPaused:Z

.field private static volatile sPreviewData:[B

.field private static volatile sThreadRunning:Z


# instance fields
.field private mAddToSetup:Z

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraOpenStartTime:J

.field private final mContinueRunnable:Ljava/lang/Runnable;

.field private final mEnrollRunnable:Ljava/lang/Runnable;

.field private mEnrollState:I

.field private mGotFirstFrame:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHeaderText:Landroid/widget/TextView;

.field private mLastFrameTime:J

.field private mLeftButton:Landroid/widget/Button;

.field private mOnResumePending:Z

.field private mOval:Lcom/android/facelock/Draw;

.field private mPreferences:Lcom/android/facelock/FaceLockPreferences;

.field private mPreview:Lcom/android/facelock/Preview;

.field private mRightButton:Landroid/widget/Button;

.field private mStartTime:J

.field private mStatusText:Landroid/widget/TextView;

.field private mTimeToFirstFrame:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/android/facelock/SetupFaceLock;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->$assertionsDisabled:Z

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sThreadRunning:Z

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sEnrollmentComplete:Z

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFinalize:Z

    sput-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPaused:Z

    const/4 v0, -0x1

    sput v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFoundId:Z

    :goto_1
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-static {v2, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v3, v1, :cond_2

    sput v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFrontCameraId:I

    sput-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFoundId:Z

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHandler:Landroid/os/Handler;

    iput-boolean v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mGotFirstFrame:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLastFrameTime:J

    iput-boolean v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mAddToSetup:Z

    new-instance v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$1;

    invoke-direct {v0, p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$1;-><init>(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)V

    iput-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$2;

    invoke-direct {v0, p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$2;-><init>(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)V

    iput-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mContinueRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)V
    .locals 0
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    invoke-direct {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->displayResult()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)V
    .locals 0
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    invoke-direct {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->enableContinue()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)I
    .locals 1
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    iget v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollState:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;I)I
    .locals 0
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;
    .param p1    # I

    iput p1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollState:I

    return p1
.end method

.method static synthetic access$300()[B
    .locals 1

    sget-object v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPreviewData:[B

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Lcom/android/facelock/Preview;
    .locals 1
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;J)J
    .locals 0
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLastFrameTime:J

    return-wide p1
.end method

.method static synthetic access$600()Z
    .locals 1

    sget-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPaused:Z

    return v0
.end method

.method static synthetic access$700(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(ZZ)Z
    .locals 1
    .param p0    # Z
    .param p1    # Z

    invoke-static {p0, p1}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->setFinalize(ZZ)Z

    move-result v0

    return v0
.end method

.method private displayResult()V
    .locals 6

    const v5, 0x7f060006

    const/high16 v4, 0x7f050000

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mEnrollState:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iput-boolean v2, v0, Lcom/android/facelock/Draw;->mEnrolling:Z

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, v1}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->keepScreenOn(Z)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    invoke-virtual {v0}, Lcom/android/facelock/Draw;->newFace()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->keepScreenOn(Z)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iput-boolean v2, v0, Lcom/android/facelock/Draw;->mEnrolling:Z

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f060007

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->keepScreenOn(Z)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iput-boolean v2, v0, Lcom/android/facelock/Draw;->mEnrolling:Z

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f06000a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v1}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->keepScreenOn(Z)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iput-boolean v2, v0, Lcom/android/facelock/Draw;->mEnrolling:Z

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f060009

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_4
    invoke-direct {p0, v2}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->keepScreenOn(Z)V

    sget-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sEnrollmentComplete:Z

    if-nez v0, :cond_0

    sput-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sEnrollmentComplete:Z

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    invoke-virtual {v0}, Lcom/android/facelock/Draw;->finish()V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private enableContinue()V
    .locals 2

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const v1, 0x7f06000b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/facelock/Preview;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRightButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private getFilesPath()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "FULSetupFaceLock"

    const-string v2, "IOException in getFilesPath()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private keepOrientation()V
    .locals 10

    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v3

    sput v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    :cond_0
    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v2, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x0

    if-ne v2, v6, :cond_1

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-eqz v3, :cond_2

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-eq v3, v6, :cond_2

    :cond_1
    if-ne v2, v5, :cond_3

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-eq v3, v5, :cond_2

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v7, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    if-eqz v1, :cond_8

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v6, :cond_6

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_6
    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v5, :cond_7

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_7
    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v7, :cond_4

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_8
    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-eqz v3, :cond_9

    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v6, :cond_a

    :cond_9
    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_a
    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v5, :cond_b

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_b
    sget v3, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    if-ne v3, v7, :cond_4

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method private keepScreenOn(Z)V
    .locals 2
    .param p1    # Z

    const/16 v0, 0x80

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private static declared-synchronized setFinalize(ZZ)Z
    .locals 3
    .param p0    # Z
    .param p1    # Z

    const/4 v0, 0x0

    const-class v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    monitor-enter v1

    if-nez p1, :cond_0

    if-eqz p0, :cond_5

    :try_start_0
    sget-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sThreadRunning:Z

    if-nez v2, :cond_5

    :cond_0
    sget-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFinalize:Z

    if-nez v2, :cond_1

    if-eqz p0, :cond_3

    :cond_1
    sget-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setupFinalize()V

    :cond_2
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFinalize:Z

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    :cond_3
    if-eqz p1, :cond_4

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sThreadRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_0
    monitor-exit v1

    return v0

    :cond_5
    :try_start_1
    sput-boolean p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFinalize:Z

    sget-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized setThreadRunning(Z)Z
    .locals 3
    .param p0    # Z

    const-class v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sEnrollmentComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v2

    return v0

    :cond_0
    :try_start_1
    sget-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sThreadRunning:Z

    sput-boolean p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sThreadRunning:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080003

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080004

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-boolean v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mAddToSetup:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setupWriteGallery()Z

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setupWriteTempGallery()Z

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PendingIntent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/app/PendingIntent;

    if-eqz v7, :cond_3

    :try_start_0
    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v7}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v0, "FULSetupFaceLock"

    const-string v1, "pendingIntent was null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    const-string v0, "FULSetupFaceLock"

    const-string v1, "Sending pendingIntent failed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->keepOrientation()V

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "addToSetup"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mAddToSetup:Z

    iget-boolean v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mAddToSetup:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060016

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v5, 0x7f060020

    const/4 v4, 0x0

    const v1, 0x7f040002

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/facelock/Preview;

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    iget-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v3, "window"

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-virtual {v2, v1}, Lcom/android/facelock/Preview;->setWindowManager(Landroid/view/WindowManager;)V

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v1, p0}, Lcom/android/facelock/Preview;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/facelock/Draw;

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    invoke-virtual {v1, p0}, Lcom/android/facelock/Draw;->setListener(Lcom/android/facelock/Draw$DrawListener;)V

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStatusText:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLeftButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRightButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-boolean v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mAddToSetup:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRightButton:Landroid/widget/Button;

    const v2, 0x7f060004

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    :cond_0
    sget-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFoundId:Z

    if-nez v1, :cond_1

    const-string v1, "FULSetupFaceLock"

    const-string v2, "Front camera not found"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const v2, 0x7f060021

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-object v0

    :cond_1
    invoke-static {v4, v4}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->setFinalize(ZZ)Z

    move-result v1

    if-nez v1, :cond_2

    sput v4, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sCount:I

    sput-boolean v4, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sEnrollmentComplete:Z

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getFilesPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/android/facelock/FaceLockUtil;->initialize(ZLandroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    :cond_2
    sget-boolean v1, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    if-nez v1, :cond_3

    const-string v1, "FULSetupFaceLock"

    const-string v2, "initialization failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mAddToSetup:Z

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setupReadGallery()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "FULSetupFaceLock"

    const-string v2, "Read gallery failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v4, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    sget v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sCount:I

    iput v2, v1, Lcom/android/facelock/Draw;->mCount:I

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setupGetMaxEnrollImages()I

    move-result v2

    iput v2, v1, Lcom/android/facelock/Draw;->mMaxCount:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    sput v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mRotation:I

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->setFinalize(ZZ)Z

    return-void
.end method

.method public onDrawComplete()V
    .locals 2

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mContinueRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onPause()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOnResumePending:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPaused:Z

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v0, v1}, Lcom/android/facelock/Preview;->setCamera(Landroid/hardware/Camera;)V

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    iput-object v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    :cond_0
    iget v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mTimeToFirstFrame:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreferences:Lcom/android/facelock/FaceLockPreferences;

    iget v1, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mTimeToFirstFrame:I

    invoke-virtual {v0, v1}, Lcom/android/facelock/FaceLockPreferences;->updateCameraDelay(I)V

    :cond_1
    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 11
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    invoke-virtual {v5}, Lcom/android/facelock/Draw;->isDone()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v5}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v5}, Lcom/android/facelock/Preview;->fade()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mGotFirstFrame:Z

    if-nez v5, :cond_2

    iput-boolean v10, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mGotFirstFrame:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCameraOpenStartTime:J

    sub-long/2addr v5, v7

    long-to-int v5, v5

    iput v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mTimeToFirstFrame:I

    const-string v5, "FULSetupFaceLock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Before camera open to first preview frame (ms): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mTimeToFirstFrame:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sget-boolean v5, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sThreadRunning:Z

    if-nez v5, :cond_3

    iget-wide v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLastFrameTime:J

    const-wide/16 v7, 0x294

    add-long/2addr v5, v7

    cmp-long v5, v0, v5

    if-ltz v5, :cond_0

    :cond_3
    invoke-static {v10}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->setThreadRunning(Z)Z

    move-result v5

    if-nez v5, :cond_0

    iput-wide v0, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mLastFrameTime:J

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iget-boolean v5, v5, Lcom/android/facelock/Draw;->mEnrolling:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, v5, Lcom/android/facelock/Draw;->mStartTime:J

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mOval:Lcom/android/facelock/Draw;

    iput-boolean v10, v5, Lcom/android/facelock/Draw;->mEnrolling:Z

    :cond_4
    iget-wide v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStartTime:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const v6, 0x7f060006

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStartTime:J

    sput-boolean v9, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPaused:Z

    :cond_5
    iget-object v5, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v5}, Lcom/android/facelock/Preview;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v3

    iget v5, v3, Landroid/hardware/Camera$Size;->width:I

    iget v6, v3, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    new-array v2, v5, [B

    iget v5, v3, Landroid/hardware/Camera$Size;->width:I

    iget v6, v3, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    invoke-static {p1, v9, v2, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sput-object v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sPreviewData:[B

    new-instance v4, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;

    invoke-direct {v4, p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment$3;-><init>(Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->getThreadGroup()Ljava/lang/ThreadGroup;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/ThreadGroup;->getMaxPriority()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 9

    const/4 v8, 0x0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mStartTime:J

    iput v8, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mTimeToFirstFrame:I

    sget-boolean v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Lcom/android/facelock/FaceLockPreferences;

    invoke-virtual {p0}, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/facelock/FaceLockPreferences;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreferences:Lcom/android/facelock/FaceLockPreferences;

    const-string v2, "FULSetupFaceLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stored camera delay (ms): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreferences:Lcom/android/facelock/FaceLockPreferences;

    invoke-virtual {v4}, Lcom/android/facelock/FaceLockPreferences;->getCameraDelay()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v2, "FULSetupFaceLock"

    const-string v3, "Camera.open()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCameraOpenStartTime:J

    sget v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFrontCameraId:I

    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    const-string v2, "FULSetupFaceLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Camera.open() elapsed time (ms): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCameraOpenStartTime:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    iget-object v3, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v3}, Lcom/android/facelock/Preview;->setCamera(Landroid/hardware/Camera;)V

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    sget v2, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sFrontCameraId:I

    invoke-static {v2, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v2, v0}, Lcom/android/facelock/Preview;->setCameraInfo(Landroid/hardware/Camera$CameraInfo;)V

    iget-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mPreview:Lcom/android/facelock/Preview;

    invoke-virtual {v2}, Lcom/android/facelock/Preview;->start()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "FULSetupFaceLock"

    const-string v3, "Runtime exception during camera.open"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v8, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->sInitialized:Z

    iget-object v2, p0, Lcom/android/facelock/SetupFaceLock$SetupFaceLockFragment;->mHeaderText:Landroid/widget/TextView;

    const v3, 0x7f060021

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0
.end method
