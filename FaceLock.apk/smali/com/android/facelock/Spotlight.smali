.class Lcom/android/facelock/Spotlight;
.super Landroid/view/View;
.source "Spotlight.java"


# instance fields
.field private mDestination:Landroid/graphics/Rect;

.field mIntroAnimation:Landroid/animation/ValueAnimator;

.field private mPaint:Landroid/graphics/Paint;

.field mSize:F

.field private mSpot:Landroid/graphics/Bitmap;

.field private mStartAnimation:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x3ca3d70a

    iput v0, p0, Lcom/android/facelock/Spotlight;->mSize:F

    iput-boolean v2, p0, Lcom/android/facelock/Spotlight;->mStartAnimation:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/facelock/Spotlight;->mDestination:Landroid/graphics/Rect;

    const v0, 0x3f4ccccd

    invoke-virtual {p0, v0}, Lcom/android/facelock/Spotlight;->setAlpha(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020004

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/Spotlight;->mSpot:Landroid/graphics/Bitmap;

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/Spotlight;->mIntroAnimation:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mIntroAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mIntroAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/facelock/Spotlight$1;

    invoke-direct {v1, p0}, Lcom/android/facelock/Spotlight$1;-><init>(Lcom/android/facelock/Spotlight;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    return-void

    :array_0
    .array-data 4
        0x3dcccccd
        0x3f800000
    .end array-data
.end method


# virtual methods
.method public disableAnimation()V
    .locals 1

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/android/facelock/Spotlight;->mSize:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/Spotlight;->mStartAnimation:Z

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v3, 0x40000000

    const/high16 v5, 0x3f800000

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/facelock/Spotlight;->mStartAnimation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/facelock/Spotlight;->mStartAnimation:Z

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mIntroAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    iget v0, p0, Lcom/android/facelock/Spotlight;->mSize:F

    const/high16 v2, 0x3fa00000

    mul-float v8, v0, v2

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v2, v5, v8

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    float-to-int v9, v0

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v2, v5, v8

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    float-to-int v11, v0

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mDestination:Landroid/graphics/Rect;

    int-to-float v2, v9

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v8

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v3, v11

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v9, v11, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    cmpg-float v0, v8, v5

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mDestination:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int v10, v9, v0

    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mDestination:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int v12, v11, v0

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getWidth()I

    move-result v0

    if-le v10, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getWidth()I

    move-result v10

    :cond_1
    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v0

    if-le v12, v0, :cond_2

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v12

    :cond_2
    int-to-float v3, v9

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    int-to-float v3, v9

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getWidth()I

    move-result v0

    int-to-float v5, v0

    int-to-float v6, v11

    iget-object v7, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    int-to-float v1, v10

    int-to-float v2, v11

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    int-to-float v1, v9

    int-to-float v2, v12

    int-to-float v3, v10

    invoke-virtual {p0}, Lcom/android/facelock/Spotlight;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Lcom/android/facelock/Spotlight;->mSpot:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/facelock/Spotlight;->mDestination:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/facelock/Spotlight;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method
