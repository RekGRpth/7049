.class public Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;
.super Ljava/lang/Object;
.source "AbsBaseActivity.java"

# interfaces
.implements Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/AbsBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ListListener"
.end annotation


# static fields
.field public static final LIST_DIALOG_TAG:Ljava/lang/String; = "ListDialogFragment"


# instance fields
.field final synthetic this$0:Lcom/mediatek/filemanager/AbsBaseActivity;


# direct methods
.method protected constructor <init>(Lcom/mediatek/filemanager/AbsBaseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected dismissDialogFragment()V
    .locals 3

    const-string v1, "FileManagerBaseActivity"

    const-string v2, "ListListener dismissDialogFragment"

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "ListDialogFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    const-string v1, "FileManagerBaseActivity"

    const-string v2, "ListListener listDialogFragment != null dismiss"

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    return-void
.end method

.method public onTaskPrepare()V
    .locals 0

    return-void
.end method

.method public onTaskProgress(Lcom/mediatek/filemanager/service/ProgressInfo;)V
    .locals 4
    .param p1    # Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "ListDialogFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/ProgressDialogFragment;

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    const/4 v1, 0x1

    const v2, 0x7f08000c

    invoke-static {v1, v3, v2, v3}, Lcom/mediatek/filemanager/ProgressDialogFragment;->newInstance(IIII)Lcom/mediatek/filemanager/ProgressDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "ListDialogFragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    invoke-virtual {v0, p1}, Lcom/mediatek/filemanager/ProgressDialogFragment;->setProgress(Lcom/mediatek/filemanager/service/ProgressInfo;)V

    :cond_1
    return-void
.end method

.method public onTaskResult(I)V
    .locals 5
    .param p1    # I

    const/4 v4, -0x1

    const-string v1, "FileManagerBaseActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "List Linstener on TaskResult result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget v3, v3, Lcom/mediatek/filemanager/AbsBaseActivity;->mSortType:I

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/filemanager/FileInfoManager;->loadFileInfoList(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-static {v1}, Lcom/mediatek/filemanager/AbsBaseActivity;->access$100(Lcom/mediatek/filemanager/AbsBaseActivity;)I

    move-result v0

    if-ne v0, v4, :cond_1

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->dismissDialogFragment()V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/AbsBaseActivity;->onPathChanged()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Loading]10000files,end time="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/filemanager/utils/LogUtils;->performance(Ljava/lang/String;)V

    return-void

    :cond_1
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mTop:I

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mTop:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iput v4, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mTop:I

    goto :goto_0
.end method
