.class public Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;
.super Ljava/lang/Object;
.source "AbsBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/AbsBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TabManager"
.end annotation


# instance fields
.field private final mBlankTab:Landroid/widget/Button;

.field private mCurFilePath:Ljava/lang/String;

.field private final mTabNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mTabsHolder:Landroid/widget/LinearLayout;

.field final synthetic this$0:Lcom/mediatek/filemanager/AbsBaseActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/AbsBaseActivity;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    iput-object p1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    iput-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    const v1, 0x7f0c0002

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v5, v2, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->showPrevNavigationView(Ljava/lang/String;)V

    return-void
.end method

.method private showPrevNavigationView(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->refreshTab(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity;->showDirectoryContent(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected addTab(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/16 v7, 0xb

    const/4 v6, -0x1

    const/4 v5, -0x2

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v2, v5, v6}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mNavigationBar:Landroid/widget/HorizontalScrollView;

    new-instance v3, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager$1;

    invoke-direct {v3, p0}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager$1;-><init>(Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_0
    new-instance v0, Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object v2, v0

    check-cast v2, Landroid/widget/Button;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v7, :cond_1

    move-object v2, v0

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v2, v5, v6}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    move-object v2, v0

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {p1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public refreshTab(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v1}, Landroid/view/ViewGroup;->removeViews(II)V

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    if-eqz v7, :cond_0

    const-string v7, "Home"

    invoke-virtual {p0, v7}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->addTab(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v7, v7, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v7, v7, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/mediatek/filemanager/MountPointManager;->getDescriptionPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "/"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v6, v0, v2

    invoke-virtual {p0, v6}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->addTab(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->updateHomeButton()V

    return-void
.end method

.method protected updateHomeButton()V
    .locals 7

    const v6, 0x7f050004

    const v5, 0x7f02000d

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    const-string v2, "FileManagerBaseActivity"

    const-string v3, "HomeBtm == null"

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f02003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v4, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f02003c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected updateNavigationBar(I)V
    .locals 9
    .param p1    # I

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge p1, v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    sub-int v0, v7, p1

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    add-int/lit8 v8, p1, 0x1

    invoke-virtual {v7, v8, v0}, Landroid/view/ViewGroup;->removeViews(II)V

    const/4 v1, 0x1

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabNameList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mTabsHolder:Landroid/widget/LinearLayout;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mBlankTab:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    if-nez p1, :cond_3

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v7, v7, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    invoke-virtual {v7}, Lcom/mediatek/filemanager/MountPointManager;->getRootPath()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    :goto_1
    const/4 v5, -0x1

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v7, v7, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/AdapterView;->getCount()I

    move-result v7

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v7, v7, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v7, v7, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v8, v6}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getItem(I)Lcom/mediatek/filemanager/FileInfo;

    move-result-object v4

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v5

    :cond_1
    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v7, v8, v4, v5}, Lcom/mediatek/filemanager/AbsBaseActivity;->addToNavigationList(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;I)V

    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->this$0:Lcom/mediatek/filemanager/AbsBaseActivity;

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/mediatek/filemanager/AbsBaseActivity;->showDirectoryContent(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->updateHomeButton()V

    :cond_2
    return-void

    :cond_3
    iget-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_2
    if-gt v1, p1, :cond_4

    const-string v7, "/"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v7, v1, 0x1

    aget-object v7, v2, v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/mediatek/filemanager/AbsBaseActivity$TabManager;->mCurFilePath:Ljava/lang/String;

    goto :goto_1
.end method
