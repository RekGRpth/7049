.class public Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;
.super Lcom/mediatek/filemanager/AlertDialogFragment;
.source "AlertDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/AlertDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChoiceDialogFragment"
.end annotation


# static fields
.field public static final ARRAY_ID:Ljava/lang/String; = "arrayId"

.field public static final DEFAULT_CHOICE:Ljava/lang/String; = "defaultChoice"

.field public static final ITEM_LISTENER:Ljava/lang/String; = "itemlistener"

.field public static final TAG:Ljava/lang/String; = "ChoiceDialogFragment"


# instance fields
.field private mArrayId:I

.field private mDefaultChoice:I

.field private mItemLinster:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/filemanager/AlertDialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mItemLinster:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mItemLinster:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mItemLinster:Landroid/content/DialogInterface$OnClickListener;

    invoke-interface {v0, p1, p2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v2, "ChoiceDialogFragment"

    const-string v3, "Show alertSortDialog"

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/AlertDialogFragment;->createAlertDialogBuilder(Landroid/os/Bundle;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const-string v2, "defaultChoice"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mDefaultChoice:I

    const-string v2, "arrayId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mArrayId:I

    :cond_0
    iget v2, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mArrayId:I

    iget v3, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mDefaultChoice:I

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public setItemClickListener(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->mItemLinster:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method
