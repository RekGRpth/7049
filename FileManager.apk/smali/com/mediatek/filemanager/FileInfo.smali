.class public Lcom/mediatek/filemanager/FileInfo;
.super Ljava/lang/Object;
.source "FileInfo.java"


# static fields
.field public static final FILENAME_MAX_LENGTH:I = 0xff

.field public static final MIMETYPE_3GPP2_VIDEO:Ljava/lang/String; = "video/3gpp2"

.field public static final MIMETYPE_3GPP_UNKONW:Ljava/lang/String; = "unknown_3gpp_mimeType"

.field public static final MIMETYPE_3GPP_VIDEO:Ljava/lang/String; = "video/3gpp"

.field public static final MIMETYPE_EXTENSION_NULL:Ljava/lang/String; = "unknown_ext_null_mimeType"

.field public static final MIMETYPE_EXTENSION_UNKONW:Ljava/lang/String; = "unknown_ext_mimeType"

.field public static final MIMETYPE_UNRECOGNIZED:Ljava/lang/String; = "application/zip"

.field public static final MIME_HAED_IMAGE:Ljava/lang/String; = "image/"

.field public static final MIME_HEAD_VIDEO:Ljava/lang/String; = "video/"

.field private static final TAG:Ljava/lang/String; = "FileInfo"


# instance fields
.field private final mAbsolutePath:Ljava/lang/String;

.field private final mFile:Ljava/io/File;

.field private mFileSizeStr:Ljava/lang/String;

.field private mIsChecked:Z

.field private final mIsDir:Z

.field private mLastModifiedTime:J

.field private mMimeType:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mParentPath:Ljava/lang/String;

.field private mSize:J


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    const-wide/16 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mParentPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFileSizeStr:Ljava/lang/String;

    iput-wide v1, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    iput-wide v1, p0, Lcom/mediatek/filemanager/FileInfo;->mSize:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsChecked:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mAbsolutePath:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsDir:Z

    iget-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsDir:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mSize:J

    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-wide/16 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mParentPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFileSizeStr:Ljava/lang/String;

    iput-wide v1, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    iput-wide v1, p0, Lcom/mediatek/filemanager/FileInfo;->mSize:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsChecked:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfo;->mAbsolutePath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsDir:Z

    iget-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsDir:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mSize:J

    :cond_1
    return-void
.end method

.method private getMimeType(Ljava/io/File;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/filemanager/utils/FileUtils;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "FileInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMimeTypeForFile fileName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v2, "unknown_ext_null_mimeType"

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-static {v1}, Landroid/media/MediaFile;->getMimeTypeBySuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, "unknown_ext_mimeType"

    goto :goto_0

    :cond_2
    const-string v3, "video/3gpp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "video/3gpp2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    const-string v3, "FileInfo"

    const-string v4, "getMimeTypeForFile, a 3gpp or 3g2 file"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "unknown_3gpp_mimeType"

    goto :goto_0
.end method

.method public static isDrmFile(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/mediatek/filemanager/utils/FileUtils;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "dcf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/mediatek/filemanager/FileInfo;

    if-eqz v1, :cond_2

    check-cast p1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {p1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public getFileAbsolutePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mAbsolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFileLastModifiedTime()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    return-wide v0
.end method

.method public getFileMimeType(Lcom/mediatek/filemanager/service/FileManagerService;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/mediatek/filemanager/service/FileManagerService;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "unknown_ext_mimeType"

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/utils/DrmManager;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    :goto_0
    const-string v0, "FileInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get mimeType for file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    const-string v1, "unknown_3gpp_mimeType"

    if-ne v0, v1, :cond_1

    invoke-virtual {p1, p0}, Lcom/mediatek/filemanager/service/FileManagerService;->update3gppMimetype(Lcom/mediatek/filemanager/FileInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/mediatek/filemanager/FileInfo;->getMimeType(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mAbsolutePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/filemanager/utils/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mName:Ljava/lang/String;

    const-string v0, "FileInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFileName = null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfo;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getFileParentPath()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mParentPath:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mAbsolutePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/filemanager/utils/FileUtils;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mParentPath:Ljava/lang/String;

    const-string v0, "FileInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFileParentPath = null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfo;->mParentPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mParentPath:Ljava/lang/String;

    return-object v0
.end method

.method public getFileSize()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mSize:J

    return-wide v0
.end method

.method public getFileSizeStr()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFileSizeStr:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mSize:J

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/FileUtils;->sizeToString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFileSizeStr:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFileSizeStr:Ljava/lang/String;

    return-object v0
.end method

.method public getNewModifiedTime()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    iget-wide v0, p0, Lcom/mediatek/filemanager/FileInfo;->mLastModifiedTime:J

    return-wide v0
.end method

.method public getShowName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getShowPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/filemanager/utils/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowParentPath()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getFileParentPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/MountPointManager;->getDescriptionPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowPath()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/MountPointManager;->getDescriptionPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mFile:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsChecked:Z

    return v0
.end method

.method public isDirectory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsDir:Z

    return v0
.end method

.method public isDrmFile()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/filemanager/FileInfo;->mIsDir:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfo;->mAbsolutePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isHideFile()Z
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfo;->getFileName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/filemanager/FileInfo;->mIsChecked:Z

    return-void
.end method

.method public setFileMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfo;->mMimeType:Ljava/lang/String;

    return-void
.end method
