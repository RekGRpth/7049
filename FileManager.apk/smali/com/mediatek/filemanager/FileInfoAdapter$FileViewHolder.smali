.class Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;
.super Ljava/lang/Object;
.source "FileInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/FileInfoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FileViewHolder"
.end annotation


# instance fields
.field protected mCheckBox:Landroid/widget/CheckBox;

.field protected mIcon:Landroid/widget/ImageView;

.field protected mName:Landroid/widget/TextView;

.field protected mSize:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/CheckBox;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;
    .param p2    # Landroid/widget/TextView;
    .param p3    # Landroid/widget/ImageView;
    .param p4    # Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mName:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mSize:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mIcon:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    return-void
.end method
