.class public Lcom/mediatek/filemanager/FileInfoAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;
    }
.end annotation


# static fields
.field private static final CUT_ICON_ALPHA:F = 0.6f

.field private static final DEFAULT_ICON_ALPHA:F = 1.0f

.field private static final DEFAULT_PRIMARY_TEXT_COLOR:I = -0x1000000

.field private static final DEFAULT_SECONDARY_SIZE_TEXT_COLOR:I = -0xbebebf

.field private static final HIDE_ICON_ALPHA:F = 0.3f

.field public static final MODE_EDIT:I = 0x1

.field public static final MODE_NORMAL:I = 0x0

.field public static final MODE_SEARCH:I = 0x2

.field private static final TAG:Ljava/lang/String; = "FileInfoAdapter"


# instance fields
.field private final mFileInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mMode:I

.field private final mResources:Landroid/content/res/Resources;

.field mService:Lcom/mediatek/filemanager/service/FileManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/filemanager/service/FileManagerService;Lcom/mediatek/filemanager/FileInfoManager;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService;
    .param p3    # Lcom/mediatek/filemanager/FileInfoManager;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mMode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mResources:Landroid/content/res/Resources;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    iput-object p3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {p3}, Lcom/mediatek/filemanager/FileInfoManager;->getShowFileList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    return-void
.end method

.method private clearChecked()V
    .locals 3

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/mediatek/filemanager/FileInfo;->setChecked(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setIcon(Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;Lcom/mediatek/filemanager/FileInfo;)V
    .locals 4
    .param p1    # Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;

    invoke-static {}, Lcom/mediatek/filemanager/IconManager;->getInstance()Lcom/mediatek/filemanager/IconManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {v1, v2, p2, v3}, Lcom/mediatek/filemanager/IconManager;->getIcon(Landroid/content/res/Resources;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p1, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p1, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mIcon:Landroid/widget/ImageView;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoManager;->getPasteType()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v1, p2}, Lcom/mediatek/filemanager/FileInfoManager;->isPasteItem(Lcom/mediatek/filemanager/FileInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mIcon:Landroid/widget/ImageView;

    const v2, 0x3f19999a

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->isHideFile()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mIcon:Landroid/widget/ImageView;

    const v2, 0x3e99999a

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    return-void
.end method

.method private setSearchSizeText(Landroid/widget/TextView;Lcom/mediatek/filemanager/FileInfo;)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->getShowParentPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setSizeText(Landroid/widget/TextView;Lcom/mediatek/filemanager/FileInfo;)V
    .locals 10
    .param p1    # Landroid/widget/TextView;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;

    const/4 v9, 0x0

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v7

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/mediatek/filemanager/MountPointManager;->isMountPoint(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/FileUtils;->sizeToString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/mediatek/filemanager/utils/FileUtils;->sizeToString(J)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f08002a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f08002b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f080011

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->getFileSizeStr()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public changeMode(I)V
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mMode:I

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileInfoAdapter;->clearChecked()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getCheckedFileInfoItemsList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getCheckedItemsCount()I
    .locals 4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFirstCheckedFileInfoItem()Lcom/mediatek/filemanager/FileInfo;
    .locals 3

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/mediatek/filemanager/FileInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getItem(I)Lcom/mediatek/filemanager/FileInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mMode:I

    return v0
.end method

.method public getPosition(Lcom/mediatek/filemanager/FileInfo;)I
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfo;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/16 v7, 0x8

    move-object v1, p2

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030001

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;

    const v3, 0x7f0c0007

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0c0008

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0c0006

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const v6, 0x7f0c0009

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getShowName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mName:Landroid/widget/TextView;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mSize:Landroid/widget/TextView;

    const v4, -0xbebebf

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget v3, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mMode:I

    packed-switch v3, :pswitch_data_0

    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->setIcon(Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;Lcom/mediatek/filemanager/FileInfo;)V

    return-object v1

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;

    goto :goto_0

    :pswitch_0
    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mSize:Landroid/widget/TextView;

    invoke-direct {p0, v3, v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->setSizeText(Landroid/widget/TextView;Lcom/mediatek/filemanager/FileInfo;)V

    goto :goto_1

    :pswitch_1
    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mSize:Landroid/widget/TextView;

    invoke-direct {p0, v3, v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->setSizeText(Landroid/widget/TextView;Lcom/mediatek/filemanager/FileInfo;)V

    goto :goto_1

    :pswitch_2
    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v2, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mSize:Landroid/widget/TextView;

    invoke-direct {p0, v3, v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->setSearchSizeText(Landroid/widget/TextView;Lcom/mediatek/filemanager/FileInfo;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public isMode(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mMode:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAllItemChecked(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1, p1}, Lcom/mediatek/filemanager/FileInfo;->setChecked(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setChecked(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoAdapter;->mFileInfoList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/mediatek/filemanager/FileInfo;->setChecked(Z)V

    :cond_0
    return-void
.end method
