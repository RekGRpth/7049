.class public Lcom/mediatek/filemanager/FileManagerOperationActivity;
.super Lcom/mediatek/filemanager/AbsBaseActivity;
.source "FileManagerOperationActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/FileManagerOperationActivity$HeavyOperationListener;,
        Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;,
        Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameDoneListener;,
        Lcom/mediatek/filemanager/FileManagerOperationActivity$SortClickListner;,
        Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameExtensionListener;,
        Lcom/mediatek/filemanager/FileManagerOperationActivity$DeleteListener;
    }
.end annotation


# static fields
.field private static final BACKGROUND_COLOR:I = -0x7b7d7c

.field private static final CURRENT_POSTION_KEY:Ljava/lang/String; = "current_postion_key"

.field private static final CURRENT_TOP_KEY:Ljava/lang/String; = "current_top_key"

.field private static final CURRENT_VIEW_MODE_KEY:Ljava/lang/String; = "view_mode_key"

.field public static final DELETE_DIALOG_TAG:Ljava/lang/String; = "delete_dialog_fragment_tag"

.field public static final INTENT_EXTRA_SELECT_PATH:Ljava/lang/String; = "select_path"

.field private static final NEW_FILE_PATH_KEY:Ljava/lang/String; = "new_file_path_key"

.field private static final PREF_SHOW_HIDEN_FILE:Ljava/lang/String; = "pref_show_hiden_file"

.field private static final PREF_SORT_BY:Ljava/lang/String; = "pref_sort_by"

.field public static final RENAME_DIALOG_TAG:Ljava/lang/String; = "rename_dialog_fragment_tag"

.field public static final RENAME_EXTENSION_DIALOG_TAG:Ljava/lang/String; = "rename_extension_dialog_fragment_tag"

.field private static final SAVED_SELECTED_PATH_KEY:Ljava/lang/String; = "saved_selected_path"

.field private static final TAG:Ljava/lang/String; = "FileManagerOperationActivity"


# instance fields
.field private mEditBar:Landroid/widget/RelativeLayout;

.field private mEditPopupMenu:Landroid/widget/PopupMenu;

.field private mIsConfigChanged:Z

.field private mNavigationPopupMenu:Landroid/widget/PopupMenu;

.field private mNavigationView:Landroid/view/View;

.field private mOrientationConfig:I

.field private mSelectedAll:Z

.field private mTextSelect:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mediatek/filemanager/AbsBaseActivity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationView:Landroid/view/View;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mTextSelect:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    iput-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationPopupMenu:Landroid/widget/PopupMenu;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mSelectedAll:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mIsConfigChanged:Z

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-boolean v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mIsConfigChanged:Z

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/filemanager/FileManagerOperationActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mIsConfigChanged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showEditPopupMenu()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showNavigationPopupMenu()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/filemanager/FileManagerOperationActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->setPrefsSortBy(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->sortFileInfoList()V

    return-void
.end method

.method private changePrefsShowHidenFile()Z
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->getPrefsShowHidenFile()Z

    move-result v1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "pref_show_hiden_file"

    if-nez v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return v1
.end method

.method private createEditPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;
    .locals 2
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    return-object v0
.end method

.method private createNaviPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;
    .locals 2
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    return-object v0
.end method

.method private getPrefsShowHidenFile()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "pref_show_hiden_file"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private getPrefsSortBy()I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "pref_sort_by"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private onEditItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showEditPopupMenu()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v3}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/filemanager/FileInfoManager;->savePasteList(ILjava/util/List;)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/mediatek/filemanager/FileInfoManager;->savePasteList(ILjava/util/List;)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showDeleteDialog()V

    goto :goto_0

    :pswitch_4
    iget-boolean v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mSelectedAll:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->setAllItemChecked(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateEditBarWidgetState()V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->setAllItemChecked(Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0c001b
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private onNormalItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/mediatek/filemanager/AbsBaseActivity;->showCreateFolderDialog()V

    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-class v0, Lcom/mediatek/filemanager/FileManagerSearchActivity;

    invoke-virtual {v6, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "current_path"

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoManager;->getPasteList()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v4}, Lcom/mediatek/filemanager/FileInfoManager;->getPasteType()I

    move-result v4

    new-instance v5, Lcom/mediatek/filemanager/FileManagerOperationActivity$HeavyOperationListener;

    const v7, 0x7f080007

    invoke-direct {v5, p0, v7}, Lcom/mediatek/filemanager/FileManagerOperationActivity$HeavyOperationListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;I)V

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/filemanager/service/FileManagerService;->pasteFiles(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showNavigationPopupMenu()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0c001f
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private onPrepareEditOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 7
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const v6, 0x7f0c001d

    const v5, 0x7f0c001c

    const v2, 0x7f0c001b

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v1, 0x7f0b0000

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v1, 0x7f0c001e

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v1, 0x7f0c001f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_0
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_1
    return v4

    :cond_0
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v1, 0x7f0c001e

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v1

    if-eq v1, v0, :cond_2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020030

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f08001e

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    iput-boolean v4, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mSelectedAll:Z

    goto :goto_1

    :cond_2
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020013

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f08001d

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    iput-boolean v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mSelectedAll:Z

    goto :goto_1
.end method

.method private onPrepareNormalOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 7
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const v6, 0x7f0c0024

    const v5, 0x7f0c0023

    const v4, 0x7f0c0025

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x7f0b0002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c001f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_0
    return v3

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfoManager;->getPasteCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_1
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_2
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_2
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2

    :cond_3
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private restoreViewMode(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-ne p1, v2, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v0, p1}, Lcom/mediatek/filemanager/FileInfoAdapter;->changeMode(I)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateEditBarWidgetState()V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p2, p3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->changeMode(I)V

    goto :goto_0
.end method

.method private setPrefsSortBy(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSortType:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_sort_by"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private share()V
    .locals 13

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v10, v11}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v10}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    if-le v10, v11, :cond_4

    const-string v10, "FileManagerOperationActivity"

    const-string v11, "Share multiple files"

    invoke-static {v10, v11}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v10

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/mediatek/filemanager/utils/DrmManager;->isRightsStatus(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v3, 0x1

    :cond_0
    if-nez v3, :cond_1

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v10, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v10, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    iget-object v11, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-static {v10, v11, v2}, Lcom/mediatek/filemanager/utils/FileUtils;->getMultipleMimeType(Lcom/mediatek/filemanager/service/FileManagerService;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v10, "android.intent.extra.STREAM"

    invoke-virtual {v6, v10, v8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const v10, 0x7f080027

    :try_start_0
    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {p0, v10}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showForbiddenDialog()V

    :goto_2
    return-void

    :cond_2
    const-string v10, "FileManagerOperationActivity"

    const-string v11, "Maybe dispatch events twice, view mode error."

    invoke-static {v10, v11}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v10, "FileManagerOperationActivity"

    const-string v11, "Cannot find any activity"

    invoke-static {v10, v11, v0}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    const-string v10, "FileManagerOperationActivity"

    const-string v11, "Share a single file"

    invoke-static {v10, v11}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v10, 0x0

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    iget-object v10, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {v1, v10}, Lcom/mediatek/filemanager/FileInfo;->getFileMimeType(Lcom/mediatek/filemanager/service/FileManagerService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v10

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/mediatek/filemanager/utils/DrmManager;->isRightsStatus(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v3, 0x1

    :cond_5
    if-eqz v7, :cond_6

    const-string v10, "unknown"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    :cond_6
    const-string v7, "application/zip"

    :cond_7
    if-nez v3, :cond_1

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v10, "android.intent.action.SEND"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    const-string v10, "android.intent.extra.STREAM"

    invoke-virtual {v6, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v10, "FileManagerOperationActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Share Uri file: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "FileManagerOperationActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Share file mimetype: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const v10, 0x7f080027

    :try_start_1
    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {p0, v10}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    const-string v10, "FileManagerOperationActivity"

    const-string v11, "Cannot find any activity"

    invoke-static {v10, v11, v0}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_8
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    goto/16 :goto_2
.end method

.method private showEditPopupMenu()V
    .locals 3

    iget v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mOrientationConfig:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const v1, 0x7f0c0012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->createEditPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/mediatek/filemanager/FileManagerOperationActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity$1;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateEditPopupMenu()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mIsConfigChanged:Z

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f0c0011

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private showNavigationPopupMenu()V
    .locals 3

    iget v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mOrientationConfig:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const v1, 0x7f0c0012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->createNaviPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationPopupMenu:Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationPopupMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/mediatek/filemanager/FileManagerOperationActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity$2;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateNaviPopupMenu()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mIsConfigChanged:Z

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f0c0011

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private sortFileInfoList()V
    .locals 3

    const-string v1, "FileManagerOperationActivity"

    const-string v2, "Start sortFileInfoList()"

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iget v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSortType:I

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/FileInfoManager;->sort(I)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    const-string v1, "FileManagerOperationActivity"

    const-string v2, "End sortFileInfoList()"

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private switchToEditView()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "FileManagerOperationActivity"

    const-string v1, "Switch to edit view"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->changeMode(I)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateEditBarWidgetState()V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private switchToEditView(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "FileManagerOperationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchToEditView position and top"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->setChecked(IZ)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToEditView()V

    return-void
.end method

.method private switchToNavigationView()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "FileManagerOperationActivity"

    const-string v1, "Switch to navigation view"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v0, v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->changeMode(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method private updateEditBarWidgetState()V
    .locals 4

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mTextSelect:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateEditPopupMenu()V
    .locals 15

    const v14, 0x7f0c0028

    const v10, 0x7f0c0027

    const v13, 0x7f0c0026

    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v9, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v9}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v5

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v8

    const v9, 0x7f0c0029

    invoke-interface {v5, v9}, Landroid/view/Menu;->removeItem(I)V

    if-nez v8, :cond_1

    invoke-interface {v5, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v8, v12, :cond_7

    invoke-interface {v5, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v12}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->canWrite()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v12}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/mediatek/filemanager/utils/DrmManager;->checkDrmObjectType(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/mediatek/filemanager/utils/DrmManager;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_3

    const v9, 0x7f0c0029

    const v10, 0x2050062

    invoke-interface {v5, v11, v9, v11, v10}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    :cond_3
    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v9

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/filemanager/utils/DrmManager;->isRightsStatus(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    :cond_4
    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    invoke-interface {v5, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_6
    invoke-interface {v5, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v12}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_7
    invoke-interface {v5, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v12}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v4}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v5, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method private updateNaviPopupMenu()V
    .locals 4

    const v2, 0x7f0c0021

    const v3, 0x7f0c0020

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->getPrefsShowHidenFile()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f080024

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :goto_0
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_1
    return-void

    :cond_1
    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f080023

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method


# virtual methods
.method protected initCurrentFileInfo()Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "select_path"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;

    const v3, 0x7f08003a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/utils/ToastHelper;->showToast(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/MountPointManager;->getRootPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/mediatek/filemanager/AbsBaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v2, "FileManagerOperationActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClick: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/MountPointManager;->isRootPathMount(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateEditBarWidgetState()V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mOrientationConfig:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mIsConfigChanged:Z

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mOrientationConfig:I

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "FileManagerOperationActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->getPrefsSortBy()I

    move-result v0

    iput v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSortType:I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mOrientationConfig:I

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v9

    if-eqz v9, :cond_6

    const-string v9, "FileManagerOperationActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Selected position: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v9

    if-ge p3, v9, :cond_2

    if-gez p3, :cond_3

    :cond_2
    const-string v9, "FileManagerOperationActivity"

    const-string v10, "click events error"

    invoke-static {v9, v10}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "FileManagerOperationActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mFileInfoList.size(): "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v11}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9, p3}, Lcom/mediatek/filemanager/FileInfoAdapter;->getItem(I)Lcom/mediatek/filemanager/FileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v7

    const-string v9, "FileManagerOperationActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "fromTop = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/filemanager/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {p0, v9, v5, v7}, Lcom/mediatek/filemanager/AbsBaseActivity;->addToNavigationList(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;I)V

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/mediatek/filemanager/AbsBaseActivity;->showDirectoryContent(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x1

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {v5, v9}, Lcom/mediatek/filemanager/FileInfo;->getFileMimeType(Lcom/mediatek/filemanager/service/FileManagerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v9

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/filemanager/utils/DrmManager;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    const/4 v0, 0x0

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;

    const v10, 0x7f080034

    invoke-virtual {v9, v10}, Lcom/mediatek/filemanager/utils/ToastHelper;->showToast(I)V

    :cond_5
    if-eqz v0, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getUri()Landroid/net/Uri;

    move-result-object v8

    const-string v9, "FileManagerOperationActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Open uri file: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;

    const v10, 0x7f080034

    invoke-virtual {v9, v10}, Lcom/mediatek/filemanager/utils/ToastHelper;->showToast(I)V

    const-string v9, "FileManagerOperationActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot open file: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const v9, 0x7f0c0009

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v6

    if-nez v6, :cond_7

    const/4 v9, 0x1

    :goto_1
    invoke-virtual {v1, v9}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v10, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    if-nez v6, :cond_8

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {v10, p3, v9}, Lcom/mediatek/filemanager/FileInfoAdapter;->setChecked(IZ)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->updateEditBarWidgetState()V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v9, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v9}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_7
    const/4 v9, 0x0

    goto :goto_1

    :cond_8
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-direct {p0, p3, v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToEditView(II)V

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v2

    :pswitch_1
    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showRenameDialog()V

    :cond_0
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    new-instance v5, Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v5, p0, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;)V

    invoke-virtual {v3, v4, v1, v5}, Lcom/mediatek/filemanager/service/FileManagerService;->getDetailInfo(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    goto :goto_1

    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/mediatek/filemanager/utils/DrmManager;->showProtectionInfoDialog(Landroid/app/Activity;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->share()V

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showSortDialog()V

    goto :goto_1

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->changePrefsShowHidenFile()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/mediatek/filemanager/service/FileManagerService;->setListType(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    new-instance v4, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;

    invoke-direct {v4, p0}, Lcom/mediatek/filemanager/AbsBaseActivity$ListListener;-><init>(Lcom/mediatek/filemanager/AbsBaseActivity;)V

    invoke-virtual {v1, v2, v3, v4}, Lcom/mediatek/filemanager/service/FileManagerService;->listFiles(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    goto/16 :goto_1

    :cond_1
    const/4 v1, 0x2

    goto :goto_2

    :pswitch_7
    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToEditView()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x7f0c0020
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    const-string v2, "select_path"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;

    const v3, 0x7f08003a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/utils/ToastHelper;->showToast(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/MountPointManager;->getRootPath()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/mediatek/filemanager/AbsBaseActivity;->addToNavigationList(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;I)V

    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/AbsBaseActivity;->showDirectoryContent(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const-string v0, "FileManagerOperationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOptionsItemSelected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->onNormalItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->onEditItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const-string v1, "FileManagerOperationActivity"

    const-string v2, "onPrepareOptionsMenu"

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->isMode(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->onPrepareNormalOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->onPrepareEditOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/filemanager/FileInfo;

    if-eqz v2, :cond_0

    const-string v6, "saved_selected_path"

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfoAdapter;->getMode()I

    move-result v0

    :goto_0
    const-string v6, "view_mode_key"

    invoke-virtual {p1, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v4}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v3

    const-string v5, "current_postion_key"

    invoke-virtual {p1, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "current_top_key"

    invoke-virtual {p1, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    return-void

    :cond_2
    move v0, v5

    goto :goto_0
.end method

.method public onUnmounted(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mMountPointManager:Lcom/mediatek/filemanager/MountPointManager;

    iget-object v5, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_0
    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v4}, Lcom/mediatek/filemanager/FileInfoAdapter;->getMode()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->switchToNavigationView()V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "HeavyDialogFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/ProgressDialogFragment;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "detaildialogtag"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/AlertDialogFragment;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "delete_dialog_fragment_tag"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/AlertDialogFragment;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "rename_extension_dialog_fragment_tag"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/AlertDialogFragment;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_5
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "ChoiceDialogFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_6
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "rename_dialog_fragment_tag"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_7
    invoke-super {p0, p1}, Lcom/mediatek/filemanager/AbsBaseActivity;->onUnmounted(Ljava/lang/String;)V

    return-void
.end method

.method protected restoreDialog()V
    .locals 11

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "HeavyDialogFragment"

    invoke-virtual {v8, v9}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/mediatek/filemanager/ProgressDialogFragment;

    if-eqz v3, :cond_0

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    invoke-virtual {v3}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v9, "saved_selected_path"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x0

    if-eqz v6, :cond_1

    new-instance v5, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v5, v6}, Lcom/mediatek/filemanager/FileInfo;-><init>(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "detaildialogtag"

    invoke-virtual {v8, v9}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/AlertDialogFragment;

    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    if-eqz v8, :cond_2

    new-instance v1, Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;

    invoke-direct {v1, p0, v5}, Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;)V

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Lcom/mediatek/filemanager/service/FileManagerService;->reconnected(Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/AlertDialogFragment;->setDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "delete_dialog_fragment_tag"

    invoke-virtual {v8, v9}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/AlertDialogFragment;

    if-eqz v0, :cond_3

    new-instance v8, Lcom/mediatek/filemanager/FileManagerOperationActivity$DeleteListener;

    invoke-direct {v8, p0, v10}, Lcom/mediatek/filemanager/FileManagerOperationActivity$DeleteListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileManagerOperationActivity$1;)V

    invoke-virtual {v0, v8}, Lcom/mediatek/filemanager/AlertDialogFragment;->setOnDoneListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "rename_extension_dialog_fragment_tag"

    invoke-virtual {v8, v9}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/AlertDialogFragment;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "new_file_path_key"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    if-eqz v5, :cond_4

    new-instance v8, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameExtensionListener;

    invoke-direct {v8, p0, v5, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameExtensionListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Lcom/mediatek/filemanager/AlertDialogFragment;->setOnDoneListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "ChoiceDialogFragment"

    invoke-virtual {v8, v9}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;

    if-eqz v7, :cond_5

    new-instance v8, Lcom/mediatek/filemanager/FileManagerOperationActivity$SortClickListner;

    invoke-direct {v8, p0, v10}, Lcom/mediatek/filemanager/FileManagerOperationActivity$SortClickListner;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileManagerOperationActivity$1;)V

    invoke-virtual {v7, v8}, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->setItemClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_5
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "rename_dialog_fragment_tag"

    invoke-virtual {v8, v9}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment;

    if-eqz v4, :cond_6

    if-eqz v5, :cond_6

    new-instance v8, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameDoneListener;

    invoke-direct {v8, p0, v5}, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameDoneListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;)V

    invoke-virtual {v4, v8}, Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment;->setOnEditTextDoneListener(Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment$EditTextDoneListener;)V

    :cond_6
    invoke-super {p0}, Lcom/mediatek/filemanager/AbsBaseActivity;->restoreDialog()V

    return-void

    :cond_7
    new-instance v1, Lcom/mediatek/filemanager/FileManagerOperationActivity$HeavyOperationListener;

    const/4 v8, -0x1

    invoke-direct {v1, p0, v8}, Lcom/mediatek/filemanager/FileManagerOperationActivity$HeavyOperationListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;I)V

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Lcom/mediatek/filemanager/service/FileManagerService;->reconnected(Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    invoke-virtual {v3, v1}, Lcom/mediatek/filemanager/ProgressDialogFragment;->setCancelListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    iget-object v8, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v5, v1}, Lcom/mediatek/filemanager/service/FileManagerService;->getDetailInfo(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    goto/16 :goto_1
.end method

.method protected serviceConnected()V
    .locals 7

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/mediatek/filemanager/AbsBaseActivity;->serviceConnected()V

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v5, "view_mode_key"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v5, "current_postion_key"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v5, "current_top_key"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "FileManagerOperationActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "serviceConnected mode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->restoreViewMode(III)V

    :cond_0
    iget-object v4, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->getPrefsShowHidenFile()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v3, 0x2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/mediatek/filemanager/service/FileManagerService;->setListType(ILjava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    return-void
.end method

.method protected setMainContentView()V
    .locals 5

    const v3, 0x7f030004

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const/high16 v3, 0x7f030000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x10

    const/16 v4, 0x1a

    invoke-virtual {v0, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const/high16 v3, 0x7f0c0000

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mNavigationView:Landroid/view/View;

    const v3, 0x7f0c0003

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    const v4, -0x7b7d7c

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mEditBar:Landroid/widget/RelativeLayout;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0c0005

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mTextSelect:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020042

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setSplitBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected showDeleteDialog()V
    .locals 5

    const v0, 0x7f08000e

    iget-object v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v3}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const v0, 0x7f08000f

    :goto_0
    new-instance v1, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    invoke-direct {v1}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setMessage(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setDoneTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v3

    const v4, 0x7f080019

    invoke-virtual {v3, v4}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setCancelTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v3

    const v4, 0x7f02003b

    invoke-virtual {v3, v4}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setIcon(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v3

    const v4, 0x7f080005

    invoke-virtual {v3, v4}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->create()Lcom/mediatek/filemanager/AlertDialogFragment;

    move-result-object v2

    new-instance v3, Lcom/mediatek/filemanager/FileManagerOperationActivity$DeleteListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/mediatek/filemanager/FileManagerOperationActivity$DeleteListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileManagerOperationActivity$1;)V

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment;->setOnDoneListener(Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "delete_dialog_fragment_tag"

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    const v0, 0x7f08000e

    goto :goto_0
.end method

.method protected showForbiddenDialog()V
    .locals 4

    new-instance v0, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    invoke-direct {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;-><init>()V

    const v2, 0x20500bd

    invoke-virtual {v0, v2}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f02003b

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setIcon(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x20500be

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setMessage(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setCancelable(Z)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setCancelTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->create()Lcom/mediatek/filemanager/AlertDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "AlertDialogFragment"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected showRenameDialog()V
    .locals 8

    iget-object v6, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfoAdapter;->getFirstCheckedFileInfoItem()Lcom/mediatek/filemanager/FileInfo;

    move-result-object v2

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfo;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/filemanager/utils/FileUtils;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    sub-int v6, v5, v6

    add-int/lit8 v5, v6, -0x1

    :cond_0
    new-instance v0, Lcom/mediatek/filemanager/AlertDialogFragment$EditDialogFragmentBuilder;

    invoke-direct {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$EditDialogFragmentBuilder;-><init>()V

    invoke-virtual {v0, v3, v5}, Lcom/mediatek/filemanager/AlertDialogFragment$EditDialogFragmentBuilder;->setDefault(Ljava/lang/String;I)Lcom/mediatek/filemanager/AlertDialogFragment$EditDialogFragmentBuilder;

    move-result-object v6

    const v7, 0x7f080018

    invoke-virtual {v6, v7}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setDoneTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v6

    const v7, 0x7f080019

    invoke-virtual {v6, v7}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setCancelTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v6

    const v7, 0x7f080008

    invoke-virtual {v6, v7}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$EditDialogFragmentBuilder;->create()Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment;

    move-result-object v4

    new-instance v6, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameDoneListener;

    invoke-direct {v6, p0, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameDoneListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;)V

    invoke-virtual {v4, v6}, Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment;->setOnEditTextDoneListener(Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment$EditTextDoneListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "rename_dialog_fragment_tag"

    invoke-virtual {v4, v6, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected showRenameExtensionDialog(Lcom/mediatek/filemanager/FileInfo;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/mediatek/filemanager/FileInfo;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    invoke-direct {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;-><init>()V

    const v2, 0x7f08000a

    invoke-virtual {v0, v2}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f02003b

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setIcon(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f080016

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setMessage(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setCancelTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setDoneTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->create()Lcom/mediatek/filemanager/AlertDialogFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "new_file_path_key"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameExtensionListener;

    invoke-direct {v2, p0, p1, p2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$RenameExtensionListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/AlertDialogFragment;->setOnDoneListener(Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "rename_extension_dialog_fragment_tag"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected showSortDialog()V
    .locals 4

    new-instance v0, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragmentBuilder;

    invoke-direct {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragmentBuilder;-><init>()V

    const/high16 v2, 0x7f090000

    iget v3, p0, Lcom/mediatek/filemanager/AbsBaseActivity;->mSortType:I

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragmentBuilder;->setDefault(II)Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f080001

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v2

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;->setCancelTitle(I)Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragmentBuilder;->create()Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/mediatek/filemanager/FileManagerOperationActivity$SortClickListner;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/filemanager/FileManagerOperationActivity$SortClickListner;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileManagerOperationActivity$1;)V

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;->setItemClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "ChoiceDialogFragment"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
