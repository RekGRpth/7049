.class public final Lcom/mediatek/filemanager/IconManager;
.super Ljava/lang/Object;
.source "IconManager.java"


# static fields
.field private static final OFFX:I = 0x4

.field public static final TAG:Ljava/lang/String; = "FileIconManager"

.field private static sInstance:Lcom/mediatek/filemanager/IconManager;


# instance fields
.field protected mDefIcons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

.field protected mIconsHead:Landroid/graphics/Bitmap;

.field private mRes:Landroid/content/res/Resources;

.field protected mSdcard2Icons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/filemanager/IconManager;

    invoke-direct {v0}, Lcom/mediatek/filemanager/IconManager;-><init>()V

    sput-object v0, Lcom/mediatek/filemanager/IconManager;->sInstance:Lcom/mediatek/filemanager/IconManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/IconManager;->mDefIcons:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/mediatek/filemanager/IconManager;->mSdcard2Icons:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/mediatek/filemanager/IconManager;->mIconsHead:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    return-void
.end method

.method public static getDrawableId(Ljava/lang/String;)I
    .locals 3
    .param p0    # Ljava/lang/String;

    const v0, 0x7f020038

    const v1, 0x7f020010

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v0, 0x7f02000f

    goto :goto_0

    :cond_2
    const-string v2, "application/zip"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const v0, 0x7f02003a

    goto :goto_0

    :cond_3
    const-string v2, "application/ogg"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const-string v2, "audio/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    const-string v1, "image/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v0, 0x7f02002b

    goto :goto_0

    :cond_6
    const-string v1, "text/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const v0, 0x7f020020

    goto :goto_0

    :cond_7
    const-string v1, "video/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f020039

    goto :goto_0
.end method

.method private getFileIcon(IZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p1    # I
    .param p2    # Z

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/IconManager;->getExternalIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/IconManager;->getDefaultIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private getFolderIcon(Lcom/mediatek/filemanager/FileInfo;Z)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # Lcom/mediatek/filemanager/FileInfo;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/MountPointManager;->isInternalMountPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f020040

    invoke-virtual {p0, v2}, Lcom/mediatek/filemanager/IconManager;->getDefaultIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/MountPointManager;->isExternalMountPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f020041

    invoke-virtual {p0, v2}, Lcom/mediatek/filemanager/IconManager;->getDefaultIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    invoke-interface {v2, v1}, Lcom/mediatek/filemanager/ext/IIconExtension;->isSystemFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    invoke-interface {v2, v1}, Lcom/mediatek/filemanager/ext/IIconExtension;->getSystemFolderIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_0

    invoke-virtual {p0, v0}, Lcom/mediatek/filemanager/IconManager;->createExternalIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v2, 0x7f020022

    invoke-direct {p0, v2, p2}, Lcom/mediatek/filemanager/IconManager;->getFileIcon(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInstance()Lcom/mediatek/filemanager/IconManager;
    .locals 1

    sget-object v0, Lcom/mediatek/filemanager/IconManager;->sInstance:Lcom/mediatek/filemanager/IconManager;

    return-object v0
.end method


# virtual methods
.method public createExternalIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    const/4 v7, 0x0

    if-nez p1, :cond_0

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "parameter bitmap is null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    iget-object v5, p0, Lcom/mediatek/filemanager/IconManager;->mIconsHead:Landroid/graphics/Bitmap;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/filemanager/IconManager;->mRes:Landroid/content/res/Resources;

    const v6, 0x7f02002d

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/filemanager/IconManager;->mIconsHead:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v5, p0, Lcom/mediatek/filemanager/IconManager;->mIconsHead:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v3, v5, 0x4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int v4, v3, v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    int-to-float v5, v3

    invoke-virtual {v0, p1, v5, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v5, p0, Lcom/mediatek/filemanager/IconManager;->mIconsHead:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v5, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-object v2
.end method

.method public getDefaultIcon(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mDefIcons:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mDefIcons:Ljava/util/HashMap;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mDefIcons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mDefIcons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mRes:Landroid/content/res/Resources;

    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "decodeResource()fail, or invalid resId"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mDefIcons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getExternalIcon(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mSdcard2Icons:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mSdcard2Icons:Ljava/util/HashMap;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mSdcard2Icons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mSdcard2Icons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/IconManager;->getDefaultIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/IconManager;->createExternalIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mSdcard2Icons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getIcon(Landroid/content/res/Resources;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;
    .param p3    # Lcom/mediatek/filemanager/service/FileManagerService;

    const/4 v1, 0x0

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/mediatek/filemanager/MountPointManager;->isExternalFile(Lcom/mediatek/filemanager/FileInfo;)Z

    move-result v3

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, p2, v3}, Lcom/mediatek/filemanager/IconManager;->getFolderIcon(Lcom/mediatek/filemanager/FileInfo;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_0
    :goto_0
    const-string v5, "FileIconManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setIcon isSdcard2File="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " icon="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/filemanager/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_1
    invoke-virtual {p2, p3}, Lcom/mediatek/filemanager/FileInfo;->getFileMimeType(Lcom/mediatek/filemanager/service/FileManagerService;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/filemanager/IconManager;->getDrawableId(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v4}, Landroid/drm/DrmUtils;->getAction(Ljava/lang/String;)I

    move-result v0

    const-string v5, "FileIconManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setIcon isDrmFile & actionId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    if-eq v0, v5, :cond_2

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v5

    invoke-virtual {p2}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, p1, v6, v0, v2}, Lcom/mediatek/filemanager/utils/DrmManager;->overlayDrmIconSkew(Landroid/content/res/Resources;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/IconManager;->createExternalIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_0

    invoke-direct {p0, v2, v3}, Lcom/mediatek/filemanager/IconManager;->getFileIcon(IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public init(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mRes:Landroid/content/res/Resources;

    :try_start_0
    const-class v1, Lcom/mediatek/filemanager/ext/IIconExtension;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p1, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/ext/IIconExtension;

    iput-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    invoke-interface {v1, p2}, Lcom/mediatek/filemanager/ext/IIconExtension;->createSystemFolder(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/filemanager/ext/DefaultIconExtension;

    invoke-direct {v1}, Lcom/mediatek/filemanager/ext/DefaultIconExtension;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    goto :goto_0
.end method

.method public isSystemFolder(Lcom/mediatek/filemanager/FileInfo;)Z
    .locals 2
    .param p1    # Lcom/mediatek/filemanager/FileInfo;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/filemanager/IconManager;->mExt:Lcom/mediatek/filemanager/ext/IIconExtension;

    invoke-virtual {p1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/ext/IIconExtension;->isSystemFolder(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
