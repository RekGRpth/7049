.class public final Lcom/mediatek/filemanager/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final alert_delete_multiple:I = 0x7f08000e

.field public static final alert_delete_single:I = 0x7f08000f

.field public static final already_exists:I = 0x7f080039

.field public static final app_name:I = 0x7f080000

.field public static final cancel:I = 0x7f080019

.field public static final change_mode:I = 0x7f080045

.field public static final confirm_rename:I = 0x7f08000a

.field public static final copy:I = 0x7f080002

.field public static final copy_deny:I = 0x7f08002d

.field public static final create_folder:I = 0x7f080003

.field public static final create_hidden_file:I = 0x7f08003c

.field public static final cut:I = 0x7f080004

.field public static final cut_deny:I = 0x7f08002e

.field public static final default_search_text:I = 0x7f080029

.field public static final delete:I = 0x7f080005

.field public static final delete_deny:I = 0x7f08002f

.field public static final delete_fail:I = 0x7f080040

.field public static final deleting:I = 0x7f08000d

.field public static final deselect_all:I = 0x7f08001d

.field public static final details:I = 0x7f080006

.field public static final done:I = 0x7f080018

.field public static final empty_msg:I = 0x7f080028

.field public static final executable:I = 0x7f080014

.field public static final file_name_too_long:I = 0x7f08003e

.field public static final free_space:I = 0x7f08002a

.field public static final hide_file:I = 0x7f080024

.field public static final insufficient_memory:I = 0x7f080030

.field public static final invalid_char_prompt:I = 0x7f080033

.field public static final invalid_empty_name:I = 0x7f080031

.field public static final loading:I = 0x7f08000c

.field public static final modified_time:I = 0x7f080012

.field public static final modify_hidden_file:I = 0x7f08003d

.field public static final more:I = 0x7f080021

.field public static final msg_rename_ext:I = 0x7f080016

.field public static final msg_unable_open_file:I = 0x7f080034

.field public static final name:I = 0x7f080010

.field public static final new_folder:I = 0x7f080009

.field public static final no:I = 0x7f08001a

.field public static final ok:I = 0x7f08001b

.field public static final operation_fail:I = 0x7f080037

.field public static final paste:I = 0x7f080007

.field public static final paste_fail:I = 0x7f08003f

.field public static final paste_same_folder:I = 0x7f080032

.field public static final paste_sub_folder:I = 0x7f080036

.field public static final pasting:I = 0x7f08000b

.field public static final path_not_exists:I = 0x7f08003a

.field public static final readable:I = 0x7f080013

.field public static final rename:I = 0x7f080008

.field public static final save:I = 0x7f080026

.field public static final search:I = 0x7f080022

.field public static final search_empty:I = 0x7f080043

.field public static final search_files:I = 0x7f08002c

.field public static final search_result:I = 0x7f080038

.field public static final search_setting_description:I = 0x7f080044

.field public static final search_text_empty:I = 0x7f080035

.field public static final select_all:I = 0x7f08001e

.field public static final selected:I = 0x7f080025

.field public static final send_file:I = 0x7f080027

.field public static final share:I = 0x7f08001f

.field public static final show_file:I = 0x7f080023

.field public static final size:I = 0x7f080011

.field public static final some_delete_fail:I = 0x7f080042

.field public static final some_paste_fail:I = 0x7f080041

.field public static final sort:I = 0x7f080020

.field public static final sort_by:I = 0x7f080001

.field public static final total_space:I = 0x7f08002b

.field public static final unmounted:I = 0x7f08003b

.field public static final wait:I = 0x7f080017

.field public static final writable:I = 0x7f080015

.field public static final yes:I = 0x7f08001c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
