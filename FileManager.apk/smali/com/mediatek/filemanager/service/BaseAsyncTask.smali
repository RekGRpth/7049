.class abstract Lcom/mediatek/filemanager/service/BaseAsyncTask;
.super Landroid/os/AsyncTask;
.source "BaseAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Lcom/mediatek/filemanager/service/ProgressInfo;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field protected mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

.field protected mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iput-object p2, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    return-void
.end method


# virtual methods
.method protected onCancelled()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCancelled()"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    const/4 v1, -0x7

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPostExecute"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/BaseAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-interface {v0}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskPrepare()V

    :cond_0
    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/mediatek/filemanager/service/ProgressInfo;)V
    .locals 3
    .param p1    # [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    aget-object v0, p1, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onProgressUpdate"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    aget-object v1, p1, v2

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskProgress(Lcom/mediatek/filemanager/service/ProgressInfo;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/BaseAsyncTask;->onProgressUpdate([Lcom/mediatek/filemanager/service/ProgressInfo;)V

    return-void
.end method

.method protected removeListener()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "removeListener"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    :cond_0
    return-void
.end method

.method public setListener(Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    return-void
.end method
