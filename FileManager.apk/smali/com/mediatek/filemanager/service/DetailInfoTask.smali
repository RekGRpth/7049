.class Lcom/mediatek/filemanager/service/DetailInfoTask;
.super Lcom/mediatek/filemanager/service/BaseAsyncTask;
.source "DetailInfoTask.java"


# instance fields
.field private final mDetailfileInfo:Lcom/mediatek/filemanager/FileInfo;

.field private mTotal:J


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Lcom/mediatek/filemanager/FileInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/filemanager/service/BaseAsyncTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mTotal:J

    iput-object p3, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mDetailfileInfo:Lcom/mediatek/filemanager/FileInfo;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 12
    .param p1    # [Ljava/lang/Void;

    const/4 v11, 0x0

    iget-object v6, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mDetailfileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mDetailfileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v7}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mDetailfileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/service/DetailInfoTask;->getContentSize(Ljava/io/File;)I

    move-result v5

    if-gez v5, :cond_0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    :goto_1
    return-object v6

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x1

    new-array v6, v6, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v7, Lcom/mediatek/filemanager/service/ProgressInfo;

    const-string v8, ""

    iget-object v9, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mDetailfileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v9}, Lcom/mediatek/filemanager/FileInfo;->getFileSize()J

    move-result-wide v9

    invoke-direct {v7, v8, v11, v9, v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJ)V

    aput-object v7, v6, v11

    invoke-virtual {p0, v6}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    :cond_2
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/DetailInfoTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getContentSize(Ljava/io/File;)I
    .locals 12
    .param p1    # Ljava/io/File;

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v6, -0x7

    :goto_1
    return v6

    :cond_0
    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/service/DetailInfoTask;->getContentSize(Ljava/io/File;)I

    move-result v5

    if-gez v5, :cond_1

    move v6, v5

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-wide v7, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mTotal:J

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v9

    add-long/2addr v7, v9

    iput-wide v7, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mTotal:J

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v8, Lcom/mediatek/filemanager/service/ProgressInfo;

    const-string v9, ""

    iget-wide v10, p0, Lcom/mediatek/filemanager/service/DetailInfoTask;->mTotal:J

    invoke-direct {v8, v9, v6, v10, v11}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJ)V

    aput-object v8, v7, v6

    invoke-virtual {p0, v7}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_1
.end method
