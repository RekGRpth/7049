.class public Lcom/mediatek/filemanager/service/FileManagerService;
.super Landroid/app/Service;
.source "FileManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/service/FileManagerService$1;,
        Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;,
        Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;,
        Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;
    }
.end annotation


# static fields
.field public static final FILE_FILTER_TYPE_ALL:I = 0x2

.field public static final FILE_FILTER_TYPE_DEFAULT:I = 0x0

.field public static final FILE_FILTER_TYPE_FOLDER:I = 0x1

.field public static final FILE_FILTER_TYPE_UNKOWN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "FileManagerService"


# instance fields
.field private final mActivityMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mBinder:Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mActivityMap:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mBinder:Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;

    return-void
.end method

.method private filterPasteList(Ljava/util/List;Ljava/lang/String;)I
    .locals 6
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mActivityMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "this activity not init in Service"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method

.method private isCutSamePath(Ljava/util/List;Ljava/lang/String;)Z
    .locals 3
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFileParentPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancel(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getTask()Lcom/mediatek/filemanager/service/BaseAsyncTask;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public createFolder(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    const-string v2, "FileManagerService"

    const-string v3, " createFolder Start "

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, -0x64

    invoke-interface {p3, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFilterType()I

    move-result v5

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/service/FileOperationTask$CreateFolderTask;

    move-object v2, p3

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/filemanager/service/FileOperationTask$CreateFolderTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/lang/String;I)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public deleteFiles(Ljava/lang/String;Ljava/util/List;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, -0x64

    invoke-interface {p3, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;

    invoke-direct {v1, v0, p3, p0, p2}, Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public disconnected(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getTask()Lcom/mediatek/filemanager/service/BaseAsyncTask;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/filemanager/service/BaseAsyncTask;->removeListener()V

    :cond_0
    return-void
.end method

.method public getDetailInfo(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;
    .param p3    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, -0x64

    invoke-interface {p3, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mediatek/filemanager/service/DetailInfoTask;

    invoke-direct {v1, v0, p3, p2}, Lcom/mediatek/filemanager/service/DetailInfoTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Lcom/mediatek/filemanager/FileInfo;)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public initFileInfoManager(Ljava/lang/String;)Lcom/mediatek/filemanager/FileInfoManager;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mActivityMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;-><init>(Lcom/mediatek/filemanager/service/FileManagerService$1;)V

    new-instance v1, Lcom/mediatek/filemanager/FileInfoManager;

    invoke-direct {v1}, Lcom/mediatek/filemanager/FileInfoManager;-><init>()V

    invoke-virtual {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setFileInfoManager(Lcom/mediatek/filemanager/FileInfoManager;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mActivityMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v1

    return-object v1
.end method

.method public isBusy(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mActivityMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getTask()Lcom/mediatek/filemanager/service/BaseAsyncTask;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-eq v3, v4, :cond_2

    invoke-virtual {v1}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v3, v4, :cond_0

    :cond_2
    const-string v2, "FileManagerService"

    const-string v3, "Task has been started."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public listFiles(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, -0x64

    invoke-interface {p3, v3}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFilterType()I

    move-result v1

    const-string v3, "FileManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "listFiles fiterType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    new-instance v2, Lcom/mediatek/filemanager/service/ListFileTask;

    invoke-direct {v2, v0, p3, p2, v1}, Lcom/mediatek/filemanager/service/ListFileTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Ljava/lang/String;I)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mBinder:Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;

    invoke-direct {v0, p0}, Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;-><init>(Lcom/mediatek/filemanager/service/FileManagerService;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileManagerService;->mBinder:Lcom/mediatek/filemanager/service/FileManagerService$ServiceBinder;

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/mediatek/filemanager/utils/DrmManager;->init(Landroid/content/Context;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-static {v0}, Landroid/os/AsyncTask;->setDefaultExecutor(Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public pasteFiles(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    const/16 v4, -0x9

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, -0x64

    invoke-interface {p5, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/mediatek/filemanager/service/FileManagerService;->filterPasteList(Ljava/util/List;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, -0x8

    invoke-interface {p5, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v2, "FileManagerService"

    const-string v3, "mFileInfoManagerMap.get FileInfoManager = null"

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p5, v4}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    packed-switch p4, :pswitch_data_0

    invoke-interface {p5, v4}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/mediatek/filemanager/service/FileManagerService;->isCutSamePath(Ljava/util/List;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, -0xc

    invoke-interface {p5, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/mediatek/filemanager/service/FileOperationTask$CutPasteFilesTask;

    move-object v2, p5

    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/filemanager/service/FileOperationTask$CutPasteFilesTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    new-array v2, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;

    move-object v2, p5

    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    new-array v2, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public reconnected(Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getTask()Lcom/mediatek/filemanager/service/BaseAsyncTask;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/mediatek/filemanager/service/BaseAsyncTask;->setListener(Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    :cond_0
    return-void
.end method

.method public rename(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;
    .param p3    # Lcom/mediatek/filemanager/FileInfo;
    .param p4    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    const-string v2, "FileManagerService"

    const-string v3, " rename Start "

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, -0x64

    invoke-interface {p4, v2}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFilterType()I

    move-result v6

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;

    move-object v2, p4

    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/FileInfo;I)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->isBusy(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->cancel(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->getFileInfoManager()Lcom/mediatek/filemanager/FileInfoManager;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/service/SearchTask;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/filemanager/service/SearchTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;)V

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setTask(Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public setListType(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/mediatek/filemanager/service/FileManagerService;->getActivityInfo(Ljava/lang/String;)Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/filemanager/service/FileManagerService$FileManagerActivityInfo;->setFilterType(I)V

    return-void
.end method

.method public update3gppMimetype(Lcom/mediatek/filemanager/FileInfo;)Ljava/lang/String;
    .locals 12
    .param p1    # Lcom/mediatek/filemanager/FileInfo;

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "FileManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "get3gppOriginalMimetype resolver ="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    const-string v3, "video/3gpp"

    invoke-virtual {p1, v3}, Lcom/mediatek/filemanager/FileInfo;->setFileMimeType(Ljava/lang/String;)V

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v11, [Ljava/lang/String;

    const-string v3, "mime_type"

    aput-object v3, v2, v10

    const-string v8, "_data=?"

    new-array v4, v11, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v10

    const/4 v6, 0x0

    :try_start_0
    const-string v3, "_data=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const-string v3, "FileManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "get3gppOriginalMimetype cursor="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " file:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "mime_type"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v3, "FileManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "get3gppOriginalMimetype mimeType: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v7, :cond_1

    invoke-virtual {p1, v7}, Lcom/mediatek/filemanager/FileInfo;->setFileMimeType(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    const-string v7, "video/3gpp"

    goto :goto_0

    :catchall_0
    move-exception v3

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method
