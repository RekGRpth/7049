.class Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;
.super Lcom/mediatek/filemanager/service/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/service/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CopyPasteFilesTask"
.end annotation


# static fields
.field public static final BUFFER_SIZE:I = 0x40000


# instance fields
.field mDstFolder:Ljava/lang/String;

.field mSrcList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Landroid/content/Context;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/filemanager/FileInfoManager;",
            "Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    iput-object p5, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 19
    .param p1    # [Ljava/lang/Void;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;

    invoke-direct {v11}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v7, v11}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllFileList(Ljava/util/List;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v10

    if-gez v10, :cond_0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    :goto_0
    return-object v12

    :cond_0
    new-instance v3, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$CopyMediaStoreHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/filemanager/service/FileOperationTask;->mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;

    invoke-direct {v3, v12}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$CopyMediaStoreHelper;-><init>(Lcom/mediatek/filemanager/service/MediaStoreHelper;)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v6}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v2, v12, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v12}, Lcom/mediatek/filemanager/service/FileOperationTask;->isEnoughSpace(Ljava/util/List;Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    const/4 v12, -0x5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto :goto_0

    :cond_2
    const/4 v12, 0x1

    new-array v12, v12, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v13, 0x0

    new-instance v14, Lcom/mediatek/filemanager/service/ProgressInfo;

    const-string v15, ""

    const/16 v16, 0x0

    const-wide/16 v17, 0x64

    invoke-direct/range {v14 .. v18}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJ)V

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    const/high16 v12, 0x40000

    new-array v1, v12, [B

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_3

    const/4 v12, 0x0

    invoke-interface {v7, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v5, v12}, Lcom/mediatek/filemanager/service/FileOperationTask;->getDstFile(Ljava/util/HashMap;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-virtual {v3}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$CopyMediaStoreHelper;->updateRecords()V

    const/4 v12, -0x7

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/16 :goto_0

    :cond_5
    if-nez v4, :cond_6

    const/4 v12, 0x1

    new-array v12, v12, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v13, 0x0

    new-instance v14, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v15, -0xe

    const/16 v16, 0x1

    invoke-direct/range {v14 .. v16}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v12

    if-eqz v12, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v5, v4}, Lcom/mediatek/filemanager/service/FileOperationTask;->mkdir(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper;->addRecord(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5, v4}, Lcom/mediatek/filemanager/service/FileOperationTask;->addItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v5}, Lcom/mediatek/filemanager/service/FileOperationTask;->updateProgressWithTime(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/io/File;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_8

    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    move-result v12

    if-nez v12, :cond_9

    :cond_8
    const/4 v12, 0x1

    new-array v12, v12, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v13, 0x0

    new-instance v14, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v15, -0xa

    const/16 v16, 0x1

    invoke-direct/range {v14 .. v16}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v5, v4, v11}, Lcom/mediatek/filemanager/service/FileOperationTask;->copyFile([BLjava/io/File;Ljava/io/File;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v10

    const/4 v12, -0x7

    if-ne v10, v12, :cond_a

    invoke-virtual {v3}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$CopyMediaStoreHelper;->updateRecords()V

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/16 :goto_0

    :cond_a
    if-gez v10, :cond_b

    const/4 v12, 0x1

    new-array v12, v12, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v13, 0x0

    new-instance v14, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v15, 0x1

    invoke-direct {v14, v10, v15}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper;->addRecord(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5, v4}, Lcom/mediatek/filemanager/service/FileOperationTask;->addItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V

    goto/16 :goto_2

    :cond_c
    invoke-virtual {v3}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$CopyMediaStoreHelper;->updateRecords()V

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
