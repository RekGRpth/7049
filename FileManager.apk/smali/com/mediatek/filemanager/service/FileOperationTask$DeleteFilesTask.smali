.class Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;
.super Lcom/mediatek/filemanager/service/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/service/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DeleteFilesTask"
.end annotation


# instance fields
.field private final mDeletedFilesInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/filemanager/FileInfoManager;",
            "Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;)V

    iput-object p4, p0, Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;->mDeletedFilesInfo:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 15
    .param p1    # [Ljava/lang/Void;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;

    invoke-direct {v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;-><init>()V

    iget-object v8, p0, Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;->mDeletedFilesInfo:Ljava/util/List;

    invoke-virtual {p0, v8, v2}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllDeleteFiles(Ljava/util/List;Ljava/util/List;)I

    move-result v6

    if-gez v6, :cond_0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_0
    return-object v8

    :cond_0
    new-instance v1, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$DeleteMediaStoreHelper;

    iget-object v8, p0, Lcom/mediatek/filemanager/service/FileOperationTask;->mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;

    invoke-direct {v1, v8}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$DeleteMediaStoreHelper;-><init>(Lcom/mediatek/filemanager/service/MediaStoreHelper;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v8, p0, Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;->mDeletedFilesInfo:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v4}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v0, v8, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateTotal(J)V

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v9, 0x0

    new-instance v10, Lcom/mediatek/filemanager/service/ProgressInfo;

    const-string v11, ""

    invoke-virtual {v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getProgress()J

    move-result-wide v12

    long-to-int v12, v12

    invoke-virtual {v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotal()J

    move-result-wide v13

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJ)V

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v1}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$DeleteMediaStoreHelper;->updateRecords()V

    const/4 v8, -0x7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v3}, Lcom/mediatek/filemanager/service/FileOperationTask;->deleteFile(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper;->addRecord(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v3, v3}, Lcom/mediatek/filemanager/service/FileOperationTask;->removeItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V

    :cond_4
    const-wide/16 v8, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    invoke-virtual {v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->needUpdate()Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v9, 0x0

    new-instance v10, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getProgress()J

    move-result-wide v12

    long-to-int v12, v12

    invoke-virtual {v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotal()J

    move-result-wide v13

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJ)V

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$DeleteMediaStoreHelper;->updateRecords()V

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
