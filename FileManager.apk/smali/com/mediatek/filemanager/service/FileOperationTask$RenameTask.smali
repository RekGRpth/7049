.class Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;
.super Lcom/mediatek/filemanager/service/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/service/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RenameTask"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "RenameTask"


# instance fields
.field private final mDstFileInfo:Lcom/mediatek/filemanager/FileInfo;

.field mFilterType:I

.field private final mSrcFileInfo:Lcom/mediatek/filemanager/FileInfo;


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/FileInfo;I)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/filemanager/FileInfo;
    .param p5    # Lcom/mediatek/filemanager/FileInfo;
    .param p6    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mFilterType:I

    iput-object p5, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mDstFileInfo:Lcom/mediatek/filemanager/FileInfo;

    iput-object p4, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mSrcFileInfo:Lcom/mediatek/filemanager/FileInfo;

    iput p6, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mFilterType:I

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 9
    .param p1    # [Ljava/lang/Void;

    const/4 v8, 0x0

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mDstFileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v5, "RenameTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rename dstFile = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/mediatek/filemanager/utils/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/filemanager/utils/FileUtils;->checkFileName(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mSrcFileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v5}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, -0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_1
    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_6

    new-instance v2, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v2, v1}, Lcom/mediatek/filemanager/FileInfo;-><init>(Ljava/io/File;)V

    iget-object v5, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iget-object v6, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mSrcFileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v5, v6}, Lcom/mediatek/filemanager/FileInfoManager;->removeItem(Lcom/mediatek/filemanager/FileInfo;)V

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfo;->isHideFile()Z

    move-result v5

    if-eqz v5, :cond_4

    iget v5, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mFilterType:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    :cond_4
    iget-object v5, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v5, v2}, Lcom/mediatek/filemanager/FileInfoManager;->addItem(Lcom/mediatek/filemanager/FileInfo;)V

    :cond_5
    iget-object v5, p0, Lcom/mediatek/filemanager/service/FileOperationTask;->mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->mSrcFileInfo:Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v7}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/mediatek/filemanager/service/MediaStoreHelper;->updateInMediaStore(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    :cond_6
    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
