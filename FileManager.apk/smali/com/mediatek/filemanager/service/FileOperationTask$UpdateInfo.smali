.class Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
.super Ljava/lang/Object;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/service/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UpdateInfo"
.end annotation


# static fields
.field protected static final NEED_UPDATE_TIME:I = 0xc8


# instance fields
.field private mProgressSize:J

.field private mStartOperationTime:J

.field private mTotalSize:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mStartOperationTime:J

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mProgressSize:J

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mTotalSize:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mStartOperationTime:J

    return-void
.end method


# virtual methods
.method public getProgress()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mProgressSize:J

    return-wide v0
.end method

.method public getTotal()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mTotalSize:J

    return-wide v0
.end method

.method public needUpdate()Z
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mStartOperationTime:J

    sub-long v0, v2, v4

    const-wide/16 v2, 0xc8

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mStartOperationTime:J

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public updateProgress(J)V
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mProgressSize:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mProgressSize:J

    return-void
.end method

.method public updateTotal(J)V
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mTotalSize:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->mTotalSize:J

    return-void
.end method
