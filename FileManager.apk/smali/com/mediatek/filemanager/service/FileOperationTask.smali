.class abstract Lcom/mediatek/filemanager/service/FileOperationTask;
.super Lcom/mediatek/filemanager/service/BaseAsyncTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$CreateFolderTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$CutPasteFilesTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;,
        Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;
    }
.end annotation


# static fields
.field protected static final BUFFER_SIZE:I = 0x40000

.field protected static final TOTAL:I = 0x64


# instance fields
.field protected mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Landroid/content/Context;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/filemanager/service/BaseAsyncTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lcom/mediatek/filemanager/service/MediaStoreHelper;

    invoke-direct {v0, p3}, Lcom/mediatek/filemanager/service/MediaStoreHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask;->mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;

    return-void
.end method

.method private calcNeedSpace(Ljava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)J"
        }
    .end annotation

    const-wide/16 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    :cond_0
    return-wide v2
.end method


# virtual methods
.method protected addItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/io/File;",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v0, p3}, Lcom/mediatek/filemanager/FileInfo;-><init>(Ljava/io/File;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/FileInfoManager;->addItem(Lcom/mediatek/filemanager/FileInfo;)V

    :cond_0
    return-void
.end method

.method public checkFileNameAndRename(Ljava/io/File;)Ljava/io/File;
    .locals 3
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    move-object v0, p1

    :cond_0
    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/mediatek/filemanager/utils/FileUtils;->genrateNextNewName(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method protected copyFile([BLjava/io/File;Ljava/io/File;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I
    .locals 11
    .param p1    # [B
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .param p4    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;

    const/16 v7, -0xe

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-nez v8, :cond_2

    if-eqz v0, :cond_0

    :try_start_1
    #Replaced unresolvable odex instruction with a throw
    throw v0

    #Replaced unresolvable odex instruction with a throw
    throw p0

    move-result-object v8

    #Replaced unresolvable odex instruction with a throw
    throw v8

    move-result-object v8

    const-string v9, " in.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v4, :cond_1

    #Replaced unresolvable odex instruction with a throw
    throw v4

    #Replaced unresolvable odex instruction with a throw
    throw p0

    move-result-object v8

    #Replaced unresolvable odex instruction with a throw
    throw v8

    move-result-object v8

    const-string v9, " out.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    return v7

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v6, -0xe

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-virtual {p2}, Ljava/io/File;->exists()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v8

    if-nez v8, :cond_4

    if-eqz v0, :cond_3

    :try_start_3
    #Replaced unresolvable odex instruction with a throw
    throw v0

    #Replaced unresolvable odex instruction with a throw
    throw p0

    move-result-object v8

    #Replaced unresolvable odex instruction with a throw
    throw v8

    move-result-object v8

    const-string v9, " in.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz v4, :cond_1

    #Replaced unresolvable odex instruction with a throw
    throw v4

    #Replaced unresolvable odex instruction with a throw
    throw p0

    move-result-object v8

    #Replaced unresolvable odex instruction with a throw
    throw v8

    move-result-object v8

    const-string v9, " out.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v6, -0xe

    goto :goto_0

    :cond_4
    :try_start_4
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v3, 0x0

    :goto_1
    :try_start_6
    invoke-virtual {v1, p1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_b

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "commit copy file cancelled; break while loop thread id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "delete fail in copyFile()"

    invoke-static {v7, v8}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_5
    const/4 v7, -0x7

    if-eqz v1, :cond_6

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, " in.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, " out.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_7
    :goto_2
    move-object v4, v5

    move-object v0, v1

    goto/16 :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v6, -0xe

    goto :goto_2

    :cond_8
    const/4 v7, 0x0

    :try_start_8
    invoke-virtual {v5, p1, v7, v3}, Ljava/io/FileOutputStream;->write([BII)V

    int-to-long v7, v3

    invoke-virtual {p4, v7, v8}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    invoke-virtual {p0, p4, p2}, Lcom/mediatek/filemanager/service/FileOperationTask;->updateProgressWithTime(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/io/File;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    :catch_3
    move-exception v2

    move-object v4, v5

    move-object v0, v1

    :goto_3
    :try_start_9
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/16 v6, -0xe

    if-eqz v0, :cond_9

    :try_start_a
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, " in.close() finish"

    invoke-static {v7, v8}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    if-eqz v4, :cond_a

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, " out.close() finish"

    invoke-static {v7, v8}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :cond_a
    :goto_4
    move v7, v6

    goto/16 :goto_0

    :cond_b
    if-eqz v1, :cond_c

    :try_start_b
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, " in.close() finish"

    invoke-static {v7, v8}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    if-eqz v5, :cond_d

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, " out.close() finish"

    invoke-static {v7, v8}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    :cond_d
    move-object v4, v5

    move-object v0, v1

    goto :goto_4

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v6, -0xe

    move-object v4, v5

    move-object v0, v1

    goto :goto_4

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v6, -0xe

    goto :goto_4

    :catchall_0
    move-exception v7

    :goto_5
    if-eqz v0, :cond_e

    :try_start_c
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, " in.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    if-eqz v4, :cond_f

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, " out.close() finish"

    invoke-static {v8, v9}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    :cond_f
    :goto_6
    throw v7

    :catch_6
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v6, -0xe

    goto :goto_6

    :catchall_1
    move-exception v7

    move-object v0, v1

    goto :goto_5

    :catchall_2
    move-exception v7

    move-object v4, v5

    move-object v0, v1

    goto :goto_5

    :catch_7
    move-exception v2

    goto/16 :goto_3

    :catch_8
    move-exception v2

    move-object v0, v1

    goto/16 :goto_3
.end method

.method protected deleteFile(Ljava/io/File;)Z
    .locals 5
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-nez p1, :cond_1

    new-array v2, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v4, -0xd

    invoke-direct {v3, v4, v0}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v3, v2, v1

    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    :goto_0
    move v0, v1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    new-array v2, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v4, -0xf

    invoke-direct {v3, v4, v0}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v3, v2, v1

    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getAllDeleteFile(Ljava/io/File;Ljava/util/List;)I
    .locals 7
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)I"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v5, -0x7

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {p2, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    invoke-virtual {p0, v1, p2}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllDeleteFile(Ljava/io/File;Ljava/util/List;)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p2, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected getAllDeleteFiles(Ljava/util/List;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)I"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllDeleteFile(Ljava/io/File;Ljava/util/List;)I

    move-result v2

    if-gez v2, :cond_0

    :cond_1
    return v2
.end method

.method protected getAllFile(Ljava/io/File;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I
    .locals 8
    .param p1    # Ljava/io/File;
    .param p3    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;",
            ")I"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v5, -0x7

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {p3, v6, v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateTotal(J)V

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    invoke-virtual {p0, v1, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllFile(Ljava/io/File;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v5

    if-ltz v5, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected getAllFileList(Ljava/util/List;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I
    .locals 4
    .param p3    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;",
            ")I"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllFile(Ljava/io/File;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v2

    if-gez v2, :cond_0

    :cond_1
    return v2
.end method

.method protected getDstFile(Ljava/util/HashMap;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")",
            "Ljava/io/File;"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v0, p3

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/service/FileOperationTask;->checkFileNameAndRename(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    return-object v2
.end method

.method protected isEnoughSpace(Ljava/util/List;Ljava/lang/String;)Z
    .locals 6
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/service/FileOperationTask;->calcNeedSpace(Ljava/util/List;)J

    move-result-wide v3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v1

    cmp-long v5, v3, v1

    if-lez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x1

    goto :goto_0
.end method

.method protected mkdir(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)Z
    .locals 5
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")Z"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p3}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return v0

    :cond_0
    new-array v2, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v4, -0xe

    invoke-direct {v3, v4, v0}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v3, v2, v1

    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method

.method protected removeItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/io/File;",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v0, p3}, Lcom/mediatek/filemanager/FileInfo;-><init>(Ljava/io/File;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/FileInfoManager;->removeItem(Lcom/mediatek/filemanager/FileInfo;)V

    :cond_0
    return-void
.end method

.method protected updateProgressWithTime(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/io/File;)V
    .locals 7
    .param p1    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .param p2    # Ljava/io/File;

    const-wide/16 v5, 0x64

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->needUpdate()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getProgress()J

    move-result-wide v1

    mul-long/2addr v1, v5

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotal()J

    move-result-wide v3

    div-long/2addr v1, v3

    long-to-int v0, v1

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v2, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0, v5, v6}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJ)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
