.class public Lcom/mediatek/filemanager/service/ProgressInfo;
.super Ljava/lang/Object;
.source "ProgressInfo.java"


# instance fields
.field private mErrorCode:I

.field private mFileInfo:Lcom/mediatek/filemanager/FileInfo;

.field private final mIsFailInfo:Z

.field private final mProgress:I

.field private final mTotal:J

.field private mUpdateInfo:Ljava/lang/String;


# direct methods
.method public constructor <init>(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mUpdateInfo:Ljava/lang/String;

    iput v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mErrorCode:I

    iput-object v1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mFileInfo:Lcom/mediatek/filemanager/FileInfo;

    iput p1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mErrorCode:I

    iput v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mProgress:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mTotal:J

    iput-boolean p2, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mIsFailInfo:Z

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/filemanager/FileInfo;IJ)V
    .locals 2
    .param p1    # Lcom/mediatek/filemanager/FileInfo;
    .param p2    # I
    .param p3    # J

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mUpdateInfo:Ljava/lang/String;

    iput v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mErrorCode:I

    iput-object v1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mFileInfo:Lcom/mediatek/filemanager/FileInfo;

    iput-object p1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mFileInfo:Lcom/mediatek/filemanager/FileInfo;

    iput p2, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mProgress:I

    iput-wide p3, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mTotal:J

    iput-boolean v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mIsFailInfo:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mUpdateInfo:Ljava/lang/String;

    iput v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mErrorCode:I

    iput-object v1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mFileInfo:Lcom/mediatek/filemanager/FileInfo;

    iput-object p1, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mUpdateInfo:Ljava/lang/String;

    iput p2, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mProgress:I

    iput-wide p3, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mTotal:J

    iput-boolean v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mIsFailInfo:Z

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mErrorCode:I

    return v0
.end method

.method public getFileInfo()Lcom/mediatek/filemanager/FileInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mFileInfo:Lcom/mediatek/filemanager/FileInfo;

    return-object v0
.end method

.method public getProgeress()I
    .locals 1

    iget v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mProgress:I

    return v0
.end method

.method public getTotal()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mTotal:J

    return-wide v0
.end method

.method public getUpdateInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mUpdateInfo:Ljava/lang/String;

    return-object v0
.end method

.method public isFailInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/filemanager/service/ProgressInfo;->mIsFailInfo:Z

    return v0
.end method
