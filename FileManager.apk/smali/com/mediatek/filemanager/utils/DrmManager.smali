.class public final Lcom/mediatek/filemanager/utils/DrmManager;
.super Ljava/lang/Object;
.source "DrmManager.java"


# static fields
.field public static final ACTIONID_INVALID_DRM:I = -0x2

.field public static final ACTIONID_NOT_DRM:I = -0x1

.field public static final APP_DRM:Ljava/lang/String; = "application/vnd.oma.drm"

.field public static final EXT_DRM_CONTENT:Ljava/lang/String; = "dcf"

.field private static sInstance:Lcom/mediatek/filemanager/utils/DrmManager;


# instance fields
.field private mDrmManagerClient:Landroid/drm/DrmManagerClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/filemanager/utils/DrmManager;

    invoke-direct {v0}, Lcom/mediatek/filemanager/utils/DrmManager;-><init>()V

    sput-object v0, Lcom/mediatek/filemanager/utils/DrmManager;->sInstance:Lcom/mediatek/filemanager/utils/DrmManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    return-void
.end method

.method public static getInstance()Lcom/mediatek/filemanager/utils/DrmManager;
    .locals 1

    sget-object v0, Lcom/mediatek/filemanager/utils/DrmManager;->sInstance:Lcom/mediatek/filemanager/utils/DrmManager;

    return-object v0
.end method


# virtual methods
.method public checkDrmObjectType(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/drm/DrmManagerClient;->getDrmObjectType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    if-nez v0, :cond_0

    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    :cond_0
    return-void
.end method

.method public isRightsStatus(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public overlayDrmIconSkew(Landroid/content/res/Resources;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/drm/DrmManagerClient;->overlayDrmIconSkew(Landroid/content/res/Resources;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showProtectionInfoDialog(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/filemanager/utils/OptionsUtils;->isMtkDrmApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/utils/DrmManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p1, p2}, Landroid/drm/DrmManagerClient;->showProtectionInfoDialog(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Dialog;

    :cond_0
    return-void
.end method
