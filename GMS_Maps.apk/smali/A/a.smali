.class public LA/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LA/b;


# instance fields
.field private final a:Lo/aB;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lo/aB;

    invoke-direct {v0}, Lo/aB;-><init>()V

    iput-object v0, p0, LA/a;->a:Lo/aB;

    return-void
.end method


# virtual methods
.method public a()Lo/aB;
    .locals 3

    iget-object v1, p0, LA/a;->a:Lo/aB;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lo/aB;

    iget-object v2, p0, LA/a;->a:Lo/aB;

    invoke-direct {v0, v2}, Lo/aB;-><init>(Lo/aB;)V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lo/aB;)Z
    .locals 2

    if-nez p1, :cond_0

    new-instance p1, Lo/aB;

    invoke-direct {p1}, Lo/aB;-><init>()V

    :cond_0
    invoke-virtual {p1}, Lo/aB;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/av;

    invoke-virtual {p0, p1, v0}, LA/a;->a(Lo/aB;Lo/av;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lo/aB;Lo/av;)Z
    .locals 2

    invoke-static {p1, p2}, Lo/aB;->a(Lo/aB;Lo/av;)Lo/at;

    move-result-object v0

    iget-object v1, p0, LA/a;->a:Lo/aB;

    invoke-static {v1, p2}, Lo/aB;->a(Lo/aB;Lo/av;)Lo/at;

    move-result-object v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Lo/at;->a(Lo/at;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {v1, v0}, Lo/at;->a(Lo/at;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lo/at;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LA/a;->a:Lo/aB;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LA/a;->a:Lo/aB;

    invoke-interface {p1}, Lo/at;->a()Lo/av;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LA/a;->a:Lo/aB;

    invoke-virtual {v0, p1}, Lo/aB;->a(Lo/at;)V

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public b()J
    .locals 4

    iget-object v1, p0, LA/a;->a:Lo/aB;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LA/a;->a:Lo/aB;

    invoke-virtual {v0}, Lo/aB;->hashCode()I

    move-result v0

    int-to-long v2, v0

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
