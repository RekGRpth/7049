.class public abstract LA/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field private static final A:Ljava/util/Map;

.field private static final G:Lo/ai;

.field private static final H:Lo/an;

.field private static final I:Lo/an;

.field private static final J:Lo/an;

.field private static final K:Lo/ao;

.field private static final L:Lo/aj;

.field private static final M:Lo/aj;

.field private static final N:Lo/aj;

.field private static O:LA/r;

.field public static final a:LA/c;

.field public static final b:LA/c;

.field public static final c:LA/c;

.field public static final d:LA/c;

.field public static final e:LA/c;

.field public static final f:LA/c;

.field public static final g:LA/c;

.field public static final h:LA/c;

.field public static final i:LA/c;

.field public static final j:LA/c;

.field public static final k:LA/c;

.field public static final l:LA/c;

.field public static final m:LA/c;

.field public static final n:LA/c;

.field public static final o:LA/c;

.field public static final p:LA/c;

.field public static final q:LA/c;

.field public static final r:LA/c;

.field public static final s:LA/c;

.field public static final t:LA/c;

.field public static final u:LA/c;


# instance fields
.field private final B:I

.field private final C:Z

.field private final D:Z

.field private final E:Z

.field private final F:Lt/J;

.field public final v:I

.field public final w:I

.field public final x:Z

.field public final y:I

.field public final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/16 v9, 0xc

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LA/c;->A:Ljava/util/Map;

    new-instance v0, LA/f;

    const/16 v3, 0xa

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->f(Z)LA/t;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/t;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->a:LA/c;

    new-instance v0, LA/f;

    const/16 v3, 0x16

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->f(Z)LA/t;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/t;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->b:LA/c;

    new-instance v0, LA/f;

    const/16 v3, 0x14

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->f(Z)LA/t;

    move-result-object v0

    const-string v3, "_tran_base"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->c:LA/c;

    new-instance v0, LA/i;

    const/4 v3, 0x3

    invoke-direct {v0, v3, v8}, LA/i;-><init>(ILA/d;)V

    invoke-virtual {v0, v2}, LA/i;->c(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->d:LA/c;

    new-instance v0, LA/i;

    invoke-direct {v0, v9, v8}, LA/i;-><init>(ILA/d;)V

    const-string v3, "_ter"

    invoke-virtual {v0, v3}, LA/i;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v2}, LA/g;->c(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->e:LA/c;

    new-instance v0, LA/t;

    const/4 v3, 0x4

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->f:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0x17

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->g:LA/c;

    new-instance v0, LA/n;

    const/16 v3, 0x8

    invoke-direct {v0, v3, v8}, LA/n;-><init>(ILA/d;)V

    invoke-virtual {v0}, LA/n;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->h:LA/c;

    new-instance v0, LA/p;

    const/16 v3, 0xb

    invoke-direct {v0, v3, v8}, LA/p;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/p;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->i:LA/c;

    new-instance v0, LA/f;

    const/16 v3, 0x12

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->a(Z)LA/f;

    move-result-object v0

    const-string v3, "_vec_bic"

    invoke-virtual {v0, v3}, LA/f;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->j:LA/c;

    new-instance v0, LA/i;

    const/4 v3, 0x7

    invoke-direct {v0, v3, v8}, LA/i;-><init>(ILA/d;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, LA/i;->a(I)LA/g;

    move-result-object v0

    const-string v3, "_ter_bic"

    invoke-virtual {v0, v3}, LA/g;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->k:LA/c;

    new-instance v0, LA/i;

    const/4 v3, 0x6

    invoke-direct {v0, v3, v8}, LA/i;-><init>(ILA/d;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, LA/i;->a(I)LA/g;

    move-result-object v0

    const-string v3, "_hy_bic"

    invoke-virtual {v0, v3}, LA/g;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->l:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0xd

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_tran"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->m:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0xe

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/t;->e(Z)LA/g;

    move-result-object v0

    const-string v3, "_inaka"

    invoke-virtual {v0, v3}, LA/g;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->n:LA/c;

    new-instance v0, LA/l;

    const/16 v3, 0xf

    invoke-direct {v0, v3, v8}, LA/l;-><init>(ILA/d;)V

    const-string v3, "_labl"

    invoke-virtual {v0, v3}, LA/l;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->o:LA/c;

    new-instance v0, LA/l;

    const/16 v3, 0x15

    invoke-direct {v0, v3, v8}, LA/l;-><init>(ILA/d;)V

    const-string v3, "_tran_labl"

    invoke-virtual {v0, v3}, LA/l;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->p:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0x10

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_psm"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->q:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0x11

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_related"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->r:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0x18

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_high"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->s:LA/c;

    new-instance v0, LA/t;

    const/16 v3, 0x19

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_api"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->t:LA/c;

    new-instance v0, LA/t;

    invoke-direct {v0, v2, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_star"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v2}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->u:LA/c;

    new-instance v0, Lo/ai;

    const/4 v3, 0x0

    new-array v4, v2, [I

    invoke-direct {v0, v2, v3, v4, v2}, Lo/ai;-><init>(IF[II)V

    sput-object v0, LA/c;->G:Lo/ai;

    new-instance v0, Lo/an;

    const/high16 v3, -0x1000000

    sget-object v4, LA/c;->G:Lo/ai;

    invoke-direct {v0, v3, v4}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, LA/c;->H:Lo/an;

    new-instance v0, Lo/an;

    const v3, -0xffff01

    sget-object v4, LA/c;->G:Lo/ai;

    invoke-direct {v0, v3, v4}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, LA/c;->I:Lo/an;

    new-instance v0, Lo/an;

    const/high16 v3, -0x10000

    sget-object v4, LA/c;->G:Lo/ai;

    invoke-direct {v0, v3, v4}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, LA/c;->J:Lo/an;

    new-instance v0, Lo/ao;

    const/16 v3, 0xa

    const v4, 0x3f99999a

    const/high16 v5, 0x3f800000

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lo/ao;-><init>(IIIFFI)V

    sput-object v0, LA/c;->K:Lo/ao;

    new-instance v0, Lo/aj;

    sget-object v5, LA/c;->K:Lo/ao;

    sget-object v6, LA/c;->H:Lo/an;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LA/c;->L:Lo/aj;

    new-instance v0, Lo/aj;

    sget-object v5, LA/c;->K:Lo/ao;

    sget-object v6, LA/c;->J:Lo/an;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LA/c;->M:Lo/aj;

    new-instance v0, Lo/aj;

    sget-object v5, LA/c;->K:Lo/ao;

    sget-object v6, LA/c;->I:Lo/an;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LA/c;->N:Lo/aj;

    return-void
.end method

.method private constructor <init>(LA/g;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, LA/g;->a(LA/g;)I

    move-result v0

    iput v0, p0, LA/c;->v:I

    invoke-static {p1}, LA/g;->b(LA/g;)I

    move-result v0

    iput v0, p0, LA/c;->w:I

    invoke-static {p1}, LA/g;->c(LA/g;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LA/c;->z:Ljava/lang/String;

    invoke-static {p1}, LA/g;->d(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->x:Z

    invoke-static {p1}, LA/g;->e(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->C:Z

    invoke-static {p1}, LA/g;->f(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->D:Z

    invoke-static {p1}, LA/g;->g(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->E:Z

    iget-boolean v0, p0, LA/c;->E:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LA/c;->l()Lt/J;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LA/c;->F:Lt/J;

    iget v0, p0, LA/c;->v:I

    iget v1, p0, LA/c;->w:I

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, LA/c;->y:I

    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, LA/c;->B:I

    iget v0, p0, LA/c;->v:I

    iget v1, p0, LA/c;->w:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LA/c;->A:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tile type with key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " already defined"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v1, LA/c;->A:Ljava/util/Map;

    invoke-interface {v1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x20

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Currently maximum 32 tile types allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method synthetic constructor <init>(LA/g;LA/d;)V
    .locals 0

    invoke-direct {p0, p1}, LA/c;-><init>(LA/g;)V

    return-void
.end method

.method public static a(I)LA/c;
    .locals 2

    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    return-object v0
.end method

.method static b()I
    .locals 2

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    const/16 v1, 0x80

    mul-int/lit8 v0, v0, 0x12

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(I)I
    .locals 1

    invoke-static {p0}, LA/c;->c(I)I

    move-result v0

    return v0
.end method

.method static synthetic b(LA/c;)Z
    .locals 1

    iget-boolean v0, p0, LA/c;->C:Z

    return v0
.end method

.method static c()I
    .locals 2

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private static c(I)I
    .locals 1

    const/16 v0, 0xa0

    if-le p0, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(LA/c;)Z
    .locals 1

    iget-boolean v0, p0, LA/c;->D:Z

    return v0
.end method

.method public static e()Ljava/lang/Iterable;
    .locals 1

    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m()LA/r;
    .locals 1

    sget-object v0, LA/c;->O:LA/r;

    return-object v0
.end method

.method static synthetic n()Lo/aj;
    .locals 1

    sget-object v0, LA/c;->L:Lo/aj;

    return-object v0
.end method

.method static synthetic o()Lo/aj;
    .locals 1

    sget-object v0, LA/c;->M:Lo/aj;

    return-object v0
.end method

.method static synthetic p()Lo/aj;
    .locals 1

    sget-object v0, LA/c;->N:Lo/aj;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method public a(ILcom/google/android/maps/driveabout/vector/q;)I
    .locals 0

    return p1
.end method

.method public a(LA/c;)I
    .locals 2

    iget v0, p0, LA/c;->B:I

    iget v1, p1, LA/c;->B:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Lo/ad;)Lo/T;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;
.end method

.method public a(Ljava/lang/String;ZLt/g;)Lt/f;
    .locals 6

    iget-boolean v0, p0, LA/c;->E:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lt/y;

    if-eqz p2, :cond_1

    const/4 v2, -0x1

    :goto_1
    iget-object v3, p0, LA/c;->F:Lt/J;

    move-object v1, p1

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lt/y;-><init>(Ljava/lang/String;ILt/J;LA/c;Lt/g;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LA/c;->a()I

    move-result v2

    goto :goto_1
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0

    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LA/c;

    invoke-virtual {p0, p1}, LA/c;->a(LA/c;)I

    move-result v0

    return v0
.end method

.method public d()Lt/M;
    .locals 2

    new-instance v0, Lt/K;

    invoke-static {}, LA/c;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lt/K;-><init>(I)V

    return-object v0
.end method

.method public f()I
    .locals 2

    iget v0, p0, LA/c;->v:I

    iget v1, p0, LA/c;->w:I

    add-int/2addr v0, v1

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, LA/c;->B:I

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()LF/O;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Lo/aj;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method abstract l()Lt/J;
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "unknown"

    goto :goto_1
.end method
