.class LA/m;
.super LA/c;
.source "SourceFile"


# direct methods
.method private constructor <init>(LA/n;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LA/c;-><init>(LA/g;LA/d;)V

    return-void
.end method

.method synthetic constructor <init>(LA/n;LA/d;)V
    .locals 0

    invoke-direct {p0, p1}, LA/m;-><init>(LA/n;)V

    return-void
.end method


# virtual methods
.method a()I
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LayerTileType does not support disk caches."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;
    .locals 2

    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v0

    new-instance v1, Lr/q;

    invoke-direct {v1, p1, v0, p4, p5}, Lr/q;-><init>(Law/p;ILjava/util/Locale;Ljava/io/File;)V

    return-object v1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LA/c;

    invoke-super {p0, p1}, LA/c;->a(LA/c;)I

    move-result v0

    return v0
.end method

.method public l()Lt/J;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
