.class public LB/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static final a:Lo/aq;

.field private static b:LB/a;


# instance fields
.field private final c:Lcom/google/googlenav/common/a;

.field private final d:Ljava/util/Map;

.field private final e:I

.field private final f:I

.field private g:I

.field private h:I

.field private i:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lo/aq;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2, v2}, Lo/aq;-><init>(III)V

    sput-object v0, LB/a;->a:Lo/aq;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/a;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LB/a;->d:Ljava/util/Map;

    iput v1, p0, LB/a;->g:I

    iput v1, p0, LB/a;->h:I

    iput-object p1, p0, LB/a;->c:Lcom/google/googlenav/common/a;

    iput p2, p0, LB/a;->e:I

    iput p3, p0, LB/a;->f:I

    return-void
.end method

.method protected static a(I)I
    .locals 1

    mul-int/lit16 v0, p0, 0x400

    mul-int/lit16 v0, v0, 0x400

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method static synthetic a(LB/a;I)I
    .locals 1

    iget v0, p0, LB/a;->g:I

    sub-int/2addr v0, p1

    iput v0, p0, LB/a;->g:I

    return v0
.end method

.method public static declared-synchronized a()LB/a;
    .locals 2

    const-class v0, LB/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, LB/a;->b:LB/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic a(LB/a;)Lcom/google/googlenav/common/a;
    .locals 1

    iget-object v0, p0, LB/a;->c:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method private declared-synchronized a(II)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget v0, p0, LB/a;->g:I

    if-gt v0, p1, :cond_1

    iget v0, p0, LB/a;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v0, p2, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LB/d;

    invoke-virtual {v1}, LB/d;->c()LR/j;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v1, v2, LR/j;->a:Ljava/lang/Object;

    sget-object v5, LB/a;->a:Lo/aq;

    if-eq v1, v5, :cond_2

    new-instance v5, LB/c;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v2, LR/j;->a:Ljava/lang/Object;

    check-cast v1, Lo/aq;

    iget-object v2, v2, LR/j;->b:Ljava/lang/Object;

    check-cast v2, LB/b;

    invoke-direct {v5, v0, v1, v2}, LB/c;-><init>(Landroid/util/Pair;Lo/aq;LB/b;)V

    invoke-interface {v3, v5}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, LB/a;->g:I

    if-gt v0, p1, :cond_5

    iget v0, p0, LB/a;->h:I

    if-le v0, p2, :cond_7

    :cond_5
    invoke-interface {v3}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/c;

    iget-object v1, p0, LB/a;->d:Ljava/util/Map;

    iget-object v4, v0, LB/c;->a:Landroid/util/Pair;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LB/d;

    iget-object v4, v0, LB/c;->b:Lo/aq;

    invoke-virtual {v1, v4}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, LB/d;->f()I

    move-result v4

    if-nez v4, :cond_6

    invoke-static {v1}, LB/d;->a(LB/d;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, v0, LB/c;->a:Landroid/util/Pair;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v3, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v1}, LB/d;->c()LR/j;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v4, v1, LR/j;->a:Ljava/lang/Object;

    sget-object v5, LB/a;->a:Lo/aq;

    if-eq v4, v5, :cond_4

    new-instance v4, LB/c;

    iget-object v5, v0, LB/c;->a:Landroid/util/Pair;

    iget-object v0, v1, LR/j;->a:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    iget-object v1, v1, LR/j;->b:Ljava/lang/Object;

    check-cast v1, LB/b;

    invoke-direct {v4, v5, v0, v1}, LB/c;-><init>(Landroid/util/Pair;Lo/aq;LB/b;)V

    invoke-interface {v3, v4}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v2, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/common/a;)V
    .locals 4

    const-class v1, LB/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, LB/a;->b:LB/a;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->h()I

    move-result v0

    new-instance v2, LB/a;

    invoke-static {v0}, LB/a;->a(I)I

    move-result v3

    invoke-static {v0}, LB/a;->b(I)I

    move-result v0

    invoke-direct {v2, p0, v3, v0}, LB/a;-><init>(Lcom/google/googlenav/common/a;II)V

    sput-object v2, LB/a;->b:LB/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected static b(I)I
    .locals 1

    mul-int/lit16 v0, p0, 0x400

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x10

    return v0
.end method

.method static synthetic b(LB/a;I)I
    .locals 1

    iget v0, p0, LB/a;->h:I

    sub-int/2addr v0, p1

    iput v0, p0, LB/a;->h:I

    return v0
.end method

.method private declared-synchronized b(LD/a;LB/e;Lo/aq;Z)LB/b;
    .locals 8

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-nez v0, :cond_3

    if-eqz p4, :cond_0

    new-instance v0, LB/d;

    invoke-direct {v0, p0, v1, v2}, LB/d;-><init>(LB/a;J)V

    iget-object v1, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v0

    :goto_0
    invoke-virtual {v7, p3}, LB/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/b;

    if-eqz v0, :cond_1

    iget-object v1, p0, LB/a;->c:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iput-wide v1, v0, LB/b;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    move-object v0, v6

    goto :goto_1

    :cond_1
    if-eqz p4, :cond_2

    :try_start_1
    new-instance v0, LB/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, LB/a;->c:Lcom/google/googlenav/common/a;

    invoke-interface {v4}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LB/b;-><init>(LF/T;IIJ)V

    invoke-virtual {v7, p3, v0}, LB/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v7, v0

    goto :goto_0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 3

    mul-int/lit8 v0, p0, 0xa

    const/high16 v1, 0x100000

    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/j;->b(II)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v0, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    rem-int/lit8 v0, v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(LD/a;LB/e;Lo/aq;Z)LF/T;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, LB/a;->b(LD/a;LB/e;Lo/aq;Z)LB/b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, LB/b;->a:LF/T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(J)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, LB/a;->i:J

    :cond_0
    return-void
.end method

.method public declared-synchronized a(LD/a;)V
    .locals 7

    monitor-enter p0

    if-nez p1, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v2

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v1, v5, v2

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LB/d;

    sget-object v5, LB/a;->a:Lo/aq;

    invoke-virtual {v1, v5}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    invoke-virtual {v0, p1}, LB/d;->a(LD/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LD/a;LB/e;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LB/d;->e()V

    invoke-virtual {v0, p1}, LB/d;->a(LD/a;)V

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LD/a;LB/e;Ljava/util/List;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/aq;

    invoke-virtual {v0, v1}, LB/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LB/b;

    if-eqz v1, :cond_2

    iget-object v4, v1, LB/b;->a:LF/T;

    if-eqz v4, :cond_2

    iget v4, v1, LB/b;->b:I

    if-nez v4, :cond_2

    iget v1, v1, LB/b;->c:I

    add-int/2addr v1, v2

    :goto_2
    move v2, v1

    goto :goto_1

    :cond_1
    iget v0, p0, LB/a;->e:I

    sub-int/2addr v0, v2

    iget v1, p0, LB/a;->f:I

    invoke-direct {p0, v0, v1}, LB/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public declared-synchronized a(LD/a;LB/e;Lo/aq;LF/T;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-nez v0, :cond_2

    new-instance v0, LB/d;

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, LB/d;-><init>(LB/a;J)V

    iget-object v2, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_0
    invoke-virtual {v1, p3}, LB/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v2, v0, LB/b;->a:LF/T;

    if-eqz v2, :cond_1

    new-instance v2, LB/b;

    invoke-direct {v2, v0}, LB/b;-><init>(LB/b;)V

    invoke-virtual {v1, v2}, LB/d;->a(LB/b;)V

    :cond_1
    iput-object p4, v0, LB/b;->a:LF/T;

    invoke-interface {p4}, LF/T;->i()I

    move-result v1

    iput v1, v0, LB/b;->b:I

    invoke-interface {p4}, LF/T;->j()I

    move-result v1

    iput v1, v0, LB/b;->c:I

    iget v1, p0, LB/a;->g:I

    iget v2, v0, LB/b;->b:I

    add-int/2addr v1, v2

    iput v1, p0, LB/a;->g:I

    iget v1, p0, LB/a;->h:I

    iget v0, v0, LB/b;->c:I

    add-int/2addr v0, v1

    iput v0, p0, LB/a;->h:I

    iget v0, p0, LB/a;->e:I

    iget v1, p0, LB/a;->f:I

    invoke-direct {p0, v0, v1}, LB/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLTileCacheManager.onLowJavaAndNativeMemory("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v0, "critical"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LB/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_2

    iget v0, p0, LB/a;->g:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LB/a;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string v0, "warning"

    goto :goto_0

    :cond_2
    iget v0, p0, LB/a;->g:I

    iget v1, p0, LB/a;->h:I

    div-int/lit8 v1, v1, 0x2

    invoke-direct {p0, v0, v1}, LB/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, LB/a;->h:I

    invoke-direct {p0, v0, v1}, LB/a;->a(II)V

    return-void
.end method

.method public declared-synchronized b(LD/a;LB/e;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LB/d;->a()V

    invoke-virtual {v0}, LB/d;->f()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(LD/a;LB/e;Ljava/util/List;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/aq;

    invoke-virtual {v0, v1}, LB/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LB/b;

    if-eqz v1, :cond_1

    iget-object v3, v1, LB/b;->a:LF/T;

    if-eqz v3, :cond_1

    iget v3, v1, LB/b;->b:I

    if-nez v3, :cond_1

    iget-object v3, v1, LB/b;->a:LF/T;

    invoke-interface {v3}, LF/T;->i()I

    move-result v3

    iput v3, v1, LB/b;->b:I

    iget v3, p0, LB/a;->g:I

    iget v4, v1, LB/b;->b:I

    add-int/2addr v3, v4

    iput v3, p0, LB/a;->g:I

    iget v3, v1, LB/b;->c:I

    iget-object v4, v1, LB/b;->a:LF/T;

    invoke-interface {v4}, LF/T;->j()I

    move-result v4

    iput v4, v1, LB/b;->c:I

    iget v1, p0, LB/a;->h:I

    sub-int/2addr v1, v3

    add-int/2addr v1, v4

    iput v1, p0, LB/a;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget v0, p0, LB/a;->e:I

    iget v1, p0, LB/a;->f:I

    invoke-direct {p0, v0, v1}, LB/a;->a(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized c()Ljava/lang/String;
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LB/d;

    invoke-virtual {v1}, LB/d;->f()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    const-string v4, " + "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "no"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v0, " tiles use "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, LB/a;->g:I

    invoke-static {v0}, LB/a;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LB/a;->e:I

    invoke-static {v1}, LB/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M GL, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LB/a;->h:I

    invoke-static {v1}, LB/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LB/a;->f:I

    invoke-static {v1}, LB/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M J+N"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized c(LD/a;LB/e;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-eqz v0, :cond_0

    sget-object v2, LB/a;->a:Lo/aq;

    invoke-virtual {v0, v2}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, LB/d;->b()V

    invoke-virtual {v0, p1}, LB/d;->a(LD/a;)V

    invoke-virtual {v0}, LB/d;->f()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(LD/a;LB/e;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LB/d;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e(LD/a;LB/e;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/d;

    if-nez v0, :cond_0

    new-instance v0, LB/d;

    invoke-direct {v0, p0, v1, v2}, LB/d;-><init>(LB/a;J)V

    iget-object v1, p0, LB/a;->d:Ljava/util/Map;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, LB/d;->d()V

    invoke-virtual {v0, p1}, LB/d;->a(LD/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
