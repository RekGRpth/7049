.class public LD/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final I:Ljava/util/concurrent/atomic/AtomicLong;

.field private static final K:Ljava/util/Map;


# instance fields
.field private A:Z

.field private final B:I

.field private C:Z

.field private final D:I

.field private final E:Lz/k;

.field private final F:Lx/r;

.field private final G:Lx/a;

.field private final H:LE/n;

.field private final J:J

.field final a:Ljavax/microedition/khronos/opengles/GL10;

.field final b:Lcom/google/android/maps/driveabout/vector/u;

.field final c:Z

.field public final d:LE/i;

.field public final e:LE/g;

.field public final f:LE/o;

.field public final g:LE/o;

.field public final h:LE/o;

.field public final i:LE/o;

.field public final j:LE/o;

.field public final k:[F

.field public final l:[F

.field public final m:Lo/T;

.field public final n:Lo/T;

.field private final o:Lx/k;

.field private final p:[I

.field private q:I

.field private r:I

.field private s:[I

.field private t:I

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/Integer;

.field private w:Z

.field private x:J

.field private y:J

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, LD/a;->I:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LD/a;->K:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljavax/microedition/khronos/opengles/GL10;Lx/k;Lcom/google/android/maps/driveabout/vector/u;Lz/k;)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->z:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->A:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->C:Z

    new-instance v2, LE/m;

    const/16 v3, 0x8

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v2, v3}, LE/m;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->d:LE/i;

    new-instance v2, LE/h;

    const/16 v3, 0x14

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    const/16 v4, 0x9

    invoke-direct {v2, v3, v4}, LE/h;-><init>([FI)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->e:LE/g;

    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_2

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->f:LE/o;

    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_3

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->g:LE/o;

    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_4

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->h:LE/o;

    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_5

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->i:LE/o;

    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_6

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->j:LE/o;

    const/16 v2, 0x8

    new-array v2, v2, [F

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->k:[F

    const/4 v2, 0x4

    new-array v2, v2, [F

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->l:[F

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->m:Lo/T;

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->n:Lo/T;

    new-instance v2, Lx/a;

    invoke-direct {v2}, Lx/a;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->G:Lx/a;

    new-instance v2, LE/n;

    invoke-direct {v2}, LE/n;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->H:LE/n;

    sget-object v2, LD/a;->I:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LD/a;->J:J

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->o:Lx/k;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->E:Lz/k;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->F:Lx/r;

    invoke-direct/range {p0 .. p0}, LD/a;->M()V

    const/16 v2, 0x100

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->p:[I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->q:I

    const/16 v2, 0x20

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->s:[I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->t:I

    move-object/from16 v0, p1

    instance-of v2, v0, Ljavax/microedition/khronos/opengles/GL11;

    if-eqz v2, :cond_0

    const/16 v2, 0x1f02

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1.1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object/from16 v0, p1

    instance-of v2, v0, Lx/e;

    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->c:Z

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/16 v2, 0xd57

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v2, 0x0

    aget v2, v3, v2

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->B:I

    const/16 v2, 0xd52

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v2, 0x0

    aget v2, v3, v2

    const/16 v4, 0xd53

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v4, 0x0

    aget v4, v3, v4

    const/16 v5, 0xd54

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v5, v3, v6}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v5, 0x0

    aget v5, v3, v5

    const/16 v6, 0xd55

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v6, v3, v7}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v6, 0x0

    aget v6, v3, v6

    const/16 v7, 0xd56

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v7, v3, v8}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v7, 0x0

    aget v7, v3, v7

    const/16 v8, 0x1f00

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x1f02

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x1f01

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x3a

    const-string v12, "gl"

    const/16 v13, 0xa

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "r="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "g="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "b="

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "a="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "d="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, LD/a;->B:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/16 v2, 0x8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/16 v4, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "e="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/android/E;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "t"

    :goto_1
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v4

    invoke-static {v13}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v12, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0xd33

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v2, 0x0

    aget v2, v3, v2

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->D:I

    sget-object v3, LD/a;->K:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    sget-object v2, LD/a;->K:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-wide v4, v0, LD/a;->J:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    const-string v2, "f"

    goto :goto_1

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x10000
        0x10000
        0x0
        0x10000
        0x10000
    .end array-data

    :array_1
    .array-data 4
        -0x40800000
        0x3f800000
        0x0
        0x0
        0x0
        -0x40800000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x3f800000
        -0x40800000
        0x0
        0x3f800000
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x10000
        0x0
        0x0
        0x0
        0x0
        0x10000
        0x10000
        0x0
        0x10000
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
        0x10000
        0x0
        0x0
        0x0
        0x10000
        0x0
        0x10000
        0x10000
        0x0
        0x0
    .end array-data

    :array_4
    .array-data 4
        -0x10000
        0x10000
        0x0
        -0x10000
        -0x10000
        0x0
        0x10000
        0x10000
        0x0
        0x10000
        -0x10000
        0x0
    .end array-data

    :array_5
    .array-data 4
        -0x10000
        0x0
        0x10000
        -0x10000
        0x0
        -0x10000
        0x10000
        0x0
        0x10000
        0x10000
        0x0
        -0x10000
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x10000
        0x0
        0x0
        0x0
        0x0
        0x10000
        0x0
        0x0
        0x10000
        0x10000
        0x0
    .end array-data
.end method

.method private M()V
    .locals 3

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbd0

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb44

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb71

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb90

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0x405

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glCullFace(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0x901

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glFrontFace(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0x1d01

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xc50

    const/16 v2, 0x1102

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glHint(II)V

    const/4 v0, 0x0

    iput v0, p0, LD/a;->r:I

    return-void
.end method

.method private N()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LD/a;->s:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, LD/a;->s:[I

    iget-object v2, p0, LD/a;->s:[I

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, LD/a;->s:[I

    return-void
.end method

.method public static a(LD/a;)J
    .locals 2

    if-nez p0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LD/a;->J:J

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 3

    if-eqz p2, :cond_0

    iget v0, p0, LD/a;->r:I

    or-int/2addr v0, p1

    iput v0, p0, LD/a;->r:I

    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->q:I

    aput p1, v0, v1

    :goto_0
    return-void

    :cond_0
    iget v0, p0, LD/a;->r:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LD/a;->r:I

    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->q:I

    const/high16 v2, 0x40000000

    or-int/2addr v2, p1

    aput v2, v0, v1

    goto :goto_0
.end method

.method public static b(J)LD/a;
    .locals 4

    sget-object v2, LD/a;->K:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    sget-object v0, LD/a;->K:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/a;

    if-nez v0, :cond_0

    sget-object v1, LD/a;->K:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    monitor-exit v2

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 3

    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->q:I

    const v2, 0x7fffffff

    aput v2, v0, v1

    return-void
.end method

.method public B()V
    .locals 9

    const/16 v8, 0xde1

    const/16 v7, 0xbe2

    const/16 v6, 0xbd0

    const/16 v5, 0xb90

    const/16 v4, 0xb71

    :goto_0
    iget v0, p0, LD/a;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LD/a;->q:I

    if-ltz v0, :cond_a

    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    aget v0, v0, v1

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_a

    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    aget v0, v0, v1

    const/high16 v1, 0x40000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, LD/a;->p:[I

    iget v2, p0, LD/a;->q:I

    aget v1, v1, v2

    const v2, 0x3fffffff

    and-int/2addr v1, v2

    sparse-switch v1, :sswitch_data_0

    :goto_2
    if-eqz v0, :cond_9

    iget v0, p0, LD/a;->r:I

    or-int/2addr v0, v1

    iput v0, p0, LD/a;->r:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_0
    if-eqz v0, :cond_1

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8074

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto :goto_2

    :cond_1
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8074

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    goto :goto_2

    :sswitch_1
    if-eqz v0, :cond_2

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v8}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8078

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto :goto_2

    :cond_2
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8078

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    goto :goto_2

    :sswitch_2
    if-eqz v0, :cond_3

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v7}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto :goto_2

    :cond_3
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_2

    :sswitch_3
    if-eqz v0, :cond_4

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8076

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto :goto_2

    :cond_4
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8076

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    goto :goto_2

    :sswitch_4
    if-eqz v0, :cond_5

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto :goto_2

    :cond_5
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_2

    :sswitch_5
    if-eqz v0, :cond_6

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto :goto_2

    :cond_6
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_2

    :sswitch_6
    if-eqz v0, :cond_7

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8037

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto/16 :goto_2

    :cond_7
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8037

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto/16 :goto_2

    :sswitch_7
    if-eqz v0, :cond_8

    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto/16 :goto_2

    :cond_8
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto/16 :goto_2

    :cond_9
    iget v0, p0, LD/a;->r:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LD/a;->r:I

    goto/16 :goto_0

    :cond_a
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
        0x80 -> :sswitch_7
    .end sparse-switch
.end method

.method public declared-synchronized C()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget v0, p0, LD/a;->t:I

    if-lez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    iget v1, p0, LD/a;->t:I

    iget-object v2, p0, LD/a;->s:[I

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    const/4 v0, 0x0

    iput v0, p0, LD/a;->t:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public D()V
    .locals 1

    iget-object v0, p0, LD/a;->h:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LD/a;->e:LE/g;

    invoke-virtual {v0, p0}, LE/g;->c(LD/a;)V

    iget-object v0, p0, LD/a;->d:LE/i;

    invoke-virtual {v0, p0}, LE/i;->c(LD/a;)V

    iget-object v0, p0, LD/a;->j:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LD/a;->f:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LD/a;->i:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LD/a;->g:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    return-void
.end method

.method public E()I
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LD/a;->u:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    iget-object v1, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v2, 0xd33

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    aget v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LD/a;->u:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, LD/a;->u:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public F()I
    .locals 4

    iget-object v0, p0, LD/a;->v:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v2, 0x846e

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LD/a;->v:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, LD/a;->v:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public G()Lx/a;
    .locals 1

    iget-object v0, p0, LD/a;->G:Lx/a;

    return-object v0
.end method

.method public H()Z
    .locals 1

    iget-boolean v0, p0, LD/a;->c:Z

    return v0
.end method

.method public I()I
    .locals 1

    iget v0, p0, LD/a;->B:I

    return v0
.end method

.method public J()Z
    .locals 1

    iget-boolean v0, p0, LD/a;->C:Z

    return v0
.end method

.method public K()I
    .locals 1

    iget v0, p0, LD/a;->D:I

    return v0
.end method

.method public L()LE/n;
    .locals 1

    iget-object v0, p0, LD/a;->H:LE/n;

    return-object v0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LD/a;->w:Z

    return-void
.end method

.method public a(J)V
    .locals 4

    iget-wide v0, p0, LD/a;->x:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iput-wide p1, p0, LD/a;->x:J

    :goto_0
    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    iget-wide v1, p0, LD/a;->x:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(J)V

    return-void

    :cond_0
    iget-wide v0, p0, LD/a;->x:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LD/a;->x:J

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LD/a;->C:Z

    return-void
.end method

.method public a(I)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/u;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    iput-boolean v0, p0, LD/a;->w:Z

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public b()J
    .locals 6

    const-wide/16 v4, 0x0

    iget-wide v0, p0, LD/a;->x:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LD/a;->x:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/u;->b(I)V

    return-void
.end method

.method public declared-synchronized c(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, LD/a;->t:I

    iget-object v1, p0, LD/a;->s:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, LD/a;->N()V

    :cond_0
    iget-object v0, p0, LD/a;->s:[I

    iget v1, p0, LD/a;->t:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->t:I

    aput p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, LD/a;->w:Z

    return v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, LD/a;->y:J

    return-wide v0
.end method

.method public e()V
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->a()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LD/a;->y:J

    const/4 v0, 0x0

    iput-boolean v0, p0, LD/a;->w:Z

    iget-wide v0, p0, LD/a;->x:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, LD/a;->y:J

    iget-wide v2, p0, LD/a;->x:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iput-wide v4, p0, LD/a;->x:J

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->b()V

    return-void
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, LD/a;->z:Z

    return v0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, LD/a;->z:Z

    return-void
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, LD/a;->A:Z

    return v0
.end method

.method public j()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, LD/a;->A:Z

    return-void
.end method

.method public k()Lx/k;
    .locals 1

    iget-object v0, p0, LD/a;->o:Lx/k;

    return-object v0
.end method

.method public l()Lz/k;
    .locals 1

    iget-object v0, p0, LD/a;->E:Lz/k;

    return-object v0
.end method

.method public m()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8074

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    invoke-direct {p0, v2, v2}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public n()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8076

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public o()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8076

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xde1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8078

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public q()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xde1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8078

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public r()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbe2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public s()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbe2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public t()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbd0

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public u()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    iput-boolean v2, p0, LD/a;->z:Z

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb71

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v0, 0x20

    invoke-direct {p0, v0, v2}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public v()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8037

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v0, 0x40

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public w()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, LD/a;->r:I

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_0

    iput-boolean v2, p0, LD/a;->A:Z

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb90

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v0, 0x80

    invoke-direct {p0, v0, v2}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public x()V
    .locals 2

    iget v0, p0, LD/a;->r:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb90

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v0, 0x80

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    :cond_0
    return-void
.end method

.method public y()Ljavax/microedition/khronos/opengles/GL10;
    .locals 1

    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/u;
    .locals 1

    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    return-object v0
.end method
