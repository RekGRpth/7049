.class public LE/c;
.super LE/a;
.source "SourceFile"


# instance fields
.field private final g:[I

.field private volatile h:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, LE/a;-><init>(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/c;->g:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/c;->h:J

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    invoke-direct {p0, p1, p2}, LE/a;-><init>(IZ)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/c;->g:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/c;->h:J

    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LE/c;->g:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, LE/c;->g:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    iget-wide v0, p0, LE/c;->h:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, LE/c;->g:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, LE/c;->g:[I

    aput v3, v0, v3

    iput v3, p0, LE/c;->e:I

    :cond_1
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/c;->h:J

    return-void
.end method

.method public c()I
    .locals 2

    const/16 v0, 0x38

    iget-object v1, p0, LE/c;->f:Li/b;

    if-eqz v1, :cond_1

    iget-object v1, p0, LE/c;->f:Li/b;

    invoke-virtual {v1}, Li/b;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LE/c;->a:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, LE/c;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(LD/a;)V
    .locals 6

    const v5, 0x8892

    const/4 v4, 0x0

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/c;->h:J

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, LE/a;->c(LD/a;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, LE/c;->g:[I

    aget v1, v1, v4

    if-nez v1, :cond_3

    iget-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, LE/c;->d(LD/a;)V

    :cond_2
    iget-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, LE/c;->g:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    iget-object v1, p0, LE/c;->g:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iput v1, p0, LE/c;->e:I

    iget v1, p0, LE/c;->e:I

    iget-object v2, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v1, 0x0

    iput-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    :cond_3
    iget-object v1, p0, LE/c;->g:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    const/4 v1, 0x4

    const/16 v2, 0x1401

    invoke-interface {v0, v1, v2, v4, v4}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method protected d(LD/a;)V
    .locals 2

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, LE/c;->c:I

    mul-int/lit8 v1, v0, 0x4

    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, LE/c;->a(IZ)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, LE/a;->d(LD/a;)V

    goto :goto_1
.end method
