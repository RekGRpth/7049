.class public LE/f;
.super LE/d;
.source "SourceFile"


# instance fields
.field private final h:[I

.field private volatile i:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, LE/d;-><init>(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/f;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/f;->i:J

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    invoke-direct {p0, p1, p2}, LE/d;-><init>(IZ)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/f;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/f;->i:J

    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, LE/d;->a(LD/a;)V

    iget-object v0, p0, LE/f;->h:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/f;->h:[I

    aput v1, v0, v1

    :cond_0
    return-void
.end method

.method public a(LD/a;I)V
    .locals 6

    const v5, 0x8893

    const/4 v4, 0x0

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/f;->i:J

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, LE/d;->a(LD/a;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, LE/f;->h:[I

    aget v1, v1, v4

    if-nez v1, :cond_3

    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, LE/f;->d(LD/a;)V

    :cond_2
    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, LE/f;->h:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    iget-object v1, p0, LE/f;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, LE/f;->f:I

    iget v1, p0, LE/f;->f:I

    iget-object v2, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v1, 0x0

    iput-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    :cond_3
    iget-object v1, p0, LE/f;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget v1, p0, LE/f;->c:I

    const/16 v2, 0x1403

    invoke-interface {v0, p2, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glDrawElements(IIII)V

    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method public b(LD/a;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LE/f;->h:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, LE/f;->h:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    iget-wide v0, p0, LE/f;->i:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, LE/f;->h:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, LE/f;->h:[I

    aput v3, v0, v3

    iput v3, p0, LE/f;->f:I

    :cond_1
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/f;->i:J

    return-void
.end method

.method public d()I
    .locals 2

    const/16 v0, 0x38

    iget-object v1, p0, LE/f;->g:Li/j;

    if-eqz v1, :cond_1

    iget-object v1, p0, LE/f;->g:Li/j;

    invoke-virtual {v1}, Li/j;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LE/f;->a:[S

    if-eqz v1, :cond_0

    iget-object v1, p0, LE/f;->a:[S

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected d(LD/a;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->b()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    iget-object v0, p0, LE/f;->g:Li/j;

    if-nez v0, :cond_2

    iget-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    iget-object v1, p0, LE/f;->a:[S

    iget v2, p0, LE/f;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    :goto_0
    iget-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    iget v1, p0, LE/f;->c:I

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LE/f;->g:Li/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/f;->g:Li/j;

    invoke-virtual {v0}, Li/j;->c()V

    iput-object v4, p0, LE/f;->g:Li/j;

    :cond_0
    iput-object v4, p0, LE/f;->a:[S

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, LE/f;->a()V

    iget-object v0, p0, LE/f;->g:Li/j;

    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v1}, Li/j;->a(Ljava/nio/ShortBuffer;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, LE/d;->d(LD/a;)V

    goto :goto_1
.end method
