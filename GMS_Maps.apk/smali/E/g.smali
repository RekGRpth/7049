.class public LE/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:I

.field private final c:Z

.field private final d:Z

.field private e:I

.field private final f:[I

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, LE/g;->f:[I

    iput-boolean v2, p0, LE/g;->g:Z

    iput p1, p0, LE/g;->b:I

    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LE/g;->c:Z

    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, LE/g;->d:Z

    const/16 v0, 0xc

    iput v0, p0, LE/g;->e:I

    iget-boolean v0, p0, LE/g;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, LE/g;->e:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, LE/g;->e:I

    :cond_0
    iget-boolean v0, p0, LE/g;->c:Z

    if-eqz v0, :cond_1

    iget v0, p0, LE/g;->e:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, LE/g;->e:I

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private e(LD/a;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, LE/g;->f:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    const v1, 0x8892

    iget-object v2, p0, LE/g;->f:[I

    aget v2, v2, v3

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LE/g;->h:I

    return v0
.end method

.method public a(LD/a;)V
    .locals 7

    const v6, 0x8892

    const/16 v5, 0x1406

    const/4 v4, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, LE/g;->f:[I

    aget v1, v1, v4

    if-nez v1, :cond_3

    invoke-direct {p0, p1}, LE/g;->e(LD/a;)V

    :goto_0
    iget-boolean v1, p0, LE/g;->g:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v1, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget-object v2, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v6, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    iput-boolean v4, p0, LE/g;->g:Z

    :cond_0
    const/4 v1, 0x3

    iget v2, p0, LE/g;->e:I

    invoke-interface {v0, v1, v5, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    const/16 v1, 0xc

    iget-boolean v2, p0, LE/g;->d:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    iget v3, p0, LE/g;->e:I

    invoke-interface {v0, v2, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    const/16 v1, 0x1c

    :cond_1
    iget-boolean v2, p0, LE/g;->c:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    iget v3, p0, LE/g;->e:I

    invoke-interface {v0, v2, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    add-int/lit8 v1, v1, 0x8

    :cond_2
    invoke-interface {v0, v6, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    return-void

    :cond_3
    iget-object v1, p0, LE/g;->f:[I

    aget v1, v1, v4

    invoke-interface {v0, v6, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 2

    iput-object p1, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    iput-boolean v0, p0, LE/g;->g:Z

    iget-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LE/g;->e:I

    div-int/2addr v0, v1

    iput v0, p0, LE/g;->h:I

    return-void
.end method

.method public b(LD/a;)V
    .locals 1

    invoke-virtual {p0, p1}, LE/g;->d(LD/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public c(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, LE/g;->d(LD/a;)V

    return-void
.end method

.method public d(LD/a;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LE/g;->f:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, LE/g;->f:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, LE/g;->f:[I

    aput v3, v0, v3

    iget-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_1
    return-void
.end method
