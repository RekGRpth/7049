.class public LE/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LE/q;


# instance fields
.field protected a:I

.field b:[I

.field c:I

.field d:I

.field e:Ljava/nio/IntBuffer;

.field f:I

.field protected g:Li/h;

.field private h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LE/o;->a:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LE/o;-><init>(IZ)V

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LE/o;->a:I

    iput-boolean p2, p0, LE/o;->h:Z

    iput p1, p0, LE/o;->c:I

    invoke-direct {p0}, LE/o;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, LE/o;->f:I

    iget-object v0, p0, LE/o;->b:[I

    if-nez v0, :cond_3

    iget v0, p0, LE/o;->c:I

    mul-int/lit8 v0, v0, 0x3

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    iget-boolean v1, p0, LE/o;->h:Z

    if-eqz v1, :cond_2

    :cond_0
    new-array v0, v0, [I

    iput-object v0, p0, LE/o;->b:[I

    :cond_1
    :goto_0
    iput v2, p0, LE/o;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    return-void

    :cond_2
    new-instance v1, Li/h;

    invoke-direct {v1, v0}, Li/h;-><init>(I)V

    iput-object v1, p0, LE/o;->g:Li/h;

    invoke-virtual {p0}, LE/o;->b()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LE/o;->g:Li/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, LE/o;->g:Li/h;

    invoke-virtual {v0}, Li/h;->a()V

    invoke-virtual {p0}, LE/o;->b()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LE/o;->d:I

    return v0
.end method

.method public a(FFF)V
    .locals 4

    const/high16 v3, 0x47800000

    iget v0, p0, LE/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/o;->d:I

    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/o;->f:I

    mul-float v2, p1, v3

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/o;->f:I

    mul-float v2, p2, v3

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/o;->f:I

    mul-float v2, p3, v3

    float-to-int v2, v2

    aput v2, v0, v1

    iget v0, p0, LE/o;->f:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LE/o;->b()V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, LE/o;->c:I

    if-le p1, v0, :cond_3

    iget v0, p0, LE/o;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    mul-int/lit8 v0, v1, 0x3

    iget-object v2, p0, LE/o;->g:Li/h;

    if-nez v2, :cond_5

    const/16 v2, 0x400

    if-lt v0, v2, :cond_0

    iget-boolean v2, p0, LE/o;->h:Z

    if-eqz v2, :cond_4

    :cond_0
    iget-boolean v2, p0, LE/o;->h:Z

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-array v0, v0, [I

    iget-object v2, p0, LE/o;->b:[I

    iget v3, p0, LE/o;->f:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, LE/o;->b:[I

    :goto_0
    iput v1, p0, LE/o;->c:I

    :cond_3
    return-void

    :cond_4
    new-instance v2, Li/h;

    invoke-direct {v2, v0}, Li/h;-><init>(I)V

    iput-object v2, p0, LE/o;->g:Li/h;

    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v2, p0, LE/o;->b:[I

    iget v3, p0, LE/o;->f:I

    invoke-virtual {v0, v2, v3}, Li/h;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/o;->b:[I

    iget-object v0, p0, LE/o;->g:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/o;->f:I

    goto :goto_0

    :cond_5
    iget-object v2, p0, LE/o;->g:Li/h;

    invoke-virtual {v2, v0}, Li/h;->c(I)V

    goto :goto_0
.end method

.method public a(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, LE/o;->b(LD/a;)V

    invoke-direct {p0}, LE/o;->e()V

    return-void
.end method

.method public a(Lo/T;I)V
    .locals 2

    iget v0, p0, LE/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/o;->d:I

    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    invoke-virtual {p1, p2, v0, v1}, Lo/T;->a(I[II)V

    iget v0, p0, LE/o;->f:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, LE/o;->f:I

    iget v0, p0, LE/o;->f:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LE/o;->b()V

    :cond_0
    return-void
.end method

.method public a(Lo/T;IB)V
    .locals 0

    invoke-virtual {p0, p1, p2}, LE/o;->a(Lo/T;I)V

    return-void
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, LE/o;->g:Li/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/o;->g:Li/h;

    iget v1, p0, LE/o;->f:I

    invoke-virtual {v0, v1}, Li/h;->b(I)V

    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/o;->b:[I

    iget-object v0, p0, LE/o;->g:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/o;->f:I

    :cond_0
    return-void
.end method

.method public b(LD/a;)V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, LE/o;->a:I

    return v0
.end method

.method public c(LD/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LE/o;->g:Li/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/o;->g:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    iput-object v1, p0, LE/o;->g:Li/h;

    :cond_0
    iput-object v1, p0, LE/o;->b:[I

    return-void
.end method

.method public d()I
    .locals 2

    const/16 v0, 0x20

    iget-object v1, p0, LE/o;->g:Li/h;

    if-eqz v1, :cond_2

    iget-object v1, p0, LE/o;->g:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    if-eqz v1, :cond_1

    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->capacity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, LE/o;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, LE/o;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public d(LD/a;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LE/o;->e(LD/a;)V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iput v0, p0, LE/o;->a:I

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x3

    const/16 v2, 0x140c

    const/4 v3, 0x0

    iget-object v4, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected e(LD/a;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget v0, p0, LE/o;->d:I

    mul-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v1

    mul-int/lit8 v2, v0, 0x4

    invoke-virtual {v1, v2}, Lx/k;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    iget-object v1, p0, LE/o;->g:Li/h;

    if-nez v1, :cond_0

    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/o;->b:[I

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    :goto_0
    iget-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    iput-object v4, p0, LE/o;->b:[I

    return-void

    :cond_0
    invoke-virtual {p0}, LE/o;->b()V

    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v1}, Li/h;->a(Ljava/nio/IntBuffer;)V

    iget-object v0, p0, LE/o;->g:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    iput-object v4, p0, LE/o;->g:Li/h;

    goto :goto_0
.end method
