.class public LE/r;
.super LE/o;
.source "SourceFile"


# instance fields
.field private final h:[I

.field private volatile i:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, LE/o;-><init>(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/r;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/r;->i:J

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    invoke-direct {p0, p1, p2}, LE/o;-><init>(IZ)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/r;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/r;->i:J

    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, LE/o;->a(LD/a;)V

    iget-object v0, p0, LE/r;->h:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/r;->h:[I

    aput v1, v0, v1

    :cond_0
    return-void
.end method

.method public b(LD/a;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LE/r;->h:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, LE/r;->h:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    iget-wide v0, p0, LE/r;->i:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, LE/r;->h:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, LE/r;->h:[I

    aput v3, v0, v3

    iput v3, p0, LE/r;->a:I

    :cond_1
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/r;->i:J

    return-void
.end method

.method public d()I
    .locals 2

    const/16 v0, 0x38

    iget-object v1, p0, LE/r;->g:Li/h;

    if-eqz v1, :cond_1

    iget-object v1, p0, LE/r;->g:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LE/r;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, LE/r;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public d(LD/a;)V
    .locals 6

    const v5, 0x8892

    const/4 v4, 0x0

    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/r;->i:J

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, LE/o;->d(LD/a;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, LE/r;->h:[I

    aget v1, v1, v4

    if-nez v1, :cond_3

    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, LE/r;->e(LD/a;)V

    :cond_2
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, LE/r;->h:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    iget-object v1, p0, LE/r;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    iput v1, p0, LE/r;->a:I

    iget v1, p0, LE/r;->a:I

    iget-object v2, p0, LE/r;->e:Ljava/nio/IntBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v1, 0x0

    iput-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    :cond_3
    iget-object v1, p0, LE/r;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    const/4 v1, 0x3

    const/16 v2, 0x140c

    invoke-interface {v0, v1, v2, v4, v4}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method protected e(LD/a;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, LE/r;->d:I

    mul-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v1

    invoke-virtual {v1}, LE/n;->c()Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    iget-object v1, p0, LE/r;->g:Li/h;

    if-nez v1, :cond_2

    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/r;->b:[I

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    :goto_0
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/IntBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LE/r;->g:Li/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/r;->g:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    iput-object v4, p0, LE/r;->g:Li/h;

    :cond_0
    iput-object v4, p0, LE/r;->b:[I

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, LE/r;->b()V

    iget-object v1, p0, LE/r;->g:Li/h;

    iget-object v2, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v2}, Li/h;->a(Ljava/nio/IntBuffer;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, LE/o;->e(LD/a;)V

    goto :goto_1
.end method
