.class LF/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private final c:I

.field private final d:F

.field private final e:I


# direct methods
.method public constructor <init>(LF/A;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->a:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->b:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, LF/C;->d:F

    const/4 v0, 0x0

    iput v0, p0, LF/C;->c:I

    iget-object v0, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {p1}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {p1}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0}, LF/C;->a()I

    move-result v0

    iput v0, p0, LF/C;->e:I

    return-void
.end method

.method public constructor <init>(Ljava/util/List;FIZ)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->a:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->b:Ljava/util/List;

    iput p2, p0, LF/C;->d:F

    iput p3, p0, LF/C;->c:I

    if-eqz p4, :cond_1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    iget-object v1, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-direct {p0}, LF/C;->a()I

    move-result v0

    iput v0, p0, LF/C;->e:I

    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    iget-object v2, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private a()I
    .locals 2

    iget-object v0, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LF/C;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LF/C;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, LF/C;

    iget v2, p1, LF/C;->d:F

    iget v3, p0, LF/C;->d:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, LF/C;->c:I

    iget v3, p1, LF/C;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, LF/C;->a:Ljava/util/List;

    iget-object v3, p1, LF/C;->a:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, LF/C;->b:Ljava/util/List;

    iget-object v3, p1, LF/C;->b:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, LF/C;->e:I

    return v0
.end method
