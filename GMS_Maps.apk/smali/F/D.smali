.class public LF/D;
.super LF/m;
.source "SourceFile"


# instance fields
.field private final A:F

.field private final B:F

.field private final C:F

.field private D:I

.field private E:I

.field private F:F

.field private final G:I

.field private H:Z

.field private I:I

.field private J:Z

.field private final K:Ljava/lang/String;

.field private final L:F

.field private final M:[F

.field private final l:Ljava/lang/String;

.field private final m:Lcom/google/android/maps/driveabout/vector/aX;

.field private final n:Lo/X;

.field private o:Lo/X;

.field private final p:F

.field private q:Lo/W;

.field private r:[LF/F;

.field private final s:Lcom/google/android/maps/driveabout/vector/aV;

.field private t:LD/b;

.field private u:LE/i;

.field private v:LE/i;

.field private w:Z

.field private x:LE/i;

.field private y:LE/o;

.field private z:Lh/e;


# direct methods
.method constructor <init>(Lo/n;Ly/b;Ljava/lang/String;Lo/aj;IIZFFFILo/X;FFLcom/google/android/maps/driveabout/vector/aX;FLcom/google/android/maps/driveabout/vector/aV;Z)V
    .locals 10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move/from16 v5, p8

    move/from16 v6, p9

    move v7, p5

    move/from16 v8, p7

    move/from16 v9, p18

    invoke-direct/range {v1 .. v9}, LF/m;-><init>(Lo/n;Ly/b;Lo/aj;FFIZZ)V

    const/4 v1, 0x0

    iput-boolean v1, p0, LF/D;->w:Z

    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, LF/D;->M:[F

    iput-object p3, p0, LF/D;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "L"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LF/D;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LF/D;->K:Ljava/lang/String;

    move-object/from16 v0, p12

    iput-object v0, p0, LF/D;->n:Lo/X;

    mul-float v1, p16, p10

    iput v1, p0, LF/D;->p:F

    move/from16 v0, p6

    iput v0, p0, LF/D;->G:I

    move-object/from16 v0, p17

    iput-object v0, p0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    move/from16 v0, p10

    iput v0, p0, LF/D;->A:F

    move/from16 v0, p13

    iput v0, p0, LF/D;->B:F

    move/from16 v0, p14

    iput v0, p0, LF/D;->C:F

    move-object/from16 v0, p15

    iput-object v0, p0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    const/4 v1, 0x0

    iput-boolean v1, p0, LF/D;->J:Z

    move/from16 v0, p11

    iput v0, p0, LF/D;->E:I

    const/4 v1, 0x0

    iput v1, p0, LF/D;->I:I

    mul-float v1, p16, p13

    iput v1, p0, LF/D;->L:F

    return-void
.end method

.method static a(F[FI)I
    .locals 4

    const/4 v3, 0x0

    aget v0, p1, p2

    sub-float v0, p0, v0

    cmpg-float v1, v0, v3

    if-gtz v1, :cond_2

    :cond_0
    :goto_0
    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    move v0, v1

    :cond_2
    cmpl-float v1, v0, v3

    if-lez v1, :cond_3

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge p2, v1, :cond_3

    add-int/lit8 v1, p2, 0x1

    aget v1, p1, v1

    sub-float v1, p0, v1

    cmpg-float v2, v1, v3

    if-gtz v2, :cond_1

    neg-float v1, v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    array-length v0, p1

    add-int/lit8 p2, v0, -0x1

    goto :goto_0
.end method

.method static a(Lo/X;)I
    .locals 3

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v1, v0, Lx/l;->a:Lo/T;

    iget-object v0, v0, Lx/l;->b:Lo/T;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lo/X;->a(ILo/T;)V

    invoke-virtual {p0, v0}, Lo/X;->a(Lo/T;)V

    invoke-static {v1, v0}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lo/aj;ZLcom/google/android/maps/driveabout/vector/q;)I
    .locals 3

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    invoke-static {v0}, Lx/d;->a(I)I

    move-result v1

    const/16 v2, 0x80

    if-lt v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p2}, LF/m;->b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Lo/K;Ly/b;Lo/H;Lo/X;ZFLcom/google/android/maps/driveabout/vector/aX;FLC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;
    .locals 12

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v11}, LF/D;->a(Lo/n;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lo/af;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;
    .locals 1

    invoke-static/range {p0 .. p11}, LF/D;->a(Lo/n;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lo/n;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;
    .locals 22

    invoke-virtual/range {p9 .. p9}, LC/a;->y()F

    move-result v13

    invoke-virtual/range {p3 .. p3}, Lo/X;->b()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_6

    mul-float v3, p6, v13

    const v4, 0x3e4ccccd

    mul-float/2addr v3, v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lo/X;->b(F)Lo/X;

    move-result-object v15

    :goto_0
    invoke-interface/range {p0 .. p0}, Lo/n;->e()Lo/aj;

    move-result-object v7

    const/4 v3, 0x0

    :goto_1
    invoke-virtual/range {p2 .. p2}, Lo/H;->b()I

    move-result v4

    if-ge v3, v4, :cond_0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lo/H;->a(I)Lo/I;

    move-result-object v4

    invoke-virtual {v4}, Lo/I;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lo/H;->a(I)Lo/I;

    move-result-object v3

    invoke-virtual {v3}, Lo/I;->j()Lo/aj;

    move-result-object v7

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lo/H;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_2
    move-object/from16 v0, p10

    move-object/from16 v1, p8

    move/from16 v2, p6

    invoke-virtual {v0, v6, v1, v3, v2}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;F)F

    move-result v19

    const/high16 v3, 0x3f800000

    add-float v3, v3, v19

    invoke-virtual/range {p9 .. p9}, LC/a;->o()F

    move-result v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v4}, LC/a;->a(FF)F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v4, v19, v4

    if-nez v4, :cond_4

    const/4 v3, 0x0

    :cond_1
    :goto_3
    return-object v3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v15}, Lo/X;->d()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    new-instance v3, LF/D;

    invoke-interface/range {p0 .. p0}, Lo/n;->i()I

    move-result v8

    const/high16 v11, -0x40800000

    const/high16 v12, -0x40800000

    invoke-virtual/range {p9 .. p9}, LC/a;->p()F

    move-result v4

    float-to-int v14, v4

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v16, p6

    move/from16 v17, p7

    move-object/from16 v18, p8

    move-object/from16 v20, p10

    move/from16 v21, p11

    invoke-direct/range {v3 .. v21}, LF/D;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/aj;IIZFFFILo/X;FFLcom/google/android/maps/driveabout/vector/aX;FLcom/google/android/maps/driveabout/vector/aV;Z)V

    invoke-virtual {v3}, LF/D;->a()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v3, 0x0

    goto :goto_3

    :cond_6
    move-object/from16 v15, p3

    goto/16 :goto_0
.end method

.method static a(Lo/X;FF)Lo/X;
    .locals 12

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v6, v0, Lx/l;->i:Lo/T;

    iget-object v7, v0, Lx/l;->j:Lo/T;

    iget-object v8, v0, Lx/l;->k:Lo/T;

    iget-object v9, v0, Lx/l;->l:Lo/T;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v2

    add-int/lit8 v10, v2, -0x1

    const/4 v2, 0x0

    move v3, p2

    :goto_0
    if-ge v2, v10, :cond_0

    invoke-virtual {p0, v2}, Lo/X;->b(I)F

    move-result v4

    sub-float/2addr p1, v4

    const v5, 0x38d1b717

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_1

    const v5, -0x472e48e9

    cmpg-float v5, p1, v5

    if-gez v5, :cond_0

    const/4 v0, 0x1

    const/high16 v5, 0x3f800000

    div-float v4, p1, v4

    add-float/2addr v4, v5

    invoke-virtual {p0, v2, v9}, Lo/X;->a(ILo/T;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5, v8}, Lo/X;->a(ILo/T;)V

    invoke-static {v9, v8, v4, v6}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    :cond_0
    move v4, v2

    :goto_1
    if-ge v4, v10, :cond_8

    invoke-virtual {p0, v4}, Lo/X;->b(I)F

    move-result v11

    sub-float v5, v3, v11

    const v3, 0x38d1b717

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_2

    const v3, 0x38d1b717

    cmpg-float v3, v5, v3

    if-gez v3, :cond_8

    const/4 v1, 0x1

    const/high16 v3, 0x3f800000

    div-float/2addr v5, v11

    add-float/2addr v3, v5

    invoke-virtual {p0, v4, v9}, Lo/X;->a(ILo/T;)V

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v5, v8}, Lo/X;->a(ILo/T;)V

    invoke-static {v9, v8, v3, v7}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    move v3, v1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    sub-int v1, v4, v2

    add-int/lit8 v5, v1, 0x1

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :goto_3
    add-int/2addr v5, v1

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    :goto_4
    add-int/2addr v1, v5

    mul-int/lit8 v1, v1, 0x3

    new-array v5, v1, [I

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    invoke-virtual {v6, v5, v1}, Lo/T;->a([II)V

    :goto_5
    move v1, v0

    move v0, v2

    :goto_6
    if-gt v0, v4, :cond_5

    invoke-virtual {p0, v0, v8}, Lo/X;->a(ILo/T;)V

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v8, v5, v1}, Lo/T;->a([II)V

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_6

    :cond_1
    sub-float/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    if-eqz v3, :cond_6

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v7, v5, v1}, Lo/T;->a([II)V

    :cond_6
    invoke-static {v5}, Lo/X;->a([I)Lo/X;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v3, v1

    goto :goto_2
.end method

.method private a(LD/a;LC/a;LF/F;F)V
    .locals 7

    const/high16 v6, 0x3f800000

    const/4 v5, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v2, v0, Lx/l;->a:Lo/T;

    iget-object v0, v0, Lx/l;->b:Lo/T;

    invoke-virtual {p2, v0}, LC/a;->a(Lo/T;)V

    iget-object v3, p3, LF/F;->a:Lo/T;

    invoke-static {v3, v0, v2}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    invoke-virtual {p2}, LC/a;->w()F

    move-result v0

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    invoke-virtual {v2}, Lo/T;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-interface {v1, v3, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v2, 0x42b40000

    iget v3, p3, LF/F;->d:F

    sub-float/2addr v2, v3

    invoke-interface {v1, v2, v5, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    cmpl-float v2, p4, v5

    if-eqz v2, :cond_0

    invoke-interface {v1, p4, v6, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_0
    iget v2, p3, LF/F;->b:F

    mul-float/2addr v2, v0

    iget v3, p3, LF/F;->c:F

    mul-float/2addr v0, v3

    invoke-interface {v1, v2, v0, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void
.end method

.method private a(LD/a;Lo/X;F)V
    .locals 11

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v2, v0, Lx/l;->a:Lo/T;

    iget-object v1, v0, Lx/l;->b:Lo/T;

    invoke-virtual {p2}, Lo/X;->b()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    mul-int/lit8 v0, v3, 0x4

    const/4 v4, 0x1

    new-instance v5, LE/l;

    invoke-direct {v5, v0, v4}, LE/l;-><init>(IZ)V

    iput-object v5, p0, LF/D;->u:LE/i;

    new-instance v5, LE/l;

    invoke-direct {v5, v0, v4}, LE/l;-><init>(IZ)V

    iput-object v5, p0, LF/D;->v:LE/i;

    new-array v0, v3, [LF/F;

    iput-object v0, p0, LF/D;->r:[LF/F;

    new-array v7, v6, [F

    const/4 v0, 0x0

    const/4 v4, 0x0

    aput v4, v7, v0

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v2}, Lo/X;->a(ILo/T;)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p2, v4, v1}, Lo/X;->a(ILo/T;)V

    iget-object v4, p0, LF/D;->r:[LF/F;

    new-instance v5, LF/F;

    const/4 v8, 0x0

    invoke-direct {v5, v2, v1, p3, v8}, LF/F;-><init>(Lo/T;Lo/T;FLF/E;)V

    aput-object v5, v4, v0

    invoke-virtual {v2, v1}, Lo/T;->c(Lo/T;)F

    move-result v4

    add-int/lit8 v5, v0, 0x1

    aget v8, v7, v0

    add-float/2addr v4, v8

    aput v4, v7, v5

    add-int/lit8 v0, v0, 0x1

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto :goto_0

    :cond_0
    iget-object v0, p0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LF/D;->l:Ljava/lang/String;

    iget-object v2, p0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v3, p0, LF/D;->b:Lo/aj;

    if-eqz v3, :cond_1

    iget-object v3, p0, LF/D;->b:Lo/aj;

    invoke-virtual {v3}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_1
    iget v4, p0, LF/D;->B:F

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->b(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v3

    const/high16 v0, 0x3f800000

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget v1, v3, v1

    div-float v1, v0, v1

    const/4 v0, 0x0

    :goto_2
    array-length v2, v3

    if-ge v0, v2, :cond_2

    aget v2, v3, v0

    mul-float/2addr v2, v1

    aput v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    const/high16 v0, 0x3f800000

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget v1, v7, v1

    div-float v1, v0, v1

    new-array v4, v6, [F

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v6, :cond_3

    aget v2, v7, v0

    mul-float/2addr v2, v1

    aput v2, v7, v0

    add-int/lit8 v2, v6, -0x1

    sub-int/2addr v2, v0

    const/high16 v5, 0x3f800000

    aget v8, v7, v0

    sub-float/2addr v5, v8

    aput v5, v4, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v6, :cond_4

    aget v5, v7, v0

    invoke-static {v5, v3, v2}, LF/D;->a(F[FI)I

    move-result v2

    aget v5, v3, v2

    aput v5, v7, v0

    aget v5, v4, v0

    invoke-static {v5, v3, v1}, LF/D;->a(F[FI)I

    move-result v1

    aget v5, v3, v1

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v1

    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->c()F

    move-result v2

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v6, :cond_5

    aget v3, v7, v0

    mul-float/2addr v3, v1

    sub-int v5, v6, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v4, v5

    mul-float/2addr v5, v1

    iget-object v8, p0, LF/D;->u:LE/i;

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, LE/i;->a(FF)V

    iget-object v8, p0, LF/D;->u:LE/i;

    invoke-virtual {v8, v3, v2}, LE/i;->a(FF)V

    iget-object v3, p0, LF/D;->v:LE/i;

    invoke-virtual {v3, v5, v2}, LE/i;->a(FF)V

    iget-object v3, p0, LF/D;->v:LE/i;

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v8}, LE/i;->a(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    return-void
.end method

.method private b(F)V
    .locals 12

    const/4 v1, 0x1

    const/4 v11, 0x0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v2, v0, Lx/l;->a:Lo/T;

    iget-object v3, v0, Lx/l;->b:Lo/T;

    iget-object v4, v0, Lx/l;->c:Lo/T;

    iget-object v5, v0, Lx/l;->d:Lo/T;

    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v6

    iget v0, p0, LF/D;->A:F

    iget v7, p0, LF/D;->B:F

    mul-float/2addr v0, v7

    mul-float/2addr v0, p1

    const/high16 v7, 0x40000000

    div-float v7, v0, v7

    mul-int/lit8 v0, v6, 0x2

    new-array v8, v0, [Lo/T;

    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-virtual {v0, v11, v5}, Lo/X;->a(ILo/T;)V

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_1

    iget-object v9, p0, LF/D;->o:Lo/X;

    invoke-virtual {v9, v0, v4}, Lo/X;->a(ILo/T;)V

    invoke-static {v4, v5, v2}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    invoke-static {v2, v7, v3}, Lo/V;->a(Lo/T;FLo/T;)V

    invoke-virtual {v4, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v9

    aput-object v9, v8, v0

    mul-int/lit8 v9, v6, 0x2

    sub-int/2addr v9, v0

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v10

    aput-object v10, v8, v9

    if-ne v0, v1, :cond_0

    invoke-virtual {v5, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v9

    aput-object v9, v8, v11

    mul-int/lit8 v9, v6, 0x2

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v10

    aput-object v10, v8, v9

    :cond_0
    invoke-virtual {v5, v4}, Lo/T;->b(Lo/T;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lo/W;

    invoke-direct {v0, v8}, Lo/W;-><init>([Lo/T;)V

    iput-object v0, p0, LF/D;->q:Lo/W;

    return-void
.end method

.method static b(Lo/X;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    const/4 v2, 0x2

    if-ge v3, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v0}, Lo/X;->d(I)F

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lo/X;->d(I)F

    move-result v5

    sub-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42700000

    cmpl-float v6, v5, v6

    if-lez v6, :cond_2

    const/high16 v6, 0x43960000

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private d(LD/a;)V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->e()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LF/D;->A:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x3fc00000

    div-float v1, v0, v1

    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v2, v0, Lx/l;->a:Lo/T;

    iget-object v0, v0, Lx/l;->b:Lo/T;

    iget-object v3, p0, LF/D;->o:Lo/X;

    invoke-virtual {v3, v6, v2}, Lo/X;->a(ILo/T;)V

    iget-object v3, p0, LF/D;->o:Lo/X;

    invoke-virtual {v3, v4, v0}, Lo/X;->a(ILo/T;)V

    new-array v3, v4, [LF/F;

    iput-object v3, p0, LF/D;->r:[LF/F;

    iget-object v3, p0, LF/D;->r:[LF/F;

    new-instance v4, LF/F;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v0, v1, v5}, LF/F;-><init>(Lo/T;Lo/T;FLF/E;)V

    aput-object v4, v3, v6

    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    iget-object v1, p0, LF/D;->t:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v6

    iput v0, v2, LF/F;->e:F

    iget-object v0, p0, LF/D;->r:[LF/F;

    aget-object v0, v0, v6

    iput v1, v0, LF/F;->f:F

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-direct {p0, p1, v0, v1}, LF/D;->a(LD/a;Lo/X;F)V

    goto :goto_0
.end method

.method private e()Lo/X;
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, LF/D;->h()Lo/X;

    move-result-object v1

    invoke-virtual {v1}, Lo/X;->b()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_2

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    iget-object v3, p0, LF/D;->l:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v1}, LF/D;->b(Lo/X;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private h()Lo/X;
    .locals 8

    const/high16 v4, 0x40000000

    const/high16 v7, 0x3e800000

    iget-object v0, p0, LF/D;->n:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    iget v0, p0, LF/D;->I:I

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LF/D;->n:Lo/X;

    invoke-virtual {v0, v1}, Lo/X;->b(I)F

    move-result v3

    iget v0, p0, LF/D;->p:F

    cmpl-float v0, v3, v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v2, v0, Lx/l;->a:Lo/T;

    iget-object v4, v0, Lx/l;->b:Lo/T;

    iget-object v5, v0, Lx/l;->c:Lo/T;

    iget-object v0, v0, Lx/l;->d:Lo/T;

    iget-object v6, p0, LF/D;->n:Lo/X;

    invoke-virtual {v6, v1, v5}, Lo/X;->a(ILo/T;)V

    iget-object v6, p0, LF/D;->n:Lo/X;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v6, v1, v0}, Lo/X;->a(ILo/T;)V

    iget v1, p0, LF/D;->p:F

    sub-float v1, v3, v1

    div-float/2addr v1, v3

    mul-float v3, v1, v7

    invoke-static {v5, v0, v3, v2}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    const/high16 v3, 0x3f400000

    mul-float/2addr v1, v3

    invoke-static {v0, v5, v1, v4}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    invoke-static {v2, v4}, Lo/X;->a(Lo/T;Lo/T;)Lo/X;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput v0, p0, LF/D;->I:I

    :cond_2
    iget-object v0, p0, LF/D;->n:Lo/X;

    invoke-virtual {v0}, Lo/X;->d()F

    move-result v0

    iget v1, p0, LF/D;->I:I

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_1

    :pswitch_0
    iget v1, p0, LF/D;->p:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v7

    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget v1, p0, LF/D;->A:F

    mul-float/2addr v1, v4

    iget v2, p0, LF/D;->B:F

    mul-float/2addr v1, v2

    iget v2, p0, LF/D;->p:F

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x0

    iget v2, p0, LF/D;->p:F

    sub-float/2addr v0, v2

    iget v2, p0, LF/D;->A:F

    mul-float/2addr v2, v4

    iget v3, p0, LF/D;->B:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget v1, p0, LF/D;->p:F

    sub-float/2addr v0, v1

    const v1, 0x3ea8f5c3

    mul-float/2addr v0, v1

    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    iget v1, p0, LF/D;->p:F

    sub-float/2addr v0, v1

    const v1, 0x3f2b851f

    mul-float/2addr v0, v1

    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method a(LC/a;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v1, v0, Lx/l;->a:Lo/T;

    iget-object v0, v0, Lx/l;->b:Lo/T;

    iget-object v2, p0, LF/D;->o:Lo/X;

    invoke-virtual {v2, v4, v1}, Lo/X;->a(ILo/T;)V

    iget-object v2, p0, LF/D;->o:Lo/X;

    invoke-virtual {v2, v0}, Lo/X;->a(Lo/T;)V

    iget-object v2, p0, LF/D;->M:[F

    invoke-virtual {p1, v1, v2}, LC/a;->a(Lo/T;[F)V

    iget-object v1, p0, LF/D;->M:[F

    aget v1, v1, v4

    iget-object v2, p0, LF/D;->M:[F

    aget v2, v2, v5

    iget-object v3, p0, LF/D;->M:[F

    invoke-virtual {p1, v0, v3}, LC/a;->a(Lo/T;[F)V

    iget-object v0, p0, LF/D;->M:[F

    aget v3, v0, v4

    sub-float v1, v3, v1

    aput v1, v0, v4

    iget-object v0, p0, LF/D;->M:[F

    aget v1, v0, v5

    sub-float/2addr v1, v2

    aput v1, v0, v5

    return-void
.end method

.method public a(LD/a;)V
    .locals 1

    invoke-super {p0, p1}, LF/m;->a(LD/a;)V

    iget-object v0, p0, LF/D;->t:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, LF/D;->t:LD/b;

    :cond_0
    iget-object v0, p0, LF/D;->u:LE/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/D;->u:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    iget-object v0, p0, LF/D;->v:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    :cond_1
    iget-object v0, p0, LF/D;->y:LE/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/D;->y:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    :cond_2
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 10

    const/high16 v9, 0x40000000

    const/4 v4, 0x0

    const/high16 v1, 0x3f800000

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, LF/D;->J:Z

    if-nez v0, :cond_1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LF/D;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LF/D;->r:[LF/F;

    array-length v0, v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    invoke-virtual {p1}, LD/a;->p()V

    iget-object v0, p0, LF/D;->r:[LF/F;

    array-length v0, v0

    if-ne v0, v3, :cond_7

    iget-object v0, p1, LD/a;->e:LE/g;

    invoke-virtual {v0, p1}, LE/g;->a(LD/a;)V

    :goto_1
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0, v6}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, LF/D;->z:Lh/e;

    if-eqz v0, :cond_8

    iget-object v0, p0, LF/D;->z:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, LF/D;->z:Lh/e;

    :cond_2
    :goto_2
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, LF/D;->x:LE/i;

    if-eqz v0, :cond_3

    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_3

    invoke-virtual {p2}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_14

    :cond_3
    invoke-virtual {p0, p2}, LF/D;->a(LC/a;)V

    iget-object v0, p0, LF/D;->M:[F

    aget v0, v0, v4

    iget-object v2, p0, LF/D;->M:[F

    aget v7, v2, v3

    mul-float v2, v0, v0

    mul-float v8, v7, v7

    add-float/2addr v2, v8

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v8

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_9

    move v2, v1

    :goto_3
    cmpl-float v0, v7, v5

    if-ltz v0, :cond_a

    move v0, v1

    :goto_4
    mul-float/2addr v7, v0

    div-float/2addr v7, v8

    sub-float v7, v1, v7

    mul-float/2addr v7, v2

    iget-object v2, p0, LF/D;->x:LE/i;

    if-nez v2, :cond_4

    cmpl-float v2, v0, v5

    if-lez v2, :cond_b

    iget-object v2, p0, LF/D;->v:LE/i;

    :goto_5
    iput-object v2, p0, LF/D;->x:LE/i;

    cmpl-float v0, v0, v5

    if-lez v0, :cond_c

    move v0, v3

    :goto_6
    iput-boolean v0, p0, LF/D;->w:Z

    :cond_4
    iget-object v0, p0, LF/D;->x:LE/i;

    iget-object v2, p0, LF/D;->v:LE/i;

    if-ne v0, v2, :cond_f

    const v0, 0x3a83126f

    cmpg-float v0, v7, v0

    if-gez v0, :cond_d

    iget-object v0, p0, LF/D;->v:LE/i;

    :goto_7
    iput-object v0, p0, LF/D;->x:LE/i;

    const v0, 0x3a83126f

    cmpg-float v0, v7, v0

    if-gez v0, :cond_e

    move v0, v3

    :goto_8
    iput-boolean v0, p0, LF/D;->w:Z

    :goto_9
    iget-boolean v0, p0, LF/D;->H:Z

    if-nez v0, :cond_14

    const/high16 v0, 0x3f400000

    cmpl-float v0, v7, v0

    if-gtz v0, :cond_5

    const/high16 v0, -0x40c00000

    cmpg-float v0, v7, v0

    if-gez v0, :cond_14

    :cond_5
    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    mul-float/2addr v0, v7

    :goto_a
    iget-object v2, p0, LF/D;->r:[LF/F;

    array-length v2, v2

    if-ge v4, v2, :cond_13

    iget-object v2, p0, LF/D;->r:[LF/F;

    array-length v2, v2

    if-ne v2, v3, :cond_12

    const/16 v2, 0x1702

    invoke-interface {v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    iget-boolean v2, p0, LF/D;->w:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    iget v2, v2, LF/F;->e:F

    div-float/2addr v2, v9

    iget-object v7, p0, LF/D;->r:[LF/F;

    aget-object v7, v7, v4

    iget v7, v7, LF/F;->f:F

    div-float/2addr v7, v9

    invoke-interface {v6, v2, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v2, 0x43340000

    invoke-interface {v6, v2, v5, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    iget v2, v2, LF/F;->e:F

    neg-float v2, v2

    div-float/2addr v2, v9

    iget-object v7, p0, LF/D;->r:[LF/F;

    aget-object v7, v7, v4

    iget v7, v7, LF/F;->f:F

    neg-float v7, v7

    div-float/2addr v7, v9

    invoke-interface {v6, v2, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_6
    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    iget v2, v2, LF/F;->e:F

    iget-object v7, p0, LF/D;->r:[LF/F;

    aget-object v7, v7, v4

    iget v7, v7, LF/F;->f:F

    invoke-interface {v6, v2, v7, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/16 v2, 0x1700

    invoke-interface {v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :goto_b
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    invoke-direct {p0, p1, p2, v2, v0}, LF/D;->a(LD/a;LC/a;LF/F;F)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_7
    iget-object v0, p1, LD/a;->h:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    goto/16 :goto_1

    :cond_8
    iget v0, p0, LF/D;->k:I

    goto/16 :goto_2

    :cond_9
    const/high16 v0, -0x40800000

    move v2, v0

    goto/16 :goto_3

    :cond_a
    const/high16 v0, -0x40800000

    goto/16 :goto_4

    :cond_b
    iget-object v2, p0, LF/D;->u:LE/i;

    goto/16 :goto_5

    :cond_c
    move v0, v4

    goto/16 :goto_6

    :cond_d
    iget-object v0, p0, LF/D;->u:LE/i;

    goto/16 :goto_7

    :cond_e
    move v0, v4

    goto/16 :goto_8

    :cond_f
    const v0, -0x457ced91

    cmpg-float v0, v7, v0

    if-gez v0, :cond_10

    iget-object v0, p0, LF/D;->v:LE/i;

    :goto_c
    iput-object v0, p0, LF/D;->x:LE/i;

    const v0, -0x457ced91

    cmpg-float v0, v7, v0

    if-gez v0, :cond_11

    move v0, v3

    :goto_d
    iput-boolean v0, p0, LF/D;->w:Z

    goto/16 :goto_9

    :cond_10
    iget-object v0, p0, LF/D;->u:LE/i;

    goto :goto_c

    :cond_11
    move v0, v4

    goto :goto_d

    :cond_12
    iget-object v2, p0, LF/D;->x:LE/i;

    mul-int/lit8 v7, v4, 0x2

    invoke-virtual {v2, p1, v7}, LE/i;->a(LD/a;I)V

    goto :goto_b

    :cond_13
    iget-object v0, p0, LF/D;->r:[LF/F;

    array-length v0, v0

    if-ne v0, v3, :cond_0

    const/16 v0, 0x1702

    invoke-interface {v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/16 v0, 0x1700

    invoke-interface {v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_0

    :cond_14
    move v0, v5

    goto/16 :goto_a
.end method

.method a()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, LF/D;->I:I

    const/4 v3, 0x6

    if-ge v2, v3, :cond_0

    iget v2, p0, LF/D;->I:I

    if-le v2, v1, :cond_1

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-virtual {v2}, Lo/X;->d()F

    move-result v2

    iget v3, p0, LF/D;->p:F

    const/high16 v4, 0x40000000

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :cond_0
    :goto_1
    return v0

    :cond_1
    iget v2, p0, LF/D;->I:I

    const/4 v3, 0x3

    if-le v2, v3, :cond_2

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-virtual {v2}, Lo/X;->d()F

    move-result v2

    iget v3, p0, LF/D;->p:F

    const/high16 v4, 0x40400000

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    :cond_2
    invoke-direct {p0}, LF/D;->e()Lo/X;

    move-result-object v2

    iput-object v2, p0, LF/D;->o:Lo/X;

    iget-object v2, p0, LF/D;->o:Lo/X;

    if-eqz v2, :cond_3

    iget v0, p0, LF/D;->C:F

    invoke-direct {p0, v0}, LF/D;->b(F)V

    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-static {v0}, LF/D;->a(Lo/X;)I

    move-result v0

    iput v0, p0, LF/D;->D:I

    move v0, v1

    goto :goto_1

    :cond_3
    iget v2, p0, LF/D;->I:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LF/D;->I:I

    goto :goto_0
.end method

.method public a(LC/a;LD/a;)Z
    .locals 1

    iget v0, p0, LF/D;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LF/D;->I:I

    invoke-virtual {p0}, LF/D;->a()Z

    move-result v0

    return v0
.end method

.method a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->b:Lo/aj;

    move-object/from16 v0, p2

    invoke-static {v1, v0}, LF/D;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, LF/D;->b:Lo/aj;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->a:Lo/n;

    invoke-interface {v1}, Lo/n;->h()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    move-object/from16 v0, p2

    invoke-static {v2, v1, v0}, LF/D;->a(Lo/aj;ZLcom/google/android/maps/driveabout/vector/q;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->b:Lo/aj;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->b:Lo/aj;

    invoke-virtual {v1}, Lo/aj;->h()Lo/ao;

    move-result-object v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/D;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget v5, v0, LF/D;->B:F

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/D;->t:LD/b;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->t:LD/b;

    if-nez v1, :cond_3

    const/16 v1, 0x2710

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LD/a;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v8, v0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    move-object/from16 v0, p0

    iget-object v10, v0, LF/D;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget v13, v0, LF/D;->B:F

    const/16 v16, 0x0

    move-object/from16 v9, p1

    move-object v12, v4

    move v14, v6

    move v15, v7

    invoke-virtual/range {v8 .. v16}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/D;->t:LD/b;

    :cond_3
    invoke-direct/range {p0 .. p1}, LF/D;->d(LD/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->r:[LF/F;

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->r:[LF/F;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, v1, LF/F;->d:F

    const/4 v1, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, LF/D;->r:[LF/F;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, LF/D;->r:[LF/F;

    aget-object v3, v3, v1

    iget v3, v3, LF/F;->d:F

    sub-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41f00000

    cmpl-float v4, v3, v4

    if-lez v4, :cond_6

    const/high16 v4, 0x43a50000

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LF/D;->H:Z

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, LF/D;->h:Z

    if-eqz v1, :cond_5

    new-instance v1, Lh/e;

    const-wide/16 v2, 0x1f4

    sget-object v4, Lh/g;->a:Lh/g;

    invoke-direct {v1, v2, v3, v4}, Lh/e;-><init>(JLh/g;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LF/D;->z:Lh/e;

    :cond_5
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LF/D;->J:Z

    const/4 v1, 0x1

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public a(Lo/aS;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LF/D;->o:Lo/X;

    invoke-virtual {v1, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/D;->o:Lo/X;

    invoke-virtual {v1}, Lo/X;->c()Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method b()I
    .locals 3

    iget v0, p0, LF/D;->E:I

    iget v1, p0, LF/D;->D:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, LF/D;->G:I

    int-to-float v1, v1

    int-to-float v0, v0

    const v2, 0x3c8efa35

    mul-float/2addr v0, v2

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    invoke-super {p0, p1}, LF/m;->b(LD/a;)V

    iget-object v0, p0, LF/D;->u:LE/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/D;->u:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    iget-object v0, p0, LF/D;->v:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/D;->y:LE/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/D;->y:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    :cond_1
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, LC/a;->y()F

    move-result v0

    iget v3, p0, LF/D;->A:F

    div-float v3, v0, v3

    invoke-virtual {p0}, LF/D;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LF/D;->i:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3e800000

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x40000000

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3}, LF/D;->a(F)I

    move-result v3

    iput v3, p0, LF/D;->k:I

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LF/D;->E:I

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    iput v0, p0, LF/D;->F:F

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const v0, 0x3f666666

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_2

    const/high16 v0, 0x3fa00000

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_2

    move v0, v1

    :goto_3
    const/high16 v3, 0x10000

    iput v3, p0, LF/D;->k:I

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public m()F
    .locals 1

    iget v0, p0, LF/D;->L:F

    return v0
.end method

.method public n()Lo/ae;
    .locals 1

    iget-object v0, p0, LF/D;->q:Lo/W;

    return-object v0
.end method

.method public t()I
    .locals 4

    const/4 v1, 0x0

    iget v2, p0, LF/D;->f:I

    iget v0, p0, LF/D;->I:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    iget v2, p0, LF/D;->F:F

    const/high16 v3, 0x41f00000

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LF/D;->b()I

    move-result v1

    goto :goto_1
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LF/D;->K:Ljava/lang/String;

    return-object v0
.end method
