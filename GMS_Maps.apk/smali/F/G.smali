.class public LF/G;
.super LF/i;
.source "SourceFile"


# static fields
.field private static b:I

.field private static c:I

.field private static d:F

.field private static e:F


# instance fields
.field private final f:LE/o;

.field private final g:Lx/c;

.field private final h:I

.field private final i:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/high16 v1, 0x3f800000

    const/16 v0, 0x4000

    sput v0, LF/G;->b:I

    const/4 v0, 0x1

    sput v0, LF/G;->c:I

    sput v1, LF/G;->d:F

    sput v1, LF/G;->e:F

    return-void
.end method

.method private constructor <init>(IILjava/util/Set;LD/a;)V
    .locals 2

    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    new-instance v0, LE/r;

    invoke-direct {v0, p1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/G;->f:LE/o;

    new-instance v0, Lx/c;

    invoke-virtual {p4}, LD/a;->G()Lx/a;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/G;->g:Lx/c;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/G;->i:Lo/T;

    const/high16 v0, 0x10000

    mul-int/2addr v0, p2

    iput v0, p0, LF/G;->h:I

    return-void
.end method

.method private static a(Lo/M;)I
    .locals 1

    invoke-virtual {p0}, Lo/M;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    return v0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;
    .locals 11

    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v1

    instance-of v0, v1, Lo/M;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    if-le v2, v0, :cond_2

    move v0, v2

    :goto_1
    if-gez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    new-instance v1, LF/G;

    invoke-direct {v1, v3, v0, v5, p4}, LF/G;-><init>(IILjava/util/Set;LD/a;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    invoke-interface {v0}, Lo/n;->h()I

    move-result v3

    const/4 v5, 0x5

    if-ne v3, v5, :cond_c

    check-cast v0, Lo/M;

    invoke-direct {v1, p0, v4, v0}, LF/G;->a(Lo/aq;Lo/ad;Lo/M;)V

    goto :goto_2

    :cond_2
    const/4 v2, 0x1

    move-object v0, v1

    check-cast v0, Lo/M;

    invoke-static {v0}, LF/G;->a(Lo/M;)I

    move-result v0

    sget v7, LF/G;->b:I

    if-le v0, v7, :cond_3

    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_3
    add-int v7, v0, v3

    sget v8, LF/G;->b:I

    if-le v7, v8, :cond_4

    move v0, v2

    goto :goto_1

    :cond_4
    add-int/2addr v0, v3

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v10, v2

    move v2, v0

    move v0, v10

    :goto_3
    invoke-interface {v1}, Lo/n;->l()[I

    move-result-object v3

    array-length v7, v3

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v7, :cond_b

    aget v8, v3, v1

    if-ltz v8, :cond_5

    array-length v9, p1

    if-ge v8, v9, :cond_5

    aget-object v8, p1, v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    instance-of v0, v1, Lo/K;

    if-eqz v0, :cond_e

    move-object v0, v1

    check-cast v0, Lo/K;

    invoke-static {v0}, LF/G;->a(Lo/K;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Lo/n;->e()Lo/aj;

    move-result-object v0

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->c()F

    move-result v0

    invoke-static {v0}, LF/G;->b(F)I

    move-result v0

    if-eq v0, v2, :cond_8

    if-lez v2, :cond_7

    move v0, v2

    goto :goto_1

    :cond_7
    move v2, v0

    :cond_8
    move-object v0, v1

    check-cast v0, Lo/K;

    invoke-static {v0}, LF/G;->b(Lo/K;)I

    move-result v0

    sget v7, LF/G;->b:I

    if-le v0, v7, :cond_9

    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto/16 :goto_0

    :cond_9
    add-int v7, v0, v3

    sget v8, LF/G;->b:I

    if-le v7, v8, :cond_a

    move v0, v2

    goto/16 :goto_1

    :cond_a
    add-int/2addr v0, v3

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v10, v2

    move v2, v0

    move v0, v10

    goto :goto_3

    :cond_b
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    move v3, v2

    move v2, v0

    goto/16 :goto_0

    :cond_c
    invoke-interface {v0}, Lo/n;->h()I

    move-result v3

    const/16 v5, 0x8

    if-ne v3, v5, :cond_1

    check-cast v0, Lo/K;

    invoke-direct {v1, p0, v4, v0}, LF/G;->a(Lo/aq;Lo/ad;Lo/K;)V

    goto/16 :goto_2

    :cond_d
    invoke-virtual {v1, p0, p3}, LF/G;->a(Lo/aq;I)V

    return-object v1

    :cond_e
    move v0, v2

    goto/16 :goto_1
.end method

.method public static declared-synchronized a(F)V
    .locals 4

    const-class v1, LF/G;

    monitor-enter v1

    :try_start_0
    sput p0, LF/G;->e:F

    const/high16 v0, 0x3f800000

    const/4 v2, 0x5

    sget v3, LF/G;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, LF/G;->e:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, LF/G;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(I)V
    .locals 4

    const-class v1, LF/G;

    monitor-enter v1

    :try_start_0
    sput p0, LF/G;->c:I

    const/high16 v0, 0x3f800000

    const/4 v2, 0x5

    sget v3, LF/G;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, LF/G;->e:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, LF/G;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lo/aq;Lo/ad;Lo/K;)V
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p3}, Lo/K;->e()Lo/aj;

    move-result-object v2

    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lo/K;->b()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    mul-int/lit8 v5, v4, 0x2

    invoke-virtual {p2}, Lo/ad;->d()Lo/T;

    move-result-object v6

    invoke-virtual {p2}, Lo/ad;->g()I

    move-result v7

    move v0, v1

    :goto_1
    if-gt v0, v4, :cond_3

    iget-object v8, p0, LF/G;->i:Lo/T;

    invoke-virtual {v3, v0, v8}, Lo/X;->a(ILo/T;)V

    iget-object v8, p0, LF/G;->i:Lo/T;

    iget-object v9, p0, LF/G;->i:Lo/T;

    invoke-static {v8, v6, v9}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    iget-object v8, p0, LF/G;->f:LE/o;

    iget-object v9, p0, LF/G;->i:Lo/T;

    invoke-virtual {v8, v9, v7}, LE/o;->a(Lo/T;I)V

    if-lez v0, :cond_2

    if-ge v0, v4, :cond_2

    iget-object v8, p0, LF/G;->f:LE/o;

    iget-object v9, p0, LF/G;->i:Lo/T;

    invoke-virtual {v8, v9, v7}, LE/o;->a(Lo/T;I)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    invoke-virtual {v1}, Lo/ai;->b()I

    move-result v1

    invoke-virtual {v0, v1, v5}, Lx/c;->a(II)V

    goto :goto_0
.end method

.method private a(Lo/aq;Lo/ad;Lo/M;)V
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p3}, Lo/M;->e()Lo/aj;

    move-result-object v2

    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lo/M;->b()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v4

    invoke-virtual {p2}, Lo/ad;->d()Lo/T;

    move-result-object v5

    invoke-virtual {p2}, Lo/ad;->g()I

    move-result v6

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    iget-object v7, p0, LF/G;->i:Lo/T;

    invoke-virtual {v3, v0, v7}, Lo/X;->a(ILo/T;)V

    iget-object v7, p0, LF/G;->i:Lo/T;

    iget-object v8, p0, LF/G;->i:Lo/T;

    invoke-static {v7, v5, v8}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    iget-object v7, p0, LF/G;->f:LE/o;

    iget-object v8, p0, LF/G;->i:Lo/T;

    invoke-virtual {v7, v8, v6}, LE/o;->a(Lo/T;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    invoke-virtual {v1}, Lo/ai;->b()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lx/c;->a(II)V

    goto :goto_0
.end method

.method public static declared-synchronized a(Lo/K;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-class v3, LF/G;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Lo/K;->e()Lo/aj;

    move-result-object v4

    invoke-virtual {v4}, Lo/aj;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->d()[I

    move-result-object v2

    array-length v2, v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Lo/aj;->b()I

    move-result v5

    if-ne v5, v0, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lo/aj;->b(I)Lo/ai;

    move-result-object v4

    invoke-virtual {v4}, Lo/ai;->c()F

    move-result v4

    sget v5, LF/G;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_1

    if-nez v2, :cond_1

    :goto_1
    monitor-exit v3

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method static declared-synchronized b(F)I
    .locals 4

    const-class v1, LF/G;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    sget v2, LF/G;->c:I

    sget v3, LF/G;->e:F

    mul-float/2addr v3, p0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lo/K;)I
    .locals 1

    invoke-virtual {p0}, Lo/K;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/G;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 5

    const/high16 v4, 0x10000

    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, LF/G;->h:I

    if-le v1, v4, :cond_0

    iget v1, p0, LF/G;->h:I

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    :cond_0
    iget-object v1, p0, LF/G;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/G;->g:Lx/c;

    invoke-virtual {v1, p1}, Lx/c;->a(LD/a;)V

    invoke-static {p1}, Lx/a;->c(LD/a;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, LF/G;->f:LE/o;

    invoke-virtual {v3}, LE/o;->a()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    invoke-static {p1}, Lx/a;->d(LD/a;)V

    :cond_1
    return-void
.end method

.method public a(Lo/aq;I)V
    .locals 0

    return-void
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x78

    iget-object v1, p0, LF/G;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    return-void
.end method
