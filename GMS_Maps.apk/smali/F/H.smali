.class public LF/H;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/c;
.implements Ljava/lang/Comparable;


# static fields
.field private static E:[F


# instance fields
.field private final A:I

.field private B:Z

.field private C:Lo/aQ;

.field private D:LF/I;

.field private F:Lh/p;

.field private G:Lz/r;

.field private H:Lz/r;

.field protected final a:Landroid/graphics/Bitmap;

.field protected final b:I

.field protected final c:I

.field private d:Lo/T;

.field private final e:Landroid/graphics/Bitmap;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Lcom/google/android/maps/driveabout/vector/A;

.field private i:LD/b;

.field private j:LD/b;

.field private k:LE/i;

.field private l:LE/i;

.field private m:F

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private final t:Z

.field private final u:F

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, LF/H;->E:[F

    return-void
.end method

.method public constructor <init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, LF/H;->s:Z

    iput-object p1, p0, LF/H;->d:Lo/T;

    iput-object p2, p0, LF/H;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, LF/H;->e:Landroid/graphics/Bitmap;

    iput p4, p0, LF/H;->b:I

    iput p5, p0, LF/H;->c:I

    iput-object p6, p0, LF/H;->f:Ljava/lang/String;

    iput-object p7, p0, LF/H;->g:Ljava/lang/String;

    iput-boolean v0, p0, LF/H;->q:Z

    iput-boolean p8, p0, LF/H;->t:Z

    iget-boolean v0, p0, LF/H;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->y()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->s()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x43700000

    div-float/2addr v0, v1

    iput v0, p0, LF/H;->u:F

    :goto_0
    iget v0, p0, LF/H;->b:I

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->v:I

    iget v0, p0, LF/H;->c:I

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->w:I

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->x:I

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->y:I

    :goto_1
    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->z:I

    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->A:I

    :goto_2
    return-void

    :cond_0
    const/high16 v0, 0x3f800000

    iput v0, p0, LF/H;->u:F

    goto :goto_0

    :cond_1
    iput v2, p0, LF/H;->x:I

    iput v2, p0, LF/H;->y:I

    goto :goto_1

    :cond_2
    iput v2, p0, LF/H;->z:I

    iput v2, p0, LF/H;->A:I

    goto :goto_2
.end method

.method private a(LD/b;)LE/i;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    invoke-virtual {p1}, LD/b;->b()F

    move-result v1

    invoke-virtual {p1}, LD/b;->c()F

    move-result v2

    invoke-virtual {v0, v3, v3}, LE/i;->a(FF)V

    invoke-virtual {v0, v3, v2}, LE/i;->a(FF)V

    invoke-virtual {v0, v1, v3}, LE/i;->a(FF)V

    invoke-virtual {v0, v1, v2}, LE/i;->a(FF)V

    return-object v0
.end method

.method private a(LD/a;Lz/r;II)V
    .locals 4

    const/high16 v2, 0x10000

    iget-object v0, p0, LF/H;->F:Lh/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/H;->F:Lh/p;

    invoke-virtual {v0, p1}, Lh/p;->a(LD/a;)I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, LF/H;->F:Lh/p;

    :cond_0
    mul-int v1, p3, v0

    div-int/2addr v1, v2

    int-to-float v1, v1

    mul-int/2addr v0, p4

    div-int/2addr v0, v2

    int-to-float v0, v0

    :goto_0
    iget-object v2, p0, LF/H;->d:Lo/T;

    const/high16 v3, 0x3f800000

    invoke-virtual {p2, v2, v1, v0, v3}, Lz/r;->a(Lo/T;FFF)V

    return-void

    :cond_1
    int-to-float v1, p3

    int-to-float v0, p4

    goto :goto_0
.end method

.method private b(LD/a;Landroid/graphics/Bitmap;)LD/b;
    .locals 2

    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/A;->h()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LD/b;->f()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    invoke-virtual {v0, p2}, LD/b;->b(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/A;->h()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(FFLo/T;LC/a;)I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LF/H;->d:Lo/T;

    invoke-virtual {p4, v0}, LC/a;->b(Lo/T;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v1, v0, v1

    iget v2, p0, LF/H;->v:I

    sub-int/2addr v1, v2

    iget v2, p0, LF/H;->x:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v2, 0x1

    aget v0, v0, v2

    iget v2, p0, LF/H;->w:I

    sub-int/2addr v0, v2

    iget v2, p0, LF/H;->y:I

    div-int/lit8 v2, v2, 0x2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v0, v2

    int-to-float v2, v1

    sub-float v2, p1, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    mul-float/2addr v1, v2

    int-to-float v2, v0

    sub-float v2, p2, v2

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(I)I
    .locals 2

    iget-boolean v0, p0, LF/H;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->y()Z

    move-result v0

    if-nez v0, :cond_0

    int-to-float v0, p1

    iget v1, p0, LF/H;->u:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p1

    :cond_0
    return p1
.end method

.method public a(LF/H;)I
    .locals 2

    iget v0, p0, LF/H;->n:I

    iget v1, p1, LF/H;->n:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LF/H;->n:I

    iget v1, p1, LF/H;->n:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(LD/a;Landroid/graphics/Bitmap;)LD/b;
    .locals 2

    new-instance v0, LD/b;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, LD/b;-><init>(LD/a;Z)V

    invoke-virtual {v0, p2}, LD/b;->b(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public a(LD/a;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/high16 v6, 0x3f800000

    const/4 v5, 0x0

    iget-object v0, p0, LF/H;->G:Lz/r;

    if-nez v0, :cond_1

    iget v0, p0, LF/H;->v:I

    int-to-float v0, v0

    iget v1, p0, LF/H;->x:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, LF/H;->w:I

    int-to-float v1, v1

    iget v2, p0, LF/H;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/16 v2, 0x14

    new-array v2, v2, [F

    const/4 v3, 0x0

    neg-float v4, v0

    aput v4, v2, v3

    aput v1, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v9

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    new-instance v0, Lz/r;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lz/r;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    iput-object v0, p0, LF/H;->G:Lz/r;

    iget-object v0, p0, LF/H;->G:Lz/r;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Marker for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, LF/H;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lz/r;->a(Ljava/lang/String;)V

    iget-object v0, p0, LF/H;->i:LD/b;

    if-nez v0, :cond_0

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, LF/H;->a(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->i:LD/b;

    :cond_0
    iget-object v0, p0, LF/H;->G:Lz/r;

    iget-object v1, p0, LF/H;->i:LD/b;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    iget-object v0, p0, LF/H;->G:Lz/r;

    new-instance v1, Lz/d;

    const/16 v3, 0x303

    invoke-direct {v1, v7, v3}, Lz/d;-><init>(II)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    new-array v0, v9, [F

    fill-array-data v0, :array_0

    new-instance v1, Lz/E;

    invoke-direct {v1, v8, v0}, Lz/E;-><init>(I[F)V

    iget-object v0, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    iget-object v0, p0, LF/H;->G:Lz/r;

    new-instance v1, Lz/v;

    const/16 v3, 0x9

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lz/v;-><init>([FII)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/M;)V

    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->a(Lz/i;)V

    :cond_1
    iget-object v0, p0, LF/H;->H:Lz/r;

    if-nez v0, :cond_3

    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget v0, p0, LF/H;->v:I

    int-to-float v0, v0

    iget v1, p0, LF/H;->z:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, LF/H;->w:I

    int-to-float v1, v1

    iget v2, p0, LF/H;->A:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/16 v2, 0x14

    new-array v2, v2, [F

    const/4 v3, 0x0

    neg-float v4, v0

    aput v4, v2, v3

    aput v1, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v9

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    new-instance v0, Lz/r;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->v:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lz/r;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    iput-object v0, p0, LF/H;->H:Lz/r;

    iget-object v0, p0, LF/H;->H:Lz/r;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Marker shadow for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, LF/H;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lz/r;->a(Ljava/lang/String;)V

    iget-object v0, p0, LF/H;->j:LD/b;

    if-nez v0, :cond_2

    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, LF/H;->a(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->j:LD/b;

    :cond_2
    iget-object v0, p0, LF/H;->H:Lz/r;

    iget-object v1, p0, LF/H;->j:LD/b;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    iget-object v0, p0, LF/H;->H:Lz/r;

    new-instance v1, Lz/d;

    const/16 v3, 0x303

    invoke-direct {v1, v7, v3}, Lz/d;-><init>(II)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    new-array v0, v9, [F

    fill-array-data v0, :array_1

    new-instance v1, Lz/E;

    invoke-direct {v1, v8, v0}, Lz/E;-><init>(I[F)V

    iget-object v0, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    iget-object v0, p0, LF/H;->H:Lz/r;

    new-instance v1, Lz/v;

    const/16 v3, 0x9

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lz/v;-><init>([FII)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/M;)V

    iget-object v0, p0, LF/H;->H:Lz/r;

    const/high16 v1, -0x3dcc0000

    invoke-virtual {v0, v1}, Lz/r;->a(F)V

    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->a(Lz/i;)V

    :cond_3
    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 9

    const/16 v6, 0x303

    const/4 v5, 0x2

    const/4 v8, 0x0

    const/high16 v7, 0x10000

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gt v0, v5, :cond_0

    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_0

    iget v0, p0, LF/H;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LF/H;->s:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    iget-object v2, p0, LF/H;->e:Landroid/graphics/Bitmap;

    iget-object v3, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LF/H;->j:LD/b;

    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, v2}, LF/H;->b(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->j:LD/b;

    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-direct {p0, v0}, LF/H;->a(LD/b;)LE/i;

    move-result-object v0

    iput-object v0, p0, LF/H;->l:LE/i;

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LF/H;->d:Lo/T;

    iget v1, p0, LF/H;->m:F

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_5

    const/high16 v0, -0x2d0000

    invoke-interface {v4, v0, v8, v8, v7}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    const/high16 v0, -0x5a0000

    invoke-interface {v4, v0, v7, v8, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    iget-object v0, p0, LF/H;->l:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-virtual {v0, v4}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget v1, p0, LF/H;->z:I

    iget v0, p0, LF/H;->A:I

    :goto_2
    iget-object v3, p0, LF/H;->F:Lh/p;

    if-eqz v3, :cond_7

    iget-object v3, p0, LF/H;->F:Lh/p;

    invoke-virtual {v3, p1}, Lh/p;->a(LD/a;)I

    move-result v3

    if-ne v3, v7, :cond_3

    const/4 v5, 0x0

    iput-object v5, p0, LF/H;->F:Lh/p;

    :cond_3
    mul-int/2addr v1, v3

    mul-int/2addr v0, v3

    :goto_3
    iget v3, p0, LF/H;->b:I

    shl-int/lit8 v3, v3, 0x10

    neg-int v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v3, v5

    const/high16 v5, -0x10000

    iget v6, p0, LF/H;->c:I

    shl-int/lit8 v6, v6, 0x10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int v2, v6, v2

    add-int/2addr v2, v5

    invoke-interface {v4, v1, v7, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    invoke-interface {v4, v3, v8, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatex(III)V

    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v4, v0, v8, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, LF/H;->i:LD/b;

    if-nez v0, :cond_2

    invoke-direct {p0, p1, v3}, LF/H;->b(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->i:LD/b;

    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-direct {p0, v0}, LF/H;->a(LD/b;)LE/i;

    move-result-object v0

    iput-object v0, p0, LF/H;->k:LE/i;

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    invoke-static {v4, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    iget-object v0, p0, LF/H;->k:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-virtual {v0, v4}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget v1, p0, LF/H;->x:I

    iget v0, p0, LF/H;->y:I

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v2

    if-ne v2, v5, :cond_6

    const/16 v2, 0x302

    invoke-interface {v4, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v5, 0x2200

    const/16 v6, 0x2100

    invoke-interface {v4, v2, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const v2, 0xcccc

    invoke-interface {v4, v7, v7, v7, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object v2, v3

    goto :goto_2

    :cond_6
    const/4 v2, 0x1

    invoke-interface {v4, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    move-object v2, v3

    goto :goto_2

    :cond_7
    shl-int/lit8 v1, v1, 0x10

    shl-int/lit8 v0, v0, 0x10

    goto :goto_3
.end method

.method public a(LF/I;)V
    .locals 0

    iput-object p1, p0, LF/H;->D:LF/I;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/A;)V
    .locals 0

    iput-object p1, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    return-void
.end method

.method public declared-synchronized a(Lo/T;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iput-object p1, p0, LF/H;->d:Lo/T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LF/H;->q:Z

    return-void
.end method

.method public declared-synchronized a(LC/a;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LF/H;->a:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    iget-object v3, p0, LF/H;->C:Lo/aQ;

    invoke-virtual {v2, v3}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v0, p0, LF/H;->B:Z

    goto :goto_0

    :cond_1
    iget-object v2, p0, LF/H;->d:Lo/T;

    invoke-virtual {p1, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, LF/H;->v:I

    sub-int v4, v3, v4

    iget v3, p0, LF/H;->x:I

    add-int/2addr v3, v4

    const/4 v5, 0x1

    aget v2, v2, v5

    iget v5, p0, LF/H;->w:I

    sub-int v5, v2, v5

    iget v2, p0, LF/H;->y:I

    add-int/2addr v2, v5

    iget-object v6, p0, LF/H;->e:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_2

    iget v6, p0, LF/H;->z:I

    add-int/2addr v6, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v6, p0, LF/H;->A:I

    add-int/2addr v6, v5

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_2
    invoke-virtual {p1}, LC/a;->k()I

    move-result v6

    if-ge v4, v6, :cond_3

    if-ltz v3, :cond_3

    invoke-virtual {p1}, LC/a;->l()I

    move-result v3

    if-ge v5, v3, :cond_3

    if-ltz v2, :cond_3

    move v0, v1

    :cond_3
    iput-boolean v0, p0, LF/H;->B:Z

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v0

    iput-object v0, p0, LF/H;->C:Lo/aQ;

    iget-boolean v0, p0, LF/H;->B:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, LF/H;->G:Lz/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, p1}, Lz/r;->a(I)V

    :cond_0
    iget-object v0, p0, LF/H;->H:Lz/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, p1}, Lz/r;->a(I)V

    :cond_1
    return-void
.end method

.method public b(LD/a;)V
    .locals 3

    iget-object v0, p0, LF/H;->G:Lz/r;

    iget v1, p0, LF/H;->x:I

    iget v2, p0, LF/H;->y:I

    invoke-direct {p0, p1, v0, v1, v2}, LF/H;->a(LD/a;Lz/r;II)V

    iget-object v0, p0, LF/H;->H:Lz/r;

    iget v1, p0, LF/H;->z:I

    iget v2, p0, LF/H;->A:I

    invoke-direct {p0, p1, v0, v1, v2}, LF/H;->a(LD/a;Lz/r;II)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, LF/H;->r:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, LF/H;->r:Z

    return v0
.end method

.method public declared-synchronized b(FFLo/T;LC/a;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LF/H;->a:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    :cond_1
    :try_start_1
    iget-object v2, p0, LF/H;->d:Lo/T;

    invoke-virtual {p4, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, LF/H;->v:I

    sub-int/2addr v3, v4

    iget v4, p0, LF/H;->x:I

    add-int/2addr v4, v3

    const/high16 v5, 0x41400000

    invoke-virtual {p4}, LC/a;->m()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_0

    add-int v3, v4, v5

    int-to-float v3, v3

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_0

    const/4 v3, 0x1

    aget v2, v2, v3

    iget v3, p0, LF/H;->w:I

    sub-int/2addr v2, v3

    iget v3, p0, LF/H;->y:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/2addr v3, v2

    sub-int/2addr v2, v5

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_2

    add-int v2, v3, v5

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(LC/a;)Z
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LF/H;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    const/high16 v0, 0x3f800000

    :try_start_1
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    invoke-virtual {p1, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, LF/H;->m:F

    iget-object v0, p0, LF/H;->d:Lo/T;

    sget-object v1, LF/H;->E:[F

    invoke-virtual {p1, v0, v1}, LC/a;->a(Lo/T;[F)V

    sget-object v0, LF/H;->E:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    const/high16 v1, 0x47800000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LF/H;->n:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(LD/a;)V
    .locals 2

    iget-object v0, p0, LF/H;->G:Lz/r;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->b(Lz/i;)V

    :cond_0
    iget-object v0, p0, LF/H;->H:Lz/r;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->b(Lz/i;)V

    :cond_1
    return-void
.end method

.method public c(Z)V
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, LF/H;->F:Lh/p;

    if-nez v0, :cond_1

    new-instance v0, Lh/p;

    invoke-direct {v0}, Lh/p;-><init>()V

    iput-object v0, p0, LF/H;->F:Lh/p;

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, LF/H;->F:Lh/p;

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LF/H;

    invoke-virtual {p0, p1}, LF/H;->a(LF/H;)I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, LF/H;->o:Z

    iget-boolean v0, p0, LF/H;->p:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LF/H;->p:Z

    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/vector/A;->c(LF/H;)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, LF/H;->p:Z

    return-void
.end method

.method public declared-synchronized e()Lo/T;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LF/H;->d:Lo/T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Lo/D;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public g_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LF/H;->o:Z

    return-void
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h_()I
    .locals 1

    iget v0, p0, LF/H;->w:I

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LF/H;->f:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LF/H;->g:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, LF/H;->q:Z

    return v0
.end method

.method public l()LF/I;
    .locals 1

    iget-object v0, p0, LF/H;->D:LF/I;

    return-object v0
.end method

.method public m()Lcom/google/android/maps/driveabout/vector/A;
    .locals 1

    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    return-object v0
.end method

.method public n()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public o()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public p()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public q()Z
    .locals 1

    iget-boolean v0, p0, LF/H;->o:Z

    return v0
.end method

.method public r()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LF/H;->i:LD/b;

    if-eqz v1, :cond_0

    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-virtual {v0}, LD/b;->h()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, LF/H;->i:LD/b;

    :cond_0
    return v0
.end method

.method public s()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LF/H;->j:LD/b;

    if-eqz v1, :cond_0

    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-virtual {v0}, LD/b;->h()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, LF/H;->j:LD/b;

    :cond_0
    return v0
.end method
