.class public LF/L;
.super LF/m;
.source "SourceFile"


# static fields
.field private static final K:[LF/O;

.field private static final L:[LF/O;


# instance fields
.field private A:F

.field private B:Z

.field private final C:[LF/O;

.field private D:I

.field private E:Lh/e;

.field private F:Z

.field private G:F

.field private final H:F

.field private final I:[F

.field private J:Ljava/lang/String;

.field protected l:Lo/a;

.field protected m:LF/o;

.field protected n:LF/o;

.field protected o:LF/O;

.field protected p:F

.field protected q:F

.field protected r:F

.field protected s:F

.field private t:Lo/a;

.field private u:Lo/t;

.field private v:Lo/t;

.field private w:LE/o;

.field private final x:Ljava/lang/String;

.field private final y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-array v0, v4, [LF/O;

    sput-object v0, LF/L;->K:[LF/O;

    const/4 v0, 0x4

    new-array v0, v0, [LF/O;

    new-instance v1, LF/O;

    sget-object v2, LF/N;->d:LF/N;

    sget-object v3, LF/s;->a:LF/s;

    invoke-direct {v1, v2, v3}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, LF/O;

    sget-object v3, LF/N;->b:LF/N;

    sget-object v4, LF/s;->a:LF/s;

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LF/O;

    sget-object v3, LF/N;->e:LF/N;

    sget-object v4, LF/s;->c:LF/s;

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, LF/O;

    sget-object v3, LF/N;->c:LF/N;

    sget-object v4, LF/s;->b:LF/s;

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v0, v1

    sput-object v0, LF/L;->L:[LF/O;

    return-void
.end method

.method constructor <init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V
    .locals 10

    invoke-interface {p1}, Lo/n;->e()Lo/aj;

    move-result-object v4

    invoke-interface {p1}, Lo/n;->i()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v8, p8

    move/from16 v9, p13

    invoke-direct/range {v1 .. v9}, LF/m;-><init>(Lo/n;Ly/b;Lo/aj;FFIZZ)V

    const/high16 v1, -0x40800000

    iput v1, p0, LF/L;->A:F

    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, LF/L;->I:[F

    iput-object p3, p0, LF/L;->x:Ljava/lang/String;

    iput-object p4, p0, LF/L;->l:Lo/a;

    iput-object p5, p0, LF/L;->t:Lo/a;

    move-object/from16 v0, p10

    iput-object v0, p0, LF/L;->m:LF/o;

    move-object/from16 v0, p11

    iput-object v0, p0, LF/L;->n:LF/o;

    move/from16 v0, p9

    iput-boolean v0, p0, LF/L;->y:Z

    iget-object v1, p0, LF/L;->n:LF/o;

    if-nez v1, :cond_0

    sget-object p12, LF/L;->K:[LF/O;

    :cond_0
    move-object/from16 v0, p12

    iput-object v0, p0, LF/L;->C:[LF/O;

    const/4 v1, 0x0

    iput v1, p0, LF/L;->D:I

    iget-object v1, p0, LF/L;->n:LF/o;

    if-eqz v1, :cond_1

    iget-object v1, p0, LF/L;->C:[LF/O;

    iget v2, p0, LF/L;->D:I

    aget-object v1, v1, v2

    iput-object v1, p0, LF/L;->o:LF/O;

    iget-object v1, p0, LF/L;->n:LF/o;

    iget-object v2, p0, LF/L;->o:LF/O;

    iget-object v2, v2, LF/O;->b:LF/s;

    invoke-virtual {v1, v2}, LF/o;->a(LF/s;)V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, LF/L;->B:Z

    const/4 v1, 0x0

    if-eqz p10, :cond_2

    invoke-virtual/range {p10 .. p10}, LF/o;->a()F

    move-result v2

    invoke-virtual/range {p10 .. p10}, LF/o;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    :cond_2
    if-eqz p11, :cond_3

    invoke-virtual/range {p11 .. p11}, LF/o;->a()F

    move-result v2

    invoke-virtual/range {p11 .. p11}, LF/o;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    :cond_3
    iput v1, p0, LF/L;->H:F

    return-void
.end method

.method private a(LC/a;)F
    .locals 3

    const/high16 v2, 0x3f800000

    const/4 v0, 0x1

    iget-boolean v1, p0, LF/L;->y:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/L;->l:Lo/a;

    invoke-virtual {v1}, Lo/a;->b()Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, LC/a;->a(Lo/T;Z)F

    move-result v0

    invoke-virtual {p1, v2, v0}, LC/a;->a(FF)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    invoke-virtual {p1, v2, v0}, LC/a;->a(FF)F

    move-result v0

    goto :goto_0
.end method

.method static a(Lo/aj;LG/a;F)I
    .locals 3

    iget v0, p1, LG/a;->e:F

    iget v1, p1, LG/a;->f:I

    iget v2, p1, LG/a;->g:I

    invoke-static {p0, v0, v1, v2, p2}, LF/m;->a(Lo/aj;FIIF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(LF/T;Lcom/google/android/maps/driveabout/vector/aV;LC/a;)LF/L;
    .locals 20

    invoke-interface/range {p0 .. p0}, LF/T;->c()LA/c;

    move-result-object v7

    invoke-virtual {v7}, LA/c;->i()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface/range {p0 .. p0}, LF/T;->b()Lo/aq;

    move-result-object v8

    invoke-virtual {v8}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-virtual {v7, v1}, LA/c;->a(Lo/ad;)Lo/T;

    move-result-object v9

    invoke-virtual {v7}, LA/c;->j()LF/O;

    move-result-object v10

    invoke-virtual {v7}, LA/c;->k()Lo/aj;

    move-result-object v5

    const/4 v1, 0x1

    new-array v14, v1, [LF/O;

    const/4 v1, 0x0

    aput-object v10, v14, v1

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v15, LG/a;->t:LG/a;

    new-instance v13, LF/r;

    const/high16 v1, 0x40800000

    invoke-direct {v13, v1}, LF/r;-><init>(F)V

    invoke-virtual/range {p2 .. p2}, LC/a;->m()F

    move-result v1

    invoke-static {v5, v15, v1}, LF/L;->a(Lo/aj;LG/a;F)I

    move-result v4

    new-instance v1, LF/t;

    invoke-virtual {v8}, Lo/aq;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, LF/t;

    invoke-virtual {v8}, Lo/aq;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, LF/t;

    invoke-virtual {v8}, Lo/aq;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, LF/t;

    invoke-virtual {v7}, LA/c;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p0}, LF/T;->d()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, LF/t;

    invoke-interface/range {p0 .. p0}, LF/T;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v13, LF/o;

    iget-object v1, v10, LF/O;->b:LF/s;

    sget-object v2, LF/v;->c:LF/v;

    invoke-direct {v13, v11, v1, v2}, LF/o;-><init>(Ljava/util/ArrayList;LF/s;LF/v;)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v16, LF/L;

    new-instance v17, Lo/l;

    const/4 v1, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Lo/l;-><init>(Lo/aj;)V

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v1, Lo/a;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v1 .. v8}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    const/4 v7, 0x0

    const/high16 v8, -0x40800000

    const/high16 v9, -0x40800000

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-boolean v15, v15, LG/a;->q:Z

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    move-object v6, v1

    invoke-direct/range {v2 .. v15}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    const/4 v1, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, LF/L;->b(Z)V

    move-object/from16 v1, v16

    goto/16 :goto_0
.end method

.method public static a(Lo/U;Ly/b;ZLC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;LZ/a;)LF/L;
    .locals 15

    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v1

    move-object v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, LF/o;->a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11}, LF/o;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    const/4 v12, 0x0

    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v1

    move-object v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, LF/o;->a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;

    move-result-object v12

    if-nez v12, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v12}, LF/o;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v12, 0x0

    :cond_4
    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_8

    if-eqz v12, :cond_8

    sget-object v13, LF/L;->L:[LF/O;

    :cond_5
    if-eqz v12, :cond_6

    array-length v1, v13

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    :cond_6
    const/4 v10, 0x1

    :goto_1
    invoke-virtual {p0}, Lo/U;->o()[Lo/a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v5, v1, v2

    invoke-virtual {v5}, Lo/a;->b()Lo/T;

    move-result-object v1

    invoke-static {p0, v1}, LF/L;->a(Lo/U;Lo/T;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lo/U;->t()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    move-object/from16 v0, p6

    iget-boolean v1, v0, LG/a;->p:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_b

    new-instance v1, LF/l;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lo/U;->k()F

    move-result v7

    invoke-virtual {p0}, Lo/U;->n()F

    move-result v8

    move-object/from16 v0, p6

    iget-boolean v14, v0, LG/a;->q:Z

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v9, p2

    invoke-direct/range {v1 .. v14}, LF/l;-><init>(Lo/U;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    :goto_3
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v2

    invoke-virtual {v2}, Lo/H;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v12, :cond_d

    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v2

    invoke-virtual {v2}, Lo/H;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-direct {v1, v2}, LF/L;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v1

    array-length v1, v1

    new-array v13, v1, [LF/O;

    const/4 v1, 0x0

    :goto_5
    array-length v2, v13

    if-ge v1, v2, :cond_5

    new-instance v2, LF/O;

    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lo/c;->d()I

    move-result v3

    invoke-static {v3}, LF/N;->a(I)LF/N;

    move-result-object v3

    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lo/c;->a()I

    move-result v4

    invoke-static {v4}, LF/s;->a(I)LF/s;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v13, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_b
    new-instance v1, LF/L;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lo/U;->k()F

    move-result v7

    invoke-virtual {p0}, Lo/U;->n()F

    move-result v8

    move-object/from16 v0, p6

    iget-boolean v14, v0, LG/a;->q:Z

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v9, p2

    invoke-direct/range {v1 .. v14}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    goto/16 :goto_3

    :cond_c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    :cond_d
    move-object v2, v3

    goto :goto_4
.end method

.method public static a(Lo/af;ILy/b;Lo/T;Lo/T;ZLG/a;LC/a;LD/c;)LF/L;
    .locals 17

    invoke-virtual/range {p0 .. p1}, Lo/af;->c(I)Lo/H;

    move-result-object v1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1}, Lo/H;->b()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {v1, v2}, Lo/H;->a(I)Lo/I;

    move-result-object v3

    invoke-virtual {v3}, Lo/I;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_0
    invoke-virtual {v3}, Lo/I;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lo/I;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, LF/o;->a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;

    move-result-object v12

    if-nez v12, :cond_4

    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    new-instance v16, LF/L;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v1, Lo/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v8}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    if-nez p4, :cond_5

    const/4 v7, 0x0

    :goto_2
    const/high16 v8, -0x40800000

    const/high16 v9, -0x40800000

    const/4 v11, 0x0

    const/4 v13, 0x0

    sget-object v14, LF/L;->K:[LF/O;

    move-object/from16 v0, p6

    iget-boolean v15, v0, LG/a;->q:Z

    move-object/from16 v2, v16

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object v5, v10

    move-object v6, v1

    move/from16 v10, p5

    invoke-direct/range {v2 .. v15}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    move-object/from16 v1, v16

    goto :goto_1

    :cond_5
    new-instance v2, Lo/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p4

    invoke-direct/range {v2 .. v9}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    move-object v7, v2

    goto :goto_2
.end method

.method private static a(Lo/U;Lo/T;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v3

    invoke-virtual {v3}, Lo/H;->b()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v3

    invoke-virtual {v3, v0}, Lo/H;->a(I)Lo/I;

    move-result-object v3

    invoke-virtual {v3}, Lo/I;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lo/I;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v0

    invoke-virtual {v0}, Lo/H;->b()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v0

    invoke-virtual {v0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v0

    invoke-virtual {v0}, Lo/I;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lo/I;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lo/U;->a()Lo/o;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lo/U;->a()Lo/o;

    move-result-object v0

    invoke-virtual {v0}, Lo/o;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {p0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    invoke-virtual {p0}, Lo/U;->b()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0xd

    if-le v0, v1, :cond_5

    invoke-virtual {p1}, Lo/T;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LF/L;->J:Ljava/lang/String;

    return-void
.end method

.method private b(LC/a;)Lo/t;
    .locals 8

    const/high16 v4, 0x42b40000

    const/high16 v3, 0x43870000

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->c()F

    move-result v0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_1

    add-float/2addr v0, v4

    move v2, v0

    :goto_1
    cmpg-float v0, v2, v3

    if-gez v0, :cond_2

    add-float v0, v2, v4

    move v1, v0

    :goto_2
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, LC/a;->a(Lo/T;Z)F

    move-result v3

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v4, v0, Lx/l;->a:Lo/T;

    iget v5, p0, LF/L;->p:F

    invoke-virtual {p1, v5, v3}, LC/a;->a(FF)F

    move-result v5

    invoke-virtual {v4, v2, v5}, Lo/T;->b(FF)V

    iget-object v5, v0, Lx/l;->b:Lo/T;

    iget v6, p0, LF/L;->q:F

    invoke-virtual {p1, v6, v3}, LC/a;->a(FF)F

    move-result v6

    invoke-virtual {v5, v2, v6}, Lo/T;->b(FF)V

    iget-object v2, v0, Lx/l;->c:Lo/T;

    iget v6, p0, LF/L;->r:F

    invoke-virtual {p1, v6, v3}, LC/a;->a(FF)F

    move-result v6

    invoke-virtual {v2, v1, v6}, Lo/T;->b(FF)V

    iget-object v6, v0, Lx/l;->d:Lo/T;

    iget v7, p0, LF/L;->s:F

    invoke-virtual {p1, v7, v3}, LC/a;->a(FF)F

    move-result v3

    invoke-virtual {v6, v1, v3}, Lo/T;->b(FF)V

    iget-object v1, v0, Lx/l;->e:Lo/T;

    iget-object v3, p0, LF/L;->l:Lo/a;

    invoke-virtual {v3}, Lo/a;->b()Lo/T;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    iget-object v0, v0, Lx/l;->f:Lo/T;

    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->b()Lo/T;

    move-result-object v2

    invoke-static {v2, v6, v0}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    invoke-virtual {v1, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v2

    invoke-virtual {v1, v5}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    invoke-virtual {v0, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {v0, v5}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v3, v0, v2, v1}, Lo/t;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/t;

    move-result-object v0

    goto :goto_0

    :cond_1
    sub-float/2addr v0, v3

    move v2, v0

    goto :goto_1

    :cond_2
    sub-float v0, v2, v3

    move v1, v0

    goto :goto_2
.end method

.method private b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 9

    const/high16 v4, 0x43b40000

    const/high16 v8, 0x40000000

    const/4 v1, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p2}, LC/a;->j()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->c()F

    move-result v2

    invoke-virtual {p2}, LC/a;->p()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v1

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p2}, LC/a;->q()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_1

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    iget-object v2, p0, LF/L;->I:[F

    invoke-virtual {p2, v0, v2}, LC/a;->a(Lo/T;[F)V

    iget-object v0, p0, LF/L;->I:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LF/L;->I:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    :cond_1
    iget v2, p0, LF/L;->A:F

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    iget-object v0, p0, LF/L;->E:Lh/e;

    if-eqz v0, :cond_8

    iget-object v0, p0, LF/L;->E:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, LF/L;->E:Lh/e;

    :cond_2
    :goto_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->c()F

    move-result v0

    invoke-virtual {p2}, LC/a;->p()F

    move-result v2

    sub-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_3

    add-float/2addr v0, v4

    :cond_3
    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->c()F

    move-result v2

    iget-boolean v3, p0, LF/L;->z:Z

    if-nez v3, :cond_b

    const/high16 v3, 0x42b40000

    cmpl-float v3, v0, v3

    if-lez v3, :cond_b

    const/high16 v3, 0x43870000

    cmpg-float v0, v0, v3

    if-gez v0, :cond_b

    const/high16 v0, 0x43340000

    add-float/2addr v0, v2

    :goto_1
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_4

    sub-float/2addr v0, v4

    :cond_4
    const/high16 v2, -0x40800000

    invoke-interface {v6, v0, v1, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v0, -0x3d4c0000

    const/high16 v2, 0x3f800000

    invoke-interface {v6, v0, v2, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :goto_2
    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_a

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v4

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v3

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    div-float v2, v0, v8

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    div-float/2addr v0, v8

    neg-float v5, v2

    neg-float v7, v0

    invoke-interface {v6, v5, v1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v5, p0, LF/L;->m:LF/o;

    invoke-virtual {v5, p1, p2, p3}, LF/o;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :goto_3
    iget-object v5, p0, LF/L;->n:LF/o;

    if-eqz v5, :cond_7

    sget-object v5, LF/M;->a:[I

    iget-object v7, p0, LF/L;->o:LF/O;

    iget-object v7, v7, LF/O;->a:LF/N;

    invoke-virtual {v7}, LF/N;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_0

    move v3, v1

    move v0, v1

    :goto_4
    iget-object v5, p0, LF/L;->o:LF/O;

    iget-object v5, v5, LF/O;->a:LF/N;

    sget-object v7, LF/N;->d:LF/N;

    if-eq v5, v7, :cond_5

    iget-object v5, p0, LF/L;->o:LF/O;

    iget-object v5, v5, LF/O;->a:LF/N;

    sget-object v7, LF/N;->b:LF/N;

    if-ne v5, v7, :cond_6

    :cond_5
    sget-object v5, LF/M;->b:[I

    iget-object v7, p0, LF/L;->o:LF/O;

    iget-object v7, v7, LF/O;->b:LF/s;

    invoke-virtual {v7}, LF/s;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_1

    :cond_6
    :goto_5
    invoke-interface {v6, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0, p1, p2, p3}, LF/o;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_7
    return-void

    :cond_8
    iget v0, p0, LF/L;->k:I

    goto/16 :goto_0

    :cond_9
    invoke-static {v6, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    goto :goto_2

    :pswitch_0
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->a()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v5, v2, v3

    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v5

    goto :goto_4

    :pswitch_1
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v4

    goto :goto_4

    :pswitch_2
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->a()F

    move-result v3

    neg-float v5, v3

    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v5

    goto :goto_4

    :pswitch_3
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v1

    goto :goto_4

    :pswitch_4
    move v0, v1

    goto :goto_4

    :pswitch_5
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v4

    goto :goto_4

    :pswitch_6
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    neg-float v0, v0

    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    neg-float v3, v3

    goto/16 :goto_4

    :pswitch_7
    move v0, v4

    goto/16 :goto_4

    :pswitch_8
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    neg-float v0, v0

    goto/16 :goto_4

    :pswitch_9
    const/high16 v0, -0x3ee00000

    goto :goto_5

    :pswitch_a
    const/high16 v0, 0x41200000

    add-float/2addr v0, v4

    iget-object v2, p0, LF/L;->n:LF/o;

    invoke-virtual {v2}, LF/o;->a()F

    move-result v2

    sub-float/2addr v0, v2

    goto :goto_5

    :pswitch_b
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    div-float/2addr v0, v8

    sub-float v0, v2, v0

    goto/16 :goto_5

    :cond_a
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_3

    :cond_b
    move v0, v2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private b(Z)V
    .locals 0

    iput-boolean p1, p0, LF/L;->z:Z

    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 1

    invoke-super {p0, p1}, LF/m;->a(LD/a;)V

    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0, p1}, LF/o;->a(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/L;->n:LF/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0, p1}, LF/o;->a(LD/a;)V

    :cond_1
    iget-object v0, p0, LF/L;->w:LE/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/L;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    :cond_2
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    iget-boolean v0, p0, LF/L;->B:Z

    if-nez v0, :cond_0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LF/L;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-direct {p0, p1, p2, p3}, LF/L;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(LC/a;LD/a;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, LF/L;->D:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, LF/L;->C:[LF/O;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v1, p0, LF/L;->C:[LF/O;

    iget v2, p0, LF/L;->D:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LF/L;->D:I

    aget-object v1, v1, v2

    iput-object v1, p0, LF/L;->o:LF/O;

    iget-object v1, p0, LF/L;->n:LF/o;

    iget-object v2, p0, LF/L;->o:LF/O;

    iget-object v2, v2, LF/O;->b:LF/s;

    invoke-virtual {v1, v2}, LF/o;->a(LF/s;)V

    iput-object v4, p0, LF/L;->u:Lo/t;

    invoke-virtual {p0, p1, p2}, LF/L;->b(LC/a;LD/a;)Z

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, LF/L;->t:Lo/a;

    if-eqz v2, :cond_2

    iget-object v2, p0, LF/L;->t:Lo/a;

    iput-object v2, p0, LF/L;->l:Lo/a;

    iput-object v4, p0, LF/L;->t:Lo/a;

    iget-object v2, p0, LF/L;->C:[LF/O;

    array-length v2, v2

    if-le v2, v0, :cond_1

    iput v1, p0, LF/L;->D:I

    iget-object v2, p0, LF/L;->C:[LF/O;

    aget-object v1, v2, v1

    iput-object v1, p0, LF/L;->o:LF/O;

    iget-object v1, p0, LF/L;->n:LF/o;

    iget-object v2, p0, LF/L;->o:LF/O;

    iget-object v2, v2, LF/O;->b:LF/s;

    invoke-virtual {v1, v2}, LF/o;->a(LF/s;)V

    :cond_1
    iput-object v4, p0, LF/L;->u:Lo/t;

    invoke-virtual {p0, p1, p2}, LF/L;->b(LC/a;LD/a;)Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, LF/L;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Lh/e;

    const-wide/16 v1, 0x1f4

    sget-object v3, Lh/g;->a:Lh/g;

    invoke-direct {v0, v1, v2, v3}, Lh/e;-><init>(JLh/g;)V

    iput-object v0, p0, LF/L;->E:Lh/e;

    :cond_0
    iput-boolean v4, p0, LF/L;->B:Z

    return v4
.end method

.method public a(Lo/aS;)Z
    .locals 1

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p1, v0}, Lo/aS;->a(Lo/T;)Z

    move-result v0

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    invoke-super {p0, p1}, LF/m;->b(LD/a;)V

    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0, p1}, LF/o;->b(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/L;->n:LF/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0, p1}, LF/o;->b(LD/a;)V

    :cond_1
    iget-object v0, p0, LF/L;->w:LE/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/L;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    :cond_2
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 10

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, LF/L;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LF/L;->i:Z

    if-eqz v0, :cond_2

    iget v0, p0, LF/L;->A:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    invoke-direct {p0, p1}, LF/L;->a(LC/a;)F

    move-result v0

    iget v3, p0, LF/L;->A:F

    div-float/2addr v0, v3

    invoke-static {v0}, LF/L;->a(F)I

    move-result v3

    iput v3, p0, LF/L;->k:I

    const/high16 v3, 0x3e800000

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_1

    const/high16 v3, 0x40000000

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_1

    move v0, v1

    :goto_0
    move v1, v0

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const/high16 v0, 0x10000

    iput v0, p0, LF/L;->k:I

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {p1}, LC/a;->r()F

    move-result v3

    iget-object v4, p0, LF/L;->u:Lo/t;

    if-eqz v4, :cond_3

    iget-boolean v4, p0, LF/L;->F:Z

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    iget v4, p0, LF/L;->G:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_0

    :cond_3
    iget-object v4, p0, LF/L;->u:Lo/t;

    if-eqz v4, :cond_5

    iget-object v4, p0, LF/L;->l:Lo/a;

    invoke-virtual {v4}, Lo/a;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget v4, p0, LF/L;->G:F

    cmpl-float v4, v3, v4

    if-nez v4, :cond_5

    iget-object v0, p0, LF/L;->u:Lo/t;

    iput-object v0, p0, LF/L;->v:Lo/t;

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iput-boolean v0, p0, LF/L;->F:Z

    iput v3, p0, LF/L;->G:F

    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_e

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v3, v0, 0x1

    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    :goto_3
    iget-object v4, p0, LF/L;->n:LF/o;

    if-nez v4, :cond_9

    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    int-to-float v3, v3

    iput v3, p0, LF/L;->q:F

    neg-int v3, v0

    int-to-float v3, v3

    iput v3, p0, LF/L;->r:F

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    :cond_6
    :goto_4
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v0, v0, Lx/l;->a:Lo/T;

    invoke-virtual {p1}, LC/a;->t()Lo/T;

    move-result-object v3

    iget-object v4, p0, LF/L;->l:Lo/a;

    invoke-virtual {v4}, Lo/a;->b()Lo/T;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    invoke-direct {p0, p1}, LF/L;->a(LC/a;)F

    move-result v0

    iput v0, p0, LF/L;->A:F

    iget-boolean v0, p0, LF/L;->y:Z

    if-nez v0, :cond_7

    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iget-object v3, p0, LF/L;->l:Lo/a;

    invoke-virtual {v3}, Lo/a;->b()Lo/T;

    move-result-object v3

    invoke-virtual {p1, v3, v1}, LC/a;->a(Lo/T;Z)F

    move-result v3

    div-float/2addr v0, v3

    iget v3, p0, LF/L;->p:F

    mul-float/2addr v3, v0

    iput v3, p0, LF/L;->p:F

    iget v3, p0, LF/L;->q:F

    mul-float/2addr v3, v0

    iput v3, p0, LF/L;->q:F

    iget v3, p0, LF/L;->r:F

    mul-float/2addr v3, v0

    iput v3, p0, LF/L;->r:F

    iget v3, p0, LF/L;->s:F

    mul-float/2addr v0, v3

    iput v0, p0, LF/L;->s:F

    :cond_7
    iget-object v0, p0, LF/L;->c:Ly/b;

    if-eqz v0, :cond_8

    iget-object v0, p0, LF/L;->c:Ly/b;

    instance-of v0, v0, Ly/a;

    if-eqz v0, :cond_8

    iget-object v0, p0, LF/L;->c:Ly/b;

    check-cast v0, Ly/a;

    invoke-virtual {v0}, Ly/a;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    instance-of v3, v0, Lo/r;

    if-eqz v3, :cond_8

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v3

    check-cast v0, Lo/r;

    invoke-virtual {v3, v0}, Ln/s;->e(Lo/r;)Ln/k;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v3, p0, LF/L;->l:Lo/a;

    invoke-virtual {v3}, Lo/a;->b()Lo/T;

    move-result-object v3

    iget-object v4, p0, LF/L;->l:Lo/a;

    invoke-virtual {v4}, Lo/a;->b()Lo/T;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3, v0}, Lo/T;->b(I)V

    :cond_8
    iget-object v0, p0, LF/L;->u:Lo/t;

    iput-object v0, p0, LF/L;->v:Lo/t;

    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-direct {p0, p1}, LF/L;->b(LC/a;)Lo/t;

    move-result-object v0

    iput-object v0, p0, LF/L;->u:Lo/t;

    :goto_5
    iget-object v0, p0, LF/L;->u:Lo/t;

    if-nez v0, :cond_0

    move v1, v2

    goto/16 :goto_1

    :cond_9
    iget-object v4, p0, LF/L;->n:LF/o;

    invoke-virtual {v4}, LF/o;->a()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v7, v4, 0x1

    iget-object v4, p0, LF/L;->n:LF/o;

    invoke-virtual {v4}, LF/o;->b()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v5, v4, 0x1

    if-le v3, v7, :cond_b

    move v6, v3

    :goto_6
    if-le v0, v5, :cond_c

    move v4, v0

    :goto_7
    sget-object v8, LF/M;->a:[I

    iget-object v9, p0, LF/L;->o:LF/O;

    iget-object v9, v9, LF/O;->a:LF/N;

    invoke-virtual {v9}, LF/N;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    int-to-float v0, v6

    iput v0, p0, LF/L;->q:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->r:F

    int-to-float v0, v4

    iput v0, p0, LF/L;->s:F

    :goto_8
    iget-object v0, p0, LF/L;->o:LF/O;

    iget-object v0, v0, LF/O;->a:LF/N;

    sget-object v4, LF/N;->d:LF/N;

    if-eq v0, v4, :cond_a

    iget-object v0, p0, LF/L;->o:LF/O;

    iget-object v0, v0, LF/O;->a:LF/N;

    sget-object v4, LF/N;->b:LF/N;

    if-ne v0, v4, :cond_6

    :cond_a
    sget-object v0, LF/M;->b:[I

    iget-object v4, p0, LF/L;->o:LF/O;

    iget-object v4, v4, LF/O;->b:LF/s;

    invoke-virtual {v4}, LF/s;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_4

    :pswitch_0
    neg-int v0, v3

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    iget v0, p0, LF/L;->p:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/L;->q:F

    goto/16 :goto_4

    :cond_b
    move v6, v7

    goto :goto_6

    :cond_c
    move v4, v5

    goto :goto_7

    :pswitch_1
    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto :goto_8

    :pswitch_2
    neg-int v0, v3

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    mul-int/lit8 v0, v7, 0x2

    add-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p0, LF/L;->q:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->r:F

    int-to-float v0, v4

    iput v0, p0, LF/L;->s:F

    goto :goto_8

    :pswitch_3
    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto :goto_8

    :pswitch_4
    neg-int v0, v3

    mul-int/lit8 v5, v7, 0x2

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    int-to-float v0, v3

    iput v0, p0, LF/L;->q:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->r:F

    int-to-float v0, v4

    iput v0, p0, LF/L;->s:F

    goto :goto_8

    :pswitch_5
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->q:F

    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_8

    :pswitch_6
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    int-to-float v4, v3

    iput v4, p0, LF/L;->q:F

    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_8

    :pswitch_7
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->q:F

    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_8

    :pswitch_8
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    int-to-float v4, v3

    iput v4, p0, LF/L;->q:F

    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_8

    :pswitch_9
    add-int/lit8 v0, v3, 0xa

    int-to-float v0, v0

    iput v0, p0, LF/L;->q:F

    iget v0, p0, LF/L;->q:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LF/L;->p:F

    goto/16 :goto_4

    :pswitch_a
    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    int-to-float v0, v6

    iput v0, p0, LF/L;->q:F

    goto/16 :goto_4

    :cond_d
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    iget-object v3, p0, LF/L;->I:[F

    invoke-virtual {p1, v0, v3}, LC/a;->a(Lo/T;[F)V

    iget-object v0, p0, LF/L;->I:[F

    aget v0, v0, v2

    iget-object v3, p0, LF/L;->I:[F

    aget v3, v3, v1

    iget v4, p0, LF/L;->p:F

    add-float/2addr v4, v0

    iget v5, p0, LF/L;->q:F

    add-float/2addr v0, v5

    iget v5, p0, LF/L;->r:F

    add-float/2addr v5, v3

    iget v6, p0, LF/L;->s:F

    add-float/2addr v3, v6

    invoke-virtual {p1, v4, v0, v5, v3}, LC/a;->a(FFFF)Lo/t;

    move-result-object v0

    iput-object v0, p0, LF/L;->u:Lo/t;

    goto/16 :goto_5

    :cond_e
    move v0, v2

    move v3, v2

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public m()F
    .locals 1

    iget v0, p0, LF/L;->H:F

    return v0
.end method

.method public n()Lo/ae;
    .locals 1

    iget-object v0, p0, LF/L;->u:Lo/t;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LF/L;->x:Ljava/lang/String;

    return-object v0
.end method

.method public y()Z
    .locals 1

    iget-object v0, p0, LF/L;->l:Lo/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    goto :goto_0
.end method
