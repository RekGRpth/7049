.class public LF/Q;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final c:[F

.field private static final d:F


# instance fields
.field private final A:Lo/T;

.field private final B:Ljava/util/ArrayList;

.field private final C:Ljava/util/ArrayList;

.field private D:Z

.field private E:F

.field b:F

.field private final e:Lo/aq;

.field private final f:[F

.field private final g:Lx/j;

.field private final h:Lx/j;

.field private final i:LE/o;

.field private final j:LE/i;

.field private final k:LE/a;

.field private final l:LE/a;

.field private final m:LE/d;

.field private final n:LE/a;

.field private final o:LE/o;

.field private final p:LE/i;

.field private final q:LE/d;

.field private final r:LE/a;

.field private final s:LE/o;

.field private final t:Lx/c;

.field private final u:LE/d;

.field private final v:LE/d;

.field private final w:LE/o;

.field private final x:LE/i;

.field private final y:LE/d;

.field private z:Lh/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, LF/Q;->c:[F

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, LF/Q;->d:F

    return-void

    nop

    :array_0
    .array-data 4
        0x3f37b7b8
        0x3f37b7b8
        0x3f65e5e6
        0x3f800000
    .end array-data
.end method

.method private constructor <init>(Lo/aq;LF/R;Ljava/util/HashSet;LD/a;I)V
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/high16 v5, -0x40800000

    const/high16 v4, 0x437f0000

    const/4 v3, 0x0

    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/Q;->A:Lo/T;

    iput-boolean v2, p0, LF/Q;->D:Z

    iput-object p1, p0, LF/Q;->e:Lo/aq;

    const/4 v0, 0x4

    new-array v0, v0, [F

    invoke-static {p5}, Lx/d;->d(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v2

    invoke-static {p5}, Lx/d;->e(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v6

    const/4 v1, 0x2

    invoke-static {p5}, Lx/d;->f(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Lx/d;->g(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    aput v2, v0, v1

    iput-object v0, p0, LF/Q;->f:[F

    iput-object v3, p0, LF/Q;->g:Lx/j;

    iput-object v3, p0, LF/Q;->h:Lx/j;

    new-instance v0, LE/r;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/r;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->i:LE/o;

    new-instance v0, LE/l;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/l;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->j:LE/i;

    new-instance v0, LE/c;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/c;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->l:LE/a;

    new-instance v0, LE/c;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/c;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->k:LE/a;

    new-instance v0, LE/f;

    iget v1, p2, LF/R;->b:I

    invoke-direct {v0, v1, v6}, LE/f;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->m:LE/d;

    iget-object v0, p2, LF/R;->i:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_0

    new-instance v0, LE/c;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1}, LE/c;-><init>(I)V

    iput-object v0, p0, LF/Q;->n:LE/a;

    :goto_0
    iget v0, p2, LF/R;->c:I

    if-lez v0, :cond_1

    new-instance v0, LE/r;

    iget v1, p2, LF/R;->c:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/Q;->s:LE/o;

    new-instance v0, Lx/c;

    iget v1, p2, LF/R;->c:I

    invoke-virtual {p4}, LD/a;->G()Lx/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/Q;->t:Lx/c;

    new-instance v0, LE/f;

    iget v1, p2, LF/R;->d:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->u:LE/d;

    new-instance v0, LE/f;

    iget v1, p2, LF/R;->b:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->v:LE/d;

    :goto_1
    iget v0, p2, LF/R;->e:I

    if-lez v0, :cond_2

    new-instance v0, LE/r;

    iget v1, p2, LF/R;->e:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/Q;->w:LE/o;

    new-instance v0, LE/l;

    iget v1, p2, LF/R;->e:I

    invoke-direct {v0, v1}, LE/l;-><init>(I)V

    iput-object v0, p0, LF/Q;->x:LE/i;

    new-instance v0, LE/f;

    iget v1, p2, LF/R;->f:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->y:LE/d;

    :goto_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LF/Q;->B:Ljava/util/ArrayList;

    iput v5, p0, LF/Q;->b:F

    iget v0, p2, LF/R;->g:I

    if-lez v0, :cond_3

    new-instance v0, LE/r;

    iget v1, p2, LF/R;->g:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/Q;->o:LE/o;

    new-instance v0, LE/l;

    iget v1, p2, LF/R;->g:I

    invoke-direct {v0, v1}, LE/l;-><init>(I)V

    iput-object v0, p0, LF/Q;->p:LE/i;

    new-instance v0, LE/f;

    iget v1, p2, LF/R;->h:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->q:LE/d;

    new-instance v0, LE/c;

    iget v1, p2, LF/R;->g:I

    invoke-direct {v0, v1}, LE/c;-><init>(I)V

    iput-object v0, p0, LF/Q;->r:LE/a;

    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LF/Q;->C:Ljava/util/ArrayList;

    iput v5, p0, LF/Q;->E:F

    return-void

    :cond_0
    iput-object v3, p0, LF/Q;->n:LE/a;

    goto/16 :goto_0

    :cond_1
    iput-object v3, p0, LF/Q;->s:LE/o;

    iput-object v3, p0, LF/Q;->t:Lx/c;

    iput-object v3, p0, LF/Q;->u:LE/d;

    iput-object v3, p0, LF/Q;->v:LE/d;

    goto :goto_1

    :cond_2
    iput-object v3, p0, LF/Q;->w:LE/o;

    iput-object v3, p0, LF/Q;->x:LE/i;

    iput-object v3, p0, LF/Q;->y:LE/d;

    goto :goto_2

    :cond_3
    iput-object v3, p0, LF/Q;->o:LE/o;

    iput-object v3, p0, LF/Q;->p:LE/i;

    iput-object v3, p0, LF/Q;->q:LE/d;

    iput-object v3, p0, LF/Q;->r:LE/a;

    goto :goto_3
.end method

.method private static a(F)F
    .locals 3

    float-to-int v1, p0

    int-to-float v0, v1

    sub-float v0, p0, v0

    const/high16 v2, 0x3f000000

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    sget v0, LF/Q;->d:F

    :goto_0
    const/4 v2, 0x1

    rsub-int/lit8 v1, v1, 0x1e

    shl-int v1, v2, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x43800000

    div-float/2addr v0, v1

    return v0

    :cond_0
    const/high16 v0, 0x3f800000

    goto :goto_0
.end method

.method private a(LC/a;)F
    .locals 2

    const/high16 v0, 0x40c00000

    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    invoke-static {v1}, LF/Q;->a(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static a(Lo/aj;)F
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->c()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(FI)I
    .locals 2

    int-to-float v0, p1

    sub-float v0, p0, v0

    const/high16 v1, 0x40800000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x40400000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40100000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/16 v0, 0x11

    if-lt p1, v0, :cond_3

    const/4 v0, 0x5

    goto :goto_0

    :cond_3
    const/16 v0, 0x16

    goto :goto_0
.end method

.method private static a(FILcom/google/android/maps/driveabout/vector/q;)I
    .locals 2

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_1

    const/high16 v0, 0x41800000

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x7

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x15

    goto :goto_0

    :cond_1
    int-to-float v0, p1

    sub-float v0, p0, v0

    const/high16 v1, 0x40800000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/high16 v1, 0x40400000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/high16 v1, 0x40100000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    const/16 v0, 0x11

    if-lt p1, v0, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public static a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, LC/a;->r()F

    move-result v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v2, :cond_0

    const/high16 v2, 0x418c0000

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_1

    :cond_0
    const/16 v0, 0x20

    :cond_1
    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v2, :cond_2

    or-int/lit8 v0, v0, 0x14

    :cond_2
    const/high16 v2, 0x41780000

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p1, v1, :cond_3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v1, :cond_4

    :cond_3
    or-int/lit8 v0, v0, 0x40

    :cond_4
    or-int/lit16 v0, v0, 0x180

    return v0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;Lz/D;Lz/D;Lx/n;LD/a;)LF/Q;
    .locals 13

    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v6

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    const/16 v0, 0x200

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v5

    new-instance v2, LF/R;

    invoke-direct {v2}, LF/R;-><init>()V

    new-instance v8, LF/S;

    invoke-direct {v8}, LF/S;-><init>()V

    const/4 v0, -0x1

    move v4, v0

    :goto_0
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v1

    instance-of v0, v1, Lo/af;

    if-nez v0, :cond_0

    move v5, v4

    :goto_1
    new-instance v0, LF/Q;

    move-object v1, p0

    move-object/from16 v4, p6

    invoke-direct/range {v0 .. v5}, LF/Q;-><init>(Lo/aq;LF/R;Ljava/util/HashSet;LD/a;I)V

    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v4

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/af;

    invoke-static {v2, v8}, LF/Q;->a(Lo/af;LF/S;)V

    move-object v1, v6

    move-object v3, v8

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, LF/Q;->a(Lo/ad;Lo/af;LF/S;Lx/h;Lx/n;)V

    goto :goto_2

    :cond_0
    move-object v0, v1

    check-cast v0, Lo/af;

    invoke-virtual {v0}, Lo/af;->o()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v9

    invoke-virtual {v9}, Lo/aj;->j()Lo/ai;

    move-result-object v9

    if-nez v9, :cond_2

    const v4, -0x48481b

    :cond_1
    :goto_3
    invoke-static {v0, v8}, LF/Q;->a(Lo/af;LF/S;)V

    invoke-static {v5, v0, v8, v2}, LF/Q;->a(ILo/af;LF/S;LF/R;)Z

    move-result v9

    if-nez v9, :cond_4

    move v5, v4

    goto :goto_1

    :cond_2
    const/4 v9, -0x1

    if-ne v4, v9, :cond_3

    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v4

    invoke-virtual {v4}, Lo/aj;->j()Lo/ai;

    move-result-object v4

    invoke-virtual {v4}, Lo/ai;->b()I

    move-result v4

    goto :goto_3

    :cond_3
    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v9

    invoke-virtual {v9}, Lo/aj;->j()Lo/ai;

    move-result-object v9

    invoke-virtual {v9}, Lo/ai;->b()I

    move-result v9

    if-eq v4, v9, :cond_1

    move v5, v4

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Lo/n;->l()[I

    move-result-object v9

    array-length v10, v9

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v10, :cond_6

    aget v11, v9, v1

    if-ltz v11, :cond_5

    array-length v12, p1

    if-ge v11, v12, :cond_5

    aget-object v11, p1, v11

    invoke-virtual {v3, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    move-object v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-direct/range {v0 .. v5}, LF/Q;->a(Lo/aq;Lz/D;Lz/D;Lx/n;LD/a;)V

    return-object v0

    :cond_8
    move v5, v4

    goto/16 :goto_1
.end method

.method public static a(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->n()V

    invoke-virtual {p0}, LD/a;->p()V

    invoke-static {p1, p2}, LF/Q;->a(FI)I

    move-result v1

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method public static a(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->p()V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method private a(Lo/X;Lo/T;II)V
    .locals 6

    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v1

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, LF/Q;->A:Lo/T;

    invoke-virtual {p1, v0, v3}, Lo/X;->a(ILo/T;)V

    iget-object v3, p0, LF/Q;->A:Lo/T;

    iget-object v4, p0, LF/Q;->A:Lo/T;

    invoke-static {v3, p2, v4}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    iget-object v3, p0, LF/Q;->s:LE/o;

    iget-object v4, p0, LF/Q;->A:Lo/T;

    invoke-virtual {v3, v4, p3}, LE/o;->a(Lo/T;I)V

    if-lez v0, :cond_0

    iget-object v3, p0, LF/Q;->u:LE/d;

    add-int v4, v1, v0

    add-int/lit8 v4, v4, -0x1

    int-to-short v4, v4

    add-int v5, v1, v0

    int-to-short v5, v5

    invoke-virtual {v3, v4, v5}, LE/d;->a(SS)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p4, v2}, Lx/c;->a(II)V

    return-void
.end method

.method private a(Lo/ad;Lo/af;LF/S;Lx/h;Lx/n;)V
    .locals 16

    move-object/from16 v0, p3

    iget-boolean v1, v0, LF/S;->a:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {p2 .. p2}, Lo/af;->e()Lo/aj;

    move-result-object v1

    invoke-virtual {v1}, Lo/aj;->b()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lo/ai;->f()Z

    move-result v9

    invoke-virtual {v1}, Lo/ai;->g()Z

    move-result v10

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lo/ad;->d()Lo/T;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lo/ad;->g()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lo/af;->b()Lo/X;

    move-result-object v2

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->c:F

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, LF/Q;->c(FI)F

    move-result v3

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->e:I

    if-nez v1, :cond_2

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->d:I

    if-nez v1, :cond_2

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->f:I

    if-eqz v1, :cond_4

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->i:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->m:LE/d;

    invoke-virtual {v1}, LE/d;->b()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->i:LE/o;

    move-object/from16 v0, p0

    iget-object v7, v0, LF/Q;->j:LE/i;

    move-object/from16 v0, p0

    iget-object v8, v0, LF/Q;->m:LE/d;

    const/4 v11, 0x0

    move-object/from16 v1, p4

    invoke-virtual/range {v1 .. v11}, Lx/h;->a(Lo/X;FLo/T;ILE/q;LE/k;LE/e;ZZB)I

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->i:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    sub-int/2addr v1, v12

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->k:LE/a;

    move-object/from16 v0, p3

    iget v7, v0, LF/S;->e:I

    invoke-virtual {v6, v7, v1}, LE/a;->b(II)V

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->l:LE/a;

    move-object/from16 v0, p3

    iget v7, v0, LF/S;->d:I

    invoke-virtual {v6, v7, v1}, LE/a;->b(II)V

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->n:LE/a;

    if-eqz v6, :cond_3

    move-object/from16 v0, p3

    iget v6, v0, LF/S;->f:I

    if-eqz v6, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->n:LE/a;

    move-object/from16 v0, p3

    iget v7, v0, LF/S;->f:I

    invoke-virtual {v6, v7, v1}, LE/a;->b(II)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_4

    move-object/from16 v0, p3

    iget-boolean v1, v0, LF/S;->b:Z

    if-eqz v1, :cond_7

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5, v1}, LF/Q;->a(Lo/X;Lo/T;II)V

    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->e:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    move-object/from16 v0, p2

    invoke-static {v1, v0}, LF/Q;->a(ILo/af;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v9, 0x0

    const/high16 v12, 0x3f800000

    move-object/from16 v0, p0

    iget-object v13, v0, LF/Q;->w:LE/o;

    move-object/from16 v0, p0

    iget-object v14, v0, LF/Q;->y:LE/d;

    const/4 v15, 0x0

    move-object/from16 v6, p4

    move-object v7, v2

    move v8, v3

    move-object v10, v4

    move v11, v5

    invoke-virtual/range {v6 .. v15}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->B:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object/from16 v0, p3

    iget v1, v0, LF/S;->g:I

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->o:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    const/4 v9, 0x0

    const/high16 v12, 0x3f800000

    move-object/from16 v0, p0

    iget-object v13, v0, LF/Q;->o:LE/o;

    move-object/from16 v0, p0

    iget-object v14, v0, LF/Q;->q:LE/d;

    const/4 v15, 0x0

    move-object/from16 v6, p4

    move-object v7, v2

    move v8, v3

    move-object v10, v4

    move v11, v5

    invoke-virtual/range {v6 .. v15}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    move-object/from16 v0, p0

    iget-object v2, v0, LF/Q;->o:LE/o;

    invoke-virtual {v2}, LE/o;->a()I

    move-result v2

    sub-int v1, v2, v1

    move-object/from16 v0, p0

    iget-object v2, v0, LF/Q;->r:LE/a;

    move-object/from16 v0, p3

    iget v3, v0, LF/S;->g:I

    invoke-virtual {v2, v3, v1}, LE/a;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->C:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual/range {p2 .. p2}, Lo/af;->r()Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LF/Q;->D:Z

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->v:LE/d;

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->m:LE/d;

    move-object/from16 v0, p0

    iget-object v7, v0, LF/Q;->m:LE/d;

    invoke-virtual {v7}, LE/d;->b()I

    move-result v7

    sub-int/2addr v7, v13

    invoke-virtual {v1, v6, v13, v7}, LE/d;->a(LE/d;II)V

    goto/16 :goto_1
.end method

.method private a(Lo/af;LE/k;F)V
    .locals 11

    const/high16 v0, 0x41800000

    mul-float/2addr v0, p3

    const/high16 v1, 0x3f800000

    div-float v4, v1, p3

    const/high16 v1, 0x3f800000

    div-float v5, v1, v0

    iget-object v0, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v0

    invoke-virtual {p1}, Lo/af;->b()Lo/X;

    move-result-object v6

    invoke-virtual {v6}, Lo/X;->b()I

    move-result v1

    add-int/lit8 v7, v1, -0x1

    invoke-virtual {p1}, Lo/af;->e()Lo/aj;

    move-result-object v1

    invoke-static {v1}, LF/Q;->a(Lo/aj;)F

    move-result v1

    const/high16 v2, 0x40000000

    invoke-direct {p0, v1, v0}, LF/Q;->c(FI)F

    move-result v0

    mul-float v8, v2, v0

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_3

    invoke-virtual {v6, v3}, Lo/X;->b(I)F

    move-result v9

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    mul-float/2addr v9, v5

    const/high16 v10, 0x3f000000

    cmpl-float v10, v9, v10

    if-lez v10, :cond_1

    mul-float v1, v8, v4

    const/high16 v2, 0x47000000

    mul-float/2addr v1, v2

    float-to-int v2, v1

    const/high16 v1, 0x47800000

    mul-float/2addr v1, v9

    float-to-int v1, v1

    float-to-int v10, v9

    int-to-float v10, v10

    sub-float/2addr v9, v10

    const/high16 v10, 0x3e000000

    cmpl-float v10, v9, v10

    if-lez v10, :cond_0

    const/high16 v10, 0x3ec00000

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    const v0, 0xa000

    :cond_0
    :goto_1
    const v9, 0x8000

    sub-int/2addr v9, v2

    const v10, 0x8000

    add-int/2addr v2, v10

    invoke-virtual {p1}, Lo/af;->k()Z

    move-result v10

    if-eqz v10, :cond_2

    add-int v10, v1, v0

    invoke-interface {p2, v2, v10}, LE/k;->a(II)V

    add-int/2addr v1, v0

    invoke-interface {p2, v9, v1}, LE/k;->a(II)V

    invoke-interface {p2, v9, v0}, LE/k;->a(II)V

    invoke-interface {p2, v2, v0}, LE/k;->a(II)V

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    const v0, 0xc000

    goto :goto_1

    :cond_2
    invoke-interface {p2, v9, v0}, LE/k;->a(II)V

    invoke-interface {p2, v2, v0}, LE/k;->a(II)V

    add-int v10, v1, v0

    invoke-interface {p2, v2, v10}, LE/k;->a(II)V

    add-int/2addr v0, v1

    invoke-interface {p2, v9, v0}, LE/k;->a(II)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method static a(Lo/af;LF/S;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0}, Lo/af;->e()Lo/aj;

    move-result-object v6

    invoke-static {v6}, LF/Q;->a(Lo/aj;)F

    move-result v0

    iput v0, p1, LF/S;->c:F

    iput v5, p1, LF/S;->d:I

    iput v5, p1, LF/S;->e:I

    iput v5, p1, LF/S;->f:I

    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-lt v0, v1, :cond_3

    invoke-virtual {v6, v5}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->d:I

    invoke-virtual {v6, v4}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->e:I

    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v2

    if-ge v0, v2, :cond_c

    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->d()[I

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->f:I

    :cond_0
    :goto_2
    iput v5, p1, LF/S;->g:I

    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-le v0, v1, :cond_5

    move v0, v1

    move v2, v3

    :goto_3
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v7

    if-ge v0, v7, :cond_5

    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v7

    invoke-virtual {v7}, Lo/ai;->d()[I

    move-result-object v7

    array-length v7, v7

    if-lez v7, :cond_4

    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->c()F

    move-result v2

    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v7

    invoke-virtual {v7}, Lo/ai;->b()I

    move-result v7

    iput v7, p1, LF/S;->g:I

    :cond_1
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-lt v0, v4, :cond_0

    invoke-virtual {v6, v5}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->e:I

    goto :goto_2

    :cond_4
    iget v7, p1, LF/S;->g:I

    if-eqz v7, :cond_1

    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v7

    invoke-virtual {v7}, Lo/ai;->c()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_1

    iput v5, p1, LF/S;->g:I

    goto :goto_4

    :cond_5
    iget v0, p1, LF/S;->g:I

    if-eqz v0, :cond_6

    iput v5, p1, LF/S;->f:I

    :cond_6
    iget v0, p1, LF/S;->f:I

    if-eqz v0, :cond_8

    iget v0, p1, LF/S;->e:I

    invoke-static {v0}, Lx/d;->a(I)I

    move-result v0

    iget v2, p1, LF/S;->d:I

    invoke-static {v2}, Lx/d;->a(I)I

    move-result v2

    if-lt v0, v2, :cond_7

    iget v0, p1, LF/S;->c:F

    const/high16 v2, 0x41100000

    cmpg-float v0, v0, v2

    if-gez v0, :cond_8

    :cond_7
    iget v0, p1, LF/S;->f:I

    iput v0, p1, LF/S;->e:I

    :cond_8
    invoke-virtual {p0}, Lo/af;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-lt v0, v1, :cond_a

    iget v0, p1, LF/S;->c:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_a

    iget v0, p1, LF/S;->e:I

    if-nez v0, :cond_9

    iget v0, p1, LF/S;->d:I

    if-nez v0, :cond_9

    iget v0, p1, LF/S;->f:I

    if-nez v0, :cond_9

    iget v0, p1, LF/S;->g:I

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lo/af;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v0, v4

    :goto_5
    iput-boolean v0, p1, LF/S;->a:Z

    invoke-virtual {p0}, Lo/af;->q()Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p1, LF/S;->g:I

    if-nez v0, :cond_b

    iget v0, p1, LF/S;->f:I

    if-nez v0, :cond_b

    iget v0, p1, LF/S;->e:I

    invoke-static {v0}, Lx/d;->c(I)Z

    move-result v0

    if-nez v0, :cond_b

    :goto_6
    iput-boolean v4, p1, LF/S;->b:Z

    return-void

    :cond_a
    move v0, v5

    goto :goto_5

    :cond_b
    move v4, v5

    goto :goto_6

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lo/aq;Lz/D;Lz/D;Lx/n;LD/a;)V
    .locals 0

    return-void
.end method

.method private static a(ILo/af;)Z
    .locals 1

    const/16 v0, 0xe

    if-lt p0, v0, :cond_0

    invoke-virtual {p1}, Lo/af;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(ILo/af;LF/S;LF/R;)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lo/af;->b()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v5, v4, -0x1

    iget-boolean v0, p2, LF/S;->a:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v3}, Lx/h;->a(Lo/X;)I

    move-result v6

    iget v0, p3, LF/R;->a:I

    if-lez v0, :cond_2

    iget v0, p3, LF/R;->a:I

    add-int/2addr v0, v6

    const/16 v7, 0x4000

    if-le v0, v7, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lo/af;->e()Lo/aj;

    move-result-object v7

    iget v0, p2, LF/S;->f:I

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v8, p3, LF/R;->i:Ljava/lang/Boolean;

    if-nez v8, :cond_6

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p3, LF/R;->i:Ljava/lang/Boolean;

    :cond_3
    iget v0, p2, LF/S;->g:I

    if-eqz v0, :cond_7

    :goto_2
    invoke-virtual {v7}, Lo/aj;->b()I

    move-result v0

    if-ge v2, v0, :cond_7

    invoke-virtual {v7, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->d()[I

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_4

    mul-int/lit8 v0, v5, 0x4

    mul-int/lit8 v8, v5, 0x2

    iget v9, p3, LF/R;->g:I

    add-int/2addr v0, v9

    iput v0, p3, LF/R;->g:I

    iget v0, p3, LF/R;->h:I

    mul-int/lit8 v8, v8, 0x3

    add-int/2addr v0, v8

    iput v0, p3, LF/R;->h:I

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-object v8, p3, LF/R;->i:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v8, v0, :cond_3

    move v1, v2

    goto :goto_0

    :cond_7
    iget v0, p3, LF/R;->a:I

    add-int/2addr v0, v6

    iput v0, p3, LF/R;->a:I

    iget v0, p3, LF/R;->b:I

    invoke-static {v3}, Lx/h;->b(Lo/X;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p3, LF/R;->b:I

    iget-boolean v0, p2, LF/S;->b:Z

    if-eqz v0, :cond_8

    iget v0, p3, LF/R;->c:I

    add-int/2addr v0, v4

    iput v0, p3, LF/R;->c:I

    iget v0, p3, LF/R;->d:I

    mul-int/lit8 v2, v5, 0x2

    add-int/2addr v0, v2

    iput v0, p3, LF/R;->d:I

    :cond_8
    invoke-static {p0, p1}, LF/Q;->a(ILo/af;)Z

    move-result v0

    if-eqz v0, :cond_0

    mul-int/lit8 v0, v5, 0x4

    mul-int/lit8 v2, v5, 0x2

    iget v3, p3, LF/R;->e:I

    add-int/2addr v0, v3

    iput v0, p3, LF/R;->e:I

    iget v0, p3, LF/R;->f:I

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    iput v0, p3, LF/R;->f:I

    goto/16 :goto_0
.end method

.method private b(LC/a;)F
    .locals 2

    const/high16 v0, 0x40c00000

    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    invoke-static {v1}, LF/Q;->a(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static b(FI)I
    .locals 2

    int-to-float v0, p1

    sub-float v0, p0, v0

    const/high16 v1, 0x40800000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const/16 v0, 0x19

    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x40400000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    const/16 v0, 0x1a

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40100000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    const/16 v0, 0x1b

    goto :goto_0

    :cond_2
    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/16 v0, 0x11

    if-lt p1, v0, :cond_3

    const/16 v0, 0x1c

    goto :goto_0

    :cond_3
    const/16 v0, 0x1d

    goto :goto_0
.end method

.method public static b(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->n()V

    invoke-virtual {p0}, LD/a;->p()V

    invoke-static {p1, p2, p3}, LF/Q;->a(FILcom/google/android/maps/driveabout/vector/q;)I

    move-result v1

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method public static b(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->n()V

    invoke-virtual {p0}, LD/a;->p()V

    const/16 v1, 0x1e

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method private c(FI)F
    .locals 3

    iget-object v0, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_0

    const/high16 v0, 0x3f000000

    :goto_0
    int-to-float v1, p2

    mul-float/2addr v1, p1

    const/high16 v2, 0x43800000

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0

    :cond_0
    const v0, 0x3e99999a

    goto :goto_0
.end method

.method public static c(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->n()V

    invoke-virtual {p0}, LD/a;->p()V

    invoke-static {p1, p2}, LF/Q;->b(FI)I

    move-result v1

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->k:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->l:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->m:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->n:LE/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/Q;->n:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_1

    iget-object v1, p0, LF/Q;->s:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, LF/Q;->t:Lx/c;

    if-eqz v1, :cond_2

    iget-object v1, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, LF/Q;->u:LE/d;

    if-eqz v1, :cond_3

    iget-object v1, p0, LF/Q;->u:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_4

    iget-object v1, p0, LF/Q;->v:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, LF/Q;->w:LE/o;

    if-eqz v1, :cond_5

    iget-object v1, p0, LF/Q;->w:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, LF/Q;->x:LE/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, LF/Q;->x:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, LF/Q;->y:LE/d;

    if-eqz v1, :cond_7

    iget-object v1, p0, LF/Q;->y:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, LF/Q;->o:LE/o;

    if-eqz v1, :cond_8

    iget-object v1, p0, LF/Q;->o:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, LF/Q;->p:LE/i;

    if-eqz v1, :cond_9

    iget-object v1, p0, LF/Q;->p:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, LF/Q;->q:LE/d;

    if-eqz v1, :cond_a

    iget-object v1, p0, LF/Q;->q:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, LF/Q;->r:LE/a;

    if-eqz v1, :cond_b

    iget-object v1, p0, LF/Q;->r:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    return v0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->j:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->l:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    iget-object v0, p0, LF/Q;->k:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->n:LE/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/Q;->n:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/Q;->s:LE/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->u:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    :cond_1
    iget-object v0, p0, LF/Q;->w:LE/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/Q;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->y:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    :cond_2
    iget-object v0, p0, LF/Q;->o:LE/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, LF/Q;->o:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->q:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->r:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    :cond_3
    return-void
.end method

.method a(LD/a;F)V
    .locals 8

    iget v0, p0, LF/Q;->b:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p2, p0, LF/Q;->b:F

    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v1, p0, LF/Q;->x:LE/i;

    iget-object v0, p0, LF/Q;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-direct {p0, v0, v1, p2}, LF/Q;->a(Lo/af;LE/k;F)V

    goto :goto_1

    :cond_1
    new-instance v0, Lh/e;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x1f4

    sget-object v5, Lh/g;->c:Lh/g;

    const/4 v6, 0x0

    const/16 v7, 0x64

    invoke-direct/range {v0 .. v7}, Lh/e;-><init>(JJLh/g;II)V

    iput-object v0, p0, LF/Q;->z:Lh/e;

    goto :goto_0
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 8

    const/4 v5, 0x1

    const-wide/16 v3, 0x0

    const/high16 v2, 0x3e800000

    const/4 v7, 0x4

    invoke-virtual {p2}, LC/a;->r()F

    move-result v0

    iget-object v1, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->a(LD/a;)V

    invoke-static {p1}, Lx/a;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->u:LE/d;

    invoke-virtual {v0, p1, v5}, LE/d;->a(LD/a;I)V

    invoke-static {p1}, Lx/a;->d(LD/a;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    iget-object v1, p0, LF/Q;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/Q;->l:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_1

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    :cond_1
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    iget-object v1, p0, LF/Q;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/Q;->k:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    :cond_3
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, LF/Q;->w:LE/o;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, LF/Q;->a(LC/a;)F

    move-result v0

    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, p1, v0}, LF/Q;->a(LD/a;F)V

    const/high16 v0, 0x3f800000

    iget-object v1, p0, LF/Q;->z:Lh/e;

    if-eqz v1, :cond_5

    iget-object v0, p0, LF/Q;->z:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    const v1, 0x3f7d70a4

    cmpl-float v1, v0, v1

    if-lez v1, :cond_5

    const/4 v1, 0x0

    iput-object v1, p0, LF/Q;->z:Lh/e;

    :cond_5
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    iget-object v2, p0, LF/Q;->f:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, LF/Q;->f:[F

    aget v3, v3, v5

    iget-object v4, p0, LF/Q;->f:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, LF/Q;->f:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    mul-float/2addr v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p0, LF/Q;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/Q;->y:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, LF/Q;->n:LE/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    iget-object v1, p0, LF/Q;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/Q;->n:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_6

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    :cond_6
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, LF/Q;->o:LE/o;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, LF/Q;->b(LC/a;)F

    move-result v0

    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, p1, v0}, LF/Q;->b(LD/a;F)V

    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p0, LF/Q;->o:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/Q;->r:LE/a;

    invoke-virtual {v0, p1}, LE/a;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->q:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public b()I
    .locals 3

    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x1d0

    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->k:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->l:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->m:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->n:LE/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/Q;->n:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_1

    iget-object v1, p0, LF/Q;->s:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, LF/Q;->t:Lx/c;

    if-eqz v1, :cond_2

    iget-object v1, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, LF/Q;->u:LE/d;

    if-eqz v1, :cond_3

    iget-object v1, p0, LF/Q;->u:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_4

    iget-object v1, p0, LF/Q;->v:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, LF/Q;->w:LE/o;

    if-eqz v1, :cond_5

    iget-object v1, p0, LF/Q;->w:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, LF/Q;->x:LE/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, LF/Q;->x:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, LF/Q;->y:LE/d;

    if-eqz v1, :cond_7

    iget-object v1, p0, LF/Q;->y:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, LF/Q;->o:LE/o;

    if-eqz v1, :cond_8

    iget-object v1, p0, LF/Q;->o:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, LF/Q;->p:LE/i;

    if-eqz v1, :cond_9

    iget-object v1, p0, LF/Q;->p:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, LF/Q;->q:LE/d;

    if-eqz v1, :cond_a

    iget-object v1, p0, LF/Q;->q:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, LF/Q;->r:LE/a;

    if-eqz v1, :cond_b

    iget-object v1, p0, LF/Q;->r:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    add-int/lit8 v0, v0, 0x18

    iget-object v1, p0, LF/Q;->B:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-virtual {v0}, Lo/af;->m()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_c
    add-int/lit8 v0, v1, 0x18

    iget-object v1, p0, LF/Q;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-virtual {v0}, Lo/af;->m()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_d
    return v1
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->j:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->l:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->k:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->n:LE/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/Q;->n:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/Q;->s:LE/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->u:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    :cond_1
    iget-object v0, p0, LF/Q;->w:LE/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/Q;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->y:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    :cond_2
    iget-object v0, p0, LF/Q;->o:LE/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, LF/Q;->o:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->q:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, LF/Q;->r:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    :cond_3
    return-void
.end method

.method b(LD/a;F)V
    .locals 12

    iget v0, p0, LF/Q;->E:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iput p2, p0, LF/Q;->E:F

    const/high16 v0, 0x41800000

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f800000

    div-float/2addr v1, p2

    const/high16 v2, 0x3f800000

    div-float/2addr v2, v0

    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v0, p0, LF/Q;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iget-object v0, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v4

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-virtual {v0}, Lo/af;->b()Lo/X;

    move-result-object v5

    invoke-virtual {v5}, Lo/X;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v0

    invoke-static {v0}, LF/Q;->a(Lo/aj;)F

    move-result v0

    const/high16 v7, 0x40000000

    invoke-direct {p0, v0, v4}, LF/Q;->c(FI)F

    move-result v0

    mul-float/2addr v7, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_2

    invoke-virtual {v5, v0}, Lo/X;->b(I)F

    move-result v8

    mul-float/2addr v8, v2

    mul-float v9, v7, v1

    const/high16 v10, 0x47000000

    mul-float/2addr v9, v10

    float-to-int v9, v9

    const/high16 v10, 0x47800000

    mul-float/2addr v8, v10

    float-to-int v8, v8

    const v10, 0x8000

    sub-int/2addr v10, v9

    const v11, 0x8000

    add-int/2addr v9, v11

    iget-object v11, p0, LF/Q;->p:LE/i;

    invoke-virtual {v11, v9, v8}, LE/i;->a(II)V

    iget-object v11, p0, LF/Q;->p:LE/i;

    invoke-virtual {v11, v10, v8}, LE/i;->a(II)V

    iget-object v8, p0, LF/Q;->p:LE/i;

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, LE/i;->a(II)V

    iget-object v8, p0, LF/Q;->p:LE/i;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, LE/i;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
