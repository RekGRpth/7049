.class public LF/U;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/h;


# static fields
.field static final a:LF/U;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LF/U;

    invoke-direct {v0}, LF/U;-><init>()V

    sput-object v0, LF/U;->a:LF/U;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LD/a;LC/a;)V
    .locals 4

    const/high16 v1, 0x10000

    invoke-virtual {p0}, LD/a;->A()V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v1, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, LD/a;->f:LE/o;

    invoke-virtual {v0, p0}, LE/o;->d(LD/a;)V

    invoke-virtual {p0}, LD/a;->o()V

    invoke-virtual {p0}, LD/a;->q()V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p0}, LD/a;->B()V

    return-void
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 3

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v0

    invoke-virtual {v0}, LA/c;->i()Z

    move-result v2

    sput-boolean v2, LF/U;->b:Z

    sget-boolean v2, LF/U;->b:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, LA/c;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/high16 v0, 0x40a00000

    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    iget-object v0, p0, LD/a;->j:LE/o;

    invoke-virtual {v0, p0}, LE/o;->d(LD/a;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 0

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    sget-boolean v0, LF/U;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    return-void
.end method

.method public b(LD/a;)V
    .locals 0

    return-void
.end method
