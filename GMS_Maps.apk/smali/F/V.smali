.class public LF/V;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static final c:I

.field private static volatile d:Z

.field private static final j:F


# instance fields
.field private final e:Lo/aq;

.field private final f:LE/o;

.field private final g:LE/a;

.field private final h:LE/i;

.field private final i:LE/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LF/V;->b:[I

    sget-object v0, LF/V;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x64

    sput v0, LF/V;->c:I

    const/4 v0, 0x0

    sput-boolean v0, LF/V;->d:Z

    const-wide/high16 v0, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, LF/V;->j:F

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
    .end array-data
.end method

.method private constructor <init>(Lo/aq;LF/X;Ljava/util/Set;)V
    .locals 2

    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, LF/V;->e:Lo/aq;

    new-instance v0, LE/o;

    iget v1, p2, LF/X;->a:I

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    iput-object v0, p0, LF/V;->f:LE/o;

    new-instance v0, LE/a;

    iget v1, p2, LF/X;->a:I

    invoke-direct {v0, v1}, LE/a;-><init>(I)V

    iput-object v0, p0, LF/V;->g:LE/a;

    new-instance v0, LE/i;

    iget v1, p2, LF/X;->a:I

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, LF/V;->h:LE/i;

    new-instance v0, LE/d;

    iget v1, p2, LF/X;->b:I

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    iput-object v0, p0, LF/V;->i:LE/d;

    return-void
.end method

.method private static a(FI)F
    .locals 2

    int-to-float v0, p1

    mul-float/2addr v0, p0

    const/high16 v1, 0x3fa00000

    mul-float/2addr v0, v1

    const/high16 v1, 0x43800000

    div-float/2addr v0, v1

    return v0
.end method

.method private static a(IF)F
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/high16 v0, 0x42a00000

    div-float/2addr v0, p1

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, 0x43b40000

    div-float/2addr v0, p1

    goto :goto_0

    :pswitch_1
    const/high16 v0, 0x43700000

    div-float/2addr v0, p1

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x43200000

    div-float/2addr v0, p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/V;
    .locals 7

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, LF/X;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, LF/X;-><init>(LF/W;)V

    invoke-interface {p2}, Lo/aO;->c()V

    :cond_0
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    instance-of v1, v0, Lo/aC;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lo/aC;

    invoke-static {v1, v3}, LF/V;->a(Lo/aC;LF/X;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    invoke-interface {p2}, Lo/aO;->d()V

    new-instance v1, LF/V;

    invoke-direct {v1, p0, v3, v2}, LF/V;-><init>(Lo/aq;LF/X;Ljava/util/Set;)V

    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v2

    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v3

    :goto_0
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v0

    instance-of v4, v0, Lo/aC;

    if-eqz v4, :cond_2

    check-cast v0, Lo/aC;

    invoke-direct {v1, v2, v0, v3}, LF/V;->a(Lo/ad;Lo/aC;Lx/h;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    invoke-direct {v1, p0}, LF/V;->a(Lo/aq;)V

    return-object v1

    :cond_3
    invoke-interface {v0}, Lo/n;->l()[I

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_0

    aget v5, v1, v0

    if-ltz v5, :cond_4

    array-length v6, p1

    if-ge v5, v6, :cond_4

    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, LD/a;->p()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    return-void
.end method

.method private a(Lo/aq;)V
    .locals 0

    return-void
.end method

.method public static a(Z)V
    .locals 0

    sput-boolean p0, LF/V;->d:Z

    return-void
.end method

.method public static a(Lo/aC;LF/X;)Z
    .locals 4

    invoke-virtual {p0}, Lo/aC;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v1, v0, 0x4

    iget v2, p1, LF/X;->a:I

    add-int/2addr v2, v1

    const/16 v3, 0x1000

    if-le v2, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v2, p1, LF/X;->a:I

    add-int/2addr v1, v2

    iput v1, p1, LF/X;->a:I

    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    iget v2, p1, LF/X;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v2

    iput v0, p1, LF/X;->b:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lo/ad;Lo/aC;Lx/h;)Z
    .locals 11

    const/4 v10, 0x1

    invoke-virtual {p2}, Lo/aC;->b()Lo/X;

    move-result-object v0

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000

    mul-float/2addr v1, v2

    const/high16 v2, 0x43800000

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lo/X;->b(F)Lo/X;

    move-result-object v1

    invoke-virtual {v1}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, LF/V;->f:LE/o;

    invoke-virtual {v2}, LE/o;->a()I

    move-result v2

    mul-int/lit8 v3, v0, 0x4

    add-int v5, v2, v3

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v4

    invoke-virtual {p2}, Lo/aC;->e()Lo/aj;

    move-result-object v2

    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v6

    if-gtz v6, :cond_0

    :goto_0
    return v10

    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lo/aj;->b(I)Lo/ai;

    move-result-object v6

    invoke-virtual {v6}, Lo/ai;->c()F

    move-result v6

    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v2, v7}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->b()I

    move-result v7

    invoke-static {v6, v4}, LF/V;->a(FI)F

    move-result v2

    invoke-virtual {p2}, Lo/aC;->c()Z

    move-result v8

    if-nez v8, :cond_1

    neg-float v2, v2

    :cond_1
    iget-object v8, p0, LF/V;->g:LE/a;

    invoke-virtual {v8, v5}, LE/a;->b(I)V

    iget-object v5, p0, LF/V;->g:LE/a;

    mul-int/lit8 v0, v0, 0x4

    invoke-virtual {v5, v7, v0}, LE/a;->b(II)V

    invoke-virtual {p2}, Lo/aC;->d()I

    move-result v0

    invoke-static {v0, v6}, LF/V;->a(IF)F

    move-result v5

    iget-object v6, p0, LF/V;->f:LE/o;

    iget-object v7, p0, LF/V;->i:LE/d;

    iget-object v8, p0, LF/V;->h:LE/i;

    const/4 v9, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v9}, Lx/h;->a(Lo/X;FLo/T;IFLE/q;LE/e;LE/k;LE/k;)V

    goto :goto_0
.end method

.method private b(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    const/4 v3, 0x4

    const/high16 v2, 0x10000

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->d:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_1

    :cond_0
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v2, v2, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1, v3}, LE/d;->a(LD/a;I)V

    :cond_1
    const/16 v0, 0xb

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, LD/a;->n()V

    sget-boolean v0, LF/V;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/V;->g:LE/a;

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, LE/a;->a(I)V

    :cond_2
    iget-object v0, p0, LF/V;->g:LE/a;

    invoke-virtual {v0, p1}, LE/a;->c(LD/a;)V

    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1, v3}, LE/d;->a(LD/a;I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/V;->g:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->h:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->i:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/V;->g:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    iget-object v0, p0, LF/V;->h:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 6

    const/16 v5, 0x1702

    const/16 v4, 0x1700

    const/4 v3, 0x0

    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0}, LE/d;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/V;->h:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    invoke-virtual {p2}, LC/a;->r()F

    move-result v0

    iget-object v1, p0, LF/V;->e:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    sget v2, LF/V;->j:F

    invoke-interface {v1, v2, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_2
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    invoke-direct {p0, p1, v1}, LF/V;->b(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0xb8

    iget-object v1, p0, LF/V;->g:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->h:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->i:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/V;->g:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    iget-object v0, p0, LF/V;->h:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    return-void
.end method
