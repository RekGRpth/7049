.class public LF/a;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static c:I

.field private static final k:Lo/aj;

.field private static final l:Lo/aj;

.field private static final m:Ljava/lang/ThreadLocal;


# instance fields
.field private final d:LE/o;

.field private final e:LE/o;

.field private final f:Lx/c;

.field private final g:Lx/c;

.field private h:Lz/v;

.field private i:Lz/v;

.field private final j:LF/c;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/4 v2, 0x2

    const/4 v10, 0x1

    const v9, -0x45749f

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LF/a;->b:[I

    const/16 v0, 0x4000

    sput v0, LF/a;->c:I

    new-instance v0, Lo/aj;

    new-array v3, v1, [I

    new-array v4, v10, [Lo/ai;

    new-instance v6, Lo/ai;

    const/high16 v7, 0x40000000

    new-array v8, v1, [I

    invoke-direct {v6, v9, v7, v8, v1}, Lo/ai;-><init>(IF[II)V

    aput-object v6, v4, v1

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LF/a;->k:Lo/aj;

    new-instance v0, Lo/aj;

    new-array v3, v1, [I

    new-array v4, v10, [Lo/ai;

    new-instance v6, Lo/ai;

    const/high16 v7, 0x3fc00000

    new-array v8, v1, [I

    invoke-direct {v6, v9, v7, v8, v1}, Lo/ai;-><init>(IF[II)V

    aput-object v6, v4, v1

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LF/a;->l:Lo/aj;

    new-instance v0, LF/b;

    invoke-direct {v0}, LF/b;-><init>()V

    sput-object v0, LF/a;->m:Ljava/lang/ThreadLocal;

    return-void

    :array_0
    .array-data 4
        0x0
        0x2
        0x2
        0x4
        0x2
        0x4
        0x4
        0x6
    .end array-data
.end method

.method private constructor <init>(IILjava/util/Set;LF/c;LD/a;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    iput-object v0, p0, LF/a;->i:Lz/v;

    iput-object v0, p0, LF/a;->h:Lz/v;

    const/4 v0, 0x1

    new-instance v1, LE/r;

    invoke-direct {v1, p1, v0}, LE/r;-><init>(IZ)V

    iput-object v1, p0, LF/a;->e:LE/o;

    new-instance v1, LE/r;

    invoke-direct {v1, p2, v0}, LE/r;-><init>(IZ)V

    iput-object v1, p0, LF/a;->d:LE/o;

    new-instance v0, Lx/c;

    invoke-virtual {p5}, LD/a;->G()Lx/a;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/a;->g:Lx/c;

    new-instance v0, Lx/c;

    invoke-virtual {p5}, LD/a;->G()Lx/a;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/a;->f:Lx/c;

    iput-object p4, p0, LF/a;->j:LF/c;

    return-void
.end method

.method static a(Lo/f;)I
    .locals 1

    invoke-virtual {p0}, Lo/f;->e()Lo/aj;

    move-result-object v0

    invoke-virtual {v0}, Lo/aj;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lo/f;->b()Lo/aF;

    move-result-object v0

    invoke-virtual {v0}, Lo/aF;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;LF/y;LF/d;ILD/a;)LF/a;
    .locals 13

    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    if-eqz p4, :cond_f

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    move-object v6, v1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v5

    instance-of v1, v5, Lo/f;

    if-eqz v1, :cond_2

    move-object v1, v5

    check-cast v1, Lo/f;

    invoke-static {v1}, LF/a;->a(Lo/f;)I

    move-result v9

    invoke-static {v1}, LF/a;->b(Lo/f;)I

    move-result v10

    sget v11, LF/a;->c:I

    if-gt v9, v11, :cond_0

    sget v11, LF/a;->c:I

    if-le v10, v11, :cond_1

    :cond_0
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_1
    add-int v11, v2, v9

    sget v12, LF/a;->c:I

    if-gt v11, v12, :cond_2

    add-int v11, v3, v10

    sget v12, LF/a;->c:I

    if-le v11, v12, :cond_4

    :cond_2
    const/4 v5, 0x0

    if-eqz v6, :cond_d

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/f;

    invoke-virtual {v1}, Lo/f;->j()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    add-int/2addr v2, v9

    add-int/2addr v3, v10

    invoke-interface {v5}, Lo/n;->l()[I

    move-result-object v9

    array-length v10, v9

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v10, :cond_6

    aget v11, v9, v5

    if-ltz v11, :cond_5

    array-length v12, p1

    if-ge v11, v12, :cond_5

    aget-object v11, p1, v11

    invoke-virtual {v4, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_b

    invoke-virtual {v1}, Lo/f;->j()Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v5, 0x1

    :goto_4
    invoke-static {v1}, LF/a;->c(Lo/f;)Z

    move-result v9

    if-nez v5, :cond_7

    if-nez v9, :cond_a

    :cond_7
    invoke-virtual {v1}, Lo/f;->c()Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-virtual {v1}, Lo/f;->b()Lo/aF;

    move-result-object v10

    invoke-virtual {v1}, Lo/f;->d()[B

    move-result-object v11

    invoke-static {v10, v11}, Lx/p;->a(Lo/aF;[B)Ljava/util/List;

    move-result-object v10

    if-eqz p3, :cond_8

    if-nez v9, :cond_8

    const/4 v9, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, v9}, LF/a;->a(LF/y;Lo/f;Ljava/util/List;Lo/aq;)V

    :cond_8
    if-eqz p3, :cond_9

    invoke-virtual {v1}, Lo/f;->j()Z

    move-result v9

    if-eqz v9, :cond_9

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, p0}, LF/a;->a(LF/y;Lo/f;Ljava/util/List;Lo/aq;)V

    :cond_9
    if-eqz v5, :cond_a

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_a
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto/16 :goto_1

    :cond_b
    const/4 v5, 0x0

    goto :goto_4

    :cond_c
    new-instance v5, LF/c;

    move-object/from16 v0, p4

    invoke-direct {v5, v7, v6, v9, v0}, LF/c;-><init>(Lo/ad;Ljava/util/List;Ljava/util/List;LF/d;)V

    :cond_d
    new-instance v1, LF/a;

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v6}, LF/a;-><init>(IILjava/util/Set;LF/c;LD/a;)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/f;

    invoke-direct {v1, v7, v2}, LF/a;->a(Lo/ad;Lo/f;)V

    goto :goto_5

    :cond_e
    move/from16 v0, p5

    invoke-direct {v1, p0, v0}, LF/a;->a(Lo/aq;I)V

    return-object v1

    :cond_f
    move-object v6, v1

    goto/16 :goto_0
.end method

.method private static a(F)Lo/aj;
    .locals 1

    const/high16 v0, 0x41900000

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    sget-object v0, LF/a;->k:Lo/aj;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LF/a;->l:Lo/aj;

    goto :goto_0
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 5

    const/high16 v4, 0x10000

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LD/a;->p()V

    :cond_0
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method private static a(LF/y;Lo/f;Ljava/util/List;Lo/aq;)V
    .locals 14

    if-eqz p3, :cond_1

    sget-object v1, Lo/av;->c:Lo/av;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v1

    check-cast v1, Lo/E;

    invoke-virtual {v1}, Lo/E;->b()Lo/r;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lo/aq;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, LF/a;->a(F)Lo/aj;

    move-result-object v5

    :goto_0
    invoke-virtual {p1}, Lo/f;->b()Lo/aF;

    move-result-object v1

    invoke-virtual {v1}, Lo/aF;->a()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v5}, Lo/aj;->b()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lo/f;->c()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lo/f;->a()Lo/o;

    move-result-object v2

    invoke-virtual {p1}, Lo/f;->e()Lo/aj;

    move-result-object v5

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lo/f;->i()I

    move-result v8

    invoke-virtual {p1}, Lo/f;->f()I

    move-result v6

    invoke-virtual {p1}, Lo/f;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lo/f;->l()[I

    move-result-object v11

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lo/X;

    new-instance v1, Lo/K;

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    if-eqz v2, :cond_3

    const/4 v12, 0x1

    :goto_2
    invoke-direct/range {v1 .. v12}, Lo/K;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[IZ)V

    invoke-virtual {p0, v1}, LF/y;->a(Lo/n;)V

    goto :goto_1

    :cond_3
    const/4 v12, 0x0

    goto :goto_2
.end method

.method private a(Lo/ad;Lo/f;)V
    .locals 17

    invoke-virtual/range {p2 .. p2}, Lo/f;->e()Lo/aj;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lo/f;->b()Lo/aF;

    move-result-object v1

    invoke-virtual {v1}, Lo/aF;->a()I

    move-result v12

    if-nez v12, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v4}, Lo/aj;->c()I

    move-result v2

    if-lez v2, :cond_7

    const/4 v2, 0x1

    move v7, v2

    :goto_1
    invoke-static/range {p2 .. p2}, LF/a;->c(Lo/f;)Z

    move-result v13

    if-nez v7, :cond_2

    if-eqz v13, :cond_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lo/ad;->g()I

    move-result v14

    if-eqz v7, :cond_8

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lo/aj;->a(I)I

    move-result v2

    move v11, v2

    :goto_2
    if-eqz v13, :cond_9

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->b()I

    move-result v2

    move v8, v2

    :goto_3
    invoke-virtual/range {p2 .. p2}, Lo/f;->d()[B

    move-result-object v15

    const/4 v10, 0x0

    const/4 v9, 0x0

    sget-object v2, LF/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lo/T;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    sget-object v2, LF/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lo/T;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    sget-object v2, LF/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lo/T;

    const/4 v6, 0x2

    aget-object v6, v2, v6

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v12, :cond_a

    invoke-virtual/range {v1 .. v6}, Lo/aF;->a(ILo/T;Lo/T;Lo/T;Lo/T;)V

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->e:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, LE/o;->a(Lo/T;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->e:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, LE/o;->a(Lo/T;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->e:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, LE/o;->a(Lo/T;I)V

    add-int/lit8 v10, v10, 0x3

    :cond_3
    if-eqz v13, :cond_6

    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x1

    if-eqz v16, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, LE/o;->a(Lo/T;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, LE/o;->a(Lo/T;I)V

    add-int/lit8 v9, v9, 0x2

    :cond_4
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x2

    if-eqz v16, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, LE/o;->a(Lo/T;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, LE/o;->a(Lo/T;I)V

    add-int/lit8 v9, v9, 0x2

    :cond_5
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x4

    if-eqz v16, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, LE/o;->a(Lo/T;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, LE/o;->a(Lo/T;I)V

    add-int/lit8 v9, v9, 0x2

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_7
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, LF/a;->g:Lx/c;

    invoke-virtual {v1, v11, v10}, Lx/c;->a(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, LF/a;->f:Lx/c;

    invoke-virtual {v1, v8, v9}, Lx/c;->a(II)V

    goto/16 :goto_0
.end method

.method private a(Lo/aq;I)V
    .locals 0

    return-void
.end method

.method static b(Lo/f;)I
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, LF/a;->c(Lo/f;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lo/f;->d()[B

    move-result-object v2

    move v1, v0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-byte v3, v2, v0

    and-int/lit8 v3, v3, 0x7

    sget-object v4, LF/a;->b:[I

    aget v3, v4, v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/ThreadLocal;
    .locals 1

    sget-object v0, LF/a;->m:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method private static c(Lo/f;)Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lo/f;->e()Lo/aj;

    move-result-object v2

    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lo/f;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v3

    invoke-virtual {v3}, Lo/ai;->c()F

    move-result v3

    const/high16 v4, 0x3f800000

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    invoke-virtual {v2, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v3

    invoke-virtual {v3}, Lo/ai;->e()Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->f:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/a;->j:LF/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0}, LF/c;->a()I

    move-result v0

    goto :goto_0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/a;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/a;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    iget-object v0, p0, LF/a;->f:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    iget-object v0, p0, LF/a;->j:LF/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0, p1}, LF/c;->a(LD/a;)V

    :cond_0
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1}, Lx/a;->c(LD/a;)V

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/a;->g:Lx/c;

    invoke-virtual {v1, p1}, Lx/c;->a(LD/a;)V

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/a;->f:Lx/c;

    invoke-virtual {v1, p1}, Lx/c;->a(LD/a;)V

    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_1
    invoke-static {p1}, Lx/a;->d(LD/a;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_4
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/a;->j:LF/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0, p1, p2, p3}, LF/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_0
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x9c

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->f:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/a;->j:LF/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0}, LF/c;->b()I

    move-result v0

    goto :goto_0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/a;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/a;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    iget-object v0, p0, LF/a;->f:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    iget-object v0, p0, LF/a;->j:LF/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0, p1}, LF/c;->b(LD/a;)V

    :cond_0
    return-void
.end method
