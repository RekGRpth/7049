.class public LF/e;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final b:Lo/T;

.field private static final c:Ljava/util/Comparator;


# instance fields
.field private final d:LE/o;

.field private final e:LE/a;

.field private f:Lh/p;

.field private final g:LE/d;

.field private final h:LE/d;

.field private final i:LE/e;

.field private final j:Lo/T;

.field private final k:Lo/T;

.field private final l:Lo/T;

.field private final m:Lo/T;

.field private final n:Lo/T;

.field private final o:Lo/T;

.field private final p:Lo/T;

.field private final q:Lo/T;

.field private final r:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const v2, 0xb504

    new-instance v0, Lo/T;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lo/T;-><init>(III)V

    sput-object v0, LF/e;->b:Lo/T;

    new-instance v0, LF/f;

    invoke-direct {v0}, LF/f;-><init>()V

    sput-object v0, LF/e;->c:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(LF/g;Ljava/util/Set;)V
    .locals 3

    invoke-direct {p0, p2}, LF/i;-><init>(Ljava/util/Set;)V

    new-instance v0, Lh/p;

    invoke-direct {v0}, Lh/p;-><init>()V

    iput-object v0, p0, LF/e;->f:Lh/p;

    new-instance v0, LE/r;

    iget v1, p1, LF/g;->a:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/e;->d:LE/o;

    new-instance v0, LE/c;

    iget v1, p1, LF/g;->a:I

    invoke-direct {v0, v1}, LE/c;-><init>(I)V

    iput-object v0, p0, LF/e;->e:LE/a;

    new-instance v0, LE/f;

    iget v1, p1, LF/g;->c:I

    iget v2, p1, LF/g;->b:I

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/e;->g:LE/d;

    new-instance v0, LE/f;

    iget v1, p1, LF/g;->d:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/e;->h:LE/d;

    new-instance v0, LE/d;

    iget v1, p1, LF/g;->c:I

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    iput-object v0, p0, LF/e;->i:LE/e;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->j:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->k:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->l:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->m:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->n:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->o:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->p:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->q:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->r:Lo/T;

    return-void
.end method

.method private static final a(I)I
    .locals 4

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 v2, p0, 0xff

    add-int/lit16 v0, v0, 0x2fd

    shr-int/lit8 v0, v0, 0x2

    add-int/lit16 v1, v1, 0x2fd

    shr-int/lit8 v1, v1, 0x2

    add-int/lit16 v2, v2, 0x2fd

    shr-int/lit8 v2, v2, 0x2

    const/high16 v3, -0x1000000

    and-int/2addr v3, p0

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    return v0
.end method

.method private static a(II)I
    .locals 2

    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const v1, 0xffffff

    and-int/2addr v1, p0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method private static a(ILo/T;)I
    .locals 6

    const/high16 v0, -0x1000000

    and-int v1, p0, v0

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v2, v0, 0xff

    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 v3, v0, 0xff

    and-int/lit16 v4, p0, 0xff

    sget-object v0, LF/e;->b:Lo/T;

    invoke-static {p1, v0}, Lo/T;->b(Lo/T;Lo/T;)F

    move-result v0

    invoke-virtual {p1}, Lo/T;->i()F

    move-result v5

    div-float/2addr v0, v5

    float-to-int v0, v0

    if-gez v0, :cond_0

    neg-int v0, v0

    :cond_0
    mul-int/lit16 v0, v0, 0x4ccc

    shr-int/lit8 v0, v0, 0x10

    const v5, 0xb333

    add-int/2addr v0, v5

    mul-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    mul-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x10

    mul-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x10

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, v3, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/e;
    .locals 11

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    invoke-virtual {v0}, Ln/s;->j()Ln/e;

    move-result-object v0

    move-object v2, v0

    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    const/16 v0, 0x80

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, LF/g;

    invoke-direct {v5}, LF/g;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v1

    instance-of v0, v1, Lo/g;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lo/g;

    invoke-static {v0, v5}, LF/e;->a(Lo/g;LF/g;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    sget-object v0, LF/e;->c:Ljava/util/Comparator;

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Lo/T;->a(D)D

    move-result-wide v2

    double-to-float v2, v2

    new-instance v3, LF/e;

    invoke-direct {v3, v5, v6}, LF/e;-><init>(LF/g;Ljava/util/Set;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/g;

    invoke-direct {v3, p0, v1, v0, v2}, LF/e;->a(Lo/aq;Lo/ad;Lo/g;F)V

    goto :goto_2

    :cond_1
    invoke-interface {v1}, Lo/n;->l()[I

    move-result-object v7

    array-length v8, v7

    move v1, v3

    :goto_3
    if-ge v1, v8, :cond_3

    aget v9, v7, v1

    if-ltz v9, :cond_2

    array-length v10, p1

    if-ge v9, v10, :cond_2

    aget-object v9, p1, v9

    invoke-virtual {v6, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    invoke-virtual {v0}, Lo/g;->d()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lo/g;->a()Lo/o;

    move-result-object v1

    invoke-interface {v2, v1}, Ln/e;->a(Lo/o;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    const/4 v1, 0x1

    :goto_4
    if-nez v1, :cond_5

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_4

    :cond_7
    invoke-direct {v3}, LF/e;->c()V

    invoke-virtual {v3, p0}, LF/e;->a(Lo/aq;)V

    return-object v3

    :cond_8
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public static a(LD/a;I)V
    .locals 4

    const/high16 v3, 0x10000

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, LD/a;->u()V

    invoke-virtual {p0}, LD/a;->v()V

    invoke-interface {v0, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glPolygonOffsetx(II)V

    const/16 v1, 0xb

    if-ne p1, v1, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x201

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0xc

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, LD/a;->n()V

    const/16 v1, 0x203

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-interface {v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    goto :goto_0
.end method

.method private a(Lo/T;Lo/T;Lo/T;Lo/T;II)V
    .locals 5

    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p1, p5}, LE/o;->a(Lo/T;I)V

    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p3, p5}, LE/o;->a(Lo/T;I)V

    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p2, p5}, LE/o;->a(Lo/T;I)V

    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p4, p5}, LE/o;->a(Lo/T;I)V

    iget-object v1, p0, LF/e;->i:LE/e;

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v4, v0, 0x2

    invoke-interface {v1, v2, v0, v3, v4}, LE/e;->a(IIII)V

    iget-object v1, p0, LF/e;->h:LE/d;

    add-int/lit8 v2, v0, 0x1

    int-to-short v2, v2

    add-int/lit8 v0, v0, 0x3

    int-to-short v0, v0

    invoke-virtual {v1, v2, v0}, LE/d;->a(SS)V

    iget-object v0, p0, LF/e;->r:Lo/T;

    invoke-static {p2, p1, v0}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    iget-object v0, p0, LF/e;->e:LE/a;

    iget-object v1, p0, LF/e;->r:Lo/T;

    invoke-static {p6, v1}, LF/e;->a(ILo/T;)I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, LE/a;->b(II)V

    return-void
.end method

.method private a(Lo/aq;Lo/ad;Lo/g;F)V
    .locals 14

    invoke-virtual/range {p3 .. p3}, Lo/g;->e()Lo/aj;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Lo/g;->b()Lo/aF;

    move-result-object v10

    invoke-virtual {v10}, Lo/aF;->a()I

    move-result v11

    invoke-virtual {v1}, Lo/aj;->c()I

    move-result v2

    if-eqz v11, :cond_0

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lo/ad;->d()Lo/T;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lo/ad;->g()I

    move-result v6

    invoke-virtual/range {p3 .. p3}, Lo/g;->f()I

    move-result v3

    invoke-virtual/range {p3 .. p3}, Lo/g;->g()I

    move-result v13

    iget-object v4, p0, LF/e;->p:Lo/T;

    const/4 v5, 0x0

    const/4 v7, 0x0

    int-to-float v3, v3

    mul-float v3, v3, p4

    float-to-int v3, v3

    invoke-virtual {v4, v5, v7, v3}, Lo/T;->a(III)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lo/aj;->a(I)I

    move-result v3

    const/16 v4, 0xa0

    invoke-static {v3, v4}, LF/e;->a(II)I

    move-result v7

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lo/aj;->a(I)I

    move-result v1

    const/16 v2, 0xa0

    invoke-static {v1, v2}, LF/e;->a(II)I

    move-result v1

    move v8, v1

    :goto_0
    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v11, :cond_0

    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->k:Lo/T;

    iget-object v4, p0, LF/e;->l:Lo/T;

    invoke-virtual {v10, v9, v2, v3, v4}, Lo/aF;->a(ILo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->j:Lo/T;

    invoke-static {v2, v12, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->k:Lo/T;

    invoke-static {v2, v12, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->l:Lo/T;

    invoke-static {v2, v12, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    if-eqz v13, :cond_2

    iget-object v2, p0, LF/e;->q:Lo/T;

    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v5, v13

    mul-float v5, v5, p4

    float-to-int v5, v5

    invoke-virtual {v2, v3, v4, v5}, Lo/T;->a(III)V

    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->q:Lo/T;

    iget-object v4, p0, LF/e;->j:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->q:Lo/T;

    iget-object v4, p0, LF/e;->k:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->q:Lo/T;

    iget-object v4, p0, LF/e;->l:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    :cond_2
    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->p:Lo/T;

    iget-object v4, p0, LF/e;->m:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->p:Lo/T;

    iget-object v4, p0, LF/e;->n:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->p:Lo/T;

    iget-object v4, p0, LF/e;->o:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    iget-object v2, p0, LF/e;->d:LE/o;

    iget-object v3, p0, LF/e;->m:Lo/T;

    invoke-virtual {v2, v3, v6}, LE/o;->a(Lo/T;I)V

    iget-object v2, p0, LF/e;->d:LE/o;

    iget-object v3, p0, LF/e;->n:Lo/T;

    invoke-virtual {v2, v3, v6}, LE/o;->a(Lo/T;I)V

    iget-object v2, p0, LF/e;->d:LE/o;

    iget-object v3, p0, LF/e;->o:Lo/T;

    invoke-virtual {v2, v3, v6}, LE/o;->a(Lo/T;I)V

    iget-object v2, p0, LF/e;->e:LE/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v8, v3}, LE/a;->b(II)V

    iget-object v2, p0, LF/e;->g:LE/d;

    int-to-short v3, v1

    add-int/lit8 v4, v1, 0x1

    int-to-short v4, v4

    add-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v2, v3, v4, v1}, LE/d;->a(SSS)V

    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lo/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->k:Lo/T;

    iget-object v4, p0, LF/e;->m:Lo/T;

    iget-object v5, p0, LF/e;->n:Lo/T;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LF/e;->a(Lo/T;Lo/T;Lo/T;Lo/T;II)V

    :cond_3
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lo/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->l:Lo/T;

    iget-object v4, p0, LF/e;->n:Lo/T;

    iget-object v5, p0, LF/e;->o:Lo/T;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LF/e;->a(Lo/T;Lo/T;Lo/T;Lo/T;II)V

    :cond_4
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lo/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->j:Lo/T;

    iget-object v4, p0, LF/e;->o:Lo/T;

    iget-object v5, p0, LF/e;->m:Lo/T;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LF/e;->a(Lo/T;Lo/T;Lo/T;Lo/T;II)V

    :cond_5
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto/16 :goto_1

    :cond_6
    invoke-static {v7}, LF/e;->a(I)I

    move-result v1

    move v8, v1

    goto/16 :goto_0
.end method

.method static a(Lo/g;LF/g;)Z
    .locals 4

    invoke-virtual {p0}, Lo/g;->b()Lo/aF;

    move-result-object v0

    invoke-virtual {v0}, Lo/aF;->a()I

    move-result v0

    invoke-virtual {p0}, Lo/g;->c()I

    move-result v1

    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v2, v1, 0x4

    iget v3, p1, LF/g;->a:I

    add-int/2addr v3, v0

    add-int/2addr v2, v3

    const/16 v3, 0x4000

    if-le v2, v3, :cond_0

    iget v3, p1, LF/g;->a:I

    if-lez v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput v2, p1, LF/g;->a:I

    iget v2, p1, LF/g;->b:I

    add-int/2addr v0, v2

    iput v0, p1, LF/g;->b:I

    iget v0, p1, LF/g;->c:I

    mul-int/lit8 v2, v1, 0x6

    add-int/2addr v0, v2

    iput v0, p1, LF/g;->c:I

    iget v0, p1, LF/g;->d:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p1, LF/g;->d:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c()V
    .locals 4

    iget-object v1, p0, LF/e;->g:LE/d;

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    const/4 v2, 0x0

    iget-object v3, p0, LF/e;->i:LE/e;

    invoke-interface {v3}, LE/e;->b()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LE/d;->a(LE/d;II)V

    const/4 v1, 0x0

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    invoke-virtual {v0, v1}, LE/d;->a(LD/a;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/e;->e:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->g:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->h:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    invoke-virtual {v0}, LE/d;->c()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/e;->e:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    iget-object v0, p0, LF/e;->g:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    iget-object v0, p0, LF/e;->h:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 5

    const/4 v4, 0x4

    const/high16 v1, 0x10000

    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/e;->e:LE/a;

    invoke-virtual {v0, p1}, LE/a;->c(LD/a;)V

    iget-object v0, p0, LF/e;->f:Lh/p;

    if-eqz v0, :cond_3

    iget-object v0, p0, LF/e;->f:Lh/p;

    invoke-virtual {v0, p1}, Lh/p;->a(LD/a;)I

    move-result v0

    if-ne v0, v1, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, LF/e;->f:Lh/p;

    :goto_1
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_4

    iget-object v0, p0, LF/e;->g:LE/d;

    invoke-virtual {v0, p1, v4}, LE/d;->a(LD/a;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LF/e;->g:LE/d;

    invoke-virtual {v2, p1, v4}, LE/d;->a(LD/a;I)V

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LF/e;->h:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    goto :goto_0
.end method

.method a(Lo/aq;)V
    .locals 0

    return-void
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x160

    iget-object v1, p0, LF/e;->e:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->g:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->h:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    invoke-virtual {v0}, LE/d;->d()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/e;->e:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    iget-object v0, p0, LF/e;->g:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, LF/e;->h:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    return-void
.end method
