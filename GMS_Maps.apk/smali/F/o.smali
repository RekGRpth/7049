.class public LF/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/h;


# instance fields
.field private a:LF/s;

.field private final b:LF/v;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:LE/q;

.field private final f:LE/k;

.field private final g:Lx/j;

.field private final h:LE/g;

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:Z

.field private n:Z


# direct methods
.method constructor <init>(Ljava/util/ArrayList;LF/s;LF/v;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LF/o;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    new-instance v0, Lx/j;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lx/j;-><init>(IIZ)V

    iput-object v0, p0, LF/o;->g:Lx/j;

    new-instance v0, LE/g;

    iget-object v1, p0, LF/o;->g:Lx/j;

    invoke-virtual {v1}, Lx/j;->d()I

    move-result v1

    invoke-direct {v0, v1}, LE/g;-><init>(I)V

    iput-object v0, p0, LF/o;->h:LE/g;

    iget-object v0, p0, LF/o;->g:Lx/j;

    iput-object v0, p0, LF/o;->e:LE/q;

    iget-object v0, p0, LF/o;->g:Lx/j;

    iput-object v0, p0, LF/o;->f:LE/k;

    iput-object p2, p0, LF/o;->a:LF/s;

    iput-object p3, p0, LF/o;->b:LF/v;

    invoke-direct {p0}, LF/o;->d()V

    iput-boolean v3, p0, LF/o;->m:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LF/o;->n:Z

    return-void
.end method

.method public static a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;
    .locals 10

    const/4 v8, 0x0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    move v6, v0

    move-object v7, v1

    :goto_0
    invoke-virtual {p0}, Lo/H;->b()I

    move-result v0

    if-ge v6, v0, :cond_8

    invoke-virtual {p0, v6}, Lo/H;->a(I)Lo/I;

    move-result-object v1

    invoke-virtual {v1}, Lo/I;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lo/I;->j()Lo/aj;

    move-result-object v4

    invoke-virtual {v1}, Lo/I;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4, p5, v0, v4, p2}, LF/o;->a(Lcom/google/android/maps/driveabout/vector/aV;LG/a;Ljava/lang/String;Lo/aj;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v5, p5, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    instance-of v0, p1, Lo/af;

    if-eqz v0, :cond_2

    iget-object v5, p5, LG/a;->a:Lcom/google/android/maps/driveabout/vector/aX;

    :cond_0
    :goto_1
    new-instance v0, LF/t;

    invoke-virtual {v1}, Lo/I;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, LC/a;->m()F

    move-result v1

    invoke-static {v4, p5, v1}, LF/L;->a(Lo/aj;LG/a;F)I

    move-result v3

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v1, v7

    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object v7, v1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lo/K;

    if-eqz v0, :cond_0

    iget-object v5, p5, LG/a;->h:Lcom/google/android/maps/driveabout/vector/aX;

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lo/I;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    invoke-virtual {v1}, Lo/I;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v8}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v0

    invoke-virtual {v0}, Lv/a;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lv/a;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1}, Lo/I;->h()F

    move-result v0

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->x()I

    invoke-virtual {v1}, Lo/I;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/road_shields/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p5, LG/a;->m:F

    mul-float/2addr v0, v1

    :goto_3
    invoke-virtual {p2}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    new-instance v1, LF/q;

    invoke-direct {v1, v2, v0, p3}, LF/q;-><init>(Landroid/graphics/Bitmap;FLD/c;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_2

    :cond_4
    iget v1, p5, LG/a;->n:F

    mul-float/2addr v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v8

    :goto_4
    return-object v0

    :cond_6
    invoke-virtual {v1}, Lo/I;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, LF/r;

    invoke-virtual {v1}, Lo/I;->k()F

    move-result v1

    invoke-direct {v0, v1}, LF/r;-><init>(F)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Lo/I;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_2

    :cond_8
    new-instance v0, LF/o;

    invoke-virtual {p0}, Lo/H;->c()Lo/b;

    move-result-object v1

    invoke-virtual {v1}, Lo/b;->a()I

    move-result v1

    invoke-static {v1}, LF/s;->a(I)LF/s;

    move-result-object v1

    invoke-virtual {p0}, Lo/H;->c()Lo/b;

    move-result-object v2

    invoke-virtual {v2}, Lo/b;->b()I

    move-result v2

    invoke-static {v2}, LF/v;->a(I)LF/v;

    move-result-object v2

    invoke-direct {v0, v9, v1, v2}, LF/o;-><init>(Ljava/util/ArrayList;LF/s;LF/v;)V

    goto :goto_4

    :cond_9
    move-object v1, v7

    goto/16 :goto_2
.end method

.method static a(Lcom/google/android/maps/driveabout/vector/aV;LG/a;Ljava/lang/String;Lo/aj;LC/a;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lo/aj;->e()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p3}, Lo/aj;->h()Lo/ao;

    move-result-object v1

    invoke-virtual {v1}, Lo/ao;->d()I

    move-result v1

    invoke-virtual {p3}, Lo/aj;->h()Lo/ao;

    move-result-object v2

    invoke-virtual {v2}, Lo/ao;->f()I

    move-result v2

    if-lez v2, :cond_0

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/q;)Z
    .locals 4

    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    instance-of v3, v0, LF/r;

    if-nez v3, :cond_2

    invoke-interface {v0, p1}, LF/u;->a(Lcom/google/android/maps/driveabout/vector/q;)LD/b;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_4
    iget-object v3, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_2
.end method

.method private c(LD/a;)V
    .locals 13

    iget-object v0, p0, LF/o;->g:Lx/j;

    invoke-virtual {v0}, Lx/j;->f()V

    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->b(LD/a;)V

    iget-object v0, p0, LF/o;->g:Lx/j;

    iget-object v1, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Lx/j;->a(I)V

    const/4 v2, 0x0

    iget v0, p0, LF/o;->j:F

    iget v1, p0, LF/o;->k:F

    sub-float v1, v0, v1

    const/4 v0, 0x0

    move v3, v1

    move v4, v2

    move v2, v0

    :goto_0
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v5

    move v5, v1

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/u;

    invoke-interface {v1}, LF/u;->e()F

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-interface {v1}, LF/u;->a()F

    move-result v1

    add-float/2addr v1, v5

    move v5, v1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    iget-object v7, p0, LF/o;->a:LF/s;

    sget-object v8, LF/s;->a:LF/s;

    if-ne v7, v8, :cond_2

    iget v1, p0, LF/o;->i:F

    sub-float/2addr v1, v5

    const/high16 v5, 0x40000000

    div-float/2addr v1, v5

    :cond_1
    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v1

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    instance-of v1, v0, LF/r;

    if-eqz v1, :cond_3

    invoke-interface {v0}, LF/u;->a()F

    move-result v0

    add-float v1, v5, v0

    move v5, v1

    goto :goto_3

    :cond_2
    iget-object v7, p0, LF/o;->a:LF/s;

    sget-object v8, LF/s;->c:LF/s;

    if-ne v7, v8, :cond_1

    iget v1, p0, LF/o;->i:F

    sub-float/2addr v1, v5

    goto :goto_2

    :cond_3
    invoke-interface {v0}, LF/u;->a()F

    move-result v7

    invoke-interface {v0}, LF/u;->b()F

    move-result v9

    iget-object v1, p0, LF/o;->b:LF/v;

    sget-object v10, LF/v;->a:LF/v;

    if-ne v1, v10, :cond_4

    invoke-interface {v0}, LF/u;->e()F

    move-result v1

    sub-float v1, v6, v1

    const/high16 v10, 0x40000000

    div-float/2addr v1, v10

    sub-float v1, v3, v1

    :goto_4
    invoke-interface {v0}, LF/u;->c()F

    move-result v10

    add-float/2addr v1, v10

    iget-object v10, p0, LF/o;->e:LE/q;

    const/4 v11, 0x0

    sub-float v12, v1, v9

    invoke-interface {v10, v5, v11, v12}, LE/q;->a(FFF)V

    iget-object v10, p0, LF/o;->e:LE/q;

    add-float v11, v5, v7

    const/4 v12, 0x0

    sub-float v9, v1, v9

    invoke-interface {v10, v11, v12, v9}, LE/q;->a(FFF)V

    iget-object v9, p0, LF/o;->e:LE/q;

    add-float/2addr v7, v5

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10, v1}, LE/q;->a(FFF)V

    iget-object v7, p0, LF/o;->e:LE/q;

    const/4 v9, 0x0

    invoke-interface {v7, v5, v9, v1}, LE/q;->a(FFF)V

    iget-object v1, p0, LF/o;->d:Ljava/util/ArrayList;

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LD/b;

    invoke-virtual {v1}, LD/b;->b()F

    move-result v4

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    iget-object v9, p0, LF/o;->f:LE/k;

    const/4 v10, 0x0

    invoke-interface {v9, v10, v1}, LE/k;->a(FF)V

    iget-object v9, p0, LF/o;->f:LE/k;

    invoke-interface {v9, v4, v1}, LE/k;->a(FF)V

    iget-object v1, p0, LF/o;->f:LE/k;

    const/4 v9, 0x0

    invoke-interface {v1, v4, v9}, LE/k;->a(FF)V

    iget-object v1, p0, LF/o;->f:LE/k;

    const/4 v4, 0x0

    const/4 v9, 0x0

    invoke-interface {v1, v4, v9}, LE/k;->a(FF)V

    invoke-interface {v0}, LF/u;->a()F

    move-result v0

    add-float v1, v5, v0

    move v5, v1

    move v4, v7

    goto/16 :goto_3

    :cond_4
    iget-object v1, p0, LF/o;->b:LF/v;

    sget-object v10, LF/v;->c:LF/v;

    if-ne v1, v10, :cond_7

    invoke-interface {v0}, LF/u;->e()F

    move-result v1

    sub-float v1, v6, v1

    sub-float v1, v3, v1

    goto :goto_4

    :cond_5
    sub-float v1, v3, v6

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, LF/o;->n:Z

    iget-object v0, p0, LF/o;->g:Lx/j;

    invoke-virtual {v0}, Lx/j;->c()V

    iget-object v0, p0, LF/o;->h:LE/g;

    iget-object v1, p0, LF/o;->g:Lx/j;

    invoke-virtual {v1}, Lx/j;->e()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, LE/g;->a(Ljava/nio/ByteBuffer;)V

    return-void

    :cond_7
    move v1, v3

    goto/16 :goto_4
.end method

.method private d()V
    .locals 10

    const/4 v2, 0x0

    const/high16 v9, 0x40000000

    const/4 v4, 0x0

    iput v4, p0, LF/o;->i:F

    move v1, v2

    move v3, v4

    :goto_0
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v4

    move v6, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    invoke-interface {v0}, LF/u;->a()F

    move-result v8

    add-float/2addr v6, v8

    invoke-interface {v0}, LF/u;->e()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v5, v0

    goto :goto_1

    :cond_0
    iget v0, p0, LF/o;->i:F

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/o;->i:F

    add-float/2addr v3, v5

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, LF/o;->c:Ljava/util/ArrayList;

    iget-object v2, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput v4, p0, LF/o;->k:F

    iput v4, p0, LF/o;->l:F

    sget-object v2, LF/p;->a:[I

    iget-object v5, p0, LF/o;->b:LF/v;

    invoke-virtual {v5}, LF/v;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_2
    iget v0, p0, LF/o;->k:F

    add-float/2addr v0, v3

    iget v1, p0, LF/o;->l:F

    add-float/2addr v0, v1

    iput v0, p0, LF/o;->j:F

    return-void

    :pswitch_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    iget v5, p0, LF/o;->k:F

    invoke-interface {v0}, LF/u;->c()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/o;->k:F

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    invoke-interface {v0}, LF/u;->e()F

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-interface {v0}, LF/u;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_4

    :cond_4
    cmpl-float v0, v4, v1

    if-lez v0, :cond_2

    sub-float v0, v4, v1

    iput v0, p0, LF/o;->l:F

    goto :goto_2

    :pswitch_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v4

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    invoke-interface {v0}, LF/u;->e()F

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-interface {v0}, LF/u;->c()F

    move-result v0

    add-float/2addr v0, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_5

    :cond_5
    cmpl-float v0, v4, v2

    if-lez v0, :cond_6

    sub-float v0, v4, v2

    iput v0, p0, LF/o;->k:F

    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    iget v2, p0, LF/o;->l:F

    invoke-interface {v0}, LF/u;->d()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/o;->l:F

    goto :goto_6

    :pswitch_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v4

    move v5, v4

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    invoke-interface {v0}, LF/u;->e()F

    move-result v7

    div-float/2addr v7, v9

    invoke-static {v5, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-interface {v0}, LF/u;->c()F

    move-result v0

    add-float/2addr v0, v7

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v2, v0

    goto :goto_7

    :cond_7
    cmpl-float v0, v2, v5

    if-lez v0, :cond_8

    sub-float v0, v2, v5

    iput v0, p0, LF/o;->k:F

    :cond_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    invoke-interface {v0}, LF/u;->e()F

    move-result v5

    div-float/2addr v5, v9

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-interface {v0}, LF/u;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_8

    :cond_9
    cmpl-float v0, v4, v1

    if-lez v0, :cond_2

    sub-float v0, v4, v1

    iput v0, p0, LF/o;->l:F

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, LF/o;->i:F

    return v0
.end method

.method public a(LD/a;)V
    .locals 4

    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/u;

    invoke-interface {v1}, LF/u;->f()V

    goto :goto_2

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->d(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 5

    iget-boolean v0, p0, LF/o;->m:Z

    if-nez v0, :cond_1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LF/o;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v0, p0, LF/o;->n:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, LF/o;->c(LD/a;)V

    :cond_2
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0}, LE/g;->a()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->a(LD/a;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    invoke-virtual {v0, v2}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x6

    mul-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(LF/s;)V
    .locals 1

    iget-object v0, p0, LF/o;->a:LF/s;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LF/o;->n:Z

    :cond_0
    iput-object p1, p0, LF/o;->a:LF/s;

    return-void
.end method

.method a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z
    .locals 5

    const/4 v1, 0x1

    invoke-direct {p0, p2}, LF/o;->a(Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x2710

    invoke-virtual {p1, v0}, LD/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty on initialize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    instance-of v4, v0, LF/r;

    if-nez v4, :cond_3

    iget-object v4, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-interface {v0, p1, p2}, LF/u;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)LD/b;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1}, LF/o;->c(LD/a;)V

    iput-boolean v1, p0, LF/o;->m:Z

    move v0, v1

    goto :goto_0
.end method

.method public b()F
    .locals 1

    iget v0, p0, LF/o;->j:F

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    invoke-virtual {p0, p1}, LF/o;->a(LD/a;)V

    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->c(LD/a;)V

    return-void
.end method

.method public c()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LF/o;->a()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LF/o;->b()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
