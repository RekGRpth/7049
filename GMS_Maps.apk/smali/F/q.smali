.class public LF/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/u;


# instance fields
.field private final a:Landroid/graphics/Bitmap;

.field private final b:LD/c;

.field private final c:I

.field private final d:I

.field private e:Z


# direct methods
.method constructor <init>(Landroid/graphics/Bitmap;FLD/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, LF/q;->b:LD/c;

    iget-object v0, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, LF/q;->c:I

    iget-object v0, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, LF/q;->d:I

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, LF/q;->c:I

    int-to-float v0, v0

    return v0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, LF/q;->b:LD/c;

    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/c;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    if-nez v0, :cond_1

    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iget-boolean v1, p0, LF/q;->e:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, LD/b;->e(Z)V

    :cond_0
    invoke-virtual {v0, v2}, LD/b;->c(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LD/b;->d(Z)V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/google/googlenav/android/E;->f:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/b;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v1, p0, LF/q;->b:LD/c;

    iget-object v2, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0}, LD/c;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v0}, LD/b;->f()V

    return-object v0

    :cond_2
    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/b;->b(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .locals 2

    iget-object v0, p0, LF/q;->b:LD/c;

    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/c;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LD/b;->f()V

    :cond_0
    return-object v0
.end method

.method public b()F
    .locals 1

    iget v0, p0, LF/q;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public c()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()F
    .locals 1

    iget v0, p0, LF/q;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public f()V
    .locals 1

    iget-boolean v0, p0, LF/q;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method
