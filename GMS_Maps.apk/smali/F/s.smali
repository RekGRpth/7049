.class public final enum LF/s;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LF/s;

.field public static final enum b:LF/s;

.field public static final enum c:LF/s;

.field private static final synthetic d:[LF/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LF/s;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, LF/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/s;->a:LF/s;

    new-instance v0, LF/s;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, LF/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/s;->b:LF/s;

    new-instance v0, LF/s;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LF/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/s;->c:LF/s;

    const/4 v0, 0x3

    new-array v0, v0, [LF/s;

    sget-object v1, LF/s;->a:LF/s;

    aput-object v1, v0, v2

    sget-object v1, LF/s;->b:LF/s;

    aput-object v1, v0, v3

    sget-object v1, LF/s;->c:LF/s;

    aput-object v1, v0, v4

    sput-object v0, LF/s;->d:[LF/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)LF/s;
    .locals 2

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown justification"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, LF/s;->a:LF/s;

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LF/s;->b:LF/s;

    goto :goto_0

    :pswitch_2
    sget-object v0, LF/s;->c:LF/s;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LF/s;
    .locals 1

    const-class v0, LF/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LF/s;

    return-object v0
.end method

.method public static values()[LF/s;
    .locals 1

    sget-object v0, LF/s;->d:[LF/s;

    invoke-virtual {v0}, [LF/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LF/s;

    return-object v0
.end method
