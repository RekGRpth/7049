.class LI/d;
.super Lcom/google/android/location/internal/a;
.source "SourceFile"


# instance fields
.field final synthetic a:LI/c;


# direct methods
.method constructor <init>(LI/c;)V
    .locals 0

    iput-object p1, p0, LI/d;->a:LI/c;

    invoke-direct {p0}, Lcom/google/android/location/internal/a;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, LI/d;->a:LI/c;

    invoke-static {v0}, LI/c;->a(LI/c;)LI/a;

    move-result-object v0

    invoke-virtual {v0, p1}, LI/a;->a(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled()V
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, LI/d;->a:LI/c;

    invoke-static {v0}, LI/c;->a(LI/c;)LI/a;

    move-result-object v0

    invoke-virtual {v0}, LI/a;->b()V

    return-void
.end method

.method public onProviderEnabled()V
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, LI/d;->a:LI/c;

    invoke-static {v0}, LI/c;->a(LI/c;)LI/a;

    move-result-object v0

    invoke-virtual {v0}, LI/a;->a()V

    return-void
.end method

.method public onStatusChanged(ILandroid/os/Bundle;)V
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, LI/d;->a:LI/c;

    invoke-static {v0}, LI/c;->a(LI/c;)LI/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LI/a;->a(ILandroid/os/Bundle;)V

    return-void
.end method
