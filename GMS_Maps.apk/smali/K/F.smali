.class public LK/F;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/C;


# static fields
.field private static a:LK/F;


# instance fields
.field private final b:Law/p;

.field private final c:Ljava/util/Map;

.field private final d:Landroid/content/Context;

.field private e:LK/D;

.field private f:LK/I;

.field private g:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Law/p;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LK/F;->c:Ljava/util/Map;

    iput-object p1, p0, LK/F;->b:Law/p;

    iput-object p2, p0, LK/F;->d:Landroid/content/Context;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LK/F;->d:Landroid/content/Context;

    invoke-static {v1}, LJ/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LK/F;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LK/F;->g:Ljava/lang/String;

    invoke-direct {p0}, LK/F;->c()V

    invoke-direct {p0}, LK/F;->e()V

    return-void
.end method

.method public static a(Law/p;Landroid/content/Context;)LK/F;
    .locals 1

    sget-object v0, LK/F;->a:LK/F;

    if-nez v0, :cond_0

    new-instance v0, LK/F;

    invoke-direct {v0, p0, p1}, LK/F;-><init>(Law/p;Landroid/content/Context;)V

    sput-object v0, LK/F;->a:LK/F;

    :cond_0
    sget-object v0, LK/F;->a:LK/F;

    return-object v0
.end method

.method static synthetic a(LK/F;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LK/F;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LK/F;LK/I;)V
    .locals 0

    invoke-direct {p0, p1}, LK/F;->b(LK/I;)V

    return-void
.end method

.method static synthetic a(LK/F;LK/I;LK/G;[B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, LK/F;->a(LK/I;LK/G;[B)V

    return-void
.end method

.method private declared-synchronized a(LK/G;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LK/F;->c:Ljava/util/Map;

    invoke-virtual {p1}, LK/G;->i()LK/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/G;

    iget-object v1, p0, LK/F;->c:Ljava/util/Map;

    invoke-virtual {p1}, LK/G;->i()LK/H;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LK/G;->b()J

    move-result-wide v1

    invoke-virtual {p1}, LK/G;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, LK/F;->b(LK/G;)V

    :cond_0
    invoke-direct {p0}, LK/F;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LK/I;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LK/I;->a(LK/I;)I

    move-result v0

    invoke-direct {p0, v0}, LK/F;->b(I)I

    move-result v0

    invoke-static {p1}, LK/I;->a(LK/I;)I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, LK/F;->b(LK/I;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {p1}, LK/I;->b(LK/I;)Ljava/util/Locale;

    move-result-object v1

    iget-object v2, p0, LK/F;->e:LK/D;

    invoke-virtual {p0, v1, v0, v2}, LK/F;->a(Ljava/util/Locale;ILK/D;)LK/w;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LK/F;->e:LK/D;

    if-eqz v1, :cond_0

    iget-object v1, p0, LK/F;->e:LK/D;

    invoke-interface {v1, v0}, LK/D;->a(LK/w;)V

    const/4 v0, 0x0

    iput-object v0, p0, LK/F;->e:LK/D;

    const/4 v0, 0x0

    iput-object v0, p0, LK/F;->f:LK/I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LK/I;LK/G;[B)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, LK/F;->c(LK/G;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, p3}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v1}, LK/w;->a(Ljava/io/File;)LK/w;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    iget-object v1, p0, LK/F;->f:LK/I;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, LK/F;->e:LK/D;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, LK/F;->a(LK/w;)V

    iget-object v1, p0, LK/F;->e:LK/D;

    invoke-interface {v1, v0}, LK/D;->a(LK/w;)V

    const/4 v0, 0x0

    iput-object v0, p0, LK/F;->e:LK/D;

    const/4 v0, 0x0

    iput-object v0, p0, LK/F;->f:LK/I;

    :cond_0
    invoke-direct {p0, p2}, LK/F;->a(LK/G;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot save bundle to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, p1}, LK/F;->b(LK/I;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot save bundle to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-direct {p0, p1}, LK/F;->b(LK/I;)V

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot parse bundle "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, LK/G;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-direct {p0, p1}, LK/F;->b(LK/I;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method private b(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(LK/F;)V
    .locals 0

    invoke-direct {p0}, LK/F;->d()V

    return-void
.end method

.method static synthetic b(LK/F;LK/I;)V
    .locals 0

    invoke-direct {p0, p1}, LK/F;->a(LK/I;)V

    return-void
.end method

.method private b(LK/G;)V
    .locals 5

    invoke-virtual {p1}, LK/G;->e()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    new-instance v3, Ljava/io/File;

    aget-object v4, v2, v0

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private declared-synchronized b(LK/I;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LK/F;->f:LK/I;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LK/F;->e:LK/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/F;->e:LK/D;

    invoke-interface {v0}, LK/D;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, LK/F;->e:LK/D;

    const/4 v0, 0x0

    iput-object v0, p0, LK/F;->f:LK/I;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(LK/G;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, LK/G;->e()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p1}, LK/G;->f()I

    move-result v2

    invoke-virtual {p0, v2}, LK/F;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private c()V
    .locals 5

    iget-object v0, p0, LK/F;->d:Landroid/content/Context;

    invoke-virtual {p0}, LK/F;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v0, v1, v2}, LR/s;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    iget-object v3, p0, LK/F;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LK/G;->a(Ljava/lang/String;Ljava/lang/String;)LK/G;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, LK/F;->c:Ljava/util/Map;

    invoke-virtual {v2}, LK/G;->i()LK/H;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private declared-synchronized d()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LK/F;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/G;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, LK/G;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LK/F;->d:Landroid/content/Context;

    invoke-virtual {p0}, LK/F;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, LK/F;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/G;

    invoke-virtual {v0}, LK/G;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, LK/F;->b(LK/G;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, LK/F;->d()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/util/Locale;ILK/D;)LK/w;
    .locals 6

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LK/F;->c:Ljava/util/Map;

    new-instance v1, LK/H;

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, LK/H;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/G;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    :try_start_1
    invoke-direct {p0, v0}, LK/F;->c(LK/G;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, LK/w;->a(Ljava/io/File;)LK/w;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :try_start_2
    invoke-virtual {p0, v2}, LK/F;->a(LK/w;)V

    invoke-virtual {v0}, LK/G;->g()V

    invoke-direct {p0}, LK/F;->d()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, LK/G;->c()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-nez v2, :cond_2

    :cond_1
    new-instance v1, LK/I;

    if-eqz v2, :cond_3

    :goto_1
    invoke-direct {v1, p0, p1, p2, v0}, LK/I;-><init>(LK/F;Ljava/util/Locale;ILK/G;)V

    iput-object v1, p0, LK/F;->f:LK/I;

    iput-object p3, p0, LK/F;->e:LK/D;

    iget-object v0, p0, LK/F;->b:Law/p;

    iget-object v1, p0, LK/F;->f:LK/I;

    invoke-interface {v0, v1}, Law/p;->c(Law/g;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    monitor-exit p0

    return-object v2

    :catch_0
    move-exception v1

    move-object v2, v3

    :goto_2
    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to parse speech bundle: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v0, v3

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2

    :cond_4
    move-object v2, v3

    goto :goto_0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    const-string v0, "/voice/"

    return-object v0
.end method

.method protected a(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string v0, "voice_instructions.zip"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "voice_instructions_imperial.zip"

    goto :goto_0

    :pswitch_1
    const-string v0, "voice_instructions_yards.zip"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(LK/w;)V
    .locals 0

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string v0, "VoiceBundles"

    return-object v0
.end method
