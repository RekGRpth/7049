.class public LK/N;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/T;


# instance fields
.field private final a:LK/T;


# direct methods
.method public constructor <init>(LK/T;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LK/N;->a:LK/T;

    return-void
.end method


# virtual methods
.method public a(LK/V;)LK/a;
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, LK/W;

    if-eqz v1, :cond_1

    check-cast p1, LK/W;

    iget-object v1, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->e()LK/V;

    move-result-object v2

    invoke-interface {v1, v2}, LK/T;->a(LK/V;)LK/a;

    move-result-object v1

    iget-object v2, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->f()LK/V;

    move-result-object v3

    invoke-interface {v2, v3}, LK/T;->a(LK/V;)LK/a;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    new-instance v0, LK/K;

    const/4 v3, 0x2

    new-array v3, v3, [LK/a;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    invoke-direct {v0, v3}, LK/K;-><init>([LK/a;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0, p1}, LK/T;->a(LK/V;)LK/a;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0}, LK/T;->a()V

    return-void
.end method

.method public a(LK/V;LK/U;)V
    .locals 3

    instance-of v0, p1, LK/W;

    if-eqz v0, :cond_0

    check-cast p1, LK/W;

    new-instance v0, LK/O;

    invoke-direct {v0, p1, p2}, LK/O;-><init>(LK/W;LK/U;)V

    iget-object v1, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->e()LK/V;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LK/T;->a(LK/V;LK/U;)V

    iget-object v1, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->f()LK/V;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LK/T;->a(LK/V;LK/U;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0, p1, p2}, LK/T;->a(LK/V;LK/U;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0}, LK/T;->b()V

    return-void
.end method
