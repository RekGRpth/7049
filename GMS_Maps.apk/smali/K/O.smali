.class LK/O;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/U;


# instance fields
.field private a:LK/W;

.field private b:LK/U;

.field private c:LK/a;

.field private d:LK/a;


# direct methods
.method public constructor <init>(LK/W;LK/U;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LK/O;->a:LK/W;

    iput-object p2, p0, LK/O;->b:LK/U;

    return-void
.end method

.method private a(LK/a;)V
    .locals 2

    iget-object v0, p0, LK/O;->b:LK/U;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/O;->b:LK/U;

    iget-object v1, p0, LK/O;->a:LK/W;

    invoke-interface {v0, v1, p1}, LK/U;->a(LK/V;LK/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, LK/O;->b:LK/U;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(LK/V;LK/a;)V
    .locals 4

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LK/O;->a(LK/a;)V

    :cond_0
    iget-object v0, p0, LK/O;->a:LK/W;

    invoke-virtual {v0}, LK/W;->e()LK/V;

    move-result-object v0

    if-ne p1, v0, :cond_3

    iput-object p2, p0, LK/O;->c:LK/a;

    :cond_1
    :goto_0
    iget-object v0, p0, LK/O;->b:LK/U;

    if-eqz v0, :cond_2

    iget-object v0, p0, LK/O;->c:LK/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, LK/O;->d:LK/a;

    if-eqz v0, :cond_2

    new-instance v0, LK/K;

    const/4 v1, 0x2

    new-array v1, v1, [LK/a;

    const/4 v2, 0x0

    iget-object v3, p0, LK/O;->c:LK/a;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LK/O;->d:LK/a;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, LK/K;-><init>([LK/a;)V

    invoke-direct {p0, v0}, LK/O;->a(LK/a;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, LK/O;->a:LK/W;

    invoke-virtual {v0}, LK/W;->f()LK/V;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iput-object p2, p0, LK/O;->d:LK/a;

    goto :goto_0
.end method
