.class public LK/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/c;


# static fields
.field public static final a:[J

.field public static final b:[J

.field public static final c:[J

.field public static final d:[J

.field public static final e:[J


# instance fields
.field private final f:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x6

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    sput-object v0, LK/ah;->a:[J

    new-array v0, v1, [J

    fill-array-data v0, :array_1

    sput-object v0, LK/ah;->b:[J

    new-array v0, v1, [J

    fill-array-data v0, :array_2

    sput-object v0, LK/ah;->c:[J

    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_3

    sput-object v0, LK/ah;->d:[J

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_4

    sput-object v0, LK/ah;->e:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0xc8
        0x12c
        0xc8
        0x12c
        0x1f4
    .end array-data

    :array_1
    .array-data 8
        0x0
        0x1f4
        0x12c
        0xc8
        0x12c
        0xc8
    .end array-data

    :array_2
    .array-data 8
        0x0
        0xc8
        0x12c
        0xc8
        0x12c
        0xc8
    .end array-data

    :array_3
    .array-data 8
        0x0
        0x5dc
        0x1f4
        0x5dc
    .end array-data

    :array_4
    .array-data 8
        0x0
        0x1f4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, LK/ah;->f:Landroid/os/Vibrator;

    return-void
.end method

.method public static a(ILO/N;)[J
    .locals 1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    sget-object v0, LK/ah;->d:[J

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LO/N;->b()I

    move-result v0

    invoke-static {v0}, LO/N;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LO/N;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, LK/ah;->c:[J

    goto :goto_0

    :pswitch_0
    sget-object v0, LK/ah;->a:[J

    goto :goto_0

    :pswitch_1
    sget-object v0, LK/ah;->b:[J

    goto :goto_0

    :cond_1
    sget-object v0, LK/ah;->e:[J

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(LK/V;)LK/a;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, LK/V;->c()I

    move-result v2

    invoke-virtual {p1}, LK/V;->d()LO/j;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LK/V;->d()LO/j;

    move-result-object v0

    invoke-virtual {v0}, LO/j;->e()LO/N;

    move-result-object v0

    :goto_0
    invoke-static {v2, v0}, LK/ah;->a(ILO/N;)[J

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, LK/ag;

    iget-object v2, p0, LK/ah;->f:Landroid/os/Vibrator;

    invoke-direct {v1, v2, v0}, LK/ag;-><init>(Landroid/os/Vibrator;[J)V

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method
