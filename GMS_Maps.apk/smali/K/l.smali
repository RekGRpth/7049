.class LK/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:LK/d;

.field private final b:LK/V;

.field private c:LK/U;

.field private d:Ljava/lang/String;

.field private e:Ljava/io/File;


# direct methods
.method public constructor <init>(LK/d;LK/V;LK/U;)V
    .locals 0

    iput-object p1, p0, LK/l;->a:LK/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, LK/l;->b:LK/V;

    iput-object p3, p0, LK/l;->c:LK/U;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LK/l;->a(LK/a;)V

    iget-object v0, p0, LK/l;->a:LK/d;

    invoke-static {v0}, LK/d;->a(LK/d;)LK/i;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LK/l;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/l;->a:LK/d;

    invoke-static {v0}, LK/d;->a(LK/d;)LK/i;

    move-result-object v0

    iget-object v0, v0, LK/i;->a:Ljava/util/ArrayList;

    iget-object v2, p0, LK/l;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, LK/l;->d:Ljava/lang/String;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(LK/a;)V
    .locals 2

    iget-object v0, p0, LK/l;->c:LK/U;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/l;->c:LK/U;

    iget-object v1, p0, LK/l;->b:LK/V;

    invoke-interface {v0, v1, p1}, LK/U;->a(LK/V;LK/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, LK/l;->c:LK/U;

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, LK/l;->d:Ljava/lang/String;

    iput-object p2, p0, LK/l;->e:Ljava/io/File;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LK/l;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LK/l;->b:LK/V;

    invoke-virtual {v0}, LK/V;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()LK/f;
    .locals 5

    iget-object v0, p0, LK/l;->e:Ljava/io/File;

    invoke-static {v0}, LK/d;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LK/f;

    iget-object v1, p0, LK/l;->a:LK/d;

    invoke-static {v1}, LK/d;->b(LK/d;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LK/l;->d:Ljava/lang/String;

    iget-object v3, p0, LK/l;->e:Ljava/io/File;

    iget-object v4, p0, LK/l;->a:LK/d;

    invoke-static {v4}, LK/d;->c(LK/d;)Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LK/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Landroid/os/Handler;)V

    goto :goto_0
.end method
