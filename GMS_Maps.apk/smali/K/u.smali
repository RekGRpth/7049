.class public LK/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/T;


# instance fields
.field private final a:LK/C;

.field private final b:Landroid/os/Handler;

.field private c:LK/w;

.field private d:Landroid/content/Context;

.field private e:I

.field private final f:LK/D;


# direct methods
.method public constructor <init>(Landroid/content/Context;LK/C;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LK/v;

    invoke-direct {v0, p0}, LK/v;-><init>(LK/u;)V

    iput-object v0, p0, LK/u;->f:LK/D;

    iput-object p1, p0, LK/u;->d:Landroid/content/Context;

    iput-object p2, p0, LK/u;->a:LK/C;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LK/u;->b:Landroid/os/Handler;

    const/4 v0, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const-string v2, "US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "GB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "LR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x2

    :cond_1
    invoke-direct {p0, v0}, LK/u;->a(I)Z

    return-void
.end method

.method private a(LK/m;)LK/a;
    .locals 9

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    instance-of v0, p1, LK/r;

    if-eqz v0, :cond_1

    check-cast p1, LK/r;

    invoke-virtual {p1}, LK/r;->b()LK/m;

    move-result-object v0

    invoke-direct {p0, v0}, LK/u;->a(LK/m;)LK/a;

    move-result-object v4

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    instance-of v0, p1, LK/q;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, LK/q;

    iget v2, p0, LK/u;->e:I

    invoke-virtual {v0}, LK/q;->c()I

    move-result v5

    if-eq v2, v5, :cond_2

    invoke-virtual {v0}, LK/q;->c()I

    move-result v0

    invoke-direct {p0, v0}, LK/u;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    instance-of v0, p1, LK/o;

    if-eqz v0, :cond_c

    check-cast p1, LK/o;

    invoke-virtual {p1}, LK/o;->b()[LK/m;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    move v2, v1

    :goto_1
    array-length v5, v6

    if-ge v0, v5, :cond_8

    aget-object v5, v6, v0

    invoke-direct {p0, v5}, LK/u;->a(LK/m;)LK/a;

    move-result-object v5

    aget-object v8, v6, v0

    invoke-virtual {v8}, LK/m;->a()Z

    move-result v8

    if-eqz v8, :cond_5

    if-eqz v5, :cond_3

    instance-of v8, v5, LK/Y;

    if-eqz v8, :cond_5

    :cond_3
    instance-of v5, v5, LK/Y;

    if-eqz v5, :cond_4

    move v2, v3

    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    if-nez v5, :cond_6

    const-string v1, "CannedSpeechAlertGenerator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No voice instruction defined for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v0, v6, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    instance-of v8, v5, LK/Y;

    if-eqz v8, :cond_7

    move-object v4, v5

    goto :goto_0

    :cond_7
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_a

    if-eqz v2, :cond_9

    new-instance v0, LK/Y;

    invoke-direct {v0}, LK/Y;-><init>()V

    :goto_3
    move-object v4, v0

    goto :goto_0

    :cond_9
    move-object v0, v4

    goto :goto_3

    :cond_a
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_b

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/a;

    move-object v4, v0

    goto/16 :goto_0

    :cond_b
    new-instance v4, LK/K;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LK/a;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LK/a;

    invoke-direct {v4, v0}, LK/K;-><init>([LK/a;)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, LK/u;->c:LK/w;

    invoke-virtual {v0, p1}, LK/w;->a(LK/m;)LK/A;

    move-result-object v0

    invoke-virtual {v0}, LK/A;->d()Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v4, LK/Y;

    invoke-direct {v4}, LK/Y;-><init>()V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v0}, LK/A;->e()Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, p0, LK/u;->d:Landroid/content/Context;

    invoke-virtual {v0}, LK/A;->c()Ljava/io/File;

    move-result-object v0

    iget-object v2, p0, LK/u;->b:Landroid/os/Handler;

    invoke-static {v1, v0, v2}, LK/P;->a(Landroid/content/Context;Ljava/io/File;Landroid/os/Handler;)LK/a;

    move-result-object v4

    goto/16 :goto_0

    :cond_e
    const-string v0, "CannedSpeechAlertGenerator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find sound for a message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic a(LK/u;LK/w;)V
    .locals 0

    invoke-direct {p0, p1}, LK/u;->a(LK/w;)V

    return-void
.end method

.method private declared-synchronized a(LK/w;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LK/u;->c:LK/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)Z
    .locals 3

    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, LK/u;->c:LK/w;

    :goto_0
    iget-object v0, p0, LK/u;->c:LK/w;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, LK/u;->a:LK/C;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    iget-object v2, p0, LK/u;->f:LK/D;

    invoke-interface {v0, v1, p1, v2}, LK/C;->a(Ljava/util/Locale;ILK/D;)LK/w;

    move-result-object v0

    iput-object v0, p0, LK/u;->c:LK/w;

    iput p1, p0, LK/u;->e:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized a(LK/V;)LK/a;
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LK/u;->c:LK/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, LK/V;->b()LK/m;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, LK/u;->a(LK/m;)LK/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(LK/V;LK/U;)V
    .locals 1

    invoke-virtual {p0, p1}, LK/u;->a(LK/V;)LK/a;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-interface {p2, p1, v0}, LK/U;->a(LK/V;LK/a;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method
