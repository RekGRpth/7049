.class public LL/G;
.super LR/c;
.source "SourceFile"


# instance fields
.field private final a:LM/z;

.field private b:Ljava/util/List;

.field private c:Ljava/util/Iterator;

.field private final d:Ljava/util/List;

.field private e:LL/b;

.field private f:LL/H;

.field private g:J

.field private h:J

.field private i:Ll/j;

.field private volatile j:F

.field private volatile k:Z

.field private volatile l:Z

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    const-string v0, "EventLogPlayerThread"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LL/G;->d:Ljava/util/List;

    iput-wide v2, p0, LL/G;->g:J

    iput-wide v2, p0, LL/G;->h:J

    const/high16 v0, 0x3f800000

    iput v0, p0, LL/G;->j:F

    const/4 v0, 0x1

    iput-boolean v0, p0, LL/G;->k:Z

    iput-boolean v1, p0, LL/G;->l:Z

    iput-boolean v1, p0, LL/G;->m:Z

    new-instance v0, LM/z;

    invoke-direct {v0}, LM/z;-><init>()V

    iput-object v0, p0, LL/G;->a:LM/z;

    invoke-virtual {p0}, LL/G;->start()V

    return-void
.end method

.method private a(Ll/j;)Z
    .locals 1

    iget-object v0, p0, LL/G;->a:LM/z;

    invoke-virtual {v0, p1}, LM/z;->a(Ll/j;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LL/G;->a:LM/z;

    invoke-virtual {v0, p1}, LM/z;->b(Ll/j;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LL/G;->b(Ll/j;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LL/G;->d(Ll/j;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LL/G;->e(Ll/j;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LL/G;->f(Ll/j;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LL/G;->g(Ll/j;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ll/j;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    instance-of v2, p1, LL/v;

    if-eqz v2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    invoke-virtual {v2, v0}, Law/h;->b(Z)V

    move v0, v1

    :cond_0
    return v0

    :cond_1
    instance-of v2, p1, LL/w;

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LL/G;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LL/G;->c:Ljava/util/Iterator;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LL/G;->h:J

    iput-wide v0, p0, LL/G;->g:J

    const/4 v0, 0x0

    iput-object v0, p0, LL/G;->i:Ll/j;

    invoke-direct {p0}, LL/G;->d()Ll/j;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Ll/j;)Z
    .locals 1

    instance-of v0, p1, LL/C;

    if-nez v0, :cond_0

    instance-of v0, p1, LL/E;

    if-nez v0, :cond_0

    instance-of v0, p1, LL/F;

    if-nez v0, :cond_0

    instance-of v0, p1, LL/n;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized d()Ll/j;
    .locals 7

    const/4 v0, 0x0

    const/high16 v6, 0x3f000000

    const-wide/16 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LL/G;->c:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LL/G;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll/j;

    :cond_0
    if-eqz v0, :cond_8

    iput-object v0, p0, LL/G;->i:Ll/j;

    iget-object v1, p0, LL/G;->b:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_2

    move-wide v0, v2

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v0

    iput-wide v4, p0, LL/G;->g:J

    iget-object v4, p0, LL/G;->i:Ll/j;

    invoke-virtual {v4}, Ll/j;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v0, p0, LL/G;->i:Ll/j;

    invoke-virtual {v0}, Ll/j;->c()J

    move-result-wide v0

    iput-wide v0, p0, LL/G;->h:J

    :cond_1
    :goto_1
    iget-object v0, p0, LL/G;->i:Ll/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    iget-object v0, p0, LL/G;->i:Ll/j;

    invoke-virtual {v0}, Ll/j;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LL/G;->i:Ll/j;

    invoke-virtual {v0}, Ll/j;->c()J

    move-result-wide v0

    iget-wide v4, p0, LL/G;->h:J

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-gez v4, :cond_3

    move-wide v0, v2

    :cond_3
    long-to-float v0, v0

    iget v1, p0, LL/G;->j:F

    div-float/2addr v0, v1

    add-float/2addr v0, v6

    float-to-long v0, v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, LL/G;->i:Ll/j;

    invoke-virtual {v0}, Ll/j;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LL/G;->i:Ll/j;

    invoke-virtual {v0}, Ll/j;->e()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-gez v4, :cond_5

    move-wide v0, v2

    :cond_5
    long-to-float v0, v0

    iget v1, p0, LL/G;->j:F

    div-float/2addr v0, v1

    add-float/2addr v0, v6

    float-to-long v0, v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, LL/G;->i:Ll/j;

    invoke-direct {p0, v0}, LL/G;->c(Ll/j;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-wide/16 v0, 0x64

    goto :goto_0

    :cond_7
    iget-object v4, p0, LL/G;->i:Ll/j;

    invoke-virtual {v4}, Ll/j;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-wide v4, p0, LL/G;->h:J

    cmp-long v2, v4, v2

    if-ltz v2, :cond_1

    iget-wide v2, p0, LL/G;->h:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LL/G;->h:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LL/G;->i:Ll/j;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LL/G;->g:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_9
    move-wide v0, v2

    goto :goto_0
.end method

.method private d(Ll/j;)Z
    .locals 3

    const/4 v1, 0x1

    instance-of v0, p1, LL/C;

    if-eqz v0, :cond_1

    check-cast p1, LL/C;

    iget-object v0, p0, LL/G;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LL/P;

    invoke-interface {v0, p1}, LL/P;->a(LL/C;)V

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_1
    return v0

    :cond_1
    instance-of v0, p1, LL/E;

    if-eqz v0, :cond_3

    check-cast p1, LL/E;

    iget-object v0, p0, LL/G;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LL/P;

    invoke-interface {v0, p1}, LL/P;->a(LL/E;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    instance-of v0, p1, LL/F;

    if-eqz v0, :cond_5

    check-cast p1, LL/F;

    iget-object v0, p0, LL/G;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LL/P;

    invoke-interface {v0, p1}, LL/P;->a(LL/F;)V

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    instance-of v0, p1, LL/n;

    if-eqz v0, :cond_7

    check-cast p1, LL/n;

    iget-object v0, p0, LL/G;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LL/P;

    invoke-interface {v0, p1}, LL/P;->a(LL/n;)V

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e(Ll/j;)Z
    .locals 1

    instance-of v0, p1, LL/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, LL/G;->e:LL/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LL/G;->e:LL/b;

    check-cast p1, LL/m;

    invoke-virtual {v0, p1}, LL/b;->a(LL/m;)Z

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Ll/j;)Z
    .locals 2

    instance-of v0, p1, LL/y;

    if-eqz v0, :cond_0

    :try_start_0
    check-cast p1, LL/y;

    invoke-virtual {p1}, LL/y;->f()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private g(Ll/j;)Z
    .locals 4

    instance-of v0, p1, LL/A;

    if-eqz v0, :cond_0

    check-cast p1, LL/A;

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {p1}, LL/A;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-class v2, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LM/z;
    .locals 1

    iget-object v0, p0, LL/G;->a:LM/z;

    return-object v0
.end method

.method public declared-synchronized a(LL/H;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LL/G;->f:LL/H;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LL/P;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LL/G;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LL/G;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LL/b;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LL/G;->e:LL/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/List;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LL/G;->b:Ljava/util/List;

    invoke-direct {p0}, LL/G;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ll/f;)V
    .locals 3

    invoke-virtual {p1}, Ll/f;->a()Ljava/util/List;

    move-result-object v0

    instance-of v1, p1, LL/O;

    if-eqz v1, :cond_0

    check-cast p1, LL/O;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p1}, LL/O;->j()I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {p0, v0}, LL/G;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LL/G;->k:Z

    return-void
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LL/G;->m:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()V
    .locals 6

    const-wide/16 v4, 0x0

    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v0, p0, LL/G;->m:Z

    if-nez v0, :cond_4

    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    :try_start_1
    iget-boolean v0, p0, LL/G;->m:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, LL/G;->l:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, LL/G;->g:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LL/G;->g:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_7

    :cond_2
    iget-boolean v0, p0, LL/G;->l:Z

    if-nez v0, :cond_3

    iget-wide v0, p0, LL/G;->g:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_6

    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, LL/G;->m:Z

    iget-object v0, p0, LL/G;->f:LL/H;

    if-eqz v0, :cond_5

    iget-object v0, p0, LL/G;->f:LL/H;

    invoke-interface {v0}, LL/H;->a()V

    :cond_5
    return-void

    :cond_6
    :try_start_3
    iget-wide v0, p0, LL/G;->g:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, LL/G;->i:Ll/j;

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_8

    :try_start_4
    invoke-direct {p0, v0}, LL/G;->a(Ll/j;)Z

    :cond_8
    invoke-direct {p0}, LL/G;->d()Ll/j;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LL/G;->k:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, LL/G;->c()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0
.end method
