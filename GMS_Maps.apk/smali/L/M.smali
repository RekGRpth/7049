.class public LL/M;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/maps/driveabout/app/dO;

.field private final c:LQ/p;

.field private final d:Ljava/lang/Runnable;

.field private final e:Ljava/util/Random;

.field private final f:J

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NONE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BACK_TO_CAR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LIST_VIEW"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SATELLITE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ROUTE_OVERVIEW"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TRAFFIC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "STREET_VIEW"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ALTERNATE_ROUTES"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ROUTE_AROUND_TRAFFIC_OVERVIEW"

    aput-object v2, v0, v1

    sput-object v0, LL/M;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/app/dO;JII)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/dO;->a()LQ/p;

    move-result-object v1

    iput-object v1, p0, LL/M;->c:LQ/p;

    iput-wide p2, p0, LL/M;->f:J

    new-instance v1, Ljava/util/Random;

    iget-wide v2, p0, LL/M;->f:J

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v1, p0, LL/M;->e:Ljava/util/Random;

    new-instance v1, LL/N;

    invoke-direct {v1, p0, v0, p5}, LL/N;-><init>(LL/M;Landroid/os/Handler;I)V

    iput-object v1, p0, LL/M;->d:Ljava/lang/Runnable;

    iget-object v1, p0, LL/M;->d:Ljava/lang/Runnable;

    mul-int/lit16 v2, p4, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a()I
    .locals 7

    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x6

    iget-object v1, p0, LL/M;->c:LQ/p;

    invoke-virtual {v1}, LQ/p;->g()LQ/s;

    move-result-object v1

    invoke-virtual {v1}, LQ/s;->G()LQ/x;

    move-result-object v2

    instance-of v1, v1, LQ/E;

    if-nez v1, :cond_0

    iget-object v1, p0, LL/M;->e:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    const/high16 v3, 0x3f000000

    cmpg-float v1, v1, v3

    if-gez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    sget-object v1, LQ/x;->i:LQ/x;

    if-ne v2, v1, :cond_1

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    :goto_1
    iget-object v1, p0, LL/M;->e:Ljava/util/Random;

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget v0, v0, v1

    goto :goto_0

    :cond_1
    sget-object v1, LQ/x;->l:LQ/x;

    if-ne v2, v1, :cond_2

    new-array v0, v6, [I

    fill-array-data v0, :array_1

    goto :goto_1

    :cond_2
    sget-object v1, LQ/x;->k:LQ/x;

    if-ne v2, v1, :cond_3

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    goto :goto_1

    :cond_3
    sget-object v1, LQ/x;->m:LQ/x;

    if-ne v2, v1, :cond_4

    new-array v0, v6, [I

    fill-array-data v0, :array_3

    goto :goto_1

    :cond_4
    sget-object v1, LQ/x;->j:LQ/x;

    if-ne v2, v1, :cond_5

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    goto :goto_1

    :cond_5
    sget-object v1, LQ/x;->f:LQ/x;

    if-ne v2, v1, :cond_6

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    goto :goto_1

    :cond_6
    sget-object v1, LQ/x;->h:LQ/x;

    if-ne v2, v1, :cond_7

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    goto :goto_1

    :cond_7
    sget-object v1, LQ/x;->g:LQ/x;

    if-ne v2, v1, :cond_8

    new-array v0, v4, [I

    fill-array-data v0, :array_7

    goto :goto_1

    :cond_8
    new-array v0, v0, [I

    aput v5, v0, v5

    goto :goto_1

    :array_0
    .array-data 4
        0x2
        0x3
        0x4
        0x5
        0x6
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x3
        0x4
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x2
        0x3
        0x4
        0x5
        0x6
        0x8
    .end array-data

    :array_3
    .array-data 4
        0x2
        0x4
        0x8
    .end array-data

    :array_4
    .array-data 4
        0x2
        0x3
        0x4
        0x5
        0x8
    .end array-data

    :array_5
    .array-data 4
        0x2
        0x3
        0x7
        0x8
    .end array-data

    :array_6
    .array-data 4
        0x4
        0x8
    .end array-data

    :array_7
    .array-data 4
        0x2
        0x3
        0x7
        0x4
        0x5
        0x6
    .end array-data
.end method

.method static synthetic a(LL/M;)I
    .locals 1

    invoke-direct {p0}, LL/M;->a()I

    move-result v0

    return v0
.end method

.method private a(I)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->h()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->e()V

    goto :goto_0

    :pswitch_2
    iget-boolean v2, p0, LL/M;->g:Z

    if-nez v2, :cond_0

    :goto_1
    iput-boolean v0, p0, LL/M;->g:Z

    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    iget-boolean v1, p0, LL/M;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->c(Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->j()V

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, LL/M;->c:LQ/p;

    invoke-virtual {v2}, LQ/p;->g()LQ/s;

    move-result-object v2

    iget-object v3, p0, LL/M;->c:LQ/p;

    sget-object v4, LQ/x;->l:LQ/x;

    invoke-virtual {v3, v4}, LQ/p;->b(LQ/x;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_2
    invoke-virtual {v2, v0}, LQ/s;->d(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->f()V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, LL/M;->c:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->C()V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->k()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic a(LL/M;I)V
    .locals 0

    invoke-direct {p0, p1}, LL/M;->a(I)V

    return-void
.end method

.method static synthetic b(LL/M;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, LL/M;->d:Ljava/lang/Runnable;

    return-object v0
.end method
