.class public LL/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LL/k;

.field private final c:Lcom/google/android/maps/driveabout/app/NavigationActivity;

.field private final d:Lcom/google/android/maps/driveabout/app/cS;

.field private final e:Lcom/google/android/maps/driveabout/app/aQ;

.field private final f:Ljava/lang/StringBuilder;

.field private g:I

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LL/k;Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LL/b;->f:Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    iput-boolean v0, p0, LL/b;->i:Z

    iput-object p1, p0, LL/b;->a:Ljava/lang/String;

    iput-object p2, p0, LL/b;->b:LL/k;

    iput-object p3, p0, LL/b;->c:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    iput-object p4, p0, LL/b;->d:Lcom/google/android/maps/driveabout/app/cS;

    iput-object p5, p0, LL/b;->e:Lcom/google/android/maps/driveabout/app/aQ;

    return-void
.end method

.method private b(LL/m;)Z
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, LL/b;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {p1}, LL/m;->f()LL/a;

    move-result-object v0

    invoke-virtual {p1}, LL/m;->i()Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, LL/b;->b:LL/k;

    iget-object v3, p0, LL/b;->c:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    iget-object v4, p0, LL/b;->d:Lcom/google/android/maps/driveabout/app/cS;

    iget-object v5, p0, LL/b;->e:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v6, p0, LL/b;->f:Ljava/lang/StringBuilder;

    invoke-interface/range {v0 .. v6}, LL/a;->a(Ljava/util/HashMap;LL/k;Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;Ljava/lang/StringBuilder;)Z

    move-result v0

    invoke-virtual {p1}, LL/m;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v7

    goto :goto_0
.end method

.method private static c(LL/m;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    :try_start_0
    invoke-virtual {p0, v0}, LL/m;->a(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected a()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method protected a(J)V
    .locals 0

    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V

    return-void
.end method

.method public a(LL/m;)Z
    .locals 8

    const/4 v1, 0x0

    iget v0, p0, LL/b;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LL/b;->g:I

    iget-object v0, p0, LL/b;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {p0}, LL/b;->a()J

    move-result-wide v4

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, LL/m;->g()I

    move-result v0

    iget-boolean v6, p0, LL/b;->i:Z

    if-eqz v6, :cond_0

    mul-int/lit8 v0, v0, 0x3

    :cond_0
    :goto_0
    invoke-virtual {p0}, LL/b;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    int-to-long v6, v0

    cmp-long v6, v2, v6

    if-gez v6, :cond_2

    invoke-direct {p0, p1}, LL/b;->b(LL/m;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {p1}, LL/b;->c(LL/m;)Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LL/m;->g()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    const-string v1, " OVERTIME"

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, LL/m;->g()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_3
    return v0

    :cond_1
    const-wide/16 v6, 0xc8

    :try_start_1
    invoke-virtual {p0, v6, v7}, LL/b;->a(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, LL/b;->f:Ljava/lang/StringBuilder;

    const-string v4, "Interrupted!"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v4, p0, LL/b;->f:Ljava/lang/StringBuilder;

    const-string v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    const-string v1, ""

    goto :goto_2

    :cond_4
    iget v2, p0, LL/b;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LL/b;->h:I

    iput-boolean v1, p0, LL/b;->i:Z

    goto :goto_3
.end method

.method public b()V
    .locals 1

    iget v0, p0, LL/b;->h:I

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method
