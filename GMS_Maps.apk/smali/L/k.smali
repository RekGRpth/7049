.class public abstract LL/k;
.super Ll/f;
.source "SourceFile"


# instance fields
.field protected b:Ljava/lang/String;

.field private c:LM/b;

.field private d:Landroid/location/Location;

.field private e:Landroid/location/Location;

.field private f:Landroid/location/Location;

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Ll/f;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x3e8

    iput v0, p0, LL/k;->g:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LL/k;->h:J

    return-void
.end method

.method static synthetic a(LL/k;)J
    .locals 2

    iget-wide v0, p0, LL/k;->h:J

    return-wide v0
.end method

.method static synthetic a(LL/k;J)J
    .locals 0

    iput-wide p1, p0, LL/k;->h:J

    return-wide p1
.end method

.method static synthetic a(LL/k;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    iput-object p1, p0, LL/k;->d:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic b(LL/k;)I
    .locals 1

    iget v0, p0, LL/k;->g:I

    return v0
.end method

.method static synthetic c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()Landroid/location/Location;
    .locals 3

    invoke-virtual {p0}, LL/k;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll/j;

    instance-of v2, v0, LL/p;

    if-eqz v2, :cond_0

    check-cast v0, LL/p;

    invoke-virtual {v0}, LL/p;->f()Landroid/location/Location;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j()I
    .locals 3

    invoke-virtual {p0}, LL/k;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll/j;

    instance-of v2, v0, LL/A;

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/maps/driveabout/app/bn;

    check-cast v0, LL/A;

    invoke-virtual {v0}, LL/A;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/maps/driveabout/app/bn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/File;)V
    .locals 1

    iget-object v0, p0, LL/k;->d:Landroid/location/Location;

    iput-object v0, p0, LL/k;->f:Landroid/location/Location;

    invoke-direct {p0}, LL/k;->i()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, LL/k;->e:Landroid/location/Location;

    invoke-super {p0, p1}, Ll/f;->a(Ljava/io/File;)V

    return-void
.end method

.method protected b(Ljava/io/BufferedWriter;)V
    .locals 2

    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<destination uri=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/k;->b:Ljava/lang/String;

    invoke-static {v1}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    :cond_0
    const-string v0, "</event-log>\n"

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/io/File;)V
    .locals 7

    const-wide v3, 0x3eb0c6f7a0b5ed8dL

    const/4 v5, 0x0

    invoke-virtual {p0}, LL/k;->h()LO/U;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v5, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v5, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v1

    invoke-virtual {v1}, Lo/u;->a()I

    move-result v1

    int-to-double v1, v1

    mul-double/2addr v1, v3

    invoke-virtual {v5, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    invoke-virtual {v0}, Lo/u;->b()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v3

    invoke-virtual {v5, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    :cond_0
    new-instance v0, Ll/b;

    iget-object v1, p0, LL/k;->a:Landroid/content/Context;

    iget-object v3, p0, LL/k;->e:Landroid/location/Location;

    iget-object v4, p0, LL/k;->f:Landroid/location/Location;

    invoke-direct {p0}, LL/k;->j()I

    move-result v6

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Ll/b;-><init>(Landroid/content/Context;Ljava/io/File;Landroid/location/Location;Landroid/location/Location;Landroid/location/Location;I)V

    invoke-virtual {v0}, Ll/b;->a()V

    return-void
.end method

.method public f()LM/b;
    .locals 1

    iget-object v0, p0, LL/k;->c:LM/b;

    if-nez v0, :cond_0

    new-instance v0, LL/l;

    invoke-direct {v0, p0}, LL/l;-><init>(LL/k;)V

    iput-object v0, p0, LL/k;->c:LM/b;

    :cond_0
    iget-object v0, p0, LL/k;->c:LM/b;

    return-object v0
.end method

.method public g()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public h()LO/U;
    .locals 2

    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/maps/driveabout/app/bn;

    iget-object v1, p0, LL/k;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/bn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bn;->b()LO/U;

    move-result-object v0

    goto :goto_0
.end method
