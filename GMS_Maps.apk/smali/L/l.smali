.class LL/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LM/b;


# instance fields
.field final synthetic a:LL/k;


# direct methods
.method constructor <init>(LL/k;)V
    .locals 0

    iput-object p1, p0, LL/l;->a:LL/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LM/C;)V
    .locals 4

    invoke-virtual {p1}, LM/C;->b()J

    move-result-wide v0

    iget-object v2, p0, LL/l;->a:LL/k;

    invoke-static {v2}, LL/k;->a(LL/k;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, LL/l;->a:LL/k;

    invoke-static {v2}, LL/k;->b(LL/k;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, LL/l;->a:LL/k;

    new-instance v1, LL/x;

    invoke-direct {v1, p1}, LL/x;-><init>(LM/C;)V

    invoke-virtual {v0, v1}, LL/k;->a(Ll/j;)V

    iget-object v0, p0, LL/l;->a:LL/k;

    invoke-virtual {p1}, LM/C;->b()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, LL/k;->a(LL/k;J)J

    :cond_0
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    iget-object v0, p0, LL/l;->a:LL/k;

    new-instance v1, LL/p;

    invoke-direct {v1, p1}, LL/p;-><init>(Landroid/location/Location;)V

    invoke-virtual {v0, v1}, LL/k;->a(Ll/j;)V

    iget-object v0, p0, LL/l;->a:LL/k;

    invoke-static {v0, p1}, LL/k;->a(LL/k;Landroid/location/Location;)Landroid/location/Location;

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LL/l;->a:LL/k;

    new-instance v1, LL/r;

    invoke-direct {v1, p1}, LL/r;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LL/k;->a(Ll/j;)V

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LL/l;->a:LL/k;

    new-instance v1, LL/s;

    invoke-direct {v1, p1}, LL/s;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LL/k;->a(Ll/j;)V

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, LL/l;->a:LL/k;

    new-instance v1, LL/t;

    invoke-direct {v1, p1, p2, p3}, LL/t;-><init>(Ljava/lang/String;ILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, LL/k;->a(Ll/j;)V

    return-void
.end method
