.class public LM/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(LM/C;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, LM/C;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, LM/C;->a(Ljava/lang/String;)V

    iget-wide v0, p1, LM/C;->b:J

    invoke-virtual {p0, v0, v1}, LM/C;->a(J)V

    iget v0, p1, LM/C;->c:F

    invoke-virtual {p0, v0}, LM/C;->a(F)V

    iget v0, p1, LM/C;->d:F

    invoke-virtual {p0, v0}, LM/C;->b(F)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, LM/C;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p2, p3}, LM/C;->a(J)V

    invoke-virtual {p0, p4}, LM/C;->a(F)V

    invoke-virtual {p0, p5}, LM/C;->b(F)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LM/C;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(F)V
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/common/util/j;->a(F)F

    move-result v0

    iput v0, p0, LM/C;->c:F

    return-void
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, LM/C;->b:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LM/C;->a:Ljava/lang/String;

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, LM/C;->b:J

    return-wide v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, LM/C;->d:F

    return-void
.end method

.method public c()F
    .locals 1

    iget v0, p0, LM/C;->c:F

    return v0
.end method

.method public d()F
    .locals 1

    iget v0, p0, LM/C;->d:F

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[provider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LM/C;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/C;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " orientation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LM/C;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
