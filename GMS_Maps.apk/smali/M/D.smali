.class public LM/D;
.super LM/e;
.source "SourceFile"

# interfaces
.implements LM/c;


# instance fields
.field private c:LO/z;

.field private d:I


# direct methods
.method constructor <init>(LM/b;)V
    .locals 1

    const-string v0, "driveabout_polyline_snapping"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    const/4 v0, 0x2

    iput v0, p0, LM/D;->d:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "driveabout_polyline_snapping"

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, LM/D;->d:I

    return-void
.end method

.method public a(LO/z;)V
    .locals 0

    iput-object p1, p0, LM/D;->c:LO/z;

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 8

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    iget v1, p0, LM/D;->d:I

    invoke-virtual {v0, v1}, LaH/j;->b(I)LaH/j;

    move-result-object v1

    iget-object v0, p0, LM/D;->c:LO/z;

    if-nez v0, :cond_0

    invoke-virtual {v1}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Lo/T;->a(D)D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v4

    invoke-virtual {v4}, LR/m;->E()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    add-float/2addr v4, v5

    iget-object v5, p0, LM/D;->c:LO/z;

    float-to-double v6, v4

    mul-double/2addr v2, v6

    invoke-virtual {v5, v0, v2, v3}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    if-eqz v0, :cond_1

    const-wide/high16 v2, 0x3ff0000000000000L

    invoke-virtual {v1, v2, v3}, LaH/j;->b(D)LaH/j;

    move-result-object v2

    invoke-virtual {v0}, LO/D;->c()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v2, v3}, LaH/j;->b(F)LaH/j;

    move-result-object v2

    invoke-virtual {v2, v0}, LaH/j;->a(LO/H;)LaH/j;

    :goto_1
    invoke-virtual {v1}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LaH/j;->b(D)LaH/j;

    goto :goto_1
.end method
