.class public LM/E;
.super LM/e;
.source "SourceFile"


# instance fields
.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/googlenav/common/a;

.field private e:LaH/h;

.field private f:J

.field private final g:[F

.field private h:I

.field private i:LR/m;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LM/b;Landroid/os/Handler;Lcom/google/googlenav/common/a;)V
    .locals 2

    const-string v0, "da_tunnel_heartbeat"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    const/4 v0, 0x0

    iput v0, p0, LM/E;->h:I

    new-instance v0, LM/F;

    invoke-direct {v0, p0}, LM/F;-><init>(LM/E;)V

    iput-object v0, p0, LM/E;->j:Ljava/lang/Runnable;

    iput-object p2, p0, LM/E;->c:Landroid/os/Handler;

    iput-object p3, p0, LM/E;->d:Lcom/google/googlenav/common/a;

    const/16 v0, 0xa

    new-array v0, v0, [F

    iput-object v0, p0, LM/E;->g:[F

    iget-object v0, p0, LM/E;->g:[F

    const/high16 v1, -0x40800000

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    return-void
.end method

.method static a([F)F
    .locals 8

    const/4 v0, 0x0

    const/high16 v2, -0x40800000

    const/4 v1, 0x0

    array-length v4, p0

    move v3, v0

    move v7, v1

    move v1, v0

    move v0, v7

    :goto_0
    if-ge v3, v4, :cond_1

    aget v5, p0, v3

    cmpl-float v6, v5, v2

    if-eqz v6, :cond_0

    add-float/2addr v0, v5

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    if-ge v1, v3, :cond_2

    move v0, v2

    :goto_1
    return v0

    :cond_2
    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_1
.end method

.method static a(LaH/h;F)LaH/j;
    .locals 9

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000

    const/4 v1, 0x0

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Lo/T;->a(D)D

    move-result-wide v4

    double-to-float v0, v4

    mul-float v4, p1, v0

    invoke-virtual {p0}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    if-nez v0, :cond_0

    move-object v0, v3

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LO/D;->a()LO/z;

    move-result-object v5

    invoke-virtual {v5, v0}, LO/z;->a(LO/D;)D

    move-result-wide v6

    double-to-float v0, v6

    add-float/2addr v0, v4

    invoke-virtual {v5}, LO/z;->n()Lo/X;

    move-result-object v4

    float-to-double v6, v0

    invoke-virtual {v5, v6, v7}, LO/z;->a(D)I

    move-result v6

    if-ltz v6, :cond_1

    invoke-virtual {v4}, Lo/X;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-lt v6, v7, :cond_2

    :cond_1
    move-object v0, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v4, v6}, Lo/X;->a(I)Lo/T;

    move-result-object v3

    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v4, v7}, Lo/X;->a(I)Lo/T;

    move-result-object v4

    invoke-virtual {v5, v6}, LO/z;->b(I)D

    move-result-wide v5

    double-to-float v5, v5

    sub-float/2addr v0, v5

    invoke-virtual {v3, v4}, Lo/T;->c(Lo/T;)F

    move-result v5

    div-float/2addr v0, v5

    cmpg-float v5, v0, v1

    if-gez v5, :cond_4

    move v0, v1

    :cond_3
    :goto_1
    invoke-virtual {v3, v4, v0}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    invoke-virtual {v1, p0}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v1

    invoke-virtual {v0}, Lo/T;->b()D

    move-result-wide v5

    invoke-virtual {v0}, Lo/T;->d()D

    move-result-wide v7

    invoke-virtual {v1, v5, v6, v7, v8}, LaH/j;->a(DD)LaH/j;

    move-result-object v0

    invoke-static {v3, v4}, Lo/T;->a(Lo/T;Lo/T;)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, LaH/j;->b(F)LaH/j;

    move-result-object v0

    goto :goto_0

    :cond_4
    cmpl-float v1, v0, v2

    if-lez v1, :cond_3

    move v0, v2

    goto :goto_1
.end method

.method public static a(Landroid/location/Location;)Z
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p0, LaH/h;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p0, LaH/h;

    invoke-virtual {p0}, LaH/h;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LaH/h;->l()Lo/af;

    move-result-object v0

    invoke-virtual {v0}, Lo/af;->g()Z

    move-result v0

    goto :goto_0
.end method

.method private d()J
    .locals 4

    const-wide/16 v0, 0x7d0

    iget-object v2, p0, LM/E;->i:LR/m;

    invoke-virtual {v2}, LR/m;->l()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    iget-object v1, p0, LM/E;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method a(J)V
    .locals 2

    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    iget-object v1, p0, LM/E;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    iget-object v1, p0, LM/E;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iput-object v0, p0, LM/E;->i:LR/m;

    invoke-direct {p0}, LM/E;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LM/E;->a(J)V

    return-void
.end method

.method c()V
    .locals 6

    iget-object v0, p0, LM/E;->e:LaH/h;

    invoke-static {v0}, LM/E;->a(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LM/E;->g:[F

    invoke-static {v0}, LM/E;->a([F)F

    move-result v0

    const/high16 v1, -0x40800000

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x40000000

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_0

    iget-object v1, p0, LM/E;->g:[F

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([FF)V

    iget-object v1, p0, LM/E;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    const/4 v3, 0x0

    iget-wide v4, p0, LM/E;->f:J

    sub-long v4, v1, v4

    long-to-float v4, v4

    const v5, 0x3a83126f

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget-object v4, p0, LM/E;->e:LaH/h;

    invoke-static {v4, v3}, LM/E;->a(LaH/h;F)LaH/j;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v1, v2}, LaH/j;->a(J)LaH/j;

    move-result-object v1

    iget-object v2, p0, LM/E;->i:LR/m;

    invoke-virtual {v2}, LR/m;->n()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, LaH/j;->a(F)LaH/j;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/j;->c(F)LaH/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaH/j;->d(Z)LaH/j;

    invoke-virtual {v3}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 3

    instance-of v0, p1, LaH/h;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, LaH/h;

    invoke-virtual {p1}, LaH/h;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iput-object v0, p0, LM/E;->i:LR/m;

    invoke-direct {p0}, LM/E;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LM/E;->a(J)V

    iput-object p1, p0, LM/E;->e:LaH/h;

    iget-object v0, p0, LM/E;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, LM/E;->f:J

    iget-object v1, p0, LM/E;->g:[F

    iget v2, p0, LM/E;->h:I

    invoke-virtual {p1}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaH/h;->getSpeed()F

    move-result v0

    :goto_1
    aput v0, v1, v2

    iget v0, p0, LM/E;->h:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, LM/E;->h:I

    goto :goto_0

    :cond_2
    const/high16 v0, -0x40800000

    goto :goto_1
.end method
