.class public LM/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LM/b;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:LM/b;


# direct methods
.method protected constructor <init>(Ljava/lang/String;LM/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LM/e;->a:Ljava/lang/String;

    iput-object p2, p0, LM/e;->b:LM/b;

    return-void
.end method


# virtual methods
.method public a(LM/C;)V
    .locals 0

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    iget-object v1, p0, LM/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    move-result-object v0

    iget-object v1, p0, LM/e;->b:LM/b;

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-interface {v1, v0}, LM/b;->onLocationChanged(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
