.class public LM/f;
.super LM/e;
.source "SourceFile"


# instance fields
.field private c:J

.field private d:J

.field private volatile e:LM/t;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:LR/m;

.field private k:LM/v;

.field private final l:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>(LM/b;LM/v;Lcom/google/googlenav/common/a;)V
    .locals 2

    const-string v0, "driveabout_gps_fixup"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LM/f;->c:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LM/f;->d:J

    iput-object p2, p0, LM/f;->k:LM/v;

    iput-object p3, p0, LM/f;->l:Lcom/google/googlenav/common/a;

    return-void
.end method

.method private a(LaH/j;)V
    .locals 5

    const/4 v0, 0x0

    iget-boolean v1, p0, LM/f;->h:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, LaH/j;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v1

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, LM/f;->h:Z

    :cond_0
    iget-boolean v1, p0, LM/f;->h:Z

    if-nez v1, :cond_1

    invoke-virtual {p1}, LaH/j;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, LaH/j;->h()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    iget-object v2, p0, LM/f;->j:LR/m;

    invoke-virtual {v2}, LR/m;->n()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x41000000

    if-ltz v1, :cond_1

    cmpl-float v4, v2, v3

    if-lez v4, :cond_1

    packed-switch v1, :pswitch_data_0

    :goto_0
    sub-float v1, v2, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    :cond_1
    return-void

    :pswitch_0
    const/high16 v0, 0x3f800000

    goto :goto_0

    :pswitch_1
    const/high16 v0, 0x3f400000

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x3f000000

    goto :goto_0

    :pswitch_3
    const/high16 v0, 0x3e800000

    goto :goto_0

    :pswitch_4
    const/high16 v0, 0x3e000000

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(LaH/j;)V
    .locals 3

    invoke-virtual {p1}, LaH/j;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x40800000

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v1

    iget-object v2, p0, LM/f;->j:LR/m;

    invoke-virtual {v2}, LR/m;->o()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    :cond_0
    return-void
.end method

.method private c(LaH/j;)V
    .locals 2

    iget-boolean v0, p0, LM/f;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, LaH/j;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/j;->j()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LM/f;->g:Z

    :cond_0
    iget-boolean v0, p0, LM/f;->g:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, LaH/j;->a()LaH/j;

    :cond_1
    return-void
.end method

.method private d(LaH/j;)V
    .locals 2

    iget-boolean v0, p0, LM/f;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LM/f;->k:LM/v;

    invoke-interface {v0}, LM/v;->d()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    invoke-virtual {p1, v0}, LaH/j;->b(F)LaH/j;

    :cond_0
    return-void
.end method

.method private e(LaH/j;)V
    .locals 1

    iget-object v0, p0, LM/f;->e:LM/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/f;->e:LM/t;

    invoke-virtual {v0}, LM/t;->b()I

    move-result v0

    invoke-virtual {p1, v0}, LaH/j;->a(I)LaH/j;

    :cond_0
    return-void
.end method

.method private f(LaH/j;)Z
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, LaH/j;->g()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, LaH/j;->h()I

    move-result v2

    if-lt v2, v4, :cond_1

    iput-boolean v0, p0, LM/f;->f:Z

    :cond_1
    iget-boolean v3, p0, LM/f;->f:Z

    if-eqz v3, :cond_2

    if-ge v2, v4, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private g(LaH/j;)V
    .locals 2

    iget-boolean v0, p0, LM/f;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/j;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/j;->h()I

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->m()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->n()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, LM/f;->j:LR/m;

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    :cond_0
    return-void
.end method

.method private h(LaH/j;)V
    .locals 2

    iget-boolean v0, p0, LM/f;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->getAccuracy()F

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->n()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, LM/f;->j:LR/m;

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    :cond_0
    return-void
.end method

.method private i(LaH/j;)V
    .locals 2

    invoke-virtual {p1}, LaH/j;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/j;->l()F

    move-result v0

    const/high16 v1, 0x42c80000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p1}, LaH/j;->b()LaH/j;

    :cond_0
    return-void
.end method

.method private j(LaH/j;)V
    .locals 6

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->n()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    iget-object v0, p0, LM/f;->l:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v0

    iget-wide v2, p0, LM/f;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-wide v2, p0, LM/f;->c:J

    sub-long v2, v0, v2

    iget-object v4, p0, LM/f;->j:LR/m;

    invoke-virtual {v4}, LR/m;->k()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const-wide/16 v2, 0x1388

    add-long/2addr v2, v0

    iget-wide v4, p0, LM/f;->d:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LM/f;->d:J

    :cond_0
    iput-wide v0, p0, LM/f;->c:J

    iget-wide v2, p0, LM/f;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, LM/f;->j:LR/m;

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LM/f;->c:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LM/f;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, LM/f;->e:LM/t;

    const/4 v0, 0x0

    iput-boolean v0, p0, LM/f;->i:Z

    return-void
.end method

.method public a(LM/t;)V
    .locals 0

    iput-object p1, p0, LM/f;->e:LM/t;

    return-void
.end method

.method public a(LM/v;)V
    .locals 0

    iput-object p1, p0, LM/f;->k:LM/v;

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "driveabout_hmm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LM/E;->a(Landroid/location/Location;)Z

    move-result v0

    iput-boolean v0, p0, LM/f;->i:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iput-object v0, p0, LM/f;->j:LR/m;

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaH/j;->a(Z)LaH/j;

    move-result-object v0

    invoke-direct {p0, v0}, LM/f;->e(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->a(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->b(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->c(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->d(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->f(LaH/j;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, LM/f;->g(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->h(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->i(LaH/j;)V

    invoke-direct {p0, v0}, LM/f;->j(LaH/j;)V

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_0
.end method
