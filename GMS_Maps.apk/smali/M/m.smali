.class LM/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LM/b;


# direct methods
.method constructor <init>(Ljava/lang/String;LM/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LM/m;->a:Ljava/lang/String;

    iput-object p2, p0, LM/m;->b:LM/b;

    return-void
.end method

.method static synthetic a(LM/m;)LM/b;
    .locals 1

    iget-object v0, p0, LM/m;->b:LM/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LM/m;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Landroid/location/LocationListener;
    .locals 1

    iget-object v0, p0, LM/m;->b:LM/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, LM/m;

    if-eqz v1, :cond_0

    check-cast p1, LM/m;

    iget-object v1, p0, LM/m;->a:Ljava/lang/String;

    iget-object v2, p1, LM/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LM/m;->b:LM/b;

    iget-object v2, p1, LM/m;->b:LM/b;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, LM/m;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, LM/m;->b:LM/b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
