.class LM/r;
.super LM/e;
.source "SourceFile"


# instance fields
.field final synthetic c:LM/n;

.field private d:Z

.field private e:I

.field private f:Z

.field private g:LaH/h;


# direct methods
.method constructor <init>(LM/n;LM/b;)V
    .locals 3

    const/4 v1, 0x1

    iput-object p1, p0, LM/r;->c:LM/n;

    const-string v0, "driveabout_base_location"

    invoke-direct {p0, v0, p2}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    invoke-static {p1}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v2, "gps"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    const-string v2, "gps"

    invoke-interface {v0, v2}, LM/v;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LM/r;->d:Z

    iput v1, p0, LM/r;->e:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, LM/r;->c:LM/n;

    invoke-static {v0}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/r;->c:LM/n;

    invoke-static {v0}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, LM/v;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LM/r;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, LM/r;->f:Z

    return v0
.end method

.method public c()Z
    .locals 2

    iget-boolean v0, p0, LM/r;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, LM/r;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v2, "network"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, LM/r;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "driveabout_gps_fixup"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v1, p0, LM/r;->d:Z

    const/4 v0, 0x2

    iput v0, p0, LM/r;->e:I

    iget-boolean v0, p0, LM/r;->f:Z

    if-eqz v0, :cond_4

    invoke-static {}, LM/n;->b()F

    move-result v0

    :goto_1
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_5

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    iget-boolean v2, p0, LM/r;->f:Z

    if-nez v2, :cond_0

    :cond_2
    iput-boolean v0, p0, LM/r;->f:Z

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0, v1}, LaH/j;->a(Z)LaH/j;

    move-result-object v0

    iget-boolean v1, p0, LM/r;->f:Z

    invoke-virtual {v0, v1}, LaH/j;->b(Z)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    iput-object v0, p0, LM/r;->g:LaH/h;

    iget-object p1, p0, LM/r;->g:LaH/h;

    :cond_3
    invoke-super {p0, p1}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_0

    :cond_4
    invoke-static {}, LM/n;->a()F

    move-result v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LM/r;->d:Z

    iput-boolean v1, p0, LM/r;->f:Z

    :cond_0
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 1

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LM/r;->d:Z

    :cond_0
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 1

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput p2, p0, LM/r;->e:I

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LM/r;->f:Z

    :cond_0
    return-void
.end method
