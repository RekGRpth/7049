.class LM/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LM/v;


# instance fields
.field private final a:Landroid/location/LocationManager;

.field private b:Landroid/location/GpsStatus;

.field private final c:LM/w;

.field private final d:LaV/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LM/w;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LM/w;-><init>(LM/o;)V

    iput-object v0, p0, LM/x;->c:LM/w;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    new-instance v0, LM/y;

    invoke-direct {v0, p0}, LM/y;-><init>(LM/x;)V

    new-instance v1, LaV/a;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LaV/a;-><init>(LaV/e;Lcom/google/googlenav/common/a;)V

    invoke-virtual {v1, p1}, LaV/a;->a(Landroid/content/Context;)V

    iput-object v1, p0, LM/x;->d:LaV/h;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LM/o;)V
    .locals 0

    invoke-direct {p0, p1}, LM/x;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(LM/x;)Landroid/location/LocationManager;
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(LM/b;)V
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method public a(LM/s;)V
    .locals 1

    iget-object v0, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, p1}, LM/w;->a(LM/s;)V

    return-void
.end method

.method public a(Ljava/lang/String;JFLM/b;)V
    .locals 6

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/location/GpsStatus$Listener;)Z
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, LM/x;->d:LaV/h;

    iget-object v1, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, v1}, LaV/h;->b(LaV/i;)V

    return-void
.end method

.method public b(LM/b;)V
    .locals 2

    iget-object v0, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, p1}, LM/w;->a(LM/b;)V

    iget-object v0, p0, LM/x;->d:LaV/h;

    iget-object v1, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, v1}, LaV/h;->a(LaV/i;)V

    return-void
.end method

.method public b(Landroid/location/GpsStatus$Listener;)V
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LM/t;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, LM/x;->a:Landroid/location/LocationManager;

    iget-object v2, p0, LM/x;->b:Landroid/location/GpsStatus;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v1

    iput-object v1, p0, LM/x;->b:Landroid/location/GpsStatus;

    iget-object v1, p0, LM/x;->b:Landroid/location/GpsStatus;

    invoke-virtual {v1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, LM/t;

    invoke-direct {v0, v2, v1}, LM/t;-><init>(II)V

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public d()F
    .locals 1

    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->d()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->h()V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->i()V

    iget-object v0, p0, LM/x;->d:LaV/h;

    sget-object v1, LaV/j;->c:LaV/j;

    invoke-virtual {v0, v1}, LaV/h;->a(LaV/j;)V

    return-void
.end method
