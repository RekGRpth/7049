.class public LO/R;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/T;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:F

.field private final i:F

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/util/List;

.field private final o:Ljava/util/List;

.field private final p:Ljava/util/List;

.field private final q:Ljava/util/List;


# direct methods
.method public constructor <init>(Lo/T;IIIIIIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LO/R;->a:Lo/T;

    iput p2, p0, LO/R;->b:I

    iput p3, p0, LO/R;->c:I

    iput p4, p0, LO/R;->d:I

    iput p5, p0, LO/R;->e:I

    iput p6, p0, LO/R;->f:I

    iput p7, p0, LO/R;->g:I

    iput p8, p0, LO/R;->h:F

    iput p9, p0, LO/R;->i:F

    iput-object p10, p0, LO/R;->j:Ljava/lang/String;

    iput-object p11, p0, LO/R;->k:Ljava/lang/String;

    iput-object p12, p0, LO/R;->l:Ljava/lang/String;

    iput-object p13, p0, LO/R;->m:Ljava/lang/String;

    iput-object p14, p0, LO/R;->n:Ljava/util/List;

    move-object/from16 v0, p15

    iput-object v0, p0, LO/R;->o:Ljava/util/List;

    move-object/from16 v0, p16

    iput-object v0, p0, LO/R;->p:Ljava/util/List;

    move-object/from16 v0, p17

    iput-object v0, p0, LO/R;->q:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()Lo/T;
    .locals 1

    iget-object v0, p0, LO/R;->a:Lo/T;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LO/R;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LO/R;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, LO/R;->d:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, LO/R;->e:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, LO/R;->f:I

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, LO/R;->g:I

    return v0
.end method

.method public h()F
    .locals 1

    iget v0, p0, LO/R;->h:F

    return v0
.end method

.method public i()F
    .locals 1

    iget v0, p0, LO/R;->i:F

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LO/R;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LO/R;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LO/R;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LO/R;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LO/R;->n:Ljava/util/List;

    return-object v0
.end method

.method public o()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LO/R;->o:Ljava/util/List;

    return-object v0
.end method

.method public p()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LO/R;->p:Ljava/util/List;

    return-object v0
.end method

.method public q()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LO/R;->q:Ljava/util/List;

    return-object v0
.end method
