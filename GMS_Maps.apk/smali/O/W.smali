.class public LO/W;
.super LO/U;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:D

.field private c:D


# direct methods
.method public constructor <init>(LO/U;LO/z;)V
    .locals 1

    invoke-direct {p0, p1}, LO/U;-><init>(LO/U;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LO/W;->a:Z

    invoke-direct {p0, p2}, LO/W;->a(LO/z;)V

    return-void
.end method

.method private a(LO/z;)V
    .locals 5

    const-wide/high16 v3, -0x4010000000000000L

    invoke-virtual {p0}, LO/W;->c()Lo/u;

    move-result-object v0

    if-nez v0, :cond_0

    iput-wide v3, p0, LO/W;->b:D

    iput-wide v3, p0, LO/W;->c:D

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lo/u;->a()I

    move-result v1

    invoke-virtual {v0}, Lo/u;->b()I

    move-result v0

    invoke-static {v1, v0}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    const-wide/high16 v1, 0x4059000000000000L

    invoke-virtual {p1, v0, v1, v2}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, LO/z;->a(LO/D;)D

    move-result-wide v1

    iput-wide v1, p0, LO/W;->b:D

    invoke-virtual {p1, v0}, LO/z;->b(LO/D;)D

    move-result-wide v0

    iput-wide v0, p0, LO/W;->c:D

    goto :goto_0

    :cond_1
    iput-wide v3, p0, LO/W;->b:D

    iput-wide v3, p0, LO/W;->c:D

    goto :goto_0
.end method


# virtual methods
.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, LO/W;->a:Z

    return-void
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, LO/W;->a:Z

    return v0
.end method

.method public n()D
    .locals 2

    iget-wide v0, p0, LO/W;->b:D

    return-wide v0
.end method
