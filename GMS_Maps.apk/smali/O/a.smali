.class public LO/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:[LO/U;

.field private d:LO/g;


# direct methods
.method private constructor <init>(LO/g;)V
    .locals 2

    invoke-virtual {p1}, LO/g;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1}, LO/g;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LO/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object p1, p0, LO/a;->d:LO/g;

    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LO/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p2, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public static a(LO/g;)LO/a;
    .locals 1

    new-instance v0, LO/a;

    invoke-direct {v0, p0}, LO/a;-><init>(LO/g;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/a;
    .locals 1

    new-instance v0, LO/a;

    invoke-direct {v0, p0}, LO/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method


# virtual methods
.method public a()LO/g;
    .locals 2

    iget-object v0, p0, LO/a;->d:LO/g;

    if-nez v0, :cond_1

    iget-object v0, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LO/i;

    iget-object v1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1}, LO/i;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    iput-object v0, p0, LO/a;->d:LO/g;

    iget-object v0, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1

    iget-object v0, p0, LO/a;->d:LO/g;

    iget-object v1, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    iget-object v0, p0, LO/a;->d:LO/g;

    goto :goto_0
.end method

.method public a([LO/U;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, LO/a;->d()[LO/U;

    aget-object v1, p1, v0

    iget-object v2, p0, LO/a;->c:[LO/U;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, LO/U;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p1, v1

    iget-object v2, p0, LO/a;->c:[LO/U;

    iget-object v3, p0, LO/a;->c:[LO/U;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, LO/U;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public d()[LO/U;
    .locals 7

    const/4 v6, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, LO/a;->c:[LO/U;

    if-nez v1, :cond_1

    iget-object v1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_0

    new-array v0, v0, [LO/U;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v2, v1, [LO/U;

    iput-object v2, p0, LO/a;->c:[LO/U;

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v2, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v3, p0, LO/a;->c:[LO/U;

    new-instance v4, LO/U;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, LO/a;->c:[LO/U;

    goto :goto_0
.end method
