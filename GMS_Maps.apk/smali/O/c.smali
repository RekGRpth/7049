.class public LO/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:LO/c;

.field private static b:Law/p;

.field private static c:Law/g;

.field private static final d:Ljava/util/ArrayList;


# instance fields
.field private final e:Ljava/util/Locale;

.field private final f:[LO/e;

.field private final g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LO/c;->d:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>(Ljava/util/Locale;[LO/e;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LO/c;->e:Ljava/util/Locale;

    iput-object p2, p0, LO/c;->f:[LO/e;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LO/c;->g:Ljava/util/HashMap;

    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    iget-object v3, p0, LO/c;->g:Ljava/util/HashMap;

    invoke-static {v2}, LO/e;->a(LO/e;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a()LO/c;
    .locals 1

    sget-object v0, LO/c;->a:LO/c;

    return-object v0
.end method

.method static a(Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DA_DirOpt_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(LO/f;)V
    .locals 2

    const-class v1, LO/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, LO/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LO/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, LO/c;->a:LO/c;

    iget-object v1, v1, LO/c;->e:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, LO/c;->a(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Law/p;)V
    .locals 3

    sput-object p1, LO/c;->b:Law/p;

    new-instance v0, LO/c;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [LO/e;

    invoke-direct {v0, v1, v2}, LO/c;-><init>(Ljava/util/Locale;[LO/e;)V

    sput-object v0, LO/c;->a:LO/c;

    const/4 v0, 0x1

    invoke-static {p0, v0}, LO/c;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method private static declared-synchronized a(Landroid/content/Context;Z)V
    .locals 4

    const-class v1, LO/c;

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LO/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/aY;->s:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p0, v2, v3}, LJ/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_1
    invoke-static {v0, v2}, LO/c;->b(Ljava/util/Locale;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    sget-object v0, LO/c;->c:Law/g;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    return-void

    :cond_1
    :try_start_3
    new-instance v0, LO/d;

    invoke-direct {v0, p0}, LO/d;-><init>(Landroid/content/Context;)V

    sput-object v0, LO/c;->c:Law/g;

    sget-object v0, LO/c;->b:Law/p;

    sget-object v2, LO/c;->c:Law/g;

    invoke-interface {v0, v2}, Law/p;->c(Law/g;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/util/Locale;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-static {p0, p1}, LO/c;->b(Ljava/util/Locale;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public static a([LO/b;I)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, p0, v2

    invoke-virtual {v4}, LO/b;->b()I

    move-result v5

    if-ne v5, p1, :cond_1

    invoke-virtual {v4}, LO/b;->c()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static declared-synchronized b(LO/f;)V
    .locals 2

    const-class v1, LO/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, LO/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Ljava/util/Locale;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const-class v1, LO/c;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v3, v2, [LO/e;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, LO/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/e;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, LO/c;->a:LO/c;

    if-eqz v0, :cond_1

    sget-object v0, LO/c;->a:LO/c;

    iget-object v0, v0, LO/c;->f:[LO/e;

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, LO/c;

    invoke-direct {v0, p0, v3}, LO/c;-><init>(Ljava/util/Locale;[LO/e;)V

    sput-object v0, LO/c;->a:LO/c;

    sget-object v0, LO/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/f;

    invoke-interface {v0}, LO/f;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_1
    sput-object v0, LO/c;->c:Law/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-void
.end method

.method public static b(I)[LO/b;
    .locals 5

    const/4 v4, 0x0

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x2

    new-array v0, v0, [LO/b;

    new-instance v1, LO/b;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v4}, LO/b;-><init>(II)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, LO/b;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v4}, LO/b;-><init>(II)V

    aput-object v2, v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static c([LO/b;)Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    if-lez v0, :cond_0

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    aget-object v2, p0, v0

    invoke-virtual {v2}, LO/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, p0, v0

    invoke-virtual {v2}, LO/b;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(I)Z
    .locals 2

    iget-object v0, p0, LO/c;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/e;

    if-eqz v0, :cond_0

    invoke-static {v0}, LO/e;->b(LO/e;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a([LO/b;)I
    .locals 5

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p1, v1

    iget-object v4, p0, LO/c;->g:Ljava/util/HashMap;

    invoke-virtual {v3}, LO/b;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public a(I)LO/e;
    .locals 2

    iget-object v0, p0, LO/c;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/e;

    return-object v0
.end method

.method public a(Landroid/content/Context;[LO/b;)V
    .locals 2

    invoke-virtual {p0, p2}, LO/c;->a([LO/b;)I

    move-result v0

    array-length v1, p2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-static {p1, v0}, LO/c;->a(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method public b([LO/b;)I
    .locals 4

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {v3}, LO/b;->b()I

    move-result v3

    invoke-direct {p0, v3}, LO/c;->c(I)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method
