.class public LO/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/common/collect/ImmutableList;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:Z

.field private g:LO/j;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private j:LO/N;

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "PREPARE"

    const-string v1, "ACT"

    const-string v2, "SUCCESS"

    const-string v3, "NOTE"

    invoke-static {v0, v1, v2, v3}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, LO/j;->a:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method public constructor <init>(IIIIZLjava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LO/j;->b:I

    iput p2, p0, LO/j;->c:I

    iput p3, p0, LO/j;->d:I

    iput p4, p0, LO/j;->e:I

    iput-boolean p5, p0, LO/j;->f:Z

    iput-object p6, p0, LO/j;->h:Ljava/lang/String;

    iput p7, p0, LO/j;->i:I

    iput p8, p0, LO/j;->k:I

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;D)LO/j;
    .locals 13

    const/4 v8, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, -0x1

    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ltz v1, :cond_0

    if-lt v1, v6, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    int-to-double v2, v0

    mul-double v9, v2, p1

    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v3

    invoke-static {p0, v6}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    int-to-double v6, v0

    mul-double v11, v6, p1

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x7

    invoke-static {p0, v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v7

    const/16 v0, 0x8

    invoke-static {p0, v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v8

    new-instance v0, LO/j;

    double-to-int v2, v9

    double-to-int v4, v11

    invoke-direct/range {v0 .. v8}, LO/j;-><init>(IIIIZLjava/lang/String;II)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LO/j;->b:I

    return v0
.end method

.method a(LO/N;)V
    .locals 0

    iput-object p1, p0, LO/j;->j:LO/N;

    return-void
.end method

.method a(LO/j;)V
    .locals 1

    iput-object p1, p0, LO/j;->g:LO/j;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LO/j;->f:Z

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, LO/j;->c:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LO/j;->d:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, LO/j;->e:I

    return v0
.end method

.method public e()LO/N;
    .locals 1

    iget-object v0, p0, LO/j;->j:LO/N;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, LO/j;->f:Z

    return v0
.end method

.method public g()LO/j;
    .locals 1

    iget-object v0, p0, LO/j;->g:LO/j;

    return-object v0
.end method

.method public h()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, LO/j;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/j;->h:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, LO/j;->i:I

    if-ltz v0, :cond_1

    iget v0, p0, LO/j;->i:I

    iget-object v1, p0, LO/j;->j:LO/N;

    invoke-virtual {v1}, LO/N;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, LO/j;->j:LO/N;

    invoke-virtual {v0}, LO/N;->u()Ljava/util/List;

    move-result-object v0

    iget v1, p0, LO/j;->i:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/Q;

    invoke-virtual {v0}, LO/Q;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LO/j;->j:LO/N;

    invoke-virtual {v0}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, LO/j;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, LO/j;->i:I

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()LK/m;
    .locals 2

    iget v0, p0, LO/j;->k:I

    if-ltz v0, :cond_0

    new-instance v0, LK/t;

    iget v1, p0, LO/j;->k:I

    invoke-direct {v0, v1}, LK/t;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, LO/j;->a:Lcom/google/common/collect/ImmutableList;

    iget v2, p0, LO/j;->b:I

    invoke-virtual {v0, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " end:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/j;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " length: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/j;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/j;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LO/j;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " step:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LO/j;->j:LO/N;

    invoke-virtual {v1}, LO/N;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
