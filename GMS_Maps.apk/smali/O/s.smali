.class public LO/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[LO/z;

.field private final b:I

.field private final c:LO/N;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:Z

.field private final i:I

.field private final j:Z

.field private final k:LO/z;


# direct methods
.method constructor <init>([LO/z;ILO/N;IIIIZIZLO/z;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LO/s;->a:[LO/z;

    iput p2, p0, LO/s;->b:I

    iput-object p3, p0, LO/s;->c:LO/N;

    iput p4, p0, LO/s;->d:I

    iput p5, p0, LO/s;->e:I

    iput p6, p0, LO/s;->f:I

    iput p7, p0, LO/s;->g:I

    iput-boolean p8, p0, LO/s;->h:Z

    iput p9, p0, LO/s;->i:I

    iput-boolean p10, p0, LO/s;->j:Z

    iput-object p11, p0, LO/s;->k:LO/z;

    return-void
.end method

.method public static a()LO/s;
    .locals 12

    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v2, -0x1

    new-instance v0, LO/s;

    move-object v3, v1

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v9, v8

    move v10, v8

    move-object v11, v1

    invoke-direct/range {v0 .. v11}, LO/s;-><init>([LO/z;ILO/N;IIIIZIZLO/z;)V

    return-object v0
.end method

.method private n()Z
    .locals 1

    iget-object v0, p0, LO/s;->a:[LO/z;

    if-eqz v0, :cond_0

    iget v0, p0, LO/s;->b:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(F)Lo/am;
    .locals 9

    iget v0, p0, LO/s;->d:I

    if-ltz v0, :cond_0

    iget-object v0, p0, LO/s;->a:[LO/z;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v1

    iget v2, p0, LO/s;->d:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, LO/z;->b(I)D

    move-result-wide v3

    const/4 v5, 0x0

    cmpl-float v5, p1, v5

    if-ltz v5, :cond_2

    invoke-virtual {v1, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v5

    invoke-virtual {v5}, Lo/T;->b()D

    move-result-wide v5

    invoke-static {v5, v6}, Lo/T;->a(D)D

    move-result-wide v5

    float-to-double v7, p1

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    invoke-virtual {v0, v3, v4}, LO/z;->a(D)I

    move-result v0

    add-int/lit8 v3, v0, 0x1

    new-instance v0, Lo/am;

    invoke-direct {v0, v1, v2, v3}, Lo/am;-><init>(Lo/X;II)V

    goto :goto_0

    :cond_2
    new-instance v0, Lo/am;

    invoke-direct {v0, v1, v2}, Lo/am;-><init>(Lo/X;I)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LO/s;->e:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public c()I
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LO/s;->g:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public d()I
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LO/s;->f:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public e()I
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LO/s;->d:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public f()[LO/z;
    .locals 1

    iget-object v0, p0, LO/s;->a:[LO/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/s;->a:[LO/z;

    invoke-virtual {v0}, [LO/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/z;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()LO/z;
    .locals 2

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/s;->a:[LO/z;

    iget v1, p0, LO/s;->b:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()LO/N;
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/s;->c:LO/N;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()LO/z;
    .locals 1

    iget-object v0, p0, LO/s;->k:LO/z;

    return-object v0
.end method

.method public j()Z
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LO/s;->h:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LO/s;->i:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    invoke-direct {p0}, LO/s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LO/s;->j:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method m()I
    .locals 1

    iget v0, p0, LO/s;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "[numRoutes:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LO/s;->a:[LO/z;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " curRoute:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LO/s;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " curStep:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LO/s;->c:LO/N;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " curSegment:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LO/s;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " metersToNextStep:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LO/s;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " metersRemaining:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LO/s;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " secondsRemaining:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LO/s;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " areAlternatesStale:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, LO/s;->h:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " trafficStatus:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LO/s;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " onRoute:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, LO/s;->j:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " hasPathfinderRoute:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LO/s;->k:LO/z;

    if-nez v0, :cond_2

    const-string v0, "false"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LO/s;->a:[LO/z;

    array-length v0, v0

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, LO/s;->c:LO/N;

    invoke-virtual {v0}, LO/N;->i()I

    move-result v0

    goto :goto_1

    :cond_2
    const-string v0, "true"

    goto :goto_2
.end method
