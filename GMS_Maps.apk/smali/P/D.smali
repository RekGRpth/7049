.class public LP/D;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LP/A;


# direct methods
.method public constructor <init>(LP/A;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LP/D;->a:LP/A;

    return-void
.end method

.method private a(LP/C;)D
    .locals 4

    invoke-virtual {p1}, LP/C;->i()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, LP/C;->j()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/T;->c(Lo/T;)F

    move-result v0

    float-to-double v0, v0

    invoke-virtual {p1}, LP/C;->i()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->e()D

    move-result-wide v2

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/util/LinkedList;)V
    .locals 0

    invoke-virtual {p0, p1}, LP/D;->b(Ljava/util/LinkedList;)V

    invoke-virtual {p0, p1}, LP/D;->c(Ljava/util/LinkedList;)V

    return-void
.end method

.method a(Ljava/util/ListIterator;)V
    .locals 12

    const/4 v7, 0x6

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-interface {p1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, LP/C;

    invoke-virtual {v4}, LP/C;->b()I

    move-result v0

    if-ne v0, v7, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/ListIterator;->previousIndex()I

    move-result v11

    move v10, v9

    :goto_0
    if-gt v10, v8, :cond_4

    invoke-interface {p1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LP/C;

    invoke-virtual {v4}, LP/C;->h()Lp/e;

    move-result-object v0

    invoke-virtual {v6}, LP/C;->h()Lp/e;

    move-result-object v1

    invoke-static {v0, v1}, LP/b;->a(Lp/e;Lp/e;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43250000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    invoke-virtual {v4}, LP/C;->h()Lp/e;

    move-result-object v0

    invoke-virtual {v0, v9}, Lp/e;->c(I)Lp/f;

    move-result-object v0

    invoke-virtual {v6}, LP/C;->g()Lp/e;

    move-result-object v1

    invoke-virtual {v1, v9}, Lp/e;->c(I)Lp/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lp/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, LP/C;

    iget-object v1, p0, LP/D;->a:LP/A;

    invoke-virtual {v4}, LP/C;->f()I

    move-result v2

    invoke-virtual {v6}, LP/C;->f()I

    move-result v3

    invoke-virtual {v4}, LP/C;->j()Lo/T;

    move-result-object v4

    invoke-virtual {v6}, LP/C;->j()Lo/T;

    move-result-object v5

    invoke-virtual {v6}, LP/C;->d()[Lp/f;

    move-result-object v6

    invoke-direct/range {v0 .. v8}, LP/C;-><init>(LP/A;IILo/T;Lo/T;[Lp/f;II)V

    move v1, v9

    :goto_1
    if-gt v1, v10, :cond_3

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    :cond_2
    invoke-interface {p1}, Ljava/util/ListIterator;->remove()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    :cond_4
    invoke-interface {p1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    move v0, v9

    :goto_2
    sub-int v2, v1, v11

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    invoke-interface {p1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    invoke-direct {p0, v6}, LP/D;->a(LP/C;)D

    move-result-wide v0

    const-wide v2, 0x4052c00000000000L

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_4

    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto/16 :goto_0
.end method

.method b(Ljava/util/LinkedList;)V
    .locals 2

    invoke-virtual {p1}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, LP/D;->a(Ljava/util/ListIterator;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method c(Ljava/util/LinkedList;)V
    .locals 10

    invoke-virtual {p1}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    :cond_0
    invoke-interface {v9}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LP/C;

    :goto_0
    move-object v4, v0

    :goto_1
    invoke-interface {v9}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LP/C;

    invoke-virtual {v4}, LP/C;->a()I

    move-result v0

    const/16 v1, 0xf

    if-ge v0, v1, :cond_3

    new-instance v0, LP/C;

    iget-object v1, p0, LP/D;->a:LP/A;

    invoke-virtual {v4}, LP/C;->e()I

    move-result v2

    invoke-virtual {v8}, LP/C;->f()I

    move-result v3

    invoke-virtual {v4}, LP/C;->i()Lo/T;

    move-result-object v4

    invoke-virtual {v8}, LP/C;->j()Lo/T;

    move-result-object v5

    invoke-virtual {v8}, LP/C;->d()[Lp/f;

    move-result-object v6

    invoke-virtual {v8}, LP/C;->b()I

    move-result v7

    invoke-virtual {v8}, LP/C;->c()I

    move-result v8

    invoke-direct/range {v0 .. v8}, LP/C;-><init>(LP/A;IILo/T;Lo/T;[Lp/f;II)V

    invoke-interface {v9}, Ljava/util/ListIterator;->remove()V

    invoke-interface {v9}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    invoke-interface {v9}, Ljava/util/ListIterator;->remove()V

    invoke-interface {v9, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    :goto_2
    move-object v4, v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    move-object v0, v8

    goto :goto_2
.end method
