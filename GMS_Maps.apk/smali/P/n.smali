.class public LP/n;
.super LP/h;
.source "SourceFile"


# static fields
.field private static final d:[I


# instance fields
.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LP/n;->d:[I

    return-void

    :array_0
    .array-data 4
        0x7f0d00f8
        0x7f0d00f9
        0x7f0d00fa
        0x7f0d00fb
        0x7f0d00fc
        0x7f0d00fd
        0x7f0d00fe
        0x7f0d00ff
        0x7f0d0100
        0x7f0d0101
        0x7f0d0102
        0x7f0d0103
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, LP/h;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput p3, p0, LP/n;->b:I

    iput p4, p0, LP/n;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LP/n;->b:I

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LP/n;->c:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, LP/n;->c:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    iget v4, p0, LP/n;->b:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    add-int/lit8 v0, v0, 0x2

    :cond_0
    :goto_1
    invoke-virtual {p0, v2}, LP/n;->a(I)LO/P;

    move-result-object v3

    if-eqz v3, :cond_4

    add-int/lit8 v0, v0, 0x6

    iget-object v4, p0, LP/n;->a:Landroid/content/Context;

    sget-object v5, LP/n;->d:[I

    aget v0, v5, v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, LO/P;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_1
    iget v0, p0, LP/n;->c:I

    if-eq v0, v1, :cond_5

    move-object v0, v3

    goto :goto_2

    :cond_2
    iget v4, p0, LP/n;->b:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_3

    add-int/lit8 v0, v0, 0x4

    goto :goto_1

    :cond_3
    iget v4, p0, LP/n;->b:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    move-object v0, v3

    goto :goto_2

    :cond_4
    iget-object v1, p0, LP/n;->a:Landroid/content/Context;

    sget-object v2, LP/n;->d:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_0
.end method
