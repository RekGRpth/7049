.class public LP/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Thread;

.field private volatile b:LP/t;

.field private final c:Lp/b;

.field private final d:LP/e;

.field private final e:LP/a;

.field private final f:LP/x;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lr/z;Ljava/lang/Thread;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, LP/r;->a:Ljava/lang/Thread;

    new-instance v0, Lp/b;

    const/4 v1, 0x1

    invoke-direct {v0, p2, v1}, Lp/b;-><init>(Lr/z;Z)V

    iput-object v0, p0, LP/r;->c:Lp/b;

    new-instance v0, LP/a;

    iget-object v1, p0, LP/r;->c:Lp/b;

    invoke-direct {v0, p1, v1}, LP/a;-><init>(Landroid/content/Context;Lp/b;)V

    iput-object v0, p0, LP/r;->e:LP/a;

    new-instance v0, LP/e;

    iget-object v1, p0, LP/r;->c:Lp/b;

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v2}, LR/m;->C()LR/n;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LP/e;-><init>(Lp/b;LR/n;)V

    iput-object v0, p0, LP/r;->d:LP/e;

    new-instance v0, LP/x;

    iget-object v1, p0, LP/r;->c:Lp/b;

    invoke-direct {v0, v1}, LP/x;-><init>(Lp/b;)V

    iput-object v0, p0, LP/r;->f:LP/x;

    return-void
.end method

.method static a(Ljava/util/Collection;Ljava/util/Collection;)Lo/ad;
    .locals 5

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v0, v1

    new-array v3, v0, [Lo/T;

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LP/d;

    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, LP/d;->b:Lo/T;

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LP/d;

    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, LP/d;->b:Lo/T;

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_1

    :cond_1
    invoke-static {v3}, Lo/ad;->a([Lo/T;)Lo/ad;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LP/d;

    iget-object v0, v0, LP/d;->b:Lo/T;

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v2

    double-to-int v0, v2

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v2}, LR/m;->C()LR/n;

    move-result-object v2

    invoke-virtual {v2}, LR/n;->a()I

    move-result v2

    mul-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lo/ad;->b(I)Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method private final b()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LP/r;->a:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Method must be called on RouteFinderThread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, LP/r;->f:LP/x;

    invoke-virtual {v0}, LP/x;->a()V

    return-void
.end method

.method public a(LP/t;)V
    .locals 0

    iput-object p1, p0, LP/r;->b:LP/t;

    return-void
.end method

.method public a(LaH/h;LO/z;)V
    .locals 3

    invoke-direct {p0}, LP/r;->b()V

    iget-object v0, p0, LP/r;->b:LP/t;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, LP/r;->b(LaH/h;LO/z;)LP/s;

    move-result-object v0

    iget-object v1, p0, LP/r;->b:LP/t;

    iget-object v2, v0, LP/s;->a:LO/z;

    iget v0, v0, LP/s;->b:I

    invoke-interface {v1, v2, v0}, LP/t;->a(LO/z;I)V

    goto :goto_0
.end method

.method public b(LaH/h;LO/z;)LP/s;
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, LP/r;->b()V

    iget-object v0, p0, LP/r;->c:Lp/b;

    invoke-virtual {v0, v4}, Lp/b;->a(Lo/ad;)V

    iget-object v0, p0, LP/r;->d:LP/e;

    invoke-virtual {v0, p1}, LP/e;->a(LaH/h;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, LP/s;

    invoke-direct {v0, v4, v3}, LP/s;-><init>(LO/z;I)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LP/r;->d:LP/e;

    invoke-virtual {v0, p1, p2}, LP/e;->a(LaH/h;LO/z;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, LP/s;

    invoke-direct {v0, v4, v3}, LP/s;-><init>(LO/z;I)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LP/d;

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LP/d;

    goto :goto_2

    :cond_3
    invoke-static {v1, v2}, LP/r;->a(Ljava/util/Collection;Ljava/util/Collection;)Lo/ad;

    move-result-object v0

    iget-object v3, p0, LP/r;->c:Lp/b;

    invoke-virtual {v3, v0}, Lp/b;->a(Lo/ad;)V

    iget-object v0, p0, LP/r;->f:LP/x;

    invoke-virtual {v0, v1, v2}, LP/x;->a(Ljava/util/Collection;Ljava/util/Collection;)LP/A;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, LP/s;

    const/4 v1, 0x2

    invoke-direct {v0, v4, v1}, LP/s;-><init>(LO/z;I)V

    goto :goto_0

    :cond_4
    new-instance v1, LO/U;

    invoke-virtual {p1}, LaH/h;->q()Lo/u;

    move-result-object v2

    invoke-direct {v1, v2}, LO/U;-><init>(Lo/u;)V

    iget-object v2, p0, LP/r;->e:LP/a;

    invoke-virtual {v2, v0, v1, p2}, LP/a;->a(LP/A;LO/U;LO/z;)LO/z;

    move-result-object v1

    new-instance v0, LP/s;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LP/s;-><init>(LO/z;I)V

    goto :goto_0
.end method
