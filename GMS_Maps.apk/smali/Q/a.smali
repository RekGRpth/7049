.class public LQ/a;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:F

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LQ/s;-><init>()V

    const/high16 v0, -0x40800000

    iput v0, p0, LQ/a;->c:F

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/a;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/a;->e:Z

    return-void
.end method

.method private a(F)V
    .locals 2

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    invoke-virtual {p0}, LQ/a;->ah()V

    iget v0, p0, LQ/a;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    add-float/2addr v0, p1

    iput v0, p0, LQ/a;->c:F

    :goto_0
    iget v0, p0, LQ/a;->c:F

    const/high16 v1, 0x41a80000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LQ/a;->c:F

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LQ/a;->a(Z)V

    return-void

    :cond_0
    iget v0, p0, LQ/a;->c:F

    add-float/2addr v0, p1

    iput v0, p0, LQ/a;->c:F

    goto :goto_0
.end method

.method private ap()Z
    .locals 7

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, LO/z;->z()J

    move-result-wide v3

    const-wide/16 v5, 0x7530

    add-long/2addr v3, v5

    cmp-long v0, v1, v3

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LO/N;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object p1

    :cond_0
    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, LO/N;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LO/N;->b()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cQ;->setNavigationImageStep(LO/N;)V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->z()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object p1

    goto :goto_0

    :cond_2
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->A()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->B()V

    const/high16 v0, -0x40800000

    iput v0, p0, LQ/a;->c:F

    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/a;->b()V

    return-void
.end method

.method protected a(FFF)V
    .locals 1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    invoke-virtual {p0}, LQ/a;->ah()V

    iput p1, p0, LQ/a;->c:F

    return-void
.end method

.method protected a(LO/N;LO/N;)V
    .locals 0

    invoke-virtual {p0, p2}, LQ/a;->a(LO/N;)V

    invoke-direct {p0, p2}, LQ/a;->b(LO/N;)V

    invoke-virtual {p0}, LQ/a;->m()V

    return-void
.end method

.method protected a(LO/z;[LO/z;)V
    .locals 1

    invoke-super {p0, p1, p2}, LQ/s;->a(LO/z;[LO/z;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LQ/a;->b(LO/N;)V

    invoke-virtual {p0}, LQ/a;->m()V

    return-void
.end method

.method protected a(Z)V
    .locals 4

    invoke-virtual {p0}, LQ/a;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->af()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    iget-boolean v1, p0, LQ/a;->d:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, LQ/a;->d:Z

    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/app/cQ;->m()V

    :cond_0
    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    iget-object v2, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    iget v3, p0, LQ/a;->c:F

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;FZ)V

    return-void
.end method

.method public b()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget v0, p0, LQ/a;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    invoke-virtual {p0}, LQ/a;->ah()V

    :cond_0
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/cQ;->g(Z)V

    :goto_0
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/a;->a(LO/N;)V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LQ/a;->b(LO/N;)V

    invoke-virtual {p0}, LQ/a;->m()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->F()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/cQ;->setDoubleTapZoomsAboutCenter(Z)V

    return-void

    :cond_1
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    new-array v2, v4, [LO/z;

    iget-object v3, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/maps/driveabout/app/cQ;->g(Z)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-boolean v0, p0, LQ/a;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/a;->e:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, LQ/b;

    invoke-direct {v1, p0}, LQ/b;-><init>(LQ/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    invoke-virtual {p0}, LQ/a;->e()V

    return-void
.end method

.method public e()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->q()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->A()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->f(Z)V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->H()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setDoubleTapZoomsAboutCenter(Z)V

    return-void
.end method

.method protected f()I
    .locals 4

    const/4 v0, 0x3

    const/4 v1, 0x1

    iget-object v2, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {v2}, LaH/h;->hasBearing()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v0, 0x2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->w()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v2

    iget-object v3, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v3

    if-eqz v3, :cond_3

    if-eqz v2, :cond_1

    invoke-virtual {v2}, LO/N;->b()I

    move-result v2

    if-eq v2, v1, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected g()V
    .locals 1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->f()V

    :cond_1
    return-void
.end method

.method protected h()V
    .locals 1

    const/high16 v0, 0x3f800000

    invoke-direct {p0, v0}, LQ/a;->a(F)V

    return-void
.end method

.method protected i()V
    .locals 1

    const/high16 v0, -0x40800000

    invoke-direct {p0, v0}, LQ/a;->a(F)V

    return-void
.end method

.method protected j()Z
    .locals 2

    iget v0, p0, LQ/a;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, LQ/a;->k()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected k()V
    .locals 2

    const/4 v1, 0x0

    const/high16 v0, -0x40800000

    iput v0, p0, LQ/a;->c:F

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->q()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->f(Z)V

    invoke-virtual {p0, v1}, LQ/a;->a(Z)V

    return-void
.end method

.method protected l()V
    .locals 1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->A()V

    invoke-virtual {p0}, LQ/a;->m()V

    return-void
.end method

.method protected m()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->A()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, LQ/a;->ap()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->w()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->A()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_4

    if-eqz v0, :cond_3

    array-length v0, v0

    if-le v0, v3, :cond_3

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/maps/driveabout/app/cQ;->c(Z)V

    :goto_1
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->v()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    array-length v0, v0

    if-gt v0, v3, :cond_1

    :cond_5
    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    goto :goto_0
.end method

.method protected n()V
    .locals 3

    iget-object v0, p0, LQ/a;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/N;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/a;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    :cond_0
    return-void
.end method

.method protected o()V
    .locals 3

    iget-object v0, p0, LQ/a;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->a(LQ/x;)LQ/s;

    move-result-object v0

    check-cast v0, LQ/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LQ/d;->b_(Z)V

    iget-object v0, p0, LQ/a;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method
