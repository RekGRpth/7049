.class public LQ/k;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/Runnable;

.field private d:[LO/z;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LQ/s;-><init>()V

    return-void
.end method

.method static synthetic a(LQ/k;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, LQ/k;->c:Ljava/lang/Runnable;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "|A="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, LO/z;->o()I

    move-result v1

    div-int/lit8 v1, v1, 0x3c

    iget-object v2, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v2

    div-int/lit8 v2, v2, 0x3c

    iget-object v3, p0, LQ/k;->d:[LO/z;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, LO/z;->o()I

    move-result v3

    div-int/lit8 v3, v3, 0x3c

    const-string v4, "|t="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "|o="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "|a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "Q"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private ap()Z
    .locals 2

    iget-object v0, p0, LQ/k;->d:[LO/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->d:[LO/z;

    array-length v0, v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aq()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    aget-object v1, v1, v3

    iget-object v2, p0, LQ/k;->d:[LO/z;

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    aget-object v1, v1, v3

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    :cond_0
    return-void
.end method

.method private c(LO/z;[LO/z;)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v8, 0x2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v2, p2

    if-lt v2, v8, :cond_0

    aget-object v2, p2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget-object v2, p2, v1

    iget-object v3, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v3

    invoke-virtual {v2}, LO/z;->o()I

    move-result v4

    sub-int/2addr v3, v4

    if-ltz v3, :cond_0

    iget-object v4, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->b()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, LO/z;->p()I

    move-result v5

    invoke-virtual {v2, v4, v0, v5}, LO/z;->a(Landroid/content/Context;II)LO/E;

    move-result-object v2

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v4

    invoke-virtual {v4, v3, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(IZ)Landroid/text/Spannable;

    move-result-object v4

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/maps/driveabout/app/dz;->a(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v5}, LQ/p;->b()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d0093

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v4, v7, v0

    invoke-virtual {v2}, LO/E;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v1

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LQ/k;->f:Ljava/lang/String;

    iget-object v4, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->b()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d0094

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v3, v6, v0

    invoke-virtual {v2}, LO/E;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LQ/k;->g:Ljava/lang/String;

    new-array v2, v8, [LO/z;

    iput-object v2, p0, LQ/k;->d:[LO/z;

    iget-object v2, p0, LQ/k;->d:[LO/z;

    invoke-static {p2, v0, v2, v0, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected A()V
    .locals 1

    const-string v0, "t"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    return-void
.end method

.method protected B()V
    .locals 2

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->h(Z)V

    return-void
.end method

.method public a()V
    .locals 4

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    iget-object v1, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LQ/k;->c(LO/z;[LO/z;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, LQ/l;

    invoke-direct {v0, p0}, LQ/l;-><init>(LQ/k;)V

    iput-object v0, p0, LQ/k;->c:Ljava/lang/Runnable;

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;J)V

    const-string v0, "s"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/t;->a(Z)V

    invoke-virtual {p0}, LQ/k;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/k;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->b(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0}, LQ/s;->a()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    goto :goto_0
.end method

.method public a(LO/z;[LO/z;)V
    .locals 1

    invoke-direct {p0, p1, p2}, LQ/k;->c(LO/z;[LO/z;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LQ/k;->aq()V

    invoke-virtual {p0}, LQ/k;->u()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    goto :goto_0
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 0

    return-void
.end method

.method protected a(Z)V
    .locals 2

    iget-boolean v0, p0, LQ/k;->e:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/k;->e:Z

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->b(Lcom/google/android/maps/driveabout/app/aQ;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/k;->e:Z

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->s()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->E()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->I()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->w()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->t()V

    invoke-direct {p0}, LQ/k;->aq()V

    invoke-virtual {p0}, LQ/k;->u()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/k;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->b(Ljava/lang/Runnable;)V

    :cond_0
    iput-object v2, p0, LQ/k;->c:Ljava/lang/Runnable;

    iput-object v2, p0, LQ/k;->d:[LO/z;

    iput-object v2, p0, LQ/k;->f:Ljava/lang/String;

    iput-object v2, p0, LQ/k;->g:Ljava/lang/String;

    :cond_1
    invoke-super {p0}, LQ/s;->d()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->J()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->n()V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->u()V

    return-void
.end method

.method protected u()V
    .locals 2

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected v()V
    .locals 2

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    :cond_0
    return-void
.end method

.method protected x()V
    .locals 2

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected y()V
    .locals 3

    const-string v0, "a"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, LO/t;->a(LO/z;)V

    :cond_0
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    return-void
.end method

.method protected z()V
    .locals 1

    const-string v0, "c"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    return-void
.end method
