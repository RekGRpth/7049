.class public LQ/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:[LQ/s;

.field protected final b:[LQ/s;

.field protected c:I

.field private final d:Ljava/lang/Thread;

.field private e:Landroid/content/Context;

.field private final f:Lcom/google/android/maps/driveabout/app/cS;

.field private g:Lcom/google/android/maps/driveabout/app/cQ;

.field private final h:Lcom/google/android/maps/driveabout/app/aQ;

.field private final i:LO/t;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/aQ;LO/t;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, LQ/s;->b:I

    new-array v0, v0, [LQ/s;

    iput-object v0, p0, LQ/p;->a:[LQ/s;

    sget v0, LQ/s;->b:I

    new-array v0, v0, [LQ/s;

    iput-object v0, p0, LQ/p;->b:[LQ/s;

    iput-object p1, p0, LQ/p;->e:Landroid/content/Context;

    iput-object p2, p0, LQ/p;->f:Lcom/google/android/maps/driveabout/app/cS;

    iput-object p3, p0, LQ/p;->g:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object p4, p0, LQ/p;->h:Lcom/google/android/maps/driveabout/app/aQ;

    iput-object p5, p0, LQ/p;->i:LO/t;

    const/4 v0, 0x0

    iput v0, p0, LQ/p;->c:I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LQ/p;->d:Ljava/lang/Thread;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/aQ;LO/t;)LQ/p;
    .locals 6

    new-instance v0, LQ/p;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LQ/p;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/aQ;LO/t;)V

    invoke-virtual {v0}, LQ/p;->a()V

    return-object v0
.end method

.method private a(I)V
    .locals 3

    :goto_0
    iget v0, p0, LQ/p;->c:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget-object v1, p0, LQ/p;->b:[LQ/s;

    add-int/lit8 v2, p1, 0x1

    aget-object v1, v1, v2

    aput-object v1, v0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    iget v0, p0, LQ/p;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LQ/p;->c:I

    return-void
.end method


# virtual methods
.method public a(LQ/x;)LQ/s;
    .locals 2

    iget-object v0, p0, LQ/p;->a:[LQ/s;

    invoke-virtual {p1}, LQ/x;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected a()V
    .locals 7

    const/4 v1, 0x0

    invoke-static {}, LQ/x;->values()[LQ/x;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget-object v5, p0, LQ/p;->a:[LQ/s;

    invoke-virtual {v4}, LQ/x;->ordinal()I

    move-result v6

    invoke-virtual {v4, p0}, LQ/x;->a(LQ/p;)LQ/s;

    move-result-object v4

    aput-object v4, v5, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, LQ/p;->b:[LQ/s;

    sget-object v2, LQ/x;->a:LQ/x;

    invoke-virtual {p0, v2}, LQ/p;->a(LQ/x;)LQ/s;

    move-result-object v2

    aput-object v2, v0, v1

    return-void
.end method

.method public a(LQ/r;)V
    .locals 5

    invoke-virtual {p0}, LQ/p;->j()V

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget v1, p0, LQ/p;->c:I

    aget-object v2, v0, v1

    invoke-virtual {p1}, LQ/r;->a()LQ/s;

    move-result-object v3

    invoke-virtual {p1}, LQ/r;->b()I

    move-result v0

    new-instance v1, LL/z;

    invoke-direct {v1, v2, v3}, LL/z;-><init>(LQ/s;LQ/s;)V

    invoke-static {v1}, Ll/f;->b(Ll/j;)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v4, p0, LQ/p;->b:[LQ/s;

    aget-object v4, v4, v1

    if-ne v4, v3, :cond_3

    invoke-direct {p0, v1}, LQ/p;->a(I)V

    add-int/lit8 v0, v0, -0x1

    :cond_0
    iget v1, p0, LQ/p;->c:I

    if-le v0, v1, :cond_1

    invoke-virtual {v2}, LQ/s;->p()V

    :cond_1
    invoke-virtual {v2}, LQ/s;->d()V

    iput v0, p0, LQ/p;->c:I

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget v1, p0, LQ/p;->c:I

    aput-object v3, v0, v1

    invoke-virtual {v3}, LQ/s;->a()V

    invoke-virtual {p1}, LQ/r;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, LQ/s;->q()V

    :cond_2
    invoke-virtual {v3}, LQ/s;->I()V

    invoke-virtual {v3}, LQ/s;->c()V

    return-void

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/cQ;)V
    .locals 0

    iput-object p1, p0, LQ/p;->g:Lcom/google/android/maps/driveabout/app/cQ;

    return-void
.end method

.method protected a(LQ/s;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, LQ/p;->b:[LQ/s;

    iget v3, p0, LQ/p;->c:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, LQ/s;->G()LQ/x;

    move-result-object v2

    invoke-virtual {p1}, LQ/s;->G()LQ/x;

    move-result-object v3

    sget-object v4, LQ/x;->a:LQ/x;

    if-ne v2, v4, :cond_2

    sget-object v2, LQ/x;->b:LQ/x;

    if-ne v3, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v4, LQ/x;->b:LQ/x;

    if-ne v2, v4, :cond_3

    sget-object v2, LQ/x;->c:LQ/x;

    if-eq v3, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v4, LQ/x;->c:LQ/x;

    if-ne v2, v4, :cond_6

    sget-object v2, LQ/x;->d:LQ/x;

    if-eq v3, v2, :cond_4

    sget-object v2, LQ/x;->e:LQ/x;

    if-ne v3, v2, :cond_5

    :cond_4
    move v1, v0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    sget-object v4, LQ/x;->d:LQ/x;

    if-ne v2, v4, :cond_7

    sget-object v2, LQ/x;->e:LQ/x;

    if-eq v3, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    sget-object v4, LQ/x;->e:LQ/x;

    if-ne v2, v4, :cond_0

    sget-object v2, LQ/x;->i:LQ/x;

    if-eq v3, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public a(LQ/x;Z)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, LQ/p;->j()V

    invoke-virtual {p0, p1}, LQ/p;->a(LQ/x;)LQ/s;

    move-result-object v3

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget v4, p0, LQ/p;->c:I

    aget-object v0, v0, v4

    if-ne v3, v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v3}, LQ/p;->a(LQ/s;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tried to transition from: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LQ/s;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, LQ/s;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v4, LQ/r;

    if-eqz p2, :cond_2

    iget v0, p0, LQ/p;->c:I

    :goto_1
    const/4 v5, 0x0

    invoke-direct {v4, v3, v0, v2, v5}, LQ/r;-><init>(LQ/s;IZLQ/q;)V

    invoke-virtual {p0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, v4}, LQ/s;->a(LQ/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v4}, LQ/p;->a(LQ/r;)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, LQ/p;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method b()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LQ/p;->e:Landroid/content/Context;

    return-object v0
.end method

.method public b(LQ/x;)Z
    .locals 1

    invoke-virtual {p0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->G()LQ/x;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Lcom/google/android/maps/driveabout/app/cS;
    .locals 1

    iget-object v0, p0, LQ/p;->f:Lcom/google/android/maps/driveabout/app/cS;

    return-object v0
.end method

.method d()Lcom/google/android/maps/driveabout/app/cQ;
    .locals 1

    iget-object v0, p0, LQ/p;->g:Lcom/google/android/maps/driveabout/app/cQ;

    return-object v0
.end method

.method e()Lcom/google/android/maps/driveabout/app/aQ;
    .locals 1

    iget-object v0, p0, LQ/p;->h:Lcom/google/android/maps/driveabout/app/aQ;

    return-object v0
.end method

.method f()LO/t;
    .locals 1

    iget-object v0, p0, LQ/p;->i:LO/t;

    return-object v0
.end method

.method public g()LQ/s;
    .locals 2

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget v1, p0, LQ/p;->c:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public h()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, LQ/p;->j()V

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget v1, p0, LQ/p;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, LQ/s;->G()LQ/x;

    move-result-object v0

    sget-object v1, LQ/x;->a:LQ/x;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LQ/p;->b:[LQ/s;

    iget v1, p0, LQ/p;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, LQ/s;->G()LQ/x;

    move-result-object v0

    sget-object v1, LQ/x;->b:LQ/x;

    if-eq v0, v1, :cond_0

    new-instance v0, LQ/r;

    sget-object v1, LQ/x;->d:LQ/x;

    invoke-virtual {p0, v1}, LQ/p;->a(LQ/x;)LQ/s;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v3, v2}, LQ/r;-><init>(LQ/s;IZLQ/q;)V

    invoke-virtual {p0, v0}, LQ/p;->a(LQ/r;)V

    :cond_0
    return-void
.end method

.method public i()Z
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p0}, LQ/p;->j()V

    iget v1, p0, LQ/p;->c:I

    if-lt v1, v0, :cond_1

    new-instance v1, LQ/r;

    iget-object v2, p0, LQ/p;->b:[LQ/s;

    iget v3, p0, LQ/p;->c:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iget v3, p0, LQ/p;->c:I

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v0, v4}, LQ/r;-><init>(LQ/s;IZLQ/q;)V

    invoke-virtual {p0}, LQ/p;->g()LQ/s;

    move-result-object v2

    invoke-virtual {v2, v1}, LQ/s;->a(LQ/r;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, LQ/p;->a(LQ/r;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LQ/p;->d:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on main (UI) thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
