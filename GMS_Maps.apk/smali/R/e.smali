.class public LR/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/location/Location;)LaN/B;
    .locals 6

    const-wide v4, 0x412e848000000000L

    new-instance v0, LaN/B;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method

.method public static a(Lo/aR;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 13

    const-wide v11, 0x412e848000000000L

    const-wide v9, 0x404ca5dc1a63c1f8L

    const-wide v7, 0x3e3921fb54442d18L

    invoke-virtual {p0}, Lo/aR;->d()Lo/T;

    move-result-object v0

    invoke-virtual {p0}, Lo/aR;->f()I

    move-result v1

    int-to-double v1, v1

    invoke-virtual {v0}, Lo/T;->b()D

    move-result-wide v3

    const-wide v5, 0x3f91df46a2529d39L

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v1, v3

    mul-double/2addr v1, v7

    mul-double/2addr v1, v9

    mul-double/2addr v1, v11

    double-to-int v1, v1

    invoke-virtual {p0}, Lo/aR;->e()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v7

    mul-double/2addr v2, v9

    mul-double/2addr v2, v11

    double-to-int v2, v2

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/wireless/googlenav/proto/j2me/dp;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v4, 0x1

    invoke-static {v0}, LR/e;->a(Lo/T;)Lo/u;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/u;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x3

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v3
.end method

.method public static a(Lo/u;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lo/u;->a()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lo/u;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method public static a(LaN/B;)Lo/T;
    .locals 2

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/u;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ne v1, v3, :cond_2

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    new-instance v0, Lo/u;

    invoke-direct {v0, v2, v1}, Lo/u;-><init>(II)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    const/16 v1, 0xe

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    new-instance v0, Lo/u;

    invoke-direct {v0, v2, v1}, Lo/u;-><init>(II)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, LR/e;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->a(Lo/T;)Lo/u;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lo/T;)Lo/u;
    .locals 3

    new-instance v0, Lo/u;

    invoke-virtual {p0}, Lo/T;->a()I

    move-result v1

    invoke-virtual {p0}, Lo/T;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lo/u;-><init>(II)V

    return-object v0
.end method

.method public static a(LaN/B;Lo/T;)V
    .locals 2

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lo/T;->e(II)V

    return-void
.end method

.method public static b(Lo/T;)LaN/B;
    .locals 3

    new-instance v0, LaN/B;

    invoke-virtual {p0}, Lo/T;->a()I

    move-result v1

    invoke-virtual {p0}, Lo/T;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method

.method public static b(Lo/u;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {p0}, Lo/u;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lo/u;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;
    .locals 5

    const/high16 v4, 0x20000000

    const/4 v3, 0x1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    rsub-int/lit8 v0, v0, 0x1e

    add-int/lit8 v0, v0, -0x7

    shl-int v0, v3, v0

    mul-int/2addr v1, v0

    sub-int/2addr v1, v4

    mul-int/2addr v0, v2

    sub-int v0, v4, v0

    new-instance v2, Lo/T;

    invoke-direct {v2, v1, v0}, Lo/T;-><init>(II)V

    return-object v2
.end method
