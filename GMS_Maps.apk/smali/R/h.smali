.class public LR/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/HashMap;

.field protected final b:I

.field private c:LR/k;

.field private d:LR/k;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    iput p1, p0, LR/h;->b:I

    return-void
.end method

.method private a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, LR/h;->b(LR/k;)V

    if-eqz p2, :cond_1

    iget-object v1, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v1}, LR/h;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(LR/k;)V
    .locals 1

    iget-object v0, p0, LR/h;->d:LR/k;

    if-nez v0, :cond_0

    iput-object p1, p0, LR/h;->c:LR/k;

    iput-object p1, p0, LR/h;->d:LR/k;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LR/h;->d:LR/k;

    iput-object v0, p1, LR/k;->a:LR/k;

    iput-object p1, v0, LR/k;->b:LR/k;

    iput-object p1, p0, LR/h;->d:LR/k;

    goto :goto_0
.end method

.method private b(LR/k;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p1, LR/k;->a:LR/k;

    iget-object v1, p1, LR/k;->b:LR/k;

    if-eqz v0, :cond_0

    iput-object v1, v0, LR/k;->b:LR/k;

    :cond_0
    if-eqz v1, :cond_1

    iput-object v0, v1, LR/k;->a:LR/k;

    :cond_1
    iput-object v2, p1, LR/k;->a:LR/k;

    iput-object v2, p1, LR/k;->b:LR/k;

    iget-object v2, p0, LR/h;->c:LR/k;

    if-ne v2, p1, :cond_2

    iput-object v1, p0, LR/h;->c:LR/k;

    :cond_2
    iget-object v1, p0, LR/h;->d:LR/k;

    if-ne v1, p1, :cond_3

    iput-object v0, p0, LR/h;->d:LR/k;

    :cond_3
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    :goto_0
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, LR/h;->c:LR/k;

    iget-object v0, v0, LR/k;->c:Ljava/lang/Object;

    iget-object v1, p0, LR/h;->c:LR/k;

    iget-object v1, v1, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {p0, v1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LR/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, LR/h;->b(LR/k;)V

    invoke-direct {p0, v0}, LR/h;->a(LR/k;)V

    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method protected b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LR/h;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    if-nez v0, :cond_0

    iget v1, p0, LR/h;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LR/h;->a(I)V

    :cond_0
    new-instance v1, LR/k;

    invoke-direct {v1}, LR/k;-><init>()V

    iput-object p2, v1, LR/k;->d:Ljava/lang/Object;

    iput-object p1, v1, LR/k;->c:Ljava/lang/Object;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, LR/h;->b(LR/k;)V

    iget-object v2, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v2}, LR/h;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, LR/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    iget-object v2, v1, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v1}, LR/h;->a(LR/k;)V

    return-void
.end method

.method public final e()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LR/h;->a(I)V

    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LR/h;->c:LR/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LR/h;->c:LR/k;

    iget-object v0, v0, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public h()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LR/h;->d:LR/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LR/h;->d:LR/k;

    iget-object v0, v0, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Ljava/util/Collection;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, LR/h;->f()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, LR/h;->c:LR/k;

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, LR/k;->b:LR/k;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public j()LR/i;
    .locals 2

    new-instance v0, LR/i;

    iget-object v1, p0, LR/h;->c:LR/k;

    invoke-direct {v0, v1}, LR/i;-><init>(LR/k;)V

    return-object v0
.end method
