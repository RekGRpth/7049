.class public final LR/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final A:I

.field private final B:I

.field private final C:LR/n;

.field private final D:Z

.field private E:I

.field private F:Z

.field private G:Z

.field private final a:D

.field private final b:D

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:D

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:F

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:D

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:Z

.field private final z:I


# direct methods
.method constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/16 v2, 0x1d

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-static {p1, v0}, LR/m;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)D

    move-result-wide v0

    iput-wide v0, p0, LR/m;->a:D

    const/4 v0, 0x2

    invoke-static {p1, v0}, LR/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)D

    move-result-wide v0

    iput-wide v0, p0, LR/m;->b:D

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->c:I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->d:I

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->e:I

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->f:I

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->g:I

    const/16 v0, 0x8

    invoke-static {p1, v0}, LR/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)D

    move-result-wide v0

    iput-wide v0, p0, LR/m;->h:D

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->i:I

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->j:I

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->k:I

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->l:I

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->m:I

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->n:I

    const/16 v0, 0x1b

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3c23d70a

    mul-float/2addr v0, v1

    iput v0, p0, LR/m;->o:F

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->p:I

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->q:I

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->r:I

    const/16 v0, 0x13

    invoke-static {p1, v0}, LR/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)D

    move-result-wide v0

    iput-wide v0, p0, LR/m;->s:D

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->t:I

    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->u:I

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->v:I

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->w:I

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->x:I

    const/16 v0, 0x19

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, LR/m;->y:Z

    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->z:I

    const/16 v0, 0x1c

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->A:I

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->B:I

    new-instance v1, LR/n;

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, LR/n;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, LR/m;->C:LR/n;

    const/16 v0, 0x1f

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, LR/m;->D:Z

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LR/m;->E:I

    const/16 v0, 0x21

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, LR/m;->F:Z

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, LR/m;->G:Z

    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->j:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    goto :goto_0
.end method

.method private static final a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)D
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method private static final b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)D
    .locals 6

    const-wide/high16 v0, 0x4024000000000000L

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    neg-int v2, v2

    int-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL

    mul-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public A()I
    .locals 1

    iget v0, p0, LR/m;->A:I

    return v0
.end method

.method public B()I
    .locals 1

    iget v0, p0, LR/m;->B:I

    return v0
.end method

.method public C()LR/n;
    .locals 1

    iget-object v0, p0, LR/m;->C:LR/n;

    return-object v0
.end method

.method public D()Z
    .locals 1

    iget-boolean v0, p0, LR/m;->D:Z

    return v0
.end method

.method public E()I
    .locals 1

    iget v0, p0, LR/m;->E:I

    return v0
.end method

.method public F()Z
    .locals 1

    iget-boolean v0, p0, LR/m;->F:Z

    return v0
.end method

.method public G()Z
    .locals 1

    iget-boolean v0, p0, LR/m;->G:Z

    return v0
.end method

.method public a()D
    .locals 2

    iget-wide v0, p0, LR/m;->a:D

    return-wide v0
.end method

.method public b()D
    .locals 2

    iget-wide v0, p0, LR/m;->b:D

    return-wide v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LR/m;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, LR/m;->d:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, LR/m;->e:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, LR/m;->f:I

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, LR/m;->g:I

    return v0
.end method

.method public h()D
    .locals 2

    iget-wide v0, p0, LR/m;->h:D

    return-wide v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, LR/m;->i:I

    return v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, LR/m;->j:I

    return v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, LR/m;->k:I

    return v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, LR/m;->l:I

    return v0
.end method

.method public m()I
    .locals 1

    iget v0, p0, LR/m;->m:I

    return v0
.end method

.method public n()I
    .locals 1

    iget v0, p0, LR/m;->n:I

    return v0
.end method

.method public o()F
    .locals 1

    iget v0, p0, LR/m;->o:F

    return v0
.end method

.method public p()I
    .locals 1

    iget v0, p0, LR/m;->p:I

    return v0
.end method

.method public q()I
    .locals 1

    iget v0, p0, LR/m;->q:I

    return v0
.end method

.method public r()I
    .locals 1

    iget v0, p0, LR/m;->r:I

    return v0
.end method

.method public s()D
    .locals 2

    iget-wide v0, p0, LR/m;->s:D

    return-wide v0
.end method

.method public t()I
    .locals 1

    iget v0, p0, LR/m;->t:I

    return v0
.end method

.method public u()I
    .locals 1

    iget v0, p0, LR/m;->u:I

    return v0
.end method

.method public v()I
    .locals 1

    iget v0, p0, LR/m;->v:I

    return v0
.end method

.method public w()I
    .locals 1

    iget v0, p0, LR/m;->w:I

    return v0
.end method

.method public x()I
    .locals 1

    iget v0, p0, LR/m;->x:I

    return v0
.end method

.method public y()Z
    .locals 1

    iget-boolean v0, p0, LR/m;->y:Z

    return v0
.end method

.method public z()I
    .locals 1

    iget v0, p0, LR/m;->z:I

    return v0
.end method
