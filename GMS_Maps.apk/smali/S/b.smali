.class public LS/b;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Vector;

.field private b:LS/a;

.field private volatile c:Z

.field private volatile d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LS/b;->a:Ljava/util/Vector;

    return-void
.end method

.method private a(LD/a;LC/a;)V
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/16 v2, 0x1701

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p2}, LC/a;->k()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, LC/a;->l()I

    move-result v3

    int-to-float v4, v3

    const/high16 v5, -0x40800000

    const/high16 v6, 0x3f800000

    move v3, v1

    invoke-interface/range {v0 .. v6}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    return-void
.end method

.method private d(LD/a;)V
    .locals 2

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1701

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, LS/b;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0}, LS/a;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, p1, p2, p3}, LS/a;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-direct {p0, p1, p2}, LS/b;->a(LD/a;LC/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0}, LS/a;->e()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0, p1, p2, p3}, LS/a;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_3
    invoke-direct {p0, p1}, LS/b;->d(LD/a;)V

    throw v0

    :cond_4
    invoke-direct {p0, p1}, LS/b;->d(LD/a;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1, p2}, LS/a;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(LS/a;)V
    .locals 2

    iget-object v1, p0, LS/b;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1, p0}, LS/a;->a(LS/b;)V

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LS/b;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(ZZ)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, LS/b;->c:Z

    if-eq p1, v2, :cond_0

    iput-boolean p1, p0, LS/b;->c:Z

    move v0, v1

    :cond_0
    iget-boolean v2, p0, LS/b;->d:Z

    if-eq p2, v2, :cond_2

    iput-boolean p2, p0, LS/b;->d:Z

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, LS/b;->h()V

    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public a(FFLC/a;)Z
    .locals 3

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1, p2, p3}, LS/a;->a(FFLC/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, p0, LS/b;->b:LS/a;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(FFLo/T;LC/a;)Z
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1, p2, p3, p4}, LS/a;->a_(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LS/a;)V
    .locals 2

    iget-object v1, p0, LS/b;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LS/b;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(FFLC/a;)Z
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1, p2, p3}, LS/a;->b(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LC/a;LD/a;)Z
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1, p2}, LS/a;->b(LC/a;LD/a;)Z

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    move-result v0

    return v0
.end method

.method public c(LD/a;)V
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1}, LS/a;->c(LD/a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(FFLo/T;LC/a;)Z
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0, p1, p2, p3, p4}, LS/a;->d(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, LS/b;->c:Z

    return v0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0}, LS/a;->i()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public j_()Z
    .locals 2

    iget-object v0, p0, LS/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LS/a;

    invoke-virtual {v0}, LS/a;->j_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k_()V
    .locals 1

    iget-object v0, p0, LS/b;->b:LS/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LS/b;->b:LS/a;

    invoke-virtual {v0}, LS/a;->k_()V

    const/4 v0, 0x0

    iput-object v0, p0, LS/b;->b:LS/a;

    :cond_0
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->B:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
