.class public LS/d;
.super LS/a;
.source "SourceFile"


# static fields
.field private static b:LE/o;

.field private static c:LE/d;

.field private static d:LE/d;

.field private static e:LE/d;


# instance fields
.field private a:I

.field private f:F

.field private g:D

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:[F

.field private k:[F

.field private l:F

.field private m:F

.field private final n:Lcom/google/android/maps/driveabout/vector/aV;

.field private o:LD/b;

.field private p:LD/b;

.field private final q:LE/i;

.field private final r:LE/i;

.field private volatile s:Lh/r;

.field private t:F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v0, LE/o;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, LS/d;->b:LE/o;

    new-instance v0, LE/d;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, LS/d;->c:LE/d;

    new-instance v0, LE/d;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, LS/d;->d:LE/d;

    new-instance v0, LE/d;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, LS/d;->e:LE/d;

    sget-object v0, LS/d;->b:LE/o;

    invoke-virtual {v0, v3, v3, v3}, LE/o;->a(FFF)V

    sget-object v0, LS/d;->b:LE/o;

    const/high16 v1, 0x41700000

    invoke-virtual {v0, v3, v1, v3}, LE/o;->a(FFF)V

    sget-object v0, LS/d;->b:LE/o;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1, v3, v3}, LE/o;->a(FFF)V

    sget-object v0, LS/d;->b:LE/o;

    const/high16 v1, 0x3f800000

    const/high16 v2, 0x41700000

    invoke-virtual {v0, v1, v2, v3}, LE/o;->a(FFF)V

    sget-object v0, LS/d;->c:LE/d;

    invoke-virtual {v0, v5, v4}, LE/d;->a(SS)V

    sget-object v0, LS/d;->c:LE/d;

    invoke-virtual {v0, v4, v6}, LE/d;->a(SS)V

    sget-object v0, LS/d;->c:LE/d;

    invoke-virtual {v0, v6, v7}, LE/d;->a(SS)V

    sget-object v0, LS/d;->d:LE/d;

    invoke-virtual {v0, v4, v5}, LE/d;->a(SS)V

    sget-object v0, LS/d;->d:LE/d;

    invoke-virtual {v0, v5, v7}, LE/d;->a(SS)V

    sget-object v0, LS/d;->d:LE/d;

    invoke-virtual {v0, v7, v6}, LE/d;->a(SS)V

    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v4}, LE/d;->a(S)V

    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v5}, LE/d;->a(S)V

    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v6}, LE/d;->a(S)V

    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v7}, LE/d;->a(S)V

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    const/4 v0, 0x0

    const/16 v2, 0x8

    invoke-direct {p0}, LS/a;-><init>()V

    iput-object v0, p0, LS/d;->h:Ljava/lang/String;

    iput-object v0, p0, LS/d;->i:Ljava/lang/String;

    const v0, 0x7f0b004f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LS/d;->f:F

    const v0, 0x7f0b004e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LS/d;->a:I

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    new-instance v0, LE/i;

    invoke-direct {v0, v2}, LE/i;-><init>(I)V

    iput-object v0, p0, LS/d;->q:LE/i;

    new-instance v0, LE/i;

    invoke-direct {v0, v2}, LE/i;-><init>(I)V

    iput-object v0, p0, LS/d;->r:LE/i;

    new-instance v0, Lh/r;

    const-wide/16 v1, 0x96

    invoke-direct {v0, v1, v2}, Lh/r;-><init>(J)V

    iput-object v0, p0, LS/d;->s:Lh/r;

    iget-object v0, p0, LS/d;->s:Lh/r;

    const/16 v1, 0xa

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Lh/r;->b(II)V

    const v0, 0x7f0b0085

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LS/d;->t:F

    return-void
.end method

.method private a(LD/a;IFFFFZ)V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v0, p3, p4, p5, p6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    int-to-float v1, p2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    int-to-float v1, p2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glPointSize(F)V

    iget v1, p0, LS/d;->m:F

    invoke-interface {v0, v1, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    sget-object v1, LS/d;->c:LE/d;

    invoke-virtual {v1, p1, v5}, LE/d;->a(LD/a;I)V

    if-eqz p7, :cond_0

    sget-object v1, LS/d;->e:LE/d;

    invoke-virtual {v1, p1, v4}, LE/d;->a(LD/a;I)V

    :cond_0
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v1, p0, LS/d;->l:F

    invoke-interface {v0, v1, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/high16 v1, -0x3e900000

    invoke-interface {v0, v3, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    sget-object v1, LS/d;->d:LE/d;

    invoke-virtual {v1, p1, v5}, LE/d;->a(LD/a;I)V

    if-eqz p7, :cond_1

    sget-object v1, LS/d;->e:LE/d;

    invoke-virtual {v1, p1, v4}, LE/d;->a(LD/a;I)V

    :cond_1
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method private d(LD/a;)V
    .locals 9

    const/4 v8, 0x1

    const/high16 v7, 0x40000000

    const/high16 v6, 0x3f800000

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p1}, LD/a;->p()V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LS/d;->q:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    invoke-interface {v0, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v1, p0, LS/d;->k:[F

    aget v1, v1, v4

    iget-object v2, p0, LS/d;->k:[F

    aget v2, v2, v8

    invoke-interface {v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v1, p0, LS/d;->o:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, LS/d;->r:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    const/high16 v1, 0x3fc00000

    iget v2, p0, LS/d;->f:F

    mul-float/2addr v1, v2

    neg-float v1, v1

    invoke-interface {v0, v7, v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v1, p0, LS/d;->j:[F

    aget v1, v1, v4

    iget-object v2, p0, LS/d;->j:[F

    aget v2, v2, v8

    invoke-interface {v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v1, p0, LS/d;->p:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, LD/a;->q()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method private j()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LS/d;->o:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LS/d;->o:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    iput-object v1, p0, LS/d;->o:LD/b;

    :cond_0
    iget-object v0, p0, LS/d;->p:LD/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, LS/d;->p:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    iput-object v1, p0, LS/d;->p:LD/b;

    :cond_1
    return-void
.end method

.method private k()V
    .locals 11

    iget v0, p0, LS/d;->a:I

    int-to-double v0, v0

    iget-wide v2, p0, LS/d;->g:D

    div-double v3, v0, v2

    const-wide v0, 0x400a3f28fd4f4b98L

    mul-double v5, v3, v0

    const-string v0, ""

    const-string v1, ""

    sget-object v2, Lcom/google/googlenav/common/util/u;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_0

    sget-object v7, Lcom/google/googlenav/common/util/u;->a:[I

    aget v7, v7, v2

    int-to-double v7, v7

    sub-double v7, v3, v7

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-ltz v7, :cond_5

    sget-object v0, Lcom/google/googlenav/common/util/u;->a:[I

    aget v2, v0, v2

    const/16 v0, 0x3e8

    if-ge v2, v0, :cond_4

    const/16 v0, 0x102

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    int-to-double v2, v2

    iget-wide v7, p0, LS/d;->g:D

    mul-double/2addr v2, v7

    double-to-float v2, v2

    iput v2, p0, LS/d;->l:F

    :cond_0
    sget-object v2, Lcom/google/googlenav/common/util/u;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_2
    if-ltz v2, :cond_1

    sget-object v3, Lcom/google/googlenav/common/util/u;->b:[I

    aget v3, v3, v2

    int-to-double v3, v3

    sub-double v3, v5, v3

    const-wide/16 v7, 0x0

    cmpl-double v3, v3, v7

    if-ltz v3, :cond_7

    sget-object v1, Lcom/google/googlenav/common/util/u;->b:[I

    aget v2, v1, v2

    const/16 v1, 0x14a0

    if-ge v2, v1, :cond_6

    const/16 v1, 0x100

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    iget-wide v3, p0, LS/d;->g:D

    const-wide v5, 0x400a3f28fd4f4b98L

    div-double/2addr v3, v5

    int-to-double v5, v2

    mul-double v2, v3, v5

    double-to-float v2, v2

    iput v2, p0, LS/d;->m:F

    :cond_1
    iget-object v2, p0, LS/d;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iput-object v1, p0, LS/d;->i:Ljava/lang/String;

    iget-object v1, p0, LS/d;->o:LD/b;

    if-eqz v1, :cond_2

    iget-object v1, p0, LS/d;->o:LD/b;

    invoke-virtual {v1}, LD/b;->g()V

    const/4 v1, 0x0

    iput-object v1, p0, LS/d;->o:LD/b;

    :cond_2
    iget-object v1, p0, LS/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iput-object v0, p0, LS/d;->h:Ljava/lang/String;

    iget-object v0, p0, LS/d;->p:LD/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, LS/d;->p:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, LS/d;->p:LD/b;

    :cond_3
    return-void

    :cond_4
    const/16 v0, 0x101

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    div-int/lit16 v7, v2, 0x3e8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_0

    :cond_6
    const/16 v1, 0x103

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    div-int/lit16 v5, v2, 0x14a0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_7
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_2
.end method


# virtual methods
.method a(LD/a;LC/a;)V
    .locals 8

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, LS/d;->s:Lh/r;

    invoke-virtual {v1, p1}, Lh/r;->a(LD/a;)V

    iget-object v1, p0, LS/d;->s:Lh/r;

    invoke-virtual {v1}, Lh/r;->a()F

    move-result v1

    iget-object v2, p0, LS/d;->s:Lh/r;

    invoke-virtual {v2}, Lh/r;->b()F

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-direct {p0, p1}, LS/d;->d(LD/a;)V

    sget-object v0, LS/d;->b:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    const/4 v2, 0x5

    const/high16 v3, 0x3f800000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const v6, 0x3e4ccccd

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LS/d;->a(LD/a;IFFFFZ)V

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LS/d;->a(LD/a;IFFFFZ)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 12

    const/4 v11, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/high16 v6, -0x1000000

    const/4 v10, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, LS/d;->g:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    iget-object v0, p0, LS/d;->o:LD/b;

    if-nez v0, :cond_2

    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LS/d;->i:Ljava/lang/String;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v5, p0, LS/d;->f:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    iput-object v0, p0, LS/d;->o:LD/b;

    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LS/d;->i:Ljava/lang/String;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v4, p0, LS/d;->f:F

    move v5, v11

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, LS/d;->k:[F

    iget-object v0, p0, LS/d;->q:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v0, p0, LS/d;->o:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    iget-object v1, p0, LS/d;->o:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v10, v10}, LE/i;->a(FF)V

    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v10, v1}, LE/i;->a(FF)V

    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v0, v10}, LE/i;->a(FF)V

    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v0, v1}, LE/i;->a(FF)V

    :cond_2
    iget-object v0, p0, LS/d;->p:LD/b;

    if-nez v0, :cond_3

    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LS/d;->h:Ljava/lang/String;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v5, p0, LS/d;->f:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    iput-object v0, p0, LS/d;->p:LD/b;

    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LS/d;->h:Ljava/lang/String;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v4, p0, LS/d;->f:F

    move v5, v11

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, LS/d;->j:[F

    iget-object v0, p0, LS/d;->r:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v0, p0, LS/d;->p:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    iget-object v1, p0, LS/d;->p:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v10, v10}, LE/i;->a(FF)V

    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v10, v1}, LE/i;->a(FF)V

    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v0, v10}, LE/i;->a(FF)V

    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v0, v1}, LE/i;->a(FF)V

    :cond_3
    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p0, p1, p2}, LS/d;->a(LD/a;LC/a;)V

    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0
.end method

.method public b(LC/a;LD/a;)Z
    .locals 4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v0

    invoke-virtual {p1}, LC/a;->y()F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, LS/d;->g:D

    invoke-direct {p0}, LS/d;->k()V

    :cond_0
    invoke-super {p0, p1, p2}, LS/a;->b(LC/a;LD/a;)Z

    move-result v0

    return v0
.end method

.method public c(LD/a;)V
    .locals 1

    invoke-direct {p0}, LS/d;->j()V

    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    return-void
.end method

.method public i()V
    .locals 3

    const/16 v0, 0x1e

    invoke-virtual {p0}, LS/d;->h()LS/b;

    move-result-object v1

    invoke-virtual {v1}, LS/b;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v0, 0x64

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->au()Z

    move-result v1

    if-eqz v1, :cond_0

    int-to-float v0, v0

    iget v1, p0, LS/d;->t:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->at()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x60

    :cond_1
    iget-object v1, p0, LS/d;->s:Lh/r;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lh/r;->a(II)V

    return-void
.end method
