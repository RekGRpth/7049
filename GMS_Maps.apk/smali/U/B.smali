.class public final enum LU/B;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LU/B;

.field public static final enum b:LU/B;

.field public static final enum c:LU/B;

.field private static final synthetic d:[LU/B;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LU/B;

    const-string v1, "MISSED_STOP"

    invoke-direct {v0, v1, v2}, LU/B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/B;->a:LU/B;

    new-instance v0, LU/B;

    const-string v1, "WRONG_WAY"

    invoke-direct {v0, v1, v3}, LU/B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/B;->b:LU/B;

    new-instance v0, LU/B;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LU/B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/B;->c:LU/B;

    const/4 v0, 0x3

    new-array v0, v0, [LU/B;

    sget-object v1, LU/B;->a:LU/B;

    aput-object v1, v0, v2

    sget-object v1, LU/B;->b:LU/B;

    aput-object v1, v0, v3

    sget-object v1, LU/B;->c:LU/B;

    aput-object v1, v0, v4

    sput-object v0, LU/B;->d:[LU/B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU/B;
    .locals 1

    const-class v0, LU/B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU/B;

    return-object v0
.end method

.method public static values()[LU/B;
    .locals 1

    sget-object v0, LU/B;->d:[LU/B;

    invoke-virtual {v0}, [LU/B;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU/B;

    return-object v0
.end method
