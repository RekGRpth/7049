.class public LU/C;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/T;
.implements LU/x;


# instance fields
.field private final a:Lbi/d;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final c:Lcom/google/googlenav/common/a;

.field private final d:LX/i;

.field private final e:LU/q;

.field private final f:LU/Y;

.field private final g:LU/F;

.field private final h:LU/F;

.field private i:Z


# direct methods
.method public constructor <init>(Lbi/d;LU/q;Lcom/google/googlenav/common/a;LX/i;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, LU/C;->c:Lcom/google/googlenav/common/a;

    iput-object p1, p0, LU/C;->a:Lbi/d;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LU/C;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p2, p0, LU/C;->e:LU/q;

    iput-object p4, p0, LU/C;->d:LX/i;

    new-instance v0, LU/Y;

    invoke-direct {v0}, LU/Y;-><init>()V

    iput-object v0, p0, LU/C;->f:LU/Y;

    new-instance v0, LU/f;

    invoke-direct {v0, p1}, LU/f;-><init>(Lbi/d;)V

    iput-object v0, p0, LU/C;->g:LU/F;

    new-instance v0, LU/W;

    iget-object v1, p0, LU/C;->f:LU/Y;

    invoke-direct {v0, p1, v1}, LU/W;-><init>(Lbi/d;LU/Y;)V

    iput-object v0, p0, LU/C;->h:LU/F;

    invoke-virtual {p2, p0}, LU/q;->a(LU/T;)V

    return-void
.end method

.method static synthetic a(LU/C;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, LU/C;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private a(LU/z;)V
    .locals 2

    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0}, LU/Y;->a()LU/z;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, LU/C;->b(LU/z;)V

    :cond_0
    :goto_0
    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0, p1}, LU/Y;->a(LU/z;)V

    iget-object v0, p0, LU/C;->d:LX/i;

    new-instance v1, LU/D;

    invoke-direct {v1, p0, p1}, LU/D;-><init>(LU/C;LU/z;)V

    invoke-interface {v0, v1}, LX/i;->b(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    iget-boolean v0, p0, LU/C;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0}, LU/Y;->a()LU/z;

    move-result-object v0

    iget-object v0, v0, LU/z;->a:LU/R;

    invoke-virtual {v0}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->c:LU/S;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LU/z;->a:LU/R;

    invoke-virtual {v0}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->c:LU/S;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0}, LU/Y;->a()LU/z;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LU/C;->a(LU/z;LU/z;)V

    goto :goto_0
.end method

.method private a(LU/z;LU/z;)V
    .locals 9

    const-string v1, ""

    const-string v0, ""

    sget-object v2, LU/E;->c:[I

    iget-object v3, p2, LU/z;->b:LU/A;

    invoke-virtual {v3}, LU/A;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v2, p1, LU/z;->a:LU/R;

    invoke-virtual {v2}, LU/R;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "T"

    :goto_1
    const/16 v3, 0x61

    const-string v4, "t"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "t="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "s="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "d="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v1, "o"

    goto :goto_0

    :pswitch_1
    const-string v1, "c"

    iget-object v2, p1, LU/z;->b:LU/A;

    sget-object v3, LU/A;->a:LU/A;

    if-ne v2, v3, :cond_0

    iget-object v0, p0, LU/C;->a:Lbi/d;

    iget-object v2, p1, LU/z;->c:Lbi/a;

    iget-object v3, p2, LU/z;->c:Lbi/a;

    invoke-static {v0, v2, v3}, LX/g;->a(Lbi/d;Lbi/a;Lbi/a;)F

    move-result v0

    invoke-static {v0}, LX/r;->a(F)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v2, "F"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0}, LU/Y;->c()LU/R;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0}, LU/Y;->c()LU/R;

    move-result-object v0

    invoke-virtual {v0}, LU/R;->a()LU/S;

    move-result-object v0

    invoke-virtual {v0}, LU/S;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(J)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, LU/C;->e:LU/q;

    const-string v2, "speed_provider"

    invoke-virtual {v0, v2}, LU/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/C;->f:LU/Y;

    invoke-virtual {v0}, LU/Y;->e()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    sub-long v2, p1, v2

    const-wide/16 v4, 0x7530

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LU/C;->i:Z

    iget-object v0, p0, LU/C;->e:LU/q;

    const-string v2, "speed_provider"

    invoke-virtual {v0, v2, v1}, LU/q;->a(Ljava/lang/String;Z)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private b(LU/z;)V
    .locals 8

    const-string v0, ""

    sget-object v1, LU/E;->b:[I

    iget-object v2, p1, LU/z;->a:LU/R;

    invoke-virtual {v2}, LU/R;->a()LU/S;

    move-result-object v2

    invoke-virtual {v2}, LU/S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, LU/C;->a:Lbi/d;

    invoke-static {v1, p1}, LX/r;->a(Lbi/d;LU/z;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x61

    const-string v3, "fl"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "s="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "m="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v0, "g"

    goto :goto_0

    :pswitch_1
    const-string v0, "n"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(LU/R;)V
    .locals 3

    iget-object v0, p0, LU/C;->g:LU/F;

    invoke-interface {v0, p1}, LU/F;->a(LU/R;)LU/G;

    move-result-object v0

    iget-object v1, v0, LU/G;->a:LU/H;

    sget-object v2, LU/H;->b:LU/H;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, LU/R;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LU/C;->a(J)LU/B;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LU/C;->a(LU/R;LU/B;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, LU/G;->a:LU/H;

    sget-object v2, LU/H;->c:LU/H;

    if-ne v1, v2, :cond_1

    iget-object v0, v0, LU/G;->b:Lbi/a;

    new-instance v1, Lbi/c;

    invoke-direct {v1, v0, v0}, Lbi/c;-><init>(Lbi/a;Lbi/a;)V

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, LU/C;->a(LU/R;Lbi/a;Lbi/c;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, v0, LU/G;->b:Lbi/a;

    new-instance v1, Lbi/c;

    invoke-direct {v1, v0, v0}, Lbi/c;-><init>(Lbi/a;Lbi/a;)V

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v0, v1, v2}, LU/C;->a(LU/R;Lbi/a;Lbi/c;Z)V

    goto :goto_0
.end method

.method private d(LU/R;)V
    .locals 3

    iget-object v0, p0, LU/C;->h:LU/F;

    invoke-interface {v0, p1}, LU/F;->a(LU/R;)LU/G;

    move-result-object v0

    iget-object v1, v0, LU/G;->a:LU/H;

    sget-object v2, LU/H;->a:LU/H;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, p1}, LU/C;->b(LU/R;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, v0, LU/G;->b:Lbi/a;

    new-instance v1, Lbi/c;

    iget-object v2, p0, LU/C;->f:LU/Y;

    invoke-virtual {v2}, LU/Y;->b()LU/z;

    move-result-object v2

    iget-object v2, v2, LU/z;->c:Lbi/a;

    invoke-direct {v1, v2, v0}, Lbi/c;-><init>(Lbi/a;Lbi/a;)V

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, LU/C;->a(LU/R;Lbi/a;Lbi/c;Z)V

    goto :goto_0
.end method


# virtual methods
.method a(J)LU/B;
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LU/C;->f:LU/Y;

    invoke-virtual {v2}, LU/Y;->b()LU/z;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v0, LU/B;->c:LU/B;

    :goto_0
    return-object v0

    :cond_0
    iget-object v3, v2, LU/z;->c:Lbi/a;

    iget-object v3, v3, Lbi/a;->a:Lbi/t;

    iget-object v4, p0, LU/C;->a:Lbi/d;

    invoke-virtual {v4, v3}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v4

    iget-object v5, v2, LU/z;->a:LU/R;

    invoke-virtual {v5}, LU/R;->getTime()J

    move-result-wide v5

    sub-long v5, p1, v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-virtual {v4}, Lbi/h;->q()I

    move-result v7

    invoke-virtual {v4}, Lbi/h;->A()I

    move-result v8

    invoke-virtual {v4}, Lbi/h;->p()Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    :cond_1
    sget-object v0, LU/B;->c:LU/B;

    goto :goto_0

    :cond_2
    int-to-float v7, v7

    iget-object v2, v2, LU/z;->c:Lbi/a;

    iget v2, v2, Lbi/a;->b:F

    mul-float/2addr v2, v7

    float-to-int v2, v2

    sub-int v2, v8, v2

    int-to-long v7, v2

    cmp-long v2, v5, v7

    if-lez v2, :cond_3

    sget-object v0, LU/B;->c:LU/B;

    goto :goto_0

    :cond_3
    sget-object v2, LU/E;->a:[I

    invoke-virtual {v4}, Lbi/h;->b()Lbi/q;

    move-result-object v4

    invoke-virtual {v4}, Lbi/q;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    sget-object v0, LU/B;->a:LU/B;

    goto :goto_0

    :pswitch_0
    sget-object v0, LU/B;->b:LU/B;

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, LU/C;->a:Lbi/d;

    invoke-virtual {v2, v3}, Lbi/d;->d(Lbi/t;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    :goto_1
    if-eqz v0, :cond_6

    sget-object v0, LU/B;->b:LU/B;

    goto :goto_0

    :cond_5
    iget-object v2, p0, LU/C;->a:Lbi/d;

    iget v3, v3, Lbi/t;->a:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lbi/d;->a(I)Lbi/g;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbi/g;->a(I)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->b()Lbi/q;

    move-result-object v2

    sget-object v3, Lbi/q;->a:Lbi/q;

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_1

    :cond_6
    sget-object v0, LU/B;->a:LU/B;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(LU/R;)V
    .locals 2

    invoke-virtual {p1}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->a:LU/S;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->b:LU/S;

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-direct {p0, p1}, LU/C;->c(LU/R;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->c:LU/S;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, LU/C;->d(LU/R;)V

    goto :goto_0
.end method

.method a(LU/R;LU/B;)V
    .locals 2

    invoke-virtual {p1}, LU/R;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LU/C;->b(J)V

    invoke-static {p1, p2}, LU/z;->a(LU/R;LU/B;)LU/z;

    move-result-object v0

    invoke-direct {p0, v0}, LU/C;->a(LU/z;)V

    return-void
.end method

.method a(LU/R;Lbi/a;Lbi/c;Z)V
    .locals 3

    invoke-static {p1, p2, p3, p4}, LU/z;->a(LU/R;Lbi/a;Lbi/c;Z)LU/z;

    move-result-object v0

    invoke-virtual {p1}, LU/R;->a()LU/S;

    move-result-object v1

    sget-object v2, LU/S;->c:LU/S;

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, LU/R;->getTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, LU/C;->b(J)V

    :cond_0
    invoke-direct {p0, v0}, LU/C;->a(LU/z;)V

    return-void
.end method

.method public a(LU/y;)V
    .locals 1

    iget-object v0, p0, LU/C;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "gps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "network"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-ne p3, v2, :cond_1

    invoke-direct {p0, p2}, LU/C;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, LU/C;->i:Z

    iget-object v0, p0, LU/C;->e:LU/q;

    const-string v1, "speed_provider"

    invoke-virtual {v0, v1, v2}, LU/q;->a(Ljava/lang/String;Z)Z

    :cond_1
    return-void
.end method

.method b(LU/R;)V
    .locals 2

    invoke-virtual {p1}, LU/R;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LU/C;->b(J)V

    invoke-static {p1}, LU/z;->a(LU/R;)LU/z;

    move-result-object v0

    invoke-direct {p0, v0}, LU/C;->a(LU/z;)V

    return-void
.end method

.method public b(LU/y;)V
    .locals 1

    iget-object v0, p0, LU/C;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, "integrated_location_provider"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LU/R;

    new-instance v1, Landroid/location/Location;

    const-string v2, "integrated_location_provider"

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LU/R;-><init>(Landroid/location/Location;)V

    sget-object v1, LU/S;->c:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    iget-object v1, p0, LU/C;->c:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LU/R;->setTime(J)V

    invoke-virtual {p0, v0}, LU/C;->b(LU/R;)V

    :cond_0
    return-void
.end method
