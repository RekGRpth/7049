.class public LU/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/x;


# instance fields
.field private final a:Lbi/d;

.field private final b:Ljava/util/ArrayList;

.field private c:Lbi/a;

.field private d:F

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;

.field private g:Z


# direct methods
.method public constructor <init>(Lbi/d;LU/L;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LU/I;->a:Lbi/d;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LU/I;->e:Landroid/os/Handler;

    new-instance v0, LU/J;

    invoke-direct {v0, p0, p2}, LU/J;-><init>(LU/I;LU/L;)V

    iput-object v0, p0, LU/I;->f:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(LU/I;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, LU/I;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(LU/I;LU/z;)V
    .locals 0

    invoke-direct {p0, p1}, LU/I;->a(LU/z;)V

    return-void
.end method

.method private a(LU/z;)V
    .locals 2

    iget-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/y;

    invoke-interface {v0, p1}, LU/y;->a(LU/z;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(LU/I;)Z
    .locals 1

    iget-boolean v0, p0, LU/I;->g:Z

    return v0
.end method

.method private c()LU/z;
    .locals 4

    const-wide/high16 v2, 0x3fe0000000000000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-direct {p0}, LU/I;->h()V

    invoke-direct {p0}, LU/I;->d()LU/z;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    invoke-direct {p0}, LU/I;->e()LU/z;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, LU/I;->f()LU/z;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(LU/I;)LU/z;
    .locals 1

    invoke-direct {p0}, LU/I;->c()LU/z;

    move-result-object v0

    return-object v0
.end method

.method private d()LU/z;
    .locals 5

    invoke-direct {p0}, LU/I;->g()LU/R;

    move-result-object v0

    iget-object v1, p0, LU/I;->c:Lbi/a;

    new-instance v2, Lbi/c;

    iget-object v3, p0, LU/I;->c:Lbi/a;

    iget-object v4, p0, LU/I;->c:Lbi/a;

    invoke-direct {v2, v3, v4}, Lbi/c;-><init>(Lbi/a;Lbi/a;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LU/z;->a(LU/R;Lbi/a;Lbi/c;Z)LU/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(LU/I;)V
    .locals 0

    invoke-direct {p0}, LU/I;->h()V

    return-void
.end method

.method private e()LU/z;
    .locals 1

    invoke-direct {p0}, LU/I;->g()LU/R;

    move-result-object v0

    invoke-static {v0}, LU/z;->a(LU/R;)LU/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(LU/I;)LU/z;
    .locals 1

    invoke-direct {p0}, LU/I;->d()LU/z;

    move-result-object v0

    return-object v0
.end method

.method private f()LU/z;
    .locals 2

    invoke-direct {p0}, LU/I;->g()LU/R;

    move-result-object v0

    sget-object v1, LU/B;->c:LU/B;

    invoke-static {v0, v1}, LU/z;->a(LU/R;LU/B;)LU/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(LU/I;)V
    .locals 0

    invoke-direct {p0}, LU/I;->i()V

    return-void
.end method

.method private g()LU/R;
    .locals 3

    new-instance v0, LU/R;

    new-instance v1, Landroid/location/Location;

    const-string v2, "gps"

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LU/R;-><init>(Landroid/location/Location;)V

    sget-object v1, LU/S;->a:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LU/R;->setTime(J)V

    return-object v0
.end method

.method static synthetic g(LU/I;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, LU/I;->e:Landroid/os/Handler;

    return-object v0
.end method

.method private h()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, LU/I;->c:Lbi/a;

    if-nez v0, :cond_1

    new-instance v0, Lbi/a;

    invoke-direct {v0, v1, v1, v4}, Lbi/a;-><init>(IIF)V

    iput-object v0, p0, LU/I;->c:Lbi/a;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LU/I;->c:Lbi/a;

    iget-object v0, v0, Lbi/a;->a:Lbi/t;

    new-instance v1, Lbi/v;

    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-direct {v1, v2}, Lbi/v;-><init>(Lbi/d;)V

    invoke-virtual {v1, v0}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v1

    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-virtual {v2, v0}, Lbi/d;->e(Lbi/t;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, LU/I;->d:F

    const v3, 0x3e4ccccd

    add-float/2addr v2, v3

    iput v2, p0, LU/I;->d:F

    iget v2, p0, LU/I;->d:F

    const/high16 v3, 0x3f800000

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    invoke-virtual {v1}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    iput v4, p0, LU/I;->d:F

    :cond_2
    new-instance v1, Lbi/a;

    iget v2, p0, LU/I;->d:F

    invoke-direct {v1, v0, v2}, Lbi/a;-><init>(Lbi/t;F)V

    iput-object v1, p0, LU/I;->c:Lbi/a;

    goto :goto_0
.end method

.method private i()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, LU/I;->c:Lbi/a;

    if-nez v0, :cond_0

    new-instance v0, Lbi/a;

    invoke-direct {v0, v1, v1, v4}, Lbi/a;-><init>(IIF)V

    iput-object v0, p0, LU/I;->c:Lbi/a;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LU/I;->c:Lbi/a;

    iget-object v1, v0, Lbi/a;->a:Lbi/t;

    iget-object v0, p0, LU/I;->a:Lbi/d;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LU/I;->a:Lbi/d;

    iget-object v2, p0, LU/I;->c:Lbi/a;

    const/high16 v3, 0x41a00000

    invoke-static {v0, v2, v3}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    :goto_1
    iget-object v2, v0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v2, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Lbi/v;

    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-direct {v0, v2}, Lbi/v;-><init>(Lbi/d;)V

    invoke-virtual {v0, v1}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v0

    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-virtual {v2, v1}, Lbi/d;->e(Lbi/t;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    :goto_2
    new-instance v1, Lbi/a;

    invoke-direct {v1, v0, v4}, Lbi/a;-><init>(Lbi/t;F)V

    move-object v0, v1

    :cond_1
    iput-object v0, p0, LU/I;->c:Lbi/a;

    goto :goto_0

    :cond_2
    iget-object v0, p0, LU/I;->a:Lbi/d;

    iget-object v2, p0, LU/I;->c:Lbi/a;

    const/high16 v3, 0x43160000

    invoke-static {v0, v2, v3}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, LU/I;->e:Landroid/os/Handler;

    iget-object v1, p0, LU/I;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, LU/I;->f:Ljava/lang/Runnable;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LU/I;->g:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(LU/y;)V
    .locals 1

    iget-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, LU/I;->e:Landroid/os/Handler;

    iget-object v1, p0, LU/I;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b(LU/y;)V
    .locals 1

    iget-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
