.class public final enum LU/L;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LU/L;

.field public static final enum b:LU/L;

.field public static final enum c:LU/L;

.field private static final synthetic d:[LU/L;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LU/L;

    const-string v1, "RANDOMIZE_EVENTS"

    invoke-direct {v0, v1, v2}, LU/L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/L;->a:LU/L;

    new-instance v0, LU/L;

    const-string v1, "CONSTANT_PROGRESS_INCREMENT"

    invoke-direct {v0, v1, v3}, LU/L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/L;->b:LU/L;

    new-instance v0, LU/L;

    const-string v1, "CONSTANT_SPEED"

    invoke-direct {v0, v1, v4}, LU/L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/L;->c:LU/L;

    const/4 v0, 0x3

    new-array v0, v0, [LU/L;

    sget-object v1, LU/L;->a:LU/L;

    aput-object v1, v0, v2

    sget-object v1, LU/L;->b:LU/L;

    aput-object v1, v0, v3

    sget-object v1, LU/L;->c:LU/L;

    aput-object v1, v0, v4

    sput-object v0, LU/L;->d:[LU/L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU/L;
    .locals 1

    const-class v0, LU/L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU/L;

    return-object v0
.end method

.method public static values()[LU/L;
    .locals 1

    sget-object v0, LU/L;->d:[LU/L;

    invoke-virtual {v0}, [LU/L;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU/L;

    return-object v0
.end method
