.class public LU/U;
.super LU/b;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Runnable;

.field private final b:Lcom/google/googlenav/common/a;

.field private final c:LX/i;

.field private d:[F

.field private e:[J

.field private f:I

.field private volatile g:F


# direct methods
.method public constructor <init>(LU/l;Lcom/google/googlenav/common/a;)V
    .locals 3

    const/16 v2, 0xa

    const/high16 v1, -0x40800000

    const-string v0, "speed_provider"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    const/4 v0, 0x0

    iput v0, p0, LU/U;->f:I

    iput v1, p0, LU/U;->g:F

    new-instance v0, LU/V;

    invoke-direct {v0, p0}, LU/V;-><init>(LU/U;)V

    iput-object v0, p0, LU/U;->a:Ljava/lang/Runnable;

    iput-object p2, p0, LU/U;->b:Lcom/google/googlenav/common/a;

    invoke-interface {p1}, LU/l;->a()LX/i;

    move-result-object v0

    iput-object v0, p0, LU/U;->c:LX/i;

    new-array v0, v2, [F

    iput-object v0, p0, LU/U;->d:[F

    new-array v0, v2, [J

    iput-object v0, p0, LU/U;->e:[J

    iget-object v0, p0, LU/U;->d:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    return-void
.end method

.method static synthetic a(LU/U;)F
    .locals 1

    iget v0, p0, LU/U;->g:F

    return v0
.end method

.method static a()J
    .locals 2

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->v:I

    int-to-long v0, v0

    return-wide v0
.end method

.method static synthetic a(LU/U;LU/R;)V
    .locals 0

    invoke-super {p0, p1}, LU/b;->a(LU/R;)V

    return-void
.end method

.method static synthetic a(LU/U;Landroid/location/Location;F)V
    .locals 0

    invoke-direct {p0, p1, p2}, LU/U;->a(Landroid/location/Location;F)V

    return-void
.end method

.method private a(Landroid/location/Location;F)V
    .locals 1

    const/high16 v0, -0x40800000

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->removeSpeed()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/location/Location;->setSpeed(F)V

    goto :goto_0
.end method

.method static synthetic b(LU/U;)Lcom/google/googlenav/common/a;
    .locals 1

    iget-object v0, p0, LU/U;->b:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method static synthetic c(LU/U;)LX/i;
    .locals 1

    iget-object v0, p0, LU/U;->c:LX/i;

    return-object v0
.end method

.method private g()F
    .locals 9

    const/4 v0, 0x0

    const/high16 v2, -0x40800000

    const/4 v1, 0x0

    iget-object v3, p0, LU/U;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    const-wide/32 v5, 0x493e0

    sub-long v4, v3, v5

    move v3, v0

    move v8, v1

    move v1, v0

    move v0, v8

    :goto_0
    const/16 v6, 0xa

    if-ge v3, v6, :cond_1

    iget-object v6, p0, LU/U;->d:[F

    aget v6, v6, v3

    cmpl-float v6, v6, v2

    if-eqz v6, :cond_0

    iget-object v6, p0, LU/U;->e:[J

    aget-wide v6, v6, v3

    cmp-long v6, v6, v4

    if-ltz v6, :cond_0

    iget-object v6, p0, LU/U;->d:[F

    aget v6, v6, v3

    add-float/2addr v0, v6

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    if-ge v1, v3, :cond_2

    move v0, v2

    :goto_1
    iput v0, p0, LU/U;->g:F

    iget v0, p0, LU/U;->g:F

    return v0

    :cond_2
    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(LU/R;)V
    .locals 4

    iget-object v1, p0, LU/U;->d:[F

    iget v2, p0, LU/U;->f:I

    invoke-virtual {p1}, LU/R;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LU/R;->getSpeed()F

    move-result v0

    :goto_0
    aput v0, v1, v2

    iget-object v0, p0, LU/U;->e:[J

    iget v1, p0, LU/U;->f:I

    invoke-virtual {p1}, LU/R;->getTime()J

    move-result-wide v2

    aput-wide v2, v0, v1

    iget v0, p0, LU/U;->f:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, LU/U;->f:I

    invoke-virtual {p0}, LU/U;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LU/S;->c:LU/S;

    invoke-virtual {p1, v0}, LU/R;->a(LU/S;)V

    invoke-direct {p0}, LU/U;->g()F

    move-result v0

    invoke-direct {p0, p1, v0}, LU/U;->a(Landroid/location/Location;F)V

    invoke-super {p0, p1}, LU/b;->a(LU/R;)V

    :cond_0
    return-void

    :cond_1
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method public b()V
    .locals 2

    invoke-virtual {p0}, LU/U;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, LU/b;->b()V

    invoke-direct {p0}, LU/U;->g()F

    iget-object v0, p0, LU/U;->c:LX/i;

    iget-object v1, p0, LU/U;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, LX/i;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, LU/U;->c:LX/i;

    iget-object v1, p0, LU/U;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, LX/i;->b(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public c()V
    .locals 0

    invoke-virtual {p0}, LU/U;->d()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, LU/U;->c:LX/i;

    iget-object v1, p0, LU/U;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, LX/i;->a(Ljava/lang/Runnable;)V

    invoke-super {p0}, LU/b;->d()V

    return-void
.end method
