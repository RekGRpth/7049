.class LU/Y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LU/z;

.field private b:LU/z;

.field private c:LU/z;

.field private d:LU/R;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()LU/z;
    .locals 1

    iget-object v0, p0, LU/Y;->a:LU/z;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, LU/Y;->f:J

    return-void
.end method

.method public a(LU/z;)V
    .locals 3

    iget-object v0, p1, LU/z;->a:LU/R;

    iget-object v1, p1, LU/z;->b:LU/A;

    sget-object v2, LU/A;->a:LU/A;

    if-ne v1, v2, :cond_2

    iput-object v0, p0, LU/Y;->d:LU/R;

    iget-object v1, p0, LU/Y;->b:LU/z;

    if-eqz v1, :cond_0

    iget-object v1, p0, LU/Y;->b:LU/z;

    iget-object v1, v1, LU/z;->c:Lbi/a;

    iget-object v1, v1, Lbi/a;->a:Lbi/t;

    iget-object v2, p1, LU/z;->c:Lbi/a;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {v0}, LU/R;->getTime()J

    move-result-wide v1

    iput-wide v1, p0, LU/Y;->e:J

    :cond_1
    iput-object p1, p0, LU/Y;->b:LU/z;

    invoke-virtual {v0}, LU/R;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p1, p0, LU/Y;->c:LU/z;

    :cond_2
    iput-object p1, p0, LU/Y;->a:LU/z;

    return-void
.end method

.method public b()LU/z;
    .locals 1

    iget-object v0, p0, LU/Y;->c:LU/z;

    return-object v0
.end method

.method public c()LU/R;
    .locals 1

    iget-object v0, p0, LU/Y;->d:LU/R;

    return-object v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, LU/Y;->e:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, LU/Y;->f:J

    return-wide v0
.end method
