.class public LU/a;
.super LU/b;
.source "SourceFile"


# instance fields
.field a:I

.field private b:Z

.field private c:Z

.field private final d:LU/T;

.field private final e:Landroid/location/LocationManager;

.field private final f:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .locals 1

    const-string v0, "base_location_provider"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    const/4 v0, 0x1

    iput v0, p0, LU/a;->a:I

    iput-object p1, p0, LU/a;->d:LU/T;

    iput-object p3, p0, LU/a;->f:Lcom/google/googlenav/common/a;

    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LU/a;->e:Landroid/location/LocationManager;

    return-void
.end method

.method private g()V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, LU/a;->f:Lcom/google/googlenav/common/a;

    instance-of v1, v1, LX/a;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LU/a;->f:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-boolean v1, p0, LU/a;->b:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, LU/a;->e:Landroid/location/LocationManager;

    const-string v4, "gps"

    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-static {}, LU/d;->a()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    new-instance v4, LU/R;

    invoke-direct {v4, v1}, LU/R;-><init>(Landroid/location/Location;)V

    sget-object v1, LU/S;->a:LU/S;

    invoke-virtual {v4, v1}, LU/R;->a(LU/S;)V

    iget-object v1, p0, LU/a;->d:LU/T;

    invoke-interface {v1, v4}, LU/T;->a(LU/R;)V

    :cond_2
    iget-boolean v1, p0, LU/a;->c:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, LU/a;->e:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long v1, v2, v4

    invoke-static {}, LU/a;->h()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    new-instance v1, LU/R;

    invoke-direct {v1, v0}, LU/R;-><init>(Landroid/location/Location;)V

    sget-object v0, LU/S;->b:LU/S;

    invoke-virtual {v1, v0}, LU/R;->a(LU/S;)V

    iget-object v0, p0, LU/a;->d:LU/T;

    invoke-interface {v0, v1}, LU/T;->a(LU/R;)V

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method private static h()J
    .locals 2

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->B:I

    int-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public a(LU/R;)V
    .locals 2

    invoke-virtual {p1}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->b:LU/S;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LU/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, LU/R;->a()LU/S;

    move-result-object v0

    sget-object v1, LU/S;->a:LU/S;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, LU/a;->b:Z

    const/4 v0, 0x2

    iput v0, p0, LU/a;->a:I

    :cond_1
    invoke-super {p0, p1}, LU/b;->a(LU/R;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "gps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, LU/a;->b:Z

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "network"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LU/a;->c:Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "gps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput p3, p0, LU/a;->a:I

    :cond_0
    invoke-super {p0, p1, p2, p3}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method a()Z
    .locals 2

    iget-boolean v0, p0, LU/a;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, LU/a;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, LU/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, LU/b;->b()V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "gps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, LU/a;->b:Z

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, LU/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "network"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LU/a;->c:Z

    goto :goto_0
.end method

.method public c()V
    .locals 0

    invoke-direct {p0}, LU/a;->g()V

    return-void
.end method
