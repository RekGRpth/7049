.class public LU/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/T;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LU/T;

.field private volatile c:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;LU/T;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LU/b;->a:Ljava/lang/String;

    iput-object p2, p0, LU/b;->b:LU/T;

    return-void
.end method


# virtual methods
.method public a(LU/R;)V
    .locals 2

    iget-boolean v0, p0, LU/b;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, LU/R;

    invoke-direct {v0, p1}, LU/R;-><init>(Landroid/location/Location;)V

    iget-object v1, p0, LU/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LU/R;->setProvider(Ljava/lang/String;)V

    iget-object v1, p0, LU/b;->b:LU/T;

    invoke-interface {v1, v0}, LU/T;->a(LU/R;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LU/b;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LU/b;->c:Z

    :cond_0
    iget-object v0, p0, LU/b;->b:LU/T;

    iget-object v1, p0, LU/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, LU/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    iget-boolean v0, p0, LU/b;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/b;->b:LU/T;

    iget-object v1, p0, LU/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p2, p3}, LU/T;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, LU/b;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LU/b;->a:Ljava/lang/String;

    iget-object v1, p0, LU/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LU/b;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LU/b;->c:Z

    :cond_0
    iget-object v0, p0, LU/b;->b:LU/T;

    iget-object v1, p0, LU/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, LU/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 2

    iget-boolean v0, p0, LU/b;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/b;->a:Ljava/lang/String;

    iget-object v1, p0, LU/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LU/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, LU/b;->c:Z

    return v0
.end method

.method public f()V
    .locals 0

    invoke-virtual {p0}, LU/b;->d()V

    return-void
.end method
