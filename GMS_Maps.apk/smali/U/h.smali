.class LU/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final k:Ljava/util/Set;

.field private static l:F

.field private static m:F

.field private static n:F

.field private static o:F


# instance fields
.field public final a:Lbi/d;

.field public final b:Lbi/t;

.field public final c:I

.field public final d:LaN/B;

.field public final e:J

.field public final f:F

.field public final g:F

.field public final h:F

.field public final i:Z

.field public final j:F

.field private final p:Lbi/a;

.field private q:Lbi/a;

.field private r:F

.field private s:F

.field private t:Z

.field private u:Lbi/a;

.field private v:LU/i;

.field private final w:F

.field private x:LU/j;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, LU/j;->c:LU/j;

    sget-object v1, LU/j;->d:LU/j;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LU/h;->k:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lbi/d;Lbi/t;ILU/R;FFZ)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LU/h;->a:Lbi/d;

    iput-object p2, p0, LU/h;->b:Lbi/t;

    iput p3, p0, LU/h;->c:I

    invoke-static {p4}, LX/g;->a(Landroid/location/Location;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LU/h;->d:LaN/B;

    invoke-virtual {p4}, LU/R;->getAccuracy()F

    move-result v0

    iput v0, p0, LU/h;->h:F

    invoke-virtual {p4}, LU/R;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, LU/h;->e:J

    iput p5, p0, LU/h;->f:F

    invoke-direct {p0, p5}, LU/h;->a(F)F

    move-result v0

    iput v0, p0, LU/h;->g:F

    iput p6, p0, LU/h;->j:F

    iput-boolean p7, p0, LU/h;->i:Z

    sget-object v0, LU/i;->b:LU/i;

    iput-object v0, p0, LU/h;->v:LU/i;

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v1, v0, LX/q;->e:F

    iget v2, v0, LX/q;->d:F

    add-float/2addr v1, v2

    sput v1, LU/h;->o:F

    iget v1, v0, LX/q;->f:F

    sput v1, LU/h;->l:F

    iget v1, v0, LX/q;->g:F

    sput v1, LU/h;->m:F

    iget v0, v0, LX/q;->h:F

    sput v0, LU/h;->n:F

    new-instance v0, Lbi/a;

    invoke-direct {p0, v3, v3}, LU/h;->a(ZZ)F

    move-result v1

    invoke-direct {v0, p2, v1}, Lbi/a;-><init>(Lbi/t;F)V

    iput-object v0, p0, LU/h;->p:Lbi/a;

    invoke-direct {p0}, LU/h;->i()F

    move-result v0

    iput v0, p0, LU/h;->w:F

    return-void
.end method

.method private a(F)F
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, LU/h;->h:F

    sub-float v1, p1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(ZZ)F
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, LU/h;->a:Lbi/d;

    iget-object v2, p0, LU/h;->b:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v1

    invoke-virtual {v1}, Lbi/h;->y()F

    move-result v2

    cmpl-float v3, v2, v0

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p2, :cond_2

    iget v3, p0, LU/h;->g:F

    sget v4, LU/h;->o:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    invoke-virtual {p0}, LU/h;->b()F

    move-result v3

    invoke-direct {p0, v3}, LU/h;->b(F)F

    move-result v3

    sub-float v3, v2, v3

    sget v4, LU/h;->o:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    const/high16 v0, 0x3f800000

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    iget v3, p0, LU/h;->g:F

    sget v4, LU/h;->o:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    invoke-virtual {p0}, LU/h;->a()F

    move-result v3

    invoke-direct {p0, v3}, LU/h;->b(F)F

    move-result v3

    sget v4, LU/h;->o:F

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    :cond_3
    iget v0, p0, LU/h;->j:F

    invoke-direct {p0, v0}, LU/h;->b(F)F

    move-result v0

    iget v3, p0, LU/h;->c:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    iget v3, p0, LU/h;->c:I

    invoke-virtual {v1}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_5

    :cond_4
    iget v1, p0, LU/h;->g:F

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    goto :goto_0

    :cond_5
    div-float/2addr v0, v2

    goto :goto_0
.end method

.method private a(Lbi/a;Lbi/a;F)V
    .locals 2

    sget-object v0, LU/i;->a:LU/i;

    iput-object v0, p0, LU/h;->v:LU/i;

    iget v0, p0, LU/h;->w:F

    neg-float v0, v0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_0

    iget-object v0, p0, LU/h;->a:Lbi/d;

    iget v1, p0, LU/h;->w:F

    neg-float v1, v1

    invoke-static {v0, p2, v1}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    iput-object v0, p0, LU/h;->u:Lbi/a;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, LU/h;->u:Lbi/a;

    goto :goto_0
.end method

.method private b(F)F
    .locals 5

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    iget-object v2, p0, LU/h;->a:Lbi/d;

    iget-object v3, p0, LU/h;->b:Lbi/t;

    invoke-virtual {v2, v3}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    iget v3, p0, LU/h;->c:I

    invoke-virtual {v2, v3}, Lbi/h;->d(I)F

    move-result v3

    iget v4, p0, LU/h;->c:I

    invoke-virtual {v2, v4}, Lbi/h;->c(I)F

    move-result v2

    cmpg-float v4, p1, v0

    if-gez v4, :cond_1

    move p1, v0

    :cond_0
    :goto_0
    mul-float v0, v3, p1

    add-float/2addr v0, v2

    return v0

    :cond_1
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    move p1, v1

    goto :goto_0
.end method

.method private b(Lbi/a;Lbi/a;F)V
    .locals 2

    sget-object v0, LU/i;->c:LU/i;

    iput-object v0, p0, LU/h;->v:LU/i;

    iget v0, p0, LU/h;->w:F

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    iget-object v0, p0, LU/h;->a:Lbi/d;

    iget v1, p0, LU/h;->w:F

    invoke-static {v0, p2, v1}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    iput-object v0, p0, LU/h;->u:Lbi/a;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, LU/h;->u:Lbi/a;

    goto :goto_0
.end method

.method private i()F
    .locals 2

    iget-object v0, p0, LU/h;->a:Lbi/d;

    iget-object v1, p0, LU/h;->b:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    iget v1, p0, LU/h;->c:I

    invoke-virtual {v0, v1}, Lbi/h;->d(I)F

    move-result v0

    sget v1, LU/h;->l:F

    mul-float/2addr v0, v1

    sget v1, LU/h;->m:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    sget v0, LU/h;->m:F

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, LU/h;->n:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    sget v0, LU/h;->n:F

    goto :goto_0
.end method

.method private declared-synchronized j()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LU/h;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, LU/h;->a:Lbi/d;

    iget-object v1, p0, LU/h;->b:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    iget v1, p0, LU/h;->c:I

    invoke-virtual {v0, v1}, Lbi/h;->d(I)F

    move-result v0

    iget v1, p0, LU/h;->h:F

    div-float v0, v1, v0

    iget v1, p0, LU/h;->j:F

    sub-float/2addr v1, v0

    iput v1, p0, LU/h;->r:F

    iget v1, p0, LU/h;->j:F

    add-float/2addr v0, v1

    iput v0, p0, LU/h;->s:F

    const/4 v0, 0x1

    iput-boolean v0, p0, LU/h;->t:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()F
    .locals 1

    invoke-direct {p0}, LU/h;->j()V

    iget v0, p0, LU/h;->r:F

    return v0
.end method

.method public a(LU/j;)V
    .locals 0

    iput-object p1, p0, LU/h;->x:LU/j;

    return-void
.end method

.method public a(Lbi/a;)V
    .locals 0

    iput-object p1, p0, LU/h;->u:Lbi/a;

    return-void
.end method

.method public a(Z)V
    .locals 4

    iget-object v0, p0, LU/h;->p:Lbi/a;

    iput-object v0, p0, LU/h;->q:Lbi/a;

    iget v0, p0, LU/h;->g:F

    sget v1, LU/h;->o:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LU/h;->a(ZZ)F

    move-result v1

    iget-object v0, p0, LU/h;->b:Lbi/t;

    const/high16 v2, 0x3f800000

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    new-instance v2, Lbi/v;

    iget-object v3, p0, LU/h;->a:Lbi/d;

    invoke-direct {v2, v3}, Lbi/v;-><init>(Lbi/d;)V

    invoke-virtual {v2, v0}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v2

    invoke-virtual {v2}, Lbi/v;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, LU/h;->p:Lbi/a;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/t;->a(Lbi/t;)I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LU/h;->p:Lbi/a;

    iget v2, v2, Lbi/a;->b:F

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v2, Lbi/a;

    invoke-direct {v2, v0, v1}, Lbi/a;-><init>(Lbi/t;F)V

    iput-object v2, p0, LU/h;->q:Lbi/a;

    :cond_2
    return-void
.end method

.method public a(LU/h;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LU/h;->b:Lbi/t;

    iget-object v3, p1, LU/h;->b:Lbi/t;

    invoke-virtual {v2, v3}, Lbi/t;->a(Lbi/t;)I

    move-result v2

    if-eqz v2, :cond_2

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, LU/h;->c:I

    iget v3, p1, LU/h;->c:I

    if-eq v2, v3, :cond_3

    iget v2, p0, LU/h;->c:I

    iget v3, p1, LU/h;->c:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, LU/h;->j:F

    iget v3, p1, LU/h;->j:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public b()F
    .locals 1

    invoke-direct {p0}, LU/h;->j()V

    iget v0, p0, LU/h;->s:F

    return v0
.end method

.method public b(LU/h;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LU/h;->b:Lbi/t;

    iget-object v3, p1, LU/h;->b:Lbi/t;

    invoke-virtual {v2, v3}, Lbi/t;->a(Lbi/t;)I

    move-result v2

    if-eqz v2, :cond_2

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, LU/h;->c:I

    iget v3, p1, LU/h;->c:I

    if-eq v2, v3, :cond_3

    iget v2, p0, LU/h;->c:I

    iget v3, p1, LU/h;->c:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, LU/h;->b()F

    move-result v2

    invoke-virtual {p1}, LU/h;->a()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public c(LU/h;)LU/i;
    .locals 5

    iget-object v0, p0, LU/h;->v:LU/i;

    sget-object v1, LU/i;->a:LU/i;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LU/h;->v:LU/i;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LU/h;->g()Lbi/a;

    move-result-object v0

    invoke-virtual {p1}, LU/h;->e()LU/i;

    move-result-object v1

    invoke-virtual {p1}, LU/h;->f()Lbi/a;

    move-result-object v2

    iget-object v3, p0, LU/h;->a:Lbi/d;

    invoke-static {v3, v0, v2}, LX/g;->a(Lbi/d;Lbi/a;Lbi/a;)F

    move-result v3

    sget-object v4, LU/i;->b:LU/i;

    if-ne v1, v4, :cond_3

    iget v1, p0, LU/h;->w:F

    neg-float v1, v1

    cmpg-float v1, v3, v1

    if-gez v1, :cond_1

    invoke-direct {p0, v2, v0, v3}, LU/h;->a(Lbi/a;Lbi/a;F)V

    :goto_1
    iget-object v0, p0, LU/h;->v:LU/i;

    goto :goto_0

    :cond_1
    iget v1, p0, LU/h;->w:F

    iget v4, p0, LU/h;->h:F

    add-float/2addr v1, v4

    cmpl-float v1, v3, v1

    if-lez v1, :cond_2

    invoke-direct {p0, v2, v0, v3}, LU/h;->b(Lbi/a;Lbi/a;F)V

    goto :goto_1

    :cond_2
    sget-object v0, LU/i;->b:LU/i;

    iput-object v0, p0, LU/h;->v:LU/i;

    iput-object v2, p0, LU/h;->u:Lbi/a;

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    cmpg-float v4, v3, v4

    if-ltz v4, :cond_4

    sget-object v4, LU/i;->a:LU/i;

    if-ne v1, v4, :cond_5

    iget v1, p0, LU/h;->h:F

    cmpg-float v1, v3, v1

    if-gez v1, :cond_5

    :cond_4
    invoke-direct {p0, v2, v0, v3}, LU/h;->a(Lbi/a;Lbi/a;F)V

    goto :goto_1

    :cond_5
    invoke-direct {p0, v2, v0, v3}, LU/h;->b(Lbi/a;Lbi/a;F)V

    goto :goto_1
.end method

.method public c()LU/j;
    .locals 1

    iget-object v0, p0, LU/h;->x:LU/j;

    return-object v0
.end method

.method public d()Z
    .locals 2

    sget-object v0, LU/h;->k:Ljava/util/Set;

    iget-object v1, p0, LU/h;->x:LU/j;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public e()LU/i;
    .locals 1

    iget-object v0, p0, LU/h;->v:LU/i;

    return-object v0
.end method

.method public f()Lbi/a;
    .locals 1

    iget-object v0, p0, LU/h;->u:Lbi/a;

    return-object v0
.end method

.method public g()Lbi/a;
    .locals 1

    iget-object v0, p0, LU/h;->p:Lbi/a;

    return-object v0
.end method

.method public h()Lbi/a;
    .locals 1

    iget-object v0, p0, LU/h;->q:Lbi/a;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LU/h;->a(Z)V

    :cond_0
    iget-object v0, p0, LU/h;->q:Lbi/a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "point"

    iget-object v2, p0, LU/h;->d:LaN/B;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "accuracy"

    iget v2, p0, LU/h;->h:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "distanceLessAccuracy"

    iget v2, p0, LU/h;->g:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "orthogonalProgress"

    iget v2, p0, LU/h;->j:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "segmentIndex"

    iget-object v2, p0, LU/h;->b:Lbi/t;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "subSegmentIndex"

    iget v2, p0, LU/h;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "validity"

    iget-object v2, p0, LU/h;->x:LU/j;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "direction"

    iget-object v2, p0, LU/h;->v:LU/i;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isOutsideBounds"

    iget-boolean v2, p0, LU/h;->i:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
