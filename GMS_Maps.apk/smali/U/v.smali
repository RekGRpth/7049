.class public final LU/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/u;


# static fields
.field private static final d:Lcom/google/common/collect/aG;


# instance fields
.field private final a:Lcom/google/googlenav/common/a;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/common/collect/ImmutableSet;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/common/collect/aG;->f()Lcom/google/common/collect/aH;

    move-result-object v0

    const-string v1, "gps_fixup_provider"

    const-string v2, "gps"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aH;

    move-result-object v0

    const-string v1, "network_fixup_provider"

    const-string v2, "network"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aH;

    move-result-object v0

    const-string v1, "base_location_provider"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "gps_fixup_provider"

    aput-object v3, v2, v4

    const-string v3, "network_fixup_provider"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/common/collect/aH;

    move-result-object v0

    const-string v1, "speed_provider"

    const-string v2, "gps_fixup_provider"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aH;

    move-result-object v0

    const-string v1, "integrated_location_provider"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "base_location_provider"

    aput-object v3, v2, v4

    const-string v3, "speed_provider"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/common/collect/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/aH;->a()Lcom/google/common/collect/aG;

    move-result-object v0

    sput-object v0, LU/v;->d:Lcom/google/common/collect/aG;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/a;

    iput-object v0, p0, LU/v;->a:Lcom/google/googlenav/common/a;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LU/v;->b:Landroid/content/Context;

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->g()Lcom/google/common/collect/aD;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "gps"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "gps_fixup_provider"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "base_location_provider"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "speed_provider"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "integrated_location_provider"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aD;->b([Ljava/lang/Object;)Lcom/google/common/collect/aD;

    invoke-virtual {v0}, Lcom/google/common/collect/aD;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, LU/v;->c:Lcom/google/common/collect/ImmutableSet;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;LU/l;)LU/b;
    .locals 3

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LU/c;

    iget-object v1, p0, LU/v;->b:Landroid/content/Context;

    iget-object v2, p0, LU/v;->a:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p2, v1, v2}, LU/c;-><init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "gps_fixup_provider"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, LU/d;

    invoke-direct {v0, p2}, LU/d;-><init>(LU/l;)V

    goto :goto_0

    :cond_1
    const-string v0, "network"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, LU/M;

    iget-object v1, p0, LU/v;->b:Landroid/content/Context;

    iget-object v2, p0, LU/v;->a:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p2, v1, v2}, LU/M;-><init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V

    goto :goto_0

    :cond_2
    const-string v0, "network_fixup_provider"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, LU/N;

    invoke-direct {v0, p2}, LU/N;-><init>(LU/l;)V

    goto :goto_0

    :cond_3
    const-string v0, "base_location_provider"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, LU/a;

    iget-object v1, p0, LU/v;->b:Landroid/content/Context;

    iget-object v2, p0, LU/v;->a:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p2, v1, v2}, LU/a;-><init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V

    goto :goto_0

    :cond_4
    const-string v0, "integrated_location_provider"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, LU/k;

    invoke-direct {v0, p2}, LU/k;-><init>(LU/T;)V

    goto :goto_0

    :cond_5
    const-string v0, "speed_provider"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, LU/U;

    iget-object v1, p0, LU/v;->a:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p2, v1}, LU/U;-><init>(LU/l;Lcom/google/googlenav/common/a;)V

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/os/Handler$Callback;)LX/i;
    .locals 2

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "RideAboutLocationThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, LX/h;

    invoke-direct {v1, v0, p1}, LX/h;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    return-object v1
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, LU/v;->c:Lcom/google/common/collect/ImmutableSet;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    sget-object v0, LU/v;->d:Lcom/google/common/collect/aG;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/aG;->c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method
