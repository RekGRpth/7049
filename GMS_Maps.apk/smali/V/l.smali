.class public LV/l;
.super Lcom/google/common/collect/dY;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/di;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;

.field private c:I

.field private d:Z

.field private final e:LV/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, LV/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LV/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Laa/e;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/common/collect/dY;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, LV/n;

    invoke-direct {v0, p1}, LV/n;-><init>(Laa/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iput-object v0, p0, LV/l;->e:LV/n;

    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private d()V
    .locals 2

    :try_start_0
    iget-boolean v0, p0, LV/l;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LV/l;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, LV/l;->c:I

    iget-object v1, p0, LV/l;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, LV/l;->e:LV/n;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LV/n;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LV/l;->b:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, LV/l;->c:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a()LV/g;
    .locals 2

    invoke-virtual {p0}, LV/l;->b()LV/g;

    move-result-object v0

    iget v1, p0, LV/l;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LV/l;->c:I

    return-object v0
.end method

.method public b()LV/g;
    .locals 2

    invoke-direct {p0}, LV/l;->d()V

    iget-object v0, p0, LV/l;->b:Ljava/util/List;

    iget v1, p0, LV/l;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LV/g;

    return-object v0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LV/l;->b()LV/g;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, LV/l;->d:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LV/l;->d()V

    iget v2, p0, LV/l;->c:I

    iget-object v3, p0, LV/l;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    iput-boolean v1, p0, LV/l;->d:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LV/l;->a()LV/g;

    move-result-object v0

    return-object v0
.end method
