.class public LW/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LW/k;

.field private b:LW/n;

.field private c:Ljava/lang/Thread;

.field private d:LW/j;

.field private e:Lbi/d;


# direct methods
.method public constructor <init>(Lbi/d;LU/x;Lcom/google/android/maps/rideabout/app/j;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LW/h;->c:Ljava/lang/Thread;

    new-instance v0, LW/j;

    sget-object v1, Lcom/google/android/maps/rideabout/app/c;->h:Lcom/google/android/maps/rideabout/app/c;

    invoke-direct {v0, p1, v1, p0, p3}, LW/j;-><init>(Lbi/d;Lcom/google/android/maps/rideabout/app/c;LW/h;Lcom/google/android/maps/rideabout/app/j;)V

    iput-object v0, p0, LW/h;->d:LW/j;

    iput-object p1, p0, LW/h;->e:Lbi/d;

    iget-object v0, p0, LW/h;->d:LW/j;

    invoke-static {p0, p1, p2, v0}, LW/k;->a(LW/h;Lbi/d;LU/x;LW/j;)V

    sget-object v0, LW/n;->b:LW/n;

    invoke-virtual {p0, v0}, LW/h;->a(LW/n;)LW/k;

    return-void
.end method


# virtual methods
.method public a(LW/n;)LW/k;
    .locals 2

    invoke-virtual {p0}, LW/h;->a()V

    iget-object v0, p0, LW/h;->b:LW/n;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, LW/h;->a:LW/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LW/h;->a:LW/k;

    invoke-virtual {v0}, LW/k;->g()V

    :cond_0
    iput-object p1, p0, LW/h;->b:LW/n;

    sget-object v0, LW/i;->a:[I

    invoke-virtual {p1}, LW/n;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, LW/e;->a:LW/e;

    iput-object v0, p0, LW/h;->a:LW/k;

    :goto_0
    iget-object v0, p0, LW/h;->a:LW/k;

    invoke-virtual {v0}, LW/k;->b()V

    :cond_1
    iget-object v0, p0, LW/h;->a:LW/k;

    return-object v0

    :pswitch_1
    sget-object v0, LW/f;->a:LW/f;

    iput-object v0, p0, LW/h;->a:LW/k;

    goto :goto_0

    :pswitch_2
    sget-object v0, LW/a;->a:LW/a;

    iput-object v0, p0, LW/h;->a:LW/k;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LW/h;->c:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on main (UI) thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public a(LU/z;)Z
    .locals 2

    invoke-virtual {p0}, LW/h;->a()V

    iget-object v0, p0, LW/h;->d:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LU/z;->c:Lbi/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, LW/h;->e:Lbi/d;

    iget-object v1, p1, LU/z;->c:Lbi/a;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/a;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LW/h;->d:LW/j;

    invoke-virtual {v0, p1}, LW/j;->a(LU/z;)V

    iget-object v0, p0, LW/h;->d:LW/j;

    invoke-virtual {v0, p1}, LW/j;->b(LU/z;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()LW/k;
    .locals 1

    invoke-virtual {p0}, LW/h;->a()V

    iget-object v0, p0, LW/h;->a:LW/k;

    return-object v0
.end method

.method public c()LW/j;
    .locals 1

    iget-object v0, p0, LW/h;->d:LW/j;

    return-object v0
.end method
