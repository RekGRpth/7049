.class public LW/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LU/z;

.field private b:Lbi/a;

.field private c:Lbi/t;

.field private d:Lcom/google/android/maps/rideabout/app/c;

.field private final e:Lcom/google/android/maps/rideabout/app/a;

.field private final f:LW/h;

.field private final g:Lcom/google/android/maps/rideabout/app/j;


# direct methods
.method public constructor <init>(Lbi/d;Lcom/google/android/maps/rideabout/app/c;LW/h;Lcom/google/android/maps/rideabout/app/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/maps/rideabout/app/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/maps/rideabout/app/a;-><init>(Lbi/d;Lcom/google/android/maps/rideabout/app/c;)V

    iput-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    iput-object p3, p0, LW/j;->f:LW/h;

    iput-object p4, p0, LW/j;->g:Lcom/google/android/maps/rideabout/app/j;

    return-void
.end method

.method private d()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->l()Lcom/google/android/maps/rideabout/app/c;

    move-result-object v3

    iget-object v0, p0, LW/j;->b:Lbi/a;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, LW/j;->c:Lbi/t;

    invoke-static {v0, v4}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LW/j;->d:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v3, v4}, Lcom/google/android/maps/rideabout/app/c;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    iput-object v0, p0, LW/j;->c:Lbi/t;

    iput-object v3, p0, LW/j;->d:Lcom/google/android/maps/rideabout/app/c;

    :goto_2
    return v1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, LW/j;->b:Lbi/a;

    iget-object v0, v0, Lbi/a;->a:Lbi/t;

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_2
.end method


# virtual methods
.method public a()LU/z;
    .locals 1

    iget-object v0, p0, LW/j;->f:LW/h;

    invoke-virtual {v0}, LW/h;->a()V

    iget-object v0, p0, LW/j;->a:LU/z;

    return-object v0
.end method

.method public a(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/j;->f:LW/h;

    invoke-virtual {v0}, LW/h;->a()V

    iput-object p1, p0, LW/j;->a:LU/z;

    iget-object v0, p1, LU/z;->c:Lbi/a;

    if-eqz v0, :cond_0

    iget-object v0, p1, LU/z;->c:Lbi/a;

    iput-object v0, p0, LW/j;->b:Lbi/a;

    iget-object v0, p0, LW/j;->b:Lbi/a;

    iget-object v0, v0, Lbi/a;->a:Lbi/t;

    iget-object v1, p0, LW/j;->c:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, LW/j;->c:Lbi/t;

    :cond_0
    return-void
.end method

.method public b()Lcom/google/android/maps/rideabout/app/a;
    .locals 1

    iget-object v0, p0, LW/j;->f:LW/h;

    invoke-virtual {v0}, LW/h;->a()V

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    return-object v0
.end method

.method public b(LU/z;)V
    .locals 1

    iget-object v0, p0, LW/j;->f:LW/h;

    invoke-virtual {v0}, LW/h;->a()V

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/rideabout/app/a;->a(LU/z;)V

    return-void
.end method

.method public c()Lbi/a;
    .locals 1

    iget-object v0, p0, LW/j;->f:LW/h;

    invoke-virtual {v0}, LW/h;->a()V

    iget-object v0, p0, LW/j;->b:Lbi/a;

    return-object v0
.end method

.method public c(LU/z;)V
    .locals 5

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LU/z;->c:Lbi/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->c()Ljava/lang/String;

    move-result-object v2

    const-string v0, ""

    iget-object v1, p1, LU/z;->c:Lbi/a;

    if-eqz v1, :cond_1

    iget-object v0, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->d()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v3, p0, LW/j;->g:Lcom/google/android/maps/rideabout/app/j;

    iget-object v1, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v1}, Lcom/google/android/maps/rideabout/app/a;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LW/j;->e:Lcom/google/android/maps/rideabout/app/a;

    invoke-virtual {v1}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0}, LW/j;->d()Z

    move-result v4

    invoke-interface {v3, v1, v4, v2, v0}, Lcom/google/android/maps/rideabout/app/j;->a(ZZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
