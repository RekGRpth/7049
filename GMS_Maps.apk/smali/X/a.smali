.class public LX/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/a;


# instance fields
.field public final a:D

.field private final b:Lcom/google/googlenav/common/a;

.field private c:J

.field private d:J

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/a;JD)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    iput-wide p4, p0, LX/a;->a:D

    iput-wide p2, p0, LX/a;->d:J

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/a;->f:Z

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/a;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/a;->f:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/a;->e:J

    iget-object v0, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, LX/a;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/a;->a()V

    iput-wide p1, p0, LX/a;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()J
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/a;->f:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, LX/a;->d:J

    iget-object v2, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, LX/a;->c:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    iget-wide v4, p0, LX/a;->a:D

    mul-double/2addr v2, v4

    double-to-long v2, v2

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/a;->e:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/a;->e:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-wide v0, p0, LX/a;->e:J

    invoke-virtual {p0, v0, v1}, LX/a;->b(J)V

    iget-wide v0, p0, LX/a;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_1
    :try_start_1
    iget-wide v0, p0, LX/a;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/a;->d:J

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/a;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()J
    .locals 6

    iget-object v0, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-virtual {p0}, LX/a;->b()J

    move-result-wide v2

    iget-object v4, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v4}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public d()J
    .locals 6

    iget-object v0, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v0

    invoke-virtual {p0}, LX/a;->b()J

    move-result-wide v2

    iget-object v4, p0, LX/a;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v4}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method
