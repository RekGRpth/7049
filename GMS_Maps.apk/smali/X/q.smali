.class public final LX/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final A:F

.field public final B:I

.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:F

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:F

.field public final i:F

.field public final j:F

.field public final k:F

.field public final l:F

.field public final m:F

.field public final n:F

.field public final o:F

.field public final p:F

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:I

.field public final y:F

.field public final z:F


# direct methods
.method constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->a:I

    const/4 v0, 0x2

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->b:I

    const/4 v0, 0x3

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->c:I

    const/4 v0, 0x4

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->d:F

    const/4 v0, 0x5

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->e:F

    const/4 v0, 0x6

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->i:F

    const/4 v0, 0x7

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->j:F

    const/16 v0, 0x8

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->k:F

    const/16 v0, 0x9

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->l:F

    const/16 v0, 0xa

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->m:F

    const/16 v0, 0x1a

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->f:F

    const/16 v0, 0x1b

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->g:F

    const/16 v0, 0x1c

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->h:F

    const/16 v0, 0xb

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->n:F

    const/16 v0, 0xc

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->o:F

    const/16 v0, 0xd

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->p:F

    const/16 v0, 0xe

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->q:I

    const/16 v0, 0xf

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->r:I

    const/16 v0, 0x10

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->s:I

    const/16 v0, 0x11

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->t:I

    const/16 v0, 0x12

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->u:I

    const/16 v0, 0x13

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->v:I

    const/16 v0, 0x14

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->w:I

    const/16 v0, 0x15

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->x:I

    const/16 v0, 0x16

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->y:F

    const/16 v0, 0x17

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->z:F

    const/16 v0, 0x18

    invoke-static {p1, v0}, LX/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F

    move-result v0

    iput v0, p0, LX/q;->A:F

    const/16 v0, 0x19

    invoke-static {p1, v0}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, LX/q;->B:I

    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I
    .locals 1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->s:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    return v0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)F
    .locals 1

    invoke-static {p0, p1}, LX/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method
