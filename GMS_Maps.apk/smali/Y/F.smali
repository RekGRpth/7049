.class abstract LY/F;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field b:I

.field c:I

.field d:LY/M;

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field f:LY/L;

.field g:LY/am;

.field h:LY/am;

.field final synthetic i:LY/n;


# direct methods
.method constructor <init>(LY/n;)V
    .locals 1

    iput-object p1, p0, LY/F;->i:LY/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, LY/n;->e:[LY/M;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LY/F;->b:I

    const/4 v0, -0x1

    iput v0, p0, LY/F;->c:I

    invoke-virtual {p0}, LY/F;->b()V

    return-void
.end method


# virtual methods
.method a(LY/L;)Z
    .locals 4

    :try_start_0
    iget-object v0, p0, LY/F;->i:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v0

    invoke-interface {p1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LY/F;->i:LY/n;

    invoke-virtual {v3, p1, v0, v1}, LY/n;->a(LY/L;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, LY/am;

    iget-object v3, p0, LY/F;->i:LY/n;

    invoke-direct {v1, v3, v2, v0}, LY/am;-><init>(LY/n;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, LY/F;->g:LY/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, LY/F;->d:LY/M;

    invoke-virtual {v1}, LY/M;->m()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, LY/F;->d:LY/M;

    invoke-virtual {v1}, LY/M;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LY/F;->d:LY/M;

    invoke-virtual {v1}, LY/M;->m()V

    throw v0
.end method

.method final b()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, LY/F;->g:LY/am;

    invoke-virtual {p0}, LY/F;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LY/F;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget v0, p0, LY/F;->b:I

    if-ltz v0, :cond_0

    iget-object v0, p0, LY/F;->i:LY/n;

    iget-object v0, v0, LY/n;->e:[LY/M;

    iget v1, p0, LY/F;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LY/F;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, LY/F;->d:LY/M;

    iget-object v0, p0, LY/F;->d:LY/M;

    iget v0, v0, LY/M;->b:I

    if-eqz v0, :cond_2

    iget-object v0, p0, LY/F;->d:LY/M;

    iget-object v0, v0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, LY/F;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, LY/F;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LY/F;->c:I

    invoke-virtual {p0}, LY/F;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method c()Z
    .locals 1

    iget-object v0, p0, LY/F;->f:LY/L;

    if-eqz v0, :cond_1

    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v0

    iput-object v0, p0, LY/F;->f:LY/L;

    :goto_0
    iget-object v0, p0, LY/F;->f:LY/L;

    if-eqz v0, :cond_1

    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-virtual {p0, v0}, LY/F;->a(LY/L;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v0

    iput-object v0, p0, LY/F;->f:LY/L;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method d()Z
    .locals 3

    :cond_0
    iget v0, p0, LY/F;->c:I

    if-ltz v0, :cond_2

    iget-object v0, p0, LY/F;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, LY/F;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LY/F;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    iput-object v0, p0, LY/F;->f:LY/L;

    if-eqz v0, :cond_0

    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-virtual {p0, v0}, LY/F;->a(LY/L;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, LY/F;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()LY/am;
    .locals 1

    iget-object v0, p0, LY/F;->g:LY/am;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, LY/F;->g:LY/am;

    iput-object v0, p0, LY/F;->h:LY/am;

    invoke-virtual {p0}, LY/F;->b()V

    iget-object v0, p0, LY/F;->h:LY/am;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, LY/F;->g:LY/am;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, LY/F;->h:LY/am;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, LY/F;->i:LY/n;

    iget-object v1, p0, LY/F;->h:LY/am;

    invoke-virtual {v1}, LY/am;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LY/n;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, LY/F;->h:LY/am;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
