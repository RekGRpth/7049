.class final LY/aj;
.super Ljava/util/AbstractQueue;
.source "SourceFile"


# instance fields
.field final a:LY/L;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    new-instance v0, LY/ak;

    invoke-direct {v0, p0}, LY/ak;-><init>(LY/aj;)V

    iput-object v0, p0, LY/aj;->a:LY/L;

    return-void
.end method


# virtual methods
.method public a()LY/L;
    .locals 2

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v0

    iget-object v1, p0, LY/aj;->a:LY/L;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(LY/L;)Z
    .locals 2

    invoke-interface {p1}, LY/L;->j()LY/L;

    move-result-object v0

    invoke-interface {p1}, LY/L;->i()LY/L;

    move-result-object v1

    invoke-static {v0, v1}, LY/n;->b(LY/L;LY/L;)V

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-interface {v0}, LY/L;->j()LY/L;

    move-result-object v0

    invoke-static {v0, p1}, LY/n;->b(LY/L;LY/L;)V

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-static {p1, v0}, LY/n;->b(LY/L;LY/L;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b()LY/L;
    .locals 2

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v0

    iget-object v1, p0, LY/aj;->a:LY/L;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, LY/aj;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v0

    :goto_0
    iget-object v1, p0, LY/aj;->a:LY/L;

    if-eq v0, v1, :cond_0

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v1

    invoke-static {v0}, LY/n;->c(LY/L;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, LY/aj;->a:LY/L;

    iget-object v1, p0, LY/aj;->a:LY/L;

    invoke-interface {v0, v1}, LY/L;->c(LY/L;)V

    iget-object v0, p0, LY/aj;->a:LY/L;

    iget-object v1, p0, LY/aj;->a:LY/L;

    invoke-interface {v0, v1}, LY/L;->d(LY/L;)V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, LY/L;

    invoke-interface {p1}, LY/L;->i()LY/L;

    move-result-object v0

    sget-object v1, LY/K;->a:LY/K;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v0

    iget-object v1, p0, LY/aj;->a:LY/L;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, LY/al;

    invoke-virtual {p0}, LY/aj;->a()LY/L;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LY/al;-><init>(LY/aj;LY/L;)V

    return-object v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, LY/L;

    invoke-virtual {p0, p1}, LY/aj;->a(LY/L;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LY/aj;->a()LY/L;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LY/aj;->b()LY/L;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, LY/L;

    invoke-interface {p1}, LY/L;->j()LY/L;

    move-result-object v0

    invoke-interface {p1}, LY/L;->i()LY/L;

    move-result-object v1

    invoke-static {v0, v1}, LY/n;->b(LY/L;LY/L;)V

    invoke-static {p1}, LY/n;->c(LY/L;)V

    sget-object v0, LY/K;->a:LY/K;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, LY/aj;->a:LY/L;

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LY/aj;->a:LY/L;

    if-eq v0, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, LY/L;->i()LY/L;

    move-result-object v0

    goto :goto_0

    :cond_0
    return v1
.end method
