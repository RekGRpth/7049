.class public LaA/b;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# static fields
.field public static final a:LaA/c;

.field public static final b:LaA/c;

.field public static final c:LaA/c;

.field public static final d:LaA/c;

.field public static final e:LaA/c;

.field public static final f:LaA/c;

.field public static final g:LaA/c;

.field public static final h:LaA/c;

.field public static final i:LaA/c;

.field public static final j:LaA/c;

.field public static final k:LaA/c;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const v8, 0x7f020219

    const/16 v7, 0x128

    new-instance v0, LaA/c;

    const/16 v1, 0x12b

    const v2, 0x7f02021b

    const/16 v3, 0xbb9

    const-string v4, "m"

    const-string v5, "Map"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->a:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x38e

    const/16 v2, 0x12a

    const v3, 0x7f02021a

    const/16 v4, 0xbba

    const-string v5, "l"

    const-string v6, "Places"

    invoke-direct/range {v0 .. v6}, LaA/c;-><init>(IIIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->b:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x12d

    const v2, 0x7f02021e

    const/16 v3, 0xbbc

    const-string v4, "t"

    const-string v5, "Traffic"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->c:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x2f8

    const v2, 0x7f02021d

    const/16 v3, 0xbbb

    const-string v4, "n"

    const-string v5, "Navigation"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->d:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x13e

    const/16 v4, 0xbbd

    const-string v5, "lj"

    const-string v6, "Latitude"

    move v2, v7

    move v3, v8

    invoke-direct/range {v0 .. v6}, LaA/c;-><init>(IIIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->e:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x13d

    const/16 v4, 0xbbe

    const-string v5, "la"

    const-string v6, "Latitude"

    move v2, v7

    move v3, v8

    invoke-direct/range {v0 .. v6}, LaA/c;-><init>(IIIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->f:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x2dc

    const v2, 0x7f02021c

    const/16 v3, 0xbc0

    const-string v4, "p"

    const-string v5, "My Places"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->g:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0xfc

    const v2, 0x7f020217

    const/16 v3, 0xbc1

    const-string v4, "d"

    const-string v5, "Directions"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->h:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x86

    const v2, 0x7f020216

    const/16 v3, 0xbc2

    const-string v4, "c"

    const-string v5, "Check in"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->i:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x150

    const v2, 0x7f020218

    const/16 v3, 0xbc3

    const-string v4, "h"

    const-string v5, "Location History"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->j:LaA/c;

    new-instance v0, LaA/c;

    const/16 v1, 0x31c

    const v2, 0x7f020351

    const/16 v3, 0xbc4

    const-string v4, "o"

    const-string v5, "Offers"

    invoke-direct/range {v0 .. v5}, LaA/c;-><init>(IIILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaA/b;->k:LaA/c;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f10001e

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic a(I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LaA/b;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    sget v1, LaA/c;->a:I

    if-le p0, v1, :cond_0

    if-ne p0, v2, :cond_1

    const/16 v0, 0x31b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v0, 0x31a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-virtual {p0}, LaA/b;->clear()V

    invoke-virtual {p0}, LaA/b;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaA/c;

    invoke-virtual {p0, v0}, LaA/b;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, LaA/b;->notifyDataSetChanged()V

    return-void
.end method

.method public b()Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    const/16 v0, 0xd

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v0, LaA/b;->a:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, LaA/b;->b:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LaA/b;->d:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LaA/b;->k:LaA/c;

    sget-object v2, LaA/b;->k:LaA/c;

    iget v2, v2, LaA/c;->f:I

    invoke-static {v2}, LaA/b;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LaA/c;->c:Ljava/lang/String;

    sget-object v0, LaA/b;->k:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v0, LaA/b;->g:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/friend/W;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LaA/b;->i:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LaA/b;->f:LaA/c;

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v0, LaA/b;->e:LaA/c;

    :cond_3
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, LaA/b;->j:LaA/c;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-virtual {p0}, LaA/b;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    if-nez p2, :cond_0

    const v1, 0x7f040075

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, LaA/b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaA/c;

    const v1, 0x7f100091

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v2, v0, LaA/c;->e:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    const v1, 0x7f10001e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, LaA/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f10004a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, LaA/c;->c:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, LaA/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const v1, 0x7f1000c2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, v0, LaA/c;->f:I

    sget v3, LaA/c;->a:I

    if-le v2, v3, :cond_2

    iget v0, v0, LaA/c;->f:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-object p2

    :cond_1
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
