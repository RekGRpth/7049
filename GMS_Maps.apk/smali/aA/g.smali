.class public LaA/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Set;

.field private final b:LaA/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaA/g;->a:Ljava/util/Set;

    iput-object v0, p0, LaA/g;->b:LaA/f;

    return-void
.end method

.method public varargs constructor <init>(LaA/f;[I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaA/g;->b:LaA/f;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaA/g;->a:Ljava/util/Set;

    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p2, v0

    iget-object v3, p0, LaA/g;->a:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private d(LaA/c;)Z
    .locals 2

    iget-object v0, p0, LaA/g;->a:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaA/g;->a:Ljava/util/Set;

    iget v1, p1, LaA/c;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LaA/f;
    .locals 1

    iget-object v0, p0, LaA/g;->b:LaA/f;

    return-object v0
.end method

.method public a(LaA/c;)V
    .locals 0

    return-void
.end method

.method public final b(LaA/c;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1}, LaA/g;->d(LaA/c;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, LaA/g;->c(LaA/c;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(LaA/c;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
