.class public LaA/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:LaA/h;

.field private static b:LaA/h;


# instance fields
.field private final c:LaA/e;

.field private d:Landroid/widget/TextView;

.field private e:LaA/a;

.field private f:LaA/b;

.field private final g:Lcom/google/googlenav/android/aa;


# direct methods
.method private constructor <init>(LaA/e;Lcom/google/googlenav/android/aa;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaA/h;->c:LaA/e;

    iput-object p2, p0, LaA/h;->g:Lcom/google/googlenav/android/aa;

    return-void
.end method

.method static synthetic a(LaA/h;)LaA/b;
    .locals 1

    iget-object v0, p0, LaA/h;->f:LaA/b;

    return-object v0
.end method

.method private a(Landroid/content/Context;)LaA/b;
    .locals 1

    iget-object v0, p0, LaA/h;->f:LaA/b;

    if-nez v0, :cond_0

    new-instance v0, LaA/b;

    invoke-direct {v0, p1}, LaA/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LaA/h;->f:LaA/b;

    :cond_0
    iget-object v0, p0, LaA/h;->f:LaA/b;

    return-object v0
.end method

.method public static a()LaA/h;
    .locals 1

    sget-object v0, LaA/h;->a:LaA/h;

    return-object v0
.end method

.method public static a(LaA/e;Lcom/google/googlenav/android/aa;)V
    .locals 1

    new-instance v0, LaA/h;

    invoke-direct {v0, p0, p1}, LaA/h;-><init>(LaA/e;Lcom/google/googlenav/android/aa;)V

    sput-object v0, LaA/h;->a:LaA/h;

    return-void
.end method

.method public static b()LaA/h;
    .locals 1

    sget-object v0, LaA/h;->b:LaA/h;

    return-object v0
.end method

.method public static b(LaA/e;Lcom/google/googlenav/android/aa;)V
    .locals 1

    new-instance v0, LaA/h;

    invoke-direct {v0, p0, p1}, LaA/h;-><init>(LaA/e;Lcom/google/googlenav/android/aa;)V

    sput-object v0, LaA/h;->b:LaA/h;

    return-void
.end method


# virtual methods
.method public a(Landroid/app/ActionBar;LaA/g;)V
    .locals 1

    invoke-virtual {p1}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;Landroid/content/Context;)V

    return-void
.end method

.method public a(Landroid/app/ActionBar;LaA/g;Landroid/content/Context;)V
    .locals 6

    const/4 v3, 0x0

    const-string v0, "layout_inflater"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040076

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LaA/h;->a(Landroid/app/ActionBar;Landroid/view/View;Landroid/app/ActionBar$LayoutParams;LaA/g;Landroid/content/Context;)V

    return-void
.end method

.method public a(Landroid/app/ActionBar;Landroid/view/View;Landroid/app/ActionBar$LayoutParams;LaA/g;Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    if-nez p3, :cond_1

    invoke-virtual {p1, p2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    :goto_1
    const v0, 0x7f10001e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaA/h;->d:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, LaA/h;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaA/h;->d:Landroid/widget/TextView;

    new-instance v1, LaA/j;

    invoke-direct {v1, p0, p5, p2, p4}, LaA/j;-><init>(LaA/h;Landroid/content/Context;Landroid/view/View;LaA/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p2, p3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Landroid/view/View;LaA/g;)V
    .locals 3

    iget-object v0, p0, LaA/h;->e:LaA/a;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, LaA/l;

    invoke-direct {p0, p1}, LaA/h;->a(Landroid/content/Context;)LaA/b;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, LaA/l;-><init>(Landroid/content/Context;Landroid/view/View;LaA/b;)V

    iput-object v0, p0, LaA/h;->e:LaA/a;

    :cond_0
    :goto_0
    iget-object v0, p0, LaA/h;->e:LaA/a;

    iget-object v1, p0, LaA/h;->c:LaA/e;

    iget-object v2, p0, LaA/h;->e:LaA/a;

    invoke-interface {v1, v2, p3}, LaA/e;->a(LaA/a;LaA/g;)LaA/d;

    move-result-object v1

    invoke-interface {v0, v1}, LaA/a;->a(LaA/d;)V

    iget-object v0, p0, LaA/h;->e:LaA/a;

    invoke-interface {v0, p2}, LaA/a;->a(Landroid/view/View;)V

    const/16 v0, 0x6c

    const-string v1, "a"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    iget-object v0, p0, LaA/h;->f:LaA/b;

    invoke-virtual {v0}, LaA/b;->a()V

    iget-object v0, p0, LaA/h;->e:LaA/a;

    invoke-interface {v0}, LaA/a;->a()V

    return-void

    :cond_1
    instance-of v0, p1, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    if-eqz v0, :cond_2

    new-instance v0, LaA/n;

    invoke-direct {p0, p1}, LaA/h;->a(Landroid/content/Context;)LaA/b;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, LaA/n;-><init>(Landroid/content/Context;Landroid/view/View;LaA/b;)V

    iput-object v0, p0, LaA/h;->e:LaA/a;

    goto :goto_0

    :cond_2
    new-instance v0, LaA/m;

    invoke-direct {p0, p1}, LaA/h;->a(Landroid/content/Context;)LaA/b;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, LaA/m;-><init>(Landroid/content/Context;Landroid/view/View;LaA/b;)V

    iput-object v0, p0, LaA/h;->e:LaA/a;

    goto :goto_0
.end method

.method public a(Landroid/view/View;LaA/g;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LaA/k;

    invoke-direct {v1, p0, v0, p1, p2}, LaA/k;-><init>(LaA/h;Landroid/content/Context;Landroid/view/View;LaA/g;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, LaA/h;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaA/h;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, LaA/h;->e:LaA/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaA/h;->e:LaA/a;

    invoke-interface {v0}, LaA/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, LaA/h;->g:Lcom/google/googlenav/android/aa;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LaA/h;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaA/h;->g:Lcom/google/googlenav/android/aa;

    new-instance v2, LaA/i;

    invoke-direct {v2, p0}, LaA/i;-><init>(LaA/h;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, LaA/h;->e:LaA/a;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaA/h;->e:LaA/a;

    invoke-interface {v0}, LaA/a;->c()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LaA/h;->e:LaA/a;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
