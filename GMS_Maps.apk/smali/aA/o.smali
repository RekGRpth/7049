.class public LaA/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaA/d;


# instance fields
.field private final a:Lcom/google/googlenav/ui/s;

.field private final b:LaA/g;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/s;LaA/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    iput-object p2, p0, LaA/o;->b:LaA/g;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/s;LaA/g;LaA/p;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LaA/o;-><init>(Lcom/google/googlenav/ui/s;LaA/g;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    const/16 v7, 0xbbd

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaA/c;

    const/16 v3, 0x6c

    const-string v4, "c"

    invoke-virtual {v0}, LaA/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, LaA/c;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Laj/a;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, LaA/o;->b:LaA/g;

    if-eqz v3, :cond_2

    iget-object v3, p0, LaA/o;->b:LaA/g;

    invoke-virtual {v3, v0}, LaA/g;->b(LaA/c;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v3, v0, LaA/c;->d:I

    const/16 v4, 0xbb9

    if-eq v3, v4, :cond_3

    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v3

    invoke-virtual {v3}, Lbf/am;->u()Lbf/bk;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v1}, Lbf/am;->b(Z)V

    :cond_3
    iget v3, v0, LaA/c;->d:I

    sparse-switch v3, :sswitch_data_0

    :cond_4
    :goto_1
    iget-object v1, p0, LaA/o;->b:LaA/g;

    if-eqz v1, :cond_1

    iget-object v1, p0, LaA/o;->b:LaA/g;

    invoke-virtual {v1}, LaA/g;->a()LaA/f;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v1, v0}, LaA/f;->a(LaA/c;)V

    :cond_5
    iget-object v1, p0, LaA/o;->b:LaA/g;

    invoke-virtual {v1, v0}, LaA/g;->a(LaA/c;)V

    goto :goto_0

    :sswitch_0
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2, v2}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->f(Z)V

    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1, v2}, Lbf/i;->a(B)V

    goto :goto_1

    :sswitch_1
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(I)V

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3, v1, v1}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->f(Z)V

    goto :goto_1

    :sswitch_3
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->k()V

    goto :goto_1

    :sswitch_4
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    iget v4, v0, LaA/c;->d:I

    if-ne v4, v7, :cond_6

    :goto_2
    const/16 v2, 0x13c

    invoke-virtual {v3, v1, v2}, Lcom/google/googlenav/ui/s;->a(ZI)V

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2

    :sswitch_5
    iget v3, v0, LaA/c;->d:I

    if-eq v3, v7, :cond_7

    :goto_3
    iget-object v2, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-static {v2, v1}, Lcom/google/googlenav/friend/ac;->a(Lcom/google/googlenav/ui/s;Z)V

    goto :goto_1

    :cond_7
    move v1, v2

    goto :goto_3

    :sswitch_6
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    goto :goto_1

    :sswitch_7
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3, v1}, Lcom/google/googlenav/ui/s;->f(Z)V

    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2, v2}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    goto :goto_1

    :sswitch_8
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    new-instance v2, Lax/j;

    invoke-direct {v2}, Lax/j;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(Lax/k;)V

    goto :goto_1

    :sswitch_9
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v6, v6}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/R;)V

    goto/16 :goto_1

    :sswitch_a
    sget-object v3, LaA/b;->k:LaA/c;

    invoke-virtual {v3}, LaA/c;->a()I

    move-result v3

    if-lez v3, :cond_8

    :goto_4
    sget-object v3, LaA/b;->k:LaA/c;

    sget v4, LaA/c;->a:I

    invoke-virtual {v3, v4}, LaA/c;->a(I)V

    if-eqz v1, :cond_9

    const-string v1, "fn"

    :goto_5
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/googlenav/offers/a;->a(ZLjava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    move v1, v2

    goto :goto_4

    :cond_9
    const-string v1, "f"

    goto :goto_5

    :sswitch_b
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    goto/16 :goto_1

    :sswitch_c
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x9cd -> :sswitch_c
        0x9d4 -> :sswitch_b
        0xbb9 -> :sswitch_0
        0xbba -> :sswitch_1
        0xbbb -> :sswitch_3
        0xbbc -> :sswitch_2
        0xbbd -> :sswitch_4
        0xbbe -> :sswitch_4
        0xbbf -> :sswitch_7
        0xbc0 -> :sswitch_6
        0xbc1 -> :sswitch_8
        0xbc2 -> :sswitch_9
        0xbc3 -> :sswitch_5
        0xbc4 -> :sswitch_a
    .end sparse-switch
.end method
