.class LaB/g;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:LaB/a;


# direct methods
.method constructor <init>(LaB/a;Las/c;)V
    .locals 0

    iput-object p1, p0, LaB/g;->a:LaB/a;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    const/4 v7, 0x1

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PROTO_CLIENT_SAVED_PHOTO_CACHE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0, v7}, LaB/a;->a(LaB/a;Z)Z

    :goto_0
    return-void

    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ag;->u:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    iget-object v0, p0, LaB/g;->a:LaB/a;

    iget-object v3, v0, LaB/a;->a:Ljava/util/Hashtable;

    monitor-enter v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    const/4 v4, 0x1

    :try_start_2
    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, LaB/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaB/n;

    move-result-object v4

    iget-object v5, p0, LaB/g;->a:LaB/a;

    iget-object v5, v5, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v4}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, LaB/g;->a:LaB/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LaB/a;->a(LaB/a;Z)Z

    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0}, LaB/a;->f(LaB/a;)Ljava/util/Vector;

    move-result-object v1

    monitor-enter v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0}, LaB/a;->f(LaB/a;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0}, LaB/a;->f(LaB/a;)Ljava/util/Vector;

    move-result-object v0

    iget-object v2, p0, LaB/g;->a:LaB/a;

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    invoke-static {v2, v3}, LaB/a;->c(LaB/a;Ljava/util/Vector;)Ljava/util/Vector;

    iget-object v2, p0, LaB/g;->a:LaB/a;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LaB/a;->a(Ljava/util/Vector;LaB/p;)Z

    :cond_3
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0, v7}, LaB/a;->a(LaB/a;Z)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catch_0
    move-exception v0

    :try_start_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SOURCE_PROFILE_PHOTO_MANAGER_IMP-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LaB/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed to load IDs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0}, LaB/a;->e(LaB/a;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    iget-object v0, p0, LaB/g;->a:LaB/a;

    invoke-static {v0, v7}, LaB/a;->a(LaB/a;Z)Z

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :catchall_2
    move-exception v0

    iget-object v1, p0, LaB/g;->a:LaB/a;

    invoke-static {v1, v7}, LaB/a;->a(LaB/a;Z)Z

    throw v0
.end method
