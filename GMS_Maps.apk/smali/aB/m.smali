.class public LaB/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I

.field public static final b:I


# instance fields
.field private final c:LaB/n;

.field private d:Lam/f;

.field private e:Lam/f;

.field private f:Lam/f;

.field private final g:Lam/f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    const/4 v1, 0x2

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    new-array v3, v0, [I

    fill-array-data v3, :array_1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->an()Z

    move-result v4

    if-eqz v4, :cond_0

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    aget v1, v2, v0

    sput v1, LaB/m;->a:I

    aget v0, v3, v0

    sput v0, LaB/m;->b:I

    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x9
        0xe
        0x15
    .end array-data

    :array_1
    .array-data 4
        0x5
        0xa
        0xf
    .end array-data
.end method

.method public constructor <init>(Lam/f;Lam/f;Lam/f;Lam/f;LaB/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaB/m;->g:Lam/f;

    iput-object p2, p0, LaB/m;->f:Lam/f;

    iput-object p3, p0, LaB/m;->e:Lam/f;

    iput-object p4, p0, LaB/m;->d:Lam/f;

    iput-object p5, p0, LaB/m;->c:LaB/n;

    return-void
.end method

.method public constructor <init>(Lam/f;Lam/f;Lam/f;Lam/f;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    new-instance v5, LaB/n;

    invoke-direct {v5, p5, p6}, LaB/n;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LaB/m;-><init>(Lam/f;Lam/f;Lam/f;Lam/f;LaB/n;)V

    return-void
.end method

.method public static a(LaB/n;ILcom/google/googlenav/common/io/protocol/ProtoBuf;II)LaB/m;
    .locals 6

    const/4 v2, 0x0

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p2, p3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v1

    if-nez v1, :cond_0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p2, p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p2, p3, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    move-object v4, v0

    move-object v3, v1

    :goto_0
    new-instance v0, LaB/m;

    invoke-static {v5, p4}, LaB/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lam/f;

    move-result-object v1

    invoke-static {v2, p4}, LaB/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lam/f;

    move-result-object v2

    invoke-static {v3, p4}, LaB/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lam/f;

    move-result-object v3

    invoke-static {v4, p4}, LaB/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lam/f;

    move-result-object v4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, LaB/m;-><init>(Lam/f;Lam/f;Lam/f;Lam/f;LaB/n;)V

    return-object v0

    :cond_0
    move-object v4, v2

    move-object v3, v2

    goto :goto_0
.end method

.method private a(II)Lam/f;
    .locals 1

    iget-object v0, p0, LaB/m;->g:Lam/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaB/m;->g:Lam/f;

    invoke-interface {v0, p1, p2}, Lam/f;->a(II)Lam/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lam/f;
    .locals 4

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v0

    invoke-interface {v1, v0, v2, v3}, Lam/h;->a([BII)Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lam/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x3

    invoke-static {p1}, Lam/j;->c(Lam/f;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, LaB/m;->c:LaB/n;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public b()LaB/n;
    .locals 1

    iget-object v0, p0, LaB/m;->c:LaB/n;

    return-object v0
.end method

.method public c()Lam/f;
    .locals 3

    iget-object v0, p0, LaB/m;->d:Lam/f;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->m()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->H:C

    invoke-interface {v0, v1}, Lam/g;->c(C)I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->m()Lam/g;

    move-result-object v1

    sget-char v2, Lcom/google/googlenav/ui/bi;->H:C

    invoke-interface {v1, v2}, Lam/g;->b(C)I

    move-result v1

    invoke-virtual {p0}, LaB/m;->f()Lam/f;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, v1}, LaB/m;->a(II)Lam/f;

    move-result-object v0

    iput-object v0, p0, LaB/m;->d:Lam/f;

    :cond_0
    iget-object v0, p0, LaB/m;->d:Lam/f;

    return-object v0
.end method

.method public d()Lam/f;
    .locals 3

    iget-object v0, p0, LaB/m;->e:Lam/f;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->l()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->H:C

    invoke-interface {v0, v1}, Lam/g;->c(C)I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->l()Lam/g;

    move-result-object v1

    sget-char v2, Lcom/google/googlenav/ui/bi;->H:C

    invoke-interface {v1, v2}, Lam/g;->b(C)I

    move-result v1

    invoke-virtual {p0}, LaB/m;->f()Lam/f;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, v1}, LaB/m;->a(II)Lam/f;

    move-result-object v0

    iput-object v0, p0, LaB/m;->e:Lam/f;

    :cond_0
    iget-object v0, p0, LaB/m;->e:Lam/f;

    return-object v0
.end method

.method public e()Lam/f;
    .locals 3

    iget-object v0, p0, LaB/m;->f:Lam/f;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->k()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->H:C

    invoke-interface {v0, v1}, Lam/g;->c(C)I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->k()Lam/g;

    move-result-object v1

    sget-char v2, Lcom/google/googlenav/ui/bi;->H:C

    invoke-interface {v1, v2}, Lam/g;->b(C)I

    move-result v1

    invoke-direct {p0, v0, v1}, LaB/m;->a(II)Lam/f;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->M()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iput-object v0, p0, LaB/m;->f:Lam/f;

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, LaB/m;->f:Lam/f;

    goto :goto_0
.end method

.method public f()Lam/f;
    .locals 1

    iget-object v0, p0, LaB/m;->g:Lam/f;

    return-object v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    const/4 v2, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->w:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, LaB/m;->g:Lam/f;

    invoke-direct {p0, v1}, LaB/m;->a(Lam/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LaB/m;->e:Lam/f;

    invoke-direct {p0, v1}, LaB/m;->a(Lam/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, LaB/m;->d:Lam/f;

    invoke-direct {p0, v1}, LaB/m;->a(Lam/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-object v0
.end method

.method public h()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LaB/m;->g:Lam/f;

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, LaB/m;->g:Lam/f;

    if-eqz v2, :cond_3

    iget-object v2, p0, LaB/m;->e:Lam/f;

    if-eqz v2, :cond_3

    iget-object v2, p0, LaB/m;->d:Lam/f;

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public i()I
    .locals 2

    iget-object v0, p0, LaB/m;->c:LaB/n;

    invoke-virtual {v0}, LaB/n;->d()I

    move-result v0

    iget-object v1, p0, LaB/m;->d:Lam/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaB/m;->d:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, LaB/m;->e:Lam/f;

    if-eqz v1, :cond_1

    iget-object v1, p0, LaB/m;->e:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, LaB/m;->f:Lam/f;

    if-eqz v1, :cond_2

    iget-object v1, p0, LaB/m;->f:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, LaB/m;->g:Lam/f;

    if-eqz v1, :cond_3

    iget-object v1, p0, LaB/m;->g:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    return v0
.end method
