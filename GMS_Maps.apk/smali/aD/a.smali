.class public LaD/a;
.super LaD/j;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0}, LaD/j;-><init>()V

    iput-object p1, p0, LaD/a;->a:Landroid/view/MotionEvent;

    return-void
.end method

.method private h()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a(I)F
    .locals 1

    invoke-direct {p0}, LaD/a;->h()V

    iget-object v0, p0, LaD/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    invoke-direct {p0}, LaD/a;->h()V

    iget-object v0, p0, LaD/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)F
    .locals 1

    invoke-direct {p0}, LaD/a;->h()V

    iget-object v0, p0, LaD/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    invoke-direct {p0}, LaD/a;->h()V

    iget-object v0, p0, LaD/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    return v0
.end method

.method public c()F
    .locals 1

    invoke-direct {p0}, LaD/a;->h()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->t()F

    move-result v0

    return v0
.end method

.method public d()F
    .locals 1

    invoke-direct {p0}, LaD/a;->h()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()F

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, LaD/a;->h()V

    iget-object v0, p0, LaD/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, LaD/a;->a:Landroid/view/MotionEvent;

    return-void
.end method
