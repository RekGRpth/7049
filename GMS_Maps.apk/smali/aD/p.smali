.class public LaD/p;
.super LaD/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .locals 0

    invoke-direct {p0, p1}, LaD/e;-><init>(LaD/n;)V

    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;
    .locals 6

    const/4 v4, 0x1

    const/high16 v5, 0x3f000000

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    invoke-virtual {v0}, LaD/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LaD/f;->a:LaD/f;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    sget-object v0, LaD/f;->b:LaD/f;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    invoke-virtual {v0}, LaD/j;->b()I

    move-result v3

    if-le v3, v4, :cond_3

    move-object v1, v0

    :cond_4
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    if-nez v1, :cond_5

    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v2

    if-gt v2, v4, :cond_6

    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_0

    :cond_6
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    const v2, 0x3db2b8c2

    :goto_1
    invoke-virtual {v1}, LaD/j;->f()F

    move-result v3

    invoke-virtual {v0}, LaD/j;->f()F

    move-result v4

    invoke-static {v3, v4}, LaD/p;->a(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v2, v3, v2

    if-gez v2, :cond_8

    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_0

    :cond_7
    const v2, 0x3e32b8c2

    goto :goto_1

    :cond_8
    invoke-virtual {v0}, LaD/j;->c()F

    move-result v2

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v4

    add-float/2addr v2, v4

    mul-float/2addr v2, v5

    invoke-virtual {v0}, LaD/j;->g()F

    move-result v0

    div-float/2addr v0, v2

    const/high16 v4, 0x3f400000

    cmpg-float v4, v0, v4

    if-gez v4, :cond_9

    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_0

    :cond_9
    invoke-virtual {v1}, LaD/j;->g()F

    move-result v1

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_b

    div-float v0, v3, v0

    cmpg-float v1, v0, v5

    if-gez v1, :cond_a

    sget-object v0, LaD/f;->a:LaD/f;

    goto/16 :goto_0

    :cond_a
    const v1, 0x3f666666

    cmpg-float v0, v0, v1

    if-gez v0, :cond_b

    sget-object v0, LaD/f;->b:LaD/f;

    goto/16 :goto_0

    :cond_b
    sget-object v0, LaD/f;->c:LaD/f;

    goto/16 :goto_0
.end method

.method protected b(LaD/k;)Z
    .locals 1

    const-string v0, "r"

    invoke-virtual {p0, v0}, LaD/p;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaD/p;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->e(LaD/k;)Z

    move-result v0

    return v0
.end method

.method protected d(LaD/k;)V
    .locals 1

    iget-object v0, p0, LaD/p;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->f(LaD/k;)V

    return-void
.end method

.method protected f(LaD/k;)Z
    .locals 1

    iget-object v0, p0, LaD/p;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->d(LaD/k;)Z

    move-result v0

    return v0
.end method
