.class public LaD/t;
.super LaD/w;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .locals 2

    invoke-direct {p0, p1}, LaD/w;-><init>(LaD/n;)V

    const v0, 0x3d4ccccd

    iput v0, p0, LaD/t;->d:F

    const-wide v0, 0x3fd657184ae74487L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LaD/t;->e:F

    return-void
.end method


# virtual methods
.method protected a(F)F
    .locals 4

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected a(LaD/j;I)F
    .locals 1

    invoke-virtual {p1, p2}, LaD/j;->b(I)F

    move-result v0

    return v0
.end method

.method protected b(LaD/j;I)F
    .locals 1

    invoke-virtual {p1, p2}, LaD/j;->a(I)F

    move-result v0

    return v0
.end method

.method protected b(LaD/k;)Z
    .locals 1

    const-string v0, "s"

    invoke-virtual {p0, v0}, LaD/t;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaD/t;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->h(LaD/k;)Z

    move-result v0

    return v0
.end method

.method protected d(LaD/k;)V
    .locals 1

    iget-object v0, p0, LaD/t;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->i(LaD/k;)V

    return-void
.end method

.method protected f(LaD/k;)Z
    .locals 1

    iget-object v0, p0, LaD/t;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->g(LaD/k;)Z

    move-result v0

    return v0
.end method
