.class public LaD/v;
.super LaD/w;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .locals 0

    invoke-direct {p0, p1}, LaD/w;-><init>(LaD/n;)V

    return-void
.end method


# virtual methods
.method protected a(F)F
    .locals 4

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff921fb54442d18L

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected a(LaD/j;I)F
    .locals 1

    invoke-virtual {p1, p2}, LaD/j;->a(I)F

    move-result v0

    return v0
.end method

.method protected b(LaD/j;I)F
    .locals 1

    invoke-virtual {p1, p2}, LaD/j;->b(I)F

    move-result v0

    return v0
.end method

.method protected b(LaD/k;)Z
    .locals 1

    const-string v0, "t"

    invoke-virtual {p0, v0}, LaD/v;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaD/v;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->b(LaD/k;)Z

    move-result v0

    return v0
.end method

.method protected d(LaD/k;)V
    .locals 1

    iget-object v0, p0, LaD/v;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->c(LaD/k;)V

    return-void
.end method

.method protected f(LaD/k;)Z
    .locals 1

    iget-object v0, p0, LaD/v;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->a(LaD/k;)Z

    move-result v0

    return v0
.end method
