.class public LaE/a;
.super LaE/h;
.source "SourceFile"


# static fields
.field public static final a:LaE/a;

.field public static final b:Lcom/google/android/maps/driveabout/vector/aX;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaE/a;

    invoke-direct {v0}, LaE/a;-><init>()V

    sput-object v0, LaE/a;->a:LaE/a;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aV;->d:Lcom/google/android/maps/driveabout/vector/aX;

    sput-object v0, LaE/a;->b:Lcom/google/android/maps/driveabout/vector/aX;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LaE/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "David D."

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/s;)V
    .locals 2

    invoke-super {p0, p1}, LaE/h;->a(Lcom/google/googlenav/ui/s;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBaseAndroidView()Lcom/google/googlenav/ui/android/BaseAndroidView;

    move-result-object v1

    instance-of v1, v1, Lcom/google/googlenav/ui/android/AndroidVectorView;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBaseAndroidView()Lcom/google/googlenav/ui/android/BaseAndroidView;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->k()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;Z)V
    .locals 5

    const/4 v0, 0x0

    invoke-super {p0, p1, v0}, LaE/h;->a(Lcom/google/googlenav/ui/s;Z)V

    if-nez p2, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBaseAndroidView()Lcom/google/googlenav/ui/android/BaseAndroidView;

    move-result-object v1

    instance-of v1, v1, Lcom/google/googlenav/ui/android/AndroidVectorView;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBaseAndroidView()Lcom/google/googlenav/ui/android/BaseAndroidView;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    const/4 v1, 0x2

    const v2, 0x3e4ccccd

    const/4 v3, 0x0

    sget-object v4, LaE/a;->b:Lcom/google/android/maps/driveabout/vector/aX;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(IFFLcom/google/android/maps/driveabout/vector/aX;)V

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x5c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x5b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    const/16 v0, 0x21

    return v0
.end method
