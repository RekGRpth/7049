.class public LaE/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:LaE/f;


# instance fields
.field private final a:[LaE/e;

.field private final b:Ljava/util/Set;

.field private final c:Lcom/google/googlenav/ui/s;

.field private e:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/s;[LaE/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaE/f;->b:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaE/f;->e:Ljava/util/List;

    iput-object p1, p0, LaE/f;->c:Lcom/google/googlenav/ui/s;

    iput-object p2, p0, LaE/f;->a:[LaE/e;

    invoke-direct {p0}, LaE/f;->e()V

    return-void
.end method

.method public static a()LaE/f;
    .locals 1

    sget-object v0, LaE/f;->d:LaE/f;

    return-object v0
.end method

.method private a(LaE/e;Z)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, LaE/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaE/f;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LaE/f;->c:Lcom/google/googlenav/ui/s;

    invoke-interface {p1, v0, p2}, LaE/e;->a(Lcom/google/googlenav/ui/s;Z)V

    invoke-direct {p0, p2}, LaE/f;->a(Z)V

    if-nez p2, :cond_0

    const/16 v0, 0x47

    const-string v1, "a"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, LaE/e;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ui/s;[LaE/e;)V
    .locals 1

    new-instance v0, LaE/f;

    invoke-direct {v0, p0, p1}, LaE/f;-><init>(Lcom/google/googlenav/ui/s;[LaE/e;)V

    sput-object v0, LaE/f;->d:LaE/f;

    return-void
.end method

.method private a(Ljava/lang/String;[I)V
    .locals 5

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    array-length v0, p2

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    array-length v3, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, p2, v0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "FLASH"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    goto :goto_1
.end method

.method private a(Z)V
    .locals 2

    invoke-direct {p0}, LaE/f;->g()[I

    move-result-object v0

    if-nez p1, :cond_0

    const-string v1, "Labs"

    invoke-direct {p0, v1, v0}, LaE/f;->a(Ljava/lang/String;[I)V

    :cond_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->b([I)V

    :cond_1
    return-void
.end method

.method public static a(I)Z
    .locals 5

    const/4 v0, 0x0

    const-string v1, "Labs"

    invoke-static {v1}, LaE/f;->a(Ljava/lang/String;)[I

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    if-ne v4, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)[I
    .locals 6

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v3

    if-nez v3, :cond_1

    new-array v0, v2, [I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-interface {v3}, Ljava/io/DataInput;->readInt()I

    move-result v4

    new-array v0, v4, [I

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-interface {v3}, Ljava/io/DataInput;->readInt()I

    move-result v5

    aput v5, v0, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "FLASH"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    new-array v0, v2, [I

    goto :goto_0
.end method

.method private b(I)LaE/e;
    .locals 5

    iget-object v2, p0, LaE/f;->a:[LaE/e;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-interface {v0}, LaE/e;->d()I

    move-result v4

    if-ne v4, p1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e()V
    .locals 10

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "LabsObsolete"

    invoke-static {v2}, LaE/f;->a(Ljava/lang/String;)[I

    move-result-object v3

    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget v5, v3, v2

    iget-object v6, p0, LaE/f;->e:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "Labs"

    invoke-static {v2}, LaE/f;->a(Ljava/lang/String;)[I

    move-result-object v4

    array-length v5, v4

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v5, :cond_4

    aget v6, v4, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v7}, LaE/f;->b(I)LaE/e;

    move-result-object v7

    if-nez v7, :cond_2

    iget-object v2, p0, LaE/f;->e:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, LaE/f;->e:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_1
    move v2, v0

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v9, v0

    move v0, v2

    move v2, v9

    goto :goto_1

    :cond_2
    iget-object v8, p0, LaE/f;->e:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v1

    :cond_3
    invoke-direct {p0, v7, v1}, LaE/f;->a(LaE/e;Z)V

    move v9, v2

    move v2, v0

    move v0, v9

    goto :goto_2

    :cond_4
    if-eqz v2, :cond_5

    const-string v1, "Labs"

    invoke-direct {p0}, LaE/f;->g()[I

    move-result-object v2

    invoke-direct {p0, v1, v2}, LaE/f;->a(Ljava/lang/String;[I)V

    :cond_5
    if-eqz v0, :cond_6

    invoke-direct {p0}, LaE/f;->f()V

    :cond_6
    return-void
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, LaE/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    iget-object v0, p0, LaE/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const-string v0, "LabsObsolete"

    invoke-direct {p0, v0, v2}, LaE/f;->a(Ljava/lang/String;[I)V

    return-void
.end method

.method private g()[I
    .locals 4

    invoke-virtual {p0}, LaE/f;->c()[LaE/e;

    move-result-object v1

    array-length v0, v1

    new-array v2, v0, [I

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, LaE/e;->d()I

    move-result v3

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(LaE/e;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LaE/f;->a(LaE/e;Z)V

    return-void
.end method

.method public a(Ljava/lang/Integer;)Z
    .locals 1

    iget-object v0, p0, LaE/f;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LaE/f;->f()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(LaE/e;)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, LaE/e;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaE/f;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, LaE/f;->c:Lcom/google/googlenav/ui/s;

    invoke-interface {p1, v0}, LaE/e;->a(Lcom/google/googlenav/ui/s;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaE/f;->a(Z)V

    const/16 v0, 0x47

    const-string v1, "d"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, LaE/e;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()[LaE/e;
    .locals 1

    iget-object v0, p0, LaE/f;->a:[LaE/e;

    return-object v0
.end method

.method public c()[LaE/e;
    .locals 2

    iget-object v0, p0, LaE/f;->b:Ljava/util/Set;

    iget-object v1, p0, LaE/f;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [LaE/e;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaE/e;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LaE/f;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaE/e;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {v0}, LaE/e;->d()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
