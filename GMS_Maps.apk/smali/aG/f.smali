.class public LaG/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:LaG/m;


# direct methods
.method constructor <init>(IIILaG/m;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LaG/f;->a:I

    iput p2, p0, LaG/f;->b:I

    iput p3, p0, LaG/f;->c:I

    iput-object p4, p0, LaG/f;->d:LaG/m;

    return-void
.end method

.method static synthetic b(LaG/f;)I
    .locals 1

    iget v0, p0, LaG/f;->a:I

    return v0
.end method


# virtual methods
.method public a(LaG/f;)I
    .locals 2

    iget v0, p0, LaG/f;->a:I

    iget v1, p1, LaG/f;->a:I

    invoke-static {v0, v1}, Lac/a;->a(II)I

    move-result v0

    return v0
.end method

.method public a()Z
    .locals 1

    iget v0, p0, LaG/f;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget v0, p0, LaG/f;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LaG/f;->c:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LaG/f;

    invoke-virtual {p0, p1}, LaG/f;->a(LaG/f;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget v0, p0, LaG/f;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Lam/f;
    .locals 1

    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->f()Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public h()LaG/m;
    .locals 1

    iget-object v0, p0, LaG/f;->d:LaG/m;

    return-object v0
.end method

.method public i()J
    .locals 2

    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->g()Z

    move-result v0

    return v0
.end method
