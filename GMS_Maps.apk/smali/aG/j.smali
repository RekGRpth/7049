.class public LaG/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaB/s;

.field private final b:Ljava/util/Set;

.field private final c:Ljava/util/List;


# direct methods
.method private constructor <init>(LaB/s;Ljava/util/Set;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaG/j;->a:LaB/s;

    iput-object p2, p0, LaG/j;->b:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LaG/j;->c:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(LaB/s;Ljava/util/Set;LaG/i;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LaG/j;-><init>(LaB/s;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic a(LaG/j;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(LaB/p;)I
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x14

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v2, :cond_3

    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/m;

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    invoke-static {v0}, LaG/h;->c(LaG/m;)Lcom/google/googlenav/ui/bs;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v6, p0, LaG/j;->b:Ljava/util/Set;

    invoke-virtual {v0}, LaG/m;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, LaG/j;->a:LaB/s;

    new-instance v1, LaG/k;

    invoke-direct {v1, p0, p1}, LaG/k;-><init>(LaG/j;LaB/p;)V

    invoke-virtual {v0, v3, v1}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public a()V
    .locals 4

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    monitor-enter v1

    :try_start_0
    new-instance v0, LaG/l;

    invoke-direct {v0, p0, v1}, LaG/l;-><init>(LaG/j;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, LaG/j;->a(LaB/p;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const-wide/16 v2, 0x2710

    :try_start_1
    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(LaG/m;)V
    .locals 1

    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
