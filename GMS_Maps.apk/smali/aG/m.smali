.class public final LaG/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Z

.field private final c:J

.field private d:Lam/f;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-boolean p2, p0, LaG/m;->b:Z

    iput-wide p3, p0, LaG/m;->c:J

    return-void
.end method

.method private static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    const-wide/32 v0, 0x493e0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/16 v0, 0x211

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "%s - %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p0, p1, v3, v3}, Lcom/google/googlenav/b;->a(JZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized a(Lam/f;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaG/m;->d:Lam/f;

    if-nez v0, :cond_0

    iput-object p1, p0, LaG/m;->d:Lam/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 8

    const-wide/16 v6, 0x0

    const-string v0, ""

    iget-object v1, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    const/4 v4, 0x4

    invoke-static {v1, v4}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p0, LaG/m;->c:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-wide v4, p0, LaG/m;->c:J

    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    const-wide/32 v4, 0x240c8400

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    invoke-static {v2, v3, v1}, LaG/m;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaG/m;->d:Lam/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, LaG/m;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LaG/m;->a()J

    move-result-wide v1

    check-cast p1, LaG/m;

    invoke-virtual {p1}, LaG/m;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public declared-synchronized f()Lam/f;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaG/m;->d:Lam/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaG/m;->d:Lam/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LaG/h;->a:Lam/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, LaG/m;->b:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, LaG/m;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lac/b;->a(J)I

    move-result v0

    return v0
.end method
