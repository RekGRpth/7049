.class public LaG/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/a;

.field private b:LaG/g;

.field private c:LaG/d;

.field private d:LaG/d;

.field private e:Lcom/google/common/collect/ImmutableList;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaG/n;->a:Lcom/google/googlenav/a;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, LaG/n;->e:Lcom/google/common/collect/ImmutableList;

    const-string v0, ""

    iput-object v0, p0, LaG/n;->f:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/util/List;)LaG/f;
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    invoke-virtual {v0}, LaG/f;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(J)Ljava/lang/String;
    .locals 4

    const-wide/16 v0, 0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    const/16 v0, 0x95

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x94

    goto :goto_0
.end method

.method static a(JJ)Ljava/lang/String;
    .locals 6

    const-wide/16 v4, 0x1

    const-wide/32 v0, 0x240c8400

    add-long/2addr v0, p2

    sub-long/2addr v0, p0

    sub-long/2addr v0, v4

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    add-long/2addr v0, v4

    invoke-static {v0, v1}, LaG/n;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(JLjava/util/TimeZone;)Ljava/lang/String;
    .locals 5

    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    const-wide/32 v1, 0x240c8400

    sub-long v1, p0, v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    sub-long v2, p0, v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "%s - %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(LaG/d;I)Ljava/util/List;
    .locals 6

    const/4 v4, 0x0

    invoke-virtual {p0}, LaG/d;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-gt v5, p1, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v3, -0x1

    move v2, v4

    :goto_1
    if-ge v2, v5, :cond_5

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    invoke-virtual {v0}, LaG/f;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_2
    if-gez v0, :cond_2

    invoke-interface {v1, v4, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    if-nez v0, :cond_3

    invoke-interface {v1, v4, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v2, p1, -0x2

    div-int/lit8 v3, v2, 0x2

    add-int/lit8 v5, v5, -0x1

    sub-int/2addr v5, v0

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v2, v0, v2

    add-int/2addr v3, v0

    const/4 v0, 0x1

    if-gt v2, v0, :cond_4

    invoke-interface {v1, v4, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_2
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, LaG/n;->a:Lcom/google/googlenav/a;

    invoke-virtual {v0}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaG/n;->c:LaG/d;

    invoke-static {v0, p1}, LaG/n;->a(LaG/d;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a(LaG/d;)V
    .locals 0

    iput-object p1, p0, LaG/n;->c:LaG/d;

    return-void
.end method

.method a(LaG/g;)V
    .locals 0

    iput-object p1, p0, LaG/n;->b:LaG/g;

    return-void
.end method

.method a(LaG/g;JJ)V
    .locals 2

    sget-object v0, LaG/o;->a:[I

    invoke-virtual {p1}, LaG/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    iput-object v0, p0, LaG/n;->f:Ljava/lang/String;

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {p4, p5, v0}, LaG/n;->a(JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaG/n;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    invoke-static {p2, p3, p4, p5}, LaG/n;->a(JJ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaG/n;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    const-string v0, ""

    iput-object v0, p0, LaG/n;->f:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Lcom/google/common/collect/ImmutableList;)V
    .locals 0

    iput-object p1, p0, LaG/n;->e:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaG/n;->f:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaG/n;->d:LaG/d;

    invoke-static {v0, p1}, LaG/n;->a(LaG/d;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method b(LaG/d;)V
    .locals 0

    iput-object p1, p0, LaG/n;->d:LaG/d;

    return-void
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaG/n;->e:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaG/n;->d:LaG/d;

    invoke-virtual {v0}, LaG/d;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, LaG/n;->c:LaG/d;

    invoke-virtual {v0}, LaG/d;->c()I

    move-result v0

    iget-object v1, p0, LaG/n;->d:LaG/d;

    invoke-virtual {v1}, LaG/d;->c()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, LaG/n;->c:LaG/d;

    invoke-virtual {v0}, LaG/d;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, LaG/n;->a(Ljava/util/List;)LaG/f;

    move-result-object v0

    iget-object v1, p0, LaG/n;->d:LaG/d;

    invoke-virtual {v1}, LaG/d;->b()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, LaG/n;->a(Ljava/util/List;)LaG/f;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, LaG/f;->c()I

    move-result v1

    invoke-virtual {v0}, LaG/f;->c()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method
