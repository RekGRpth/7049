.class public abstract LaH/b;
.super LaH/c;
.source "SourceFile"


# instance fields
.field private final a:LaH/C;

.field protected final b:Landroid/location/LocationManager;


# direct methods
.method protected constructor <init>(Z)V
    .locals 2

    invoke-direct {p0, p1}, LaH/c;-><init>(Z)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    iput-object v0, p0, LaH/b;->b:Landroid/location/LocationManager;

    sget-boolean v0, Lcom/google/googlenav/android/E;->i:Z

    if-eqz v0, :cond_0

    new-instance v0, LaH/C;

    iget-object v1, p0, LaH/b;->b:Landroid/location/LocationManager;

    invoke-direct {v0, v1}, LaH/C;-><init>(Landroid/location/LocationManager;)V

    :goto_0
    iput-object v0, p0, LaH/b;->a:LaH/C;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, LaH/b;->a:LaH/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/b;->a:LaH/C;

    invoke-virtual {v0, p1}, LaH/C;->a(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaH/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaH/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static b(Landroid/location/Location;)Lo/D;
    .locals 5

    const/high16 v4, -0x80000000

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->t()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "levelId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "levelNumberE3"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v2, :cond_0

    invoke-static {v2}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v2

    if-eqz v2, :cond_0

    if-ne v1, v4, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing level number for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "LOCATION"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    new-instance v0, Lo/D;

    invoke-direct {v0, v2, v1}, Lo/D;-><init>(Lo/r;I)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LaH/c;->f()V

    iget-object v0, p0, LaH/b;->a:LaH/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/b;->a:LaH/C;

    invoke-virtual {v0}, LaH/C;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaH/b;->a:LaH/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/b;->a:LaH/C;

    invoke-virtual {v0}, LaH/C;->b()V

    :cond_0
    invoke-super {p0}, LaH/c;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public i()Z
    .locals 1

    invoke-virtual {p0}, LaH/b;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaH/b;->j()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    :try_start_0
    const-string v0, "network"

    invoke-direct {p0, v0}, LaH/b;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    :try_start_0
    const-string v0, "gps"

    invoke-direct {p0, v0}, LaH/b;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
