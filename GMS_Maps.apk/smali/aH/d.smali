.class public LaH/d;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:LaN/B;

.field private final b:I

.field private final c:I

.field private final d:LaI/b;

.field private final e:LaL/d;

.field private final f:LaH/m;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Z

.field private final j:J

.field private final k:LaH/f;

.field private final l:Ljava/lang/String;

.field private final m:LaN/B;

.field private final n:Z

.field private final o:Ljava/lang/String;


# direct methods
.method private constructor <init>(LaN/B;IILaI/b;LaL/d;LaH/m;Ljava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, LaH/d;->a:LaN/B;

    iput p2, p0, LaH/d;->b:I

    iput p3, p0, LaH/d;->c:I

    iput-object p4, p0, LaH/d;->d:LaI/b;

    iput-object p5, p0, LaH/d;->e:LaL/d;

    iput-object p6, p0, LaH/d;->f:LaH/m;

    iput-object p7, p0, LaH/d;->g:Ljava/lang/String;

    iput p8, p0, LaH/d;->h:I

    iput-boolean p9, p0, LaH/d;->i:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iput-wide v1, p0, LaH/d;->j:J

    iput-object p10, p0, LaH/d;->k:LaH/f;

    iput-object p11, p0, LaH/d;->l:Ljava/lang/String;

    iput-object p12, p0, LaH/d;->m:LaN/B;

    move/from16 v0, p13

    iput-boolean v0, p0, LaH/d;->n:Z

    move-object/from16 v0, p14

    iput-object v0, p0, LaH/d;->o:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(LaN/B;IILaI/b;LaL/d;LaH/m;Ljava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;LaH/e;)V
    .locals 0

    invoke-direct/range {p0 .. p14}, LaH/d;-><init>(LaN/B;IILaI/b;LaL/d;LaH/m;Ljava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaH/f;
    .locals 3

    new-instance v0, LaH/f;

    invoke-static {}, LaL/d;->d()[LaL/d;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, LaH/f;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LaL/d;LaH/e;)V

    return-object v0
.end method

.method public static a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 15

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, LaH/m;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    invoke-static {v0}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaH/f;

    move-result-object v2

    const/4 v5, 0x0

    invoke-static {}, LaI/b;->d()LaI/b;

    move-result-object v0

    invoke-static {}, LaL/d;->c()LaL/d;

    move-result-object v1

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    const/4 v9, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move v10, p0

    move/from16 v11, p1

    invoke-static/range {v0 .. v14}, LaH/d;->a(LaI/b;LaL/d;LaH/f;IILjava/lang/String;LaH/m;JIIILaN/B;ZLjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LaH/m;JII)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const/4 v5, 0x2

    const/4 v0, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lby/Q;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz p0, :cond_2

    invoke-interface {p0}, LaH/m;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {p0}, LaH/m;->s()LaH/h;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x3

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v3, 0x6

    invoke-virtual {v1, v3, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2}, Landroid/location/Location;->hasSpeed()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0xc

    invoke-virtual {v2}, Landroid/location/Location;->getSpeed()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {v2}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0xd

    invoke-virtual {v2}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lby/O;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v5, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p0, :cond_3

    invoke-interface {p0}, LaH/m;->p()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_3

    const/4 v0, 0x0

    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method private static a(LaI/b;LaL/d;LaH/f;IILjava/lang/String;LaH/m;JIIILaN/B;ZLjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 11

    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lby/aF;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lby/Q;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v7, 0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/googlenav/common/Config;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, LaH/g;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v8, 0x3

    invoke-virtual {v6, v8, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {p0}, LaI/b;->f()Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v7, 0x6

    invoke-virtual {p0}, LaI/b;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {p1}, LaL/d;->f()Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x7

    invoke-virtual {p1}, LaL/d;->i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lby/Q;->j:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v7, 0x3

    invoke-virtual {v6, v7, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v7, 0x2

    if-ne p3, v7, :cond_3

    const/4 v7, 0x4

    invoke-virtual {v6, v7, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v7, 0x5

    if-eqz p5, :cond_8

    :goto_0
    move-object/from16 v0, p5

    invoke-virtual {v6, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    if-lez p9, :cond_4

    const/4 v7, 0x6

    move/from16 v0, p9

    invoke-virtual {v6, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    const/4 v7, 0x2

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v6, p3

    move-object/from16 v7, p6

    move-object/from16 v8, p12

    move/from16 v9, p13

    move-object/from16 v10, p14

    invoke-static/range {v5 .. v10}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaH/m;LaN/B;ZLjava/lang/String;)V

    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, LaI/b;->f()Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x1

    invoke-virtual {p0}, LaI/b;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    invoke-virtual {p1}, LaL/d;->f()Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x2

    invoke-virtual {p1}, LaL/d;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_6
    move-object/from16 v0, p6

    move-wide/from16 v1, p7

    move/from16 v3, p10

    move/from16 v4, p11

    invoke-static {v0, v1, v2, v3, v4}, LaH/d;->a(LaH/m;JII)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v6, v8, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v7, 0x4

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    if-eqz p2, :cond_7

    invoke-virtual {p2}, LaH/f;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-static {v5, v6}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p2}, LaH/f;->b()[LaL/d;

    move-result-object v6

    invoke-static {v5, v6}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LaL/d;)V

    :cond_7
    return-object v5

    :cond_8
    const-string p5, ""

    goto :goto_0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    const/4 v6, 0x4

    const/4 v2, 0x1

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    cmp-long v4, p1, v4

    if-nez v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V
    .locals 14

    if-nez p5, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, LaH/e;

    move-object v2, p1

    move-object/from16 v3, p5

    move/from16 v4, p6

    move v5, p0

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p7

    move-object/from16 v9, p2

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v1 .. v13}, LaH/e;-><init>(LaH/m;LaN/B;IILjava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    new-instance v2, Las/d;

    move-object/from16 v0, p8

    invoke-direct {v2, v0, v1}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Las/d;->g()V

    goto :goto_0
.end method

.method public static a(LaH/m;LaN/B;ZLjava/lang/String;Las/c;)V
    .locals 13

    if-eqz p0, :cond_0

    invoke-interface {p0}, LaH/m;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaH/f;

    move-result-object v2

    invoke-interface {p0}, LaH/m;->s()LaH/h;

    move-result-object v1

    const/16 v0, 0x80

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v5

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object/from16 v8, p4

    move-object v10, p1

    move v11, p2

    move-object/from16 v12, p3

    invoke-static/range {v0 .. v12}, LaH/d;->a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static a(LaN/B;ILas/c;)V
    .locals 13

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x7

    move-object v2, v1

    move-object v3, v1

    move-object v5, p0

    move v6, p1

    move v7, v4

    move-object v8, p2

    move-object v9, v1

    move-object v10, v1

    move v11, v4

    move-object v12, v1

    invoke-static/range {v0 .. v12}, LaH/d;->a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaH/m;LaN/B;ZLjava/lang/String;)V
    .locals 3

    const/16 v0, 0x80

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, LaH/m;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-static {v0, p3, p4, p5}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ZLjava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ZLjava/lang/String;)V
    .locals 6

    const/4 v3, 0x7

    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lby/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    :try_start_0
    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p1, :cond_0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lby/O;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v4, 0x1

    invoke-virtual {p1}, LaN/B;->d()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    invoke-virtual {p1}, LaN/B;->f()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {v3, v2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/4 v2, 0x4

    if-eqz p2, :cond_2

    :goto_0
    invoke-virtual {v3, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(II[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LaL/d;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    aget-object v1, p1, v0

    invoke-virtual {v1}, LaL/d;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LaL/d;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;ILaN/B;ILas/c;)V
    .locals 13

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move/from16 v6, p3

    move-object/from16 v8, p4

    invoke-static/range {v0 .. v12}, LaH/d;->a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 18

    new-instance v17, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/el;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    move-object/from16 v0, p0

    iget v2, v0, LaH/d;->c:I

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    const/4 v3, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LaH/d;->d:LaI/b;

    move-object/from16 v0, p0

    iget-object v3, v0, LaH/d;->e:LaL/d;

    move-object/from16 v0, p0

    iget-object v4, v0, LaH/d;->k:LaH/f;

    move-object/from16 v0, p0

    iget v5, v0, LaH/d;->c:I

    move-object/from16 v0, p0

    iget v6, v0, LaH/d;->h:I

    move-object/from16 v0, p0

    iget-object v7, v0, LaH/d;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LaH/d;->f:LaH/m;

    move-object/from16 v0, p0

    iget-wide v9, v0, LaH/d;->j:J

    move-object/from16 v0, p0

    iget v11, v0, LaH/d;->b:I

    move-object/from16 v0, p0

    iget-object v12, v0, LaH/d;->a:LaN/B;

    invoke-virtual {v12}, LaN/B;->c()I

    move-result v12

    mul-int/lit8 v12, v12, 0xa

    move-object/from16 v0, p0

    iget-object v13, v0, LaH/d;->a:LaN/B;

    invoke-virtual {v13}, LaN/B;->e()I

    move-result v13

    mul-int/lit8 v13, v13, 0xa

    move-object/from16 v0, p0

    iget-object v14, v0, LaH/d;->m:LaN/B;

    move-object/from16 v0, p0

    iget-boolean v15, v0, LaH/d;->n:Z

    move-object/from16 v0, p0

    iget-object v0, v0, LaH/d;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v2 .. v16}, LaH/d;->a(LaI/b;LaL/d;LaH/f;IILjava/lang/String;LaH/m;JIIILaN/B;ZLjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, LaH/d;->c:I

    const/16 v4, 0x81

    if-ne v3, v4, :cond_0

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget-object v5, v0, LaH/d;->l:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v3, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, LaH/d;->c:I

    const/16 v3, 0x81

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/el;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaH/g;->a(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x29

    return v0
.end method

.method public s_()Z
    .locals 1

    iget-boolean v0, p0, LaH/d;->i:Z

    return v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GlsLocationReport[mapPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->a:LaN/B;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", zoomLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaH/d;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaH/d;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cellTowerInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->d:LaI/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectedWifi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->e:LaL/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->f:LaH/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchTerm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaH/d;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", immediate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LaH/d;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, LaH/d;->j:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locationInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->k:LaH/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
