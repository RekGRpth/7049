.class public LaH/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:D

.field private c:F

.field private d:Landroid/os/Bundle;

.field private e:D

.field private f:D

.field private g:Ljava/lang/String;

.field private h:F

.field private i:J

.field private j:J

.field private k:Lo/D;

.field private l:LaN/B;

.field private m:LaH/l;

.field private n:LaH/k;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, LaH/j;->o:Z

    iput-boolean v0, p0, LaH/j;->p:Z

    iput-boolean v0, p0, LaH/j;->q:Z

    iput-boolean v0, p0, LaH/j;->r:Z

    iput-boolean v0, p0, LaH/j;->s:Z

    iput-boolean v0, p0, LaH/j;->t:Z

    return-void
.end method

.method static synthetic a(LaH/j;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaH/j;->g:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/location/Location;)V
    .locals 4

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p0, v0}, LaH/j;->a(F)LaH/j;

    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaH/j;->a(D)LaH/j;

    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p0, v0}, LaH/j;->b(F)LaH/j;

    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, LaH/j;->a(DD)LaH/j;

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-virtual {p0, v0}, LaH/j;->c(F)LaH/j;

    :cond_3
    return-void
.end method

.method static synthetic b(LaH/j;)Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->o:Z

    return v0
.end method

.method static synthetic c(LaH/j;)F
    .locals 1

    iget v0, p0, LaH/j;->a:F

    return v0
.end method

.method static synthetic d(LaH/j;)Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->p:Z

    return v0
.end method

.method static synthetic e(LaH/j;)D
    .locals 2

    iget-wide v0, p0, LaH/j;->b:D

    return-wide v0
.end method

.method static synthetic f(LaH/j;)Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->q:Z

    return v0
.end method

.method static synthetic g(LaH/j;)F
    .locals 1

    iget v0, p0, LaH/j;->c:F

    return v0
.end method

.method static synthetic h(LaH/j;)D
    .locals 2

    iget-wide v0, p0, LaH/j;->e:D

    return-wide v0
.end method

.method static synthetic i(LaH/j;)D
    .locals 2

    iget-wide v0, p0, LaH/j;->f:D

    return-wide v0
.end method

.method static synthetic j(LaH/j;)Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->r:Z

    return v0
.end method

.method static synthetic k(LaH/j;)F
    .locals 1

    iget v0, p0, LaH/j;->h:F

    return v0
.end method

.method static synthetic l(LaH/j;)Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->s:Z

    return v0
.end method

.method static synthetic m(LaH/j;)J
    .locals 2

    iget-wide v0, p0, LaH/j;->i:J

    return-wide v0
.end method

.method static synthetic n(LaH/j;)Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->t:Z

    return v0
.end method

.method static synthetic o(LaH/j;)J
    .locals 2

    iget-wide v0, p0, LaH/j;->j:J

    return-wide v0
.end method

.method static synthetic p(LaH/j;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, LaH/j;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method private q()LaH/l;
    .locals 1

    iget-object v0, p0, LaH/j;->m:LaH/l;

    if-nez v0, :cond_0

    new-instance v0, LaH/l;

    invoke-direct {v0}, LaH/l;-><init>()V

    iput-object v0, p0, LaH/j;->m:LaH/l;

    :cond_0
    iget-object v0, p0, LaH/j;->m:LaH/l;

    return-object v0
.end method

.method static synthetic q(LaH/j;)LaN/B;
    .locals 1

    iget-object v0, p0, LaH/j;->l:LaN/B;

    return-object v0
.end method

.method private r()LaH/k;
    .locals 1

    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-nez v0, :cond_0

    new-instance v0, LaH/k;

    invoke-direct {v0}, LaH/k;-><init>()V

    iput-object v0, p0, LaH/j;->n:LaH/k;

    :cond_0
    iget-object v0, p0, LaH/j;->n:LaH/k;

    return-object v0
.end method

.method static synthetic r(LaH/j;)Lo/D;
    .locals 1

    iget-object v0, p0, LaH/j;->k:Lo/D;

    return-object v0
.end method

.method static synthetic s(LaH/j;)LaH/l;
    .locals 1

    iget-object v0, p0, LaH/j;->m:LaH/l;

    return-object v0
.end method

.method static synthetic t(LaH/j;)LaH/k;
    .locals 1

    iget-object v0, p0, LaH/j;->n:LaH/k;

    return-object v0
.end method


# virtual methods
.method public a()LaH/j;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, LaH/j;->c:F

    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/j;->q:Z

    return-object p0
.end method

.method public a(D)LaH/j;
    .locals 1

    iput-wide p1, p0, LaH/j;->b:D

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->p:Z

    return-object p0
.end method

.method public a(DD)LaH/j;
    .locals 5

    const-wide v3, 0x412e848000000000L

    iput-wide p1, p0, LaH/j;->e:D

    iput-wide p3, p0, LaH/j;->f:D

    new-instance v0, LaN/B;

    mul-double v1, p1, v3

    double-to-int v1, v1

    mul-double v2, p3, v3

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    iput-object v0, p0, LaH/j;->l:LaN/B;

    return-object p0
.end method

.method public a(F)LaH/j;
    .locals 1

    iput p1, p0, LaH/j;->a:F

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->o:Z

    return-object p0
.end method

.method public a(I)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->r()LaH/k;

    move-result-object v0

    iput p1, v0, LaH/k;->c:I

    return-object p0
.end method

.method public a(J)LaH/j;
    .locals 1

    iput-wide p1, p0, LaH/j;->i:J

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->s:Z

    return-object p0
.end method

.method public a(LO/H;)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-object p1, v0, LaH/l;->d:LO/H;

    return-object p0
.end method

.method public a(LaN/B;)LaH/j;
    .locals 4

    const-wide v2, 0x412e848000000000L

    iput-object p1, p0, LaH/j;->l:LaN/B;

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, LaH/j;->e:D

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, LaH/j;->f:D

    return-object p0
.end method

.method public a(Landroid/location/Location;)LaH/j;
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-direct {p0, p1}, LaH/j;->b(Landroid/location/Location;)V

    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Landroid/os/Bundle;)LaH/j;

    invoke-static {p1}, LaH/b;->b(Landroid/location/Location;)Lo/D;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Lo/D;)LaH/j;

    instance-of v0, p1, LaH/h;

    if-eqz v0, :cond_4

    check-cast p1, LaH/h;

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->t:Z

    invoke-static {p1}, LaH/h;->b(LaH/h;)J

    move-result-wide v0

    iput-wide v0, p0, LaH/j;->j:J

    invoke-virtual {p1}, LaH/h;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaH/h;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaH/j;->a(J)LaH/j;

    :cond_2
    invoke-static {p1}, LaH/h;->c(LaH/h;)LaH/l;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, LaH/l;

    invoke-static {p1}, LaH/h;->c(LaH/h;)LaH/l;

    move-result-object v1

    invoke-direct {v0, v1}, LaH/l;-><init>(LaH/l;)V

    iput-object v0, p0, LaH/j;->m:LaH/l;

    :cond_3
    invoke-static {p1}, LaH/h;->d(LaH/h;)LaH/k;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, LaH/k;

    invoke-static {p1}, LaH/h;->d(LaH/h;)LaH/k;

    move-result-object v1

    invoke-direct {v0, v1}, LaH/k;-><init>(LaH/k;)V

    iput-object v0, p0, LaH/j;->n:LaH/k;

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaH/j;->a(J)LaH/j;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)LaH/j;
    .locals 0

    iput-object p1, p0, LaH/j;->d:Landroid/os/Bundle;

    return-object p0
.end method

.method public a(Ljava/lang/String;)LaH/j;
    .locals 0

    iput-object p1, p0, LaH/j;->g:Ljava/lang/String;

    return-object p0
.end method

.method public a(Lo/D;)LaH/j;
    .locals 1

    iput-object p1, p0, LaH/j;->k:Lo/D;

    if-eqz p1, :cond_0

    invoke-static {p1}, LaK/a;->a(Lo/D;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Landroid/os/Bundle;)LaH/j;

    :cond_0
    return-object p0
.end method

.method public a(Lo/af;Lo/T;)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-object p1, v0, LaH/l;->b:Lo/af;

    if-nez p1, :cond_0

    const/4 p2, 0x0

    :cond_0
    iput-object p2, v0, LaH/l;->c:Lo/T;

    return-object p0
.end method

.method public a(Z)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->r()LaH/k;

    move-result-object v0

    iput-boolean p1, v0, LaH/k;->a:Z

    return-object p0
.end method

.method public b()LaH/j;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, LaH/j;->h:F

    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/j;->r:Z

    return-object p0
.end method

.method public b(D)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-wide p1, v0, LaH/l;->e:D

    return-object p0
.end method

.method public b(F)LaH/j;
    .locals 1

    iput p1, p0, LaH/j;->c:F

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->q:Z

    return-object p0
.end method

.method public b(I)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput p1, v0, LaH/l;->f:I

    return-object p0
.end method

.method public b(Z)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->r()LaH/k;

    move-result-object v0

    iput-boolean p1, v0, LaH/k;->b:Z

    return-object p0
.end method

.method public c()LaH/j;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/j;->s:Z

    return-object p0
.end method

.method public c(F)LaH/j;
    .locals 1

    iput p1, p0, LaH/j;->h:F

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->r:Z

    return-object p0
.end method

.method public c(Z)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-boolean p1, v0, LaH/l;->a:Z

    return-object p0
.end method

.method public d()LaH/h;
    .locals 2

    iget-object v0, p0, LaH/j;->l:LaN/B;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, LaH/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaH/h;-><init>(LaH/j;LaH/i;)V

    return-object v0
.end method

.method public d(Z)LaH/j;
    .locals 1

    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-boolean p1, v0, LaH/l;->g:Z

    return-object p0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->o:Z

    return v0
.end method

.method public f()F
    .locals 1

    iget v0, p0, LaH/j;->a:F

    return v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget v0, v0, LaH/k;->c:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget v0, v0, LaH/k;->c:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->q:Z

    return v0
.end method

.method public j()F
    .locals 1

    iget v0, p0, LaH/j;->c:F

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, LaH/j;->r:Z

    return v0
.end method

.method public l()F
    .locals 1

    iget v0, p0, LaH/j;->h:F

    return v0
.end method

.method public m()D
    .locals 2

    iget-wide v0, p0, LaH/j;->e:D

    return-wide v0
.end method

.method public n()D
    .locals 2

    iget-wide v0, p0, LaH/j;->f:D

    return-wide v0
.end method

.method public o()LO/H;
    .locals 1

    iget-object v0, p0, LaH/j;->m:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/j;->m:LaH/l;

    iget-object v0, v0, LaH/l;->d:LO/H;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget-boolean v0, v0, LaH/k;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget-boolean v0, v0, LaH/k;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
