.class public LaI/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:LaI/c;

.field private static b:Z

.field private static n:LaI/b;

.field private static o:J

.field private static p:I


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, LaI/b;->b:Z

    const-wide/32 v0, -0x927c0

    sput-wide v0, LaI/b;->o:J

    const/4 v0, -0x1

    sput v0, LaI/b;->p:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIIIIIJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaI/b;->c:Ljava/lang/String;

    iput p3, p0, LaI/b;->j:I

    iput p4, p0, LaI/b;->i:I

    iput p5, p0, LaI/b;->h:I

    iput p6, p0, LaI/b;->f:I

    iput p7, p0, LaI/b;->g:I

    iput p8, p0, LaI/b;->d:I

    iput p9, p0, LaI/b;->e:I

    iput p2, p0, LaI/b;->l:I

    iput p10, p0, LaI/b;->k:I

    iput-wide p11, p0, LaI/b;->m:J

    return-void
.end method

.method public static a(Ljava/lang/String;IIIIIIIII)LaI/b;
    .locals 13

    new-instance v0, LaI/b;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v11

    move-object v1, p0

    move v2, p1

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v12}, LaI/b;-><init>(Ljava/lang/String;IIIIIIIIIJ)V

    return-object v0
.end method

.method public static a(LaI/c;)V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, LaI/b;->b:Z

    if-nez p0, :cond_0

    invoke-static {}, LaI/b;->b()V

    :cond_0
    sput-object p0, LaI/b;->a:LaI/c;

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, LaI/b;->b:Z

    return v0
.end method

.method static b()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, LaI/b;->b:Z

    return-void
.end method

.method public static c()I
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    sget-wide v2, LaI/b;->o:J

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    invoke-static {}, LaI/b;->d()LaI/b;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, LaI/b;->i()I

    move-result v2

    sput v2, LaI/b;->p:I

    :cond_0
    sput-wide v0, LaI/b;->o:J

    :cond_1
    sget v0, LaI/b;->p:I

    return v0
.end method

.method public static d()LaI/b;
    .locals 1

    sget-object v0, LaI/b;->a:LaI/c;

    if-nez v0, :cond_0

    invoke-static {}, LaI/b;->e()LaI/b;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LaI/b;->a:LaI/c;

    invoke-interface {v0}, LaI/c;->a()LaI/b;

    move-result-object v0

    goto :goto_0
.end method

.method public static e()LaI/b;
    .locals 13

    const/4 v2, -0x1

    sget-object v0, LaI/b;->n:LaI/b;

    if-nez v0, :cond_0

    new-instance v0, LaI/b;

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v11

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    invoke-direct/range {v0 .. v12}, LaI/b;-><init>(Ljava/lang/String;IIIIIIIIIJ)V

    sput-object v0, LaI/b;->n:LaI/b;

    :cond_0
    sget-object v0, LaI/b;->n:LaI/b;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, LaI/b;

    iget v2, p0, LaI/b;->j:I

    iget v3, p1, LaI/b;->j:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LaI/b;->i:I

    iget v3, p1, LaI/b;->i:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    iget v0, p0, LaI/b;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    iget v0, p0, LaI/b;->j:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, LaI/b;->i:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, LaI/b;->j:I

    mul-int/lit8 v0, v0, 0x1d

    iget v1, p0, LaI/b;->i:I

    add-int/2addr v0, v1

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, LaI/b;->f:I

    return v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, LaI/b;->g:I

    return v0
.end method

.method public k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lby/Q;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x4

    iget v2, p0, LaI/b;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    iget v2, p0, LaI/b;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    iget v2, p0, LaI/b;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget-object v2, p0, LaI/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v3, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lby/i;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-wide v1, p0, LaI/b;->m:J

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lby/i;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v2, p0, LaI/b;->j:I

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v2, p0, LaI/b;->i:I

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    iget v3, p0, LaI/b;->g:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    iget v3, p0, LaI/b;->f:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    iget v3, p0, LaI/b;->k:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method
