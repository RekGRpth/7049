.class public LaK/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(JLbw/b;Landroid/os/Bundle;Ljava/lang/String;)Landroid/location/Location;
    .locals 5

    const-wide v3, 0x416312d000000000L

    if-nez p2, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget v1, p2, Lbw/b;->d:I

    int-to-float v1, v1

    const/high16 v2, 0x447a0000

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    iget v1, p2, Lbw/b;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setBearing(F)V

    :cond_2
    iget v1, p2, Lbw/b;->b:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    iget v1, p2, Lbw/b;->c:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    const-wide/32 v1, 0xf4240

    div-long v1, p0, v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static a(Lo/D;)Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "networkLocationSource"

    const-string v2, "cached"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "networkLocationType"

    const-string v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    const-string v1, "levelId"

    invoke-virtual {p0}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v2}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "levelNumberE3"

    invoke-virtual {p0}, Lo/D;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/location/Location;Ljava/lang/String;F)Lbw/b;
    .locals 8

    const-wide v4, 0x416312d000000000L

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    mul-double/2addr v0, v4

    double-to-int v1, v0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v3, 0x447a0000

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-int v6, v0

    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    cmpl-float v0, v0, p2

    if-nez v0, :cond_2

    :cond_1
    const/4 v6, -0x1

    :cond_2
    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v7

    :cond_3
    new-instance v0, Lbw/b;

    const-string v4, ""

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lbw/b;-><init>(IIILjava/lang/String;Ljava/lang/String;IF)V

    goto :goto_0
.end method

.method public static a(Landroid/location/Location;Lo/D;F)Lbw/b;
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {p0, v0, p2}, LaK/a;->a(Landroid/location/Location;Ljava/lang/String;F)Lbw/b;

    move-result-object v0

    return-object v0
.end method
