.class public LaM/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:LaM/j;


# instance fields
.field private a:Landroid/accounts/AccountManager;

.field private b:[Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaM/j;

    invoke-direct {v0}, LaM/j;-><init>()V

    sput-object v0, LaM/j;->c:LaM/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LaM/j;
    .locals 1

    sget-object v0, LaM/j;->c:LaM/j;

    return-object v0
.end method

.method public static a([Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 1

    array-length v0, p0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, LaM/j;->c([Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p0, v0

    goto :goto_0
.end method

.method static synthetic a(LaM/j;)Landroid/accounts/AccountManager;
    .locals 1

    iget-object v0, p0, LaM/j;->a:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method public static a(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V
    .locals 8

    const/4 v3, 0x0

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "introMessage"

    const/16 v1, 0x24

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string v0, "allowSkip"

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "com.google"

    const-string v2, "local"

    new-instance v6, LaM/l;

    invoke-direct {v6, p0, p2, p3, p4}, LaM/l;-><init>(Landroid/accounts/AccountManager;ZZLandroid/app/Activity;)V

    move-object v0, p0

    move-object v5, p4

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v7

    new-instance v0, LaM/m;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LaM/m;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V

    invoke-direct {v6, v7, v0}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Las/b;->g()V

    goto :goto_0
.end method

.method public static a(Z)V
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    check-cast v0, LaM/a;

    invoke-virtual {v0, p0}, LaM/a;->a(Z)V

    return-void
.end method

.method static synthetic a(LaM/j;[Landroid/accounts/Account;)[Landroid/accounts/Account;
    .locals 0

    iput-object p1, p0, LaM/j;->b:[Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic b([Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 1

    invoke-static {p0}, LaM/j;->c([Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V
    .locals 4

    :try_start_0
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    check-cast v0, LaM/a;

    const-string v1, "local"

    invoke-virtual {p0, p1, v1, p2}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SID"

    invoke-virtual {p0, p1, v2, p2}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    const-string v3, "LSID"

    invoke-virtual {p0, p1, v3, p2}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1, v2, v3, p3}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, LaM/a;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-static {p0, v0, p2, p3, p4}, LaM/j;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {p3}, LaM/j;->a(Z)V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {p3}, LaM/j;->a(Z)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {p3}, LaM/j;->a(Z)V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-static {p3}, LaM/j;->a(Z)V

    goto :goto_0
.end method

.method static synthetic b(LaM/j;)[Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, LaM/j;->b:[Landroid/accounts/Account;

    return-object v0
.end method

.method private static c([Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 2

    invoke-static {p0}, LaM/j;->d([Landroid/accounts/Account;)I

    move-result v0

    if-ltz v0, :cond_0

    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-object v0, p0, v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d([Landroid/accounts/Account;)I
    .locals 3

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    aget-object v2, p0, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->p()V

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public a(I)Landroid/accounts/Account;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, LaM/j;->b:[Landroid/accounts/Account;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LaM/j;->b:[Landroid/accounts/Account;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, LaM/j;->a:Landroid/accounts/AccountManager;

    iget-object v0, p0, LaM/j;->b:[Landroid/accounts/Account;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Landroid/accounts/Account;

    iput-object v0, p0, LaM/j;->b:[Landroid/accounts/Account;

    :cond_0
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, LaM/k;

    invoke-direct {v2, p0}, LaM/k;-><init>(LaM/j;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    return-void
.end method

.method public b()Z
    .locals 1

    invoke-virtual {p0}, LaM/j;->e()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, LaM/j;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaM/j;->b:[Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaM/j;->b:[Landroid/accounts/Account;

    array-length v1, v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()[Ljava/lang/String;
    .locals 3

    iget-object v0, p0, LaM/j;->b:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LaM/j;->b:[Landroid/accounts/Account;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, LaM/j;->b:[Landroid/accounts/Account;

    aget-object v2, v2, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public e()Z
    .locals 1

    sget-boolean v0, LaM/n;->a:Z

    return v0
.end method
