.class public LaN/H;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaN/B;

.field private final b:LaN/Y;

.field private final c:I

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(LaN/B;LaN/Y;I)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-void
.end method

.method public constructor <init>(LaN/B;LaN/Y;IZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaN/H;->a:LaN/B;

    iput-object p2, p0, LaN/H;->b:LaN/Y;

    iput p3, p0, LaN/H;->c:I

    iput-boolean p4, p0, LaN/H;->d:Z

    iput-boolean p5, p0, LaN/H;->e:Z

    return-void
.end method

.method public static i()LaN/H;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, LaN/H;

    new-instance v1, LaN/B;

    invoke-direct {v1, v3, v3}, LaN/B;-><init>(II)V

    const/4 v2, 0x1

    invoke-static {v2}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    return-object v0
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, LaN/H;->a:LaN/B;

    return-object v0
.end method

.method public a(I)LaN/H;
    .locals 6

    new-instance v0, LaN/H;

    iget-object v1, p0, LaN/H;->a:LaN/B;

    iget-object v2, p0, LaN/H;->b:LaN/Y;

    iget-boolean v4, p0, LaN/H;->d:Z

    iget-boolean v5, p0, LaN/H;->e:Z

    move v3, p1

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public a(LaN/B;)LaN/H;
    .locals 6

    new-instance v0, LaN/H;

    iget-object v2, p0, LaN/H;->b:LaN/Y;

    iget v3, p0, LaN/H;->c:I

    iget-boolean v4, p0, LaN/H;->d:Z

    iget-boolean v5, p0, LaN/H;->e:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public a(LaN/Y;)LaN/H;
    .locals 6

    new-instance v0, LaN/H;

    iget-object v1, p0, LaN/H;->a:LaN/B;

    iget v3, p0, LaN/H;->c:I

    iget-boolean v4, p0, LaN/H;->d:Z

    iget-boolean v5, p0, LaN/H;->e:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public a(Z)LaN/H;
    .locals 6

    new-instance v0, LaN/H;

    iget-object v1, p0, LaN/H;->a:LaN/B;

    iget-object v2, p0, LaN/H;->b:LaN/Y;

    iget v3, p0, LaN/H;->c:I

    iget-boolean v5, p0, LaN/H;->e:Z

    move v4, p1

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public b(Z)LaN/H;
    .locals 6

    new-instance v0, LaN/H;

    iget-object v1, p0, LaN/H;->a:LaN/B;

    iget-object v2, p0, LaN/H;->b:LaN/Y;

    iget v3, p0, LaN/H;->c:I

    iget-boolean v4, p0, LaN/H;->d:Z

    move v5, p1

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public b()LaN/Y;
    .locals 1

    iget-object v0, p0, LaN/H;->b:LaN/Y;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LaN/H;->c:I

    return v0
.end method

.method public d()Z
    .locals 1

    iget v0, p0, LaN/H;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, LaN/H;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    iget v0, p0, LaN/H;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, LaN/H;->d:Z

    return v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, LaN/H;->e:Z

    return v0
.end method
