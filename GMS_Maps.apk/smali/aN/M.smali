.class public LaN/M;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/g;
.implements Lcom/google/googlenav/ui/aH;


# static fields
.field private static final d:Ljava/lang/Object;

.field private static e:I


# instance fields
.field private final a:[LaN/B;

.field private final b:I

.field private final c:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LaN/M;->d:Ljava/lang/Object;

    const/4 v0, 0x1

    sput v0, LaN/M;->e:I

    return-void
.end method

.method public constructor <init>([LaN/B;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, LaN/M;->p()I

    move-result v0

    iput v0, p0, LaN/M;->f:I

    iput-object p1, p0, LaN/M;->a:[LaN/B;

    iput p2, p0, LaN/M;->b:I

    iput p3, p0, LaN/M;->c:I

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/M;
    .locals 4

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v2

    invoke-static {v2}, LaN/M;->a([B)[LaN/B;

    move-result-object v2

    new-instance v3, LaN/M;

    invoke-direct {v3, v2, v0, v1}, LaN/M;-><init>([LaN/B;II)V

    return-object v3
.end method

.method public static a([JLaN/B;LaN/Y;)Z
    .locals 13

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1, p2}, LaN/B;->a(LaN/Y;)I

    move-result v4

    invoke-virtual {p1, p2}, LaN/B;->b(LaN/Y;)I

    move-result v5

    move v0, v1

    move v2, v3

    :goto_1
    array-length v6, p0

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_0

    aget-wide v6, p0, v2

    invoke-static {v6, v7}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v6

    aget-wide v7, p0, v2

    invoke-static {v7, v8}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v7

    aget-wide v8, p0, v0

    invoke-static {v8, v9}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v8

    aget-wide v9, p0, v0

    invoke-static {v9, v10}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v9

    if-gt v7, v9, :cond_3

    add-int/lit8 v10, v7, -0x4

    if-gt v10, v5, :cond_2

    add-int/lit8 v10, v9, 0x4

    if-le v5, v10, :cond_4

    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v10, v9, -0x4

    if-gt v10, v5, :cond_2

    add-int/lit8 v10, v7, 0x4

    if-gt v5, v10, :cond_2

    :cond_4
    if-gt v6, v8, :cond_6

    add-int/lit8 v10, v6, -0x4

    if-gt v10, v4, :cond_2

    add-int/lit8 v10, v8, 0x4

    if-gt v4, v10, :cond_2

    :cond_5
    sub-int/2addr v8, v6

    sub-int/2addr v9, v7

    sub-int v6, v4, v6

    sub-int v7, v5, v7

    const/16 v10, 0x50

    mul-int v11, v8, v8

    mul-int v12, v9, v9

    add-int/2addr v11, v12

    invoke-static {v11}, Lcom/google/googlenav/common/util/j;->b(I)I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    mul-int/2addr v6, v9

    mul-int/2addr v7, v8

    sub-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    div-int/2addr v6, v10

    const/4 v7, 0x4

    if-gt v6, v7, :cond_2

    move v3, v1

    goto :goto_0

    :cond_6
    add-int/lit8 v10, v8, -0x4

    if-gt v10, v4, :cond_2

    add-int/lit8 v10, v6, 0x4

    if-le v4, v10, :cond_5

    goto :goto_2
.end method

.method public static a([LaN/B;)[B
    .locals 4

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x8

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v0, 0x0

    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_0

    aget-object v3, p0, v0

    invoke-virtual {v3}, LaN/B;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    aget-object v3, p0, v0

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a([B)[LaN/B;
    .locals 7

    array-length v0, p0

    div-int/lit8 v1, v0, 0x8

    new-array v2, v1, [LaN/B;

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    new-instance v6, LaN/B;

    invoke-direct {v6, v4, v5}, LaN/B;-><init>(II)V

    aput-object v6, v2, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    return-object v2
.end method

.method private static p()I
    .locals 3

    sget-object v1, LaN/M;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget v0, LaN/M;->e:I

    add-int/lit8 v2, v0, 0x1

    sput v2, LaN/M;->e:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public a(LaN/Y;)I
    .locals 3

    const/4 v0, 0x2

    invoke-virtual {p1}, LaN/Y;->a()I

    move-result v1

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0xa

    iget v2, p0, LaN/M;->c:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public b()LaN/B;
    .locals 2

    iget-object v0, p0, LaN/M;->a:[LaN/B;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public c()Lo/D;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x5

    iget v2, p0, LaN/M;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    iget v2, p0, LaN/M;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    iget-object v2, p0, LaN/M;->a:[LaN/B;

    invoke-static {v2}, LaN/M;->a([LaN/B;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, LaN/M;->c:I

    return v0
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()Z
    .locals 1

    invoke-virtual {p0}, LaN/M;->g()Z

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, LaN/M;->f:I

    return v0
.end method

.method public j()[LaN/B;
    .locals 1

    iget-object v0, p0, LaN/M;->a:[LaN/B;

    return-object v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, LaN/M;->b:I

    return v0
.end method

.method public l()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public m()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    const-string v0, "polyline"

    return-object v0
.end method

.method public o()[[LaN/B;
    .locals 1

    const/4 v0, 0x0

    check-cast v0, [[LaN/B;

    return-object v0
.end method
