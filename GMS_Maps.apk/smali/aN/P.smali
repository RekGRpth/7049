.class public LaN/P;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static f:I

.field private static g:[LaN/P;

.field private static final h:[I


# instance fields
.field private final a:I

.field private final b:I

.field private final c:LaN/Y;

.field private final d:B

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LaN/P;->h:[I

    const/4 v0, 0x1

    invoke-static {v0}, LaN/P;->a(I)V

    return-void

    :array_0
    .array-data 4
        0x83
        0x101
        0x209
        0x407
        0x805
        0x1003
        0x2011
        0x401b
    .end array-data
.end method

.method private constructor <init>(BIILaN/Y;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zoom cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-byte p1, p0, LaN/P;->d:B

    iput p2, p0, LaN/P;->a:I

    iput p3, p0, LaN/P;->b:I

    iput-object p4, p0, LaN/P;->c:LaN/Y;

    iput p5, p0, LaN/P;->e:I

    return-void
.end method

.method public static a()B
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method private static a(IILaN/Y;I)I
    .locals 2

    mul-int/lit8 v0, p0, 0x1d

    xor-int/2addr v0, p1

    mul-int/lit8 v0, v0, 0x1d

    invoke-virtual {p2}, LaN/Y;->a()I

    move-result v1

    add-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x8

    add-int/2addr v0, p3

    return v0
.end method

.method public static a(LaN/B;LaN/Y;)I
    .locals 1

    invoke-virtual {p0, p1}, LaN/B;->a(LaN/Y;)I

    move-result v0

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method public static a(BIILaN/Y;)LaN/P;
    .locals 7

    invoke-virtual {p3}, LaN/Y;->b()I

    move-result v0

    div-int/lit16 v0, v0, 0x100

    rem-int v2, p1, v0

    if-gez v2, :cond_0

    invoke-virtual {p3}, LaN/Y;->b()I

    move-result v0

    div-int/lit16 v0, v0, 0x100

    add-int/2addr v2, v0

    :cond_0
    invoke-static {v2, p2, p3, p0}, LaN/P;->a(IILaN/Y;I)I

    move-result v5

    sget v0, LaN/P;->f:I

    rem-int v0, v5, v0

    if-gez v0, :cond_2

    sget v1, LaN/P;->f:I

    add-int/2addr v0, v1

    move v6, v0

    :goto_0
    sget-object v0, LaN/P;->g:[LaN/P;

    aget-object v0, v0, v6

    if-eqz v0, :cond_1

    iget-byte v1, v0, LaN/P;->d:B

    if-ne v1, p0, :cond_1

    iget v1, v0, LaN/P;->a:I

    if-ne v1, v2, :cond_1

    iget v1, v0, LaN/P;->b:I

    if-ne v1, p2, :cond_1

    iget-object v1, v0, LaN/P;->c:LaN/Y;

    if-ne v1, p3, :cond_1

    :goto_1
    return-object v0

    :cond_1
    new-instance v0, LaN/P;

    move v1, p0

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LaN/P;-><init>(BIILaN/Y;I)V

    sget-object v1, LaN/P;->g:[LaN/P;

    aput-object v0, v1, v6

    goto :goto_1

    :cond_2
    move v6, v0

    goto :goto_0
.end method

.method public static a(BLaN/B;LaN/Y;)LaN/P;
    .locals 2

    invoke-static {p1, p2}, LaN/P;->a(LaN/B;LaN/Y;)I

    move-result v0

    invoke-static {p1, p2}, LaN/P;->b(LaN/B;LaN/Y;)I

    move-result v1

    invoke-static {p0, v0, v1, p2}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v0

    return-object v0
.end method

.method public static a(BLaN/P;)LaN/P;
    .locals 3

    iget v0, p1, LaN/P;->a:I

    iget v1, p1, LaN/P;->b:I

    iget-object v2, p1, LaN/P;->c:LaN/Y;

    invoke-static {p0, v0, v1, v2}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)LaN/P;
    .locals 4

    :try_start_0
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    invoke-static {v3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LaN/P;->a(BIILaN/Y;)LaN/P;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(I)V
    .locals 1

    mul-int/lit8 v0, p0, 0x6

    invoke-static {v0}, LaN/P;->b(I)I

    move-result v0

    sput v0, LaN/P;->f:I

    sget v0, LaN/P;->f:I

    new-array v0, v0, [LaN/P;

    sput-object v0, LaN/P;->g:[LaN/P;

    return-void
.end method

.method private static b(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget-object v1, LaN/P;->h:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, LaN/P;->h:[I

    aget v1, v1, v0

    if-lt v1, p0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, LaN/P;->h:[I

    sget-object v1, LaN/P;->h:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_1
.end method

.method public static b(LaN/B;LaN/Y;)I
    .locals 1

    invoke-virtual {p0, p1}, LaN/B;->b(LaN/Y;)I

    move-result v0

    div-int/lit16 v0, v0, 0x100

    return v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-byte v0, p0, LaN/P;->d:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    iget v0, p0, LaN/P;->a:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget v0, p0, LaN/P;->b:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p0, LaN/P;->c:LaN/Y;

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    return-void
.end method

.method public b()B
    .locals 1

    iget-byte v0, p0, LaN/P;->d:B

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LaN/P;->a:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, LaN/P;->b:I

    return v0
.end method

.method public e()LaN/Y;
    .locals 1

    iget-object v0, p0, LaN/P;->c:LaN/Y;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LaN/P;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, LaN/P;

    iget v2, p0, LaN/P;->a:I

    iget v3, p1, LaN/P;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, LaN/P;->b:I

    iget v3, p1, LaN/P;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LaN/P;->c:LaN/Y;

    iget-object v3, p1, LaN/P;->c:LaN/Y;

    if-ne v2, v3, :cond_3

    iget-byte v2, p0, LaN/P;->d:B

    iget-byte v3, p1, LaN/P;->d:B

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, LaN/P;->a:I

    mul-int/lit16 v0, v0, 0x100

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, LaN/P;->b:I

    mul-int/lit16 v0, v0, 0x100

    return v0
.end method

.method public h()LaN/P;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, LaN/P;->c:LaN/Y;

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    if-eqz v2, :cond_2

    iget v1, p0, LaN/P;->a:I

    iget v0, p0, LaN/P;->b:I

    if-gez v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    if-gez v0, :cond_1

    add-int/lit8 v0, v0, -0x1

    :cond_1
    iget-byte v3, p0, LaN/P;->d:B

    div-int/lit8 v1, v1, 0x2

    div-int/lit8 v0, v0, 0x2

    invoke-static {v3, v1, v0, v2}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, LaN/P;->e:I

    return v0
.end method

.method public i()LaN/P;
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0, p0}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 2

    iget v0, p0, LaN/P;->b:I

    if-ltz v0, :cond_0

    iget v0, p0, LaN/P;->b:I

    iget-object v1, p0, LaN/P;->c:LaN/Y;

    invoke-virtual {v1}, LaN/Y;->b()I

    move-result v1

    div-int/lit16 v1, v1, 0x100

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaN/P;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaN/P;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaN/P;->c:LaN/Y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
