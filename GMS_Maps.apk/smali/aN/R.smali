.class public LaN/R;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LaN/U;I)I
    .locals 1

    invoke-virtual {p0, p1}, LaN/U;->c(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x60d1d1d1

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, -0x640000

    goto :goto_0

    :pswitch_1
    const/high16 v0, -0x670000

    goto :goto_0

    :pswitch_2
    const/16 v0, -0x2000

    goto :goto_0

    :pswitch_3
    const v0, -0xcc4f00

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a([LaN/U;LaN/T;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    invoke-interface {p2}, LaN/T;->a()LaN/S;

    move-result-object v4

    aget-object v5, p1, v0

    invoke-virtual {p0, v4, v5, v1, v1}, LaN/R;->a(LaN/S;LaN/U;IZ)V

    invoke-virtual {v5}, LaN/U;->a()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_0

    invoke-virtual {p0, v4, v5, v2, v3}, LaN/R;->a(LaN/S;LaN/U;IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    const v2, -0x2f000001

    invoke-virtual {v5}, LaN/U;->c()I

    move-result v5

    shl-int/lit8 v5, v5, 0x8

    invoke-interface {p2, v4, v2, v5}, LaN/T;->a(LaN/S;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b([LaN/U;LaN/T;)V
    .locals 10

    const/4 v6, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-object v7, p1, v0

    invoke-virtual {p0, p2, v7, v1}, LaN/R;->a(LaN/T;LaN/U;I)LaN/S;

    move-result-object v4

    invoke-static {v7, v1}, LaN/R;->a(LaN/U;I)I

    move-result v3

    invoke-virtual {v7}, LaN/U;->a()I

    move-result v8

    move v5, v6

    :goto_1
    if-ge v5, v8, :cond_0

    invoke-virtual {p0, v4, v7, v5, v6}, LaN/R;->a(LaN/S;LaN/U;IZ)V

    invoke-static {v7, v5}, LaN/R;->a(LaN/U;I)I

    move-result v2

    if-eq v2, v3, :cond_2

    invoke-virtual {v7}, LaN/U;->b()I

    move-result v9

    shl-int/lit8 v9, v9, 0x8

    invoke-interface {p2, v4, v3, v9}, LaN/T;->a(LaN/S;II)V

    invoke-virtual {p0, p2, v7, v5}, LaN/R;->a(LaN/T;LaN/U;I)LaN/S;

    move-result-object v3

    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v3

    move v3, v2

    goto :goto_1

    :cond_0
    invoke-virtual {v7}, LaN/U;->b()I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    invoke-interface {p2, v4, v3, v2}, LaN/T;->a(LaN/S;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    move v2, v3

    move-object v3, v4

    goto :goto_2
.end method


# virtual methods
.method protected a(LaN/T;LaN/U;I)LaN/S;
    .locals 2

    invoke-interface {p1}, LaN/T;->a()LaN/S;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, p3, v1}, LaN/R;->a(LaN/S;LaN/U;IZ)V

    return-object v0
.end method

.method protected a(LaN/S;LaN/U;IZ)V
    .locals 2

    invoke-virtual {p2, p3}, LaN/U;->a(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p2, p3}, LaN/U;->b(I)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    if-eqz p4, :cond_0

    invoke-interface {p1, v0, v1}, LaN/S;->a(II)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1, v0, v1}, LaN/S;->b(II)V

    goto :goto_0
.end method

.method public a(LaN/X;LaN/T;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LaN/X;->a(J)V

    invoke-virtual {p1}, LaN/X;->c()[LaN/U;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LaN/R;->a([LaN/U;LaN/T;)V

    invoke-virtual {p1}, LaN/X;->c()[LaN/U;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LaN/R;->b([LaN/U;LaN/T;)V

    return-void
.end method
