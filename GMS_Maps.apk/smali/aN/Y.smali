.class public final LaN/Y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:I

.field private static d:I

.field private static final e:[LaN/Y;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    const/16 v5, 0x16

    sput v0, LaN/Y;->c:I

    sput v5, LaN/Y;->d:I

    new-array v1, v5, [LaN/Y;

    sput-object v1, LaN/Y;->e:[LaN/Y;

    const/16 v1, 0x100

    :goto_0
    if-gt v0, v5, :cond_0

    sget-object v2, LaN/Y;->e:[LaN/Y;

    add-int/lit8 v3, v0, -0x1

    new-instance v4, LaN/Y;

    invoke-direct {v4, v0, v1}, LaN/Y;-><init>(II)V

    aput-object v4, v2, v3

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LaN/Y;->b:I

    iput p2, p0, LaN/Y;->a:I

    return-void
.end method

.method public static b(I)LaN/Y;
    .locals 2

    const/4 v0, 0x0

    sget v1, LaN/Y;->c:I

    if-lt p0, v1, :cond_1

    const/16 v1, 0x16

    if-gt p0, v1, :cond_1

    sget v0, LaN/Y;->d:I

    if-le p0, v0, :cond_0

    sget p0, LaN/Y;->d:I

    :cond_0
    sget-object v0, LaN/Y;->e:[LaN/Y;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    :cond_1
    return-object v0
.end method

.method public static b(II)V
    .locals 3

    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x16

    sput v1, LaN/Y;->c:I

    :goto_0
    sget v1, LaN/Y;->c:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    sget v1, LaN/Y;->c:I

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    iget v1, v1, LaN/Y;->a:I

    mul-int/lit8 v2, v0, 0xa

    div-int/lit8 v2, v2, 0x9

    if-gt v1, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    sget v1, LaN/Y;->c:I

    add-int/lit8 v1, v1, -0x1

    sput v1, LaN/Y;->c:I

    goto :goto_0
.end method

.method public static c(LaN/Y;)LaN/Y;
    .locals 1

    invoke-virtual {p0}, LaN/Y;->a()I

    move-result v0

    invoke-static {v0}, LaN/Y;->d(I)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public static c(I)V
    .locals 2

    const/16 v0, 0x16

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    sput v0, LaN/Y;->d:I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    const/16 v0, 0x10

    :cond_1
    sput v0, LaN/Y;->d:I

    goto :goto_0
.end method

.method public static d(I)I
    .locals 1

    sget v0, LaN/Y;->c:I

    if-ge p0, v0, :cond_0

    sget p0, LaN/Y;->c:I

    :cond_0
    return p0
.end method

.method public static e()I
    .locals 1

    sget v0, LaN/Y;->c:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LaN/Y;->b:I

    return v0
.end method

.method public a(I)I
    .locals 4

    int-to-long v0, p1

    iget v2, p0, LaN/Y;->a:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x26382e0

    div-long/2addr v0, v2

    long-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public a(II)I
    .locals 1

    iget v0, p0, LaN/Y;->b:I

    if-ge v0, p2, :cond_0

    iget v0, p0, LaN/Y;->b:I

    sub-int v0, p2, v0

    shl-int v0, p1, v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LaN/Y;->b:I

    sub-int/2addr v0, p2

    shr-int v0, p1, v0

    goto :goto_0
.end method

.method public a(LaN/Y;)I
    .locals 2

    iget v0, p1, LaN/Y;->a:I

    iget v1, p0, LaN/Y;->a:I

    div-int/2addr v0, v1

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LaN/Y;->a:I

    return v0
.end method

.method public b(LaN/Y;)Z
    .locals 2

    iget v0, p0, LaN/Y;->b:I

    iget v1, p1, LaN/Y;->b:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LaN/Y;
    .locals 1

    iget v0, p0, LaN/Y;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public d()LaN/Y;
    .locals 1

    iget v0, p0, LaN/Y;->b:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
