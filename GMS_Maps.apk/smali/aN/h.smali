.class public LaN/h;
.super LaN/u;
.source "SourceFile"


# instance fields
.field private final c:Z

.field private final d:LaN/p;

.field private final e:Lcom/google/googlenav/ui/bF;

.field private f:LaN/s;


# direct methods
.method public constructor <init>(LaN/p;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;Z)V
    .locals 2

    invoke-direct {p0}, LaN/u;-><init>()V

    iput-object p1, p0, LaN/h;->d:LaN/p;

    iput-object p2, p0, LaN/h;->e:Lcom/google/googlenav/ui/bF;

    iput-boolean p4, p0, LaN/h;->c:Z

    if-eqz p2, :cond_0

    new-instance v0, LaN/i;

    invoke-direct {v0, p0}, LaN/i;-><init>(LaN/h;)V

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/bF;->a(Lcom/google/googlenav/ui/bG;)V

    :cond_0
    if-eqz p3, :cond_1

    new-instance v0, LaN/j;

    invoke-direct {v0, p0}, LaN/j;-><init>(LaN/h;)V

    invoke-virtual {p3, v0}, Lcom/google/googlenav/ui/p;->a(Lcom/google/googlenav/ui/q;)V

    :cond_1
    invoke-virtual {p1}, LaN/p;->t()I

    move-result v0

    invoke-virtual {p1}, LaN/p;->s()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LaN/h;->c(II)V

    return-void
.end method

.method private g(LaN/B;)Z
    .locals 3

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->c(LaN/B;)Landroid/graphics/Point;

    move-result-object v0

    iget-boolean v1, p0, LaN/h;->c:Z

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/graphics/Point;->x:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget-object v2, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v2}, LaN/p;->t()I

    move-result v2

    mul-int/lit8 v2, v2, 0x7

    if-ge v1, v2, :cond_0

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v1}, LaN/p;->s()I

    move-result v1

    mul-int/lit8 v1, v1, 0x7

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 1

    iget-object v0, p0, LaN/h;->e:Lcom/google/googlenav/ui/bF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/h;->e:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILaN/B;)F
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->c(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public a()I
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->m()I

    move-result v0

    return v0
.end method

.method public a(LaN/B;)I
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->e(LaN/B;)I

    move-result v0

    return v0
.end method

.method public a(LaN/H;)I
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->b(LaN/H;)I

    move-result v0

    return v0
.end method

.method protected a(LaN/B;LaN/Y;II)LaN/B;
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1, p2, p3, p4}, LaN/p;->a(LaN/B;LaN/Y;II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    invoke-direct {p0}, LaN/h;->r()V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->b(I)V

    return-void
.end method

.method public a(II)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, LaN/h;->h()V

    invoke-direct {p0}, LaN/h;->r()V

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, v1}, LaN/h;->a(ZZZ)V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->d()LaN/B;

    move-result-object v0

    iget-object v1, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v1}, LaN/p;->c()LaN/Y;

    move-result-object v1

    invoke-static {v0, p1, p2, v1}, LaN/s;->a(LaN/B;IILaN/Y;)LaN/s;

    move-result-object v0

    iput-object v0, p0, LaN/h;->f:LaN/s;

    return-void
.end method

.method public a(LaN/B;LaN/Y;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, LaN/h;->d(LaN/B;LaN/Y;)V

    return-void
.end method

.method protected a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1, p2, p3, p4}, LaN/p;->a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V

    return-void
.end method

.method protected a(LaN/B;Landroid/graphics/Point;)V
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1, p2}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    return-void
.end method

.method protected a(LaN/Y;II)V
    .locals 3

    iget-object v0, p0, LaN/h;->e:Lcom/google/googlenav/ui/bF;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    invoke-virtual {p0, p2, p3}, LaN/h;->b(II)LaN/B;

    move-result-object v0

    neg-int v1, p2

    neg-int v2, p3

    invoke-virtual {v0, v1, v2, p1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, LaN/h;->e(LaN/B;LaN/Y;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaN/h;->e:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/bF;->a(LaN/Y;II)V

    goto :goto_0
.end method

.method public a(Lo/D;)V
    .locals 0

    return-void
.end method

.method public a([LaN/B;IIILaN/Y;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p5}, LaN/h;->a(LaN/Y;)V

    invoke-direct {p0}, LaN/h;->r()V

    aget-object v0, p1, p3

    iget-object v2, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v2, v0}, LaN/p;->d(LaN/B;)V

    invoke-direct {p0, v0}, LaN/h;->g(LaN/B;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, LaN/h;->h()V

    invoke-virtual {p0}, LaN/h;->c()LaN/B;

    move-result-object v2

    invoke-virtual {v0, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0, v6, v1}, LaN/h;->a(ZZZ)V

    new-instance v0, LaN/s;

    int-to-long v1, p4

    sget-wide v3, LaN/s;->a:J

    mul-long v4, v1, v3

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, LaN/s;-><init>([LaN/B;IIJI)V

    iput-object v0, p0, LaN/h;->f:LaN/s;

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v0}, LaN/h;->c(LaN/B;)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->n()I

    move-result v0

    return v0
.end method

.method public b(LaN/H;)I
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->c(LaN/H;)I

    move-result v0

    return v0
.end method

.method public b(II)LaN/B;
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1, p2}, LaN/p;->b(II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method protected b(LaN/B;LaN/Y;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, LaN/h;->h()V

    invoke-direct {p0}, LaN/h;->r()V

    if-nez p2, :cond_0

    invoke-virtual {p0}, LaN/h;->d()LaN/Y;

    move-result-object p2

    :cond_0
    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p2}, LaN/p;->a(LaN/Y;)V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->c()LaN/Y;

    move-result-object v0

    invoke-virtual {p1, v2, v2, v0}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v4

    invoke-direct {p0, v4}, LaN/h;->g(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, LaN/h;->c()LaN/B;

    move-result-object v0

    invoke-virtual {v4, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0}, LaN/h;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {p0, v0, v3, v1}, LaN/h;->a(ZZZ)V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, v4}, LaN/p;->d(LaN/B;)V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->d()LaN/B;

    move-result-object v0

    iget-object v1, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v1}, LaN/p;->c()LaN/Y;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(I)I

    move-result v0

    new-instance v1, LaN/s;

    iget-object v3, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v3}, LaN/p;->d()LaN/B;

    move-result-object v3

    invoke-static {v0}, LaN/h;->b(I)I

    move-result v0

    invoke-direct {v1, v3, v4, v0, v2}, LaN/s;-><init>(LaN/B;LaN/B;II)V

    iput-object v1, p0, LaN/h;->f:LaN/s;

    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v4}, LaN/h;->c(LaN/B;)V

    goto :goto_2
.end method

.method public c()LaN/B;
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->d()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method protected c(LaN/B;LaN/Y;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, LaN/h;->h()V

    invoke-direct {p0}, LaN/h;->r()V

    invoke-virtual {p0}, LaN/h;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, LaN/h;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {p0, v0, v1, v2}, LaN/h;->a(ZZZ)V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->b(LaN/B;)V

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0, p2}, LaN/p;->a(LaN/Y;)V

    invoke-virtual {p0}, LaN/h;->m()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public d()LaN/Y;
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->c()LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public e()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()LaN/H;
    .locals 1

    iget-object v0, p0, LaN/h;->d:LaN/p;

    invoke-virtual {v0}, LaN/p;->b()LaN/H;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, LaN/h;->f:LaN/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/h;->f:LaN/s;

    invoke-virtual {v0}, LaN/s;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LaN/h;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaN/h;->d:LaN/p;

    iget-object v1, p0, LaN/h;->f:LaN/s;

    invoke-virtual {v1}, LaN/s;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/B;)V

    goto :goto_0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, LaN/h;->f:LaN/s;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, LaN/h;->f:LaN/s;

    invoke-virtual {p0}, LaN/h;->m()V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 0

    return-void
.end method

.method public j()Z
    .locals 2

    invoke-virtual {p0}, LaN/h;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->c()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    invoke-virtual {p0}, LaN/h;->c()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1}, LaN/h;->a(LaN/B;)I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    invoke-virtual {p0}, LaN/h;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    invoke-static {}, LaN/Y;->e()I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, LaN/h;->f:LaN/s;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
