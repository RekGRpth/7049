.class public LaN/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/util/n;


# instance fields
.field private final a:LaN/P;

.field private final b:Ljava/util/Hashtable;

.field private c:Lam/f;

.field private d:J

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(LaN/P;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaN/n;->e:J

    iput-object p1, p0, LaN/n;->a:LaN/P;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/n;->f:Z

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    invoke-virtual {p0}, LaN/n;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LaN/n;->e:J

    goto :goto_0
.end method

.method public a(Ljava/lang/String;J)Ljava/util/Hashtable;
    .locals 2

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/n;->d:J

    :goto_0
    iget-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    return-object v0

    :cond_0
    iput-wide p2, p0, LaN/n;->d:J

    goto :goto_0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, LaN/n;->d:J

    return-void
.end method

.method public declared-synchronized a([BJ)V
    .locals 3

    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/n;->d:J

    :goto_0
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LaN/n;->c:Lam/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iput-wide p2, p0, LaN/n;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, p1

    invoke-interface {v0, p1, v1, v2}, Lam/h;->a([BII)Lam/f;

    move-result-object v0

    iput-object v0, p0, LaN/n;->c:Lam/f;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a([Lcom/google/googlenav/layer/j;J)V
    .locals 1

    iget-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/n;->f:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, LaN/n;->b([Lcom/google/googlenav/layer/j;J)V

    goto :goto_0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, LaN/n;->d:J

    return-wide v0
.end method

.method public declared-synchronized b(J)Lam/f;
    .locals 2

    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/n;->d:J

    :goto_0
    iget-object v0, p0, LaN/n;->c:Lam/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iput-wide p1, p0, LaN/n;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b([Lcom/google/googlenav/layer/j;J)V
    .locals 9

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p2, v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/n;->d:J

    :goto_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_5

    aget-object v4, p1, v3

    invoke-virtual {v4}, Lcom/google/googlenav/layer/j;->g()[Lcom/google/googlenav/layer/o;

    move-result-object v5

    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_3
    if-ltz v2, :cond_4

    aget-object v6, v5, v2

    iget-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v6}, Lcom/google/googlenav/layer/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    const/4 v1, 0x0

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iget-object v7, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v6}, Lcom/google/googlenav/layer/o;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4
    if-nez v1, :cond_1

    new-instance v1, Lcom/google/googlenav/W;

    invoke-direct {v1, v6, v4}, Lcom/google/googlenav/W;-><init>(Lcom/google/googlenav/layer/o;Lcom/google/googlenav/layer/j;)V

    iget-object v7, p0, LaN/n;->a:LaN/P;

    invoke-virtual {v7}, LaN/P;->e()LaN/Y;

    move-result-object v7

    invoke-virtual {v7}, LaN/Y;->a()I

    move-result v7

    invoke-virtual {v1, v7}, Lcom/google/googlenav/W;->f(I)V

    invoke-virtual {v6}, Lcom/google/googlenav/layer/o;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_3

    :cond_2
    iput-wide p2, p0, LaN/n;->d:J

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Lcom/google/googlenav/layer/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/W;

    goto :goto_4

    :cond_4
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/n;->e:J

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/n;->f:Z

    goto/16 :goto_0
.end method

.method public c()LaN/P;
    .locals 1

    iget-object v0, p0, LaN/n;->a:LaN/P;

    return-object v0
.end method

.method public declared-synchronized d()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, LaN/n;->c:Lam/f;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/n;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, LaN/n;->f:Z

    return v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized g()Lcom/google/googlenav/common/util/l;
    .locals 5

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v0, p0, LaN/n;->c:Lam/f;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v3, "image"

    iget-object v4, p0, LaN/n;->c:Lam/f;

    invoke-interface {v4}, Lam/f;->g()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, LaN/n;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    iget v0, v0, Lcom/google/googlenav/common/util/l;->b:I

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v3, "cache"

    invoke-direct {v0, v3, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v3, "LayerTile"

    invoke-direct {v1, v3, v0, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, LaN/n;->c:Lam/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
