.class public LaN/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/C;


# instance fields
.field private final A:LaN/t;

.field private B:LaN/r;

.field private C:I

.field private D:Z

.field private E:LaN/w;

.field private F:Ljava/lang/String;

.field private G:Lo/C;

.field protected final a:LaN/D;

.field protected b:LaN/H;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:LaN/P;

.field private s:LaN/P;

.field private t:[LaN/P;

.field private u:LaN/Q;

.field private v:Z

.field private w:I

.field private x:Z

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>(IIILaN/B;LaN/Y;I)V
    .locals 7

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v6, p0, LaN/p;->c:I

    iput v6, p0, LaN/p;->d:I

    iput v6, p0, LaN/p;->e:I

    iput v6, p0, LaN/p;->f:I

    iput v6, p0, LaN/p;->g:I

    iput v6, p0, LaN/p;->h:I

    iput v6, p0, LaN/p;->i:I

    iput v6, p0, LaN/p;->j:I

    iput v6, p0, LaN/p;->k:I

    iput v6, p0, LaN/p;->l:I

    iput v6, p0, LaN/p;->m:I

    iput v6, p0, LaN/p;->n:I

    iput v6, p0, LaN/p;->o:I

    iput v6, p0, LaN/p;->w:I

    new-instance v0, LaN/t;

    invoke-direct {v0}, LaN/t;-><init>()V

    iput-object v0, p0, LaN/p;->A:LaN/t;

    const/4 v0, 0x0

    iput-object v0, p0, LaN/p;->B:LaN/r;

    iput v6, p0, LaN/p;->C:I

    iput-boolean v6, p0, LaN/p;->D:Z

    new-instance v0, LaN/D;

    const-string v5, "Tiles"

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p6

    invoke-direct/range {v0 .. v5}, LaN/D;-><init>(IIIILjava/lang/String;)V

    iput-object v0, p0, LaN/p;->a:LaN/D;

    new-instance v0, LaN/H;

    const/4 v1, 0x3

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-direct {v0, p4, v1, v6}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    iput-object v0, p0, LaN/p;->b:LaN/H;

    invoke-direct {p0, p4, p5}, LaN/p;->a(LaN/B;LaN/Y;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/p;->v:Z

    return-void
.end method

.method public static a(I)I
    .locals 1

    const/16 v0, 0x100

    invoke-static {p0, v0}, Lcom/google/googlenav/common/util/j;->a(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private declared-synchronized a(IILaN/H;)LaN/Y;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->e(LaN/B;)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p3, v0}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v1

    invoke-virtual {p0, v1}, LaN/p;->b(LaN/H;)I

    move-result v1

    if-lt v1, p1, :cond_0

    invoke-virtual {p3, v0}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v1

    invoke-virtual {p0, v1}, LaN/p;->c(LaN/H;)I

    move-result v1

    if-ge v1, p2, :cond_1

    :cond_0
    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    goto :goto_0

    :cond_1
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/util/HashSet;Ljava/util/HashSet;I)Ljava/lang/String;
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-string v2, ", "

    invoke-static {v1, v2}, Lau/b;->a(Ljava/util/Iterator;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const-string v3, ", "

    invoke-static {v2, v3}, Lau/b;->a(Ljava/util/Iterator;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v5

    aput-object v0, v3, v4

    aput-object v2, v3, v6

    aput-object v0, v3, v7

    const/4 v0, 0x4

    aput-object v1, v3, v0

    const/16 v0, 0x2aa

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v1, 0x2ab

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    new-array v2, v7, [Ljava/lang/String;

    aput-object v0, v2, v5

    aput-object v0, v2, v4

    aput-object v1, v2, v6

    const/16 v0, 0x2ad

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-array v1, v7, [Ljava/lang/String;

    aput-object v0, v1, v5

    aput-object v0, v1, v4

    aput-object v2, v1, v6

    const/16 v0, 0x2ac

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(LaN/B;LaN/Y;)V
    .locals 8

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "Map info"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_5

    :try_start_0
    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->a([B)Ljava/io/DataInput;

    move-result-object v4

    invoke-interface {v4}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v1

    if-ne v1, v6, :cond_4

    invoke-static {v4}, LaN/B;->a(Ljava/io/DataInput;)LaN/B;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :try_start_1
    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    :try_start_2
    invoke-interface {v4}, Ljava/io/DataInput;->readUnsignedByte()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    move-object v4, v3

    move v3, v1

    move v1, v2

    :goto_0
    move v7, v3

    move v3, v1

    move-object v1, v4

    :goto_1
    if-eqz v3, :cond_3

    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_0

    move v3, v2

    :goto_2
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    move v4, v2

    :goto_3
    new-instance v0, LaN/H;

    invoke-static {v1}, LaN/p;->f(LaN/B;)I

    move-result v2

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v2}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    invoke-virtual {p0, v0}, LaN/p;->a(LaN/H;)V

    :goto_4
    return-void

    :catch_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move v1, v5

    :goto_5
    const-string v4, "MAP"

    invoke-static {v4, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v4, "Map info"

    invoke-interface {v0, v4}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    move v0, v5

    move v7, v1

    move-object v1, v3

    move v3, v5

    goto :goto_1

    :cond_0
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_1

    move v3, v6

    goto :goto_2

    :cond_1
    move v3, v5

    goto :goto_2

    :cond_2
    move v4, v5

    goto :goto_3

    :cond_3
    new-instance v0, LaN/H;

    invoke-direct {v0, p1, p2, v5}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    invoke-virtual {p0, v0}, LaN/p;->a(LaN/H;)V

    goto :goto_4

    :catch_1
    move-exception v0

    move v1, v5

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_5

    :cond_4
    move v1, v5

    move v3, v5

    move-object v4, v0

    move v0, v5

    goto :goto_0

    :cond_5
    move v3, v5

    move v7, v5

    move-object v1, v0

    move v0, v5

    goto :goto_1
.end method

.method private a(LaN/B;LaN/Y;IILandroid/graphics/Point;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2}, LaN/B;->a(LaN/Y;)I

    move-result v0

    sub-int/2addr v0, p3

    iput v0, p5, Landroid/graphics/Point;->x:I

    invoke-virtual {p2}, LaN/Y;->b()I

    move-result v0

    iget v1, p5, Landroid/graphics/Point;->x:I

    neg-int v2, v0

    div-int/lit8 v2, v2, 0x2

    if-ge v1, v2, :cond_3

    iget v1, p5, Landroid/graphics/Point;->x:I

    add-int/2addr v0, v1

    iput v0, p5, Landroid/graphics/Point;->x:I

    :cond_2
    :goto_1
    invoke-virtual {p1, p2}, LaN/B;->b(LaN/Y;)I

    move-result v0

    sub-int/2addr v0, p4

    iput v0, p5, Landroid/graphics/Point;->y:I

    iget-object v0, p0, LaN/p;->B:LaN/r;

    if-eqz v0, :cond_0

    iget v0, p5, Landroid/graphics/Point;->x:I

    iget v1, p0, LaN/p;->j:I

    add-int/2addr v0, v1

    iput v0, p5, Landroid/graphics/Point;->x:I

    iget v0, p5, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/p;->k:I

    add-int/2addr v0, v1

    iput v0, p5, Landroid/graphics/Point;->y:I

    iget-object v0, p0, LaN/p;->B:LaN/r;

    invoke-interface {v0, p5}, LaN/r;->a(Landroid/graphics/Point;)V

    iget v0, p5, Landroid/graphics/Point;->x:I

    iget v1, p0, LaN/p;->j:I

    sub-int/2addr v0, v1

    iput v0, p5, Landroid/graphics/Point;->x:I

    iget v0, p5, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/p;->k:I

    sub-int/2addr v0, v1

    iput v0, p5, Landroid/graphics/Point;->y:I

    goto :goto_0

    :cond_3
    iget v1, p5, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v0, 0x2

    if-le v1, v2, :cond_2

    iget v1, p5, Landroid/graphics/Point;->x:I

    sub-int v0, v1, v0

    iput v0, p5, Landroid/graphics/Point;->x:I

    goto :goto_1
.end method

.method private a(LaN/I;)V
    .locals 4

    invoke-virtual {p1}, LaN/I;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, LaN/I;->h()J

    move-result-wide v0

    iget-wide v2, p0, LaN/p;->y:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LaN/p;->y:J

    :cond_0
    return-void
.end method

.method private a(ZZ)V
    .locals 4

    iget-object v0, p0, LaN/p;->r:LaN/P;

    iget-object v1, p0, LaN/p;->s:LaN/P;

    iget v2, p0, LaN/p;->l:I

    iget v3, p0, LaN/p;->m:I

    invoke-virtual {p0, v2, v3}, LaN/p;->a(II)LaN/P;

    move-result-object v2

    iput-object v2, p0, LaN/p;->r:LaN/P;

    iget v2, p0, LaN/p;->n:I

    iget v3, p0, LaN/p;->o:I

    invoke-virtual {p0, v2, v3}, LaN/p;->a(II)LaN/P;

    move-result-object v2

    iput-object v2, p0, LaN/p;->s:LaN/P;

    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, LaN/p;->r:LaN/P;

    invoke-virtual {v0, v2}, LaN/P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, LaN/p;->w()V

    :cond_1
    if-nez p2, :cond_2

    if-eqz v1, :cond_2

    iget-object v0, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v1, v0}, LaN/P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->l()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/p;->D:Z

    :cond_3
    return-void
.end method

.method private static a([Ljava/lang/String;Ljava/util/HashSet;)V
    .locals 2

    if-eqz p0, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget-object v1, p0, v0

    if-eqz v1, :cond_0

    aget-object v1, p0, v0

    invoke-virtual {p1, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(J)Z
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    sub-long/2addr v0, p0

    const-wide/16 v2, 0xc8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LaN/P;IILam/e;IIZZZJJ)Z
    .locals 13

    mul-int/lit16 v2, p2, 0x100

    add-int v10, p5, v2

    move/from16 v0, p3

    mul-int/lit16 v2, v0, 0x100

    add-int v11, p6, v2

    invoke-virtual {p1}, LaN/P;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p4, :cond_1

    const v2, 0xffffff

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lam/e;->a(I)V

    const/16 v2, 0x100

    const/16 v3, 0x100

    move-object/from16 v0, p4

    invoke-interface {v0, v10, v11, v2, v3}, Lam/e;->b(IIII)V

    const/4 v3, 0x1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    iget v2, p0, LaN/p;->f:I

    add-int/lit16 v3, v10, 0x80

    sub-int/2addr v2, v3

    iget v3, p0, LaN/p;->g:I

    add-int/lit16 v4, v11, 0x80

    sub-int/2addr v3, v4

    mul-int/2addr v2, v2

    mul-int/2addr v3, v3

    add-int v4, v2, v3

    invoke-virtual {p0, p1}, LaN/p;->a(LaN/P;)Z

    move-result v9

    if-eqz p7, :cond_6

    if-nez v9, :cond_6

    const/16 p7, 0x0

    move/from16 v5, p7

    :goto_1
    iget-object v2, p0, LaN/p;->a:LaN/D;

    if-eqz p9, :cond_4

    const/4 v6, 0x2

    :goto_2
    move-object v3, p1

    move-wide/from16 v7, p12

    invoke-virtual/range {v2 .. v8}, LaN/D;->a(LaN/P;IZIJ)LaN/I;

    move-result-object v6

    if-eqz v9, :cond_3

    iget-object v2, p0, LaN/p;->u:LaN/Q;

    if-eqz v2, :cond_3

    if-eqz p9, :cond_3

    iget-object v2, p0, LaN/p;->u:LaN/Q;

    invoke-interface {v2, v6, v5}, LaN/Q;->a(LaN/I;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v6}, LaN/I;->t()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, LaN/p;->C:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LaN/p;->C:I

    :cond_3
    const/4 v2, 0x0

    if-eqz p4, :cond_5

    move-object/from16 v7, p4

    move-wide/from16 v8, p12

    move/from16 v12, p8

    invoke-virtual/range {v6 .. v12}, LaN/I;->a(Lam/e;JIIZ)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-wide v3, p0, LaN/p;->z:J

    move-wide/from16 v0, p10

    invoke-virtual {v6, v0, v1, v3, v4}, LaN/I;->a(JJ)V

    invoke-virtual {v6}, LaN/I;->i()I

    move-result v3

    if-lez v3, :cond_5

    invoke-direct {p0, v6}, LaN/p;->a(LaN/I;)V

    invoke-direct {p0, v6}, LaN/p;->b(LaN/I;)V

    const/4 v2, 0x1

    move v3, v2

    :goto_3
    iget-object v2, p0, LaN/p;->a:LaN/D;

    move-wide/from16 v0, p12

    invoke-virtual {v2, p1, v5, v0, v1}, LaN/D;->a(LaN/P;ZJ)Ljava/util/Vector;

    move-result-object v5

    if-eqz p4, :cond_0

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_4
    if-ltz v4, :cond_0

    invoke-virtual {v5, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lam/f;

    move-object/from16 v0, p4

    invoke-interface {v0, v2, v10, v11}, Lam/e;->a(Lam/f;II)V

    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_4

    :cond_4
    const/4 v6, 0x1

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_3

    :cond_6
    move/from16 v5, p7

    goto :goto_1
.end method

.method private a(Lam/e;ZZ)Z
    .locals 22

    move-object/from16 v0, p0

    iget v1, v0, LaN/p;->f:I

    move-object/from16 v0, p0

    iget-object v2, v0, LaN/p;->r:LaN/P;

    invoke-virtual {v2}, LaN/P;->f()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, LaN/p;->b:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LaN/p;->b:LaN/H;

    invoke-virtual {v3}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/B;->a(LaN/Y;)I

    move-result v2

    sub-int v6, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, LaN/p;->g:I

    move-object/from16 v0, p0

    iget-object v2, v0, LaN/p;->r:LaN/P;

    invoke-virtual {v2}, LaN/P;->g()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, LaN/p;->b:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LaN/p;->b:LaN/H;

    invoke-virtual {v3}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/B;->b(LaN/Y;)I

    move-result v2

    sub-int v7, v1, v2

    :goto_0
    if-lez v6, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->b()I

    move-result v1

    sub-int/2addr v6, v1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->u:LaN/Q;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->u:LaN/Q;

    invoke-interface {v1}, LaN/Q;->a()V

    :cond_1
    const/4 v2, 0x0

    const/4 v15, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v11

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v17

    const/4 v1, 0x1

    if-nez p1, :cond_2

    const/4 v1, 0x0

    :cond_2
    const-wide v3, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v3, v0, LaN/p;->y:J

    move-object/from16 v0, p0

    iget v0, v0, LaN/p;->l:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LaN/p;->m:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LaN/p;->t:[LaN/P;

    move-object/from16 v21, v0

    mul-int v3, v19, v20

    move-object/from16 v0, v21

    array-length v4, v0

    if-eq v3, v4, :cond_3

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_3
    const/4 v3, 0x0

    move v10, v1

    move v1, v15

    :goto_2
    move/from16 v0, v19

    if-ge v3, v0, :cond_7

    const/4 v4, 0x0

    move v15, v1

    move v1, v2

    :goto_3
    move/from16 v0, v20

    if-ge v4, v0, :cond_6

    int-to-long v8, v1

    add-long v13, v17, v8

    add-int/lit8 v16, v1, 0x1

    aget-object v2, v21, v1

    move-object/from16 v1, p0

    move-object/from16 v5, p1

    move/from16 v8, p2

    move/from16 v9, p3

    invoke-direct/range {v1 .. v14}, LaN/p;->a(LaN/P;IILam/e;IIZZZJJ)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    add-int/2addr v1, v15

    if-eqz v10, :cond_4

    invoke-static {v11, v12}, LaN/p;->a(J)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v10, 0x0

    :cond_4
    add-int/lit8 v4, v4, 0x1

    move v15, v1

    move/from16 v1, v16

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    move v1, v15

    goto :goto_2

    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, LaN/p;->d(I)V

    move-object/from16 v0, p0

    iget v2, v0, LaN/p;->C:I

    const/16 v3, 0x30

    if-le v2, v3, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, LaN/p;->a:LaN/D;

    invoke-virtual {v2}, LaN/D;->f()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LaN/p;->C:I

    :cond_8
    if-eqz p1, :cond_9

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, LaN/p;->e(I)V

    :cond_9
    if-eqz p2, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->a:LaN/D;

    invoke-virtual {v1}, LaN/D;->j()Z

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->a:LaN/D;

    invoke-virtual {v1}, LaN/D;->k()V

    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->u:LaN/Q;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, LaN/p;->u:LaN/Q;

    invoke-interface {v1}, LaN/Q;->b()V

    :cond_b
    invoke-static {}, LaN/f;->f()V

    move-object/from16 v0, p0

    iput-wide v11, v0, LaN/p;->z:J

    invoke-static {v11, v12}, LaN/p;->a(J)Z

    move-result v1

    if-nez v1, :cond_c

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method private b(LaN/I;)V
    .locals 7

    invoke-virtual {p1}, LaN/I;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaN/I;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    const/16 v2, 0x16

    const-string v3, "pc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, LaN/I;->h()J

    move-result-wide v5

    sub-long/2addr v0, v5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private b(LaN/P;)Z
    .locals 3

    invoke-virtual {p1}, LaN/P;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LaN/P;->d()I

    move-result v0

    iget-object v1, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v1}, LaN/P;->d()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, LaN/P;->d()I

    move-result v0

    iget-object v1, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v1}, LaN/P;->d()I

    move-result v1

    iget v2, p0, LaN/p;->o:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LaN/P;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, LaN/P;->e()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->b()I

    move-result v2

    div-int/lit16 v3, v2, 0x100

    iget v2, p0, LaN/p;->n:I

    if-lt v2, v3, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v2}, LaN/P;->c()I

    move-result v2

    iget v4, p0, LaN/p;->n:I

    add-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    rem-int/2addr v2, v3

    iget-object v3, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v3}, LaN/P;->c()I

    move-result v3

    if-ge v3, v2, :cond_4

    invoke-virtual {p1}, LaN/P;->c()I

    move-result v3

    iget-object v4, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v4}, LaN/P;->c()I

    move-result v4

    if-lt v3, v4, :cond_3

    invoke-virtual {p1}, LaN/P;->c()I

    move-result v3

    if-le v3, v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, LaN/P;->c()I

    move-result v3

    iget-object v4, p0, LaN/p;->s:LaN/P;

    invoke-virtual {v4}, LaN/P;->c()I

    move-result v4

    if-ge v3, v4, :cond_5

    invoke-virtual {p1}, LaN/P;->c()I

    move-result v3

    if-gt v3, v2, :cond_6

    :cond_5
    move v1, v0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private d(I)V
    .locals 9

    const/4 v1, 0x0

    iget-object v3, p0, LaN/p;->t:[LaN/P;

    iget v0, p0, LaN/p;->l:I

    iget v2, p0, LaN/p;->m:I

    mul-int/2addr v0, v2

    array-length v2, v3

    if-eq v0, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaN/p;->E:LaN/w;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaN/p;->D:Z

    if-eqz v0, :cond_0

    iget v0, p0, LaN/p;->n:I

    iget v2, p0, LaN/p;->o:I

    mul-int/2addr v0, v2

    if-lt p1, v0, :cond_0

    iput-boolean v1, p0, LaN/p;->D:Z

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    const/4 v0, -0x1

    move v2, v0

    move v0, v1

    :goto_1
    array-length v6, v3

    if-ge v0, v6, :cond_3

    aget-object v6, v3, v0

    invoke-virtual {p0, v6}, LaN/p;->a(LaN/P;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, LaN/p;->a:LaN/D;

    aget-object v7, v3, v0

    const v8, 0x7a1200

    invoke-virtual {v6, v7, v8, v1, v1}, LaN/D;->a(LaN/P;IZZ)LaN/I;

    move-result-object v6

    invoke-virtual {v6}, LaN/I;->q()[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v4}, LaN/p;->a([Ljava/lang/String;Ljava/util/HashSet;)V

    invoke-virtual {v6}, LaN/I;->r()[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, LaN/p;->a([Ljava/lang/String;Ljava/util/HashSet;)V

    invoke-virtual {v6}, LaN/I;->s()I

    move-result v7

    if-le v7, v2, :cond_2

    invoke-virtual {v6}, LaN/I;->s()I

    move-result v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-direct {p0, v4, v5, v2}, LaN/p;->a(Ljava/util/HashSet;Ljava/util/HashSet;I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaN/p;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, LaN/p;->F:Ljava/lang/String;

    iget-object v1, p0, LaN/p;->E:LaN/w;

    invoke-interface {v1, v0}, LaN/w;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e(I)V
    .locals 7

    const/16 v6, 0x16

    iget-wide v0, p0, LaN/p;->y:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iput p1, p0, LaN/p;->w:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LaN/p;->k()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    and-int/lit8 v0, v0, -0x80

    if-eqz v0, :cond_0

    goto :goto_0

    :pswitch_1
    const-string v0, "s"

    :goto_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iget-wide v3, p0, LaN/p;->y:J

    sub-long v2, v1, v3

    iget v1, p0, LaN/p;->w:I

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tf"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v1, v4}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, LaN/p;->x:Z

    :cond_2
    iget v1, p0, LaN/p;->w:I

    if-ge v1, p1, :cond_3

    iget v1, p0, LaN/p;->l:I

    iget v4, p0, LaN/p;->m:I

    mul-int/2addr v1, v4

    if-ne p1, v1, :cond_3

    iget-boolean v1, p0, LaN/p;->x:Z

    if-eqz v1, :cond_4

    const-string v1, "tc"

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_3
    iput p1, p0, LaN/p;->w:I

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "h"

    goto :goto_1

    :pswitch_3
    const-string v0, "n"

    goto/16 :goto_1

    :pswitch_4
    const-string v0, "m"

    goto/16 :goto_1

    :cond_4
    const-string v1, "tp"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static f(LaN/B;)I
    .locals 2

    const/16 v0, 0xf

    invoke-static {p0}, LaN/p;->h(LaN/B;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LaN/p;->g(LaN/B;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x10

    goto :goto_0
.end method

.method public static g(LaN/B;)Z
    .locals 3

    if-eqz p0, :cond_0

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v1

    const v2, 0x16c6e44

    if-le v0, v2, :cond_0

    const v2, 0x2bf01d6

    if-ge v0, v2, :cond_0

    const v0, 0x7604113

    if-le v1, v0, :cond_0

    const v0, 0x8920c07

    if-ge v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LaN/B;)Z
    .locals 7

    const v6, 0x7cfb66a

    const v5, 0x7ae0f28

    const v4, 0x7a8b1de

    const v3, 0x24e6895

    if-eqz p0, :cond_4

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v1

    const v2, 0x1f75f9c

    if-le v0, v2, :cond_0

    if-ge v0, v3, :cond_0

    const v2, 0x76d5478

    if-le v1, v2, :cond_0

    if-lt v1, v4, :cond_3

    :cond_0
    const v2, 0x20de3a2

    if-le v0, v2, :cond_1

    if-ge v0, v3, :cond_1

    if-le v1, v4, :cond_1

    if-lt v1, v5, :cond_3

    :cond_1
    const v2, 0x2167b23

    if-le v0, v2, :cond_2

    if-ge v0, v3, :cond_2

    if-le v1, v5, :cond_2

    if-lt v1, v6, :cond_3

    :cond_2
    const v2, 0x234ffbd

    if-le v0, v2, :cond_4

    if-ge v0, v3, :cond_4

    if-le v1, v6, :cond_4

    const v0, 0x7de3697

    if-ge v1, v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 10

    const/4 v1, 0x0

    iget-object v0, p0, LaN/p;->t:[LaN/P;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LaN/p;->k()B

    move-result v5

    move v0, v1

    move v2, v1

    :goto_0
    iget v3, p0, LaN/p;->l:I

    if-ge v0, v3, :cond_1

    move v3, v1

    :goto_1
    iget v4, p0, LaN/p;->m:I

    if-ge v3, v4, :cond_0

    iget-object v6, p0, LaN/p;->t:[LaN/P;

    add-int/lit8 v4, v2, 0x1

    iget-object v7, p0, LaN/p;->r:LaN/P;

    invoke-virtual {v7}, LaN/P;->c()I

    move-result v7

    add-int/2addr v7, v0

    iget-object v8, p0, LaN/p;->r:LaN/P;

    invoke-virtual {v8}, LaN/P;->d()I

    move-result v8

    add-int/2addr v8, v3

    iget-object v9, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v9}, LaN/H;->b()LaN/Y;

    move-result-object v9

    invoke-static {v5, v7, v8, v9}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v7

    aput-object v7, v6, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private x()V
    .locals 2

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/B;->a(LaN/Y;)I

    move-result v0

    iput v0, p0, LaN/p;->p:I

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/B;->b(LaN/Y;)I

    move-result v0

    iput v0, p0, LaN/p;->q:I

    return-void
.end method

.method private y()B
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :pswitch_0
    invoke-static {}, LaN/P;->a()B

    move-result v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(LaN/B;II)LaN/B;
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, LaN/p;->a(LaN/B;LaN/Y;II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public a(LaN/B;LaN/Y;II)LaN/B;
    .locals 2

    iget v0, p0, LaN/p;->j:I

    sub-int v0, p3, v0

    iget v1, p0, LaN/p;->k:I

    sub-int v1, p4, v1

    invoke-virtual {p1, v0, v1, p2}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public a()LaN/D;
    .locals 1

    iget-object v0, p0, LaN/p;->a:LaN/D;

    return-object v0
.end method

.method a(II)LaN/P;
    .locals 10

    const/16 v9, 0x80

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget-object v3, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v3}, LaN/H;->b()LaN/Y;

    move-result-object v5

    invoke-static {v0, v5}, LaN/P;->a(LaN/B;LaN/Y;)I

    move-result v3

    div-int/lit8 v4, p1, 0x2

    sub-int/2addr v3, v4

    invoke-static {v0, v5}, LaN/P;->b(LaN/B;LaN/Y;)I

    move-result v4

    div-int/lit8 v6, p2, 0x2

    sub-int/2addr v4, v6

    invoke-virtual {p0}, LaN/p;->k()B

    move-result v6

    invoke-static {v6, v0, v5}, LaN/P;->a(BLaN/B;LaN/Y;)LaN/P;

    move-result-object v6

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget v7, p0, LaN/p;->p:I

    invoke-virtual {v6}, LaN/P;->f()I

    move-result v8

    sub-int/2addr v7, v8

    if-eqz v0, :cond_3

    if-le v7, v9, :cond_3

    add-int/lit8 v0, v3, 0x1

    :goto_1
    rem-int/lit8 v3, p2, 0x2

    if-nez v3, :cond_1

    :goto_2
    iget v2, p0, LaN/p;->q:I

    invoke-virtual {v6}, LaN/P;->g()I

    move-result v3

    sub-int/2addr v2, v3

    if-eqz v1, :cond_2

    if-le v2, v9, :cond_2

    add-int/lit8 v1, v4, 0x1

    :goto_3
    invoke-virtual {p0}, LaN/p;->k()B

    move-result v2

    invoke-static {v2, v0, v1, v5}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    move v1, v4

    goto :goto_3

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public declared-synchronized a([LaN/B;LaN/B;)LaN/Y;
    .locals 8

    const/4 v0, 0x0

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    array-length v1, p1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v1, 0x0

    :try_start_1
    aget-object v1, p1, v1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v5

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    move v7, v0

    move v4, v1

    move v6, v5

    :goto_0
    array-length v0, p1

    if-ge v7, v0, :cond_3

    aget-object v0, p1, v7

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v2

    aget-object v0, p1, v7

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    if-ge v2, v6, :cond_2

    move v6, v2

    :cond_2
    if-le v2, v5, :cond_7

    move v3, v2

    :goto_1
    if-ge v0, v4, :cond_6

    move v2, v0

    :goto_2
    if-le v0, v1, :cond_5

    :goto_3
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v4, v2

    move v5, v3

    move v1, v0

    goto :goto_0

    :cond_3
    sub-int v3, v5, v6

    sub-int v2, v1, v4

    iget-object v0, p0, LaN/p;->b:LaN/H;

    if-eqz p2, :cond_4

    add-int v0, v5, v6

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2}, LaN/B;->c()I

    move-result v5

    sub-int v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v3, v0

    add-int v0, v1, v4

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2}, LaN/B;->e()I

    move-result v1

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int v1, v2, v0

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0, p2}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    move v2, v3

    :goto_4
    invoke-direct {p0, v2, v1, v0}, LaN/p;->a(IILaN/H;)LaN/Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_4
    move v1, v2

    move v2, v3

    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move v2, v4

    goto :goto_2

    :cond_7
    move v3, v5

    goto :goto_1
.end method

.method public a(LaN/B;)Lo/D;
    .locals 1

    iget-object v0, p0, LaN/p;->G:Lo/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/p;->G:Lo/C;

    invoke-interface {v0, p1}, Lo/C;->a(LaN/B;)Lo/D;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IIII)V
    .locals 9

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, LaN/p;->d:I

    if-ne p1, v0, :cond_0

    iget v0, p0, LaN/p;->c:I

    if-ne p2, v0, :cond_0

    iget v0, p0, LaN/p;->i:I

    if-ne p3, v0, :cond_0

    iget v0, p0, LaN/p;->h:I

    if-ne p4, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p2, p0, LaN/p;->c:I

    iput p1, p0, LaN/p;->d:I

    iget v0, p0, LaN/p;->d:I

    iget v3, p0, LaN/p;->d:I

    mul-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x4

    iget v3, p0, LaN/p;->c:I

    iget v4, p0, LaN/p;->c:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x4

    add-int/2addr v0, v3

    int-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-int v0, v3

    iput v0, p0, LaN/p;->e:I

    iget v0, p0, LaN/p;->d:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LaN/p;->f:I

    iget v0, p0, LaN/p;->c:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LaN/p;->g:I

    iput p4, p0, LaN/p;->h:I

    iput p3, p0, LaN/p;->i:I

    div-int/lit8 v0, p3, 0x2

    iput v0, p0, LaN/p;->j:I

    div-int/lit8 v0, p4, 0x2

    iput v0, p0, LaN/p;->k:I

    iget v0, p0, LaN/p;->l:I

    iget v3, p0, LaN/p;->m:I

    iget v4, p0, LaN/p;->n:I

    iget v5, p0, LaN/p;->o:I

    iget v6, p0, LaN/p;->d:I

    invoke-static {v6}, LaN/p;->a(I)I

    move-result v6

    iput v6, p0, LaN/p;->l:I

    iget v6, p0, LaN/p;->c:I

    invoke-static {v6}, LaN/p;->a(I)I

    move-result v6

    iput v6, p0, LaN/p;->m:I

    invoke-static {p3}, LaN/p;->a(I)I

    move-result v6

    iput v6, p0, LaN/p;->n:I

    invoke-static {p4}, LaN/p;->a(I)I

    move-result v6

    iput v6, p0, LaN/p;->o:I

    iget-object v6, p0, LaN/p;->t:[LaN/P;

    if-eqz v6, :cond_1

    mul-int v6, v0, v3

    iget v7, p0, LaN/p;->l:I

    iget v8, p0, LaN/p;->m:I

    mul-int/2addr v7, v8

    if-eq v6, v7, :cond_2

    :cond_1
    iget v6, p0, LaN/p;->l:I

    iget v7, p0, LaN/p;->m:I

    mul-int/2addr v6, v7

    new-array v6, v6, [LaN/P;

    iput-object v6, p0, LaN/p;->t:[LaN/P;

    :cond_2
    iget v6, p0, LaN/p;->l:I

    if-ne v0, v6, :cond_3

    iget v0, p0, LaN/p;->m:I

    if-eq v3, v0, :cond_5

    :cond_3
    move v0, v2

    :goto_1
    iget v3, p0, LaN/p;->n:I

    if-ne v4, v3, :cond_4

    iget v3, p0, LaN/p;->o:I

    if-eq v5, v3, :cond_6

    :cond_4
    :goto_2
    invoke-direct {p0, v0, v2}, LaN/p;->a(ZZ)V

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->o()V

    iput v1, p0, LaN/p;->w:I

    iput-boolean v1, p0, LaN/p;->x:Z

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_2
.end method

.method public a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V
    .locals 2

    invoke-virtual {p0, p1, p2, p3, p4}, LaN/p;->b(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V

    iget v0, p4, Landroid/graphics/Point;->x:I

    iget v1, p0, LaN/p;->j:I

    add-int/2addr v0, v1

    iput v0, p4, Landroid/graphics/Point;->x:I

    iget v0, p4, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/p;->k:I

    add-int/2addr v0, v1

    iput v0, p4, Landroid/graphics/Point;->y:I

    return-void
.end method

.method public a(LaN/B;Landroid/graphics/Point;)V
    .locals 2

    invoke-virtual {p0, p1, p2}, LaN/p;->c(LaN/B;Landroid/graphics/Point;)V

    iget v0, p2, Landroid/graphics/Point;->x:I

    iget v1, p0, LaN/p;->f:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->x:I

    iget v0, p2, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/p;->g:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->y:I

    return-void
.end method

.method public declared-synchronized a(LaN/H;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    iput-object p1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {p0}, LaN/p;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->e(LaN/B;)I

    move-result v0

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    if-le v1, v0, :cond_1

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-virtual {p1, v0}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v0

    iput-object v0, p0, LaN/p;->b:LaN/H;

    :cond_1
    invoke-direct {p0}, LaN/p;->x()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/p;->a(ZZ)V

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->o()V

    const/4 v0, 0x0

    iput v0, p0, LaN/p;->w:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/p;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LaN/Q;)V
    .locals 0

    iput-object p1, p0, LaN/p;->u:LaN/Q;

    return-void
.end method

.method public declared-synchronized a(LaN/Y;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0, p1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->a(LaN/H;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LaN/k;)V
    .locals 1

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0, p1}, LaN/D;->a(LaN/k;)V

    return-void
.end method

.method public a(LaN/m;)V
    .locals 1

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0, p1}, LaN/D;->a(LaN/m;)V

    return-void
.end method

.method public a(LaN/q;)V
    .locals 1

    iget-object v0, p0, LaN/p;->A:LaN/t;

    invoke-virtual {v0, p1}, LaN/t;->a(LaN/q;)V

    return-void
.end method

.method public a(LaN/r;)V
    .locals 0

    iput-object p1, p0, LaN/p;->B:LaN/r;

    return-void
.end method

.method public a(LaN/w;)V
    .locals 0

    iput-object p1, p0, LaN/p;->E:LaN/w;

    return-void
.end method

.method public a(Lat/d;)V
    .locals 1

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0, p1}, LaN/D;->a(Lat/d;)V

    return-void
.end method

.method public a(Lo/C;)V
    .locals 0

    iput-object p1, p0, LaN/p;->G:Lo/C;

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LaN/p;->f()V

    :cond_0
    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0, p1}, LaN/D;->c(Z)V

    return-void
.end method

.method public a(LaN/B;Z)Z
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LaN/p;->a(LaN/B;ZLaN/Y;)Z

    move-result v0

    return v0
.end method

.method public a(LaN/B;ZLaN/Y;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, LaN/p;->a:LaN/D;

    invoke-virtual {p0}, LaN/p;->k()B

    move-result v2

    invoke-static {v2, p1, p3}, LaN/P;->a(BLaN/B;LaN/Y;)LaN/P;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v0, p2}, LaN/D;->a(LaN/P;IZZ)LaN/I;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {v1}, LaN/I;->m()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {v1}, LaN/I;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public a(LaN/P;)Z
    .locals 1

    invoke-direct {p0, p1}, LaN/p;->b(LaN/P;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, LaN/p;->c(LaN/P;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lam/e;ZZZZ)Z
    .locals 2

    iget v0, p0, LaN/p;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, LaN/p;->d:I

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Map has zero size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, LaN/p;->A:LaN/t;

    invoke-virtual {v0, p3, p4, p0}, LaN/t;->a(ZZLaN/p;)V

    :cond_2
    invoke-direct {p0, p1, p2, p5}, LaN/p;->a(Lam/e;ZZ)Z

    move-result v0

    return v0
.end method

.method public b(LaN/H;)I
    .locals 5

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    iget v2, p0, LaN/p;->j:I

    iget v3, p0, LaN/p;->k:I

    invoke-virtual {v0, v2, v3, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v2

    iget v3, p0, LaN/p;->j:I

    neg-int v3, v3

    iget v4, p0, LaN/p;->k:I

    neg-int v4, v4

    invoke-virtual {v0, v3, v4, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public b(II)LaN/B;
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, LaN/p;->a(LaN/B;II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b()LaN/H;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .locals 1

    invoke-virtual {p0}, LaN/p;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    :cond_0
    invoke-static {p1}, LaN/Y;->c(I)V

    :cond_1
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0, p1}, LaN/H;->a(I)LaN/H;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->a(LaN/H;)V

    return-void
.end method

.method public declared-synchronized b(LaN/B;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0, p1}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->a(LaN/H;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V
    .locals 6

    invoke-virtual {p1, p2}, LaN/B;->a(LaN/Y;)I

    move-result v3

    invoke-virtual {p1, p2}, LaN/B;->b(LaN/Y;)I

    move-result v4

    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LaN/p;->a(LaN/B;LaN/Y;IILandroid/graphics/Point;)V

    return-void
.end method

.method public b(LaN/B;Landroid/graphics/Point;)V
    .locals 2

    invoke-virtual {p0, p1, p2}, LaN/p;->c(LaN/B;Landroid/graphics/Point;)V

    iget v0, p2, Landroid/graphics/Point;->x:I

    iget v1, p0, LaN/p;->j:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->x:I

    iget v0, p2, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/p;->k:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->y:I

    return-void
.end method

.method public b(LaN/k;)V
    .locals 1

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0, p1}, LaN/D;->b(LaN/k;)V

    return-void
.end method

.method public c(I)I
    .locals 1

    iget-object v0, p0, LaN/p;->B:LaN/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/p;->B:LaN/r;

    invoke-interface {v0, p1}, LaN/r;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/Y;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public c(LaN/H;)I
    .locals 5

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    iget v2, p0, LaN/p;->j:I

    iget v3, p0, LaN/p;->k:I

    invoke-virtual {v0, v2, v3, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v2

    iget v3, p0, LaN/p;->j:I

    neg-int v3, v3

    iget v4, p0, LaN/p;->k:I

    neg-int v4, v4

    invoke-virtual {v0, v3, v4, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    sub-int v0, v1, v0

    if-gez v0, :cond_0

    const v1, 0x15752a00

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public declared-synchronized c()LaN/Y;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(LaN/B;)Landroid/graphics/Point;
    .locals 1

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p0, p1, v0}, LaN/p;->c(LaN/B;Landroid/graphics/Point;)V

    return-object v0
.end method

.method public declared-synchronized c(II)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->b(LaN/B;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(LaN/B;Landroid/graphics/Point;)V
    .locals 6

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v2

    iget v3, p0, LaN/p;->p:I

    iget v4, p0, LaN/p;->q:I

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LaN/p;->a(LaN/B;LaN/Y;IILandroid/graphics/Point;)V

    return-void
.end method

.method public declared-synchronized d()LaN/B;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(II)V
    .locals 0

    invoke-virtual {p0, p1, p2, p1, p2}, LaN/p;->a(IIII)V

    return-void
.end method

.method public declared-synchronized d(LaN/B;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    :try_start_1
    invoke-virtual {p0, p1}, LaN/p;->b(LaN/B;)V

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, LaN/p;->a(Lam/e;ZZ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0, v1}, LaN/p;->b(LaN/B;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {p0, v1}, LaN/p;->b(LaN/B;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()I
    .locals 3

    invoke-virtual {p0}, LaN/p;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/p;->e(LaN/B;)I

    move-result v0

    invoke-virtual {p0}, LaN/p;->c()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    const/16 v2, 0x1388

    div-int/2addr v2, v0

    sub-int/2addr v0, v1

    mul-int/2addr v0, v2

    return v0
.end method

.method public e(LaN/B;)I
    .locals 2

    invoke-virtual {p0}, LaN/p;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->c()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/16 v0, 0x10

    goto :goto_0

    :cond_1
    invoke-static {}, LaN/I;->w()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    add-int/lit8 v0, v0, 0x14

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized f()V
    .locals 5

    const/4 v0, 0x2

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0xe

    invoke-direct {v2, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x2

    invoke-virtual {v3, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->a()LaN/B;

    move-result-object v1

    invoke-static {v1, v3}, LaN/B;->a(LaN/B;Ljava/io/DataOutput;)V

    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v1, 0x0

    iget-object v4, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v4}, LaN/H;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    :cond_0
    :goto_0
    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, "Map info"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v4, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v4}, LaN/H;->f()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "MAP"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()[LaN/P;
    .locals 1

    iget-object v0, p0, LaN/p;->t:[LaN/P;

    return-object v0
.end method

.method public h()V
    .locals 1

    iget-boolean v0, p0, LaN/p;->v:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/p;->v:Z

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->p()V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, LaN/p;->v:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, LaN/p;->v:Z

    const/4 v0, 0x0

    iput-object v0, p0, LaN/p;->F:Ljava/lang/String;

    iput-boolean v1, p0, LaN/p;->D:Z

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->q()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, LaN/p;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->r()V

    return-void
.end method

.method public k()B
    .locals 3

    const/16 v0, 0x14

    invoke-direct {p0}, LaN/p;->y()B

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v2, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v2}, LaN/H;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iget-object v1, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v1}, LaN/H;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, -0x80

    int-to-byte v0, v0

    :cond_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public l()B
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x14

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public declared-synchronized m()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {p0, v0}, LaN/p;->b(LaN/H;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized n()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {p0, v0}, LaN/p;->c(LaN/H;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()Z
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->e()Z

    move-result v0

    return v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, LaN/p;->b:LaN/H;

    invoke-virtual {v0}, LaN/H;->h()Z

    move-result v0

    return v0
.end method

.method public q()I
    .locals 1

    iget v0, p0, LaN/p;->c:I

    return v0
.end method

.method public r()I
    .locals 1

    iget v0, p0, LaN/p;->d:I

    return v0
.end method

.method public s()I
    .locals 1

    iget v0, p0, LaN/p;->h:I

    return v0
.end method

.method public t()I
    .locals 1

    iget v0, p0, LaN/p;->i:I

    return v0
.end method

.method public u()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    invoke-virtual {p0}, LaN/p;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, LaN/p;->m()I

    move-result v1

    invoke-virtual {p0}, LaN/p;->n()I

    move-result v2

    invoke-virtual {p0}, LaN/p;->c()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public v()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
