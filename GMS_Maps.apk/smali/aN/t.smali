.class LaN/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LaN/B;

.field private b:LaN/Y;

.field private c:LaN/q;

.field private d:LaN/B;

.field private e:LaN/Y;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaN/t;->a:LaN/B;

    iput-object v0, p0, LaN/t;->b:LaN/Y;

    iput-object v0, p0, LaN/t;->c:LaN/q;

    iput-object v0, p0, LaN/t;->d:LaN/B;

    iput-object v0, p0, LaN/t;->e:LaN/Y;

    return-void
.end method

.method private a(LaN/B;ZZLaN/p;)V
    .locals 2

    invoke-virtual {p4}, LaN/p;->b()LaN/H;

    move-result-object v0

    iget-object v1, p0, LaN/t;->a:LaN/B;

    iput-object v1, p0, LaN/t;->d:LaN/B;

    iget-object v1, p0, LaN/t;->b:LaN/Y;

    iput-object v1, p0, LaN/t;->e:LaN/Y;

    iput-object p1, p0, LaN/t;->a:LaN/B;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v1

    iput-object v1, p0, LaN/t;->b:LaN/Y;

    invoke-static {p1, p2, p3, p4}, LaN/t;->b(LaN/B;ZZLaN/p;)V

    iget-object v1, p0, LaN/t;->c:LaN/q;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaN/t;->c:LaN/q;

    invoke-interface {v1, v0}, LaN/q;->a(LaN/H;)V

    :cond_0
    return-void
.end method

.method private static a()Z
    .locals 2

    invoke-static {}, LaN/P;->a()B

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LaN/B;ZZLaN/p;)V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p3}, LaN/p;->b()LaN/H;

    move-result-object v0

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-static {p0, v1}, LaN/B;->a(LaN/B;Ljava/io/DataOutput;)V

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v4

    invoke-static {v4, v1}, LaN/B;->a(LaN/B;Ljava/io/DataOutput;)V

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p3, v0}, LaN/p;->b(LaN/H;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p3, v0}, LaN/p;->c(LaN/H;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v5, 0x1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Law/h;->a(I[BZZZ)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method a(LaN/q;)V
    .locals 0

    iput-object p1, p0, LaN/t;->c:LaN/q;

    return-void
.end method

.method a(ZZLaN/p;)V
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p3}, LaN/p;->b()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaN/t;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaN/t;->a:LaN/B;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaN/t;->b:LaN/Y;

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v2

    if-eq v0, v2, :cond_3

    :cond_2
    invoke-virtual {v4}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2, p3}, LaN/t;->a(LaN/B;ZZLaN/p;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    iget-object v0, p0, LaN/t;->a:LaN/B;

    invoke-virtual {p3, v0}, LaN/p;->c(LaN/B;)Landroid/graphics/Point;

    move-result-object v5

    invoke-virtual {p3}, LaN/p;->r()I

    move-result v0

    invoke-virtual {p3}, LaN/p;->t()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p3}, LaN/p;->q()I

    move-result v3

    invoke-virtual {p3}, LaN/p;->s()I

    move-result v6

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    div-int/lit8 v6, v0, 0x2

    div-int/lit8 v7, v3, 0x2

    iget v8, v5, Landroid/graphics/Point;->x:I

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-gt v8, v0, :cond_4

    iget v8, v5, Landroid/graphics/Point;->y:I

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-le v8, v3, :cond_6

    :cond_4
    invoke-virtual {v4}, LaN/H;->a()LaN/B;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, LaN/t;->d:LaN/B;

    if-eqz v1, :cond_5

    iget-object v1, p0, LaN/t;->e:LaN/Y;

    iget-object v2, p0, LaN/t;->b:LaN/Y;

    if-ne v1, v2, :cond_5

    invoke-virtual {v4}, LaN/H;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/B;->a(LaN/B;)J

    move-result-wide v1

    invoke-virtual {v4}, LaN/H;->a()LaN/B;

    move-result-object v3

    iget-object v4, p0, LaN/t;->a:LaN/B;

    invoke-virtual {v3, v4}, LaN/B;->a(LaN/B;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :cond_5
    invoke-direct {p0, v0, p1, p2, p3}, LaN/t;->a(LaN/B;ZZLaN/p;)V

    goto :goto_0

    :cond_6
    iget v8, v5, Landroid/graphics/Point;->x:I

    neg-int v9, v6

    if-ge v8, v9, :cond_9

    :goto_2
    iget v6, v5, Landroid/graphics/Point;->y:I

    neg-int v8, v7

    if-ge v6, v8, :cond_a

    move v1, v3

    :cond_7
    :goto_3
    if-nez v1, :cond_8

    if-eqz v0, :cond_b

    :cond_8
    iget-object v2, p0, LaN/t;->a:LaN/B;

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    goto :goto_1

    :cond_9
    iget v8, v5, Landroid/graphics/Point;->x:I

    if-le v8, v6, :cond_c

    neg-int v0, v0

    goto :goto_2

    :cond_a
    iget v5, v5, Landroid/graphics/Point;->y:I

    if-le v5, v7, :cond_7

    neg-int v1, v3

    goto :goto_3

    :cond_b
    move-object v0, v2

    goto :goto_1

    :cond_c
    move v0, v1

    goto :goto_2
.end method
