.class public LaO/a;
.super LaN/p;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/maps/driveabout/vector/bk;

.field private d:Lcom/google/android/maps/driveabout/vector/bA;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:[LaN/P;

.field private h:B

.field private i:B

.field private j:Lcom/google/android/maps/driveabout/vector/aZ;

.field private k:Lcom/google/android/maps/driveabout/vector/aZ;

.field private l:Z

.field private m:Ln/s;

.field private n:Z

.field private o:Lcom/google/android/maps/driveabout/vector/aZ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;IIILaN/B;LaN/Y;I)V
    .locals 7

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, LaN/p;-><init>(IIILaN/B;LaN/Y;I)V

    const/4 v0, 0x0

    iput-object v0, p0, LaO/a;->f:Landroid/widget/TextView;

    new-instance v0, LaO/b;

    invoke-direct {v0, p0, p1}, LaO/b;-><init>(LaO/a;Landroid/content/res/Resources;)V

    iput-object v0, p0, LaO/a;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    iput-object v0, p0, LaO/a;->m:Ln/s;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bB;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bB;-><init>(Law/p;)V

    new-instance v1, LaO/c;

    invoke-direct {v1, p0}, LaO/c;-><init>(LaO/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bB;->a(Lcom/google/android/maps/driveabout/vector/bC;)V

    new-instance v1, LaO/d;

    invoke-direct {v1, p0}, LaO/d;-><init>(LaO/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bB;->a(Lcom/google/android/maps/driveabout/vector/bD;)V

    iget-object v1, p0, LaO/a;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/bB;)V

    const/4 v0, 0x0

    new-array v0, v0, [LaN/P;

    iput-object v0, p0, LaO/a;->g:[LaN/P;

    return-void
.end method

.method private A()V
    .locals 4

    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaO/a;->k()B

    move-result v0

    iget-byte v1, p0, LaO/a;->h:B

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LaO/a;->k()B

    move-result v0

    iput-byte v0, p0, LaO/a;->h:B

    invoke-virtual {p0}, LaO/a;->b()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LA/c;->d:LA/c;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    :goto_1
    iget-object v2, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v2, :cond_2

    iget-object v2, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    iget-object v3, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-interface {v2, v3}, Lcom/google/android/maps/driveabout/vector/bA;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_2
    sget-object v2, LA/c;->a:LA/c;

    if-eq v0, v2, :cond_9

    sget-object v2, LA/c;->c:LA/c;

    if-eq v0, v2, :cond_9

    sget-object v2, LA/c;->j:LA/c;

    if-eq v0, v2, :cond_9

    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v1, v2, :cond_8

    iget-object v2, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bA;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->c(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    :goto_2
    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    iget-object v2, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/vector/bA;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :goto_3
    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/bA;->setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :cond_3
    sget-object v0, LA/c;->d:LA/c;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, LaN/H;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, LA/c;->e:LA/c;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    goto :goto_1

    :cond_5
    sget-object v0, LA/c;->e:LA/c;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->d:Lcom/google/android/maps/driveabout/vector/q;

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, LA/c;->j:LA/c;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    goto :goto_1

    :cond_7
    sget-object v0, LA/c;->a:LA/c;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    goto :goto_1

    :cond_8
    iget-object v2, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bA;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    goto :goto_3
.end method

.method private B()V
    .locals 2

    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaO/a;->l()B

    move-result v0

    iget-byte v1, p0, LaO/a;->i:B

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LaO/a;->l()B

    move-result v0

    iput-byte v0, p0, LaO/a;->i:B

    invoke-virtual {p0}, LaO/a;->b()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LA/c;->j:LA/c;

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bA;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    iget-object v1, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/vector/bA;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LA/c;->c:LA/c;

    goto :goto_1

    :cond_3
    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bA;->c()LA/c;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(LaO/a;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, LaO/a;->d(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaO/a;)Ln/s;
    .locals 1

    iget-object v0, p0, LaO/a;->m:Ln/s;

    return-object v0
.end method

.method private d(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "|z="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private z()V
    .locals 2

    iget-object v0, p0, LaO/a;->k:Lcom/google/android/maps/driveabout/vector/aZ;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, LaO/a;->l:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, LaO/a;->y()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    iget-object v1, p0, LaO/a;->k:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/bA;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaO/a;->l:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, LaO/a;->l:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaO/a;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    iget-object v1, p0, LaO/a;->k:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/bA;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaO/a;->l:Z

    goto :goto_0
.end method


# virtual methods
.method public a(LaN/B;II)LaN/B;
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getMapPoint should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized a(LaN/H;)V
    .locals 1

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    iput-object p1, p0, LaO/a;->b:LaN/H;

    invoke-direct {p0}, LaO/a;->A()V

    invoke-direct {p0}, LaO/a;->z()V

    invoke-direct {p0}, LaO/a;->B()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/Y;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setZoom should be called on the MapController, not map."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aq;)V
    .locals 1

    iput-object p1, p0, LaO/a;->k:Lcom/google/android/maps/driveabout/vector/aZ;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaO/a;->l:Z

    invoke-direct {p0}, LaO/a;->z()V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bA;)V
    .locals 2

    iput-object p1, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    if-eqz p1, :cond_0

    iget-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    iget-object v1, p0, LaO/a;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/bA;->setController(Lcom/google/android/maps/driveabout/vector/bk;)V

    invoke-virtual {p0}, LaO/a;->b()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LaO/a;->b(I)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LaN/P;

    iput-object v0, p0, LaO/a;->g:[LaN/P;

    const/4 v0, 0x0

    invoke-virtual {p0}, LaO/a;->k()B

    move-result v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, LaN/Y;->b(I)LaN/Y;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v5

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    invoke-static {v2, v5, v0, v4}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v4

    iget-object v5, p0, LaO/a;->g:[LaN/P;

    add-int/lit8 v0, v1, 0x1

    aput-object v4, v5, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, LaO/a;->a:LaN/D;

    invoke-virtual {v0}, LaN/D;->l()V

    iget-object v0, p0, LaO/a;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, LaO/a;->b()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->c()I

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaO/a;->g()[LaN/P;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->a([LaN/P;)I

    move-result v0

    iget-object v1, p0, LaO/a;->f:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "% prefetched"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LaO/a;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaO/a;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public a(LaN/B;Z)Z
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "canCover should not be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LaN/B;ZLaN/Y;)Z
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "canCover should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LaN/P;)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, LaO/a;->g:[LaN/P;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    if-ne p1, v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public a(Lam/e;ZZZZ)Z
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "drawMap should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized b(LaN/B;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setCenterPoint should be called on the MapController, not map."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(LaN/B;)V
    .locals 0

    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public g()[LaN/P;
    .locals 1

    iget-object v0, p0, LaO/a;->g:[LaN/P;

    return-object v0
.end method

.method public v()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public w()Lcom/google/android/maps/driveabout/vector/bk;
    .locals 1

    iget-object v0, p0, LaO/a;->c:Lcom/google/android/maps/driveabout/vector/bk;

    return-object v0
.end method

.method public x()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, LaO/a;->d:Lcom/google/android/maps/driveabout/vector/bA;

    invoke-virtual {p0, v0}, LaO/a;->a(Lo/C;)V

    iput-object v0, p0, LaO/a;->e:Landroid/widget/TextView;

    iput-object v0, p0, LaO/a;->j:Lcom/google/android/maps/driveabout/vector/aZ;

    iput-object v0, p0, LaO/a;->k:Lcom/google/android/maps/driveabout/vector/aZ;

    iput-object v0, p0, LaO/a;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    iput-byte v1, p0, LaO/a;->h:B

    iput-boolean v1, p0, LaO/a;->n:Z

    return-void
.end method

.method public y()Z
    .locals 2

    invoke-virtual {p0}, LaO/a;->b()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
