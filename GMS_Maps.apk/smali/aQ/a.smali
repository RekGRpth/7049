.class public LaQ/a;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:[LaQ/c;

.field private b:[LaQ/c;

.field private c:I

.field private d:LaQ/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LaQ/a;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, LaQ/a;->d:LaQ/b;

    return-void
.end method


# virtual methods
.method public a(LaQ/b;)V
    .locals 0

    iput-object p1, p0, LaQ/a;->d:LaQ/b;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/iB;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 9

    const/4 v2, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iB;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v2, p0, LaQ/a;->a:[LaQ/c;

    iput-object v2, p0, LaQ/a;->b:[LaQ/c;

    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LaQ/a;->c:I

    iget v0, p0, LaQ/a;->c:I

    if-eqz v0, :cond_0

    :goto_0
    return v7

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_3

    new-instance v5, LaQ/c;

    invoke-virtual {v1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-direct {v5, v6}, LaQ/c;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v5}, LaQ/c;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, LaQ/c;->e()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LaQ/c;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaQ/c;

    iput-object v0, p0, LaQ/a;->a:[LaQ/c;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LaQ/c;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaQ/c;

    iput-object v0, p0, LaQ/a;->b:[LaQ/c;

    goto :goto_0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x3c

    return v0
.end method

.method public d_()V
    .locals 3

    iget-object v0, p0, LaQ/a;->d:LaQ/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaQ/a;->a:[LaQ/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaQ/a;->b:[LaQ/c;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, LaQ/a;->d:LaQ/b;

    invoke-interface {v0}, LaQ/b;->e()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, LaQ/a;->d:LaQ/b;

    iget-object v1, p0, LaQ/a;->a:[LaQ/c;

    iget-object v2, p0, LaQ/a;->b:[LaQ/c;

    invoke-interface {v0, v1, v2}, LaQ/b;->a([LaQ/c;[LaQ/c;)V

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    iget v0, p0, LaQ/a;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    iget v0, p0, LaQ/a;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
