.class public LaR/D;
.super LaR/t;
.source "SourceFile"


# static fields
.field private static final d:Ljava/util/regex/Pattern;


# instance fields
.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:LaN/B;

.field private h:Ljava/util/List;

.field private i:I

.field private j:Lo/o;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/lang/Boolean;

.field private q:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "(cid|ftid)=([:x0-9a-fA-F]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LaR/D;->d:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, LaR/t;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LaR/D;->i:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LaR/D;->q:J

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    invoke-direct {p0}, LaR/t;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LaR/D;->i:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LaR/D;->q:J

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v0

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gq;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, LaR/D;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 2

    invoke-direct {p0}, LaR/t;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LaR/D;->i:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LaR/D;->q:J

    iput-object p1, p0, LaR/D;->a:Ljava/lang/String;

    iget-object v0, p0, LaR/D;->a:Ljava/lang/String;

    invoke-static {v0}, LaR/D;->c(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, LaR/D;->j:Lo/o;

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    iput-object p2, p0, LaR/D;->e:Ljava/lang/String;

    iput-object p3, p0, LaR/D;->f:Ljava/lang/String;

    invoke-static {p4}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaR/D;->g:LaN/B;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, LaR/D;->h:Ljava/util/List;

    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v1, 0x5

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->a:Ljava/lang/String;

    iget-object v0, p0, LaR/D;->a:Ljava/lang/String;

    invoke-static {v0}, LaR/D;->c(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, LaR/D;->j:Lo/o;

    const/4 v0, 0x2

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->e:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->f:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaR/D;->g:LaN/B;

    :cond_0
    const/16 v0, 0x8

    const-wide/16 v1, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->b:J

    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaR/D;->h:Ljava/util/List;

    const/16 v0, 0x9

    const-wide/16 v1, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->c:J

    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v1, 0x3

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->a:Ljava/lang/String;

    iget-object v0, p0, LaR/D;->a:Ljava/lang/String;

    invoke-static {v0}, LaR/D;->c(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, LaR/D;->j:Lo/o;

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->e:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaR/D;->g:LaN/B;

    :cond_0
    const/4 v0, 0x4

    const-wide/16 v1, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->b:J

    const/4 v0, 0x5

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, LaR/D;->i:I

    const/4 v0, 0x6

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    const/4 v0, 0x7

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->l:Ljava/lang/Boolean;

    const/16 v0, 0x9

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->m:Ljava/lang/Boolean;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaR/D;->h:Ljava/util/List;

    const/16 v0, 0xd

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    const/16 v0, 0x10

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    const/16 v0, 0xb

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    const/16 v0, 0xc

    const-wide/16 v1, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->c:J

    return-void
.end method

.method private static c(Ljava/lang/String;)Lo/o;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, LaR/D;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/o;->a(Ljava/lang/String;)Lo/o;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, LaR/D;->i:I

    return-void
.end method

.method public a(LaN/B;)V
    .locals 0

    iput-object p1, p0, LaR/D;->g:LaN/B;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, LaR/D;->h:Ljava/util/List;

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LaR/D;->e:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaR/D;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()LaN/B;
    .locals 1

    iget-object v0, p0, LaR/D;->g:LaN/B;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, LaR/D;->i:I

    return v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, LaR/D;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/D;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    iget-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gq;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x1

    iget-object v2, p0, LaR/D;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    iget-object v2, p0, LaR/D;->k:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    iget-object v2, p0, LaR/D;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, LaR/D;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    iget-object v2, p0, LaR/D;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v0, p0, LaR/D;->g:LaN/B;

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    iget-object v2, p0, LaR/D;->g:LaN/B;

    invoke-static {v2}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-wide v2, p0, LaR/D;->b:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    iget-wide v2, p0, LaR/D;->b:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    const/4 v3, 0x7

    invoke-virtual {v0}, Lo/D;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_4
    iget-wide v2, p0, LaR/D;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    const/16 v0, 0x9

    iget-wide v2, p0, LaR/D;->c:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    return-object v1
.end method

.method public q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    invoke-super {p0}, LaR/t;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v2, p0, LaR/D;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, LaR/D;->g:LaN/B;

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-object v2, p0, LaR/D;->g:LaN/B;

    invoke-static {v2}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget v0, p0, LaR/D;->i:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    const/4 v0, 0x5

    iget v2, p0, LaR/D;->i:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/4 v0, 0x6

    iget-object v2, p0, LaR/D;->k:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    const/4 v0, 0x7

    iget-object v2, p0, LaR/D;->l:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    iget-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    const/16 v0, 0xd

    iget-object v2, p0, LaR/D;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    const/16 v0, 0x9

    iget-object v2, p0, LaR/D;->m:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    const/16 v3, 0xa

    invoke-virtual {v0}, Lo/D;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    const/16 v0, 0x10

    iget-object v2, p0, LaR/D;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    iget-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    const/16 v0, 0xb

    iget-object v2, p0, LaR/D;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_6
    return-object v1
.end method

.method public r()J
    .locals 2

    iget-wide v0, p0, LaR/D;->q:J

    return-wide v0
.end method
