.class public LaR/G;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(LaR/D;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2e3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x2e4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LaR/G;->a(LaR/D;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, LaR/G;->a(Ljava/lang/String;LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 3

    if-eqz p0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LaR/D;->i()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LaR/D;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static b(LaR/D;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LaR/G;->c(LaR/D;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LaR/G;->b(LaR/D;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, LaR/G;->a(Ljava/lang/String;LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(LaR/D;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2e6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LaR/D;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x2e5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LaR/D;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x3f1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, LaR/D;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x6f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
