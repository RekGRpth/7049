.class public final enum LaR/O;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaR/O;

.field public static final enum b:LaR/O;

.field public static final enum c:LaR/O;

.field public static final enum d:LaR/O;

.field public static final enum e:LaR/O;

.field public static final enum f:LaR/O;

.field public static final enum g:LaR/O;

.field public static final enum h:LaR/O;

.field private static final synthetic j:[LaR/O;


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, LaR/O;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->a:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "ACTIVATE_WIZARD"

    invoke-direct {v0, v1, v5, v5}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->b:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "REFRESH"

    invoke-direct {v0, v1, v6, v6}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->c:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "UPDATE_ITEM"

    invoke-direct {v0, v1, v7, v7}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->d:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "ON_RESUME"

    invoke-direct {v0, v1, v8, v8}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->e:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "ON_SIGN_IN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->f:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "CONTENT_PROVIDER"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->g:LaR/O;

    new-instance v0, LaR/O;

    const-string v1, "ACTIVATE_PLACES_WIZARD"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LaR/O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaR/O;->h:LaR/O;

    const/16 v0, 0x8

    new-array v0, v0, [LaR/O;

    sget-object v1, LaR/O;->a:LaR/O;

    aput-object v1, v0, v4

    sget-object v1, LaR/O;->b:LaR/O;

    aput-object v1, v0, v5

    sget-object v1, LaR/O;->c:LaR/O;

    aput-object v1, v0, v6

    sget-object v1, LaR/O;->d:LaR/O;

    aput-object v1, v0, v7

    sget-object v1, LaR/O;->e:LaR/O;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LaR/O;->f:LaR/O;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaR/O;->g:LaR/O;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaR/O;->h:LaR/O;

    aput-object v2, v0, v1

    sput-object v0, LaR/O;->j:[LaR/O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LaR/O;->i:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaR/O;
    .locals 1

    const-class v0, LaR/O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaR/O;

    return-object v0
.end method

.method public static values()[LaR/O;
    .locals 1

    sget-object v0, LaR/O;->j:[LaR/O;

    invoke-virtual {v0}, [LaR/O;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaR/O;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LaR/O;->i:I

    return v0
.end method
