.class public LaR/Y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/T;
.implements LaR/W;
.implements LaR/X;
.implements Lcom/google/googlenav/ui/wizard/bo;


# instance fields
.field private a:Z

.field private final b:LaR/aa;

.field private final c:LaR/g;

.field private final d:LaR/e;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Lcom/google/googlenav/ui/wizard/jv;

.field private final h:Ljava/util/List;


# direct methods
.method public constructor <init>(LaR/aa;Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "SYNC_DATA_LOCAL"

    iput-object v0, p0, LaR/Y;->e:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LaR/Y;->h:Ljava/util/List;

    iput-object p1, p0, LaR/Y;->b:LaR/aa;

    iput-object p2, p0, LaR/Y;->g:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v0, LaR/g;

    invoke-direct {v0}, LaR/g;-><init>()V

    iput-object v0, p0, LaR/Y;->c:LaR/g;

    new-instance v0, LaR/e;

    invoke-direct {v0}, LaR/e;-><init>()V

    iput-object v0, p0, LaR/Y;->d:LaR/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/Y;->f:Z

    invoke-direct {p0}, LaR/Y;->j()V

    return-void
.end method

.method static synthetic a(LaR/Y;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaR/Y;->h:Ljava/util/List;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;)V
    .locals 5

    const/4 v4, 0x2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private c(ILjava/lang/String;)V
    .locals 2

    new-instance v0, LaR/Z;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, LaR/Z;-><init>(LaR/Y;Las/c;ILjava/lang/String;)V

    invoke-virtual {v0}, LaR/Z;->g()V

    return-void
.end method

.method private declared-synchronized c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x6

    :try_start_1
    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, v1}, LaR/g;->c(I)V

    invoke-direct {p0, v1}, LaR/Y;->f(I)V

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    const/4 v3, 0x4

    invoke-virtual {p1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v4, v1, v3}, LaR/g;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v4, v1, v3}, LaR/e;->b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LaR/e;->a(IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d(ILjava/lang/String;)V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "n="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "e="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    const-string v2, "i"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, LaR/Y;->f:Z

    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 1

    iput-boolean p1, p0, LaR/Y;->a:Z

    if-eqz p1, :cond_0

    const-string v0, "SYNC_DATA"

    :goto_0
    iput-object v0, p0, LaR/Y;->e:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "SYNC_DATA_LOCAL"

    goto :goto_0
.end method

.method private declared-synchronized e(Z)V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaR/Y;->f:Z

    invoke-virtual {p0}, LaR/Y;->e()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaR/Y;->d(Z)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LaR/Y;->e()V

    :goto_0
    invoke-direct {p0}, LaR/Y;->k()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaR/Y;->e(I)V

    const/4 v0, 0x1

    sget-object v1, LaR/O;->f:LaR/O;

    invoke-virtual {p0, v0, v1}, LaR/Y;->a(ILaR/O;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0}, LaR/e;->c()V

    invoke-virtual {p0}, LaR/Y;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f(I)V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, p1, v0}, LaR/Y;->c(ILjava/lang/String;)V

    return-void
.end method

.method private declared-synchronized j()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p0}, LaR/g;->a(LaR/W;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaR/Y;->d(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0}, LaR/g;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, LaR/Y;->f(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/j;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-boolean v2, p0, LaR/Y;->f:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0}, LaR/e;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1}, LaR/e;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1, p2}, LaR/e;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1, p2}, LaR/g;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILaR/O;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaR/Y;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1}, LaR/e;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, LaR/Y;->b:LaR/aa;

    invoke-interface {v1, v0, p1, p2}, LaR/aa;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    iget-object v1, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v1, v0}, LaR/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILaR/U;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1, p2}, LaR/g;->a(ILaR/U;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LaR/W;)V
    .locals 1

    iget-object v0, p0, LaR/Y;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaR/Y;->a:Z

    if-nez v0, :cond_1

    const-string v0, "MAPS"

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "MyPlaces: updateStorage called with sync off"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {p0, v2}, LaR/Y;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    iput-object p1, p0, LaR/Y;->g:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method public declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaR/e;->c(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, LaR/Y;->d(ILjava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaR/Y;->e(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 2

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v1, p1, v0}, LaR/e;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LaR/Y;->c(ILjava/lang/String;)V

    sget-object v0, LaR/O;->d:LaR/O;

    invoke-virtual {p0, p1, v0}, LaR/Y;->a(ILaR/O;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LaR/Y;->c(ILjava/lang/String;)V

    return-void
.end method

.method public declared-synchronized b(I)Ljava/util/List;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1}, LaR/g;->b(I)Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1}, LaR/e;->c(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, v1}, LaR/Y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0}, LaR/g;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LaR/Y;->e(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b(ILjava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1, p2}, LaR/g;->b(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1}, LaR/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaR/e;->c(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, LaR/Y;->d(ILjava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaR/Y;->e(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1, p2}, LaR/g;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0}, LaR/e;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1}, LaR/g;->c(I)V

    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1}, LaR/e;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(Z)V
    .locals 0

    return-void
.end method

.method public declared-synchronized d(I)LaR/T;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0, p1}, LaR/g;->a(I)LaR/V;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0}, LaR/g;->b()V

    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0}, LaR/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbM/j;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    invoke-direct {p0}, LaR/Y;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    iget-object v3, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v3}, LaR/g;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    iget-object v3, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v3}, LaR/e;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v3, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iget-object v2, p0, LaR/Y;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "MAPSError saving Sync prefs"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v1, "MAPSOOME saving Sync prefs"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized e(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0, p1}, LaR/e;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "SyncedDataViewImpl.loadSavedState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    iget-object v1, p0, LaR/Y;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, LbM/j;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v1}, LaR/Y;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, LaR/Y;->c:LaR/g;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, LaR/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, LaR/Y;->d:LaR/e;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v0}, LaR/e;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    const-string v0, "SyncedDataViewImpl.loadSavedState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "MAPSError loading Sync prefs"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v1, "MAPSError loading Sync prefs"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized g()V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaR/Y;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->r()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, LaR/Y;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0}, LaR/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, LaR/Y;->d(ILjava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/Y;->f:Z

    :cond_2
    iget-boolean v0, p0, LaR/Y;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LaR/Y;->g:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaR/Y;->g:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x5a5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5a4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    iget-boolean v0, p0, LaR/Y;->f:Z

    invoke-direct {p0, v0}, LaR/Y;->e(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized h()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaR/Y;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, LaR/Y;->b:LaR/aa;

    invoke-interface {v0}, LaR/aa;->h()V

    iget-object v0, p0, LaR/Y;->c:LaR/g;

    invoke-virtual {v0}, LaR/g;->b()V

    iget-object v0, p0, LaR/Y;->d:LaR/e;

    invoke-virtual {v0}, LaR/e;->c()V

    invoke-virtual {p0}, LaR/Y;->e()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaR/Y;->d(Z)V

    invoke-virtual {p0}, LaR/Y;->f()V

    invoke-direct {p0}, LaR/Y;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, LaR/Y;->a:Z

    return v0
.end method
