.class public LaR/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/Q;
.implements LaR/aa;


# instance fields
.field private a:LaR/X;

.field private b:LaR/P;

.field private c:Ljava/util/Vector;

.field private d:LaR/ab;

.field private e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method constructor <init>(LaR/X;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    iput-object p1, p0, LaR/ac;->a:LaR/X;

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, LaR/ac;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/ac;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, LaR/ac;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaR/ac;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method private a(ILaR/O;)V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sa="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, LaR/O;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7b

    const-string v2, "y"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(LaR/ac;)V
    .locals 0

    invoke-direct {p0}, LaR/ac;->a()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-static {p0}, LaR/ac;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    iget-object v0, p0, LaR/ac;->b:LaR/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/ac;->b:LaR/P;

    invoke-virtual {v0}, LaR/P;->Z()V

    :cond_0
    new-instance v0, LaR/P;

    invoke-direct {v0, p1}, LaR/P;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, LaR/ac;->b:LaR/P;

    iget-object v0, p0, LaR/ac;->b:LaR/P;

    invoke-virtual {v0, p0}, LaR/P;->a(LaR/Q;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LaR/ac;->b:LaR/P;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    iget-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/ab;

    iget-object v2, p0, LaR/ac;->b:LaR/P;

    invoke-interface {v0, v2}, LaR/ab;->a(LaR/P;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaR/ac;->d:LaR/ab;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaR/ac;->d:LaR/ab;

    iget-object v1, p0, LaR/ac;->b:LaR/P;

    invoke-interface {v0, v1}, LaR/ab;->a(LaR/P;)V

    :cond_2
    return-void
.end method

.method private static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 11

    const/4 v10, 0x4

    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v0, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, -0x1

    invoke-static {v2, v8, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ne v2, v8, :cond_0

    invoke-virtual {p0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-static {v5, v8}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x1

    invoke-direct {p0, p1, p4}, LaR/ac;->a(ILaR/O;)V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hi;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const-string v1, "0"

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hi;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hi;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, LaR/P;

    invoke-direct {v1, v0}, LaR/P;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v0, LaR/ae;

    invoke-direct {v0, p0, p3}, LaR/ae;-><init>(LaR/ac;LaR/p;)V

    invoke-virtual {v1, v0}, LaR/P;->a(LaR/Q;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(LaR/ab;)V
    .locals 1

    iget-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/ab;

    invoke-interface {v0, p1}, LaR/ab;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaR/ac;->d:LaR/ab;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaR/ac;->d:LaR/ab;

    invoke-interface {v0, p1}, LaR/ab;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    iget-object v0, p0, LaR/ac;->a:LaR/X;

    invoke-interface {v0, p1}, LaR/X;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaR/ac;->b:LaR/P;

    iget-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/ab;

    invoke-interface {v0}, LaR/ab;->F_()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, LaR/ac;->d:LaR/ab;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaR/ac;->d:LaR/ab;

    invoke-interface {v0}, LaR/ab;->F_()V

    :cond_3
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    .locals 2

    invoke-direct {p0, p2, p3}, LaR/ac;->a(ILaR/O;)V

    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, LaR/ac;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    new-instance v1, LaR/ad;

    invoke-direct {v1, p0}, LaR/ad;-><init>(LaR/ac;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, LaR/ac;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public b(LaR/ab;)V
    .locals 1

    iget-object v0, p0, LaR/ac;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, LaR/ac;->b:LaR/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/ac;->b:LaR/P;

    invoke-virtual {v0}, LaR/P;->Z()V

    const/4 v0, 0x0

    iput-object v0, p0, LaR/ac;->b:LaR/P;

    :cond_0
    return-void
.end method
