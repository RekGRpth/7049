.class public LaR/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/T;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaR/g;->b:Ljava/util/List;

    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/T;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, LaR/g;->a(I)LaR/V;

    move-result-object v0

    invoke-virtual {v0, p1}, LaR/V;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method private d(I)LaR/V;
    .locals 3

    new-instance v1, LaR/V;

    invoke-direct {v1, p1}, LaR/V;-><init>(I)V

    iget-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LaR/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/W;

    invoke-virtual {v1, v0}, LaR/V;->a(LaR/W;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private e(I)LaR/V;
    .locals 2

    iget-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/V;

    return-object v0
.end method


# virtual methods
.method public a(I)LaR/V;
    .locals 1

    invoke-direct {p0, p1}, LaR/g;->e(I)LaR/V;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, LaR/g;->d(I)LaR/V;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-direct {p0, p1}, LaR/g;->e(I)LaR/V;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, LaR/T;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(ILaR/U;)V
    .locals 1

    invoke-virtual {p0, p1}, LaR/g;->a(I)LaR/V;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LaR/T;->a(ILaR/U;)V

    return-void
.end method

.method public a(LaR/W;)V
    .locals 2

    iget-object v0, p0, LaR/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/V;

    invoke-virtual {v0, p1}, LaR/V;->a(LaR/W;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {p0, v2}, LaR/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/T;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-virtual {p0, p1}, LaR/g;->a(I)LaR/V;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LaR/T;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b(I)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, LaR/g;->e(I)LaR/V;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LaR/T;->b(I)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/V;

    invoke-virtual {v0}, LaR/V;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(ILjava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p1}, LaR/g;->a(I)LaR/V;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LaR/T;->b(ILjava/lang/String;)V

    return-void
.end method

.method public b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-virtual {p0, p1, p2}, LaR/g;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbM/j;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, LaR/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/V;

    const/4 v3, 0x1

    invoke-virtual {v0}, LaR/V;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public c(I)V
    .locals 1

    invoke-direct {p0, p1}, LaR/g;->e(I)LaR/V;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LaR/T;->c(I)V

    :cond_0
    return-void
.end method
