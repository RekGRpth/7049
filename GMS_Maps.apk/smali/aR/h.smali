.class public LaR/h;
.super LaR/t;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Lax/b;


# direct methods
.method private constructor <init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, LaR/t;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p0, p4, p5}, LaR/h;->b(J)V

    iput-object p8, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p6, p0, LaR/h;->d:Ljava/lang/String;

    iput-object p7, p0, LaR/h;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/h;
    .locals 9

    const/4 v6, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x4

    const-wide/16 v2, -0x1

    invoke-static {p0, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v2

    const/16 v0, 0xc

    const-wide/16 v4, 0x0

    invoke-static {p0, v0, v4, v5}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v4

    const/16 v0, 0xe

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    array-length v0, v8

    const/4 v7, 0x2

    if-ge v0, v7, :cond_0

    new-instance v0, LaR/h;

    move-object v7, v6

    invoke-direct/range {v0 .. v8}, LaR/h;-><init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    aget-object v0, v8, v0

    invoke-static {v0}, LaR/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v6

    array-length v0, v8

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v8, v0

    invoke-static {v0}, LaR/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v7

    new-instance v0, LaR/h;

    invoke-direct/range {v0 .. v8}, LaR/h;-><init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/16 v0, 0x3a6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()Lax/b;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, LaR/h;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lbm/b;

    const-string v2, "dirflg"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbm/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbm/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lax/x;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v3}, Lax/x;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lbm/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lax/w;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lax/w;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lbm/b;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lax/i;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v3}, Lax/i;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lax/s;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v3}, Lax/s;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    invoke-super {p0}, LaR/t;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const/16 v2, 0xe

    iget-object v3, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public b()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaR/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaR/h;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaR/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaR/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lax/b;
    .locals 1

    iget-object v0, p0, LaR/h;->g:Lax/b;

    if-nez v0, :cond_0

    invoke-direct {p0}, LaR/h;->f()Lax/b;

    move-result-object v0

    iput-object v0, p0, LaR/h;->g:Lax/b;

    :cond_0
    iget-object v0, p0, LaR/h;->g:Lax/b;

    return-object v0
.end method
