.class public abstract LaR/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/W;
.implements LaR/u;


# static fields
.field private static final f:Ljava/util/Comparator;

.field private static final g:Ljava/util/Comparator;


# instance fields
.field protected final a:I

.field protected final b:LaR/T;

.field private final c:Ljava/util/List;

.field private d:LaR/I;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaR/w;

    invoke-direct {v0}, LaR/w;-><init>()V

    sput-object v0, LaR/v;->f:Ljava/util/Comparator;

    new-instance v0, LaR/x;

    invoke-direct {v0}, LaR/x;-><init>()V

    sput-object v0, LaR/v;->g:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(LaR/T;LaR/I;IZLaR/U;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaR/v;->b:LaR/T;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {p1, p0}, LaR/T;->a(LaR/W;)V

    invoke-interface {p1, p3, p5}, LaR/T;->a(ILaR/U;)V

    iput-object p2, p0, LaR/v;->d:LaR/I;

    iput p3, p0, LaR/v;->a:I

    iput-boolean p4, p0, LaR/v;->e:Z

    return-void
.end method

.method public static a(ILaR/T;LaR/I;)LaR/v;
    .locals 2

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, LaR/M;

    invoke-direct {v0, p1, p2}, LaR/M;-><init>(LaR/T;LaR/I;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, LaR/k;

    new-instance v1, LaR/y;

    invoke-direct {v1}, LaR/y;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, LaR/k;

    new-instance v1, LaR/z;

    invoke-direct {v1}, LaR/z;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, LaR/k;

    new-instance v1, LaR/A;

    invoke-direct {v1}, LaR/A;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, LaR/k;

    new-instance v1, LaR/B;

    invoke-direct {v1}, LaR/B;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_0

    :pswitch_6
    new-instance v0, LaR/E;

    invoke-direct {v0, p1, p2}, LaR/E;-><init>(LaR/T;LaR/I;)V

    goto :goto_0

    :pswitch_7
    new-instance v0, LaR/i;

    invoke-direct {v0, p1, p2}, LaR/i;-><init>(LaR/T;LaR/I;)V

    goto :goto_0

    :pswitch_8
    new-instance v0, LaR/J;

    invoke-direct {v0, p1, p2}, LaR/J;-><init>(LaR/T;LaR/I;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 5

    const/4 v4, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hi;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v4, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaR/v;->b:LaR/T;

    iget v3, p0, LaR/v;->a:I

    invoke-interface {v2, v3, v1}, LaR/T;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {p0, v1, p1}, LaR/v;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-boolean v1, p0, LaR/v;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LaR/v;->b:LaR/T;

    iget v2, p0, LaR/v;->a:I

    invoke-interface {v1, v2, v0}, LaR/T;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, LaR/v;->b:LaR/T;

    iget v2, p0, LaR/v;->a:I

    invoke-interface {v1, v2, v0}, LaR/T;->b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/C;

    invoke-interface {v0, p1}, LaR/C;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LaR/t;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LaR/v;->b:LaR/T;

    iget v2, p0, LaR/v;->a:I

    invoke-interface {v1, v2, p1}, LaR/T;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, LaR/v;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/t;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 3

    iget-object v0, p0, LaR/v;->b:LaR/T;

    iget v1, p0, LaR/v;->a:I

    invoke-interface {v0, v1}, LaR/T;->b(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, v0}, LaR/v;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/t;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public a(LaR/C;)V
    .locals 1

    iget-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(LaR/H;Z)V
    .locals 1

    invoke-virtual {p1}, LaR/H;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, LaR/v;->a(Ljava/lang/String;LaR/H;Z)V

    invoke-direct {p0, v0}, LaR/v;->f(Ljava/lang/String;)V

    return-void
.end method

.method protected abstract a(LaR/t;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method a(Ljava/lang/String;LaR/H;Z)V
    .locals 4

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p2}, LaR/H;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p2}, LaR/H;->c()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p3, :cond_1

    const/4 v1, 0x5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v1, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v1, p1, v0}, LaR/I;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(LaR/t;)Z
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hi;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0, p1, v0}, LaR/v;->a(LaR/t;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x2

    invoke-virtual {p1}, LaR/t;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, LaR/v;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public a_(ILjava/lang/String;)V
    .locals 1

    iget v0, p0, LaR/v;->a:I

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, LaR/v;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)LaR/H;
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v1, p1}, LaR/I;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, LaR/H;

    invoke-direct {v0, v1}, LaR/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method protected abstract b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/t;
.end method

.method public b()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, LaR/v;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(LaR/C;)V
    .locals 1

    iget-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public c()Ljava/util/List;
    .locals 5

    invoke-virtual {p0}, LaR/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LaR/t;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    sget-object v2, LaR/v;->g:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v4}, LaR/t;->h()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v0, p1}, LaR/I;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, LaR/v;->b:LaR/T;

    iget v1, p0, LaR/v;->a:I

    invoke-interface {v0, v1}, LaR/T;->c(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaR/v;->f(Ljava/lang/String;)V

    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v0, p1}, LaR/I;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaR/v;->b:LaR/T;

    iget v1, p0, LaR/v;->a:I

    invoke-interface {v0, v1, p1}, LaR/T;->b(ILjava/lang/String;)V

    invoke-direct {p0, p1}, LaR/v;->f(Ljava/lang/String;)V

    return-void
.end method
