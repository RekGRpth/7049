.class LaT/c;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:LaT/a;


# direct methods
.method constructor <init>(LaT/a;Las/c;)V
    .locals 0

    iput-object p1, p0, LaT/c;->a:LaT/a;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    iget-object v2, p0, LaT/c;->a:LaT/a;

    monitor-enter v2

    :try_start_0
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbM/r;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->b(LaT/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/f;

    const/4 v4, 0x1

    invoke-static {v0}, LaT/f;->a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v0, v1

    const/4 v6, 0x1

    invoke-static {v5}, LaT/f;->a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->a(LaT/a;)LaT/f;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->a(LaT/a;)LaT/f;

    move-result-object v1

    invoke-static {v1}, LaT/f;->a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->d(LaT/a;)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->e(LaT/a;)Lo/aq;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->e(LaT/a;)Lo/aq;

    move-result-object v1

    invoke-virtual {v1}, Lo/aq;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    const/4 v0, 0x4

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->f(LaT/a;)J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    :try_start_2
    invoke-virtual {v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    sget-object v2, LaT/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2
.end method
