.class public LaT/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/android/x;


# instance fields
.field private a:LaT/j;

.field private b:Ljava/util/TimerTask;

.field private final c:Ljava/lang/Object;

.field private d:J

.field private e:LaN/B;

.field private f:LaN/B;

.field private g:F

.field private h:Z

.field private volatile i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaT/h;->c:Ljava/lang/Object;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaT/h;->d:J

    iput-object v2, p0, LaT/h;->e:LaN/B;

    iput-object v2, p0, LaT/h;->f:LaN/B;

    const/high16 v0, -0x40800000

    iput v0, p0, LaT/h;->g:F

    const/4 v0, 0x1

    iput-boolean v0, p0, LaT/h;->h:Z

    return-void
.end method

.method static synthetic a(LaT/h;)J
    .locals 2

    iget-wide v0, p0, LaT/h;->d:J

    return-wide v0
.end method

.method static synthetic a(LaT/h;LaT/j;)LaT/j;
    .locals 0

    iput-object p1, p0, LaT/h;->a:LaT/j;

    return-object p1
.end method

.method static synthetic b(LaT/h;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LaT/h;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(LaT/h;)I
    .locals 1

    iget v0, p0, LaT/h;->i:I

    return v0
.end method


# virtual methods
.method a()V
    .locals 6

    iget-object v0, p0, LaT/h;->b:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LaT/h;->b:Ljava/util/TimerTask;

    iget-object v1, p0, LaT/h;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, LaT/h;->d:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, LaT/h;->d:J

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(LaN/B;LaN/B;IFLcom/google/googlenav/ui/android/w;)V
    .locals 8

    const/4 v5, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaT/h;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, LaT/h;->g:F

    cmpl-float v0, v0, p4

    if-nez v0, :cond_2

    iget-object v0, p0, LaT/h;->e:LaN/B;

    invoke-virtual {p1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaT/h;->f:LaN/B;

    invoke-virtual {p2, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iput p4, p0, LaT/h;->g:F

    iput-object p1, p0, LaT/h;->e:LaN/B;

    iput-object p2, p0, LaT/h;->f:LaN/B;

    invoke-virtual {p0}, LaT/h;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p5, Lcom/google/googlenav/ui/android/w;->i:Z

    const/16 v0, 0x10

    if-le p3, v0, :cond_3

    const/16 v0, 0x34c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/ui/android/w;->d:[F

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->g:[F

    const/4 v0, -0x1

    iput v0, p5, Lcom/google/googlenav/ui/android/w;->f:I

    sget-object v0, Lcom/google/googlenav/ui/android/w;->a:[F

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->h:[F

    const/4 v0, 0x1

    iput-boolean v0, p5, Lcom/google/googlenav/ui/android/w;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    new-instance v3, Lcom/google/googlenav/prefetch/android/k;

    invoke-direct {v3, p1, p2}, Lcom/google/googlenav/prefetch/android/k;-><init>(LaN/B;LaN/B;)V

    invoke-virtual {v3}, Lcom/google/googlenav/prefetch/android/k;->e()I

    move-result v0

    const/16 v1, 0x1770

    if-le v0, v1, :cond_4

    const/16 v0, 0x34b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/ui/android/w;->d:[F

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->g:[F

    const/4 v0, -0x1

    iput v0, p5, Lcom/google/googlenav/ui/android/w;->f:I

    sget-object v0, Lcom/google/googlenav/ui/android/w;->a:[F

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->h:[F

    const/4 v0, 0x1

    iput-boolean v0, p5, Lcom/google/googlenav/ui/android/w;->j:Z

    goto :goto_0

    :cond_4
    mul-int/lit8 v0, v0, 0xf

    div-int/lit16 v0, v0, 0x400

    iput v0, p0, LaT/h;->i:I

    const/16 v0, 0x33d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p5, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/android/w;->b:[F

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->h:[F

    sget-object v0, Lcom/google/googlenav/ui/android/w;->c:[F

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->g:[F

    const/high16 v0, -0x1000000

    iput v0, p5, Lcom/google/googlenav/ui/android/w;->f:I

    const/16 v0, 0x338

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget v0, p0, LaT/h;->i:I

    if-ge v0, v5, :cond_6

    const-string v0, "<1"

    :goto_1
    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p5, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p5, Lcom/google/googlenav/ui/android/w;->j:Z

    :cond_5
    iget-object v7, p0, LaT/h;->c:Ljava/lang/Object;

    monitor-enter v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-wide v4, p0, LaT/h;->d:J

    new-instance v0, LaT/i;

    move-object v1, p0

    move v2, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LaT/i;-><init>(LaT/h;FLcom/google/googlenav/prefetch/android/k;JLcom/google/googlenav/ui/android/w;)V

    iput-object v0, p0, LaT/h;->b:Ljava/util/TimerTask;

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iget-object v1, p0, LaT/h;->b:Ljava/util/TimerTask;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    monitor-exit v7

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    :cond_6
    iget v0, p0, LaT/h;->i:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaT/h;->h:Z

    invoke-virtual {p0}, LaT/h;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
