.class LaT/i;
.super Ljava/util/TimerTask;
.source "SourceFile"


# instance fields
.field final synthetic a:F

.field final synthetic b:Lcom/google/googlenav/prefetch/android/k;

.field final synthetic c:J

.field final synthetic d:Lcom/google/googlenav/ui/android/w;

.field final synthetic e:LaT/h;


# direct methods
.method constructor <init>(LaT/h;FLcom/google/googlenav/prefetch/android/k;JLcom/google/googlenav/ui/android/w;)V
    .locals 0

    iput-object p1, p0, LaT/i;->e:LaT/h;

    iput p2, p0, LaT/i;->a:F

    iput-object p3, p0, LaT/i;->b:Lcom/google/googlenav/prefetch/android/k;

    iput-wide p4, p0, LaT/i;->c:J

    iput-object p6, p0, LaT/i;->d:Lcom/google/googlenav/ui/android/w;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/16 v0, 0xe

    const/4 v1, 0x5

    iget v2, p0, LaT/i;->a:F

    float-to-int v2, v2

    if-le v2, v0, :cond_1

    :goto_0
    iget-object v1, p0, LaT/i;->b:Lcom/google/googlenav/prefetch/android/k;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    :goto_1
    iget-object v1, p0, LaT/i;->b:Lcom/google/googlenav/prefetch/android/k;

    invoke-virtual {v1}, Lcom/google/googlenav/prefetch/android/k;->c()Lo/aq;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v2

    if-eq v2, v0, :cond_2

    :cond_0
    iget-object v6, p0, LaT/i;->e:LaT/h;

    new-instance v0, LaT/j;

    iget-object v1, p0, LaT/i;->e:LaT/h;

    iget-wide v2, p0, LaT/i;->c:J

    iget-object v5, p0, LaT/i;->d:Lcom/google/googlenav/ui/android/w;

    invoke-direct/range {v0 .. v5}, LaT/j;-><init>(LaT/h;JLjava/util/List;Lcom/google/googlenav/ui/android/w;)V

    invoke-static {v6, v0}, LaT/h;->a(LaT/h;LaT/j;)LaT/j;

    return-void

    :cond_1
    if-ge v2, v1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method
