.class LaT/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# instance fields
.field final synthetic a:LaT/h;

.field final synthetic b:J

.field final synthetic c:Lcom/google/googlenav/ui/android/w;

.field final synthetic d:LaT/j;


# direct methods
.method constructor <init>(LaT/j;LaT/h;JLcom/google/googlenav/ui/android/w;)V
    .locals 0

    iput-object p1, p0, LaT/k;->d:LaT/j;

    iput-object p2, p0, LaT/k;->a:LaT/h;

    iput-wide p3, p0, LaT/k;->b:J

    iput-object p5, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lo/aq;ILo/ap;)V
    .locals 8

    const/4 v7, 0x1

    iget-object v1, p0, LaT/k;->d:LaT/j;

    invoke-static {v1}, LaT/j;->a(LaT/j;)I

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    iget-object v1, p0, LaT/k;->d:LaT/j;

    iget-object v1, v1, LaT/j;->a:LaT/h;

    invoke-static {v1}, LaT/h;->a(LaT/h;)J

    move-result-wide v1

    iget-wide v3, p0, LaT/k;->b:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LaT/k;->d:LaT/j;

    iget-object v1, v1, LaT/j;->a:LaT/h;

    invoke-static {v1}, LaT/h;->b(LaT/h;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, LaT/k;->d:LaT/j;

    iget-object v1, v1, LaT/j;->a:LaT/h;

    invoke-static {v1}, LaT/h;->a(LaT/h;)J

    move-result-wide v3

    iget-wide v5, p0, LaT/k;->b:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_2

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    iget-object v1, p0, LaT/k;->d:LaT/j;

    invoke-static {v1}, LaT/j;->b(LaT/j;)I

    instance-of v1, p3, Lo/aL;

    if-eqz v1, :cond_3

    move-object v0, p3

    check-cast v0, Lo/aL;

    move-object v1, v0

    invoke-virtual {v1}, Lo/aL;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LaT/k;->d:LaT/j;

    const/4 v3, 0x1

    invoke-static {v1, v3}, LaT/j;->a(LaT/j;Z)Z

    :cond_3
    if-eqz p3, :cond_4

    invoke-interface {p3}, Lo/ap;->i()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    sget-object v3, Lcom/google/googlenav/ui/android/w;->b:[F

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->h:[F

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    sget-object v3, Lcom/google/googlenav/ui/android/w;->c:[F

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->g:[F

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/high16 v3, -0x1000000

    iput v3, v1, Lcom/google/googlenav/ui/android/w;->f:I

    iget-object v3, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/16 v1, 0x338

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v1, p0, LaT/k;->d:LaT/j;

    iget-object v1, v1, LaT/j;->a:LaT/h;

    invoke-static {v1}, LaT/h;->c(LaT/h;)I

    move-result v1

    if-ge v1, v7, :cond_6

    const-string v1, "<1"

    :goto_1
    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/googlenav/ui/android/w;->j:Z

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/googlenav/ui/android/w;->i:Z

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/w;->a()V

    :cond_4
    iget-object v1, p0, LaT/k;->d:LaT/j;

    invoke-static {v1}, LaT/j;->a(LaT/j;)I

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    iget-boolean v1, v1, Lcom/google/googlenav/ui/android/w;->i:Z

    if-nez v1, :cond_7

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/16 v3, 0x33d

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    sget-object v3, Lcom/google/googlenav/ui/android/w;->d:[F

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->g:[F

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/4 v3, -0x1

    iput v3, v1, Lcom/google/googlenav/ui/android/w;->f:I

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    sget-object v3, Lcom/google/googlenav/ui/android/w;->a:[F

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->h:[F

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/googlenav/ui/android/w;->j:Z

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/w;->a()V

    :cond_5
    :goto_2
    monitor-exit v2

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, LaT/k;->d:LaT/j;

    iget-object v1, v1, LaT/j;->a:LaT/h;

    invoke-static {v1}, LaT/h;->c(LaT/h;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_7
    iget-object v1, p0, LaT/k;->d:LaT/j;

    invoke-static {v1}, LaT/j;->a(LaT/j;)I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, LaT/k;->d:LaT/j;

    invoke-static {v1}, LaT/j;->c(LaT/j;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    sget-object v3, Lcom/google/googlenav/ui/android/w;->b:[F

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->h:[F

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    sget-object v3, Lcom/google/googlenav/ui/android/w;->c:[F

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->g:[F

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/high16 v3, -0x1000000

    iput v3, v1, Lcom/google/googlenav/ui/android/w;->f:I

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/googlenav/ui/android/w;->i:Z

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/16 v3, 0x33f

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/googlenav/ui/android/w;->j:Z

    iget-object v1, p0, LaT/k;->c:Lcom/google/googlenav/ui/android/w;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/w;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
