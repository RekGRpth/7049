.class LaV/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private b:F

.field private c:F


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaV/d;->a:J

    return-void
.end method

.method synthetic constructor <init>(LaV/b;)V
    .locals 0

    invoke-direct {p0}, LaV/d;-><init>()V

    return-void
.end method

.method private a(JFFLaV/j;)Z
    .locals 6

    const/4 v3, 0x1

    iget-wide v0, p0, LaV/d;->a:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_0

    iput-wide p1, p0, LaV/d;->a:J

    iput p3, p0, LaV/d;->b:F

    iput p4, p0, LaV/d;->c:F

    :cond_0
    return v0

    :cond_1
    sget-object v0, LaV/b;->b:[I

    invoke-virtual {p5}, LaV/j;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-wide/16 v1, 0x64

    const/high16 v0, 0x40000000

    :goto_1
    iget-wide v4, p0, LaV/d;->a:J

    sub-long v4, p1, v4

    cmp-long v1, v4, v1

    if-gtz v1, :cond_2

    iget v1, p0, LaV/d;->b:F

    sub-float v1, p3, v1

    invoke-static {v1}, Lcom/google/googlenav/common/util/j;->c(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_2

    iget v1, p0, LaV/d;->c:F

    sub-float v1, p4, v1

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const-wide/16 v1, 0x32

    const/high16 v0, 0x3f800000

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(LaV/d;JFFLaV/j;)Z
    .locals 1

    invoke-direct/range {p0 .. p5}, LaV/d;->a(JFFLaV/j;)Z

    move-result v0

    return v0
.end method
