.class public final enum LaV/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaV/j;

.field public static final enum b:LaV/j;

.field public static final enum c:LaV/j;

.field private static final synthetic e:[LaV/j;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, LaV/j;

    const-string v1, "UPDATE_FREQUENCY_NONE"

    invoke-direct {v0, v1, v3, v3}, LaV/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaV/j;->a:LaV/j;

    new-instance v0, LaV/j;

    const-string v1, "UPDATE_FREQUENCY_SLOW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v4, v2}, LaV/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaV/j;->b:LaV/j;

    new-instance v0, LaV/j;

    const-string v1, "UPDATE_FREQUENCY_FAST"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v5, v2}, LaV/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaV/j;->c:LaV/j;

    const/4 v0, 0x3

    new-array v0, v0, [LaV/j;

    sget-object v1, LaV/j;->a:LaV/j;

    aput-object v1, v0, v3

    sget-object v1, LaV/j;->b:LaV/j;

    aput-object v1, v0, v4

    sget-object v1, LaV/j;->c:LaV/j;

    aput-object v1, v0, v5

    sput-object v0, LaV/j;->e:[LaV/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, LaV/j;->d:I

    return-void

    :cond_0
    const v0, 0xf4240

    div-int/2addr v0, p3

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LaV/j;
    .locals 1

    const-class v0, LaV/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaV/j;

    return-object v0
.end method

.method public static values()[LaV/j;
    .locals 1

    sget-object v0, LaV/j;->e:[LaV/j;

    invoke-virtual {v0}, [LaV/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaV/j;

    return-object v0
.end method
