.class public abstract enum LaW/H;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaW/H;

.field public static final enum b:LaW/H;

.field public static final enum c:LaW/H;

.field public static final enum d:LaW/H;

.field public static final enum e:LaW/H;

.field private static final synthetic i:[LaW/H;


# instance fields
.field protected f:I

.field private final g:I

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-instance v0, LaW/I;

    const-string v1, "RATE_AND_REVIEW"

    const v4, 0x7f020373

    const/16 v5, 0x3bb

    move v3, v2

    invoke-direct/range {v0 .. v5}, LaW/I;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LaW/H;->a:LaW/H;

    new-instance v3, LaW/J;

    const-string v4, "CHECKIN_OR_CHECKOUT"

    const v7, 0x7f0200b0

    const/16 v8, 0x3b6

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, LaW/J;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LaW/H;->b:LaW/H;

    new-instance v3, LaW/K;

    const-string v4, "UPLOAD_PHOTO"

    const v7, 0x7f0200cd

    const/16 v8, 0x3c9

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, LaW/K;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LaW/H;->c:LaW/H;

    new-instance v3, LaW/L;

    const-string v4, "PLACE_PAGE"

    const v7, 0x7f020281

    const/16 v8, 0x3ba

    move v5, v11

    move v6, v11

    invoke-direct/range {v3 .. v8}, LaW/L;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LaW/H;->d:LaW/H;

    new-instance v3, LaW/M;

    const-string v4, "LOCATION_SELECTOR"

    const/4 v7, -0x1

    const/16 v8, 0x3b8

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, LaW/M;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LaW/H;->e:LaW/H;

    const/4 v0, 0x5

    new-array v0, v0, [LaW/H;

    sget-object v1, LaW/H;->a:LaW/H;

    aput-object v1, v0, v2

    sget-object v1, LaW/H;->b:LaW/H;

    aput-object v1, v0, v9

    sget-object v1, LaW/H;->c:LaW/H;

    aput-object v1, v0, v10

    sget-object v1, LaW/H;->d:LaW/H;

    aput-object v1, v0, v11

    sget-object v1, LaW/H;->e:LaW/H;

    aput-object v1, v0, v12

    sput-object v0, LaW/H;->i:[LaW/H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LaW/H;->g:I

    iput p4, p0, LaW/H;->h:I

    iput p5, p0, LaW/H;->f:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIIILaW/G;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, LaW/H;-><init>(Ljava/lang/String;IIII)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaW/H;
    .locals 1

    const-class v0, LaW/H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaW/H;

    return-object v0
.end method

.method public static values()[LaW/H;
    .locals 1

    sget-object v0, LaW/H;->i:[LaW/H;

    invoke-virtual {v0}, [LaW/H;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaW/H;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/CharSequence;
    .locals 1

    iget v0, p0, LaW/H;->f:I

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()I
    .locals 1

    iget v0, p0, LaW/H;->h:I

    return v0
.end method

.method public abstract c()V
.end method
