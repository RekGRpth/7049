.class public LaW/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/util/List;

.field private c:Landroid/widget/LinearLayout;

.field private d:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaW/a;->b:Ljava/util/List;

    invoke-static {}, LaW/a;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LaW/a;->d:Ljava/util/Map;

    iput-object p1, p0, LaW/a;->a:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/F;)I
    .locals 1

    iget-object v0, p0, LaW/a;->d:Ljava/util/Map;

    invoke-static {v0, p1}, LaW/a;->a(Ljava/util/Map;Lcom/google/googlenav/ui/wizard/F;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Map;Lcom/google/googlenav/ui/wizard/F;)I
    .locals 1

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const v0, 0x7f020300

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a()Ljava/util/Map;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/16 v1, 0x38f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0202f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x390

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0202f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x391

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0202f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x392

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0202fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x394

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020304

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x396

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020308

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x397

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f02030c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x39a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020310

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x39b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020314

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ui/wizard/gj;)V
    .locals 1

    iget-object v0, p0, LaW/a;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaW/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, LaW/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-direct {p0, p1}, LaW/a;->b(Lcom/google/googlenav/ui/wizard/gj;)V

    invoke-direct {p0}, LaW/a;->b()V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 7

    const/4 v4, 0x0

    const/4 v2, 0x0

    move v3, v2

    move-object v0, v4

    move v5, v2

    :goto_0
    iget-object v1, p0, LaW/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    if-nez v5, :cond_0

    const v0, 0x7f04011f

    invoke-static {v0, v4, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, LaW/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    move-object v1, v0

    iget-object v0, p0, LaW/a;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/d;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v0, v6}, LaW/d;->a(Landroid/view/View;)Landroid/view/View;

    add-int/lit8 v0, v5, 0x1

    const/4 v5, 0x4

    if-ne v0, v5, :cond_1

    move v0, v2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    move v5, v0

    move-object v0, v1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private b(Lcom/google/googlenav/ui/wizard/gj;)V
    .locals 8

    new-instance v5, LaW/b;

    invoke-direct {v5, p0, p1}, LaW/b;-><init>(LaW/a;Lcom/google/googlenav/ui/wizard/gj;)V

    new-instance v6, LaW/c;

    invoke-direct {v6, p0, p1}, LaW/c;-><init>(LaW/a;Lcom/google/googlenav/ui/wizard/gj;)V

    iget-object v0, p0, LaW/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/googlenav/ui/wizard/F;

    invoke-direct {p0, v4}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/F;)I

    move-result v0

    const v1, 0x7f020300

    if-ne v0, v1, :cond_0

    iget-object v0, v4, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    :goto_1
    new-instance v0, LaW/d;

    invoke-direct {p0, v4}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/F;)I

    move-result v1

    iget-object v3, v4, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, LaW/d;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/F;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, LaW/a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/wizard/gj;Landroid/view/View;)V
    .locals 1

    const v0, 0x7f10033a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LaW/a;->c:Landroid/widget/LinearLayout;

    invoke-direct {p0, p1}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/gj;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gj;Ljava/util/List;)V
    .locals 0

    iput-object p2, p0, LaW/a;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/gj;)V

    return-void
.end method
