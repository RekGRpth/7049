.class public LaW/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private final f:Lcom/google/googlenav/ui/wizard/gj;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/gj;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaW/y;->a:Ljava/util/List;

    iput-object p2, p0, LaW/y;->b:Ljava/lang/String;

    iput-object p3, p0, LaW/y;->f:Lcom/google/googlenav/ui/wizard/gj;

    return-void
.end method

.method private a(LaW/R;LaW/R;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v0, 0x7f040128

    invoke-static {v0, p4, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10032f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f100331

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v1, p1, p3}, LaW/R;->a(Landroid/view/View;LaW/R;Landroid/view/View$OnClickListener;)V

    invoke-static {v2, p2, p3}, LaW/R;->a(Landroid/view/View;LaW/R;Landroid/view/View$OnClickListener;)V

    if-eqz p5, :cond_0

    invoke-virtual {v1, v3, v3, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v2, v3, v3, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v3, v3, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v2, v3, v3, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method static synthetic a(LaW/y;)Lcom/google/googlenav/ui/wizard/gj;
    .locals 1

    iget-object v0, p0, LaW/y;->f:Lcom/google/googlenav/ui/wizard/gj;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaW/y;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f100326

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaW/y;->d:Landroid/widget/TextView;

    iget-object v0, p0, LaW/y;->d:Landroid/widget/TextView;

    const/16 v1, 0x3af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100327

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/y;->e:Landroid/view/View;

    const v0, 0x7f100328

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/PlacesRefinementLinearLayout;

    iput-object v0, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    iget-object v0, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    iget-object v1, p0, LaW/y;->f:Lcom/google/googlenav/ui/wizard/gj;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->setPlacesWizardActions(Lcom/google/googlenav/ui/wizard/gj;)V

    iget-object v0, p0, LaW/y;->a:Ljava/util/List;

    iget-object v1, p0, LaW/y;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LaW/y;->a(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 11

    const/16 v10, 0xa

    const/4 v8, 0x1

    const/16 v1, 0x8

    const/4 v7, 0x0

    iput-object p1, p0, LaW/y;->a:Ljava/util/List;

    iput-object p2, p0, LaW/y;->b:Ljava/lang/String;

    iget-object v0, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->removeAllViews()V

    iget-object v0, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LaW/y;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, LaW/y;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaW/y;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, LaW/y;->e:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    invoke-virtual {v0, v7}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->setVisibility(I)V

    new-instance v3, LaW/z;

    invoke-direct {v3, p0}, LaW/z;-><init>(LaW/y;)V

    move v5, v7

    move v6, v8

    :goto_1
    if-ge v5, v10, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_4

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaW/R;

    invoke-virtual {v1}, LaW/R;->e()Z

    move-result v0

    and-int v4, v6, v0

    const/4 v2, 0x0

    add-int/lit8 v0, v5, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    add-int/lit8 v0, v5, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/R;

    invoke-virtual {v0}, LaW/R;->e()Z

    move-result v2

    and-int/2addr v2, v4

    move v6, v2

    move-object v2, v0

    :goto_2
    add-int/lit8 v9, v5, 0x2

    iget-object v4, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    if-ge v9, v10, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v9, v0, :cond_3

    :cond_2
    move v5, v8

    :goto_3
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LaW/y;->a(LaW/R;LaW/R;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LaW/y;->c:Lcom/google/android/maps/PlacesRefinementLinearLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->addView(Landroid/view/View;)V

    move v5, v9

    goto :goto_1

    :cond_3
    move v5, v7

    goto :goto_3

    :cond_4
    if-eqz v6, :cond_5

    iget-object v0, p0, LaW/y;->d:Landroid/widget/TextView;

    const/16 v1, 0x3b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, LaW/y;->d:Landroid/widget/TextView;

    const/16 v1, 0x3af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    move v6, v4

    goto :goto_2
.end method
