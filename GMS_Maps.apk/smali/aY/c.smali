.class public LaY/c;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# static fields
.field private static final n:Lcom/google/common/collect/ax;

.field private static final o:Lcom/google/common/collect/ax;

.field private static final p:Lcom/google/common/collect/ax;

.field private static final q:Lcom/google/common/collect/ax;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/EditText;

.field private C:Landroid/widget/EditText;

.field private D:Landroid/widget/TextView;

.field private E:Ljava/util/Map;

.field private F:Landroid/view/MenuItem;

.field private G:Landroid/widget/Button;

.field private H:Landroid/view/MenuItem;

.field private I:Landroid/widget/Button;

.field private J:Lcom/google/googlenav/ui/br;

.field private K:Ljava/util/List;

.field private L:Ljava/lang/String;

.field private final M:Lcom/google/googlenav/ui/wizard/hb;

.field a:Landroid/view/MenuItem;

.field b:Landroid/widget/Button;

.field public c:Landroid/app/AlertDialog;

.field final d:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private final l:Lcom/google/googlenav/ui/wizard/hh;

.field private final m:Lcom/google/googlenav/ai;

.field private r:Landroid/view/View;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/ImageView;

.field private w:Landroid/view/ViewGroup;

.field private x:Landroid/view/ViewGroup;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const v7, 0x7f100394

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    const-string v1, "0"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const-string v1, "1"

    const v2, 0x7f100395

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const-string v1, "2"

    const v2, 0x7f100396

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const-string v1, "3"

    const v2, 0x7f100397

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->n:Lcom/google/common/collect/ax;

    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->o:Lcom/google/common/collect/ax;

    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x41b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const v1, 0x7f100395

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x41a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const v1, 0x7f100396

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x41c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const v1, 0x7f100397

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x419

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->p:Lcom/google/common/collect/ax;

    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f02009e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->q:Lcom/google/common/collect/ax;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hh;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;Lcom/google/googlenav/ui/wizard/hb;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaY/c;->K:Ljava/util/List;

    new-instance v0, LaY/d;

    invoke-direct {v0, p0}, LaY/d;-><init>(LaY/c;)V

    iput-object v0, p0, LaY/c;->d:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    iput-object p1, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    iput-object p2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    iput-object p4, p0, LaY/c;->M:Lcom/google/googlenav/ui/wizard/hb;

    return-void

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method

.method private A()V
    .locals 3

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v0

    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-virtual {v1, v0}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, LaY/f;

    invoke-direct {v2, p0, v1, v0}, LaY/f;-><init>(LaY/c;LaB/s;Lcom/google/googlenav/ui/bs;)V

    invoke-virtual {v1, v0, v2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    iget-object v1, p0, LaY/c;->v:Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Ljava/lang/String;)Landroid/widget/RadioButton;
    .locals 1

    sget-object v0, LaY/c;->n:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p2}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LaY/c;)Lcom/google/googlenav/ui/wizard/hh;
    .locals 1

    iget-object v0, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    return-object v0
.end method

.method static synthetic a(LaY/c;Landroid/widget/RadioGroup;)V
    .locals 0

    invoke-direct {p0, p1}, LaY/c;->a(Landroid/widget/RadioGroup;)V

    return-void
.end method

.method private a(Landroid/widget/RadioGroup;)V
    .locals 3

    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f10039f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, LaY/c;->a(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, LaY/c;->E:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/cy;Z)V
    .locals 6

    iget-boolean v0, p1, Lcom/google/googlenav/cy;->e:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04014c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f100392

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100394

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const v1, 0x7f100395

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const v2, 0x7f100396

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    const v3, 0x7f100397

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    const/16 v5, 0x41b

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x41a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x41c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x419

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    invoke-direct {p0, v4, v0}, LaY/c;->a(Landroid/view/View;Ljava/lang/String;)Landroid/widget/RadioButton;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 2

    iget-object v1, p0, LaY/c;->r:Landroid/view/View;

    sget-object v0, LaY/c;->o:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p1, p3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-void

    :cond_0
    sget-object v1, LaY/c;->q:Lcom/google/common/collect/ax;

    invoke-virtual {v1, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x55

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    if-lez v0, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "p="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, LaY/c;->L:Ljava/lang/String;

    if-eqz v4, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, LaY/c;->L:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    aput-object v1, v3, v0

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, p1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaY/c;->y:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->z:Landroid/widget/TextView;

    const/16 v1, 0x3f3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->A:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, LaY/c;->B:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LaY/c;->y:Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->z:Landroid/widget/TextView;

    const/16 v1, 0x3f3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, v0, p2}, LaY/c;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iget-object v2, p0, LaY/c;->d:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    invoke-direct {p0, v1}, LaY/c;->a(Landroid/widget/RadioGroup;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ay;)Landroid/view/View;
    .locals 6

    const v5, 0x7f0b00ac

    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040152

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f1003ba

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/googlenav/ay;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v1

    :goto_0
    invoke-virtual {p0}, LaY/c;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    check-cast v1, Lan/f;

    invoke-virtual {v1}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lbm/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v5, 0x0

    invoke-static {v1, v4, v3, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-object v2

    :cond_0
    iget-object v1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    invoke-virtual {p1}, Lcom/google/googlenav/ay;->b()Lcom/google/googlenav/ui/bs;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic b(LaY/c;)Lcom/google/googlenav/ui/wizard/hb;
    .locals 1

    iget-object v0, p0, LaY/c;->M:Lcom/google/googlenav/ui/wizard/hb;

    return-object v0
.end method

.method private b(Landroid/view/View;)Ljava/lang/String;
    .locals 3

    sget-object v0, LaY/c;->n:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->x_()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, LaY/c;->n:Lcom/google/common/collect/ax;

    invoke-virtual {v1, v0}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f10004a

    const v1, 0x7f0201a7

    invoke-virtual {p0, p2, v0, v1}, LaY/c;->a(Ljava/lang/CharSequence;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    const v1, 0x7f1003a5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x422

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    const v1, 0x7f1000fc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v1, LaY/m;

    invoke-direct {v1, p0}, LaY/m;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private c(Landroid/view/View;)Ljava/lang/String;
    .locals 3

    sget-object v0, LaY/c;->p:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->x_()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LaY/c;->p:Lcom/google/common/collect/ax;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(LaY/c;)V
    .locals 0

    invoke-direct {p0}, LaY/c;->w()V

    return-void
.end method

.method static synthetic d(LaY/c;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, LaY/c;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic e(LaY/c;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, LaY/c;->v:Landroid/widget/ImageView;

    return-object v0
.end method

.method private l()V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v0, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cy;

    add-int/lit8 v3, v1, 0x1

    if-ne v3, v4, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v0, v1}, LaY/c;->a(Lcom/google/googlenav/cy;Z)V

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method private m()V
    .locals 4

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f100398

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaY/c;->x:Landroid/view/ViewGroup;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f100399

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->y:Landroid/widget/TextView;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->z:Landroid/widget/TextView;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->A:Landroid/widget/TextView;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LaY/c;->B:Landroid/widget/EditText;

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cu;->i()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v1, :cond_1

    invoke-direct {p0, v1, v2, v0}, LaY/c;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->h()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LaY/c;->E:Ljava/util/Map;

    iget-object v1, p0, LaY/c;->E:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LaY/c;->E:Ljava/util/Map;

    invoke-direct {p0, v1, v3, v0}, LaY/c;->a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method private n()V
    .locals 2

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x422

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LaY/c;->C:Landroid/widget/EditText;

    iget-object v0, p0, LaY/c;->C:Landroid/widget/EditText;

    const/16 v1, 0x421

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->C:Landroid/widget/EditText;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private o()V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040150

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003ac

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    new-instance v2, LaY/i;

    invoke-direct {v2, p0}, LaY/i;-><init>(LaY/c;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_1
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LaY/c;->b:Landroid/widget/Button;

    iget-object v0, p0, LaY/c;->b:Landroid/widget/Button;

    new-instance v1, LaY/j;

    invoke-direct {v1, p0}, LaY/j;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    iget-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    new-instance v1, LaY/k;

    invoke-direct {v1, p0}, LaY/k;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method private v()Z
    .locals 4

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v3, 0x7f100393

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private w()V
    .locals 5

    invoke-direct {p0}, LaY/c;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LaY/c;->x()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "s"

    invoke-direct {p0, v0}, LaY/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LaY/c;->h()Lcom/google/googlenav/cx;

    move-result-object v0

    iget-object v1, p0, LaY/c;->C:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaY/c;->B:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, LaY/c;->a(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private x()V
    .locals 4

    const/16 v0, 0x418

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x35d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, LaY/l;

    invoke-direct {v2, p0}, LaY/l;-><init>(LaY/c;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, LaY/c;->c:Landroid/app/AlertDialog;

    iget-object v0, p0, LaY/c;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private y()V
    .locals 10

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003c0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, LaY/c;->r:Landroid/view/View;

    const v3, 0x7f1003bf

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    invoke-virtual {v1, v5, v3}, Landroid/widget/LinearLayout;->removeViews(II)V

    move v4, v5

    :goto_0
    iget-object v3, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_0

    iget-object v3, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/googlenav/ay;

    invoke-direct {p0, v3}, LaY/c;->b(Lcom/google/googlenav/ay;)Landroid/view/View;

    move-result-object v7

    new-instance v8, LaY/n;

    invoke-direct {v8, p0, v3}, LaY/n;-><init>(LaY/c;Lcom/google/googlenav/ay;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const v8, 0x7f1003bb

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    new-instance v9, LaY/e;

    invoke-direct {v9, p0, v3}, LaY/e;-><init>(LaY/c;Lcom/google/googlenav/ay;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    const/16 v1, 0x42

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    iget-object v1, p0, LaY/c;->F:Landroid/view/MenuItem;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v6

    :goto_2
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_3
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_2
    return-void

    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v0, v5

    goto :goto_2

    :cond_5
    move v6, v5

    goto :goto_3
.end method

.method private z()Z
    .locals 2

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public J_()V
    .locals 0

    invoke-direct {p0}, LaY/c;->y()V

    return-void
.end method

.method protected a(Landroid/view/View;)Ljava/lang/Integer;
    .locals 3

    sget-object v0, LaY/c;->o:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    const/16 v0, 0x404

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ay;)V
    .locals 0

    invoke-direct {p0}, LaY/c;->y()V

    return-void
.end method

.method public a(Lcom/google/googlenav/cx;)V
    .locals 1

    iget-object v0, p1, Lcom/google/googlenav/cx;->b:Lcom/google/common/collect/ImmutableList;

    iput-object v0, p0, LaY/c;->K:Ljava/util/List;

    iget-object v0, p1, Lcom/google/googlenav/cx;->d:Ljava/lang/String;

    iput-object v0, p0, LaY/c;->L:Ljava/lang/String;

    invoke-direct {p0}, LaY/c;->l()V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/br;)V
    .locals 0

    iput-object p1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1003ad

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, LaY/c;->w()V

    :goto_0
    return v0

    :cond_0
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1003ac

    if-ne v1, v2, :cond_1

    iget-object v1, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/hh;->e()V

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1003ae

    if-ne v1, v2, :cond_2

    iget-object v1, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/hh;->f()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v2, p0, LaY/c;->F:Landroid/view/MenuItem;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    iget-object v2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->b()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    return v1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04014e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaY/c;->r:Landroid/view/View;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1001fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LaY/g;

    invoke-direct {v1, p0}, LaY/g;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10037e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->t:Landroid/widget/TextView;

    iget-object v0, p0, LaY/c;->t:Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10037f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->u:Landroid/widget/TextView;

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/c;->u:Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10037d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LaY/c;->v:Landroid/widget/ImageView;

    invoke-direct {p0}, LaY/c;->A()V

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003a8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->b()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LaY/c;->M:Lcom/google/googlenav/ui/wizard/hb;

    if-eqz v2, :cond_2

    const/16 v2, 0x423

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v2, LaY/h;

    invoke-direct {v2, p0}, LaY/h;-><init>(LaY/c;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    invoke-virtual {v1}, Lcom/google/googlenav/cu;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaY/c;->K:Ljava/util/List;

    invoke-direct {p0}, LaY/c;->l()V

    invoke-direct {p0}, LaY/c;->m()V

    invoke-direct {p0}, LaY/c;->n()V

    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0401cd

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->D:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, LaY/c;->o()V

    :cond_1
    invoke-virtual {p0}, LaY/c;->J_()V

    invoke-static {}, Lcom/google/googlenav/bm;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaY/c;->D:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    const/16 v2, 0x40c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const/16 v1, 0x404

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LaY/c;->b(Landroid/view/View;Ljava/lang/String;)V

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    return-object v0

    :cond_2
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x40d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected d()V
    .locals 3

    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v2, p0, LaY/c;->b:Landroid/widget/Button;

    if-eqz v1, :cond_2

    const/16 v0, 0x41d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    const/16 v2, 0x41f

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    const/16 v1, 0x3f5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x40b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method h()Lcom/google/googlenav/cx;
    .locals 10

    const/4 v9, 0x0

    const/4 v0, 0x0

    new-instance v7, Ljava/util/ArrayList;

    iget-object v1, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/googlenav/cy;

    iget-boolean v0, v2, Lcom/google/googlenav/cy;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, LaY/c;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0}, LaY/c;->c(Landroid/view/View;)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_1

    new-instance v0, Lcom/google/googlenav/cy;

    iget-object v1, v2, Lcom/google/googlenav/cy;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/cy;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move v1, v6

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/cx;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v7, v9, v9}, Lcom/google/googlenav/cx;-><init>(ILjava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()V
    .locals 0

    invoke-direct {p0}, LaY/c;->y()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-object v0, LaY/c;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001f

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f1003ac

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    const/16 v1, 0x41f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :goto_0
    const v0, 0x7f1003ad

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LaY/c;->a:Landroid/view/MenuItem;

    iget-object v0, p0, LaY/c;->a:Landroid/view/MenuItem;

    const/16 v1, 0x412

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f1003ae

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    const/16 v1, 0x3f5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
