.class public LaZ/a;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:[B

.field private final d:LaN/B;

.field private e:Lo/D;

.field private final f:LaN/H;

.field private final g:I

.field private final h:I

.field private final i:I

.field private j:Z

.field private k:LaZ/b;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:[B


# direct methods
.method public constructor <init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IIIILjava/lang/String;Ljava/lang/String;LaZ/b;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p8, p0, LaZ/a;->f:LaN/H;

    iput p9, p0, LaZ/a;->g:I

    iput p10, p0, LaZ/a;->h:I

    iput p1, p0, LaZ/a;->a:I

    iput-object p2, p0, LaZ/a;->b:Ljava/lang/String;

    iput-object p3, p0, LaZ/a;->c:[B

    iput-object p4, p0, LaZ/a;->d:LaN/B;

    iput-object p5, p0, LaZ/a;->e:Lo/D;

    iput-boolean p6, p0, LaZ/a;->j:Z

    iput p7, p0, LaZ/a;->i:I

    iput-object p14, p0, LaZ/a;->q:Ljava/lang/String;

    iput-object p15, p0, LaZ/a;->k:LaZ/b;

    iput p11, p0, LaZ/a;->n:I

    iput p12, p0, LaZ/a;->o:I

    iput-object p13, p0, LaZ/a;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IILjava/lang/String;LaZ/b;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p8, p0, LaZ/a;->f:LaN/H;

    iput p9, p0, LaZ/a;->g:I

    iput p10, p0, LaZ/a;->h:I

    iput p1, p0, LaZ/a;->a:I

    iput-object p2, p0, LaZ/a;->b:Ljava/lang/String;

    iput-object p3, p0, LaZ/a;->c:[B

    iput-object p4, p0, LaZ/a;->d:LaN/B;

    iput-object p5, p0, LaZ/a;->e:Lo/D;

    iput-boolean p6, p0, LaZ/a;->j:Z

    iput p7, p0, LaZ/a;->i:I

    iput-object p11, p0, LaZ/a;->q:Ljava/lang/String;

    iput-object p12, p0, LaZ/a;->k:LaZ/b;

    return-void
.end method

.method private static a(I)Z
    .locals 1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 9

    const/16 v8, 0x9

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ff;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v1, p0, LaZ/a;->a:I

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LaZ/a;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaZ/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, LaZ/a;->c:[B

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    iget-object v2, p0, LaZ/a;->c:[B

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-boolean v1, p0, LaZ/a;->j:Z

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    iget v2, p0, LaZ/a;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LaZ/a;->q:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/16 v1, 0xb

    iget-object v2, p0, LaZ/a;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v1, p0, LaZ/a;->d:LaN/B;

    invoke-static {v1}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LaZ/a;->f:LaN/H;

    invoke-virtual {v1}, LaN/H;->a()LaN/B;

    move-result-object v1

    iget v2, p0, LaZ/a;->g:I

    iget v3, p0, LaZ/a;->h:I

    iget-object v4, p0, LaZ/a;->f:LaN/H;

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LaZ/a;->e:Lo/D;

    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ej;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v2, p0, LaZ/a;->e:Lo/D;

    invoke-virtual {v2}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v2}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ff;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v2, p0, LaZ/a;->a:I

    invoke-static {v2}, LaZ/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_8

    iget v2, p0, LaZ/a;->n:I

    invoke-virtual {v1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v2, p0, LaZ/a;->o:I

    invoke-virtual {v1, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, LaZ/a;->p:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    :goto_0
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    iget-object v1, p0, LaZ/a;->s:[B

    if-eqz v1, :cond_6

    iget-object v1, p0, LaZ/a;->s:[B

    array-length v1, v1

    if-lez v1, :cond_6

    iget-object v1, p0, LaZ/a;->s:[B

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_6
    iget-object v1, p0, LaZ/a;->r:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0xa

    iget-object v2, p0, LaZ/a;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_7
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void

    :cond_8
    iget v2, p0, LaZ/a;->a:I

    if-ne v2, v8, :cond_4

    iget-object v2, p0, LaZ/a;->p:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LaZ/a;->p:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, -0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ff;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    iput v1, p0, LaZ/a;->l:I

    iput v2, p0, LaZ/a;->m:I

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, LaZ/a;->m:I

    :cond_0
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget v1, p0, LaZ/a;->m:I

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    return v3
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x4f

    return v0
.end method

.method public d_()V
    .locals 2

    iget v0, p0, LaZ/a;->l:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, LaZ/a;->k:LaZ/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaZ/a;->k:LaZ/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LaZ/b;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, LaZ/a;->k:LaZ/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaZ/a;->k:LaZ/b;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LaZ/b;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
