.class public final Lad/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lad/d;


# static fields
.field private static final a:Lad/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lad/b;

    invoke-direct {v0}, Lad/b;-><init>()V

    sput-object v0, Lad/b;->a:Lad/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lad/c;III)I
    .locals 1

    invoke-interface {p1, p2, p3}, Lad/c;->c(II)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1, p3, p4}, Lad/c;->c(II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return p3

    :cond_1
    invoke-interface {p1, p2, p4}, Lad/c;->c(II)Z

    move-result v0

    if-eqz v0, :cond_2

    move p3, p4

    goto :goto_0

    :cond_2
    move p3, p2

    goto :goto_0

    :cond_3
    invoke-interface {p1, p4, p3}, Lad/c;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, p4, p2}, Lad/c;->c(II)Z

    move-result v0

    if-eqz v0, :cond_4

    move p3, p4

    goto :goto_0

    :cond_4
    move p3, p2

    goto :goto_0
.end method

.method public static a()Lad/d;
    .locals 1

    sget-object v0, Lad/b;->a:Lad/d;

    return-object v0
.end method

.method private b(Lad/c;II)I
    .locals 5

    sub-int v0, p3, p2

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    sub-int v0, p3, p2

    div-int/lit8 v0, v0, 0x8

    mul-int/lit8 v1, v0, 0x0

    add-int/2addr v1, p2

    mul-int/lit8 v2, v0, 0x1

    add-int/2addr v2, p2

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v3, p2

    invoke-direct {p0, p1, v1, v2, v3}, Lad/b;->a(Lad/c;III)I

    move-result v1

    mul-int/lit8 v2, v0, 0x3

    add-int/2addr v2, p2

    mul-int/lit8 v3, v0, 0x4

    add-int/2addr v3, p2

    mul-int/lit8 v4, v0, 0x5

    add-int/2addr v4, p2

    invoke-direct {p0, p1, v2, v3, v4}, Lad/b;->a(Lad/c;III)I

    move-result v2

    mul-int/lit8 v3, v0, 0x6

    add-int/2addr v3, p2

    mul-int/lit8 v4, v0, 0x7

    add-int/2addr v4, p2

    mul-int/lit8 v0, v0, 0x8

    add-int/2addr v0, p2

    invoke-direct {p0, p1, v3, v4, v0}, Lad/b;->a(Lad/c;III)I

    move-result v0

    invoke-direct {p0, p1, v1, v2, v0}, Lad/b;->a(Lad/c;III)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    add-int v0, p2, p3

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, p2, v0, p3}, Lad/b;->a(Lad/c;III)I

    move-result v0

    goto :goto_0
.end method

.method private b(Lad/c;III)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p4, :cond_0

    add-int v1, p2, v0

    add-int v2, p3, v0

    invoke-interface {p1, v1, v2}, Lad/c;->d(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lad/c;II)V
    .locals 9

    move v5, p3

    move v6, p2

    :cond_0
    :goto_0
    sub-int v0, v5, v6

    const/16 v1, 0x8

    if-lt v0, v1, :cond_7

    invoke-direct {p0, p1, v6, v5}, Lad/b;->b(Lad/c;II)I

    move-result v0

    move v2, v5

    move v3, v5

    move v4, v6

    move v1, v6

    :goto_1
    if-gt v4, v3, :cond_1

    invoke-interface {p1, v0, v4}, Lad/c;->c(II)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-interface {p1, v4, v0}, Lad/c;->c(II)Z

    move-result v7

    if-nez v7, :cond_9

    invoke-interface {p1, v1, v4}, Lad/c;->d(II)V

    add-int/lit8 v0, v1, 0x1

    :goto_2
    add-int/lit8 v4, v4, 0x1

    move v8, v0

    move v0, v1

    move v1, v8

    goto :goto_1

    :cond_1
    :goto_3
    if-gt v4, v3, :cond_2

    invoke-interface {p1, v3, v0}, Lad/c;->c(II)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-interface {p1, v0, v3}, Lad/c;->c(II)Z

    move-result v7

    if-nez v7, :cond_8

    invoke-interface {p1, v3, v2}, Lad/c;->d(II)V

    add-int/lit8 v0, v2, -0x1

    :goto_4
    add-int/lit8 v3, v3, -0x1

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :cond_2
    if-le v4, v3, :cond_3

    sub-int v0, v1, v6

    sub-int v7, v4, v1

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v7, v4, v0

    invoke-direct {p0, p1, v6, v7, v0}, Lad/b;->b(Lad/c;III)V

    sub-int v0, v2, v3

    sub-int v7, v5, v2

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v7, v5, 0x1

    sub-int/2addr v7, v0

    invoke-direct {p0, p1, v4, v7, v0}, Lad/b;->b(Lad/c;III)V

    sub-int v0, v4, v1

    add-int/2addr v0, v6

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v5, 0x1

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    sub-int v2, v0, v6

    sub-int v3, v5, v1

    if-ge v2, v3, :cond_6

    move v8, v6

    move v6, v1

    move v1, v8

    :goto_5
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, p1, v1, v0}, Lad/b;->a(Lad/c;II)V

    goto :goto_0

    :cond_3
    if-ne v4, v0, :cond_5

    move v0, v3

    :cond_4
    :goto_6
    invoke-interface {p1, v4, v3}, Lad/c;->d(II)V

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_5
    if-ne v3, v0, :cond_4

    move v0, v4

    goto :goto_6

    :cond_6
    move v8, v5

    move v5, v0

    move v0, v8

    goto :goto_5

    :cond_7
    invoke-static {}, Lad/a;->a()Lad/d;

    move-result-object v0

    invoke-interface {v0, p1, v6, v5}, Lad/d;->a(Lad/c;II)V

    return-void

    :cond_8
    move v8, v2

    move v2, v0

    move v0, v8

    goto :goto_4

    :cond_9
    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_2
.end method
