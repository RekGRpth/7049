.class public abstract Lae/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lae/i;


# instance fields
.field private final a:Lae/b;

.field private final b:Lae/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lae/b;

    invoke-direct {v0}, Lae/b;-><init>()V

    iput-object v0, p0, Lae/a;->a:Lae/b;

    new-instance v0, Lae/e;

    invoke-direct {v0}, Lae/e;-><init>()V

    iput-object v0, p0, Lae/a;->b:Lae/e;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    iget-object v0, p0, Lae/a;->b:Lae/e;

    invoke-virtual {v0, p1, p2}, Lae/e;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lae/a;->a:Lae/b;

    invoke-virtual {v0, p1}, Lae/b;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lae/a;->b:Lae/e;

    invoke-virtual {v1}, Lae/e;->a()V

    :cond_0
    return v0
.end method

.method protected a(Ljava/lang/Throwable;)Z
    .locals 2

    iget-object v1, p0, Lae/a;->a:Lae/b;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v1, v0}, Lae/b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lae/a;->b:Lae/e;

    invoke-virtual {v1}, Lae/e;->a()V

    :cond_0
    instance-of v1, p1, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast p1, Ljava/lang/Error;

    throw p1

    :cond_1
    return v0
.end method

.method public cancel(Z)Z
    .locals 1

    iget-object v0, p0, Lae/a;->a:Lae/b;

    invoke-virtual {v0}, Lae/b;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lae/a;->b:Lae/e;

    invoke-virtual {v0}, Lae/e;->a()V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lae/a;->a()V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lae/a;->a:Lae/b;

    invoke-virtual {v0}, Lae/b;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lae/a;->a:Lae/b;

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lae/b;->a(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isCancelled()Z
    .locals 1

    iget-object v0, p0, Lae/a;->a:Lae/b;

    invoke-virtual {v0}, Lae/b;->c()Z

    move-result v0

    return v0
.end method

.method public isDone()Z
    .locals 1

    iget-object v0, p0, Lae/a;->a:Lae/b;

    invoke-virtual {v0}, Lae/b;->b()Z

    move-result v0

    return v0
.end method
