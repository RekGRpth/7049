.class public final Laf/a;
.super LbH/j;
.source "SourceFile"


# static fields
.field public static a:LbH/r;

.field private static final b:Laf/a;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Laf/b;

    invoke-direct {v0}, Laf/b;-><init>()V

    sput-object v0, Laf/a;->a:LbH/r;

    new-instance v0, Laf/a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Laf/a;-><init>(Z)V

    sput-object v0, Laf/a;->b:Laf/a;

    sget-object v0, Laf/a;->b:Laf/a;

    invoke-direct {v0}, Laf/a;->k()V

    return-void
.end method

.method private constructor <init>(LbH/f;LbH/i;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/a;->f:B

    iput v0, p0, Laf/a;->g:I

    invoke-direct {p0}, Laf/a;->k()V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    :try_start_0
    invoke-virtual {p1}, LbH/f;->a()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Laf/a;->a(LbH/f;LbH/i;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_0
    move v0, v1

    goto :goto_0

    :sswitch_1
    iget v2, p0, Laf/a;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Laf/a;->c:I

    invoke-virtual {p1}, LbH/f;->e()LbH/d;

    move-result-object v2

    iput-object v2, p0, Laf/a;->d:Ljava/lang/Object;
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Laf/a;->s()V

    throw v0

    :sswitch_2
    :try_start_2
    iget v2, p0, Laf/a;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Laf/a;->c:I

    invoke-virtual {p1}, LbH/f;->e()LbH/d;

    move-result-object v2

    iput-object v2, p0, Laf/a;->e:Ljava/lang/Object;
    :try_end_2
    .catch LbH/l; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    new-instance v1, LbH/l;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LbH/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    invoke-virtual {p0}, Laf/a;->s()V

    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(LbH/f;LbH/i;Laf/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Laf/a;-><init>(LbH/f;LbH/i;)V

    return-void
.end method

.method private constructor <init>(LbH/k;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, LbH/j;-><init>(LbH/k;)V

    iput-byte v0, p0, Laf/a;->f:B

    iput v0, p0, Laf/a;->g:I

    return-void
.end method

.method synthetic constructor <init>(LbH/k;Laf/b;)V
    .locals 0

    invoke-direct {p0, p1}, Laf/a;-><init>(LbH/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/a;->f:B

    iput v0, p0, Laf/a;->g:I

    return-void
.end method

.method static synthetic a(Laf/a;I)I
    .locals 0

    iput p1, p0, Laf/a;->c:I

    return p1
.end method

.method public static a()Laf/a;
    .locals 1

    sget-object v0, Laf/a;->b:Laf/a;

    return-object v0
.end method

.method static synthetic a(Laf/a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Laf/a;->d:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Laf/a;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Laf/a;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Laf/a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Laf/a;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Laf/a;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Laf/a;->e:Ljava/lang/Object;

    return-object p1
.end method

.method public static h()Laf/c;
    .locals 1

    invoke-static {}, Laf/c;->g()Laf/c;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 1

    const-string v0, ""

    iput-object v0, p0, Laf/a;->d:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Laf/a;->e:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(LbH/g;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Laf/a;->g()I

    iget v0, p0, Laf/a;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Laf/a;->c()LbH/d;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LbH/g;->a(ILbH/d;)V

    :cond_0
    iget v0, p0, Laf/a;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Laf/a;->e()LbH/d;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LbH/g;->a(ILbH/d;)V

    :cond_1
    return-void
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Laf/a;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LbH/d;
    .locals 2

    iget-object v0, p0, Laf/a;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LbH/d;->a(Ljava/lang/String;)LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/a;->d:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LbH/d;

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    iget v0, p0, Laf/a;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()LbH/d;
    .locals 2

    iget-object v0, p0, Laf/a;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LbH/d;->a(Ljava/lang/String;)LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/a;->e:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LbH/d;

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Laf/a;->f:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Laf/a;->f:B

    goto :goto_0
.end method

.method public g()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Laf/a;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Laf/a;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Laf/a;->c()LbH/d;

    move-result-object v1

    invoke-static {v2, v1}, LbH/g;->b(ILbH/d;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Laf/a;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    invoke-virtual {p0}, Laf/a;->e()LbH/d;

    move-result-object v1

    invoke-static {v3, v1}, LbH/g;->b(ILbH/d;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Laf/a;->g:I

    goto :goto_0
.end method

.method public i()Laf/c;
    .locals 1

    invoke-static {}, Laf/a;->h()Laf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic j()LbH/q;
    .locals 1

    invoke-virtual {p0}, Laf/a;->i()Laf/c;

    move-result-object v0

    return-object v0
.end method
