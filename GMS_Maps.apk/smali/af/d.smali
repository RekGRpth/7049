.class public final Laf/d;
.super LbH/j;
.source "SourceFile"


# static fields
.field public static a:LbH/r;

.field private static final b:Laf/d;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ljava/util/List;

.field private e:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Laf/e;

    invoke-direct {v0}, Laf/e;-><init>()V

    sput-object v0, Laf/d;->a:LbH/r;

    new-instance v0, Laf/d;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Laf/d;-><init>(Z)V

    sput-object v0, Laf/d;->b:Laf/d;

    sget-object v0, Laf/d;->b:Laf/d;

    invoke-direct {v0}, Laf/d;->i()V

    return-void
.end method

.method private constructor <init>(LbH/f;LbH/i;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v2, p0, Laf/d;->f:B

    iput v2, p0, Laf/d;->g:I

    invoke-direct {p0}, Laf/d;->i()V

    move v2, v0

    :cond_0
    :goto_0
    if-nez v0, :cond_3

    :try_start_0
    invoke-virtual {p1}, LbH/f;->a()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v3}, Laf/d;->a(LbH/f;LbH/i;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_0
    move v0, v1

    goto :goto_0

    :sswitch_1
    and-int/lit8 v3, v2, 0x1

    if-eq v3, v1, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Laf/d;->d:Ljava/util/List;

    or-int/lit8 v2, v2, 0x1

    :cond_1
    iget-object v3, p0, Laf/d;->d:Ljava/util/List;

    sget-object v4, Laf/a;->a:LbH/r;

    invoke-virtual {p1, v4, p2}, LbH/f;->a(LbH/r;LbH/i;)LbH/p;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    iget-object v1, p0, Laf/d;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Laf/d;->d:Ljava/util/List;

    :cond_2
    invoke-virtual {p0}, Laf/d;->s()V

    throw v0

    :sswitch_2
    :try_start_2
    iget v3, p0, Laf/d;->c:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Laf/d;->c:I

    invoke-virtual {p1}, LbH/f;->e()LbH/d;

    move-result-object v3

    iput-object v3, p0, Laf/d;->e:Ljava/lang/Object;
    :try_end_2
    .catch LbH/l; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    new-instance v3, LbH/l;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, LbH/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Laf/d;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laf/d;->d:Ljava/util/List;

    :cond_4
    invoke-virtual {p0}, Laf/d;->s()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(LbH/f;LbH/i;Laf/e;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Laf/d;-><init>(LbH/f;LbH/i;)V

    return-void
.end method

.method private constructor <init>(LbH/k;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, LbH/j;-><init>(LbH/k;)V

    iput-byte v0, p0, Laf/d;->f:B

    iput v0, p0, Laf/d;->g:I

    return-void
.end method

.method synthetic constructor <init>(LbH/k;Laf/e;)V
    .locals 0

    invoke-direct {p0, p1}, Laf/d;-><init>(LbH/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/d;->f:B

    iput v0, p0, Laf/d;->g:I

    return-void
.end method

.method static synthetic a(Laf/d;I)I
    .locals 0

    iput p1, p0, Laf/d;->c:I

    return p1
.end method

.method public static a()Laf/d;
    .locals 1

    sget-object v0, Laf/d;->b:Laf/d;

    return-object v0
.end method

.method public static a(Laf/d;)Laf/f;
    .locals 1

    invoke-static {}, Laf/d;->e()Laf/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Laf/f;->a(Laf/d;)Laf/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laf/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Laf/d;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Laf/d;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Laf/d;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Laf/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Laf/d;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Laf/d;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Laf/d;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static e()Laf/f;
    .locals 1

    invoke-static {}, Laf/f;->g()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laf/d;->d:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Laf/d;->e:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(LbH/g;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Laf/d;->g()I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Laf/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Laf/d;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbH/p;

    invoke-virtual {p1, v2, v0}, LbH/g;->a(ILbH/p;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Laf/d;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Laf/d;->c()LbH/d;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LbH/g;->a(ILbH/d;)V

    :cond_1
    return-void
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Laf/d;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LbH/d;
    .locals 2

    iget-object v0, p0, Laf/d;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LbH/d;->a(Ljava/lang/String;)LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/d;->e:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LbH/d;

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Laf/d;->f:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Laf/d;->f:B

    goto :goto_0
.end method

.method public f()Laf/f;
    .locals 1

    invoke-static {}, Laf/d;->e()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget v2, p0, Laf/d;->g:I

    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    :goto_1
    iget-object v0, p0, Laf/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Laf/d;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbH/p;

    invoke-static {v3, v0}, LbH/g;->b(ILbH/p;)I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget v0, p0, Laf/d;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0}, Laf/d;->c()LbH/d;

    move-result-object v1

    invoke-static {v0, v1}, LbH/g;->b(ILbH/d;)I

    move-result v0

    add-int/2addr v2, v0

    :cond_2
    iput v2, p0, Laf/d;->g:I

    goto :goto_0
.end method

.method public h()Laf/f;
    .locals 1

    invoke-static {p0}, Laf/d;->a(Laf/d;)Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic j()LbH/q;
    .locals 1

    invoke-virtual {p0}, Laf/d;->f()Laf/f;

    move-result-object v0

    return-object v0
.end method
