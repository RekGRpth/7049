.class public final Laf/g;
.super LbH/j;
.source "SourceFile"


# static fields
.field public static a:LbH/r;

.field private static final b:Laf/g;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:J

.field private e:Laf/d;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Laf/h;

    invoke-direct {v0}, Laf/h;-><init>()V

    sput-object v0, Laf/g;->a:LbH/r;

    new-instance v0, Laf/g;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Laf/g;-><init>(Z)V

    sput-object v0, Laf/g;->b:Laf/g;

    sget-object v0, Laf/g;->b:Laf/g;

    invoke-direct {v0}, Laf/g;->t()V

    return-void
.end method

.method private constructor <init>(LbH/f;LbH/i;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/g;->i:B

    iput v0, p0, Laf/g;->j:I

    invoke-direct {p0}, Laf/g;->t()V

    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_2

    :try_start_0
    invoke-virtual {p1}, LbH/f;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Laf/g;->a(LbH/f;LbH/i;I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :sswitch_0
    move v0, v1

    goto :goto_1

    :sswitch_1
    iget v0, p0, Laf/g;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/g;->c:I

    invoke-virtual {p1}, LbH/f;->c()J

    move-result-wide v3

    iput-wide v3, p0, Laf/g;->d:J

    move v0, v2

    goto :goto_1

    :sswitch_2
    const/4 v0, 0x0

    iget v3, p0, Laf/g;->c:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iget-object v0, p0, Laf/g;->e:Laf/d;

    invoke-virtual {v0}, Laf/d;->h()Laf/f;

    move-result-object v0

    move-object v3, v0

    :goto_2
    sget-object v0, Laf/d;->a:LbH/r;

    invoke-virtual {p1, v0, p2}, LbH/f;->a(LbH/r;LbH/i;)LbH/p;

    move-result-object v0

    check-cast v0, Laf/d;

    iput-object v0, p0, Laf/g;->e:Laf/d;

    if-eqz v3, :cond_0

    iget-object v0, p0, Laf/g;->e:Laf/d;

    invoke-virtual {v3, v0}, Laf/f;->a(Laf/d;)Laf/f;

    invoke-virtual {v3}, Laf/f;->c()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->e:Laf/d;

    :cond_0
    iget v0, p0, Laf/g;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/g;->c:I

    move v0, v2

    goto :goto_1

    :sswitch_3
    iget v0, p0, Laf/g;->c:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laf/g;->c:I

    invoke-virtual {p1}, LbH/f;->e()LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->f:Ljava/lang/Object;

    move v0, v2

    goto :goto_1

    :sswitch_4
    iget v0, p0, Laf/g;->c:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laf/g;->c:I

    invoke-virtual {p1}, LbH/f;->e()LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->g:Ljava/lang/Object;

    move v0, v2

    goto :goto_1

    :sswitch_5
    iget v0, p0, Laf/g;->c:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laf/g;->c:I

    invoke-virtual {p1}, LbH/f;->e()LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->h:Ljava/lang/Object;
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Laf/g;->s()V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Laf/g;->s()V

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    new-instance v1, LbH/l;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LbH/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v3, v0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method synthetic constructor <init>(LbH/f;LbH/i;Laf/h;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Laf/g;-><init>(LbH/f;LbH/i;)V

    return-void
.end method

.method private constructor <init>(LbH/k;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, LbH/j;-><init>(LbH/k;)V

    iput-byte v0, p0, Laf/g;->i:B

    iput v0, p0, Laf/g;->j:I

    return-void
.end method

.method synthetic constructor <init>(LbH/k;Laf/h;)V
    .locals 0

    invoke-direct {p0, p1}, Laf/g;-><init>(LbH/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/g;->i:B

    iput v0, p0, Laf/g;->j:I

    return-void
.end method

.method static synthetic a(Laf/g;I)I
    .locals 0

    iput p1, p0, Laf/g;->c:I

    return p1
.end method

.method static synthetic a(Laf/g;J)J
    .locals 0

    iput-wide p1, p0, Laf/g;->d:J

    return-wide p1
.end method

.method static synthetic a(Laf/g;Laf/d;)Laf/d;
    .locals 0

    iput-object p1, p0, Laf/g;->e:Laf/d;

    return-object p1
.end method

.method public static a()Laf/g;
    .locals 1

    sget-object v0, Laf/g;->b:Laf/g;

    return-object v0
.end method

.method public static a(Laf/g;)Laf/i;
    .locals 1

    invoke-static {}, Laf/g;->o()Laf/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Laf/i;->a(Laf/g;)Laf/i;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Laf/g;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Laf/g;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Laf/g;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Laf/g;->g:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Laf/g;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Laf/g;->g:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Laf/g;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic d(Laf/g;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Laf/g;->h:Ljava/lang/Object;

    return-object v0
.end method

.method public static o()Laf/i;
    .locals 1

    invoke-static {}, Laf/i;->g()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method private t()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laf/g;->d:J

    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->e:Laf/d;

    const-string v0, ""

    iput-object v0, p0, Laf/g;->f:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Laf/g;->g:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Laf/g;->h:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(LbH/g;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Laf/g;->g()I

    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Laf/g;->d:J

    invoke-virtual {p1, v2, v0, v1}, LbH/g;->a(IJ)V

    :cond_0
    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Laf/g;->e:Laf/d;

    invoke-virtual {p1, v3, v0}, LbH/g;->a(ILbH/p;)V

    :cond_1
    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Laf/g;->h()LbH/d;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LbH/g;->a(ILbH/d;)V

    :cond_2
    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Laf/g;->k()LbH/d;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, LbH/g;->a(ILbH/d;)V

    :cond_3
    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Laf/g;->m()LbH/d;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LbH/g;->a(ILbH/d;)V

    :cond_4
    return-void
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Laf/g;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Laf/g;->d:J

    return-wide v0
.end method

.method public d()Z
    .locals 2

    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Laf/d;
    .locals 1

    iget-object v0, p0, Laf/g;->e:Laf/d;

    return-object v0
.end method

.method public f()Z
    .locals 2

    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Laf/g;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Laf/g;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    iget-wide v1, p0, Laf/g;->d:J

    invoke-static {v3, v1, v2}, LbH/g;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Laf/g;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Laf/g;->e:Laf/d;

    invoke-static {v4, v1}, LbH/g;->b(ILbH/p;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Laf/g;->c:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    const/4 v1, 0x3

    invoke-virtual {p0}, Laf/g;->h()LbH/d;

    move-result-object v2

    invoke-static {v1, v2}, LbH/g;->b(ILbH/d;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Laf/g;->c:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Laf/g;->k()LbH/d;

    move-result-object v1

    invoke-static {v5, v1}, LbH/g;->b(ILbH/d;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Laf/g;->c:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    invoke-virtual {p0}, Laf/g;->m()LbH/d;

    move-result-object v2

    invoke-static {v1, v2}, LbH/g;->b(ILbH/d;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Laf/g;->j:I

    goto :goto_0
.end method

.method public h()LbH/d;
    .locals 2

    iget-object v0, p0, Laf/g;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LbH/d;->a(Ljava/lang/String;)LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->f:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LbH/d;

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic j()LbH/q;
    .locals 1

    invoke-virtual {p0}, Laf/g;->p()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public k()LbH/d;
    .locals 2

    iget-object v0, p0, Laf/g;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LbH/d;->a(Ljava/lang/String;)LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->g:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LbH/d;

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    iget v0, p0, Laf/g;->c:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()LbH/d;
    .locals 2

    iget-object v0, p0, Laf/g;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LbH/d;->a(Ljava/lang/String;)LbH/d;

    move-result-object v0

    iput-object v0, p0, Laf/g;->h:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LbH/d;

    goto :goto_0
.end method

.method public final n()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Laf/g;->i:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Laf/g;->i:B

    goto :goto_0
.end method

.method public p()Laf/i;
    .locals 1

    invoke-static {}, Laf/g;->o()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public q()Laf/i;
    .locals 1

    invoke-static {p0}, Laf/g;->a(Laf/g;)Laf/i;

    move-result-object v0

    return-object v0
.end method
