.class public final Laf/i;
.super LbH/k;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:J

.field private c:Laf/d;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, LbH/k;-><init>()V

    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/i;->c:Laf/d;

    const-string v0, ""

    iput-object v0, p0, Laf/i;->d:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Laf/i;->e:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Laf/i;->f:Ljava/lang/Object;

    invoke-direct {p0}, Laf/i;->h()V

    return-void
.end method

.method static synthetic g()Laf/i;
    .locals 1

    invoke-static {}, Laf/i;->i()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 0

    return-void
.end method

.method private static i()Laf/i;
    .locals 1

    new-instance v0, Laf/i;

    invoke-direct {v0}, Laf/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Laf/i;
    .locals 2

    invoke-static {}, Laf/i;->i()Laf/i;

    move-result-object v0

    invoke-virtual {p0}, Laf/i;->c()Laf/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/i;->a(Laf/g;)Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Laf/i;
    .locals 1

    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/i;->a:I

    iput-wide p1, p0, Laf/i;->b:J

    return-object p0
.end method

.method public a(Laf/d;)Laf/i;
    .locals 2

    iget v0, p0, Laf/i;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Laf/i;->c:Laf/d;

    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Laf/i;->c:Laf/d;

    invoke-static {v0}, Laf/d;->a(Laf/d;)Laf/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Laf/f;->a(Laf/d;)Laf/f;

    move-result-object v0

    invoke-virtual {v0}, Laf/f;->c()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/i;->c:Laf/d;

    :goto_0
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/i;->a:I

    return-object p0

    :cond_0
    iput-object p1, p0, Laf/i;->c:Laf/d;

    goto :goto_0
.end method

.method public a(Laf/f;)Laf/i;
    .locals 1

    invoke-virtual {p1}, Laf/f;->b()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/i;->c:Laf/d;

    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/i;->a:I

    return-object p0
.end method

.method public a(Laf/g;)Laf/i;
    .locals 2

    invoke-static {}, Laf/g;->a()Laf/g;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Laf/g;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Laf/g;->c()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Laf/i;->a(J)Laf/i;

    :cond_2
    invoke-virtual {p1}, Laf/g;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Laf/g;->e()Laf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Laf/i;->a(Laf/d;)Laf/i;

    :cond_3
    invoke-virtual {p1}, Laf/g;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laf/i;->a:I

    invoke-static {p1}, Laf/g;->b(Laf/g;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/i;->d:Ljava/lang/Object;

    :cond_4
    invoke-virtual {p1}, Laf/g;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laf/i;->a:I

    invoke-static {p1}, Laf/g;->c(Laf/g;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/i;->e:Ljava/lang/Object;

    :cond_5
    invoke-virtual {p1}, Laf/g;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laf/i;->a:I

    invoke-static {p1}, Laf/g;->d(Laf/g;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/i;->f:Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(LbH/f;LbH/i;)Laf/i;
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    sget-object v0, Laf/g;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/g;
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Laf/i;->a(Laf/g;)Laf/i;

    :cond_0
    return-object p0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Laf/i;->a(Laf/g;)Laf/i;

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Laf/i;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laf/i;->a:I

    iput-object p1, p0, Laf/i;->d:Ljava/lang/Object;

    return-object p0
.end method

.method public b()Laf/g;
    .locals 2

    invoke-virtual {p0}, Laf/i;->c()Laf/g;

    move-result-object v0

    invoke-virtual {v0}, Laf/g;->n()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Laf/i;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public b(Ljava/lang/String;)Laf/i;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laf/i;->a:I

    iput-object p1, p0, Laf/i;->e:Ljava/lang/Object;

    return-object p0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .locals 1

    invoke-virtual {p0, p1, p2}, Laf/i;->a(LbH/f;LbH/i;)Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/g;
    .locals 6

    const/4 v0, 0x1

    new-instance v2, Laf/g;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Laf/g;-><init>(LbH/k;Laf/h;)V

    iget v3, p0, Laf/i;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-wide v4, p0, Laf/i;->b:J

    invoke-static {v2, v4, v5}, Laf/g;->a(Laf/g;J)J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Laf/i;->c:Laf/d;

    invoke-static {v2, v1}, Laf/g;->a(Laf/g;Laf/d;)Laf/d;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Laf/i;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/g;->a(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Laf/i;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/g;->b(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Laf/i;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/g;->c(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Laf/g;->a(Laf/g;I)I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Laf/i;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laf/i;->a:I

    iput-object p1, p0, Laf/i;->f:Ljava/lang/Object;

    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Laf/i;->a()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .locals 1

    invoke-virtual {p0}, Laf/i;->a()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .locals 1

    invoke-virtual {p0}, Laf/i;->a()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .locals 1

    invoke-virtual {p0}, Laf/i;->b()Laf/g;

    move-result-object v0

    return-object v0
.end method
