.class public final Laf/j;
.super LbH/j;
.source "SourceFile"


# static fields
.field public static a:LbH/r;

.field private static final b:Laf/j;

.field private static final serialVersionUID:J


# instance fields
.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Laf/k;

    invoke-direct {v0}, Laf/k;-><init>()V

    sput-object v0, Laf/j;->a:LbH/r;

    new-instance v0, Laf/j;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Laf/j;-><init>(Z)V

    sput-object v0, Laf/j;->b:Laf/j;

    sget-object v0, Laf/j;->b:Laf/j;

    invoke-direct {v0}, Laf/j;->e()V

    return-void
.end method

.method private constructor <init>(LbH/f;LbH/i;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/j;->c:B

    iput v0, p0, Laf/j;->d:I

    invoke-direct {p0}, Laf/j;->e()V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    :try_start_0
    invoke-virtual {p1}, LbH/f;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Laf/j;->a(LbH/f;LbH/i;I)Z
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Laf/j;->s()V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Laf/j;->s()V

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    new-instance v1, LbH/l;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LbH/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, LbH/l;->a(LbH/p;)LbH/l;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method synthetic constructor <init>(LbH/f;LbH/i;Laf/k;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Laf/j;-><init>(LbH/f;LbH/i;)V

    return-void
.end method

.method private constructor <init>(LbH/k;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, LbH/j;-><init>(LbH/k;)V

    iput-byte v0, p0, Laf/j;->c:B

    iput v0, p0, Laf/j;->d:I

    return-void
.end method

.method synthetic constructor <init>(LbH/k;Laf/k;)V
    .locals 0

    invoke-direct {p0, p1}, Laf/j;-><init>(LbH/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, LbH/j;-><init>()V

    iput-byte v0, p0, Laf/j;->c:B

    iput v0, p0, Laf/j;->d:I

    return-void
.end method

.method public static a()Laf/j;
    .locals 1

    sget-object v0, Laf/j;->b:Laf/j;

    return-object v0
.end method

.method public static c()Laf/l;
    .locals 1

    invoke-static {}, Laf/l;->g()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a(LbH/g;)V
    .locals 0

    invoke-virtual {p0}, Laf/j;->g()I

    return-void
.end method

.method public final b()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Laf/j;->c:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Laf/j;->c:B

    goto :goto_0
.end method

.method public d()Laf/l;
    .locals 1

    invoke-static {}, Laf/j;->c()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 2

    iget v0, p0, Laf/j;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Laf/j;->d:I

    goto :goto_0
.end method

.method public synthetic j()LbH/q;
    .locals 1

    invoke-virtual {p0}, Laf/j;->d()Laf/l;

    move-result-object v0

    return-object v0
.end method
