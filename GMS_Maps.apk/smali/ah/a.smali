.class public Lah/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LaN/B;

.field private final c:I

.field private final d:I

.field private final e:Lah/d;

.field private f:Lcom/google/googlenav/ui/ak;

.field private g:Landroid/graphics/Rect;

.field private final h:LaN/u;


# direct methods
.method constructor <init>(Ljava/lang/String;LaN/B;IILah/d;LaN/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lah/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lah/a;->b:LaN/B;

    iput p3, p0, Lah/a;->c:I

    iput p4, p0, Lah/a;->d:I

    iput-object p5, p0, Lah/a;->e:Lah/d;

    iput-object p6, p0, Lah/a;->h:LaN/u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/graphics/Rect;Lah/d;LaN/u;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lah/a;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lah/a;->b:LaN/B;

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lah/a;->c:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lah/a;->d:I

    iput-object p3, p0, Lah/a;->e:Lah/d;

    iput-object p2, p0, Lah/a;->g:Landroid/graphics/Rect;

    iput-object p4, p0, Lah/a;->h:LaN/u;

    return-void
.end method

.method public static a(LaN/u;)Lah/a;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, LaN/u;->n()I

    move-result v1

    invoke-virtual {p0}, LaN/u;->o()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Lah/a;

    const-string v2, "map"

    sget-object v3, Lah/d;->e:Lah/d;

    invoke-direct {v1, v2, v0, v3, p0}, Lah/a;-><init>(Ljava/lang/String;Landroid/graphics/Rect;Lah/d;LaN/u;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/f;Lcom/google/android/maps/driveabout/vector/c;Lah/c;LaN/u;)Lah/a;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/f;->e()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0, p2, p3}, Lah/a;->a(Lcom/google/android/maps/driveabout/vector/c;Landroid/view/View;Lah/c;LaN/u;)Landroid/graphics/Rect;

    move-result-object v0

    instance-of v1, p1, Lcom/google/android/maps/driveabout/vector/aA;

    if-nez v1, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x64

    iput v1, v0, Landroid/graphics/Rect;->left:I

    :cond_0
    invoke-static {p1}, Lah/a;->a(Lcom/google/android/maps/driveabout/vector/c;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lah/a;

    sget-object v3, Lah/d;->c:Lah/d;

    invoke-direct {v2, v1, v0, v3, p3}, Lah/a;-><init>(Ljava/lang/String;Landroid/graphics/Rect;Lah/d;LaN/u;)V

    return-object v2
.end method

.method public static a(Lcom/google/googlenav/ai;LaN/u;)Lah/a;
    .locals 7

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(B)I

    move-result v3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(B)I

    move-result v4

    new-instance v0, Lah/a;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    sget-object v5, Lah/d;->b:Lah/d;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lah/a;-><init>(Ljava/lang/String;LaN/B;IILah/d;LaN/u;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ui/ak;LaN/u;)Lah/a;
    .locals 7

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->R()[Lam/f;

    move-result-object v0

    aget-object v0, v0, v3

    if-nez v0, :cond_0

    :goto_0
    new-instance v0, Lah/a;

    const-string v1, "current location"

    const/4 v2, 0x0

    sget-object v5, Lah/d;->a:Lah/d;

    move v4, v3

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lah/a;-><init>(Ljava/lang/String;LaN/B;IILah/d;LaN/u;)V

    invoke-direct {v0, p0}, Lah/a;->a(Lcom/google/googlenav/ui/ak;)V

    return-object v0

    :cond_0
    invoke-interface {v0}, Lam/f;->a()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    goto :goto_0
.end method

.method private static a(Lcom/google/android/maps/driveabout/vector/c;Landroid/view/View;Lah/c;LaN/u;)Landroid/graphics/Rect;
    .locals 5

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    invoke-virtual {p3, v0}, LaN/u;->f(LaN/B;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Point;->y:I

    sub-int v2, v4, v2

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v4

    sub-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1e

    iget v4, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v4

    sub-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x1e

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v3, v2, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method static a(Lcom/google/android/maps/driveabout/vector/c;)Ljava/lang/String;
    .locals 3

    const-string v0, "more details button"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/google/googlenav/ui/ak;)V
    .locals 0

    iput-object p1, p0, Lah/a;->f:Lcom/google/googlenav/ui/ak;

    return-void
.end method

.method public static b(Lcom/google/android/maps/driveabout/vector/f;Lcom/google/android/maps/driveabout/vector/c;Lah/c;LaN/u;)Lah/a;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/f;->e()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0, p2, p3}, Lah/a;->a(Lcom/google/android/maps/driveabout/vector/c;Landroid/view/View;Lah/c;LaN/u;)Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x64

    iput v1, v0, Landroid/graphics/Rect;->right:I

    new-instance v1, Lah/a;

    invoke-static {}, Lah/a;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lah/d;->d:Lah/d;

    invoke-direct {v1, v2, v0, v3, p3}, Lah/a;-><init>(Ljava/lang/String;Landroid/graphics/Rect;Lah/d;LaN/u;)V

    return-object v1
.end method

.method private static e()Ljava/lang/String;
    .locals 2

    sget-object v0, Lah/b;->a:[I

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/settings/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "call button"

    goto :goto_0

    :pswitch_1
    const-string v0, "directions button"

    goto :goto_0

    :pswitch_2
    const-string v0, "navigation button"

    goto :goto_0

    :pswitch_3
    const-string v0, "street view button"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(Lah/a;)I
    .locals 2

    invoke-virtual {p0}, Lah/a;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1}, Lah/a;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public a()LaN/B;
    .locals 2

    sget-object v0, Lah/b;->b:[I

    iget-object v1, p0, Lah/a;->e:Lah/d;

    invoke-virtual {v1}, Lah/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lah/a;->b:LaN/B;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lah/a;->b:LaN/B;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah/a;->b:LaN/B;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lah/a;->f:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lah/a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, " "

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lah/a;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public c()I
    .locals 2

    sget-object v0, Lah/b;->b:[I

    iget-object v1, p0, Lah/a;->e:Lah/d;

    invoke-virtual {v1}, Lah/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lah/a;->b:LaN/B;

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    iget-object v1, p0, Lah/a;->b:LaN/B;

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    xor-int/2addr v0, v1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lah/a;

    invoke-virtual {p0, p1}, Lah/a;->a(Lah/a;)I

    move-result v0

    return v0
.end method

.method public d()Landroid/graphics/Rect;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lah/a;->g:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah/a;->e:Lah/d;

    sget-object v1, Lah/d;->a:Lah/d;

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lah/a;->a()LaN/B;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v5, v5, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lah/a;->h:LaN/u;

    invoke-virtual {p0}, Lah/a;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->f(LaN/B;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, Lah/a;->c:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Point;->y:I

    iget v3, p0, Lah/a;->d:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v4, p0, Lah/a;->c:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v4, p0, Lah/a;->d:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v1, v2, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lah/a;->g:Landroid/graphics/Rect;

    :cond_2
    iget-object v0, p0, Lah/a;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v5, v5, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lah/a;->g:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lah/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lah/a;

    invoke-virtual {p0}, Lah/a;->c()I

    move-result v1

    invoke-virtual {p1}, Lah/a;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lah/a;->c()I

    move-result v0

    return v0
.end method
