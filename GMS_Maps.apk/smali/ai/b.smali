.class public Lai/b;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final c:I

.field private d:[Lcom/google/googlenav/ai;

.field private final e:Lai/c;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lai/c;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lai/b;->a:Ljava/lang/Object;

    iput p1, p0, Lai/b;->c:I

    iput-object p2, p0, Lai/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p3, p0, Lai/b;->e:Lai/c;

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 2

    iget-object v1, p0, Lai/b;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lai/b;->d:[Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lai/b;->d:[Lcom/google/googlenav/ai;

    iget-object v0, p0, Lai/b;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    iget v2, p0, Lai/b;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    iget-object v2, p0, Lai/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/c;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    iget-object v3, p0, Lai/b;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lai/b;->d:[Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    iget-object v5, p0, Lai/b;->d:[Lcom/google/googlenav/ai;

    invoke-static {v4}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v4

    aput-object v4, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lai/b;->e:Lai/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lai/b;->e:Lai/c;

    invoke-interface {v0}, Lai/c;->a()V

    :cond_1
    iget-object v0, p0, Lai/b;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3

    return v6

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x67

    return v0
.end method

.method public i()[Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lai/b;->d:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
