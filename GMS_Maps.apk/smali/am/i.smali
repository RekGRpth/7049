.class public final Lam/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lam/g;
.implements Lcom/google/googlenav/common/h;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lam/f;

.field private final c:Ljava/lang/Integer;

.field private volatile d:[Lam/f;


# direct methods
.method public constructor <init>(Lam/f;C)V
    .locals 1

    invoke-static {p2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lam/i;-><init>(Lam/f;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lam/f;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lam/i;-><init>(Ljava/lang/Integer;Lam/f;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Lam/f;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lam/i;->b:Lam/f;

    iput-object p1, p0, Lam/i;->c:Ljava/lang/Integer;

    iput-object p3, p0, Lam/i;->a:Ljava/lang/String;

    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lam/i;-><init>(Ljava/lang/Integer;Lam/f;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lam/f;I)Lam/f;
    .locals 7

    iget-object v0, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-interface {p1}, Lam/f;->a()I

    move-result v0

    iget-object v1, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    div-int v3, v0, v1

    invoke-interface {p1}, Lam/f;->b()I

    move-result v4

    mul-int v1, p2, v3

    const/4 v2, 0x0

    move-object v0, p1

    move v5, v3

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lam/f;->a(IIIIII)Lam/f;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Lam/f;)V
    .locals 3

    iget-object v0, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v1, v0, [Lam/f;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    invoke-direct {p0, p1, v0}, Lam/i;->a(Lam/f;I)Lam/f;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lam/i;->d:[Lam/f;

    return-void
.end method

.method private c()[Lam/f;
    .locals 1

    iget-object v0, p0, Lam/i;->d:[Lam/f;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lam/i;->b()V

    :cond_0
    iget-object v0, p0, Lam/i;->d:[Lam/f;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lam/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lam/i;->d:[Lam/f;

    return-void
.end method

.method public a(C)Z
    .locals 1

    iget-object v0, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(CLam/e;II)Z
    .locals 1

    invoke-virtual {p0, p1}, Lam/i;->e(C)Lam/f;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p2, v0, p3, p4}, Lam/e;->a(Lam/f;II)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(C)I
    .locals 1

    invoke-virtual {p0, p1}, Lam/i;->a(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lam/i;->e(C)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected b()V
    .locals 4

    iget-object v0, p0, Lam/i;->b:Lam/f;

    if-nez v0, :cond_0

    iget-object v0, p0, Lam/i;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    iget-object v1, p0, Lam/i;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lam/h;->b(I)Lam/f;

    move-result-object v0

    iput-object v0, p0, Lam/i;->b:Lam/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iget-object v0, p0, Lam/i;->b:Lam/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lam/i;->b:Lam/f;

    invoke-interface {v0}, Lam/f;->a()I

    move-result v0

    iget-object v1, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Image strip "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lam/i;->c:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " width "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lam/i;->b:Lam/f;

    invoke-interface {v2}, Lam/f;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "divisible by number of icons "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": find it in R.java"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lam/i;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lam/i;->b:Lam/f;

    invoke-direct {p0, v0}, Lam/i;->a(Lam/f;)V

    :cond_3
    return-void
.end method

.method public c(C)I
    .locals 1

    invoke-virtual {p0, p1}, Lam/i;->a(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lam/i;->e(C)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public d(C)I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public e(C)Lam/f;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lam/i;->a(C)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lam/i;->c()[Lam/f;

    move-result-object v0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v1, p0, Lam/i;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v1, p0, Lam/i;->b:Lam/f;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v4, "imageStrip"

    iget-object v5, p0, Lam/i;->b:Lam/f;

    invoke-interface {v5}, Lam/f;->g()I

    move-result v5

    invoke-direct {v1, v4, v5}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lam/i;->d:[Lam/f;

    if-eqz v1, :cond_2

    move v1, v0

    :goto_0
    iget-object v4, p0, Lam/i;->d:[Lam/f;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lam/i;->d:[Lam/f;

    aget-object v4, v4, v0

    invoke-interface {v4}, Lam/f;->g()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v4, "icons"

    invoke-direct {v0, v4, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lam/i;->c:Ljava/lang/Integer;

    if-nez v0, :cond_3

    const-string v0, "ImageStripIconProvider"

    :goto_1
    new-instance v1, Lcom/google/googlenav/common/util/l;

    invoke-direct {v1, v0, v3, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Image_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lam/i;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
