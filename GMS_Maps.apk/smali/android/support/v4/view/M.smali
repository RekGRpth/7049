.class public Landroid/support/v4/view/M;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/S;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v2, "JellyBean"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Landroid/support/v4/view/R;

    invoke-direct {v0}, Landroid/support/v4/view/R;-><init>()V

    sput-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    :goto_0
    return-void

    :cond_1
    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/support/v4/view/Q;

    invoke-direct {v0}, Landroid/support/v4/view/Q;-><init>()V

    sput-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    goto :goto_0

    :cond_2
    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    new-instance v0, Landroid/support/v4/view/P;

    invoke-direct {v0}, Landroid/support/v4/view/P;-><init>()V

    sput-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    goto :goto_0

    :cond_3
    const/16 v1, 0x9

    if-lt v0, v1, :cond_4

    new-instance v0, Landroid/support/v4/view/O;

    invoke-direct {v0}, Landroid/support/v4/view/O;-><init>()V

    sput-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    goto :goto_0

    :cond_4
    new-instance v0, Landroid/support/v4/view/N;

    invoke-direct {v0}, Landroid/support/v4/view/N;-><init>()V

    sput-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0}, Landroid/support/v4/view/S;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/a;)V
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/S;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    return-void
.end method

.method public static a(Landroid/view/View;Le/f;)V
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/S;->a(Landroid/view/View;Le/f;)V

    return-void
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/S;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0}, Landroid/support/v4/view/S;->b(Landroid/view/View;)V

    return-void
.end method

.method public static b(Landroid/view/View;I)V
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/S;->b(Landroid/view/View;I)V

    return-void
.end method

.method public static c(Landroid/view/View;)I
    .locals 1

    sget-object v0, Landroid/support/v4/view/M;->a:Landroid/support/v4/view/S;

    invoke-interface {v0, p0}, Landroid/support/v4/view/S;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method
