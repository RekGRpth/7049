.class public Law/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:I

.field private q:Law/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Law/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Law/j;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Law/j;->f:Z

    const/4 v0, -0x1

    iput v0, p0, Law/j;->p:I

    return-void
.end method


# virtual methods
.method public a()Law/h;
    .locals 5

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Law/j;->b:Ljava/lang/String;

    iget-object v1, p0, Law/j;->c:Ljava/lang/String;

    iget-object v2, p0, Law/j;->d:Ljava/lang/String;

    iget-object v3, p0, Law/j;->e:Ljava/lang/String;

    iget-boolean v4, p0, Law/j;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Law/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;

    move-result-object v0

    iput-object v0, p0, Law/j;->q:Law/h;

    iget-object v0, p0, Law/j;->q:Law/h;

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0, v1}, Law/h;->a(Law/h;Ljava/lang/String;)V

    iget-object v0, p0, Law/j;->q:Law/h;

    const-string v1, "SYSTEM"

    invoke-virtual {v0, v1}, Law/h;->c(Ljava/lang/String;)V

    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Law/h;->d(Ljava/lang/String;)V

    iget-object v0, p0, Law/j;->q:Law/h;

    iget v1, p0, Law/j;->g:I

    invoke-static {v0, v1}, Law/h;->a(Law/h;I)V

    iget-object v0, p0, Law/j;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->a(Law/h;Z)V

    :cond_1
    iget-object v0, p0, Law/j;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->b(Law/h;Z)V

    :cond_2
    iget-object v0, p0, Law/j;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->c(Law/h;Z)V

    :cond_3
    iget-object v0, p0, Law/j;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->d(Law/h;Z)V

    :cond_4
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v0, v0, Law/h;->f:Law/o;

    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/o;->a(Law/o;Z)V

    :cond_5
    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Law/o;->a(Law/o;Ljava/lang/String;)V

    :cond_6
    iget v1, p0, Law/j;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    iget v1, p0, Law/j;->p:I

    invoke-virtual {v0, v1}, Law/o;->c(I)V

    :cond_7
    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Law/o;->a(Z)V

    :cond_8
    iget-object v0, p0, Law/j;->q:Law/h;

    return-object v0
.end method

.method public a(I)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Law/j;->g:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Law/j;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-boolean p1, p0, Law/j;->f:Z

    return-object p0
.end method

.method public b(I)Law/j;
    .locals 0

    iput p1, p0, Law/j;->p:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Law/j;->c:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->j:Ljava/lang/Boolean;

    return-object p0
.end method

.method public b()Law/p;
    .locals 3

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Law/j;->q:Law/h;

    invoke-static {v0}, Law/h;->a(Law/h;)Law/o;

    move-result-object v0

    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/o;->a(Law/o;Z)V

    :cond_1
    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Law/o;->a(Law/o;Ljava/lang/String;)V

    :cond_2
    iget v1, p0, Law/j;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    iget v1, p0, Law/j;->p:I

    invoke-virtual {v0, v1}, Law/o;->c(I)V

    :goto_0
    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Law/o;->a(Z)V

    :cond_3
    return-object v0

    :cond_4
    invoke-static {v0}, Law/o;->a(Law/o;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Law/j;->d:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->k:Ljava/lang/Boolean;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Law/j;->e:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->m:Ljava/lang/Boolean;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Law/j;->h:Ljava/lang/String;

    return-object p0
.end method

.method public e(Z)Law/j;
    .locals 1

    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->l:Ljava/lang/Boolean;

    return-object p0
.end method

.method public f(Ljava/lang/String;)Law/j;
    .locals 0

    iput-object p1, p0, Law/j;->i:Ljava/lang/String;

    return-object p0
.end method

.method public f(Z)Law/j;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->n:Ljava/lang/Boolean;

    return-object p0
.end method
