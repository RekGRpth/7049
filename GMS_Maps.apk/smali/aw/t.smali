.class public Law/t;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private f:Z

.field private g:Z

.field private final h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(I[BZZZLjava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput p1, p0, Law/t;->a:I

    iput-object p2, p0, Law/t;->b:[B

    iput-boolean p3, p0, Law/t;->c:Z

    iput-boolean p4, p0, Law/t;->d:Z

    iput-boolean p5, p0, Law/t;->e:Z

    iput-object p6, p0, Law/t;->h:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 2

    iget-object v0, p0, Law/t;->b:[B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Law/t;->f:Z

    iget-object v0, p0, Law/t;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v1, p0, Law/t;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Law/t;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Law/t;->g:Z

    iget-object v0, p0, Law/t;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v1, p0, Law/t;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Law/t;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    :cond_0
    return v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Law/t;->a:I

    return v0
.end method

.method public c_()Z
    .locals 1

    iget-boolean v0, p0, Law/t;->e:Z

    return v0
.end method

.method public s_()Z
    .locals 1

    iget-boolean v0, p0, Law/t;->c:Z

    return v0
.end method

.method public t_()Z
    .locals 1

    iget-boolean v0, p0, Law/t;->d:Z

    return v0
.end method
