.class public abstract Lax/b;
.super Law/a;
.source "SourceFile"

# interfaces
.implements Lax/k;
.implements Lcom/google/googlenav/F;


# static fields
.field public static final d:LaN/Y;

.field private static final m:Ljava/lang/Object;

.field private static n:I

.field private static final q:Lbm/i;

.field private static final r:Lbm/i;


# instance fields
.field private A:[Lax/y;

.field private B:Lax/y;

.field private C:Lax/y;

.field private D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private E:I

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:Z

.field private J:I

.field private K:Z

.field private L:I

.field private M:I

.field private N:B

.field private O:I

.field private P:I

.field private Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private R:Z

.field private S:Z

.field private T:I

.field private U:Z

.field private V:Ljava/lang/String;

.field private W:I

.field private X:Lax/e;

.field protected a:Ljava/lang/String;

.field protected b:I

.field protected c:I

.field e:Lcom/google/googlenav/ui/m;

.field protected f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field protected g:I

.field protected h:[Lax/h;

.field i:[Lax/d;

.field j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field protected k:I

.field protected l:[I

.field private final o:I

.field private p:I

.field private s:Z

.field private t:Lax/l;

.field private u:Lax/y;

.field private v:Z

.field private w:[Lax/y;

.field private x:Lax/y;

.field private y:Z

.field private z:[Lax/y;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x16

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lax/b;->m:Ljava/lang/Object;

    const/4 v0, 0x1

    sput v0, Lax/b;->n:I

    new-instance v0, Lbm/i;

    const-string v1, "directions"

    const-string v2, "r"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lax/b;->q:Lbm/i;

    new-instance v0, Lbm/i;

    const-string v1, "directions time update"

    const-string v2, "T"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lax/b;->r:Lbm/i;

    const/16 v0, 0xf

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, Lax/b;->d:LaN/Y;

    return-void
.end method

.method protected constructor <init>(Lax/k;Lcom/google/googlenav/ui/m;)V
    .locals 3

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v1

    invoke-interface {p1}, Lax/k;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p2}, Lax/b;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    return-void
.end method

.method protected constructor <init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V
    .locals 1

    invoke-direct {p0, p4}, Lax/b;-><init>(Lcom/google/googlenav/ui/m;)V

    iput-object p1, p0, Lax/b;->u:Lax/y;

    iput-object p2, p0, Lax/b;->x:Lax/y;

    iput-object p3, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    const/4 v0, -0x1

    iput v0, p0, Lax/b;->k:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/b;->S:Z

    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/m;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Law/a;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lax/b;->a:Ljava/lang/String;

    iput v3, p0, Lax/b;->b:I

    iput v1, p0, Lax/b;->c:I

    iput v3, p0, Lax/b;->p:I

    iput-boolean v1, p0, Lax/b;->s:Z

    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v0

    iput-object v0, p0, Lax/b;->t:Lax/l;

    new-array v0, v1, [Lax/y;

    iput-object v0, p0, Lax/b;->A:[Lax/y;

    iput-object v2, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput v1, p0, Lax/b;->E:I

    iput-object v2, p0, Lax/b;->G:Ljava/lang/String;

    iput-boolean v1, p0, Lax/b;->H:Z

    new-array v0, v1, [Lax/d;

    iput-object v0, p0, Lax/b;->i:[Lax/d;

    new-array v0, v1, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-boolean v4, p0, Lax/b;->I:Z

    iput v1, p0, Lax/b;->J:I

    iput-boolean v1, p0, Lax/b;->K:Z

    iput v1, p0, Lax/b;->L:I

    iput v1, p0, Lax/b;->P:I

    new-array v0, v1, [I

    iput-object v0, p0, Lax/b;->l:[I

    new-array v0, v1, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xf

    iput v0, p0, Lax/b;->T:I

    iput-boolean v4, p0, Lax/b;->U:Z

    iput-object v2, p0, Lax/b;->V:Ljava/lang/String;

    iput v3, p0, Lax/b;->W:I

    iput-object p1, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    invoke-static {}, Lax/b;->aV()I

    move-result v0

    iput v0, p0, Lax/b;->o:I

    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    return-void
.end method

.method protected constructor <init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V
    .locals 4

    invoke-direct {p0, p2}, Lax/b;-><init>(Lcom/google/googlenav/ui/m;)V

    array-length v0, p1

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v1}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v1

    iput-object v1, p0, Lax/b;->u:Lax/y;

    add-int/lit8 v1, v0, -0x1

    aget-object v1, p1, v1

    invoke-static {v1}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v1

    iput-object v1, p0, Lax/b;->x:Lax/y;

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    add-int/lit8 v0, v0, -0x2

    new-array v0, v0, [Lax/y;

    iput-object v0, p0, Lax/b;->A:[Lax/y;

    const/4 v0, 0x1

    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lax/b;->A:[Lax/y;

    add-int/lit8 v2, v0, -0x1

    aget-object v3, p1, v0

    invoke-static {v3}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lax/b;)I
    .locals 1

    iget v0, p0, Lax/b;->O:I

    return v0
.end method

.method static synthetic a(Lax/b;I)I
    .locals 0

    iput p1, p0, Lax/b;->O:I

    return p1
.end method

.method private static a(ILcom/google/googlenav/ui/m;)Lax/b;
    .locals 1

    packed-switch p0, :pswitch_data_0

    new-instance v0, Lax/s;

    invoke-direct {v0, p1}, Lax/s;-><init>(Lcom/google/googlenav/ui/m;)V

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lax/w;

    invoke-direct {v0, p1}, Lax/w;-><init>(Lcom/google/googlenav/ui/m;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lax/x;

    invoke-direct {v0, p1}, Lax/x;-><init>(Lcom/google/googlenav/ui/m;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lax/i;

    invoke-direct {v0, p1}, Lax/i;-><init>(Lcom/google/googlenav/ui/m;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;
    .locals 3

    const/4 v2, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    check-cast p0, Ljava/io/InputStream;

    invoke-virtual {v0, p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed directions stream. directionsResponse is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-static {v1, p1}, Lax/b;->a(ILcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lax/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed directions stream."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lax/b;->b(Z)V

    return-object v1
.end method

.method protected static a(Lax/y;Lax/y;)Lax/y;
    .locals 2

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {p0}, Lax/y;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lax/y;->t()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-virtual {p0}, Lax/y;->h()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1}, Lax/y;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lax/y;->a(Lax/y;Ljava/lang/String;Ljava/lang/String;)Lax/y;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LaN/B;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    invoke-static {p0}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0xd

    invoke-static {p0}, LaN/C;->a(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method private a(Lax/y;[Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v4, 0x7

    const/4 v1, 0x0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bA;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {p1}, Lax/y;->a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    if-eqz p2, :cond_2

    :goto_1
    array-length v0, p2

    if-ge v1, v0, :cond_2

    aget-object v0, p2, v1

    invoke-virtual {v0, p1}, Lax/y;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lax/y;->a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p1, :cond_0

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-object v0
.end method

.method protected static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V
    .locals 2

    invoke-static {p1}, Lax/y;->a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private aU()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/b;->s:Z

    return-void
.end method

.method private static aV()I
    .locals 2

    sget-object v1, Lax/b;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget v0, Lax/b;->n:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lax/b;->n:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private aW()Ljava/lang/String;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lax/b;->Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lax/b;->R()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const/16 v3, 0x5be

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    aput-object v0, v4, v5

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Lax/b;->N()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lax/b;->M()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const/16 v3, 0x377

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    aput-object v0, v4, v5

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lax/b;->O()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lax/b;->am()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method static synthetic b(Lax/b;I)I
    .locals 0

    iput p1, p0, Lax/b;->M:I

    return p1
.end method

.method static synthetic b(Lax/b;)Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->x:Lax/y;

    return-object v0
.end method

.method public static b(II)Z
    .locals 2

    const/4 v0, 0x1

    shl-int v1, v0, p0

    and-int/2addr v1, p1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/b;
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0, v2}, Lax/b;->a(ILcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    const-string v1, ""

    invoke-static {v1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/y;)V

    const-string v1, ""

    invoke-static {v1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->b(Lax/y;)V

    invoke-virtual {v0, p0, v2}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    return-object v0
.end method

.method public static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 1

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 1

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v0, v0

    div-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/h;
    .locals 2

    new-instance v0, Lax/h;

    invoke-direct {v0, p0, p1}, Lax/h;-><init>(Lax/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lax/h;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_0

    if-eq v1, v0, :cond_0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lax/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bA;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v2, 0x11

    iget-object v3, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/m;->f()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/ui/m;->d(J)[B

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v5, v0

    if-eqz v5, :cond_1

    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-virtual {v5, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v7, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_2
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method private static j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;
    .locals 11

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    const/16 v0, 0x8

    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/16 v0, 0x9

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_2

    const/16 v0, 0x9

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    const/16 v0, 0xa

    invoke-virtual {v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v8, :cond_1

    const/16 v9, 0xa

    invoke-virtual {v7, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    const/16 v10, 0xc

    invoke-virtual {v9, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_0

    const/16 v10, 0xc

    invoke-virtual {v9, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    return-object v3
.end method


# virtual methods
.method public A()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lax/b;->U:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lax/b;->p:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, v0}, Lax/b;->t(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lax/b;->t(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public B()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    const/16 v2, 0x314

    invoke-virtual {p0}, Lax/b;->y()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :pswitch_1
    invoke-virtual {p0}, Lax/b;->aD()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v1, 0x6c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lax/b;->aE()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    iget-boolean v0, p0, Lax/b;->U:Z

    if-eqz v0, :cond_2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/16 v0, 0x318

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public C()[Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->w:[Lax/y;

    return-object v0
.end method

.method public D()[Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->z:[Lax/y;

    return-object v0
.end method

.method public E()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lax/b;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Z
    .locals 1

    iget-boolean v0, p0, Lax/b;->R:Z

    return v0
.end method

.method public G()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lax/b;->c:I

    iput-boolean v0, p0, Lax/b;->R:Z

    return-void
.end method

.method public H()Lcom/google/googlenav/ui/m;
    .locals 1

    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    return-object v0
.end method

.method public I()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x5

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x1

    iget v3, p0, Lax/b;->p:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    iget v3, p0, Lax/b;->b:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v0}, Lax/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    iget-object v3, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v3}, Lax/l;->c()Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Date;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v0, 0x4

    iget-object v3, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v3}, Lax/l;->d()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xd

    iget v3, p0, Lax/b;->P:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lax/b;->u:Lax/y;

    iget-object v3, p0, Lax/b;->w:[Lax/y;

    invoke-direct {p0, v0, v3}, Lax/b;->a(Lax/y;[Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lax/b;->x:Lax/y;

    iget-object v3, p0, Lax/b;->z:[Lax/y;

    invoke-direct {p0, v0, v3}, Lax/b;->a(Lax/y;[Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v1

    :goto_0
    iget v3, p0, Lax/b;->J:I

    if-ge v0, v3, :cond_1

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    iget-object v4, p0, Lax/b;->h:[Lax/h;

    aget-object v4, v4, v0

    invoke-virtual {v4, v3}, Lax/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v2, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    iget-object v3, p0, Lax/b;->i:[Lax/d;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    const/16 v3, 0xc

    iget-object v4, p0, Lax/b;->i:[Lax/d;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lax/d;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_2
    iget-object v3, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    const/16 v3, 0x10

    iget-object v4, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v4, v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-direct {p0, v2}, Lax/b;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_3
    iget-object v0, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    const/16 v0, 0xe

    iget-object v3, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v3, v3, v1

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    return-object v2
.end method

.method protected J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lax/b;->I()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lax/b;->u:Lax/y;

    invoke-virtual {v1}, Lax/y;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lax/b;->x:Lax/y;

    invoke-virtual {v1}, Lax/y;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lax/b;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lax/b;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0}, Lax/b;->az()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lax/b;->X()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lax/b;->W()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-object v0
.end method

.method public K()I
    .locals 2

    iget v0, p0, Lax/b;->o:I

    iget v1, p0, Lax/b;->L:I

    add-int/2addr v0, v1

    return v0
.end method

.method abstract L()I
.end method

.method public M()I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->k()I

    move-result v0

    return v0
.end method

.method public N()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->l()Z

    move-result v0

    return v0
.end method

.method public O()I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->m()I

    move-result v0

    return v0
.end method

.method public P()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->n()Z

    move-result v0

    return v0
.end method

.method public Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->s()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public R()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public S()Lax/l;
    .locals 1

    iget-object v0, p0, Lax/b;->t:Lax/l;

    return-object v0
.end method

.method public T()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->h()Z

    move-result v0

    return v0
.end method

.method public U()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lax/h;->g()[LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public V()V
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->w()V

    return-void
.end method

.method public W()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->x()Z

    move-result v0

    goto :goto_0
.end method

.method public X()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lax/h;->u()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lax/b;->ae()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lax/b;->n(I)Lax/t;

    move-result-object v2

    invoke-virtual {v2}, Lax/t;->F()I

    move-result v2

    if-ltz v2, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(LaN/p;)LaN/Y;
    .locals 4

    iget v0, p0, Lax/b;->L:I

    if-ltz v0, :cond_0

    iget v0, p0, Lax/b;->L:I

    iget-object v1, p0, Lax/b;->h:[Lax/h;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    iget v1, p0, Lax/b;->L:I

    aget-object v0, v0, v1

    const/4 v1, 0x2

    new-array v1, v1, [LaN/B;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lax/h;->i()LaN/B;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lax/h;->j()LaN/B;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0}, Lax/h;->d(Lax/h;)LaN/B;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LaN/p;->a([LaN/B;LaN/B;)LaN/Y;

    move-result-object v0

    goto :goto_0
.end method

.method public a(B)V
    .locals 0

    iput-byte p1, p0, Lax/b;->N:B

    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-virtual {p0}, Lax/b;->f()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lax/b;->f()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    :cond_0
    if-gez p1, :cond_1

    const/4 v0, 0x0

    iput-byte v0, p0, Lax/b;->N:B

    iput p1, p0, Lax/b;->M:I

    :goto_0
    return-void

    :cond_1
    iget-byte v0, p0, Lax/b;->N:B

    if-nez v0, :cond_2

    iput p1, p0, Lax/b;->M:I

    const/4 v0, 0x1

    iput-byte v0, p0, Lax/b;->N:B

    goto :goto_0

    :cond_2
    iput p1, p0, Lax/b;->M:I

    goto :goto_0
.end method

.method public a(Lax/e;)V
    .locals 0

    iput-object p1, p0, Lax/b;->X:Lax/e;

    return-void
.end method

.method public a(Lax/l;)V
    .locals 0

    iput-object p1, p0, Lax/b;->t:Lax/l;

    return-void
.end method

.method public a(Lax/y;)V
    .locals 0

    iput-object p1, p0, Lax/b;->u:Lax/y;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lax/b;->a:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lax/b;->U:Z

    return-void
.end method

.method public a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 11

    const/4 v5, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v0, p0, Lax/b;->W:I

    iput-boolean v1, p0, Lax/b;->H:Z

    invoke-virtual {p0}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lax/b;->R:Z

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    array-length v0, v0

    if-nez v0, :cond_15

    :cond_0
    new-array v0, v2, [Lax/h;

    new-instance v3, Lax/h;

    invoke-direct {v3, p0}, Lax/h;-><init>(Lax/b;)V

    aput-object v3, v0, v1

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    move v0, v2

    :goto_0
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lez v3, :cond_2

    const/16 v3, 0x8

    invoke-virtual {p1, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    iget-object v4, p0, Lax/b;->h:[Lax/h;

    aget-object v1, v4, v1

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lax/h;->a(Lax/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    :goto_1
    invoke-direct {p0}, Lax/b;->aU()V

    :cond_1
    :goto_2
    return v2

    :cond_2
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    aget-object v0, v0, v1

    invoke-static {v0, v1}, Lax/h;->a(Lax/h;I)I

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    iput v1, p0, Lax/b;->J:I

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lax/b;->P:I

    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v0

    iput v0, p0, Lax/b;->b:I

    invoke-virtual {p0}, Lax/b;->L()I

    move-result v0

    iput v0, p0, Lax/b;->L:I

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lax/b;->p:I

    iget v0, p0, Lax/b;->p:I

    if-eq v0, v10, :cond_a

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lax/b;->v:Z

    iget v0, p0, Lax/b;->p:I

    if-eq v0, v10, :cond_b

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lax/b;->y:Z

    invoke-virtual {p1, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {p1, v5}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/b;->g:I

    :cond_4
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lax/b;->F:Ljava/lang/String;

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lax/b;->G:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ne v0, v9, :cond_1

    iget-object v0, p0, Lax/b;->u:Lax/y;

    iput-object v0, p0, Lax/b;->B:Lax/y;

    iget-object v0, p0, Lax/b;->x:Lax/y;

    iput-object v0, p0, Lax/b;->C:Lax/y;

    const/4 v0, 0x5

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {p0, v0}, Lax/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    iput-boolean v4, p0, Lax/b;->v:Z

    iget-boolean v4, p0, Lax/b;->v:Z

    if-eqz v4, :cond_5

    if-eqz p2, :cond_c

    array-length v4, p2

    if-lt v4, v2, :cond_c

    aget-object v4, p2, v1

    invoke-static {v4, v3}, Lax/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->u:Lax/y;

    :cond_5
    :goto_5
    invoke-virtual {p0, v0}, Lax/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->w:[Lax/y;

    iget-boolean v3, p0, Lax/b;->v:Z

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_d

    move v0, v2

    :goto_6
    or-int/2addr v0, v3

    iput-boolean v0, p0, Lax/b;->v:Z

    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {p0, v0}, Lax/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    iput-boolean v4, p0, Lax/b;->y:Z

    iget-boolean v4, p0, Lax/b;->y:Z

    if-eqz v4, :cond_6

    if-eqz p2, :cond_e

    array-length v4, p2

    if-lt v4, v9, :cond_e

    aget-object v4, p2, v2

    invoke-static {v4, v3}, Lax/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->x:Lax/y;

    :cond_6
    :goto_7
    invoke-virtual {p0, v0}, Lax/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->z:[Lax/y;

    iget-boolean v3, p0, Lax/b;->y:Z

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_f

    move v0, v2

    :goto_8
    or-int/2addr v0, v3

    iput-boolean v0, p0, Lax/b;->y:Z

    const/16 v0, 0x13

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lax/b;->I:Z

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    iput v0, p0, Lax/b;->J:I

    iget v0, p0, Lax/b;->J:I

    invoke-virtual {p0}, Lax/b;->p()I

    move-result v3

    if-le v0, v3, :cond_7

    invoke-virtual {p0}, Lax/b;->p()I

    move-result v0

    iput v0, p0, Lax/b;->J:I

    iput v5, p0, Lax/b;->p:I

    :cond_7
    iget v0, p0, Lax/b;->J:I

    new-array v0, v0, [Lax/h;

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    iput-boolean v2, p0, Lax/b;->K:Z

    move v0, v1

    :goto_9
    :try_start_0
    iget v3, p0, Lax/b;->J:I

    if-ge v0, v3, :cond_10

    const/16 v3, 0x8

    invoke-virtual {p1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    iget-object v4, p0, Lax/b;->h:[Lax/h;

    invoke-direct {p0, v3}, Lax/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/h;

    move-result-object v3

    aput-object v3, v4, v0

    iget-object v3, p0, Lax/b;->h:[Lax/h;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lax/h;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    iput-boolean v3, p0, Lax/b;->K:Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_9
    iput v5, p0, Lax/b;->p:I

    goto/16 :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_4

    :cond_c
    iget-object v4, p0, Lax/b;->u:Lax/y;

    invoke-static {v3, v0}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    invoke-static {v4, v3}, Lax/b;->a(Lax/y;Lax/y;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->u:Lax/y;

    goto/16 :goto_5

    :cond_d
    move v0, v1

    goto/16 :goto_6

    :cond_e
    iget-object v4, p0, Lax/b;->x:Lax/y;

    invoke-static {v3, v0}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    invoke-static {v4, v3}, Lax/b;->a(Lax/y;Lax/y;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->x:Lax/y;

    goto/16 :goto_7

    :cond_f
    move v0, v1

    goto :goto_8

    :catch_0
    move-exception v3

    iput v0, p0, Lax/b;->J:I

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    iget v3, p0, Lax/b;->J:I

    const/4 v4, 0x0

    aput-object v4, v0, v3

    iget v0, p0, Lax/b;->J:I

    if-nez v0, :cond_10

    iput v5, p0, Lax/b;->p:I

    :cond_10
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-array v0, v3, [Lax/d;

    iput-object v0, p0, Lax/b;->i:[Lax/d;

    move v0, v1

    :goto_a
    if-ge v0, v3, :cond_11

    const/16 v4, 0xc

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    iget-object v5, p0, Lax/b;->i:[Lax/d;

    new-instance v6, Lax/d;

    invoke-virtual {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v10}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, p0, v7, v8, v4}, Lax/d;-><init>(Lax/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_11
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-array v0, v3, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :goto_b
    if-ge v0, v3, :cond_12

    iget-object v4, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v5, 0x10

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    aput-object v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_12
    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {p1}, Lax/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/ui/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;)V

    :cond_13
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v3, v0, [I

    iput-object v3, p0, Lax/b;->l:[I

    new-array v3, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v3, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_c
    if-ge v1, v0, :cond_14

    const/16 v3, 0xe

    invoke-virtual {p1, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    iget-object v4, p0, Lax/b;->l:[I

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    aput v5, v4, v1

    iget-object v4, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aput-object v3, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    :cond_14
    invoke-direct {p0}, Lax/b;->aU()V

    goto/16 :goto_2

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 3

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v0}, Lax/l;->d()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    iget v2, p0, Lax/b;->g:I

    invoke-static {v0, v2}, Lax/l;->a(Ljava/util/Date;I)Lax/l;

    move-result-object v0

    iput-object v0, p0, Lax/b;->t:Lax/l;

    :cond_0
    invoke-virtual {p0}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    :goto_0
    return v1

    :cond_1
    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    goto :goto_0
.end method

.method public a(II)[LaN/B;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v3

    if-nez v3, :cond_1

    new-array v0, v1, [LaN/B;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-ltz p1, :cond_2

    if-gez p2, :cond_3

    :cond_2
    invoke-virtual {v3}, Lax/h;->g()[LaN/B;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v3, p1}, Lax/h;->e(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3, p2}, Lax/h;->e(I)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    new-array v0, v1, [LaN/B;

    goto :goto_0

    :cond_5
    invoke-virtual {v3, p1}, Lax/h;->f(I)I

    move-result v2

    invoke-virtual {v3, p2}, Lax/h;->f(I)I

    move-result v4

    sub-int v0, v4, v2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [LaN/B;

    move v1, v2

    :goto_1
    if-gt v1, v4, :cond_0

    sub-int v5, v1, v2

    invoke-virtual {v3}, Lax/h;->g()[LaN/B;

    move-result-object v6

    aget-object v6, v6, v1

    aput-object v6, v0, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public aA()I
    .locals 1

    iget v0, p0, Lax/b;->J:I

    return v0
.end method

.method public aB()LaN/B;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->i()LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lax/b;->u:Lax/y;

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public aC()LaN/B;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->j()LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lax/b;->x:Lax/y;

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public aD()Z
    .locals 2

    iget v0, p0, Lax/b;->p:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lax/b;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aE()Z
    .locals 2

    iget v0, p0, Lax/b;->p:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lax/b;->y:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aF()[I
    .locals 1

    iget-object v0, p0, Lax/b;->l:[I

    return-object v0
.end method

.method public aG()Ljava/lang/String;
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lax/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    const/16 v1, 0x600

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lax/h;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method abstract aH()Lax/b;
.end method

.method public aI()Lax/b;
    .locals 2

    invoke-virtual {p0}, Lax/b;->aH()Lax/b;

    move-result-object v0

    invoke-virtual {p0}, Lax/b;->S()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/l;)V

    invoke-virtual {p0}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lax/b;->aN()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/b;->v(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lax/b;->b(Z)V

    return-object v0
.end method

.method public aJ()Lax/b;
    .locals 2

    invoke-virtual {p0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    invoke-virtual {p0}, Lax/b;->J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/b;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public aL()Z
    .locals 1

    iget-boolean v0, p0, Lax/b;->H:Z

    return v0
.end method

.method public aM()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/b;->H:Z

    return-void
.end method

.method public aN()I
    .locals 1

    iget v0, p0, Lax/b;->T:I

    return v0
.end method

.method public aO()Lax/b;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lax/b;->t(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lax/s;

    invoke-virtual {p0}, Lax/b;->ap()Lax/j;

    move-result-object v1

    invoke-direct {v0, v1}, Lax/s;-><init>(Lax/k;)V

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget v1, p0, Lax/b;->T:I

    invoke-virtual {v0, v1}, Lax/b;->v(I)V

    :cond_1
    return-object v0

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lax/b;->t(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lax/w;

    invoke-virtual {p0}, Lax/b;->ap()Lax/j;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    goto :goto_0
.end method

.method public aP()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/b;->V:Ljava/lang/String;

    return-object v0
.end method

.method public aQ()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lax/b;->b:I

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x108

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const/16 v0, 0x5ca

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x607

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x59

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public aT()Z
    .locals 1

    iget-boolean v0, p0, Lax/b;->S:Z

    return v0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aa()Z
    .locals 1

    iget-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ab()Lax/h;
    .locals 1

    iget v0, p0, Lax/b;->L:I

    invoke-virtual {p0, v0}, Lax/b;->l(I)Lax/h;

    move-result-object v0

    return-object v0
.end method

.method public ac()Ljava/util/List;
    .locals 2

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lax/h;->c()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public ad()[LaN/B;
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v0}, Lax/b;->a(II)[LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public ae()I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lax/h;->e()I

    move-result v0

    goto :goto_0
.end method

.method public af()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lax/b;->ae()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->f()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public ag()I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-static {v0}, Lax/h;->a(Lax/h;)I

    move-result v0

    return v0
.end method

.method public ah()I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-static {v0}, Lax/h;->b(Lax/h;)I

    move-result v0

    return v0
.end method

.method public ai()LaN/B;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-static {v0}, Lax/h;->c(Lax/h;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public aj()I
    .locals 1

    iget v0, p0, Lax/b;->E:I

    return v0
.end method

.method public ak()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/b;->F:Ljava/lang/String;

    return-object v0
.end method

.method public al()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/b;->G:Ljava/lang/String;

    return-object v0
.end method

.method public am()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public an()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lax/b;->L:I

    invoke-virtual {p0, v0}, Lax/b;->p(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ao()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lax/b;->aW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lax/b;->an()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ap()Lax/j;
    .locals 4

    new-instance v0, Lax/j;

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-virtual {p0}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public aq()Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->u:Lax/y;

    return-object v0
.end method

.method public ar()Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->B:Lax/y;

    return-object v0
.end method

.method public as()Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->x:Lax/y;

    return-object v0
.end method

.method public at()Lax/y;
    .locals 1

    iget-object v0, p0, Lax/b;->C:Lax/y;

    return-object v0
.end method

.method public au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public av()I
    .locals 1

    iget v0, p0, Lax/b;->P:I

    return v0
.end method

.method public aw()LaN/B;
    .locals 2

    iget v0, p0, Lax/b;->L:I

    if-ltz v0, :cond_0

    iget v0, p0, Lax/b;->L:I

    iget-object v1, p0, Lax/b;->h:[Lax/h;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    iget v1, p0, Lax/b;->L:I

    aget-object v0, v0, v1

    invoke-static {v0}, Lax/h;->d(Lax/h;)LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public ax()Z
    .locals 1

    iget-boolean v0, p0, Lax/b;->I:Z

    return v0
.end method

.method public ay()I
    .locals 1

    iget v0, p0, Lax/b;->k:I

    return v0
.end method

.method public az()I
    .locals 1

    iget v0, p0, Lax/b;->L:I

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x1c

    return v0
.end method

.method public b(I)Lcom/google/googlenav/E;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lax/h;->a(I)Lax/m;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lax/y;)V
    .locals 0

    iput-object p1, p0, Lax/b;->x:Lax/y;

    return-void
.end method

.method public b(Ljava/io/DataOutput;)V
    .locals 1

    invoke-virtual {p0}, Lax/b;->J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lax/b;->V:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lax/b;->S:Z

    return-void
.end method

.method protected b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lax/y;
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x7

    const/4 v5, 0x1

    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    if-eq v1, v5, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-array v1, v3, [Lax/y;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4, p1}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v4

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    array-length v2, v1

    if-le v2, v5, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lax/b;->M:I

    return v0
.end method

.method public c(I)I
    .locals 0

    return p1
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x1

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v1, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lax/b;->a(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-virtual {p0, v0}, Lax/b;->r(I)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    iget-object v1, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_3

    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iget v2, p0, Lax/b;->g:I

    invoke-static {v1, v2}, Lax/l;->a(Ljava/util/Date;I)Lax/l;

    move-result-object v1

    iput-object v1, p0, Lax/b;->t:Lax/l;

    :goto_1
    if-eqz v0, :cond_2

    const/4 v1, 0x4

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p0, v1}, Lax/b;->s(I)V

    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lax/b;->V()V

    :cond_2
    return v0

    :cond_3
    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v1

    iput-object v1, p0, Lax/b;->t:Lax/l;

    goto :goto_1
.end method

.method public c_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()B
    .locals 1

    iget-byte v0, p0, Lax/b;->N:B

    return v0
.end method

.method public d(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v3, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v0

    invoke-static {v3, v0}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V

    move v0, v1

    :goto_0
    iget-object v4, p0, Lax/b;->A:[Lax/y;

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lax/b;->A:[Lax/y;

    aget-object v4, v4, v0

    invoke-static {v3, v4}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-static {v3, v0}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V

    const/4 v0, 0x5

    invoke-virtual {p0}, Lax/b;->p()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x7

    iget v4, p0, Lax/b;->c:I

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xd

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1e

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1f

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    iget-object v4, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/16 v0, 0xb

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1c

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :goto_1
    iget-object v4, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    const/16 v4, 0xa

    iget-object v5, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v5, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/16 v0, 0x10

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x15

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x22

    invoke-virtual {p0}, Lax/b;->aT()Z

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1b

    invoke-virtual {p0}, Lax/b;->m()Z

    move-result v4

    if-nez v4, :cond_3

    move v1, v2

    :cond_3
    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v3
.end method

.method public final d_()V
    .locals 0

    return-void
.end method

.method public e()Lcom/google/googlenav/E;
    .locals 2

    invoke-virtual {p0}, Lax/b;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lax/b;->g:I

    return-void
.end method

.method public f()I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lax/h;->d()I

    move-result v0

    goto :goto_0
.end method

.method public f(I)J
    .locals 2

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->h(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g(I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lax/b;->l(I)Lax/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lax/h;->p()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public h(I)LaN/B;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->c(I)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public i(I)Lo/D;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->d(I)Lo/D;

    move-result-object v0

    return-object v0
.end method

.method public j(I)Lax/d;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lax/b;->i:[Lax/d;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lax/b;->i:[Lax/d;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lax/b;->b:I

    return v0
.end method

.method public k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public l(I)Lax/h;
    .locals 1

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    iget v0, p0, Lax/b;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m(I)I
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->g(I)I

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lax/b;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n(I)Lax/t;
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->b(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .locals 2

    iget v0, p0, Lax/b;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o(I)V
    .locals 0

    iput p1, p0, Lax/b;->E:I

    return-void
.end method

.method public o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public abstract p()I
.end method

.method public p(I)Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lax/b;->K:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lax/b;->g(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public q(I)Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->i(I)Z

    move-result v0

    return v0
.end method

.method public r()I
    .locals 1

    iget v0, p0, Lax/b;->g:I

    return v0
.end method

.method public r(I)V
    .locals 0

    iput p1, p0, Lax/b;->k:I

    return-void
.end method

.method public s(I)V
    .locals 1

    iput p1, p0, Lax/b;->L:I

    const/4 v0, 0x0

    iput v0, p0, Lax/b;->M:I

    return-void
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public s_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public t()V
    .locals 1

    iget-object v0, p0, Lax/b;->X:Lax/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/b;->X:Lax/e;

    invoke-interface {v0}, Lax/e;->a()V

    :cond_0
    return-void
.end method

.method public t(I)Z
    .locals 1

    iget v0, p0, Lax/b;->T:I

    invoke-static {p1, v0}, Lax/b;->b(II)Z

    move-result v0

    return v0
.end method

.method public t_()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->E()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u(I)V
    .locals 2

    invoke-virtual {p0, p1}, Lax/b;->t(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lax/b;->T:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    sub-int/2addr v0, v1

    iput v0, p0, Lax/b;->T:I

    :cond_0
    return-void
.end method

.method public u()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public v(I)V
    .locals 0

    iput p1, p0, Lax/b;->T:I

    return-void
.end method

.method public v()Z
    .locals 1

    iget-boolean v0, p0, Lax/b;->s:Z

    return v0
.end method

.method public w()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lax/b;->s:Z

    return-void
.end method

.method public x()Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->C()[Lax/y;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lax/b;->D()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()I
    .locals 1

    iget v0, p0, Lax/b;->p:I

    return v0
.end method

.method public z()Z
    .locals 1

    iget v0, p0, Lax/b;->p:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
