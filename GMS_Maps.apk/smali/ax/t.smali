.class public Lax/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lax/v;

.field protected b:Lax/v;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final d:[Lax/n;

.field private final e:[Lax/u;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Z

.field private j:I

.field private k:LaN/B;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-direct {p0, v0}, Lax/t;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/16 v3, 0x15

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lax/t;->j:I

    iput v2, p0, Lax/t;->j:I

    iput-object p1, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, Lax/t;->S()[Lax/n;

    move-result-object v0

    iput-object v0, p0, Lax/t;->d:[Lax/n;

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v0, v0, [Lax/u;

    iput-object v0, p0, Lax/t;->e:[Lax/u;

    invoke-direct {p0}, Lax/t;->T()V

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x13

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lax/t;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v1

    iput v1, p0, Lax/t;->f:I

    invoke-static {v0}, Lax/t;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v1

    iput v1, p0, Lax/t;->g:I

    const/4 v1, 0x7

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lax/t;->h:I

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lax/t;->i:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lax/t;->i:Z

    goto :goto_0
.end method

.method private S()[Lax/n;
    .locals 11

    const/16 v2, 0x13

    const/4 v10, 0x5

    const/4 v9, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-array v0, v1, [Lax/n;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    new-array v2, v5, [Lax/n;

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_2
    new-instance v7, Lax/n;

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v0, v6}, Lax/n;-><init>(ILjava/lang/String;)V

    aput-object v7, v2, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private T()V
    .locals 14

    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lax/t;->E()I

    move-result v0

    if-ne v0, v10, :cond_0

    invoke-static {p0}, Lax/v;->a(Lax/t;)Lax/v;

    move-result-object v0

    iput-object v0, p0, Lax/t;->a:Lax/v;

    :cond_0
    iget-object v6, p0, Lax/t;->a:Lax/v;

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    iget-object v0, p0, Lax/t;->e:[Lax/u;

    array-length v0, v0

    if-ge v7, v0, :cond_2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v3

    :goto_1
    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v4

    :goto_2
    invoke-virtual {v0, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v5

    :goto_3
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x6

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x6

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget v6, v6, Lax/v;->b:I

    invoke-static {v0, v6}, Lax/v;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lax/v;

    move-result-object v6

    iget-object v9, p0, Lax/t;->e:[Lax/u;

    new-instance v0, Lax/u;

    invoke-direct/range {v0 .. v6}, Lax/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LaN/B;Lax/v;)V

    aput-object v0, v9, v7

    invoke-static {v6}, Lax/v;->a(Lax/v;)Lax/v;

    move-result-object v6

    :goto_4
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lax/t;->e:[Lax/u;

    new-instance v0, Lax/u;

    invoke-direct/range {v0 .. v6}, Lax/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;LaN/B;Lax/v;)V

    aput-object v0, v9, v7

    goto :goto_4

    :cond_2
    iput-object v6, p0, Lax/t;->b:Lax/v;

    return-void

    :cond_3
    move-object v5, v8

    goto :goto_3

    :cond_4
    move-object v4, v8

    goto :goto_2

    :cond_5
    move-object v3, v8

    goto :goto_1
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/16 v2, 0x12

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private e(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lax/t;->d:[Lax/n;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lax/t;->d:[Lax/n;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lax/n;->a()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1}, Lax/n;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1c

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public B()I
    .locals 2

    iget v0, p0, Lax/t;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/t;->j:I

    :cond_0
    iget v0, p0, Lax/t;->j:I

    return v0
.end method

.method public C()Z
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    return v0
.end method

.method public D()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lax/t;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/t;->l:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lax/t;->e(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Lax/t;->l:Ljava/lang/String;

    iget-object v0, p0, Lax/t;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public E()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public F()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public G()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()[Lax/n;
    .locals 1

    iget-object v0, p0, Lax/t;->d:[Lax/n;

    return-object v0
.end method

.method public I()I
    .locals 3

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed()I

    move-result v0

    add-int/lit8 v1, v0, 0x14

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lax/t;->d:[Lax/n;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lax/t;->d:[Lax/n;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/n;->d()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public J()Z
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public K()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public L()Z
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public M()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public N()Z
    .locals 1

    iget-boolean v0, p0, Lax/t;->m:Z

    return v0
.end method

.method public O()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/t;->m:Z

    return-void
.end method

.method public P()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lax/t;->m:Z

    return-void
.end method

.method public Q()[Lax/u;
    .locals 1

    iget-object v0, p0, Lax/t;->e:[Lax/u;

    return-object v0
.end method

.method public R()Z
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x17

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public a()Lax/v;
    .locals 1

    iget-object v0, p0, Lax/t;->a:Lax/v;

    return-object v0
.end method

.method public a(Lax/b;)Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lax/t;->i:Z

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/t;->F()I

    move-result v0

    invoke-virtual {p1, v0}, Lax/b;->j(I)Lax/d;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lax/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lax/d;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lax/d;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(J)V
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(LaN/B;)V
    .locals 0

    iput-object p1, p0, Lax/t;->k:LaN/B;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public b()Lax/v;
    .locals 1

    iget-object v0, p0, Lax/t;->b:Lax/v;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lax/t;->f:I

    return v0
.end method

.method public c(I)V
    .locals 2

    iput p1, p0, Lax/t;->j:I

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lax/t;->g:I

    return v0
.end method

.method public d(I)V
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lax/t;->h:I

    return v0
.end method

.method public f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public g()LaN/B;
    .locals 1

    iget-object v0, p0, Lax/t;->k:LaN/B;

    return-object v0
.end method

.method public h()Lo/D;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x19

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lo/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public k()LaN/B;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6d

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1d

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public q()LaN/B;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public s()Z
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public t()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public u()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public w()Z
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public x()I
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public y()J
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lax/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
