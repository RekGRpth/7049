.class public Lax/w;
.super Lax/b;
.source "SourceFile"


# instance fields
.field private m:I

.field private n:I

.field private o:J

.field private p:Z


# direct methods
.method public constructor <init>(Lax/k;Lcom/google/googlenav/ui/m;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lax/b;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    const/4 v0, -0x1

    iput v0, p0, Lax/w;->m:I

    const/4 v0, 0x1

    iput v0, p0, Lax/w;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/m;)V
    .locals 1

    invoke-direct {p0, p1}, Lax/b;-><init>(Lcom/google/googlenav/ui/m;)V

    const/4 v0, -0x1

    iput v0, p0, Lax/w;->m:I

    const/4 v0, 0x1

    iput v0, p0, Lax/w;->b:I

    return-void
.end method

.method public constructor <init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lax/b;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    const/4 v0, -0x1

    iput v0, p0, Lax/w;->m:I

    const/4 v0, 0x1

    iput v0, p0, Lax/w;->b:I

    return-void
.end method


# virtual methods
.method protected J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    invoke-super {p0}, Lax/b;->J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-boolean v1, p0, Lax/w;->p:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-object v0
.end method

.method L()I
    .locals 1

    iget v0, p0, Lax/w;->m:I

    return v0
.end method

.method public V()V
    .locals 1

    invoke-super {p0}, Lax/b;->V()V

    invoke-virtual {p0}, Lax/w;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lax/w;->n:I

    :cond_0
    return-void
.end method

.method public a(ILcom/google/googlenav/ui/m;Z)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0, p1}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x65

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lax/m;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p3, :cond_2

    invoke-virtual {p0, p1}, Lax/w;->f(I)J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/google/googlenav/ui/m;->a(J)C

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lax/w;->f(I)J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/google/googlenav/ui/m;->a(J)C

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lax/m;)Ljava/lang/String;
    .locals 4

    const/16 v2, 0x11

    const/4 v3, 0x1

    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lax/w;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v2, Lcom/google/googlenav/ui/bi;->A:C

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(I)V
    .locals 1

    invoke-super {p0, p1}, Lax/b;->a(I)V

    invoke-virtual {p0}, Lax/w;->c()I

    move-result v0

    iput v0, p0, Lax/w;->n:I

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lax/w;->d(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lax/w;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lax/w;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v1, p0, Lax/w;->g:I

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    :goto_0
    const/16 v1, 0x24

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    iget-object v2, p0, Lax/w;->e:Lcom/google/googlenav/ui/m;

    invoke-static {v2}, Lcom/google/googlenav/ui/m;->a(Lcom/google/googlenav/ui/m;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x21

    invoke-virtual {v0, v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lax/w;->aj()I

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {p0}, Lax/w;->aj()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lax/w;->o:J

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lax/w;->S()Lax/l;

    move-result-object v1

    invoke-virtual {v1}, Lax/l;->b()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x2

    invoke-static {}, Lcom/google/googlenav/ui/bd;->a()I

    move-result v3

    const v4, 0xea60

    div-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lax/w;->S()Lax/l;

    move-result-object v2

    invoke-virtual {v2}, Lax/l;->c()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v6, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lax/w;->S()Lax/l;

    move-result-object v1

    invoke-virtual {v1}, Lax/l;->d()I

    move-result v1

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public aH()Lax/b;
    .locals 3

    new-instance v0, Lax/w;

    invoke-virtual {p0}, Lax/w;->ap()Lax/j;

    move-result-object v1

    invoke-virtual {p0}, Lax/w;->H()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    return-object v0
.end method

.method public aU()I
    .locals 1

    iget v0, p0, Lax/w;->n:I

    return v0
.end method

.method public aV()J
    .locals 2

    iget-wide v0, p0, Lax/w;->o:J

    return-wide v0
.end method

.method public aW()Z
    .locals 1

    iget-boolean v0, p0, Lax/w;->p:Z

    return v0
.end method

.method public aX()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lax/w;->ab()Lax/h;

    move-result-object v3

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lax/h;->e()I

    move-result v4

    if-ge v0, v4, :cond_0

    invoke-virtual {v3, v0}, Lax/h;->b(I)Lax/t;

    move-result-object v4

    invoke-virtual {v4}, Lax/t;->E()I

    move-result v4

    if-ne v4, v2, :cond_1

    move v1, v2

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lax/w;->p:Z

    return-void
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 2

    invoke-super {p0, p1}, Lax/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    iput-boolean v1, p0, Lax/w;->p:Z

    :cond_0
    return v0
.end method

.method public p()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public s(I)V
    .locals 1

    invoke-virtual {p0}, Lax/w;->az()I

    move-result v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lax/w;->n:I

    :cond_0
    invoke-super {p0, p1}, Lax/b;->s(I)V

    return-void
.end method

.method public u()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public w(I)V
    .locals 0

    iput p1, p0, Lax/w;->m:I

    return-void
.end method
