.class public Lay/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaN/B;

.field private final b:D


# direct methods
.method public constructor <init>(LaN/B;D)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lay/a;->a:LaN/B;

    iput-wide p2, p0, Lay/a;->b:D

    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lay/a;->a:LaN/B;

    return-object v0
.end method

.method public b()D
    .locals 2

    iget-wide v0, p0, Lay/a;->b:D

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    instance-of v1, p1, Lay/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lay/a;

    iget-object v1, p0, Lay/a;->a:LaN/B;

    iget-object v2, p1, Lay/a;->a:LaN/B;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lay/a;->b:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-wide v2, p1, Lay/a;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lay/a;->a:LaN/B;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lay/a;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "mapPoint"

    iget-object v2, p0, Lay/a;->a:LaN/B;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "elevation"

    iget-wide v2, p0, Lay/a;->b:D

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/G;->a(Ljava/lang/String;D)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
