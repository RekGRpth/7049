.class public Laz/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Laz/c;


# instance fields
.field private final a:[Laz/b;

.field private final b:Ljava/util/Hashtable;

.field private final c:Law/h;

.field private d:Z


# direct methods
.method private constructor <init>(Law/h;[Laz/b;)V
    .locals 6

    const v5, 0x155cc0

    const v4, 0x13d620

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Laz/c;->b:Ljava/util/Hashtable;

    iput-boolean v0, p0, Laz/c;->d:Z

    iput-object p2, p0, Laz/c;->a:[Laz/b;

    iput-object p1, p0, Laz/c;->c:Law/h;

    iget-object v1, p0, Laz/c;->a:[Laz/b;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Bad KNE"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_5

    aget-object v2, p2, v0

    instance-of v3, v2, Laz/l;

    if-eqz v3, :cond_2

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicate version control experiments: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v2}, Laz/b;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v1, 0x1

    :cond_2
    invoke-interface {v2}, Laz/b;->a()I

    move-result v3

    if-lt v3, v4, :cond_3

    invoke-interface {v2}, Laz/b;->a()I

    move-result v3

    if-lt v3, v5, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad experiment id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v2}, Laz/b;->a()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " <= "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v2}, Laz/b;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    array-length v0, p2

    if-lez v0, :cond_6

    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No control experiment."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-direct {p0}, Laz/c;->e()V

    invoke-direct {p0}, Laz/c;->d()V

    new-instance v0, Laz/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laz/e;-><init>(Laz/c;Laz/d;)V

    invoke-virtual {p1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method static synthetic a(Laz/c;I)Laz/b;
    .locals 1

    invoke-direct {p0, p1}, Laz/c;->c(I)Laz/b;

    move-result-object v0

    return-object v0
.end method

.method public static a()Laz/c;
    .locals 1

    sget-object v0, Laz/c;->e:Laz/c;

    return-object v0
.end method

.method public static a(Law/h;[Laz/b;)V
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Bad drd"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Laz/c;

    invoke-direct {v0, p0, p1}, Laz/c;-><init>(Law/h;[Laz/b;)V

    sput-object v0, Laz/c;->e:Laz/c;

    return-void
.end method

.method private a(Laz/b;)V
    .locals 1

    iget-object v0, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Laz/b;->b()V

    return-void
.end method

.method static synthetic a(Laz/c;Laz/b;)V
    .locals 0

    invoke-direct {p0, p1}, Laz/c;->a(Laz/b;)V

    return-void
.end method

.method static synthetic a(Laz/c;)[Laz/b;
    .locals 1

    iget-object v0, p0, Laz/c;->a:[Laz/b;

    return-object v0
.end method

.method static synthetic b(Laz/c;)Z
    .locals 1

    iget-boolean v0, p0, Laz/c;->d:Z

    return v0
.end method

.method private c(I)Laz/b;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Laz/c;->a:[Laz/b;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Laz/c;->a:[Laz/b;

    aget-object v1, v1, v0

    invoke-interface {v1}, Laz/b;->a()I

    move-result v1

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Laz/c;->a:[Laz/b;

    aget-object v0, v1, v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Laz/f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Laz/f;-><init>(ILaz/d;)V

    iget-object v1, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz/b;

    goto :goto_1
.end method

.method static synthetic c(Laz/c;)Ljava/util/Hashtable;
    .locals 1

    invoke-direct {p0}, Laz/c;->f()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Laz/c;)Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Laz/c;->b:Ljava/util/Hashtable;

    return-object v0
.end method

.method private d()V
    .locals 6

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Laz/c;->b()[Laz/b;

    move-result-object v3

    array-length v0, v3

    new-array v4, v0, [I

    :try_start_0
    array-length v0, v3

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_0

    aget-object v5, v3, v0

    invoke-interface {v5}, Laz/b;->a()I

    move-result v5

    aput v5, v4, v0

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v2, "Experiments"

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    iget-object v0, p0, Laz/c;->c:Law/h;

    invoke-virtual {v0, v4}, Law/h;->a([I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private e()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "Experiments"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v3

    invoke-direct {p0, v3}, Laz/c;->c(I)Laz/b;

    move-result-object v3

    invoke-direct {p0, v3}, Laz/c;->a(Laz/b;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "FLASH"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "Experiments"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    goto :goto_0
.end method

.method static synthetic e(Laz/c;)V
    .locals 0

    invoke-direct {p0}, Laz/c;->d()V

    return-void
.end method

.method private f()Ljava/util/Hashtable;
    .locals 3

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iget-object v0, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz/b;

    invoke-virtual {v1, v0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(I)V
    .locals 2

    invoke-direct {p0, p1}, Laz/c;->c(I)Laz/b;

    move-result-object v0

    invoke-interface {v0}, Laz/b;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Laz/b;->b()V

    invoke-direct {p0}, Laz/c;->d()V

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 2

    invoke-direct {p0, p1}, Laz/c;->c(I)Laz/b;

    move-result-object v0

    invoke-interface {v0}, Laz/b;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Laz/b;->c()V

    invoke-direct {p0}, Laz/c;->d()V

    :cond_0
    return-void
.end method

.method public b()[Laz/b;
    .locals 5

    iget-object v0, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    new-array v3, v0, [Laz/b;

    iget-object v0, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz/b;

    add-int/lit8 v2, v1, 0x1

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public c()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Laz/c;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz/b;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {v0}, Laz/b;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
