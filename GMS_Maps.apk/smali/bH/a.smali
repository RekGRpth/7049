.class public abstract LbH/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LbH/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public r()[B
    .locals 3

    :try_start_0
    invoke-virtual {p0}, LbH/a;->g()I

    move-result v0

    new-array v0, v0, [B

    invoke-static {v0}, LbH/g;->a([B)LbH/g;

    move-result-object v1

    invoke-virtual {p0, v1}, LbH/a;->a(LbH/g;)V

    invoke-virtual {v1}, LbH/g;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
