.class public abstract LbH/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LbH/q;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(LbH/p;)LbH/x;
    .locals 1

    new-instance v0, LbH/x;

    invoke-direct {v0, p0}, LbH/x;-><init>(LbH/p;)V

    return-object v0
.end method


# virtual methods
.method public a(LbH/f;)LbH/b;
    .locals 1

    invoke-static {}, LbH/i;->a()LbH/i;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LbH/b;->b(LbH/f;LbH/i;)LbH/b;

    move-result-object v0

    return-object v0
.end method

.method public a([B)LbH/b;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LbH/b;->a([BII)LbH/b;

    move-result-object v0

    return-object v0
.end method

.method public a([BII)LbH/b;
    .locals 3

    :try_start_0
    invoke-static {p1, p2, p3}, LbH/f;->a([BII)LbH/f;

    move-result-object v0

    invoke-virtual {p0, v0}, LbH/b;->a(LbH/f;)LbH/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LbH/f;->a(I)V
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public abstract b(LbH/f;LbH/i;)LbH/b;
.end method

.method public synthetic b([B)LbH/q;
    .locals 1

    invoke-virtual {p0, p1}, LbH/b;->a([B)LbH/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LbH/b;->e()LbH/b;

    move-result-object v0

    return-object v0
.end method

.method public abstract e()LbH/b;
.end method
