.class public abstract Lbb/c;
.super Lbb/f;
.source "SourceFile"


# instance fields
.field private volatile a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lbb/f;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbb/c;->a:Z

    return-void
.end method


# virtual methods
.method public L_()Z
    .locals 1

    invoke-super {p0}, Lbb/f;->L_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbb/c;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a_(Lbb/s;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbb/c;->a:Z

    invoke-virtual {p0, p1}, Lbb/c;->b(Lbb/s;)Z

    move-result v0

    invoke-virtual {p0, p1}, Lbb/c;->c(Lbb/s;)Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    iput-boolean v3, p0, Lbb/c;->a:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lbb/c;->d(Lbb/s;)Lbb/z;

    move-result-object v2

    iput-boolean v3, p0, Lbb/c;->a:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2, v3}, Lbb/c;->a(Lbb/z;Z)V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_0

    invoke-virtual {p0, v2}, Lbb/c;->a(Lbb/z;)V

    goto :goto_0
.end method

.method protected abstract d(Lbb/s;)Lbb/z;
.end method
