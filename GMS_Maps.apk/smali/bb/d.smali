.class public abstract Lbb/d;
.super Lbb/f;
.source "SourceFile"


# instance fields
.field private volatile a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lbb/f;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lbb/d;->a:I

    return-void
.end method

.method static synthetic a(Lbb/d;)I
    .locals 2

    iget v0, p0, Lbb/d;->a:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lbb/d;->a:I

    return v0
.end method


# virtual methods
.method public declared-synchronized L_()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lbb/f;->L_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbb/d;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(Lbb/s;)Lbb/z;
.end method

.method protected a_(Lbb/s;)V
    .locals 6

    invoke-virtual {p0, p1}, Lbb/d;->b(Lbb/s;)Z

    move-result v4

    invoke-virtual {p0, p1}, Lbb/d;->c(Lbb/s;)Z

    move-result v5

    if-nez v4, :cond_0

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lbb/d;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbb/d;->a:I

    new-instance v0, Lbb/e;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lbb/e;-><init>(Lbb/d;Las/c;Lbb/s;ZZ)V

    invoke-virtual {v0}, Lbb/e;->g()V

    goto :goto_0
.end method
