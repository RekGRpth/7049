.class public abstract Lbb/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbb/r;


# instance fields
.field private a:I

.field private b:Las/d;

.field private volatile c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lbb/f;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lbb/f;->b:Las/d;

    iput-boolean v1, p0, Lbb/f;->c:Z

    return-void
.end method

.method public static a(II)LaN/B;
    .locals 1

    invoke-static {p0, p1}, Lbb/f;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LaN/B;

    invoke-direct {v0, p0, p1}, LaN/B;-><init>(II)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(II)Z
    .locals 1

    const v0, 0xbebc200

    if-eq p0, v0, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static f(Lbb/s;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public L_()Z
    .locals 1

    iget-boolean v0, p0, Lbb/f;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final declared-synchronized a(Lbb/z;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lbb/f;->i()Z

    new-instance v0, Lbb/g;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lbb/g;-><init>(Lbb/f;Las/c;Lbb/z;)V

    iput-object v0, p0, Lbb/f;->b:Las/d;

    iget-object v0, p0, Lbb/f;->b:Las/d;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    iget-object v0, p0, Lbb/f;->b:Las/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Las/d;->b(I)V

    iget-object v0, p0, Lbb/f;->b:Las/d;

    invoke-virtual {v0}, Las/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lbb/z;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lbb/f;->a(Lbb/z;ZZ)V

    return-void
.end method

.method protected a(Lbb/z;ZZ)V
    .locals 3

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {p0}, Lbb/f;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lbb/o;->a(I)V

    invoke-virtual {p1}, Lbb/z;->b()Lbb/s;

    move-result-object v1

    invoke-virtual {v1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lbb/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lbb/f;->c()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lbb/o;->a(Lbb/z;ZI)V

    :cond_0
    return-void
.end method

.method protected abstract a_(Lbb/s;)V
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string v0, "o"

    return-object v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lbb/f;->a:I

    return-void
.end method

.method public b(Lbb/s;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(Lbb/s;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()[I
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    return-object v0
.end method

.method public final e(Lbb/s;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbb/f;->c:Z

    invoke-virtual {p0, p1}, Lbb/f;->a_(Lbb/s;)V

    return-void
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbb/f;->c:Z

    return-void
.end method

.method public declared-synchronized i()Z
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lbb/f;->b:Las/d;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbb/f;->b:Las/d;

    invoke-virtual {v1}, Las/d;->c()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lbb/f;->b:Las/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lbb/f;->a:I

    return v0
.end method
