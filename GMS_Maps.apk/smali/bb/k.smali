.class public Lbb/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbb/j;

.field private b:Z

.field private c:Las/d;


# direct methods
.method protected constructor <init>(Lbb/j;)V
    .locals 1

    iput-object p1, p0, Lbb/k;->a:Lbb/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbb/k;->b:Z

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbb/k;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbb/k;->b:Z

    invoke-virtual {p0}, Lbb/k;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->a(Lbb/j;)Lbb/l;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v1}, Lbb/j;->a(Lbb/j;)Lbb/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v1}, Lbb/j;->a(Lbb/j;)Lbb/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    iget-object v0, p0, Lbb/k;->a:Lbb/j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbb/j;->a(Lbb/j;Lbb/l;)Lbb/l;

    new-instance v0, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lbb/k;->c:Las/d;

    iget-object v0, p0, Lbb/k;->c:Las/d;

    iget-object v1, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v1}, Lbb/j;->c(Lbb/j;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    iget-object v0, p0, Lbb/k;->c:Las/d;

    invoke-virtual {v0}, Las/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lbb/k;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
