.class public Lbb/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:LaN/B;

.field private final k:LaN/Y;

.field private final l:I

.field private final m:Ljava/lang/String;

.field private final n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final o:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "indexInList="

    sput-object v0, Lbb/w;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;LaN/B;LaN/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_1

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lbb/w;->b:Ljava/lang/String;

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput-object p1, p0, Lbb/w;->c:Ljava/lang/String;

    if-nez p3, :cond_0

    const-string p3, ""

    :cond_0
    iput-object p3, p0, Lbb/w;->d:Ljava/lang/String;

    iput p4, p0, Lbb/w;->f:I

    iput p5, p0, Lbb/w;->g:I

    iput p6, p0, Lbb/w;->h:I

    iput-object p7, p0, Lbb/w;->i:Ljava/lang/String;

    iput-object p8, p0, Lbb/w;->j:LaN/B;

    iput-object p9, p0, Lbb/w;->k:LaN/Y;

    iput p10, p0, Lbb/w;->l:I

    iput-object p11, p0, Lbb/w;->m:Ljava/lang/String;

    iput-object p12, p0, Lbb/w;->o:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p13, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide p14, p0, Lbb/w;->e:J

    return-void

    :cond_1
    move-object v0, p1

    goto :goto_0

    :cond_2
    move-object p1, p2

    goto :goto_1
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;LaN/B;LaN/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;JLbb/x;)V
    .locals 0

    invoke-direct/range {p0 .. p15}, Lbb/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;LaN/B;LaN/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    invoke-static {p0}, Lcom/google/common/base/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/common/base/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbb/w;->g:I

    return v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbb/w;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lbb/w;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbb/w;->h()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbb/w;->h()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, -0x1

    if-eq p1, v1, :cond_2

    sget-object v1, Lbb/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbb/w;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/w;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/w;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lbb/w;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbb/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/w;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbb/w;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbb/w;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbb/w;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lbb/w;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lbb/w;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lbb/w;

    iget v1, p0, Lbb/w;->f:I

    invoke-virtual {p1}, Lbb/w;->e()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lbb/w;->g:I

    invoke-virtual {p1}, Lbb/w;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lbb/w;->h:I

    invoke-virtual {p1}, Lbb/w;->f()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lbb/w;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbb/w;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbb/w;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lbb/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbb/w;->i:Ljava/lang/String;

    invoke-virtual {p1}, Lbb/w;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lbb/w;->h:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbb/w;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h()LaN/B;
    .locals 1

    iget-object v0, p0, Lbb/w;->j:LaN/B;

    return-object v0
.end method

.method public i()LaN/Y;
    .locals 1

    iget-object v0, p0, Lbb/w;->k:LaN/Y;

    return-object v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, Lbb/w;->e:J

    return-wide v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lbb/w;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "d"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "c"

    goto :goto_0

    :pswitch_2
    const-string v0, "h"

    goto :goto_0

    :pswitch_3
    const-string v0, "r"

    goto :goto_0

    :pswitch_4
    const-string v0, "v"

    goto :goto_0

    :pswitch_5
    const-string v0, "f"

    goto :goto_0

    :pswitch_6
    const-string v0, "p"

    goto :goto_0

    :pswitch_7
    const-string v0, "l"

    goto :goto_0

    :pswitch_8
    const-string v0, "d"

    goto :goto_0

    :pswitch_9
    const-string v0, "x"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_9
        :pswitch_8
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public l()Ljava/lang/String;
    .locals 3

    const v0, 0x7f0203dc

    const v1, 0x7f0203da

    iget v2, p0, Lbb/w;->f:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0203d8

    :goto_0
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_3
    move v0, v1

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0203d9

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbb/w;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    const/16 v4, 0xe

    const/16 v3, 0xd

    const/16 v6, 0xc

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hb;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget v2, p0, Lbb/w;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    iget-object v2, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    iget-object v2, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDouble(I)D

    move-result-wide v2

    const-wide v4, 0x40c3880000000000L

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v1, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lbb/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    return-object v0
.end method

.method public o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lbb/w;->o:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "query:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbb/w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " description:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbb/w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbb/w;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " providerId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbb/w;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ranking:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbb/w;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
