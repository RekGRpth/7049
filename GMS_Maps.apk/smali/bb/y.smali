.class public Lbb/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:LaN/B;

.field private i:LaN/Y;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/util/List;

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/y;->l:Ljava/util/List;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbb/y;->n:J

    return-void
.end method


# virtual methods
.method public a()Lbb/w;
    .locals 18

    new-instance v1, Lbb/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbb/y;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbb/y;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbb/y;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lbb/y;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lbb/y;->e:I

    move-object/from16 v0, p0

    iget v7, v0, Lbb/y;->f:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lbb/y;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbb/y;->h:LaN/B;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbb/y;->i:LaN/Y;

    move-object/from16 v0, p0

    iget v11, v0, Lbb/y;->j:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lbb/y;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbb/y;->l:Ljava/util/List;

    const/4 v14, 0x0

    new-array v14, v14, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v13, v14}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbb/y;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-wide v15, v0, Lbb/y;->n:J

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, Lbb/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;LaN/B;LaN/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;JLbb/x;)V

    return-object v1
.end method

.method public a(I)Lbb/y;
    .locals 0

    iput p1, p0, Lbb/y;->d:I

    return-object p0
.end method

.method public a(J)Lbb/y;
    .locals 0

    iput-wide p1, p0, Lbb/y;->n:J

    return-object p0
.end method

.method public a(LaN/B;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->h:LaN/B;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/y;
    .locals 1

    iget-object v0, p0, Lbb/y;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->a:Ljava/lang/String;

    return-object p0
.end method

.method public b(I)Lbb/y;
    .locals 0

    iput p1, p0, Lbb/y;->e:I

    return-object p0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->b:Ljava/lang/String;

    return-object p0
.end method

.method public c(I)Lbb/y;
    .locals 0

    iput p1, p0, Lbb/y;->f:I

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->c:Ljava/lang/String;

    return-object p0
.end method

.method public d(I)Lbb/y;
    .locals 0

    iput p1, p0, Lbb/y;->j:I

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->g:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lbb/y;
    .locals 0

    iput-object p1, p0, Lbb/y;->k:Ljava/lang/String;

    return-object p0
.end method
