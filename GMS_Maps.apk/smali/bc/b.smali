.class Lbc/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:F

.field private final b:F

.field private final c:Ljava/lang/Runnable;

.field private d:Z


# direct methods
.method public constructor <init>(FFLjava/lang/Runnable;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbc/b;->a:F

    iput p2, p0, Lbc/b;->b:F

    iput-object p3, p0, Lbc/b;->c:Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbc/b;->d:Z

    return-void
.end method

.method private static a(FFF)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const v3, 0x3a83126f

    cmpg-float v2, p1, p2

    if-gez v2, :cond_2

    sub-float v2, p1, v3

    cmpg-float v2, v2, p0

    if-gtz v2, :cond_1

    add-float v2, p2, v3

    cmpg-float v2, p0, v2

    if-gtz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sub-float v2, p2, v3

    cmpg-float v2, v2, p0

    if-gtz v2, :cond_3

    add-float v2, p1, v3

    cmpg-float v2, p0, v2

    if-lez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(FFFF)Z
    .locals 1

    iget-boolean v0, p0, Lbc/b;->d:Z

    if-nez v0, :cond_0

    iget v0, p0, Lbc/b;->a:F

    invoke-static {v0, p1, p2}, Lbc/b;->a(FFF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbc/b;->b:F

    invoke-static {v0, p3, p4}, Lbc/b;->a(FFF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbc/b;->d:Z

    iget-object v0, p0, Lbc/b;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-boolean v0, p0, Lbc/b;->d:Z

    return v0
.end method
