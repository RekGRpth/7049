.class public Lbd/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private final b:Ljava/lang/CharSequence;

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbd/a;->b:Ljava/lang/CharSequence;

    iput-object p2, p0, Lbd/a;->a:Landroid/view/View$OnClickListener;

    iput p3, p0, Lbd/a;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbd/a;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbd/c;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbd/c;-><init>(Lbd/b;)V

    const v0, 0x7f100255

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbd/c;->a:Landroid/widget/TextView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lbd/c;

    iget-object v0, p2, Lbd/c;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lbd/a;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbd/a;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400b5

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
