.class public abstract Lbe/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field protected final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbe/v;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-boolean p2, p0, Lbe/v;->b:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbe/w;

    invoke-direct {v1}, Lbe/w;-><init>()V

    const v0, 0x7f10026f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbe/w;->b:Landroid/widget/TextView;

    const v0, 0x7f100270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbe/w;->c:Landroid/widget/TextView;

    const v0, 0x7f100259

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbe/w;->a:Landroid/widget/ImageView;

    const v0, 0x7f1002ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbe/w;->d:Landroid/view/View;

    return-object v1
.end method

.method protected abstract a(Lbe/w;)V
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lbe/w;

    invoke-virtual {p0, p2}, Lbe/v;->a(Lbe/w;)V

    iget-boolean v0, p0, Lbe/v;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p2, Lbe/w;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p2, Lbe/w;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040113

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
