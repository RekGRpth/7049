.class public Lbe/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/c;

.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:Z

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/c;Landroid/content/Context;ZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    iput-object p2, p0, Lbe/x;->b:Landroid/content/Context;

    iput-boolean p3, p0, Lbe/x;->c:Z

    iput-boolean p4, p0, Lbe/x;->d:Z

    iput-object p5, p0, Lbe/x;->e:Landroid/view/View$OnClickListener;

    iput-object p6, p0, Lbe/x;->f:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbe/z;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbe/z;-><init>(Lbe/y;)V

    const v0, 0x7f1000ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/z;->a(Lbe/z;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f10025a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/z;->b(Lbe/z;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100259

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbe/z;->a(Lbe/z;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    const v0, 0x7f10026d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/z;->c(Lbe/z;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100258

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-static {v1, v0}, Lbe/z;->a(Lbe/z;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;

    const v0, 0x7f100051

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lbe/z;->a(Lbe/z;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    const v0, 0x7f10026e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbe/z;->a(Lbe/z;Landroid/view/View;)Landroid/view/View;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 7

    const/4 v2, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    check-cast p2, Lbe/z;

    invoke-static {p2}, Lbe/z;->a(Lbe/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lbe/z;->b(Lbe/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lbe/x;->c:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lbe/x;->d:Z

    if-nez v0, :cond_2

    invoke-static {p2}, Lbe/z;->b(Lbe/z;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x269

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/c;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    invoke-static {p2}, Lbe/z;->c(Lbe/z;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lbe/x;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    iget-boolean v3, p0, Lbe/x;->c:Z

    iget-boolean v4, p0, Lbe/x;->d:Z

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/friend/history/c;->a(ZZ)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p2}, Lbe/z;->d(Lbe/z;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lbe/x;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p2}, Lbe/z;->e(Lbe/z;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lbe/x;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->p()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->n()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    invoke-static {p2}, Lbe/z;->f(Lbe/z;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbe/z;->d(Lbe/z;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->l()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    iget-object v1, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->p()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p2}, Lbe/z;->g(Lbe/z;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lbe/z;->g(Lbe/z;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void

    :cond_2
    iget-boolean v0, p0, Lbe/x;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbe/x;->c:Z

    if-nez v0, :cond_0

    invoke-static {p2}, Lbe/z;->b(Lbe/z;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x263

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lbe/x;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/c;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {p2}, Lbe/z;->f(Lbe/z;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbe/z;->d(Lbe/z;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    :cond_4
    invoke-static {p2}, Lbe/z;->g(Lbe/z;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400c1

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
