.class public Lbf/A;
.super Lbf/h;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;
    .locals 9

    const/16 v6, 0x8

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    sget-object v0, Lbf/A;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100042

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget-object v1, Lbf/A;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100048

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lbf/A;->c:Lbf/i;

    check-cast v1, Lbf/C;

    invoke-virtual {v1}, Lbf/C;->bu()Lcom/google/googlenav/ai;

    move-result-object v5

    const v2, 0x7f100043

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v1}, Lbf/C;->bH()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lbf/C;->bG()Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    invoke-virtual {v3}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {v1}, Lbf/C;->c()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lbf/C;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x259

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-virtual {v1}, Lbf/C;->f()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v5}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    invoke-static {v5}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    :goto_2
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_9

    :cond_1
    :goto_3
    move-object v5, v3

    move-object v3, v2

    move-object v2, v4

    :goto_4
    const v6, 0x7f10001e

    invoke-static {v0, v6, v5}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    const v5, 0x7f100044

    invoke-static {v0, v5, v3}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    const v3, 0x7f100045

    invoke-static {v0, v3, v2}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    const v2, 0x7f100046

    invoke-virtual {v1}, Lbf/C;->bI()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lbf/C;->bK()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x247

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    :cond_2
    invoke-static {v0, v2, v4}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    const v1, 0x7f100047

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lbf/A;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p1}, Lbf/A;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_5

    array-length v2, v3

    if-lez v2, :cond_5

    aget-object v2, v3, v8

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    array-length v6, v3

    if-le v6, v7, :cond_0

    aget-object v2, v3, v7

    goto :goto_1

    :cond_5
    const/16 v2, 0x5e8

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v1}, Lbf/C;->d()[Ljava/lang/String;

    move-result-object v5

    aget-object v2, v5, v8

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    array-length v2, v5

    if-le v2, v7, :cond_8

    aget-object v2, v5, v7

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_5
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    array-length v6, v5

    const/4 v7, 0x2

    if-le v6, v7, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Context menu bubble contains "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lines: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move-object v5, v3

    move-object v3, v4

    goto/16 :goto_4

    :cond_8
    move-object v2, v4

    goto :goto_5

    :cond_9
    move-object v2, v4

    goto/16 :goto_3

    :cond_a
    move-object v3, v4

    goto/16 :goto_2
.end method


# virtual methods
.method public b()Lcom/google/googlenav/ui/view/d;
    .locals 1

    iget-object v0, p0, Lbf/A;->c:Lbf/i;

    invoke-direct {p0, v0}, Lbf/A;->a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0
.end method
