.class public Lbf/G;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lax/w;Ljava/util/Vector;Ljava/util/Vector;IZ)I
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p0, p3}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {p0, v0}, Lax/w;->a(Lax/m;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v2, 0x1

    if-eqz p4, :cond_2

    sget-object v1, Lcom/google/googlenav/ui/aV;->i:Lcom/google/googlenav/ui/aV;

    :goto_0
    invoke-static {v4, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move v1, v2

    :goto_1
    invoke-static {p0, v0, v3}, Lbf/G;->a(Lax/b;Lax/m;Z)Lbf/H;

    move-result-object v0

    invoke-virtual {v0}, Lbf/H;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lbf/H;->a:Ljava/lang/String;

    invoke-static {p4}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    :cond_0
    invoke-virtual {v0}, Lbf/H;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, v0, Lbf/H;->c:Ljava/lang/String;

    invoke-static {p4}, Lbf/G;->c(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    :cond_1
    return v1

    :cond_2
    sget-object v1, Lcom/google/googlenav/ui/aV;->F:Lcom/google/googlenav/ui/aV;

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1
.end method

.method public static a(Lax/b;IZ)Lbf/H;
    .locals 4

    new-instance v1, Lbf/H;

    invoke-direct {v1}, Lbf/H;-><init>()V

    invoke-virtual {p0, p1}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lax/m;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->a:Ljava/lang/String;

    :cond_1
    :goto_1
    invoke-virtual {v2}, Lax/t;->H()[Lax/n;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->b:[Lax/n;

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lax/t;->v()I

    move-result v0

    if-lez v0, :cond_1

    if-nez p2, :cond_1

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v0

    invoke-virtual {v2}, Lax/t;->v()I

    move-result v3

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method private static a(Lax/b;Lax/m;)Lbf/H;
    .locals 9

    const/4 v8, 0x2

    const/16 v7, 0xa

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lax/t;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0xf0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lax/t;->u()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lbf/G;->b(Lax/t;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v2, 0x590

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_0
    new-instance v0, Lbf/H;

    invoke-direct {v0}, Lbf/H;-><init>()V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->a:Ljava/lang/String;

    return-object v0

    :cond_2
    const/16 v3, 0x591

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lax/m;->p()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Lbf/G;->a(Lax/t;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const/16 v3, 0x58b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    const/16 v2, 0x58a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lax/t;->v()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lax/t;->v()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a(Lax/b;Lax/m;Z)Lbf/H;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v3

    new-instance v2, Lbf/H;

    invoke-direct {v2}, Lbf/H;-><init>()V

    if-nez v3, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lax/m;->r()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Lax/m;->n()Z

    move-result v4

    if-eqz v4, :cond_4

    check-cast p1, Lax/a;

    const/16 v4, 0x609

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3}, Lax/t;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v4, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbf/H;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lax/a;->j()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1}, Lax/a;->j()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbf/H;->c:Ljava/lang/String;

    :cond_2
    :goto_1
    move-object v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lax/a;->i()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, Lax/a;->i()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbf/H;->c:Ljava/lang/String;

    goto :goto_1

    :cond_4
    invoke-static {p0}, Lbf/G;->a(Lax/b;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p1}, Lax/m;->p()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    invoke-static {p0, p1}, Lbf/G;->a(Lax/b;Lax/m;)Lbf/H;

    move-result-object v0

    :goto_2
    invoke-virtual {p1}, Lax/m;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lax/m;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lbf/H;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    if-nez p2, :cond_7

    :goto_3
    invoke-static {p0, p1, v0}, Lbf/G;->b(Lax/b;Lax/m;Z)Lbf/H;

    move-result-object v0

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public static a(Lax/b;IILcom/google/googlenav/ui/e;Z)Lbj/H;
    .locals 8

    const/4 v1, 0x0

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    invoke-static {p0, p1, v1}, Lbf/G;->a(Lax/b;IZ)Lbf/H;

    move-result-object v5

    invoke-virtual {v5}, Lbf/H;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v5, Lbf/H;->a:Ljava/lang/String;

    invoke-static {v1}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    :cond_0
    invoke-virtual {v5}, Lbf/H;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v6, v5, Lbf/H;->b:[Lax/n;

    array-length v6, v6

    if-ge v0, v6, :cond_2

    iget-object v6, v5, Lbf/H;->b:[Lax/n;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lax/n;->c()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Lax/n;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lbf/G;->b(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Lbf/H;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v5, Lbf/H;->c:Ljava/lang/String;

    invoke-static {v1}, Lbf/G;->c(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    :cond_3
    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    :cond_4
    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v3, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    :cond_5
    if-eqz p4, :cond_6

    const/16 v4, 0x5ec

    :goto_1
    if-eqz p4, :cond_7

    const/4 v5, -0x1

    :goto_2
    invoke-virtual {p0, p1}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v2

    new-instance v0, Lbj/s;

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    move v3, p2

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lbj/s;-><init>(Ljava/lang/CharSequence;Lax/t;IIII)V

    return-object v0

    :cond_6
    const/16 v4, 0xc8

    goto :goto_1

    :cond_7
    const/16 v5, 0xe7

    goto :goto_2
.end method

.method public static a(Z)Lcom/google/googlenav/ui/aV;
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->j:Lcom/google/googlenav/ui/aV;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/aV;->G:Lcom/google/googlenav/ui/aV;

    goto :goto_0
.end method

.method private static a(Lax/t;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lax/t;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x3b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lax/t;->x()I

    move-result v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static a(Lax/b;)Z
    .locals 1

    invoke-virtual {p0}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lax/b;Lax/m;Z)Lbf/H;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lax/m;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lax/m;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lax/m;->z()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x595

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->D()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lax/m;->z()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x593

    :goto_1
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    invoke-virtual {v1}, Lax/t;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "\n"

    const/16 v4, 0xf0

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->u()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lbf/G;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lax/m;->k()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    if-eqz p2, :cond_4

    invoke-virtual {p1}, Lax/m;->v()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1}, Lax/t;->w()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_3
    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    :goto_3
    new-instance v0, Lbf/H;

    invoke-direct {v0}, Lbf/H;-><init>()V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->a:Ljava/lang/String;

    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_6

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->c:Ljava/lang/String;

    :cond_6
    return-object v0

    :cond_7
    const/16 v0, 0x597

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0x596

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v1}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :pswitch_0
    invoke-static {v1}, Lbf/G;->c(Lax/t;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "\n"

    invoke-static {v3, v1, v0}, Lbf/G;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_1
    const/16 v0, 0x592

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->i()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lbf/G;->d(Lax/t;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_a
    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    if-lez v0, :cond_4

    if-eqz p2, :cond_4

    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :pswitch_2
    invoke-virtual {v1}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_b
    invoke-virtual {v1}, Lax/t;->w()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Lax/t;->x()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Z)Lcom/google/googlenav/ui/aV;
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->l:Lcom/google/googlenav/ui/aV;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/aV;->I:Lcom/google/googlenav/ui/aV;

    goto :goto_0
.end method

.method private static b(Lax/t;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lax/t;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xe9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lax/t;->t()I

    move-result v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static c(Z)Lcom/google/googlenav/ui/aV;
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->k:Lcom/google/googlenav/ui/aV;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/aV;->H:Lcom/google/googlenav/ui/aV;

    goto :goto_0
.end method

.method private static c(Lax/t;)Ljava/lang/String;
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x58e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lax/t;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x58f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lax/t;->t()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static d(Lax/t;)Ljava/lang/String;
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x58d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lax/t;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x58c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lax/t;->x()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
