.class public Lbf/O;
.super Lbf/F;
.source "SourceFile"


# static fields
.field private static F:Z

.field private static G:Ljava/util/Vector;

.field public static final w:I


# instance fields
.field private final A:LaH/m;

.field private B:Ln/s;

.field private C:Lcom/google/googlenav/ui/view/dialog/r;

.field private D:Z

.field private E:Z

.field private H:Lcom/google/googlenav/ui/view/d;

.field private final I:Lbf/I;

.field private J:Ljava/util/List;

.field private K:Lcom/google/googlenav/ui/android/M;

.field v:Z

.field private x:I

.field private y:Ljava/lang/String;

.field private z:Lcom/google/googlenav/ui/view/android/C;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/O;->w:I

    const/4 v0, 0x0

    sput-boolean v0, Lbf/O;->F:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lbf/O;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/F;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/F;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lbf/F;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    iput-object v6, p0, Lbf/O;->y:Ljava/lang/String;

    iput-object v6, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    iput-object v6, p0, Lbf/O;->B:Ln/s;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/O;->v:Z

    iput-object p5, p0, Lbf/O;->A:LaH/m;

    new-instance v0, Lbf/I;

    invoke-direct {v0, p0}, Lbf/I;-><init>(Lbf/O;)V

    iput-object v0, p0, Lbf/O;->I:Lbf/I;

    return-void
.end method

.method private a(Lax/t;)I
    .locals 3

    const v0, -0x57f0f0f1

    invoke-direct {p0}, Lbf/O;->bx()Ln/s;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lax/t;->h()Lo/D;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Ln/s;->d(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_0

    const v0, 0x300f0f0f

    :cond_0
    return v0
.end method

.method public static a(Lax/b;)Lax/b;
    .locals 5

    const/4 v2, 0x0

    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lax/b;->aL()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/b;

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v3

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v4

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->aq()Lax/y;

    move-result-object v4

    invoke-virtual {v3, v4}, Lax/y;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v4

    invoke-virtual {v3, v4}, Lax/y;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_0
.end method

.method private a(Lax/y;)Lax/y;
    .locals 6

    invoke-virtual {p1}, Lax/y;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lax/y;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lax/y;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lax/y;->l()Lo/D;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lax/y;->a(Ljava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method static synthetic a(Lbf/O;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lbf/O;->J:Ljava/util/List;

    return-object p1
.end method

.method private a(Lat/b;I)V
    .locals 2

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {p0, p1}, Lbf/O;->b(Lat/b;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/m;->a(LaN/B;)V

    invoke-virtual {p0}, Lbf/O;->R()V

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->af()Lcom/google/googlenav/ui/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/p;->h()V

    invoke-virtual {p0}, Lbf/O;->al()V

    invoke-virtual {p0, p2}, Lbf/O;->b(I)V

    invoke-virtual {p0}, Lbf/O;->an()Z

    return-void
.end method

.method static synthetic a(Lbf/O;)V
    .locals 0

    invoke-direct {p0}, Lbf/O;->bO()V

    return-void
.end method

.method static synthetic a(Lbf/O;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbf/O;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/F;Z)V
    .locals 8

    const/4 v7, -0x1

    const-wide v4, 0x3ff3333333333333L

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    check-cast p1, Lax/b;

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->E()V

    sget-boolean v0, Lbf/O;->F:Z

    if-eqz v0, :cond_5

    sput-boolean v1, Lbf/O;->F:Z

    invoke-virtual {p0, v1, v6}, Lbf/O;->b(ILjava/lang/Object;)V

    :goto_0
    invoke-virtual {p1}, Lax/b;->aP()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p1, v0}, Lax/b;->b(Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Lbf/O;->e(Lax/b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->y:Ljava/lang/String;

    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbf/O;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aN()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->y:Ljava/lang/String;

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lbf/O;->bP()V

    :cond_2
    invoke-direct {p0}, Lbf/O;->bN()V

    sget-object v0, LaE/b;->a:LaE/b;

    invoke-virtual {v0}, LaE/b;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lbf/O;->bt()V

    iput-object v6, p0, Lbf/O;->J:Ljava/util/List;

    invoke-virtual {p0}, Lbf/O;->bi()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p0}, Lbf/O;->br()V

    :cond_4
    return-void

    :cond_5
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    if-eq v0, v3, :cond_9

    invoke-virtual {p0, v1, v6}, Lbf/O;->b(ILjava/lang/Object;)V

    invoke-virtual {p1}, Lax/b;->ah()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    invoke-virtual {p1}, Lax/b;->ag()I

    move-result v1

    int-to-double v1, v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p0, v3}, Lbf/O;->c(Z)I

    move-result v4

    invoke-virtual {p0}, Lbf/O;->q()I

    move-result v5

    invoke-virtual {v2, v0, v1, v4, v5}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->ai()LaN/B;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, v3}, Lbf/O;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p0}, Lbf/O;->q()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    neg-int v2, v2

    neg-int v4, v4

    invoke-virtual {v0, v2, v4, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    :cond_6
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v2, v0, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    invoke-virtual {p0, v7}, Lbf/O;->b(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    :cond_7
    iput-boolean v3, p0, Lbf/O;->u:Z

    :cond_8
    :goto_1
    invoke-static {p1}, Lbf/O;->b(Lax/b;)V

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ca;->b(I)V

    goto/16 :goto_0

    :cond_9
    if-eqz p2, :cond_c

    move v0, v1

    :goto_2
    iget-object v2, p0, Lbf/O;->c:LaN/p;

    invoke-virtual {p1, v2}, Lax/b;->a(LaN/p;)LaN/Y;

    move-result-object v4

    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    const/16 v5, 0xe

    if-ge v2, v5, :cond_d

    move v2, v3

    :goto_3
    if-eqz v4, :cond_b

    iget-object v5, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v5}, LaN/u;->d()LaN/Y;

    move-result-object v5

    invoke-virtual {v4, v5}, LaN/Y;->b(LaN/Y;)Z

    move-result v5

    if-nez v5, :cond_a

    if-eqz v2, :cond_b

    :cond_a
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p1}, Lax/b;->aw()LaN/B;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, LaN/u;->e(LaN/B;LaN/Y;)V

    :cond_b
    invoke-virtual {p1}, Lax/b;->ay()I

    move-result v2

    if-eq v2, v7, :cond_e

    invoke-virtual {p0, v1, v6}, Lbf/O;->b(ILjava/lang/Object;)V

    invoke-virtual {p0, v2, p2, v1}, Lbf/O;->a(IZZ)V

    goto :goto_1

    :cond_c
    iget-object v0, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/O;->b(LaN/B;)I

    move-result v0

    goto :goto_2

    :cond_d
    move v2, v1

    goto :goto_3

    :cond_e
    if-ltz v0, :cond_8

    invoke-virtual {p0, v0, p2, v3}, Lbf/O;->a(IZZ)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;Lax/b;)V
    .locals 0

    invoke-static {p0, p1}, Lbf/O;->b(Lcom/google/googlenav/ui/s;Lax/b;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit16 v0, v1, 0x200

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :cond_0
    new-instance v2, Lay/b;

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbf/Q;

    invoke-direct {v1, p0, p2, p1}, Lbf/Q;-><init>(Lbf/O;Ljava/util/List;Ljava/util/List;)V

    invoke-direct {v2, v0, v1}, Lay/b;-><init>(Ljava/util/List;Lay/c;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public static a(ILcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/J;Lax/b;Lcom/google/googlenav/ui/wizard/ca;Lcom/google/googlenav/ui/wizard/z;Z)Z
    .locals 5

    const/16 v3, 0x1d

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p3}, Lax/b;->ap()Lax/j;

    move-result-object v0

    packed-switch p0, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    const-string v0, "ti"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/ah;->h()V

    move v0, v2

    goto :goto_0

    :pswitch_1
    const-string v1, "sd"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    new-instance v1, Lax/s;

    invoke-direct {v1, v0}, Lax/s;-><init>(Lax/k;)V

    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    goto :goto_0

    :pswitch_2
    const-string v1, "sw"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    new-instance v1, Lax/x;

    invoke-direct {v1, v0}, Lax/x;-><init>(Lax/k;)V

    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    goto :goto_0

    :pswitch_3
    const-string v1, "st"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    new-instance v1, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    goto :goto_0

    :pswitch_4
    const-string v1, "sk"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    new-instance v1, Lax/i;

    invoke-direct {v1, v0}, Lax/i;-><init>(Lax/k;)V

    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x0

    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->k()Ljava/lang/String;

    move-result-object v0

    :cond_0
    instance-of v3, p3, Lax/s;

    if-eqz v3, :cond_2

    const-string v3, "dn"

    invoke-static {v3, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-virtual {p3}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "d"

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    :cond_1
    :goto_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    instance-of v1, p3, Lax/x;

    if-eqz v1, :cond_3

    const-string v1, "wn"

    invoke-static {v1, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p3}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "d"

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    instance-of v1, p3, Lax/i;

    if-eqz v1, :cond_4

    const-string v1, "bn"

    invoke-static {v1, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p3}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "d"

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    instance-of v1, p3, Lax/w;

    if-eqz v1, :cond_1

    const-string v1, "tn"

    invoke-static {v1, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x61

    const-string v1, "n"

    const-string v3, "h"

    invoke-static {v0, v1, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0, v2}, LaS/a;->a(Z)V

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/ui/s;

    move-object v1, p3

    check-cast v1, Lax/w;

    invoke-static {v0, v1}, Lbf/O;->a(Lcom/google/googlenav/ui/s;Lax/w;)Z

    move-result v0

    if-nez v0, :cond_1

    check-cast p3, Lax/w;

    invoke-interface {p2, p3}, Lcom/google/googlenav/J;->a(Lax/w;)V

    goto :goto_1

    :pswitch_6
    const-string v0, "es"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    if-eqz p6, :cond_5

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/ah;->dismiss()V

    new-instance v0, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v0, v3, p1}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p5, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_5
    invoke-virtual {p4, p3, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;Z)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_7
    const-string v0, "ee"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    if-eqz p6, :cond_6

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/ah;->dismiss()V

    new-instance v0, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v0, v3, p1}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p5, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_6
    invoke-virtual {p4, p3, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;Z)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xe8
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/E;Lcom/google/googlenav/E;)Z
    .locals 3

    iget-object v0, p0, Lbf/O;->c:LaN/p;

    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v1

    invoke-virtual {v0, v1}, LaN/p;->c(I)I

    move-result v0

    sget v1, Lbf/O;->w:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    instance-of v2, p0, Lcom/google/googlenav/ui/s;

    if-eqz v2, :cond_0

    check-cast p0, Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->c(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->d(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->e(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;B)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;B)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_1
        0x8 -> :sswitch_0
        0x18 -> :sswitch_2
        0x19 -> :sswitch_3
        0x1a -> :sswitch_4
        0x1f -> :sswitch_5
        0x20 -> :sswitch_6
    .end sparse-switch
.end method

.method private static a(Lcom/google/googlenav/ui/s;Lax/w;)Z
    .locals 4

    invoke-virtual {p1}, Lax/w;->aX()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/w;->aV()J

    move-result-wide v0

    invoke-static {p1}, Lcom/google/googlenav/ui/bd;->a(Lax/b;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/ui/bd;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x61

    const-string v1, "sr"

    const-string v2, "s"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lbf/O;->b(Lcom/google/googlenav/ui/s;Lax/w;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lax/b;)V
    .locals 3

    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lbf/O;->G:Ljava/util/Vector;

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/b;

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v2

    if-ne v0, v2, :cond_1

    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, p0, v1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :goto_1
    return-void

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static b(Lcom/google/googlenav/ui/s;Lax/b;)V
    .locals 4

    new-instance v0, Lax/j;

    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-virtual {p1}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v0

    invoke-virtual {v1, v0}, Lax/b;->a(Lax/l;)V

    invoke-virtual {v1}, Lax/b;->aM()V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    return-void
.end method

.method private static b(Lcom/google/googlenav/ui/s;Lax/w;)V
    .locals 8

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x4c4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x4c3

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x4c2

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x4c1

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lbf/P;

    invoke-direct {v7, p0, p1}, Lbf/P;-><init>(Lcom/google/googlenav/ui/s;Lax/w;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x4

    const-string v1, "da"

    invoke-static {v0, v1, p0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x4

    const-string v2, "da"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private bA()[Lcom/google/googlenav/ui/aH;
    .locals 4

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aH;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/aF;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;)V

    aput-object v3, v1, v2

    return-object v1
.end method

.method private bB()I
    .locals 1

    iget v0, p0, Lbf/O;->x:I

    return v0
.end method

.method private bC()Lax/s;
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/s;

    return-object v0
.end method

.method private bD()Lax/x;
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/x;

    return-object v0
.end method

.method private bE()Lax/i;
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/i;

    return-object v0
.end method

.method private bF()V
    .locals 2

    const-string v0, "va"

    invoke-static {v0}, Lbf/O;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->h(Lax/b;)V

    return-void
.end method

.method private bG()V
    .locals 4

    const-string v0, "r"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    new-instance v1, Lax/j;

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-direct {p0, v2}, Lbf/O;->a(Lax/y;)Lax/y;

    move-result-object v2

    invoke-virtual {v0}, Lax/b;->aq()Lax/y;

    move-result-object v3

    invoke-direct {p0, v3}, Lbf/O;->a(Lax/y;)Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0, v1}, Lbf/O;->b(Lax/j;)V

    return-void
.end method

.method private bH()V
    .locals 4

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    iget-object v1, p0, Lbf/O;->A:LaH/m;

    invoke-interface {v1}, LaH/m;->s()LaH/h;

    move-result-object v1

    new-instance v2, Lax/j;

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v1}, LaH/h;->b()Lo/D;

    move-result-object v1

    invoke-static {v3, v1}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v1

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v2, v1, v3, v0}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0, v2}, Lbf/O;->b(Lax/j;)V

    return-void
.end method

.method private bI()V
    .locals 2

    const-string v0, "av"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->g(Lax/b;)V

    return-void
.end method

.method private bJ()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bK()V
    .locals 2

    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/O;->bL()V

    :cond_0
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    const/16 v1, 0xfb

    invoke-static {v0, v1, p0}, Lcom/google/googlenav/ui/f;->a(Lax/b;ILcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/C;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    iget-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/C;->show()V

    return-void
.end method

.method private bL()V
    .locals 1

    iget-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/C;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    return-void
.end method

.method private bM()V
    .locals 2

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aM()V

    iget-object v1, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/C;->h()Lcom/google/googlenav/ui/view/q;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/f;->a(Lax/b;Lcom/google/googlenav/ui/view/q;)V

    invoke-direct {p0}, Lbf/O;->bL()V

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    return-void
.end method

.method private bN()V
    .locals 5

    const/4 v4, -0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040063

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10017c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    const/16 v3, 0xf4

    invoke-static {v1, v2, p0, v3}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/view/View;Lax/b;Lcom/google/googlenav/ui/e;I)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x13

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->d:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/O;->bk()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, p0, v0}, Lcom/google/googlenav/ui/view/e;->a(Lax/b;Lcom/google/googlenav/ui/e;Z)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->b()V

    goto :goto_0
.end method

.method private bO()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lbf/O;->J:Ljava/util/List;

    invoke-static {v0}, Lay/d;->e(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lay/d;->f(Ljava/util/List;)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, v5}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbf/O;->J:Ljava/util/List;

    invoke-static {v1}, Lay/d;->c(Ljava/util/List;)Lay/a;

    move-result-object v1

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v1, v5}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/googlenav/ui/android/M;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v3

    const v4, 0x7f100060

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/ui/android/M;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V

    iput-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    :cond_0
    iget-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    iget-object v3, p0, Lbf/O;->J:Ljava/util/List;

    invoke-static {v3}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/google/googlenav/ui/android/M;->a(Ljava/util/List;Z)V

    iget-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    const/16 v3, 0xf3

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/M;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private bP()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->j()V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    goto :goto_0
.end method

.method public static bq()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private bu()Z
    .locals 13

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lbf/O;->bv()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lba/c;->b()V

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->ad()[LaN/B;

    move-result-object v10

    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v11

    new-array v12, v11, [Lba/f;

    move v9, v0

    :goto_1
    if-ge v9, v11, :cond_1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v9}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lax/m;

    invoke-virtual {v2}, Lax/m;->l()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    array-length v3, v10

    if-ge v1, v3, :cond_3

    invoke-virtual {v2}, Lax/m;->l()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    aget-object v6, v10, v1

    :goto_2
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_2

    invoke-virtual {v2}, Lax/m;->l()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    aget-object v5, v10, v0

    :goto_3
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-static {v0, v2, v8}, Lbf/G;->a(Lax/b;Lax/m;Z)Lbf/H;

    move-result-object v0

    iget-object v3, v0, Lbf/H;->a:Ljava/lang/String;

    new-instance v0, Lba/f;

    invoke-virtual {v2}, Lax/m;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v2}, Lax/m;->t()Ljava/lang/String;

    move-result-object v2

    const-string v4, ""

    invoke-direct/range {v0 .. v6}, Lba/f;-><init>(LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;LaN/B;)V

    aput-object v0, v12, v9

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {v0, v1, v12}, Lcom/google/googlenav/ui/s;->a(I[Lba/f;)V

    move v0, v8

    goto :goto_0

    :cond_2
    move-object v5, v7

    goto :goto_3

    :cond_3
    move-object v6, v7

    goto :goto_2
.end method

.method private bv()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->u()Z

    move-result v0

    goto :goto_0
.end method

.method private bw()[Lcom/google/googlenav/ui/aH;
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/O;->f()Lax/w;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v3}, Lax/w;->ae()I

    move-result v4

    const/4 v1, 0x1

    if-lt v4, v1, :cond_0

    add-int/lit8 v0, v4, -0x1

    new-array v1, v0, [Lcom/google/googlenav/ui/aH;

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_2

    new-instance v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->b(Z)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {v0, v3, v5, v2, v6}, Lcom/google/googlenav/ui/aJ;-><init>(Lax/w;III)V

    aput-object v0, v1, v2

    aget-object v0, v1, v2

    check-cast v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->a(Z)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/aJ;->a(I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private bx()Ln/s;
    .locals 1

    iget-object v0, p0, Lbf/O;->B:Ln/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->B:Ln/s;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    goto :goto_0
.end method

.method private by()[Lcom/google/googlenav/ui/aH;
    .locals 7

    invoke-direct {p0}, Lbf/O;->bD()Lax/x;

    move-result-object v2

    invoke-virtual {v2}, Lax/x;->af()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/googlenav/ui/aF;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v0, v1, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    add-int/lit8 v0, v1, 0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    new-instance v6, Lcom/google/googlenav/ui/aF;

    invoke-direct {v6, v2, v5, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;II)V

    invoke-virtual {v2, v5}, Lax/x;->n(I)Lax/t;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/O;->a(Lax/t;)I

    move-result v0

    const/4 v5, 0x1

    invoke-virtual {v6, v5}, Lcom/google/googlenav/ui/aF;->a(I)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/aF;->b(I)V

    aput-object v6, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lax/x;->ae()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    return-object v4
.end method

.method private bz()[Lcom/google/googlenav/ui/aH;
    .locals 4

    invoke-direct {p0}, Lbf/O;->bE()Lax/i;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aH;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/aF;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;)V

    aput-object v3, v1, v2

    return-object v1
.end method

.method public static c(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x4

    const-string v1, "al"

    invoke-static {v0, v1, p0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lax/y;->k()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private static e(Lax/b;)Ljava/lang/String;
    .locals 4

    instance-of v0, p0, Lax/w;

    if-eqz v0, :cond_0

    check-cast p0, Lax/w;

    const/16 v0, 0x5d0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x5d6

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lax/w;->az()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Lax/x;

    if-eqz v0, :cond_1

    const/16 v0, 0x608

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lax/i;

    if-eqz v0, :cond_2

    const/16 v0, 0x5a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x109

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 3

    const-string v0, ""

    const-string v1, ""

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->k()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private g(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x38

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/O;->ag()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(I)V
    .locals 4

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const-string v3, "d"

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/googlenav/ui/s;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    return-void
.end method

.method private i(I)V
    .locals 2

    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/O;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-ltz p1, :cond_1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lbf/O;->a(IZZ)V

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/O;->b(ILjava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/O;->b(I)V

    goto :goto_0
.end method

.method private j(I)V
    .locals 1

    iget-object v0, p0, Lbf/O;->I:Lbf/I;

    invoke-virtual {v0, p1}, Lbf/I;->a(I)Lcom/google/googlenav/ui/view/dialog/r;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    iget-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/r;->show()V

    return-void
.end method


# virtual methods
.method protected A()V
    .locals 2

    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/ah;

    invoke-virtual {p0}, Lbf/O;->G()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/ah;->a(I)V

    :cond_0
    return-void
.end method

.method protected B()V
    .locals 2

    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/ah;

    invoke-virtual {p0}, Lbf/O;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/ah;->a(I)V

    :cond_0
    return-void
.end method

.method public I()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lbf/O;->y:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/O;->y:Ljava/lang/String;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-static {v2}, Lbf/O;->e(Lax/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v1, v1, Lax/s;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v1

    invoke-virtual {v1}, Lax/s;->ab()Lax/h;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v1

    invoke-virtual {v1}, Lax/s;->aU()I

    move-result v1

    iget v2, p0, Lbf/O;->x:I

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected T()Ljava/lang/String;
    .locals 1

    const/16 v0, 0xf6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public a(Lax/j;)Lax/b;
    .locals 2

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    instance-of v1, v0, Lax/w;

    if-eqz v1, :cond_0

    new-instance v0, Lax/w;

    iget-object v1, p0, Lbf/O;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/l;)V

    :goto_0
    invoke-virtual {v0}, Lax/b;->aM()V

    return-object v0

    :cond_0
    instance-of v1, v0, Lax/x;

    if-eqz v1, :cond_1

    new-instance v0, Lax/x;

    invoke-direct {v0, p1}, Lax/x;-><init>(Lax/k;)V

    goto :goto_0

    :cond_1
    instance-of v0, v0, Lax/i;

    if-eqz v0, :cond_2

    new-instance v0, Lax/i;

    invoke-direct {v0, p1}, Lax/i;-><init>(Lax/k;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lax/s;

    invoke-direct {v0, p1}, Lax/s;-><init>(Lax/k;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v2}, Lbf/O;->i(Z)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    invoke-virtual {p0, v2}, Lbf/O;->b(B)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aF()[I

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1

    const-string v0, "a"

    invoke-direct {p0, v0}, Lbf/O;->e(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->ay()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0, v2}, Lbf/O;->b(I)V

    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/O;->b(B)V

    goto :goto_0
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/b;->b(Ljava/io/DataOutput;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lbf/O;->G:Ljava/util/Vector;

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 11

    const/4 v10, 0x2

    const/4 v9, -0x1

    const/16 v8, 0x45

    const/4 v6, 0x0

    const/4 v7, 0x1

    iget-object v1, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v1, Lcom/google/googlenav/ui/view/android/ah;

    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v3

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v4

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v5

    move v0, p1

    invoke-static/range {v0 .. v6}, Lbf/O;->a(ILcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/J;Lax/b;Lcom/google/googlenav/ui/wizard/ca;Lcom/google/googlenav/ui/wizard/z;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v6, v7

    :cond_0
    :goto_0
    return v6

    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "sb"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lbf/O;->i(I)V

    move v6, v7

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/O;->c(ILjava/lang/Object;)V

    move v6, v7

    goto :goto_0

    :sswitch_2
    invoke-direct {p0, p2}, Lbf/O;->i(I)V

    move v6, v7

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    move v6, v7

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lbf/O;->bI()V

    move v6, v7

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lbf/O;->bF()V

    move v6, v7

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lbf/O;->bG()V

    move v6, v7

    goto :goto_0

    :sswitch_7
    invoke-direct {p0}, Lbf/O;->bH()V

    move v6, v7

    goto :goto_0

    :sswitch_8
    invoke-direct {p0, p2}, Lbf/O;->j(I)V

    move v6, v7

    goto :goto_0

    :sswitch_9
    invoke-virtual {p0, p2}, Lbf/O;->b(I)V

    invoke-virtual {p0}, Lbf/O;->be()Z

    invoke-direct {p0}, Lbf/O;->bv()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/O;->bu()Z

    move v6, v7

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->az()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/O;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    if-ne v0, v9, :cond_2

    :goto_1
    invoke-virtual {v1, v6}, Lax/b;->r(I)V

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0, v9}, Lax/b;->s(I)V

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->f(Lax/b;)V

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    move v6, v7

    goto/16 :goto_0

    :cond_2
    move v6, v0

    goto :goto_1

    :sswitch_b
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Navigation"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const/16 v0, 0x25c

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_0

    :pswitch_1
    const/16 v0, 0x25e

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_0

    :pswitch_2
    const/16 v0, 0x25d

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_0

    :pswitch_3
    const/16 v0, 0x263

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "n"

    const-string v1, "d"

    invoke-direct {p0, v1}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v6}, Lbf/O;->h(I)V

    move v6, v7

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "n"

    const-string v1, "d"

    invoke-direct {p0, v1}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v10}, Lbf/O;->h(I)V

    move v6, v7

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "n"

    const-string v1, "d"

    invoke-direct {p0, v1}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbf/O;->h(I)V

    move v6, v7

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x61

    const-string v1, "n"

    const-string v2, "d"

    invoke-direct {p0, v2}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0, v7}, LaS/a;->a(Z)V

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v1, v0}, Lbf/O;->a(Lcom/google/googlenav/ui/s;Lax/w;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/w;)V

    :cond_4
    move v6, v7

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p0, p2}, Lbf/O;->b(I)V

    invoke-virtual {p0}, Lbf/O;->be()Z

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    iget-object v2, p0, Lbf/O;->I:Lbf/I;

    invoke-virtual {v2, v6}, Lbf/I;->b(I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Lax/b;Ljava/util/List;)V

    new-array v0, v10, [Ljava/lang/String;

    const-string v1, "a=s"

    aput-object v1, v0, v6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "i="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {p0}, Lbf/O;->av()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v6, v7

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "od"

    invoke-direct {p0, v0}, Lbf/O;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/O;->bK()V

    move v6, v7

    goto/16 :goto_0

    :sswitch_12
    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lbf/O;->bM()V

    :cond_5
    move v6, v7

    goto/16 :goto_0

    :sswitch_13
    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "nc"

    invoke-direct {p0, v0}, Lbf/O;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/O;->bL()V

    :cond_6
    move v6, v7

    goto/16 :goto_0

    :sswitch_14
    invoke-super {p0, p1, p2, p3}, Lbf/F;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/O;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lbf/O;->W()V

    move v6, v7

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-static {v0, v1}, Lbf/O;->b(Lcom/google/googlenav/ui/s;Lax/b;)V

    move v6, v7

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_14
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_4
        0xca -> :sswitch_5
        0xcb -> :sswitch_6
        0xcc -> :sswitch_7
        0xdc -> :sswitch_11
        0xdd -> :sswitch_12
        0xe5 -> :sswitch_9
        0xe6 -> :sswitch_11
        0xe7 -> :sswitch_8
        0xf0 -> :sswitch_16
        0xf1 -> :sswitch_1
        0xf2 -> :sswitch_a
        0xf4 -> :sswitch_3
        0x1f4 -> :sswitch_13
        0x25c -> :sswitch_c
        0x25d -> :sswitch_d
        0x25e -> :sswitch_f
        0x261 -> :sswitch_b
        0x263 -> :sswitch_e
        0x3f9 -> :sswitch_15
        0x5e6 -> :sswitch_10
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Lat/b;Z)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lbf/O;->D:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lbf/O;->E:Z

    if-eqz v2, :cond_1

    :goto_0
    invoke-direct {p0, p1, v0}, Lbf/O;->a(Lat/b;I)V

    :cond_0
    :goto_1
    return v1

    :cond_1
    invoke-virtual {p0}, Lbf/O;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lat/b;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lbf/O;->D:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {p0, p1}, Lbf/O;->b(Lat/b;)LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/O;->a(LaN/B;)Lo/D;

    move-result-object v3

    invoke-static {v0, v3}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v3

    iget-boolean v0, p0, Lbf/O;->E:Z

    if-eqz v0, :cond_3

    new-instance v0, Lax/j;

    invoke-virtual {v2}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lax/j;-><init>(Lax/y;Lax/y;)V

    :goto_2
    invoke-virtual {p0, v0}, Lbf/O;->b(Lax/j;)V

    invoke-virtual {p0}, Lbf/O;->c()V

    goto :goto_1

    :cond_3
    new-instance v0, Lax/j;

    invoke-virtual {v2}, Lax/b;->aq()Lax/y;

    move-result-object v2

    invoke-direct {v0, v2, v3}, Lax/j;-><init>(Lax/y;Lax/y;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p1}, Lat/b;->f()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_5
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v3

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LaN/u;->b(II)LaN/B;

    move-result-object v2

    invoke-virtual {p0, v2}, Lbf/O;->b(LaN/B;)I

    move-result v2

    if-ltz v2, :cond_b

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v3, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    iget-object v3, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iget-object v5, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v5, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lbf/O;->a(Lcom/google/googlenav/E;Lcom/google/googlenav/E;)Z

    move-result v5

    if-eqz v5, :cond_9

    move v2, v0

    :cond_6
    :goto_3
    if-eqz v2, :cond_7

    if-ne v2, v3, :cond_0

    :cond_7
    iput-boolean v1, p0, Lbf/O;->D:Z

    if-nez v2, :cond_8

    move v0, v1

    :cond_8
    iput-boolean v0, p0, Lbf/O;->E:Z

    iget-boolean v0, p0, Lbf/O;->E:Z

    if-eqz v0, :cond_a

    const-string v0, "ds"

    :goto_4
    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    invoke-direct {p0, p1, v2}, Lbf/O;->a(Lat/b;I)V

    goto/16 :goto_1

    :cond_9
    iget-object v5, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v5, v3}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lbf/O;->a(Lcom/google/googlenav/E;Lcom/google/googlenav/E;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v2, v3

    goto :goto_3

    :cond_a
    const-string v0, "de"

    goto :goto_4

    :cond_b
    move v2, v1

    :goto_5
    if-eqz v2, :cond_c

    const/4 v2, -0x1

    const-string v3, "c"

    const-string v4, "c"

    invoke-virtual {p0, v2, v3, v4}, Lbf/O;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/O;->Z()V

    if-eqz p2, :cond_c

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_c
    move v1, v0

    goto/16 :goto_1

    :cond_d
    move v2, v0

    goto :goto_5
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    const/4 v0, 0x4

    invoke-super {p0, p1, v0}, Lbf/F;->a(Lcom/google/googlenav/ui/view/t;I)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 2

    iget-object v0, p0, Lbf/O;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    invoke-static {p1, v0}, Lax/b;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    instance-of v1, v0, Lax/s;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lax/b;->G()V

    :cond_0
    iput-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f11000b

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lbf/F;->aF()I

    move-result v0

    goto :goto_0
.end method

.method public aH()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0xfc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aI()Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0xf8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x238

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v1}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x23e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lax/b;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x233

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x240

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aL()Lam/f;
    .locals 2

    iget-object v0, p0, Lbf/O;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ah:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lax/b;->z_()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/b;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Lax/b;->F()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/b;)V

    :cond_2
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lax/b;->n()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->c_(Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public aU()V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->az()I

    move-result v0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->aI()Lax/b;

    move-result-object v1

    iput-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v1, v1, Lax/w;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lbf/O;->f()Lax/w;

    move-result-object v1

    invoke-virtual {v1, v0}, Lax/w;->w(I)V

    :cond_0
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->c_(Z)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->J:Ljava/util/List;

    return-void
.end method

.method public aW()V
    .locals 2

    invoke-virtual {p0}, Lbf/O;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/O;->b(B)V

    :cond_0
    invoke-super {p0}, Lbf/F;->aW()V

    invoke-direct {p0}, Lbf/O;->bN()V

    sget-object v0, LaE/b;->a:LaE/b;

    invoke-virtual {v0}, LaE/b;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/O;->J:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbf/O;->bO()V

    :cond_1
    return-void
.end method

.method public aX()V
    .locals 0

    invoke-virtual {p0}, Lbf/O;->be()Z

    invoke-super {p0}, Lbf/F;->aX()V

    invoke-virtual {p0}, Lbf/O;->bt()V

    invoke-direct {p0}, Lbf/O;->bP()V

    return-void
.end method

.method public aa()Z
    .locals 1

    invoke-super {p0}, Lbf/F;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbf/O;->D:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ab()Z
    .locals 1

    iget-boolean v0, p0, Lbf/O;->D:Z

    return v0
.end method

.method public ad()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ap()V
    .locals 0

    return-void
.end method

.method public aq()V
    .locals 1

    iget-object v0, p0, Lbf/O;->I:Lbf/I;

    invoke-virtual {v0}, Lbf/I;->a()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public ar()Lcom/google/googlenav/F;
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    return-object v0
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b(Lat/b;)LaN/B;
    .locals 4

    iget-object v0, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    sget v3, Lbf/O;->w:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LaN/u;->b(II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public b(Lax/j;)V
    .locals 2

    invoke-virtual {p0, p1}, Lbf/O;->a(Lax/j;)Lax/b;

    move-result-object v0

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    invoke-virtual {p0}, Lbf/O;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lbf/O;->j(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public bc()Ljava/lang/String;
    .locals 5

    const/16 v4, 0x26

    const/16 v3, 0x3d

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Directions"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "saddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "daddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "dirflg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->k()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const-string v1, "r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    const-string v1, "w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_3
    const-string v1, "b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public be()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/r;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bf()Z
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "nc"

    invoke-direct {p0, v1}, Lbf/O;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/O;->bL()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/O;->be()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lbf/O;->ae()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v4}, Lbf/O;->b(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_3
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_0

    :sswitch_0
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v1, v2}, Lbf/O;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, v1, v5}, Lbf/O;->c(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, v1, v5}, Lbf/O;->b(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_3
    move v0, v1

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v1, v1, Lax/w;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1, v4}, Lax/b;->s(I)V

    :cond_4
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->f(Lax/b;)V

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_0

    :sswitch_5
    const-string v1, "b"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(Lax/k;)V

    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_0

    :sswitch_6
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    const-string v3, ""

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x7 -> :sswitch_0
        0x12 -> :sswitch_3
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1c -> :sswitch_6
    .end sparse-switch
.end method

.method public bg()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/w;

    return v0
.end method

.method public bh()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/s;

    return v0
.end method

.method public bi()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/x;

    return v0
.end method

.method public bj()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/i;

    return v0
.end method

.method public bk()Z
    .locals 1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/O;->d(Lax/b;)Z

    move-result v0

    return v0
.end method

.method public bl()Z
    .locals 1

    iget-object v0, p0, Lbf/O;->A:LaH/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->A:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->A:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method bm()Ljava/lang/String;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->P()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lax/b;->O()I

    move-result v0

    invoke-virtual {v2}, Lax/b;->av()I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2}, Lax/b;->N()Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x10b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v2}, Lax/b;->M()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    const/16 v1, 0xff

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v5

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_2
    return-object v0

    :cond_1
    if-eqz v2, :cond_2

    move-object v0, v2

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v2, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method bn()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/O;->bh()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v1

    invoke-virtual {v1}, Lax/s;->N()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lbf/O;->bB()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->F()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x25e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v2, 0x3c

    if-lt v1, v2, :cond_0

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v0

    invoke-virtual {v0}, Lax/s;->M()I

    move-result v0

    add-int/2addr v0, v1

    const/16 v1, 0x5c3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method bo()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lbf/O;->f()Lax/w;

    move-result-object v1

    invoke-virtual {v1}, Lax/w;->Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xe3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bp()Z
    .locals 1

    iget-boolean v0, p0, Lbf/O;->D:Z

    return v0
.end method

.method br()V
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lax/b;->aA()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v3, v0}, Lax/b;->l(I)Lax/h;

    move-result-object v2

    invoke-virtual {v2}, Lax/h;->g()[LaN/B;

    move-result-object v5

    array-length v6, v5

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    aget-object v7, v5, v2

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v4, v0}, Lbf/O;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public bs()V
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/O;->J:Ljava/util/List;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lbf/O;->bi()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    invoke-virtual {p0}, Lbf/O;->br()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lbf/O;->bO()V

    goto :goto_0
.end method

.method public bt()V
    .locals 1

    iget-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->c()V

    iget-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lbf/O;->F:Z

    return-void
.end method

.method public c(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    return-void
.end method

.method public c(Lcom/google/googlenav/F;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/O;->ax()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/O;->ai()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/O;->y()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    check-cast v0, Lax/b;

    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->a()V

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v2, v2, Lax/s;

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v2

    invoke-virtual {v2}, Lax/s;->E()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    :goto_1
    invoke-direct {p0, p1, v1}, Lbf/O;->a(Lcom/google/googlenav/F;Z)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lax/b;->r(I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public d()Z
    .locals 1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->y()Z

    move-result v0

    return v0
.end method

.method public d(Lax/b;)Z
    .locals 3

    invoke-virtual {p1}, Lax/b;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/b;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/b;->z()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lax/b;->T()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lax/b;->aB()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aC()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lbf/O;->a(LaN/B;LaN/B;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 1

    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/O;->bw()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lbf/O;->bi()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbf/O;->by()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lbf/O;->bz()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lbf/O;->bA()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Lax/w;
    .locals 1

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    return-object v0
.end method

.method protected f(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lbf/O;->ae()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "m"

    const-string v2, "#"

    invoke-virtual {p0, v1, v2}, Lbf/O;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-direct {p0, v1}, Lbf/O;->i(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "l"

    const-string v2, "#"

    invoke-virtual {p0, v1, v2}, Lbf/O;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/O;->c(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/O;->ai()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-direct {p0, p1}, Lbf/O;->g(Lat/a;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lbf/O;->bu()Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbf/O;->b(LaN/B;)I

    move-result v1

    if-ltz v1, :cond_4

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    :cond_4
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lbf/O;->bf()Z

    move-result v0

    goto :goto_0

    :cond_5
    invoke-virtual {p0, p1}, Lbf/O;->e(Lat/a;)Z

    move-result v0

    goto :goto_0
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lbf/O;->bf()Z

    return-void
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/d;

    invoke-direct {v0, p0}, Lbh/d;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected m()V
    .locals 0

    return-void
.end method

.method protected n()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/O;->i(Z)V

    invoke-super {p0}, Lbf/F;->n()V

    return-void
.end method

.method protected x()I
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/O;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lbf/O;->au()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lbf/O;->bk()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
