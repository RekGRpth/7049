.class public Lbf/R;
.super Lbf/h;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/ui/aW;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0}, Lcom/google/googlenav/friend/aI;->a(ZZ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->be:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v1, Lcom/google/googlenav/ui/aV;->n:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/friend/aI;Z)Ljava/util/List;
    .locals 7

    const/4 v2, 0x1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->A()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lbf/R;->c:Lbf/i;

    check-cast v0, Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bJ()J

    move-result-wide v5

    invoke-static {p1, v5, v6, v2, p2}, Lcom/google/googlenav/friend/M;->a(Lcom/google/googlenav/friend/aI;JZZ)Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v4, Lcom/google/googlenav/ui/aV;->m:Lcom/google/googlenav/ui/aV;

    if-nez v5, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v0, v4, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->b:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v3

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public b()Lcom/google/googlenav/ui/view/d;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbf/R;->a()Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lbf/R;->c:Lbf/i;

    check-cast v0, Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lbf/R;->a(Lcom/google/googlenav/friend/aI;Z)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/R;->a(Landroid/view/View;Ljava/util/List;)V

    invoke-direct {p0, v0}, Lbf/R;->a(Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ui/aW;

    aput-object v0, v2, v4

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lbf/R;->b(Landroid/view/View;Ljava/util/List;)V

    :cond_0
    invoke-virtual {p0, v1, v3}, Lbf/R;->c(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v1, v3, v3}, Lbf/R;->a(Landroid/view/View;Lam/f;Lam/f;)V

    invoke-virtual {p0, v1, v3}, Lbf/R;->a(Landroid/view/View;Lam/f;)V

    invoke-virtual {p0, v1, v3}, Lbf/R;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    invoke-virtual {p0, v1, v3}, Lbf/R;->a(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, v1, v4}, Lbf/R;->a(Landroid/view/View;Z)V

    sget-object v0, Lbf/R;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/R;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/R;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0
.end method
