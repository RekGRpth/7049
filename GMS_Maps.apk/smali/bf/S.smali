.class public Lbf/S;
.super Lbf/be;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/googlenav/L;


# direct methods
.method public constructor <init>(Lbf/m;Lcom/google/googlenav/L;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/be;-><init>(Lbf/m;)V

    iput-object p2, p0, Lbf/S;->b:Lcom/google/googlenav/L;

    return-void
.end method

.method static synthetic a(Lbf/S;)Lbf/X;
    .locals 1

    invoke-direct {p0}, Lbf/S;->d()Lbf/X;

    move-result-object v0

    return-object v0
.end method

.method private a()Lcom/google/googlenav/ui/friend/n;
    .locals 1

    new-instance v0, Lbf/U;

    invoke-direct {v0, p0}, Lbf/U;-><init>(Lbf/S;)V

    return-object v0
.end method

.method private a(Lcom/google/googlenav/friend/aI;)V
    .locals 9

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lbf/S;->d()Lbf/X;

    move-result-object v0

    invoke-virtual {v0}, Lbf/X;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    invoke-direct {p0}, Lbf/S;->d()Lbf/X;

    move-result-object v0

    invoke-virtual {v0}, Lbf/X;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v6

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->i()Landroid/app/Activity;

    move-result-object v0

    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/16 v5, 0x98

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    move-object v7, v0

    move v5, v2

    move v2, v1

    :goto_0
    const/16 v0, 0x150

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    const/16 v0, 0x29d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v4

    new-instance v0, Lbf/T;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbf/T;-><init>(Lbf/S;ILcom/google/googlenav/ui/wizard/jv;IILcom/google/googlenav/ui/s;)V

    invoke-virtual {v8, v7, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_0
    new-array v0, v4, [Ljava/lang/CharSequence;

    const/4 v5, -0x1

    move-object v7, v0

    move v4, v1

    goto :goto_0
.end method

.method static synthetic b(Lbf/S;)Lcom/google/googlenav/L;
    .locals 1

    iget-object v0, p0, Lbf/S;->b:Lcom/google/googlenav/L;

    return-object v0
.end method

.method private b()Lcom/google/googlenav/ui/friend/p;
    .locals 2

    invoke-direct {p0}, Lbf/S;->d()Lbf/X;

    move-result-object v0

    invoke-virtual {v0}, Lbf/X;->bR()Lcom/google/googlenav/friend/t;

    move-result-object v0

    new-instance v1, Lbf/V;

    invoke-direct {v1, p0, v0}, Lbf/V;-><init>(Lbf/S;Lcom/google/googlenav/friend/t;)V

    return-object v1
.end method

.method private c()Lcom/google/googlenav/ui/friend/o;
    .locals 1

    new-instance v0, Lbf/W;

    invoke-direct {v0, p0}, Lbf/W;-><init>(Lbf/S;)V

    return-object v0
.end method

.method private d()Lbf/X;
    .locals 1

    iget-object v0, p0, Lbf/S;->a:Lbf/i;

    check-cast v0, Lbf/X;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;
    .locals 7

    invoke-direct {p0}, Lbf/S;->d()Lbf/X;

    move-result-object v1

    invoke-virtual {v1}, Lbf/X;->f()Lcom/google/googlenav/ui/friend/q;

    move-result-object v2

    invoke-direct {p0}, Lbf/S;->d()Lbf/X;

    move-result-object v0

    invoke-virtual {v0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/google/googlenav/ui/friend/q;->b()J

    move-result-wide v3

    invoke-virtual {v1}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/aK;->a(Ljava/lang/Long;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/friend/q;->a(J)V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lbf/S;->a(Lcom/google/googlenav/friend/aI;)V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v1

    if-ne v1, v0, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/ui/friend/q;->b()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/friend/q;->a(J)V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/googlenav/ui/friend/a;

    invoke-direct {p0}, Lbf/S;->c()Lcom/google/googlenav/ui/friend/o;

    move-result-object v2

    invoke-direct {p0}, Lbf/S;->b()Lcom/google/googlenav/ui/friend/p;

    move-result-object v3

    invoke-direct {p0}, Lbf/S;->a()Lcom/google/googlenav/ui/friend/n;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/googlenav/ui/friend/a;-><init>(Lcom/google/googlenav/friend/aI;Lcom/google/googlenav/ui/friend/o;Lcom/google/googlenav/ui/friend/p;Lcom/google/googlenav/ui/friend/n;)V

    move-object v0, v1

    goto :goto_1
.end method
