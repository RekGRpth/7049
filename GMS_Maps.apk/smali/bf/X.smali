.class public Lbf/X;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaB/p;
.implements Lbf/ag;
.implements Lcom/google/googlenav/friend/an;


# instance fields
.field private final B:Lcom/google/googlenav/ui/friend/q;

.field private C:Lcom/google/googlenav/friend/J;

.field private D:Lcom/google/googlenav/friend/p;

.field private E:Lbf/ae;

.field private F:Lcom/google/googlenav/friend/ag;

.field private G:LaB/a;

.field private H:Lcom/google/googlenav/ui/friend/D;

.field private I:Lcom/google/googlenav/ui/friend/D;

.field private J:Lbf/a;

.field private K:Lcom/google/googlenav/android/aa;

.field private L:Lcom/google/googlenav/friend/aK;

.field private M:Z

.field private N:[Lcom/google/googlenav/ui/aI;

.field private O:Lcom/google/googlenav/friend/t;

.field private P:Landroid/graphics/Point;

.field private Q:Lbf/aG;

.field private R:Z

.field private S:Lcom/google/googlenav/ui/friend/C;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lbf/a;)V
    .locals 8

    invoke-direct {p0, p1, p2, p3, p5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    new-instance v0, Lcom/google/googlenav/ui/friend/q;

    invoke-direct {v0}, Lcom/google/googlenav/ui/friend/q;-><init>()V

    iput-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    new-instance v1, Lcom/google/googlenav/friend/aK;

    const/4 v0, 0x0

    check-cast v0, Ljava/util/Vector;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/aK;-><init>(Ljava/util/Vector;)V

    iput-object v1, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lbf/X;->P:Landroid/graphics/Point;

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/X;->Q:Lbf/aG;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v5

    move-object v0, p0

    move-object v1, p4

    move-object v2, p6

    move-object v3, p7

    move-object/from16 v4, p8

    move-object v6, p1

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lbf/X;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/s;Lbf/a;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/F;Lbf/a;)V
    .locals 8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    new-instance v0, Lcom/google/googlenav/ui/friend/q;

    invoke-direct {v0}, Lcom/google/googlenav/ui/friend/q;-><init>()V

    iput-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    new-instance v1, Lcom/google/googlenav/friend/aK;

    const/4 v0, 0x0

    check-cast v0, Ljava/util/Vector;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/aK;-><init>(Ljava/util/Vector;)V

    iput-object v1, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lbf/X;->P:Landroid/graphics/Point;

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/X;->Q:Lbf/aG;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v5

    move-object v0, p0

    move-object v1, p4

    move-object v2, p6

    move-object v3, p7

    move-object/from16 v4, p8

    move-object v6, p1

    move-object/from16 v7, p10

    invoke-direct/range {v0 .. v7}, Lbf/X;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/s;Lbf/a;)V

    return-void
.end method

.method static synthetic a(Lbf/X;)Lcom/google/googlenav/ui/friend/q;
    .locals 1

    iget-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    return-object v0
.end method

.method static synthetic a(Lbf/X;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/X;->l(Z)V

    return-void
.end method

.method private a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/s;Lbf/a;)V
    .locals 8

    new-instance v0, Lbf/S;

    invoke-virtual {p6}, Lcom/google/googlenav/ui/s;->l()Lcom/google/googlenav/L;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbf/S;-><init>(Lbf/m;Lcom/google/googlenav/L;)V

    iput-object v0, p0, Lbf/X;->A:Lbf/be;

    invoke-virtual {p6}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->a()LaB/a;

    move-result-object v0

    iput-object v0, p0, Lbf/X;->G:LaB/a;

    new-instance v0, Lcom/google/googlenav/ui/friend/D;

    iget-object v1, p0, Lbf/X;->G:LaB/a;

    const/4 v2, 0x0

    sget v3, Lcom/google/googlenav/ui/bi;->bp:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/friend/D;-><init>(LaB/a;Lcom/google/googlenav/friend/aK;I)V

    iput-object v0, p0, Lbf/X;->H:Lcom/google/googlenav/ui/friend/D;

    new-instance v0, Lcom/google/googlenav/ui/friend/D;

    iget-object v1, p0, Lbf/X;->G:LaB/a;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/friend/D;-><init>(LaB/a;Lcom/google/googlenav/friend/aK;I)V

    iput-object v0, p0, Lbf/X;->I:Lcom/google/googlenav/ui/friend/D;

    iput-object p2, p0, Lbf/X;->C:Lcom/google/googlenav/friend/J;

    iput-object p3, p0, Lbf/X;->D:Lcom/google/googlenav/friend/p;

    iput-object p4, p0, Lbf/X;->F:Lcom/google/googlenav/friend/ag;

    iput-object p1, p0, Lbf/X;->K:Lcom/google/googlenav/android/aa;

    iput-object p7, p0, Lbf/X;->J:Lbf/a;

    new-instance v0, Lbf/ae;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbf/ae;-><init>(Lbf/X;Lbf/Y;)V

    iput-object v0, p0, Lbf/X;->E:Lbf/ae;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ui/aI;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v0, v1

    iput-object v0, p0, Lbf/X;->N:[Lcom/google/googlenav/ui/aI;

    new-instance v0, Lcom/google/googlenav/friend/t;

    iget-object v3, p0, Lbf/X;->d:LaN/u;

    iget-object v4, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    move-object v1, p1

    move-object v2, p6

    move-object v5, p0

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/friend/t;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/s;LaN/u;Lcom/google/googlenav/ui/friend/q;Lbf/X;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lbf/X;->O:Lcom/google/googlenav/friend/t;

    const-string v0, "WIFI_DETECTION_ALERT_ALLOWED"

    const/4 v1, 0x1

    new-instance v2, Lbf/Y;

    invoke-direct {v2, p0}, Lbf/Y;-><init>(Lbf/X;)V

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    iget-object v0, p0, Lbf/X;->G:LaB/a;

    invoke-virtual {v0, p0}, LaB/a;->a(LaB/p;)V

    new-instance v0, Lcom/google/googlenav/ui/friend/C;

    invoke-direct {v0}, Lcom/google/googlenav/ui/friend/C;-><init>()V

    iput-object v0, p0, Lbf/X;->S:Lcom/google/googlenav/ui/friend/C;

    new-instance v0, Lbf/aG;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lbf/ac;

    iget-object v3, p0, Lbf/X;->S:Lcom/google/googlenav/ui/friend/C;

    invoke-direct {v2, p0, v3}, Lbf/ac;-><init>(Lbf/X;Lcom/google/googlenav/ui/friend/C;)V

    invoke-direct {v0, v1, v2}, Lbf/aG;-><init>(Las/c;Lbf/aH;)V

    iput-object v0, p0, Lbf/X;->Q:Lbf/aG;

    return-void
.end method

.method private a(Lcom/google/googlenav/friend/aI;IILam/e;Lcom/google/googlenav/ui/r;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/X;->c:LaN/p;

    iget-object v2, p0, Lbf/X;->e:Landroid/graphics/Point;

    invoke-virtual {v1, v0, v2}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, p2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    iget-object v0, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, p3

    iput v1, v0, Landroid/graphics/Point;->y:I

    iget-object v0, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {p4, v0, v1, p2, p3}, Lam/r;->a(Lam/e;IIII)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->z()C

    move-result v0

    iget-object v1, p0, Lbf/X;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p5}, Lcom/google/googlenav/ui/r;->a()Lam/e;

    move-result-object v2

    iget-object v3, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/googlenav/ui/bi;->a(CLam/e;II)V

    iget-object v0, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sget v1, LaB/m;->a:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lbf/X;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sget v2, LaB/m;->b:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lbf/X;->G:LaB/a;

    new-instance v3, Lcom/google/googlenav/ui/friend/E;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v4

    sget v5, Lcom/google/googlenav/ui/bi;->bq:I

    invoke-direct {v3, v4, v5}, Lcom/google/googlenav/ui/friend/E;-><init>(Ljava/lang/Long;I)V

    invoke-virtual {v2, v3}, LaB/a;->a(Lcom/google/googlenav/ui/friend/E;)Lam/f;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {p5}, Lcom/google/googlenav/ui/r;->a()Lam/e;

    move-result-object v3

    invoke-interface {v3, v2, v0, v1}, Lam/e;->a(Lam/f;II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p5}, Lcom/google/googlenav/ui/r;->a()Lam/e;

    move-result-object v2

    iget-object v3, p0, Lbf/X;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v4

    sget v5, Lcom/google/googlenav/ui/bi;->bq:I

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/ui/bi;->a(ZI)Lam/f;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Lam/e;->a(Lam/f;II)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lbf/X;)Lcom/google/googlenav/friend/p;
    .locals 1

    iget-object v0, p0, Lbf/X;->D:Lcom/google/googlenav/friend/p;

    return-object v0
.end method

.method static synthetic b(Lbf/X;Z)Z
    .locals 0

    iput-boolean p1, p0, Lbf/X;->R:Z

    return p1
.end method

.method public static bN()I
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const-wide v1, 0x4007333333333333L

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/Config;->a(D)I

    move-result v0

    return v0
.end method

.method private bU()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0xf

    invoke-virtual {p0}, Lbf/X;->bQ()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private bV()Lcom/google/googlenav/friend/aI;
    .locals 4

    const/4 v0, 0x0

    const/4 v2, -0x1

    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    iget-object v3, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    if-lt v1, v3, :cond_2

    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->a(I)V

    move v1, v2

    :cond_2
    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    goto :goto_0
.end method

.method private bW()V
    .locals 5

    invoke-direct {p0}, Lbf/X;->bY()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->r()J

    move-result-wide v1

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    long-to-int v1, v1

    iget-object v2, p0, Lbf/X;->N:[Lcom/google/googlenav/ui/aI;

    const/4 v3, 0x0

    new-instance v4, Lbf/aa;

    invoke-direct {v4, p0, v1, v0}, Lbf/aa;-><init>(Lbf/X;ILcom/google/googlenav/friend/aI;)V

    aput-object v4, v2, v3

    :cond_0
    return-void
.end method

.method private bX()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lbf/X;->o:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    if-nez v3, :cond_3

    move v3, v1

    :goto_1
    iget-object v4, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lbf/X;->ah()Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v1

    :goto_2
    iget-object v5, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v5

    if-nez v5, :cond_5

    move v5, v1

    :goto_3
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    if-nez v4, :cond_0

    if-eqz v5, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v4, v2

    goto :goto_2

    :cond_5
    move v5, v2

    goto :goto_3
.end method

.method private bY()Z
    .locals 4

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/aH;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->r()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbf/X;->o:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bZ()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/X;->h:Lcom/google/googlenav/E;

    invoke-virtual {p0}, Lbf/X;->an()Z

    return-void
.end method

.method static synthetic c(Lbf/X;)Lbf/ae;
    .locals 1

    iget-object v0, p0, Lbf/X;->E:Lbf/ae;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/aZ;)V
    .locals 14

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->c()Z

    move-result v6

    const-wide/16 v0, -0x1

    if-eqz v6, :cond_3

    invoke-direct {p0}, Lbf/X;->bV()Lcom/google/googlenav/friend/aI;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :cond_2
    iget-object v2, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->a(B)V

    move-wide v1, v0

    :goto_1
    new-instance v7, Ljava/util/Hashtable;

    invoke-direct {v7}, Ljava/util/Hashtable;-><init>()V

    if-eqz v6, :cond_5

    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    if-ge v0, v3, :cond_5

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    if-nez v6, :cond_4

    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->a(B)V

    :cond_4
    move-wide v1, v0

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/google/googlenav/friend/aK;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/aK;-><init>(Lcom/google/googlenav/F;)V

    iput-object v0, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v5, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v5}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v8

    move v5, v0

    :goto_3
    if-ge v5, v8, :cond_c

    iget-object v0, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/googlenav/friend/aH;->b()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lbf/X;->E:Lbf/ae;

    invoke-virtual {v0, v9}, Lbf/ae;->a(Lcom/google/googlenav/friend/aI;)Z

    :cond_6
    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    if-eqz v0, :cond_f

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v10

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->r()J

    move-result-wide v11

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v13

    invoke-static {v10, v11, v12, v13}, Lcom/google/googlenav/friend/J;->a(LaN/B;JLaN/B;)Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/google/googlenav/friend/aI;->d(Z)V

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->H()Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v3, 0x1

    :cond_7
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->H()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->o()J

    move-result-wide v10

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->o()J

    move-result-wide v12

    cmp-long v0, v10, v12

    if-nez v0, :cond_a

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Lcom/google/googlenav/friend/aI;->h(Z)V

    :cond_8
    :goto_4
    if-nez v4, :cond_f

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->k()Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_5
    if-nez v6, :cond_b

    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v4

    if-eqz v4, :cond_b

    const-wide/16 v10, -0x1

    cmp-long v4, v1, v10

    if-nez v4, :cond_b

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->az()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual {p1, v5}, Lcom/google/googlenav/aZ;->a(I)V

    :cond_9
    :goto_6
    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->y()V

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v0

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lbf/X;->S:Lcom/google/googlenav/ui/friend/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/C;->a()V

    goto :goto_4

    :cond_b
    invoke-virtual {v9}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v1, v10

    if-nez v4, :cond_9

    invoke-virtual {p1, v5}, Lcom/google/googlenav/aZ;->a(I)V

    if-eqz v6, :cond_9

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->d()B

    move-result v4

    iget-object v10, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v10}, Lcom/google/googlenav/F;->d()B

    move-result v10

    if-eq v4, v10, :cond_9

    iget-object v4, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v4}, Lcom/google/googlenav/F;->d()B

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/googlenav/aZ;->a(B)V

    goto :goto_6

    :cond_c
    if-eqz v3, :cond_d

    iget-object v0, p0, Lbf/X;->S:Lcom/google/googlenav/ui/friend/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/C;->b()V

    invoke-virtual {p0}, Lbf/X;->bO()Lbf/aG;

    move-result-object v0

    invoke-virtual {v0}, Lbf/aG;->a()V

    :cond_d
    iget-object v0, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->a()V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/4 v0, 0x0

    iget-object v2, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    :goto_7
    if-ge v0, v2, :cond_e

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    new-instance v4, LaB/n;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v4, v5, v3}, LaB/n;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_e
    iget-object v0, p0, Lbf/X;->G:LaB/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LaB/a;->a(Ljava/util/Vector;LaB/p;)Z

    new-instance v0, Lcom/google/googlenav/ui/friend/D;

    iget-object v1, p0, Lbf/X;->G:LaB/a;

    iget-object v2, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    sget v3, Lcom/google/googlenav/ui/bi;->bp:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/friend/D;-><init>(LaB/a;Lcom/google/googlenav/friend/aK;I)V

    iput-object v0, p0, Lbf/X;->H:Lcom/google/googlenav/ui/friend/D;

    new-instance v0, Lcom/google/googlenav/ui/friend/D;

    iget-object v1, p0, Lbf/X;->G:LaB/a;

    iget-object v2, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/friend/D;-><init>(LaB/a;Lcom/google/googlenav/friend/aK;I)V

    iput-object v0, p0, Lbf/X;->I:Lcom/google/googlenav/ui/friend/D;

    goto/16 :goto_0

    :cond_f
    move v0, v4

    goto/16 :goto_5
.end method

.method static synthetic d(Lbf/X;)V
    .locals 0

    invoke-direct {p0}, Lbf/X;->bZ()V

    return-void
.end method

.method static synthetic e(Lbf/X;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lbf/X;->K:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method static synthetic f(Lbf/X;)Z
    .locals 1

    iget-boolean v0, p0, Lbf/X;->M:Z

    return v0
.end method

.method private g(Lat/a;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/X;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/X;->bK()V

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/4 v3, 0x7

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lbf/X;->ag()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/X;->bL()V

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private l(Z)V
    .locals 8

    const/16 v1, 0x11

    invoke-virtual {p0}, Lbf/X;->ax()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/X;->M:Z

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->r()J

    move-result-wide v4

    long-to-int v0, v4

    int-to-long v6, v0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_2

    const v0, 0x7fffffff

    :cond_2
    invoke-static {v0, v3}, Lcom/google/googlenav/ui/ak;->a(ILaN/B;)LaN/Y;

    move-result-object v0

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    if-nez v0, :cond_4

    :cond_3
    const/16 v0, 0xf

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :cond_4
    iget-object v4, p0, Lbf/X;->d:LaN/u;

    invoke-virtual {v4, v3}, LaN/u;->e(LaN/B;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lbf/X;->d:LaN/u;

    invoke-virtual {v4}, LaN/u;->d()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_5
    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v4

    if-le v4, v1, :cond_6

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :cond_6
    if-eqz p1, :cond_8

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lbf/X;->d:LaN/u;

    invoke-virtual {v1, v3, v0}, LaN/u;->a(LaN/B;LaN/Y;)V

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/X;->M:Z

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lbf/X;->d:LaN/u;

    invoke-virtual {v1, v3, v0}, LaN/u;->d(LaN/B;LaN/Y;)V

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lbf/X;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->R()V

    iget-object v1, p0, Lbf/X;->d:LaN/u;

    invoke-virtual {v1, v3, v0}, LaN/u;->e(LaN/B;LaN/Y;)V

    goto :goto_1
.end method


# virtual methods
.method protected B()V
    .locals 0

    invoke-super {p0}, Lbf/m;->B()V

    invoke-virtual {p0}, Lbf/X;->w()V

    return-void
.end method

.method public I()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public M()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public N()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected O()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    if-eqz v2, :cond_1

    invoke-super {p0}, Lbf/m;->O()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aK;->g()I

    move-result v2

    if-le v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public Q_()V
    .locals 1

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, LaB/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-interface {v0}, LaB/p;->Q_()V

    :cond_0
    return-void
.end method

.method public R()V
    .locals 0

    invoke-super {p0}, Lbf/m;->R()V

    return-void
.end method

.method protected T()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x215

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .locals 1

    const/16 v0, 0x11

    return v0
.end method

.method protected X()Z
    .locals 2

    invoke-super {p0}, Lbf/m;->X()Z

    move-result v0

    iget-boolean v1, p0, Lbf/X;->R:Z

    or-int/2addr v0, v1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lbf/X;->R:Z

    return v0
.end method

.method public Y()V
    .locals 1

    invoke-super {p0}, Lbf/m;->Y()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/X;->M:Z

    return-void
.end method

.method public Z()V
    .locals 1

    invoke-super {p0}, Lbf/m;->Z()V

    iget-object v0, p0, Lbf/X;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->d(Lbf/i;)V

    return-void
.end method

.method public a(LaN/B;Ljava/lang/Long;)V
    .locals 3

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/aI;->a(LaN/g;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/aI;->a(J)V

    if-nez p2, :cond_1

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/aI;->c(J)V

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/aI;->c(J)V

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/X;->j(I)V

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/X;->M:Z

    :cond_3
    invoke-virtual {p0}, Lbf/X;->b()V

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    check-cast p1, Lcom/google/googlenav/aZ;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->p()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lbf/X;->c(Lcom/google/googlenav/aZ;)V

    iput-object p1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0}, Lbf/X;->bI()V

    invoke-virtual {p0}, Lbf/X;->ax()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/X;->j()V

    :cond_2
    invoke-static {}, Lcom/google/googlenav/friend/aH;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/X;->E:Lbf/ae;

    invoke-virtual {v0}, Lbf/ae;->a()V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .locals 5

    const-wide/16 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    invoke-static {p3}, Lcom/google/googlenav/Q;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :cond_0
    xor-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    return-void

    :cond_2
    move-wide v0, v2

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/friend/aI;)V
    .locals 4

    const/4 v3, -0x1

    if-nez p1, :cond_1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v3}, Lcom/google/googlenav/F;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/friend/aK;->a(J)I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->a(I)V

    goto :goto_0
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/aZ;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/X;->bU()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public a(ZLcom/google/googlenav/aZ;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/X;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    instance-of v3, v0, Lcom/google/googlenav/aZ;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ay()J

    move-result-wide v3

    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->ay()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-ltz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p2}, Lbf/X;->b(Lcom/google/googlenav/F;)V

    if-eqz p1, :cond_4

    iget-boolean v0, p0, Lbf/X;->M:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/X;->b(I)V

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lbf/X;->an()Z

    invoke-direct {p0, v1}, Lbf/X;->l(Z)V

    :cond_3
    :goto_3
    const/16 v0, 0x144

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lbf/X;->a(IILjava/lang/Object;)Z

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0, v2}, Lbf/X;->b(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lbf/X;->ag()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_7
    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/X;->b(I)V

    invoke-virtual {p0}, Lbf/X;->an()Z

    goto :goto_3
.end method

.method public a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 7

    const/4 v6, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_3

    aget-object v1, p1, v0

    invoke-static {v1, v6}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v2, v6}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/googlenav/friend/aK;->a(Ljava/lang/Long;)Lcom/google/googlenav/friend/aI;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/friend/aI;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbf/X;->S:Lcom/google/googlenav/ui/friend/C;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/friend/C;->a()V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/friend/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/r;->k()V

    goto :goto_0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lbf/X;->O:Lcom/google/googlenav/friend/t;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/friend/t;->a(IILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->a(Lcom/google/googlenav/F;)V

    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 5

    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0}, Lcom/google/googlenav/aZ;-><init>()V

    iput-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    iget-object v1, p0, Lbf/X;->J:Lbf/a;

    invoke-virtual {p0}, Lbf/X;->av()I

    move-result v2

    iget-object v3, p0, Lbf/X;->K:Lcom/google/googlenav/android/aa;

    new-instance v4, Lbf/Z;

    invoke-direct {v4, p0, v0}, Lbf/Z;-><init>(Lbf/X;Lcom/google/googlenav/aZ;)V

    invoke-virtual {v1, v2, v3, v4}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aD()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/X;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f11000c

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lbf/m;->aF()I

    move-result v0

    goto :goto_0
.end method

.method public aG()I
    .locals 1

    invoke-virtual {p0}, Lbf/X;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020288

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lbf/m;->aG()I

    move-result v0

    goto :goto_0
.end method

.method public aH()Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p0}, Lbf/X;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x195

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lbf/m;->aH()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0x155

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x23a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aL()Lam/f;
    .locals 2

    iget-object v0, p0, Lbf/X;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->af:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected aT()Z
    .locals 4

    iget-object v0, p0, Lbf/X;->F:Lcom/google/googlenav/friend/ag;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/ag;->a(Lcom/google/googlenav/friend/an;)V

    iget-object v0, p0, Lbf/X;->C:Lcom/google/googlenav/friend/J;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/J;->a()Lcom/google/googlenav/friend/K;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbf/X;->E:Lbf/ae;

    iget-object v2, v0, Lcom/google/googlenav/friend/K;->a:LaN/B;

    iget v3, v0, Lcom/google/googlenav/friend/K;->c:I

    iget-object v0, v0, Lcom/google/googlenav/friend/K;->d:Lo/D;

    invoke-virtual {v1, v2, v3, v0}, Lbf/ae;->a(LaN/B;ILo/D;)V

    :cond_0
    iget-object v0, p0, Lbf/X;->C:Lcom/google/googlenav/friend/J;

    iget-object v1, p0, Lbf/X;->E:Lbf/ae;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/J;->a(LaH/A;)V

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 2

    invoke-static {}, Lbf/ch;->e()V

    iget-object v0, p0, Lbf/X;->G:LaB/a;

    invoke-virtual {v0, p0}, LaB/a;->b(LaB/p;)V

    iget-object v0, p0, Lbf/X;->C:Lcom/google/googlenav/friend/J;

    iget-object v1, p0, Lbf/X;->E:Lbf/ae;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/J;->b(LaH/A;)V

    invoke-static {}, Lcom/google/googlenav/friend/aI;->x()V

    iget-object v0, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->h()V

    invoke-super {p0}, Lbf/m;->aU()V

    return-void
.end method

.method public aV()V
    .locals 4

    invoke-super {p0}, Lbf/m;->aV()V

    iget-object v0, p0, Lbf/X;->D:Lcom/google/googlenav/friend/p;

    const/4 v1, 0x0

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    return-void
.end method

.method public aW()V
    .locals 1

    invoke-super {p0}, Lbf/m;->aW()V

    invoke-static {}, Lcom/google/googlenav/friend/p;->g()Lcom/google/googlenav/friend/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/o;->c()V

    return-void
.end method

.method public aX()V
    .locals 1

    invoke-super {p0}, Lbf/m;->aX()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/X;->M:Z

    iget-object v0, p0, Lbf/X;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->n(Lbf/i;)V

    invoke-static {}, Lcom/google/googlenav/friend/p;->g()Lcom/google/googlenav/friend/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/o;->d()V

    return-void
.end method

.method public al()V
    .locals 1

    invoke-super {p0}, Lbf/m;->al()V

    iget-object v0, p0, Lbf/X;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->n(Lbf/i;)V

    return-void
.end method

.method public an()Z
    .locals 1

    invoke-super {p0}, Lbf/m;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/X;->bW()V

    :cond_0
    return v0
.end method

.method protected aq()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/friend/r;

    iget-object v1, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    iget-object v2, p0, Lbf/X;->H:Lcom/google/googlenav/ui/friend/D;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/google/googlenav/ui/friend/r;-><init>(Lbf/ag;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/friend/q;Lcom/google/googlenav/ui/aa;)V

    iput-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public au()Z
    .locals 1

    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lbf/m;->au()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lbf/X;->K:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/ab;

    invoke-direct {v1, p0}, Lbf/ab;-><init>(Lbf/X;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method protected b(Lcom/google/googlenav/friend/aI;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->y()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/X;->R:Z

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->a(I)V

    :cond_1
    invoke-virtual {p0}, Lbf/X;->bK()V

    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bG()Lcom/google/googlenav/friend/aI;
    .locals 1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/X;->ah()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lbf/X;->bV()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    goto :goto_0
.end method

.method public bH()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/friend/ad;->i()V

    invoke-virtual {p0}, Lbf/X;->J()V

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    const-class v1, Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/S;->a(Ljava/lang/Class;)V

    return-void
.end method

.method public bI()V
    .locals 1

    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/friend/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/r;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/X;->l()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/X;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/friend/a;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/a;->h()V

    goto :goto_0
.end method

.method public bJ()J
    .locals 5

    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Lbf/X;->bQ()J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    sub-long/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method public bK()V
    .locals 1

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lbf/X;->b(Lcom/google/googlenav/friend/aI;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/X;->l(Z)V

    invoke-static {}, Lcom/google/googlenav/friend/ad;->a()V

    :cond_0
    invoke-virtual {p0}, Lbf/X;->n()V

    return-void
.end method

.method public bL()V
    .locals 2

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lbf/X;->b(Lcom/google/googlenav/friend/aI;)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/X;->l(Z)V

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->a(Lcom/google/googlenav/F;)V

    :cond_1
    invoke-virtual {p0}, Lbf/X;->m()V

    return-void
.end method

.method public bM()V
    .locals 1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/aH;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/X;->b(I)V

    invoke-virtual {p0}, Lbf/X;->an()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/X;->b(I)V

    goto :goto_0
.end method

.method public bO()Lbf/aG;
    .locals 1

    iget-object v0, p0, Lbf/X;->Q:Lbf/aG;

    return-object v0
.end method

.method public bP()Z
    .locals 1

    iget-object v0, p0, Lbf/X;->Q:Lbf/aG;

    invoke-virtual {v0}, Lbf/aG;->b()Z

    move-result v0

    return v0
.end method

.method public bQ()J
    .locals 2

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ay()J

    move-result-wide v0

    return-wide v0
.end method

.method public bR()Lcom/google/googlenav/friend/t;
    .locals 1

    iget-object v0, p0, Lbf/X;->O:Lcom/google/googlenav/friend/t;

    return-object v0
.end method

.method public bS()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aH;->d()I

    move-result v0

    return v0
.end method

.method public bT()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aH;->e()Z

    move-result v0

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 2

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lbf/X;->bN()I

    move-result v0

    invoke-virtual {p0, p1}, Lbf/X;->f(Lcom/google/googlenav/E;)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public c()Lcom/google/googlenav/friend/aK;
    .locals 1

    iget-object v0, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    return-object v0
.end method

.method protected c(Lcom/google/googlenav/ui/r;)V
    .locals 3

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p0}, Lbf/X;->aj()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lbf/X;->b(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V

    goto :goto_0
.end method

.method public d()Lcom/google/googlenav/ui/friend/D;
    .locals 1

    iget-object v0, p0, Lbf/X;->H:Lcom/google/googlenav/ui/friend/D;

    return-object v0
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .locals 9

    const/16 v1, 0xe

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v4

    iget-object v0, p0, Lbf/X;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(B)I

    move-result v2

    iget-object v0, p0, Lbf/X;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(B)I

    move-result v3

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->j()Lcom/google/googlenav/common/f;

    move-result-object v7

    const/4 v0, 0x0

    invoke-virtual {v7}, Lcom/google/googlenav/common/f;->b()I

    move-result v8

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_0

    invoke-virtual {v7, v6}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v0

    long-to-int v0, v0

    iget-object v1, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v1

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/X;->a(Lcom/google/googlenav/friend/aI;IILam/e;Lcom/google/googlenav/ui/r;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v1

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/X;->a(Lcom/google/googlenav/friend/aI;IILam/e;Lcom/google/googlenav/ui/r;)V

    :cond_1
    return-void
.end method

.method public e(Lat/a;)Z
    .locals 10

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/16 v9, 0x34

    const/4 v2, 0x1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v4

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v5

    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-eq v5, v9, :cond_2

    const/16 v0, 0x36

    if-eq v5, v0, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    if-ne v5, v9, :cond_3

    const-string v0, "pf"

    :goto_1
    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/X;->ah()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eq v4, v1, :cond_4

    invoke-virtual {p0, v4}, Lbf/X;->j(I)V

    goto :goto_0

    :cond_3
    const-string v0, "nf"

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v0

    if-le v0, v2, :cond_6

    invoke-virtual {p0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->i()Lcom/google/googlenav/common/f;

    move-result-object v6

    int-to-long v7, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/f;->c(J)I

    move-result v7

    if-ltz v7, :cond_0

    if-ne v5, v9, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v7

    invoke-virtual {v6}, Lcom/google/googlenav/common/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {v6}, Lcom/google/googlenav/common/f;->b()I

    move-result v1

    rem-int/2addr v0, v1

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v0

    long-to-int v0, v0

    if-eq v0, v4, :cond_6

    invoke-virtual {p0, v0}, Lbf/X;->j(I)V

    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->a(Lcom/google/googlenav/F;)V

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v2, v3

    goto :goto_0
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 1

    invoke-virtual {p0}, Lbf/X;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/X;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/X;->bY()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/X;->N:[Lcom/google/googlenav/ui/aI;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/google/googlenav/ui/friend/q;
    .locals 1

    iget-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    return-object v0
.end method

.method protected f(Lat/a;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lbf/X;->ae()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lbf/X;->bK()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v2

    if-eqz v2, :cond_2

    iput v1, p0, Lbf/X;->v:I

    invoke-virtual {p0}, Lbf/X;->bK()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/friend/ad;->c()V

    invoke-virtual {p0}, Lbf/X;->l()V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lbf/X;->g(Lat/a;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lbf/X;->d(Lat/a;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public h()V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lbf/X;->bz()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lbf/X;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/friend/q;->a()B

    move-result v2

    if-eq v2, v0, :cond_3

    :goto_1
    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/q;->a()B

    move-result v0

    invoke-virtual {p0, v0}, Lbf/X;->a(B)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    :cond_5
    invoke-super {p0}, Lbf/m;->h()V

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/e;

    invoke-direct {v0, p0}, Lbh/e;-><init>(Lbf/i;)V

    return-object v0
.end method

.method public j()V
    .locals 0

    invoke-super {p0}, Lbf/m;->j()V

    return-void
.end method

.method public j(I)V
    .locals 3

    iget-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    iget-object v1, p0, Lbf/X;->L:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/friend/q;->a(J)V

    invoke-virtual {p0, p1}, Lbf/X;->b(I)V

    invoke-virtual {p0}, Lbf/X;->an()Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/X;->l(Z)V

    return-void
.end method

.method public k(Z)V
    .locals 1

    iget-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/friend/q;->a(Z)V

    invoke-virtual {p0}, Lbf/X;->bI()V

    return-void
.end method

.method protected l()V
    .locals 1

    invoke-super {p0}, Lbf/m;->l()V

    invoke-static {}, Lcom/google/googlenav/friend/p;->g()Lcom/google/googlenav/friend/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/o;->b()V

    return-void
.end method

.method public m()V
    .locals 2

    invoke-virtual {p0}, Lbf/X;->af()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/X;->B:Lcom/google/googlenav/ui/friend/q;

    iget-object v1, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/friend/q;->a(B)V

    :cond_0
    invoke-super {p0}, Lbf/m;->m()V

    return-void
.end method

.method protected n()V
    .locals 2

    invoke-virtual {p0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/X;->f:Lcom/google/googlenav/F;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(I)V

    :cond_0
    :goto_0
    invoke-super {p0}, Lbf/m;->n()V

    return-void

    :cond_1
    iget-boolean v0, p0, Lbf/X;->M:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/X;->l(Z)V

    goto :goto_0
.end method

.method protected z()V
    .locals 1

    invoke-direct {p0}, Lbf/X;->bX()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lbf/m;->z()V

    invoke-direct {p0}, Lbf/X;->bW()V

    goto :goto_0
.end method
