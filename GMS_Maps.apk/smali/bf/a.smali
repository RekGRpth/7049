.class public Lbf/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Las/c;

.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Las/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbf/a;->b:Ljava/util/Map;

    iput-object p1, p0, Lbf/a;->a:Las/c;

    return-void
.end method

.method static synthetic a(Lbf/a;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lbf/a;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lbf/a;Lcom/google/googlenav/F;Lbf/f;Lcom/google/googlenav/android/aa;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lbf/a;->a(Lcom/google/googlenav/F;Lbf/f;Lcom/google/googlenav/android/aa;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/F;Lbf/f;Lcom/google/googlenav/android/aa;)V
    .locals 2

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    new-instance v1, Lbf/e;

    invoke-direct {v1, p0, p2, p1}, Lbf/e;-><init>(Lbf/a;Lbf/f;Lcom/google/googlenav/F;)V

    invoke-virtual {p3, v1, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2, p1}, Lbf/f;->a(Lcom/google/googlenav/F;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/common/io/j;)V
    .locals 1

    const-string v0, "SAVED_BGFS_"

    invoke-interface {p0, v0}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lbf/a;->b(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BACKGROUND_FEATURE_SET_MANAGER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p0}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1, v1, v1}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    return-void
.end method

.method public a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V
    .locals 7

    new-instance v0, Lbf/b;

    iget-object v2, p0, Lbf/a;->a:Las/c;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lbf/b;-><init>(Lbf/a;Las/c;ILcom/google/googlenav/F;Lbf/g;Lcom/google/googlenav/android/aa;)V

    invoke-virtual {v0}, Lbf/b;->g()V

    return-void
.end method

.method public a(ILcom/google/googlenav/android/aa;Lbf/f;)V
    .locals 6

    iget-object v0, p0, Lbf/a;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/F;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p3, p2}, Lbf/a;->a(Lcom/google/googlenav/F;Lbf/f;Lcom/google/googlenav/android/aa;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lbf/d;

    iget-object v2, p0, Lbf/a;->a:Las/c;

    move-object v1, p0

    move v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lbf/d;-><init>(Lbf/a;Las/c;ILbf/f;Lcom/google/googlenav/android/aa;)V

    invoke-virtual {v0}, Lbf/d;->g()V

    goto :goto_0
.end method
