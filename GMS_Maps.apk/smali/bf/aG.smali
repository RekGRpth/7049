.class public Lbf/aG;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Las/d;

.field private b:J

.field private final c:Lbf/aH;


# direct methods
.method public constructor <init>(Las/c;Lbf/aH;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lbf/aG;->c:Lbf/aH;

    new-instance v0, Las/d;

    invoke-direct {v0, p1, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lbf/aG;->a:Las/d;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x7530

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbf/aG;->b:J

    iget-object v0, p0, Lbf/aG;->a:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    iget-object v0, p0, Lbf/aG;->a:Las/d;

    iget-wide v1, p0, Lbf/aG;->b:J

    invoke-virtual {v0, v1, v2}, Las/d;->b(J)V

    iget-object v0, p0, Lbf/aG;->a:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    return-void
.end method

.method public b()Z
    .locals 4

    iget-wide v0, p0, Lbf/aG;->b:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 1

    iget-object v0, p0, Lbf/aG;->c:Lbf/aH;

    invoke-interface {v0}, Lbf/aH;->a()V

    return-void
.end method
