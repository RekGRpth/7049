.class public Lbf/aI;
.super Lbf/aZ;
.source "SourceFile"


# instance fields
.field protected final d:LaB/s;


# direct methods
.method public constructor <init>(Lbf/i;LaB/s;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/aZ;-><init>(Lbf/i;)V

    iput-object p2, p0, Lbf/aI;->d:LaB/s;

    return-void
.end method

.method private f(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;)Lbj/F;
    .locals 3

    invoke-direct {p0, p1}, Lbf/aI;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/ay;

    invoke-virtual {p0}, Lbf/aI;->c()Lbf/m;

    move-result-object v1

    iget-object v2, p0, Lbf/aI;->d:LaB/s;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/googlenav/ui/ay;-><init>(Lcom/google/googlenav/ai;Lbf/m;LaB/s;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lbf/aZ;->a(Lcom/google/googlenav/ai;)Lbj/F;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/ai;Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lbf/aI;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, v1, v1}, Lbf/aI;->a(Landroid/view/View;Lam/f;Lam/f;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Landroid/view/View;)V

    goto :goto_0
.end method

.method protected b(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lbf/aI;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lbf/aZ;->b(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
