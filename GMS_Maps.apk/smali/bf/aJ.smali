.class public Lbf/aJ;
.super Lbf/bk;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private E:Lcom/google/googlenav/ui/view/dialog/bl;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .locals 8

    const/4 v7, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;I)V

    iput-object p7, p0, Lbf/aJ;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .locals 10

    const/4 v9, 0x6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    move-object/from16 v0, p8

    iput-object v0, p0, Lbf/aJ;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    return-void
.end method

.method static synthetic a(Lbf/aJ;)V
    .locals 0

    invoke-direct {p0}, Lbf/aJ;->bW()V

    return-void
.end method

.method private bW()V
    .locals 4

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lbf/aJ;->y:LaB/s;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lbf/aL;

    invoke-direct {v3, p0}, Lbf/aL;-><init>(Lbf/aJ;)V

    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    invoke-virtual {p0}, Lbf/aJ;->h()V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lbf/bk;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbf/aJ;->B:Z

    iput p2, p0, Lbf/aJ;->D:I

    const-string v1, "o"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lbf/aJ;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lbf/aJ;->j(I)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lbf/aJ;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    iget-object v2, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2bc -> :sswitch_0
        0x76f -> :sswitch_1
    .end sparse-switch
.end method

.method public aC()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aF()I
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/aJ;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    const v0, 0x7f110010

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f110011

    goto :goto_0

    :cond_1
    invoke-super {p0}, Lbf/bk;->aF()I

    move-result v0

    goto :goto_0
.end method

.method protected ap()V
    .locals 3

    invoke-virtual {p0}, Lbf/aJ;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-super {p0}, Lbf/bk;->ap()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/aJ;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    invoke-virtual {p0}, Lbf/aJ;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    invoke-virtual {p0}, Lbf/aJ;->bL()V

    iput-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_0
.end method

.method protected aq()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/aJ;->d(Z)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bo;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bo;-><init>(Lbf/aJ;)V

    iput-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lbf/aJ;->bG()V

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x1a

    return v0
.end method

.method protected b(ILjava/lang/Object;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lbf/bk;->b(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/common/offerslib/y;->b:Lcom/google/android/apps/common/offerslib/y;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/y;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/offers/i;->a(Lcom/google/googlenav/aZ;Ljava/lang/String;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    invoke-virtual {p0}, Lbf/aJ;->h()V

    const/4 v0, 0x1

    return v0
.end method

.method protected bG()V
    .locals 2

    iget-object v0, p0, Lbf/aJ;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lbf/aK;

    invoke-direct {v1, p0, v0}, Lbf/aK;-><init>(Lbf/aJ;Las/c;)V

    invoke-virtual {v1}, Lbf/aK;->g()V

    :cond_0
    return-void
.end method

.method protected bH()Lcom/google/googlenav/aZ;
    .locals 3

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->j(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->aq()I

    move-result v2

    invoke-virtual {v1, v0, v2, p0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public bp()V
    .locals 3

    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/aJ;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/aJ;->bA()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/bk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-virtual {p0}, Lbf/aJ;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    goto :goto_0

    :cond_2
    invoke-super {p0}, Lbf/bk;->bp()V

    goto :goto_0
.end method

.method protected c(ILjava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lbf/bk;->c(ILjava/lang/Object;)V

    iget-object v0, p0, Lbf/aJ;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->e()V

    invoke-static {}, Lcom/google/googlenav/offers/k;->h()Lcom/google/googlenav/offers/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/offers/k;->e()V

    return-void
.end method

.method protected c(Lcom/google/googlenav/aZ;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method protected f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected k(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
