.class public Lbf/aM;
.super Lbf/i;
.source "SourceFile"

# interfaces
.implements LaT/m;


# instance fields
.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Ljava/lang/String;)V
    .locals 6

    new-instance v5, Lbf/aP;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lbf/aP;-><init>(Lbf/aN;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/i;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    iput-object p5, p0, Lbf/aM;->u:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lbf/aM;Lcom/google/googlenav/F;LaT/f;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbf/aM;->a(Lcom/google/googlenav/F;LaT/f;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/F;LaT/f;)V
    .locals 2

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lbf/aP;

    new-instance v1, Lbf/aO;

    invoke-virtual {p2}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/prefetch/android/k;

    invoke-direct {v1, v0}, Lbf/aO;-><init>(Lcom/google/googlenav/prefetch/android/k;)V

    invoke-virtual {p1, v1}, Lbf/aP;->a(Lbf/aO;)V

    goto :goto_0
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .locals 1

    invoke-super {p0}, Lbf/i;->X()Z

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    iput-object p1, p0, Lbf/aM;->f:Lcom/google/googlenav/F;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/aM;->b(I)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aC()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aF()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f110012

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lbf/i;->aF()I

    move-result v0

    goto :goto_0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0x33e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x339

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aL()Lam/f;
    .locals 2

    iget-object v0, p0, Lbf/aM;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->aw:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public aT()Z
    .locals 1

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p0}, LaT/a;->a(LaT/m;)V

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 1

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p0}, LaT/a;->b(LaT/m;)V

    return-void
.end method

.method public aW()V
    .locals 5

    invoke-super {p0}, Lbf/i;->aW()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Z)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const v1, 0x7f020222

    const/16 v2, 0x349

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->d:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public aX()V
    .locals 2

    invoke-super {p0}, Lbf/i;->aX()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->j()V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected ap()V
    .locals 0

    return-void
.end method

.method protected aq()V
    .locals 0

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x16

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 3

    iget-object v0, p0, Lbf/aM;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    new-array v2, v0, [Lcom/google/googlenav/ui/aI;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/aM;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0}, Lbf/aM;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lbf/aO;

    invoke-virtual {v0}, Lbf/aO;->h()Lcom/google/googlenav/ui/aI;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method protected f(Lat/a;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lbf/aM;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->aC()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lbf/aM;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->a()V

    iget-object v1, p0, Lbf/aM;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "offline"

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lbf/aM;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/g;

    invoke-direct {v0, p0}, Lbh/g;-><init>(Lbf/i;)V

    return-object v0
.end method

.method public onOfflineDataUpdate(LaT/l;)V
    .locals 3

    invoke-virtual {p1}, LaT/l;->n()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/aM;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lbf/aN;

    invoke-direct {v1, p0, p1}, Lbf/aN;-><init>(Lbf/aM;LaT/l;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0
.end method
