.class Lbf/aO;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/E;


# instance fields
.field private final a:LaN/B;

.field private final b:Lcom/google/googlenav/ui/aI;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/prefetch/android/k;)V
    .locals 7

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/prefetch/android/k;->k()LaN/B;

    move-result-object v0

    iput-object v0, p0, Lbf/aO;->a:LaN/B;

    const/4 v0, 0x5

    new-array v1, v0, [LaN/B;

    invoke-virtual {p1}, Lcom/google/googlenav/prefetch/android/k;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/prefetch/android/k;->b()LaN/B;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v0, v1, v4

    new-instance v4, LaN/B;

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v5

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v6

    invoke-direct {v4, v5, v6}, LaN/B;-><init>(II)V

    aput-object v4, v1, v3

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const/4 v4, 0x3

    new-instance v5, LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v6

    invoke-direct {v5, v2, v6}, LaN/B;-><init>(II)V

    aput-object v5, v1, v4

    const/4 v2, 0x4

    aput-object v0, v1, v2

    const/4 v4, -0x1

    const v2, -0xd5ba98

    new-instance v0, LaN/N;

    const/4 v5, 0x0

    check-cast v5, [[LaN/B;

    invoke-direct/range {v0 .. v5}, LaN/N;-><init>([LaN/B;III[[LaN/B;)V

    iput-object v0, p0, Lbf/aO;->b:Lcom/google/googlenav/ui/aI;

    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lbf/aO;->a:LaN/B;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public c()B
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "OfflineLayer"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public h()Lcom/google/googlenav/ui/aI;
    .locals 1

    iget-object v0, p0, Lbf/aO;->b:Lcom/google/googlenav/ui/aI;

    return-object v0
.end method
