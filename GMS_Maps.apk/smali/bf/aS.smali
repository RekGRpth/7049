.class public Lbf/aS;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbf/aS;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static a(Lcom/google/googlenav/ai;Ljava/util/Hashtable;Ljava/lang/Object;)Lbf/aQ;
    .locals 3

    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    if-nez v0, :cond_0

    new-instance v0, Lbf/aQ;

    invoke-static {p0}, Lbf/aS;->c(Lcom/google/googlenav/ai;)[I

    move-result-object v1

    invoke-static {p0}, Lbf/aS;->h(Lcom/google/googlenav/ai;)[I

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbf/aQ;-><init>([I[I)V

    invoke-virtual {p1, p2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public static a(Lbf/aT;I)Lcom/google/googlenav/ui/view/android/a;
    .locals 9

    const/4 v5, 0x0

    iget-object v0, p0, Lbf/aT;->a:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v5

    :cond_0
    iget-boolean v0, p0, Lbf/aT;->d:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    iget v1, p0, Lbf/aT;->c:I

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->f(I)C

    move-result v1

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    move-object v4, v0

    :goto_1
    new-instance v6, Lcom/google/googlenav/ui/view/a;

    iget v0, p0, Lbf/aT;->c:I

    const/4 v1, -0x1

    invoke-direct {v6, v0, v1, v5}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    iget-object v2, p0, Lbf/aT;->a:Ljava/lang/String;

    iget-object v3, p0, Lbf/aT;->b:Ljava/lang/String;

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    move v1, p1

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    move-object v5, v0

    goto :goto_0

    :cond_1
    move-object v4, v5

    goto :goto_1
.end method

.method public static a(Lai/d;)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/16 v1, 0x3ef

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->bQ:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    const-string v1, " "

    sget-object v2, Lcom/google/googlenav/ui/aV;->bQ:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {p0}, Lai/d;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->bP:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    const-string v1, " "

    sget-object v2, Lcom/google/googlenav/ui/aV;->bP:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {p0}, Lai/d;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->bR:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;
    .locals 5

    const/4 v2, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/google/googlenav/ac;->a(J)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    move-object v0, v1

    :goto_2
    move-object v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Ljava/lang/CharSequence;
    .locals 2

    invoke-static {p0}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.google.com/m/place?hl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lbf/aS;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&ppmode=image&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->S()Ljava/util/Vector;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    if-eqz v1, :cond_2

    const-string v0, " \u00b7 "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_4

    const-string v0, "\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, "+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ai;I)Ljava/util/List;
    .locals 5

    const/4 v4, 0x2

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    const/16 v0, 0x1a

    sget-object v2, LbK/bb;->a:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-le v0, p1, :cond_1

    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p1, :cond_2

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aO;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/aO;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    move p1, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ai;Lbf/i;)Ljava/util/List;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0}, Lbf/i;->b(LaN/g;)Z

    move-result v2

    invoke-virtual {p1, v0}, Lbf/i;->c(LaN/g;)Z

    move-result v3

    if-eqz v2, :cond_0

    invoke-virtual {p1, v0}, Lbf/i;->c(LaN/g;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x2f7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/16 v2, 0x25c

    invoke-static {v0, v2, v4, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    if-eqz v3, :cond_1

    const/16 v0, 0x2f9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x25d

    invoke-static {v0, v2, v4, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_1
    const/16 v0, 0x1ba

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x25b

    invoke-static {v0, v2, v4, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->o(I)V

    return-object v1

    :cond_2
    const/16 v0, 0x2f6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ai;ZZ)Ljava/util/Vector;
    .locals 3

    const/16 v0, 0xf

    sget-object v1, LbK/az;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/googlenav/ad;

    invoke-direct {v2, v0}, Lcom/google/googlenav/ad;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    if-eqz p1, :cond_1

    invoke-virtual {v2, v1, p2}, Lcom/google/googlenav/ad;->a(Ljava/util/Vector;Z)V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v2, v1}, Lcom/google/googlenav/ad;->a(Ljava/util/Vector;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/e;Ljava/util/List;)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aI()Ljava/lang/String;

    move-result-object v6

    if-nez v2, :cond_2

    if-nez v3, :cond_2

    if-nez v4, :cond_2

    if-eqz v5, :cond_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/ui/view/android/bj;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bj;-><init>(Lcom/google/googlenav/ui/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ai;Ljava/util/Vector;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bE()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x2a7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2c3

    invoke-static {p1, v0, v1, p2}, Lbf/aS;->a(Ljava/util/Vector;Ljava/lang/String;IZ)V

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ai;ZILjava/util/List;)V
    .locals 9

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->z()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/16 v0, 0x4b1

    :goto_1
    if-eqz p1, :cond_3

    const/16 v1, 0x255

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/4 v1, -0x1

    invoke-direct {v6, v0, v1, v4}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    invoke-static {p0, p1}, Lbf/aS;->c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    move v1, p2

    move-object v5, v4

    move-object v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x4b0

    goto :goto_1

    :cond_3
    const/16 v1, 0x257

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;IILjava/util/List;)V
    .locals 9

    const/4 v3, 0x0

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/4 v0, -0x1

    invoke-direct {v6, p1, v0, v3}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    move v1, p2

    move-object v2, p0

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static a(Ljava/util/Vector;Ljava/lang/String;IZ)V
    .locals 1

    new-instance v0, Lbf/aT;

    invoke-direct {v0, p1, p2, p3}, Lbf/aT;-><init>(Ljava/lang/String;IZ)V

    invoke-virtual {p0, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/al;Ljava/util/Vector;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Z
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/al;->b()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/am;

    invoke-virtual {v0}, Lcom/google/googlenav/am;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/googlenav/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " - "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/al;->a()Lcom/google/googlenav/am;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/am;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/googlenav/am;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-static {v4, p3, v3, v2, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/a;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move v0, v3

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/google/googlenav/ui/view/a;

    const/4 v5, 0x5

    const/4 v6, -0x1

    invoke-virtual {v0}, Lcom/google/googlenav/am;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v5, v6, v0}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public static b(Lcom/google/googlenav/ai;ZZ)Lcom/google/googlenav/ui/view/a;
    .locals 7

    const/4 v1, 0x0

    const/4 v6, -0x1

    if-eqz p2, :cond_3

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/16 v2, 0x258

    invoke-direct {v0, v2, v6, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->P()[Lcom/google/googlenav/aw;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->P()[Lcom/google/googlenav/aw;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->O()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/16 v2, 0x907

    invoke-direct {v0, v2, v6, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/16 v1, 0x908

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->P()[Lcom/google/googlenav/aw;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/4 v1, 0x5

    invoke-static {p0}, Lbf/aS;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static b(Lcom/google/googlenav/ai;Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/16 v0, 0x1b

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->o(I)V

    return-void

    :cond_0
    const/16 v0, 0x1a

    goto :goto_0
.end method

.method public static b(Lcom/google/googlenav/ai;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->Z()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->Y()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_0

    const/16 v0, 0x254

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->z()I

    move-result v2

    if-le v2, v4, :cond_1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->z()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/16 v0, 0x256

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static c(Lcom/google/googlenav/ai;)[I
    .locals 4

    const/4 v3, 0x2

    const/16 v0, 0xa

    sget-object v1, LbK/e;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lbf/aS;->a:[I

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v3, [I

    const/4 v3, 0x0

    aput v1, v0, v3

    const/4 v1, 0x1

    aput v2, v0, v1

    goto :goto_0
.end method

.method public static d(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x1

    const/16 v0, 0x1a

    sget-object v1, LbK/bb;->a:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Lcom/google/googlenav/ai;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bT()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-static {v0, v1}, LaV/g;->a(LaH/m;LaN/B;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/16 v1, 0xfa

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->e(Z)V

    :cond_0
    return-void
.end method

.method public static f(Lcom/google/googlenav/ai;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/aM;->a()Lcom/google/googlenav/aM;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/aM;->a(Lcom/google/googlenav/ai;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->f(Z)V

    :cond_0
    return-void
.end method

.method public static g(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 4

    const/16 v0, 0x537

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bO()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static h(Lcom/google/googlenav/ai;)[I
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    const/16 v1, 0xe

    sget-object v2, LbK/aa;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    aput v3, v0, v5

    rsub-int/lit8 v2, v2, 0x3

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    aput v2, v0, v4

    :cond_2
    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    aput v2, v0, v7

    rsub-int/lit8 v1, v1, 0x3

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    aput v1, v0, v6

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method
