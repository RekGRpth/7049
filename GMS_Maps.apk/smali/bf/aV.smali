.class public Lbf/aV;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lbf/aU;


# direct methods
.method public constructor <init>(Lbf/aU;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbf/aV;->a:Lbf/aU;

    return-void
.end method

.method static synthetic a(Lbf/aV;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lbf/aV;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ui/view/android/J;Lcom/google/googlenav/aZ;)V
    .locals 5

    const/4 v4, -0x1

    const/16 v0, 0x542

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v4, v4}, Lbf/aV;->a(Ljava/lang/CharSequence;II)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/view/android/J;Lcom/google/googlenav/ai;ILjava/lang/String;)Z
    .locals 7

    const v4, 0x7f0f0074

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ac()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_1
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1, p3}, Lbf/aV;->a(Ljava/lang/CharSequence;II)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-static {p4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    const/16 v5, 0x437

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    aput-object p4, v6, v0

    aput-object v3, v6, v2

    invoke-static {v5, v6}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->aw:Lcom/google/googlenav/ui/aV;

    sget-object v5, Lcom/google/googlenav/ui/aV;->ax:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v0, v3, v5}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lbf/aV;)Lbf/aU;
    .locals 1

    iget-object v0, p0, Lbf/aV;->a:Lbf/aU;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbf/aV;->a:Lbf/aU;

    invoke-virtual {v0}, Lbf/aU;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/aV;->a:Lbf/aU;

    invoke-virtual {v0}, Lbf/aU;->bh()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xef

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0xee

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .locals 9

    const/16 v8, 0x65

    const/4 v7, -0x1

    const/4 v0, 0x0

    const/4 v3, 0x1

    new-instance v4, Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {v4, p1, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lbf/aV;->a:Lbf/aU;

    invoke-virtual {v1}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v5

    iget-object v1, p0, Lbf/aV;->a:Lbf/aU;

    invoke-virtual {v1}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->v()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lbf/aV;->a:Lbf/aU;

    invoke-virtual {v1}, Lbf/aU;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->I()[Lcom/google/googlenav/ai;

    move-result-object v1

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_4

    aget-object v2, v1, v0

    invoke-direct {p0, v4, v2, v0, v6}, Lbf/aV;->a(Lcom/google/googlenav/ui/view/android/J;Lcom/google/googlenav/ai;ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    move v2, v0

    :goto_1
    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v5, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-direct {p0, v4, v0, v1, v6}, Lbf/aV;->a(Lcom/google/googlenav/ui/view/android/J;Lcom/google/googlenav/ai;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v2, v3

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    if-nez v2, :cond_4

    invoke-direct {p0, v4, v5}, Lbf/aV;->a(Lcom/google/googlenav/ui/view/android/J;Lcom/google/googlenav/aZ;)V

    :cond_4
    :goto_2
    return-object v4

    :cond_5
    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    if-nez v1, :cond_6

    invoke-direct {p0, v4, v5}, Lbf/aV;->a(Lcom/google/googlenav/ui/view/android/J;Lcom/google/googlenav/aZ;)V

    :cond_6
    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v7}, Lbf/aV;->a(Ljava/lang/CharSequence;II)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    if-ne v1, v3, :cond_7

    const/16 v1, 0x543

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v8, v7}, Lbf/aV;->a(Ljava/lang/CharSequence;II)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    if-le v1, v3, :cond_4

    const/16 v1, 0x541

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v0

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v8, v7}, Lbf/aV;->a(Ljava/lang/CharSequence;II)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method protected a(Ljava/lang/CharSequence;II)Lcom/google/googlenav/ui/view/android/a;
    .locals 9

    const/4 v3, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/4 v1, 0x0

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    invoke-direct {v6, p2, p3, v3}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->aw:Lcom/google/googlenav/ui/aV;

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    return-object v0
.end method

.method public a()Lcom/google/googlenav/ui/view/android/aL;
    .locals 2

    new-instance v0, Lbf/aY;

    iget-object v1, p0, Lbf/aV;->a:Lbf/aU;

    invoke-direct {v0, p0, v1}, Lbf/aY;-><init>(Lbf/aV;Lbf/aU;)V

    return-object v0
.end method
