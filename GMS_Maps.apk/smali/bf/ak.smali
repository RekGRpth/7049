.class public Lbf/ak;
.super Lbf/m;
.source "SourceFile"


# instance fields
.field private final B:Lcom/google/googlenav/layer/s;

.field private final C:Z

.field private final D:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/ai;ZBZ)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    invoke-virtual {p6}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p6, Lcom/google/googlenav/W;

    new-instance v0, Lcom/google/googlenav/layer/m;

    invoke-virtual {p6}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, Lbf/ak;->B:Lcom/google/googlenav/layer/s;

    new-instance v1, Lcom/google/googlenav/T;

    invoke-direct {v1, v0, p5, p2, p3}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-virtual {v1, p6}, Lcom/google/googlenav/T;->a(Lcom/google/googlenav/W;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/T;->a(I)V

    iput-object v1, p0, Lbf/ak;->f:Lcom/google/googlenav/F;

    :goto_0
    iput-boolean p7, p0, Lbf/ak;->C:Z

    iput-boolean p9, p0, Lbf/ak;->D:Z

    invoke-virtual {p0, p8}, Lbf/ak;->b(B)V

    invoke-virtual {p0}, Lbf/ak;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/ak;->e(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/ak;->B:Lcom/google/googlenav/layer/s;

    new-instance v0, Lcom/google/googlenav/bl;

    invoke-direct {v0, p6}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v0, p0, Lbf/ak;->f:Lcom/google/googlenav/F;

    goto :goto_0
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .locals 1

    invoke-super {p0}, Lbf/m;->Z()V

    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 0

    iput-object p1, p0, Lbf/ak;->f:Lcom/google/googlenav/F;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lbf/ak;->C:Z

    return v0
.end method

.method public aC()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aE()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aW()V
    .locals 3

    const/4 v1, 0x4

    iget-object v0, p0, Lbf/ak;->f:Lcom/google/googlenav/F;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v1, v0}, Lbf/ak;->a(ILcom/google/googlenav/ai;)V

    invoke-super {p0}, Lbf/m;->aW()V

    iget-boolean v0, p0, Lbf/ak;->C:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/ak;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/ak;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/ak;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/ak;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/ak;->d:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->b(LaN/B;)V

    :cond_0
    return-void
.end method

.method protected am()V
    .locals 4

    invoke-virtual {p0}, Lbf/ak;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/google/googlenav/W;

    const/16 v2, 0x43

    const-string v3, "o"

    invoke-virtual {v1}, Lcom/google/googlenav/W;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbf/ak;->B:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Law/g;

    :cond_0
    return-void
.end method

.method public ao()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/ak;->bu()Lcom/google/googlenav/ai;

    move-result-object v3

    if-nez v3, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aw()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/settings/e;->c:Lcom/google/googlenav/settings/e;

    if-ne v2, v4, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/aA;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/settings/e;->d:Lcom/google/googlenav/settings/e;

    if-ne v2, v4, :cond_4

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected aq()V
    .locals 0

    return-void
.end method

.method public au()Z
    .locals 1

    iget-boolean v0, p0, Lbf/ak;->C:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lbf/m;->au()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public av()I
    .locals 1

    const/16 v0, 0xf

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lcom/google/googlenav/T;
    .locals 1

    iget-object v0, p0, Lbf/ak;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/T;

    return-object v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 2

    iget-boolean v0, p0, Lbf/ak;->C:Z

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bq()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->f()I

    move-result v0

    :goto_0
    rsub-int/lit8 v0, v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lbf/ak;->f(Lcom/google/googlenav/E;)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method protected f(Lat/a;)Z
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbf/ak;->h()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    invoke-virtual {p0}, Lbf/ak;->bz()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/ak;->b(Lcom/google/googlenav/ui/wizard/A;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ag()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lbf/ak;->D:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    iget-object v0, p0, Lbf/ak;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/ak;->ag()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/ak;->a(B)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lbf/ak;->n()V

    goto :goto_0
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .locals 1

    iget-object v0, p0, Lbf/ak;->B:Lcom/google/googlenav/layer/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/ak;->B:Lcom/google/googlenav/layer/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/g;

    invoke-direct {v0, p0}, Lbh/g;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected l()V
    .locals 0

    return-void
.end method
