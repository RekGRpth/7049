.class public Lbf/al;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lbf/al;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lbf/al;
    .locals 1

    sget-object v0, Lbf/al;->a:Lbf/al;

    return-object v0
.end method

.method public static a(Lbf/al;)V
    .locals 0

    sput-object p0, Lbf/al;->a:Lbf/al;

    return-void
.end method


# virtual methods
.method public a(Lbf/i;LaB/s;)Lbf/w;
    .locals 3

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Layer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_2
    new-instance v0, Lbf/aZ;

    invoke-direct {v0, p1}, Lbf/aZ;-><init>(Lbf/i;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lbf/aI;

    invoke-direct {v0, p1, p2}, Lbf/aI;-><init>(Lbf/i;LaB/s;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lbf/bF;

    check-cast p1, Lbf/bH;

    invoke-direct {v0, p1}, Lbf/bF;-><init>(Lbf/bH;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lbf/aj;

    invoke-direct {v0, p1}, Lbf/aj;-><init>(Lbf/i;)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lbf/A;

    invoke-direct {v0, p1}, Lbf/A;-><init>(Lbf/i;)V

    goto :goto_0

    :pswitch_7
    new-instance v0, Lbf/R;

    invoke-direct {v0, p1}, Lbf/R;-><init>(Lbf/i;)V

    goto :goto_0

    :pswitch_8
    new-instance v0, Lbf/E;

    check-cast p1, Lbf/O;

    invoke-direct {v0, p1}, Lbf/E;-><init>(Lbf/O;)V

    goto :goto_0

    :pswitch_9
    new-instance v0, Lbf/bJ;

    check-cast p1, Lbf/bK;

    invoke-direct {v0, p1}, Lbf/bJ;-><init>(Lbf/bK;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_9
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
