.class public Lbf/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/layer/l;


# static fields
.field private static final I:I

.field private static final J:I

.field private static final K:I

.field private static final Q:I

.field public static a:I

.field public static b:I

.field protected static final j:I

.field private static final t:[I

.field private static final u:[Z

.field private static v:Z

.field private static volatile w:I


# instance fields
.field private A:Z

.field private B:I

.field private C:Ljava/lang/Object;

.field private D:Ljava/lang/String;

.field private final E:Landroid/graphics/Point;

.field private final F:Lcom/google/googlenav/layer/r;

.field private final G:Lbf/a;

.field private final H:Ljava/util/Map;

.field private final L:Ljava/util/Vector;

.field private M:Z

.field private N:LaN/B;

.field private O:I

.field private P:Lcom/google/googlenav/offers/j;

.field protected final c:Lcom/google/googlenav/ui/s;

.field protected final d:LaH/m;

.field protected final e:LaN/p;

.field protected final f:LaN/u;

.field protected final g:Lcom/google/googlenav/ui/X;

.field protected final h:LaN/k;

.field protected final i:Ljava/util/Vector;

.field private final k:Lcom/google/googlenav/friend/J;

.field private final l:Lcom/google/googlenav/friend/p;

.field private final m:Lcom/google/googlenav/friend/ag;

.field private final n:Lcom/google/googlenav/android/aa;

.field private final o:Lcom/google/googlenav/ui/wizard/jv;

.field private final p:Ljava/util/Vector;

.field private final q:Ljava/util/Vector;

.field private final r:Ljava/util/Vector;

.field private s:Z

.field private x:B

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x5

    sput v2, Lbf/am;->a:I

    const/16 v0, 0x14

    sput v0, Lbf/am;->b:I

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbf/am;->t:[I

    sget-object v0, Lbf/am;->t:[I

    array-length v0, v0

    new-array v0, v0, [Z

    sput-object v0, Lbf/am;->u:[Z

    sput-boolean v1, Lbf/am;->v:Z

    sput v1, Lbf/am;->w:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/am;->I:I

    sget v0, Lbf/am;->I:I

    sget v1, Lbf/am;->I:I

    mul-int/2addr v0, v1

    sput v0, Lbf/am;->J:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/am;->K:I

    sget v0, Lbf/am;->K:I

    sget v1, Lbf/am;->K:I

    mul-int/2addr v0, v1

    sput v0, Lbf/am;->j:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/am;->Q:I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x0
    .end array-data
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->r:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->s:Z

    const/4 v0, -0x1

    iput-byte v0, p0, Lbf/am;->x:B

    const/16 v0, -0xa

    iput v0, p0, Lbf/am;->y:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->z:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->A:Z

    const/4 v0, -0x1

    iput v0, p0, Lbf/am;->B:I

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lbf/am;->E:Landroid/graphics/Point;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->M:Z

    const/4 v0, -0x1

    iput v0, p0, Lbf/am;->O:I

    iput-object p1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iput-object p2, p0, Lbf/am;->e:LaN/p;

    iput-object p3, p0, Lbf/am;->f:LaN/u;

    iput-object p4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iput-object p5, p0, Lbf/am;->d:LaH/m;

    iput-object p6, p0, Lbf/am;->k:Lcom/google/googlenav/friend/J;

    iput-object p7, p0, Lbf/am;->l:Lcom/google/googlenav/friend/p;

    iput-object p8, p0, Lbf/am;->m:Lcom/google/googlenav/friend/ag;

    iput-object p9, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    iput-object p10, p0, Lbf/am;->o:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p11, p0, Lbf/am;->h:LaN/k;

    iput-object p12, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    iput-object p13, p0, Lbf/am;->P:Lcom/google/googlenav/offers/j;

    new-instance v0, Lbf/a;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lbf/a;-><init>(Las/c;)V

    iput-object v0, p0, Lbf/am;->G:Lbf/a;

    return-void
.end method

.method public static W()I
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    sget-object v2, Lbf/am;->u:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lbf/am;->u:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_1

    sget-object v1, Lbf/am;->t:[I

    aget v1, v1, v0

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static X()I
    .locals 2

    sget v0, Lbf/am;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbf/am;->w:I

    return v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p2}, Lbf/i;->av()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {p2}, Lbf/i;->aO()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p2}, Lbf/i;->ay()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public static a(IZ)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lbf/am;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lbf/am;->t:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_1

    sget-object v1, Lbf/am;->u:[Z

    aput-boolean p1, v1, v0

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lbf/am;)V
    .locals 0

    invoke-direct {p0}, Lbf/am;->ag()V

    return-void
.end method

.method static synthetic a(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->a(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private a(Lbf/i;)V
    .locals 3

    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbf/i;->aC()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    sget v1, Lbf/am;->b:I

    if-le v0, v1, :cond_3

    sget v0, Lbf/am;->b:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-direct {p0, v0}, Lbf/am;->c(Lbf/i;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    sget v1, Lbf/am;->b:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->A:Z

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lbf/i;Ljava/lang/String;)V
    .locals 2

    const/16 v1, 0x43

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {p1}, Lbf/am;->p(Lbf/i;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lbf/am;->aj()V

    return-void

    :pswitch_0
    check-cast p1, Lbf/y;

    invoke-virtual {p1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-static {p1}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lbf/am;->aj()V

    return-void
.end method

.method private a(Ljava/io/DataInput;IZZ)V
    .locals 8

    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v5, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    invoke-static {}, Lbf/am;->W()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;I)V

    invoke-virtual {v0, p1}, Lbf/bk;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p2}, Lbf/bk;->e(I)V

    invoke-virtual {v0, p3}, Lbf/bk;->h(Z)V

    if-eqz p4, :cond_0

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->f()Z

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lbf/am;->a(Lbf/i;ZZ)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-interface {v0, p2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "LAYER_MANAGER-LayerManager Error saving layers"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lbf/am;->ag()V

    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "LAYER_MANAGER-LayerManager OOME saving layers"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lbf/am;->ag()V

    goto :goto_2
.end method

.method private ab()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xd

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    const/16 v0, 0x12

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    const/16 v0, 0xf

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method private ac()Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    return-object v0
.end method

.method private ad()V
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    invoke-virtual {v0, v2}, Lbf/ai;->a(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private ae()V
    .locals 7

    const/4 v6, 0x1

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lbf/am;->i:Ljava/util/Vector;

    monitor-enter v4

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v5, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    iget-object v5, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lbf/i;->aM()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-direct {p0, v3, v0, v6}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v1, v0}, Lcom/google/googlenav/layer/r;->a(Lbf/i;)V

    :cond_1
    :goto_2
    move-object v1, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    const/4 v2, -0x1

    const/16 v0, -0xa

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v2

    const/16 v0, 0xa

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/16 v4, 0xb

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v4, 0xe

    invoke-virtual {v1}, Lbf/i;->aA()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    :goto_3
    iget-boolean v2, p0, Lbf/am;->z:Z

    if-eqz v2, :cond_3

    iget-byte v2, p0, Lbf/am;->x:B

    if-ne v1, v2, :cond_3

    iget v2, p0, Lbf/am;->y:I

    if-eq v0, v2, :cond_4

    :cond_3
    const-string v2, "PROTO_SAVED_LAYER_STATE"

    invoke-direct {p0, v3, v2}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lbf/am;->z:Z

    iput-byte v1, p0, Lbf/am;->x:B

    iput v0, p0, Lbf/am;->y:I

    :cond_4
    return-void

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private af()V
    .locals 6

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v3, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->aM()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v2, v0, v5}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v4, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v4, v0}, Lcom/google/googlenav/layer/r;->a(Lbf/i;)V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lbf/am;->A:Z

    if-nez v0, :cond_2

    const-string v0, "PROTO_SAVED_RECENT_LAYERS"

    invoke-direct {p0, v2, v0}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbf/am;->A:Z

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private ag()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PROTO_SAVED_LAYER_STATE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const-string v1, "PROTO_SAVED_RECENT_LAYERS"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const-string v1, "LAYER_"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    return-void
.end method

.method private ah()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "SAVED_SEARCH_1_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const-string v1, "SAVED_SEARCH_1"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const-string v1, "PROTO_SAVED_SEARCH_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const-string v1, "SAVED_SEARCH_INFO"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    invoke-static {v0}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;)V

    return-void
.end method

.method private ai()V
    .locals 4

    const/4 v3, 0x0

    move v2, v3

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    if-nez v1, :cond_0

    move-object v1, v0

    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lbf/i;->h(Z)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private aj()V
    .locals 3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-static {v0}, Lbf/am;->p(Lbf/i;)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/16 v0, 0x43

    const-string v1, "v"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private ak()V
    .locals 3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-static {v0}, Lbf/am;->p(Lbf/i;)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/16 v0, 0x43

    const-string v1, "r"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b(I)I
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown layer of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move v0, v1

    :goto_0
    :pswitch_2
    return v0

    :pswitch_3
    move v0, v1

    goto :goto_0

    :pswitch_4
    move v0, v1

    goto :goto_0

    :pswitch_5
    move v0, v1

    goto :goto_0

    :pswitch_6
    move v0, v1

    goto :goto_0

    :pswitch_7
    move v0, v1

    goto :goto_0

    :pswitch_8
    move v0, v1

    goto :goto_0

    :pswitch_9
    move v0, v1

    goto :goto_0

    :pswitch_a
    move v0, v1

    goto :goto_0

    :pswitch_b
    move v0, v1

    goto :goto_0

    :pswitch_c
    move v0, v1

    goto :goto_0

    :pswitch_d
    move v0, v1

    goto :goto_0

    :pswitch_e
    move v0, v1

    goto :goto_0

    :pswitch_f
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_3
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_f
    .end packed-switch
.end method

.method private b(IZ)V
    .locals 4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lbf/i;->ay()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, p2, v2}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lbf/am;->d()V

    return-void
.end method

.method static synthetic b(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->b(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private b(Lbf/i;)V
    .locals 2

    new-instance v0, Lbf/an;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lbf/an;-><init>(Lbf/am;Las/c;Lbf/i;)V

    invoke-virtual {v0}, Lbf/an;->g()V

    return-void
.end method

.method private b(Lbf/i;I)V
    .locals 2

    invoke-virtual {p1}, Lbf/i;->ah()Z

    move-result v0

    invoke-virtual {p1, p2}, Lbf/i;->b(I)V

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lbf/i;->n()V

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lbf/i;->al()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lbf/i;->b(B)V

    invoke-virtual {p0, p1}, Lbf/am;->e(Lbf/i;)V

    goto :goto_0
.end method

.method private b(Ljava/io/DataInput;IZZ)V
    .locals 9

    const/4 v8, 0x0

    new-instance v0, Lbf/aJ;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v5, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    iget-object v7, p0, Lbf/am;->P:Lcom/google/googlenav/offers/j;

    invoke-direct/range {v0 .. v7}, Lbf/aJ;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V

    invoke-virtual {v0, p1}, Lbf/aJ;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p2}, Lbf/aJ;->e(I)V

    invoke-virtual {v0, p3}, Lbf/aJ;->h(Z)V

    if-eqz p4, :cond_0

    invoke-virtual {v0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0, v8, v8}, Lbf/am;->a(Lbf/i;ZZ)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method static synthetic c(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->g(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private c(Lbf/i;)V
    .locals 2

    new-instance v0, Lbf/ao;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lbf/ao;-><init>(Lbf/am;Las/c;Lbf/i;)V

    invoke-virtual {v0}, Lbf/ao;->g()V

    return-void
.end method

.method private c(Lbf/i;I)V
    .locals 2

    invoke-virtual {p1, p2}, Lbf/i;->b(I)V

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbf/i;->l()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lbf/i;->m()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lbf/i;->n()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lbf/i;->al()V

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lbf/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1}, Lbf/am;->e(Lbf/i;)V

    invoke-virtual {p1}, Lbf/i;->ad()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lbf/i;->m()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lbf/i;->l()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lbf/am;->e(Lbf/i;)V

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/aZ;Z)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-nez v0, :cond_2

    sget-boolean v1, Lbf/am;->v:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lbf/am;->e(Lcom/google/googlenav/aZ;)Lbf/X;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lbf/am;->f(Lcom/google/googlenav/aZ;)V

    sget-boolean v1, Lbf/am;->v:Z

    if-eqz v1, :cond_1

    if-nez p2, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lbf/X;->a(ZLcom/google/googlenav/aZ;)V

    :cond_1
    return-void

    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lbf/am;->a(Lbf/X;)V

    goto :goto_0
.end method

.method private c(Ljava/io/DataInput;IZZ)V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lbf/O;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->d:LaH/m;

    invoke-direct/range {v0 .. v5}, Lbf/O;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;)V

    invoke-virtual {v0, p1}, Lbf/O;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p2}, Lbf/O;->e(I)V

    invoke-virtual {v0, p3}, Lbf/O;->h(Z)V

    if-eqz p4, :cond_0

    invoke-virtual {p0, v0, v6, v6}, Lbf/am;->a(Lbf/i;ZZ)Z

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/O;->b(B)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private c(LaN/B;)Z
    .locals 9

    const/4 v6, 0x0

    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->R()[Lam/f;

    move-result-object v0

    aget-object v0, v0, v6

    if-nez v0, :cond_1

    move v4, v6

    :goto_0
    const/4 v3, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    move v7, v6

    invoke-virtual/range {v0 .. v8}, Lbf/am;->a(LaN/B;LaN/B;IIIIILam/e;)Z

    move-result v6

    :cond_0
    return v6

    :cond_1
    invoke-interface {v0}, Lam/f;->a()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    goto :goto_0
.end method

.method static synthetic d(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->f(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private d(Ljava/io/DataInput;IZZ)V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lbf/bK;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v0, v1, v2, v3, v4}, Lbf/bK;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    invoke-virtual {v0, p1}, Lbf/bK;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p2}, Lbf/bK;->e(I)V

    invoke-virtual {v0, p3}, Lbf/bK;->h(Z)V

    if-eqz p4, :cond_0

    invoke-virtual {p0, v0, v5, v5}, Lbf/am;->a(Lbf/i;ZZ)Z

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/bK;->b(B)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private e(I)I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    if-ne v0, p1, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method static synthetic e(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->c(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private e(Ljava/io/DataInput;IZZ)V
    .locals 8

    const/4 v7, 0x0

    new-instance v0, Lbf/bU;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    new-instance v6, Lcom/google/googlenav/Y;

    invoke-direct {v6}, Lcom/google/googlenav/Y;-><init>()V

    invoke-direct/range {v0 .. v7}, Lbf/bU;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/F;Z)V

    invoke-virtual {v0, p1}, Lbf/bU;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p2}, Lbf/bU;->e(I)V

    invoke-virtual {v0, p3}, Lbf/bU;->h(Z)V

    if-eqz p4, :cond_0

    invoke-virtual {p0, v0, v7, v7}, Lbf/am;->a(Lbf/i;ZZ)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private f(I)Lbf/i;
    .locals 3

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Layer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has MAX_ALLOWED_INSTANCES > 1."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic f(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->d(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private f(Ljava/io/DataInput;IZZ)V
    .locals 9

    const/4 v8, 0x0

    new-instance v0, Lbf/y;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v5}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;)V

    invoke-virtual {v0, p1}, Lbf/y;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "msid:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v7, Lbf/bD;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v5

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lbf/bD;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    move-object v0, v7

    :cond_0
    :goto_0
    invoke-virtual {v0, p2}, Lbf/y;->e(I)V

    invoke-virtual {v0, p3}, Lbf/y;->h(Z)V

    if-eqz p4, :cond_2

    invoke-virtual {p0, v0, v8, v8}, Lbf/am;->a(Lbf/i;ZZ)Z

    :goto_1
    return-void

    :cond_1
    const-string v2, "LayerTransit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/m;->a(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private static g(I)C
    .locals 1

    add-int/lit8 v0, p0, 0x61

    int-to-char v0, v0

    return v0
.end method

.method private g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;
    .locals 6

    new-instance v0, Lcom/google/googlenav/n;

    new-instance v1, Lcom/google/googlenav/T;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v2

    iget-object v3, p0, Lbf/am;->h:LaN/k;

    iget-object v4, p0, Lbf/am;->e:LaN/p;

    iget-object v5, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    return-object v0
.end method

.method static synthetic g(Lbf/am;Ljava/io/DataInput;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->e(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private g(Ljava/io/DataInput;IZZ)V
    .locals 11

    const/4 v10, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lbf/X;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    iget-object v5, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->k:Lcom/google/googlenav/friend/J;

    iget-object v7, p0, Lbf/am;->l:Lcom/google/googlenav/friend/p;

    iget-object v8, p0, Lbf/am;->m:Lcom/google/googlenav/friend/ag;

    iget-object v9, p0, Lbf/am;->G:Lbf/a;

    invoke-direct/range {v0 .. v9}, Lbf/X;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lbf/a;)V

    invoke-virtual {v0, p1}, Lbf/X;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, p2}, Lbf/X;->e(I)V

    invoke-virtual {v0, p3}, Lbf/X;->h(Z)V

    if-eqz p4, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v10, v1}, Lbf/am;->a(Lbf/i;ZZ)Z

    sput-boolean v10, Lbf/am;->v:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p1}, Lbf/am;->b(Ljava/lang/String;)Lbf/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    :cond_0
    return-void
.end method

.method private g(Z)V
    .locals 3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, p1}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private h(Lcom/google/googlenav/aZ;)V
    .locals 2

    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aJ;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/aJ;->b(Lcom/google/googlenav/aZ;Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/am;->e(Z)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/aJ;->c(Lcom/google/googlenav/F;)V

    goto :goto_0
.end method

.method private h(Ljava/lang/String;)V
    .locals 13

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v9

    invoke-interface {v9, p1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lbf/am;->c(Ljava/lang/String;)Z

    move-result v3

    :try_start_0
    new-instance v10, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v11

    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v11, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v10, v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v5

    sget v1, Lbf/am;->w:I

    if-lt v5, v1, :cond_2

    add-int/lit8 v1, v5, 0x1

    sput v1, Lbf/am;->w:I

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LAYER_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v4

    if-nez v4, :cond_4

    invoke-direct {p0}, Lbf/am;->ag()V

    :cond_3
    iget-object v0, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/as;

    move-object v2, p0

    move-object v4, p1

    move-object v5, v10

    move v6, v11

    invoke-direct/range {v1 .. v6}, Lbf/as;-><init>(Lbf/am;ZLjava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "LAYER_MANAGER-LayersManager load"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lbf/am;->ag()V

    goto :goto_0

    :cond_4
    const/4 v1, 0x4

    :try_start_1
    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v6

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-direct {p0, v2}, Lbf/am;->e(I)I

    move-result v0

    invoke-static {v2}, Lbf/am;->b(I)I

    move-result v1

    if-lt v0, v1, :cond_5

    :goto_2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    :cond_5
    iget-object v12, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v0, Lbf/ar;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lbf/ar;-><init>(Lbf/am;IZLjava/io/DataInput;IZZ)V

    const/4 v1, 0x1

    invoke-virtual {v12, v0, v1}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private i(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    const/16 v0, 0x43

    const/16 v1, 0xd

    invoke-static {v1}, Lbf/am;->g(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private i(Lcom/google/googlenav/aZ;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-interface {v1, v0, p1}, Lcom/google/googlenav/ba;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/aZ;)V

    :cond_0
    return-void
.end method

.method private j(Lcom/google/googlenav/aZ;)V
    .locals 8

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    new-instance v0, Lbf/cg;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    invoke-static {}, Lbf/am;->W()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lbf/cg;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;LaN/k;I)V

    invoke-virtual {v0}, Lbf/cg;->av()I

    move-result v1

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;Z)V

    invoke-virtual {v0}, Lbf/cg;->l()V

    return-void
.end method

.method public static m(Lbf/i;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LAYER_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/i;->aO()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o(Lbf/i;)V
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->a(Lbf/i;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lbf/am;->c(Lbf/i;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->A:Z

    monitor-exit v2

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static p(Lbf/i;)C
    .locals 3

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    check-cast p0, Lbf/y;

    invoke-virtual {p0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LayerTransit"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x54

    :goto_0
    return v0

    :cond_0
    const-string v2, "LayerWikipedia"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x57

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lbf/am;->g(I)C

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public A()Lbf/bx;
    .locals 1

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bx;

    return-object v0
.end method

.method public B()Lbf/X;
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/X;

    return-object v0
.end method

.method public C()Lbf/aJ;
    .locals 1

    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aJ;

    return-object v0
.end method

.method protected D()I
    .locals 11

    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v5, -0x1

    iget-object v1, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v5

    :cond_0
    iget-object v1, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ne v1, v8, :cond_1

    move v5, v0

    goto :goto_0

    :cond_1
    const-wide v1, 0x7fffffffffffffffL

    move v4, v5

    move-wide v9, v1

    move-wide v2, v9

    move v1, v0

    :goto_1
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    invoke-virtual {v0}, Lbf/ai;->e()Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lbf/ai;->d()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gez v6, :cond_2

    invoke-virtual {v0}, Lbf/ai;->d()J

    move-result-wide v2

    move v4, v1

    goto :goto_2

    :cond_4
    if-ne v4, v5, :cond_6

    invoke-direct {p0}, Lbf/am;->ad()V

    iget v0, p0, Lbf/am;->O:I

    if-eq v0, v5, :cond_5

    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    iget v1, p0, Lbf/am;->O:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    invoke-virtual {v0, v8}, Lbf/ai;->a(Z)V

    :cond_5
    invoke-virtual {p0}, Lbf/am;->D()I

    move-result v5

    goto :goto_0

    :cond_6
    move v5, v4

    goto :goto_0
.end method

.method public E()V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v2

    if-nez v2, :cond_1

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    :cond_0
    :goto_0
    iget-object v2, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    const-string v3, "22"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v1, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public F()Z
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->X()Z

    move-result v0

    or-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/am;->d(Z)V

    :cond_1
    return v2
.end method

.method public G()V
    .locals 0

    return-void
.end method

.method public H()Lbf/i;
    .locals 1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    goto :goto_0
.end method

.method public I()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    return-object v0
.end method

.method public J()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    return-object v0
.end method

.method public K()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    return-object v0
.end method

.method public L()V
    .locals 1

    iget-object v0, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v0}, Lcom/google/googlenav/layer/r;->a()V

    :cond_0
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lbf/am;->ae()V

    invoke-direct {p0}, Lbf/am;->af()V

    :cond_1
    return-void
.end method

.method public M()V
    .locals 3

    const/4 v2, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/am;->ah()V

    :cond_0
    sget-object v0, LaE/g;->a:LaE/g;

    invoke-virtual {v0}, LaE/g;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/ap;

    invoke-direct {v1, p0}, Lbf/ap;-><init>(Lbf/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_1
    const-string v0, "PROTO_SAVED_LAYER_STATE"

    invoke-direct {p0, v0}, Lbf/am;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    iget-object v0, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/aq;

    invoke-direct {v1, p0}, Lbf/aq;-><init>(Lbf/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public N()V
    .locals 1

    const-string v0, "PROTO_SAVED_RECENT_LAYERS"

    invoke-direct {p0, v0}, Lbf/am;->h(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/am;->ak()V

    return-void
.end method

.method public O()Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aA;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gtz v1, :cond_0

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->b()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->ah()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public P()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbf/am;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public Q()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/am;->D:Ljava/lang/String;

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbf/am;->A()Lbf/bx;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/bx;->Y()V

    :cond_1
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->Y()V

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->V()V

    goto :goto_0
.end method

.method public R()Z
    .locals 1

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public S()V
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lbf/am;->ab()V

    invoke-direct {p0, v5, v2}, Lbf/am;->b(IZ)V

    const/16 v0, 0x1a

    invoke-direct {p0, v0, v2}, Lbf/am;->b(IZ)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    if-nez v4, :cond_0

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bl()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {p0, v0, v2, v5}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lbf/am;->d()V

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->e(Lbf/i;)V

    return-void
.end method

.method protected T()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/am;->a(Z)Lbf/by;

    invoke-virtual {p0}, Lbf/am;->s()V

    invoke-virtual {p0}, Lbf/am;->t()Lbf/bx;

    sget-object v0, LaE/g;->a:LaE/g;

    invoke-virtual {v0}, LaE/g;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/am;->m()Lbf/aA;

    :cond_0
    return-void
.end method

.method public U()V
    .locals 2

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lbf/i;->al()V

    :goto_0
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->H()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lbf/i;->Z()V

    goto :goto_0
.end method

.method public V()Lcom/google/googlenav/E;
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    :cond_0
    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v0

    const/16 v3, 0xd

    if-ne v0, v3, :cond_2

    move-object v0, v1

    check-cast v0, Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v2

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_0
.end method

.method public Y()V
    .locals 1

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->e(Lbf/i;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/i;->aV()V

    :cond_0
    return-void
.end method

.method public Z()I
    .locals 1

    iget v0, p0, Lbf/am;->B:I

    return v0
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p3, v1}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ai;->a(Lo/D;)V

    new-instance v1, Lcom/google/googlenav/bl;

    invoke-direct {v1, v0}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, p4, p5}, Lbf/am;->a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)Lbf/C;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)Lbf/C;
    .locals 10

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/bl;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/am;->c(LaN/B;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->g()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->z()V

    :goto_0
    if-nez v8, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/am;->d(Z)V

    :cond_0
    const-string v0, "s"

    invoke-direct {p0, v0}, Lbf/am;->i(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    new-instance v0, Lbf/C;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    move v6, p3

    invoke-direct/range {v0 .. v9}, Lbf/C;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;Z[Ljava/lang/String;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v9}, Lbf/am;->a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, p2}, Lbf/C;->a(B)V

    return-object v0

    :cond_1
    move-object v7, p4

    goto :goto_0
.end method

.method public a(Lax/b;)Lbf/O;
    .locals 7

    invoke-virtual {p0}, Lbf/am;->i()V

    new-instance v0, Lbf/O;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->d:LaH/m;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lbf/O;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/F;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;)Lbf/aJ;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    :cond_0
    invoke-direct {p0}, Lbf/am;->ab()V

    invoke-virtual {p0, p1}, Lbf/am;->d(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(LaN/B;LaN/Y;Ljava/lang/String;)Lbf/aM;
    .locals 2

    iget-object v0, p0, Lbf/am;->f:LaN/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaN/u;->a(I)V

    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v0, p1, p2}, LaN/u;->e(LaN/B;LaN/Y;)V

    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aM;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/aM;->aW()V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p3}, Lbf/am;->a(Ljava/lang/String;)Lbf/aM;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lbf/aM;
    .locals 6

    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    new-instance v0, Lbf/aM;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/aM;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/F;)Lbf/aU;
    .locals 7

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    new-instance v0, Lbf/aU;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-static {}, Lbf/am;->W()I

    move-result v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/aU;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;I)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;
    .locals 10

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    new-instance v0, Lbf/ak;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    move-object v6, p1

    move v7, p2

    move v8, p3

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lbf/ak;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/ai;ZBZ)V

    invoke-virtual {p0, v0, p5}, Lbf/am;->a(Lbf/i;Z)V

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/layer/m;)Lbf/bE;
    .locals 7

    new-instance v0, Lbf/bE;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    new-instance v6, LaN/k;

    invoke-direct {v6}, LaN/k;-><init>()V

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/bE;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bN;)Lbf/bH;
    .locals 6

    invoke-virtual {p0}, Lbf/am;->k()V

    new-instance v0, Lbf/bH;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/bH;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(Lax/w;)Lbf/bK;
    .locals 6

    invoke-virtual {p0}, Lbf/am;->j()V

    new-instance v0, Lbf/bK;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/bK;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/Y;Z)Lbf/bU;
    .locals 8

    invoke-virtual {p0}, Lbf/am;->l()V

    new-instance v0, Lbf/bU;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lbf/bU;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/F;Z)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;Z)Lbf/bk;
    .locals 9

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez p2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, v1, v1}, Lbf/am;->b(IZ)V

    :cond_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    :cond_2
    invoke-direct {p0}, Lbf/am;->ab()V

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v8, 0x5

    :goto_0
    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0

    :cond_3
    invoke-static {}, Lbf/am;->W()I

    move-result v8

    goto :goto_0
.end method

.method public a(Z)Lbf/by;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    new-instance v1, Lbf/by;

    iget-object v2, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v3, p0, Lbf/am;->e:LaN/p;

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    iget-object v5, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v1, v2, v3, v4, v5}, Lbf/by;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    sget-object v2, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v2}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1, v0, v0}, Lbf/am;->a(Lbf/i;ZZ)Z

    :goto_0
    return-object v1

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0, v1, v0}, Lbf/am;->a(Lbf/i;Z)V

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .locals 7

    new-instance v0, Lbf/y;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/layer/m;ZZZ)Lbf/y;
    .locals 7

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbf/am;->a(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v3

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZLbf/y;ZZ)V

    return-object v3

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->i()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    new-instance v6, LaN/k;

    invoke-direct {v6}, LaN/k;-><init>()V

    :goto_1
    new-instance v0, Lbf/y;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    move-object v3, v0

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lbf/am;->h:LaN/k;

    goto :goto_1
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->r:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->r:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/l;

    invoke-interface {v0}, Lcom/google/googlenav/layer/l;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(I)V
    .locals 4

    const/4 v2, 0x0

    invoke-static {p1}, Lbf/am;->b(I)I

    move-result v0

    invoke-direct {p0, p1}, Lbf/am;->e(I)I

    move-result v1

    if-lt v1, v0, :cond_0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v3

    if-ne v3, p1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lbf/am;->b(Lbf/i;ZZ)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lbf/am;->B:I

    iput-object p2, p0, Lbf/am;->C:Ljava/lang/Object;

    return-void
.end method

.method protected a(LaN/B;)V
    .locals 4

    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    iput-object p1, p0, Lbf/am;->N:LaN/B;

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    iget-object v2, p0, Lbf/am;->L:Ljava/util/Vector;

    sget v3, Lbf/am;->J:I

    invoke-virtual {v0, v2, p1, v3}, Lbf/i;->a(Ljava/util/Vector;LaN/B;I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v0

    sget v2, Lbf/am;->J:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    iget-object v2, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-static {v0, v1}, Lbf/ai;->a(J)Lbf/ai;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->a(Landroid/content/res/Configuration;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lbb/w;)V
    .locals 11

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v3, v3}, Lbf/am;->b(IZ)V

    invoke-direct {p0, v4, v3}, Lbf/am;->b(IZ)V

    invoke-direct {p0}, Lbf/am;->ab()V

    invoke-virtual {p1}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbb/w;->h()LaN/B;

    move-result-object v9

    invoke-virtual {p1}, Lbb/w;->i()LaN/Y;

    move-result-object v10

    new-instance v1, Lcom/google/googlenav/ai;

    const/16 v2, 0xa

    invoke-direct {v1, v9, v0, v2}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    new-array v2, v4, [Lcom/google/googlenav/ai;

    aput-object v1, v2, v3

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v3}, LaN/u;->a()I

    move-result v3

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v4}, LaN/u;->b()I

    move-result v4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/T;

    sget-object v2, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v3, p0, Lbf/am;->h:LaN/k;

    iget-object v4, p0, Lbf/am;->e:LaN/p;

    iget-object v5, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    new-instance v5, Lcom/google/googlenav/n;

    invoke-direct {v5, v0, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v6, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-static {}, Lbf/am;->W()I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1, v9, v10}, LaN/u;->d(LaN/B;LaN/Y;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-void
.end method

.method public a(Lbf/X;)V
    .locals 3

    const/4 v2, -0x1

    iget v0, p0, Lbf/am;->B:I

    iget-object v1, p0, Lbf/am;->C:Ljava/lang/Object;

    invoke-virtual {p1, v0, v2, v1}, Lbf/X;->a(IILjava/lang/Object;)Z

    iput v2, p0, Lbf/am;->B:I

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/am;->C:Ljava/lang/Object;

    return-void
.end method

.method public a(Lbf/aU;)V
    .locals 2

    invoke-virtual {p1}, Lbf/aU;->a()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->b(Lcom/google/googlenav/aZ;Z)V

    :cond_0
    return-void
.end method

.method public a(Lbf/au;)V
    .locals 2

    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbf/au;LR/a;)V
    .locals 2

    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Lbf/i;I)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lbf/am;->c(Lbf/i;I)V

    :goto_0
    invoke-virtual {p1}, Lbf/i;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lbf/i;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->b(LaN/B;)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1, p2}, Lbf/am;->b(Lbf/i;I)V

    goto :goto_0
.end method

.method public a(Lbf/i;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lbf/am;->a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->aX()V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lbf/am;->a(Lbf/i;ZZ)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lbf/i;->aW()V

    invoke-virtual {p1}, Lbf/i;->aM()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lbf/am;->X()I

    move-result v0

    invoke-virtual {p1, v0}, Lbf/i;->e(I)V

    invoke-direct {p0, p1}, Lbf/am;->b(Lbf/i;)V

    :cond_1
    invoke-virtual {p0}, Lbf/am;->d()V

    :cond_2
    if-eqz p3, :cond_3

    invoke-direct {p0, p3}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-void

    :cond_3
    const-string v0, "s"

    invoke-direct {p0, p1, v0}, Lbf/am;->a(Lbf/i;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/J;Lbf/aU;)V
    .locals 6

    const/4 v5, 0x4

    invoke-virtual {p2}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v1

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->z()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(LaN/M;)Lcom/google/googlenav/bg;

    move-result-object v0

    iget-object v2, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lbf/aU;->be()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lbf/aU;->be()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lbf/aU;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lbf/aU;->bf()Lcom/google/googlenav/ai;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    invoke-virtual {p2}, Lbf/aU;->bf()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->am()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/aZ;I)V
    .locals 2

    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/F;)Lbf/aU;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lbf/aU;->a(Lcom/google/googlenav/aZ;ZI)V

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;ILcom/google/googlenav/ba;)V
    .locals 1

    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/F;)Lbf/aU;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p2}, Lbf/aU;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/ba;I)V

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-direct {p0, p1}, Lbf/am;->i(Lcom/google/googlenav/aZ;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lbf/am;->a(Lcom/google/googlenav/aZ;ILcom/google/googlenav/ba;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v0}, Lbf/am;->a(Lcom/google/googlenav/ai;)V

    :cond_3
    invoke-virtual {p2}, Lcom/google/googlenav/A;->d()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->H:Z

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, p1}, Lbf/am;->j(Lcom/google/googlenav/aZ;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v3, :cond_6

    invoke-virtual {p2}, Lcom/google/googlenav/A;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, p1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p2, v0}, Lcom/google/googlenav/A;->a(Lcom/google/googlenav/ai;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-gtz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_8

    invoke-direct {p0, p1, p3}, Lbf/am;->c(Lcom/google/googlenav/aZ;Z)V

    goto :goto_1

    :cond_8
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0, p1}, Lbf/am;->h(Lcom/google/googlenav/aZ;)V

    goto :goto_1

    :cond_9
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->K()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->N()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0, p1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    goto :goto_1

    :cond_a
    invoke-virtual {p0, p1, v3}, Lbf/am;->b(Lcom/google/googlenav/aZ;Z)V

    goto/16 :goto_1
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 2

    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    invoke-static {p1}, Lax/y;->b(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/z;->a(Lax/y;)Z

    return-void
.end method

.method protected a(Lcom/google/googlenav/layer/m;ZLbf/y;)V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZLbf/y;ZZ)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/layer/m;ZLbf/y;ZZ)V
    .locals 2

    const/4 v1, 0x6

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lbf/am;->b(IZ)V

    :cond_0
    if-eqz p5, :cond_1

    invoke-direct {p0}, Lbf/am;->ab()V

    :cond_1
    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/am;->g(Ljava/lang/String;)V

    invoke-virtual {p0, p3, p4}, Lbf/am;->a(Lbf/i;Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->b(Lcom/google/googlenav/ui/r;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/i;->e(Lcom/google/googlenav/ui/r;)V

    :goto_1
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->d(Lcom/google/googlenav/ui/r;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/jv;Lbf/aU;)V
    .locals 2

    invoke-virtual {p2}, Lbf/aU;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "100"

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p2}, Lbf/aU;->bg()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->y()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/b;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dg;->h()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lo/S;)V
    .locals 5

    invoke-virtual {p1}, Lo/S;->a()Lo/T;

    move-result-object v1

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v2, 0xd

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lbf/C;->aa()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lbf/C;->bJ()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lbf/am;->f:LaN/u;

    invoke-static {v1}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lbf/C;->a()LaN/B;

    move-result-object v3

    sget v4, Lbf/am;->Q:I

    invoke-virtual {v2, v1, v3, v4}, LaN/u;->a(LaN/B;LaN/B;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    goto :goto_0
.end method

.method public a(ZZZ)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lbf/am;->g(Z)V

    if-eqz p1, :cond_0

    iget-object v2, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lbf/am;->T()V

    :cond_1
    iget-object v0, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->l()V

    move v0, v1

    :goto_0
    sget-object v2, Lbf/am;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    sget-object v2, Lbf/am;->u:[Z

    aput-boolean v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    return-void
.end method

.method public a(LaN/B;LaN/B;IIIIILam/e;)Z
    .locals 7

    const/4 v2, 0x0

    if-nez p2, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {p1, v0}, LaN/B;->a(LaN/Y;)I

    move-result v1

    invoke-virtual {p2, v0}, LaN/B;->a(LaN/Y;)I

    move-result v3

    sub-int v4, v1, v3

    invoke-virtual {p1, v0}, LaN/B;->b(LaN/Y;)I

    move-result v1

    invoke-virtual {p2, v0}, LaN/B;->b(LaN/Y;)I

    move-result v0

    sub-int v5, v1, v0

    packed-switch p3, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Feature.ANCHOR_* type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    div-int/lit8 v0, p4, 0x2

    neg-int v1, v0

    neg-int v0, p5

    :goto_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->i()I

    move-result v3

    if-ge p4, v3, :cond_1

    sub-int v6, v3, p4

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v6

    move p4, v3

    :cond_1
    if-ge p5, v3, :cond_2

    sub-int v6, v3, p5

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v0, v6

    move p5, v3

    :cond_2
    add-int v3, v1, p4

    add-int v6, v0, p5

    if-lt v4, v1, :cond_3

    if-gt v4, v3, :cond_3

    if-lt v5, v0, :cond_3

    if-gt v5, v6, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v2, v0

    goto :goto_0

    :pswitch_1
    div-int/lit8 v0, p4, 0x2

    neg-int v1, v0

    div-int/lit8 v0, p5, 0x2

    neg-int v0, v0

    goto :goto_1

    :pswitch_2
    neg-int v1, p4

    neg-int v0, p5

    goto :goto_1

    :pswitch_3
    neg-int v1, p6

    neg-int v0, p7

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lat/a;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lbf/i;->aa()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v3

    const/16 v4, 0x32

    if-ne v3, v4, :cond_1

    iget-object v0, p0, Lbf/am;->o:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;Z)V

    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v3

    const/16 v4, 0x2a

    if-ne v3, v4, :cond_2

    iget-object v2, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->j()Lbf/by;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lbf/by;->e(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2, p1}, Lbf/i;->a(Lat/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lbf/am;->w()Lbf/bK;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->H()V

    move v0, v1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method public a(Lat/b;)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/aA;->a(Lat/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lat/b;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v3

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v4

    invoke-virtual {v0, v3, v4}, LaN/u;->b(II)LaN/B;

    move-result-object v3

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    iget-boolean v4, p0, Lbf/am;->M:Z

    if-nez v4, :cond_3

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lbf/i;->aj()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    move v0, v2

    :goto_1
    if-nez v0, :cond_4

    iget-object v0, p0, Lbf/am;->N:LaN/B;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbf/am;->N:LaN/B;

    iget-object v4, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v4}, LaN/p;->c()LaN/Y;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v4

    sget v0, Lbf/am;->J:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-lez v0, :cond_8

    :cond_4
    invoke-virtual {p0, v1}, Lbf/am;->d(Z)V

    invoke-virtual {p0, v3}, Lbf/am;->a(LaN/B;)V

    move v0, v2

    :goto_2
    invoke-virtual {p0, v0}, Lbf/am;->c(Z)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Lbf/i;->a(Lat/b;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    move v2, v1

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    invoke-virtual {p0, v3}, Lbf/am;->b(LaN/B;)V

    move v0, v1

    goto :goto_2
.end method

.method protected a(Lbf/i;ZZ)Z
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lbf/i;->aS()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    sget v1, Lbf/am;->a:I

    if-lt v0, v1, :cond_1

    move v1, v2

    :goto_1
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->aB()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0, v0, v2, v3}, Lbf/am;->b(Lbf/i;ZZ)V

    :cond_1
    if-eqz p2, :cond_6

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :goto_2
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_7

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, p1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :cond_2
    :goto_3
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lbf/am;->o(Lbf/i;)V

    :cond_3
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->a(Lcom/google/googlenav/ui/Y;)V

    invoke-virtual {p0, p1}, Lbf/am;->j(Lbf/i;)V

    if-eqz p3, :cond_4

    invoke-virtual {p1, v3}, Lbf/i;->f(I)V

    invoke-virtual {p1}, Lbf/i;->at()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lbf/i;->f(I)V

    :cond_4
    move v2, v3

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public aa()Lbf/a;
    .locals 1

    iget-object v0, p0, Lbf/am;->G:Lbf/a;

    return-object v0
.end method

.method public b(Lax/b;)Lbf/O;
    .locals 3

    invoke-virtual {p0}, Lbf/am;->v()Lbf/O;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/O;->ae()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-virtual {p0, p1}, Lbf/am;->a(Lax/b;)Lbf/O;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    iget-object v1, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lbf/am;->i(Lbf/i;)V

    invoke-virtual {v0, p1}, Lbf/O;->b(Lcom/google/googlenav/F;)V

    invoke-virtual {v0}, Lbf/O;->J()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lbf/am;->a(Lbf/i;ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lbf/O;->aM()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lbf/am;->X()I

    move-result v1

    invoke-virtual {v0, v1}, Lbf/O;->e(I)V

    invoke-direct {p0, v0}, Lbf/am;->b(Lbf/i;)V

    :cond_3
    invoke-virtual {p0}, Lbf/am;->d()V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/aZ;)Lbf/x;
    .locals 9

    const/16 v4, 0x28

    const/16 v3, 0x24

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    if-eq v2, v4, :cond_0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lbf/am;->ab()V

    :cond_0
    :goto_1
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v2, 0x11

    if-eq v0, v2, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    if-eq v0, v3, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    if-ne v0, v4, :cond_2

    :cond_1
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbf/i;->ah()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lbf/i;->aQ()V

    :cond_2
    invoke-static {}, Lbf/am;->W()I

    move-result v8

    new-instance v5, Lcom/google/googlenav/n;

    new-instance v0, Lcom/google/googlenav/T;

    sget-object v1, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v2, p0, Lbf/am;->h:LaN/k;

    iget-object v3, p0, Lbf/am;->e:LaN/p;

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-direct {v5, p1, v0}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    new-instance v0, Lbf/x;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v6, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v8}, Lbf/x;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lbf/am;->e()V

    goto :goto_1
.end method

.method public b(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZZZ)Lbf/y;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lbf/y;
    .locals 5

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    const/4 v4, 0x6

    if-ne v1, v4, :cond_0

    move-object v1, v0

    check-cast v1, Lbf/y;

    invoke-virtual {v1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lbf/y;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/am;->T()V

    iget-object v0, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->l()V

    move v0, v1

    :goto_0
    sget-object v2, Lbf/am;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lbf/am;->u:[Z

    aput-boolean v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected b(LaN/B;)V
    .locals 7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    invoke-virtual {v0}, Lbf/ai;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbf/ai;->b(J)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v0, v2, v3}, Lbf/ai;->b(J)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lbf/ai;->a()Lbf/i;

    move-result-object v4

    invoke-virtual {v0}, Lbf/ai;->b()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {v4, v2, p1}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v2

    invoke-virtual {v4}, Lbf/i;->ax()Z

    move-result v5

    if-nez v5, :cond_2

    sget v5, Lbf/am;->j:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    :cond_2
    invoke-virtual {v4}, Lbf/i;->av()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    sget v5, Lbf/am;->j:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    :cond_3
    invoke-virtual {v4}, Lbf/i;->av()I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_4

    sget v4, Lbf/am;->j:I

    mul-int/lit8 v4, v4, 0x2

    int-to-long v4, v4

    add-long/2addr v2, v4

    :cond_4
    invoke-virtual {v0, v2, v3}, Lbf/ai;->b(J)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method protected b(Lbf/i;ZZ)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0, p1}, Lbf/am;->k(Lbf/i;)V

    invoke-virtual {p1, v2}, Lbf/i;->h(Z)V

    invoke-virtual {p1}, Lbf/i;->aX()V

    invoke-virtual {p1}, Lbf/i;->aU()V

    iget-object v1, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    iget-object v1, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lbf/am;->i(Lbf/i;)V

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->aW()V

    :cond_0
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, v2}, Lbf/am;->f(Z)V

    :cond_1
    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    invoke-direct {p0, p1}, Lbf/am;->a(Lbf/i;)V

    invoke-virtual {p1}, Lbf/i;->aM()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p3, :cond_2

    invoke-virtual {p1}, Lbf/i;->aN()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lbf/am;->b(Lbf/i;)V

    :cond_2
    :goto_0
    if-eqz p3, :cond_3

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lbf/i;->f(I)V

    :cond_3
    return-void

    :cond_4
    invoke-direct {p0, p1}, Lbf/am;->c(Lbf/i;)V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/J;Lbf/aU;)V
    .locals 2

    invoke-virtual {p2}, Lbf/aU;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lbf/am;->a(Lcom/google/googlenav/J;Lbf/aU;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lbf/am;->o:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {p2}, Lbf/aU;->bg()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ca;->c(Lax/b;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public b(Lcom/google/googlenav/aZ;Z)V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    check-cast v0, Lbf/bk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/bk;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbf/bk;->ae()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "20"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    instance-of v0, v0, Lbf/bk;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->ax()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lbf/bk;->ae()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/bk;->c(Lcom/google/googlenav/F;)V

    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lbf/bk;->af()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->W()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lbf/bk;->h()V

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/bk;->c(Lcom/google/googlenav/F;)V

    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    goto :goto_0

    :cond_3
    move v2, v3

    :goto_1
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move-object v1, v0

    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_4
    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    :cond_5
    move v2, v3

    :goto_2
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    if-nez v1, :cond_c

    move-object v1, v0

    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ae()Z

    move-result v1

    if-eqz v1, :cond_c

    check-cast v0, Lbf/bk;

    invoke-virtual {v0, v3}, Lbf/bk;->k(I)V

    :cond_6
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lbf/am;->ai()V

    :cond_7
    iget-boolean v0, p0, Lbf/am;->s:Z

    if-nez v0, :cond_d

    move v0, v4

    :goto_3
    invoke-virtual {p0, p1, v0}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    iget-boolean v1, p0, Lbf/am;->s:Z

    if-nez v1, :cond_8

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ae()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    invoke-virtual {v0, v4}, Lbf/bk;->h(Z)V

    :cond_9
    invoke-virtual {p0, v3}, Lbf/am;->e(Z)V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_a
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->i()V

    goto/16 :goto_0

    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_d
    move v0, v3

    goto :goto_3
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 2

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lbf/i;->a(Lcom/google/googlenav/ui/r;)V

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->B()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbf/i;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbf/am;->D:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/am;->D:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Lcom/google/googlenav/ui/r;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->a(I)Z

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->c_(Z)V

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2, v2}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lbf/am;->U()V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lbf/i;->aB()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lbf/i;->aj()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    invoke-virtual {v0}, Lbf/i;->al()V

    goto :goto_1

    :cond_4
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {p0, v0, v6, v6}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_2

    :cond_6
    sget-object v0, LaE/g;->a:LaE/g;

    invoke-virtual {v0}, LaE/g;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbf/aA;->k(Z)V

    :cond_7
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_8

    invoke-virtual {p0}, Lbf/am;->p()V

    :cond_8
    iget-object v0, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->l()V

    move v0, v2

    :goto_3
    sget-object v1, Lbf/am;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_9

    sget-object v1, Lbf/am;->u:[Z

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Lbf/am;->d()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/am;->d(Lbf/i;)V

    return-void
.end method

.method public b(Lat/a;)Z
    .locals 1

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lbf/i;->b(Lat/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/google/googlenav/aZ;)Lbf/bk;
    .locals 9

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    invoke-static {}, Lbf/am;->W()I

    move-result v8

    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    return-object v0
.end method

.method public c(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .locals 7

    new-instance v0, Lbf/bD;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/bD;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    invoke-virtual {p0, p1, p2, v0}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZLbf/y;)V

    return-object v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public c(I)V
    .locals 5

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v0}, Lbf/i;->aD()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lbf/am;->b(Lbf/i;ZZ)V

    invoke-direct {p0, v0}, Lbf/am;->o(Lbf/i;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lbf/am;->d()V

    return-void
.end method

.method public c(Lax/b;)V
    .locals 2

    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/F;)Lbf/aU;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/aU;->a(Lax/b;I)V

    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "PROTO_SAVED_LAYER_STATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->as()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Z)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lbf/am;->O:I

    invoke-virtual {p0}, Lbf/am;->D()I

    move-result v3

    iput v3, p0, Lbf/am;->O:I

    iget v3, p0, Lbf/am;->O:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    if-nez p1, :cond_0

    iget v3, p0, Lbf/am;->O:I

    if-eq v0, v3, :cond_2

    :cond_0
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    iget v3, p0, Lbf/am;->O:I

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    invoke-virtual {v0, v2}, Lbf/ai;->a(Z)V

    invoke-virtual {v0}, Lbf/ai;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lbf/am;->h()Lbf/C;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Lbf/C;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;I)V

    :cond_2
    :goto_1
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lbf/ai;->a()Lbf/i;

    move-result-object v1

    invoke-virtual {v0}, Lbf/ai;->c()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lbf/am;->a(Lbf/i;I)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public d(Lcom/google/googlenav/aZ;)Lbf/aJ;
    .locals 9

    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    new-instance v0, Lbf/aJ;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    iget-object v8, p0, Lbf/am;->P:Lcom/google/googlenav/offers/j;

    invoke-direct/range {v0 .. v8}, Lbf/aJ;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V

    return-object v0
.end method

.method protected d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->z:Z

    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->e()V

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->V()V

    return-void
.end method

.method public d(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbf/am;->a(ILjava/lang/Object;)V

    return-void
.end method

.method d(Lbf/i;)V
    .locals 2

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbf/i;->aE()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/as;->a(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/am;->M:Z

    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/y;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/y;->bH()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Lcom/google/googlenav/aZ;)Lbf/X;
    .locals 11

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    new-instance v0, Lbf/X;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    iget-object v5, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->k:Lcom/google/googlenav/friend/J;

    iget-object v7, p0, Lbf/am;->l:Lcom/google/googlenav/friend/p;

    iget-object v8, p0, Lbf/am;->m:Lcom/google/googlenav/friend/ag;

    iget-object v10, p0, Lbf/am;->G:Lbf/a;

    move-object v9, p1

    invoke-direct/range {v0 .. v10}, Lbf/X;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/F;Lbf/a;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    invoke-virtual {p0, v0}, Lbf/am;->a(Lbf/X;)V

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lbf/i;
    .locals 4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    const/4 v3, 0x6

    if-ne v1, v3, :cond_0

    move-object v1, v0

    check-cast v1, Lbf/y;

    invoke-virtual {v1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x12

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    const/16 v0, 0xf

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method public e(Lbf/i;)V
    .locals 3

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbf/i;->aX()V

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->b(Lcom/google/googlenav/ui/Y;)V

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->a(Lcom/google/googlenav/ui/Y;)V

    invoke-virtual {p1}, Lbf/i;->aW()V

    invoke-virtual {p0}, Lbf/am;->d()V

    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lbf/i;->ax()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lbf/i;->aW()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lbf/i;->k()Lcom/google/googlenav/settings/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbf/i;->k()Lcom/google/googlenav/settings/e;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->g(Z)Z

    goto :goto_0
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/am;->s:Z

    return-void
.end method

.method public f()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method public f(Lbf/i;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lbf/am;->a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public f(Lcom/google/googlenav/aZ;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/am;->G:Lbf/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, v2, v2}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lbf/am;->D:Ljava/lang/String;

    return-void
.end method

.method public f(Z)V
    .locals 0

    sput-boolean p1, Lbf/am;->v:Z

    return-void
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 6

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "visible layers"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v5

    move v1, v2

    :goto_1
    :try_start_0
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "recent layers"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "LayerManager"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public g(Lbf/i;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v1

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p0, v5}, Lbf/am;->e(Z)V

    check-cast p1, Lbf/bk;

    invoke-virtual {p1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/aZ;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-direct {v3}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->z()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->W()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v3

    const-string v4, "13"

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v4}, LaN/u;->f()LaN/H;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v3

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->N()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aZ;->a(Z)V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aN()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aZ;->c(Z)V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/z;->a()V

    :cond_0
    iget-object v3, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {v3, v2, v0, v5}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0, p1}, Lbf/am;->f(Lbf/i;)V

    invoke-virtual {p1, v5}, Lbf/i;->h(Z)V

    goto :goto_0

    :sswitch_2
    check-cast p1, Lbf/y;

    invoke-virtual {p1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "msid:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1, v0}, Lbf/am;->c(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v0

    :goto_2
    invoke-virtual {v0, v5}, Lbf/y;->h(Z)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0, v1, v0}, Lbf/am;->b(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v0

    goto :goto_2

    :sswitch_3
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x15 -> :sswitch_1
    .end sparse-switch
.end method

.method protected h()Lbf/C;
    .locals 6

    const/4 v3, 0x0

    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->i()LaH/h;

    move-result-object v2

    invoke-static {v2}, LaH/h;->d(Landroid/location/Location;)Lo/D;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->g()[Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;

    move-result-object v3

    :cond_0
    return-object v3
.end method

.method public h(Lbf/i;)V
    .locals 1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v0, v0}, Lbf/am;->b(Lbf/i;ZZ)V

    invoke-virtual {p0}, Lbf/am;->d()V

    const-string v0, "h"

    invoke-direct {p0, p1, v0}, Lbf/am;->a(Lbf/i;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public i()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    invoke-virtual {p0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    invoke-direct {p0, v2, v1}, Lbf/am;->b(IZ)V

    invoke-direct {p0}, Lbf/am;->ab()V

    invoke-virtual {p0, v2}, Lbf/am;->a(I)V

    return-void
.end method

.method protected i(Lbf/i;)V
    .locals 1

    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->b(Lcom/google/googlenav/ui/Y;)V

    return-void
.end method

.method public j()V
    .locals 3

    const/16 v2, 0x15

    const/4 v1, 0x0

    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    invoke-direct {p0, v1, v1}, Lbf/am;->b(IZ)V

    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    invoke-direct {p0, v2, v1}, Lbf/am;->b(IZ)V

    invoke-direct {p0}, Lbf/am;->ab()V

    invoke-virtual {p0, v2}, Lbf/am;->a(I)V

    return-void
.end method

.method protected j(Lbf/i;)V
    .locals 5

    iget-object v2, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/au;

    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LR/a;

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, LR/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Lbf/au;->a(Lbf/i;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public k()V
    .locals 2

    invoke-direct {p0}, Lbf/am;->ab()V

    const/16 v0, 0x17

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method protected k(Lbf/i;)V
    .locals 5

    iget-object v2, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/au;

    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LR/a;

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, LR/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Lbf/au;->b(Lbf/i;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public l()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lbf/am;->ab()V

    const/16 v0, 0x18

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    const/16 v0, 0x17

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method protected l(Lbf/i;)V
    .locals 5

    iget-object v2, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/au;

    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LR/a;

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, LR/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Lbf/au;->c(Lbf/i;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public m()Lbf/aA;
    .locals 5

    const/4 v1, 0x0

    const/16 v0, 0x13

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    const/16 v0, 0xd

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    new-instance v0, Lbf/aA;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v0, v1, v2, v3, v4}, Lbf/aA;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public n()Lbf/aA;
    .locals 1

    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aA;

    return-object v0
.end method

.method public n(Lbf/i;)V
    .locals 1

    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->e()V

    return-void
.end method

.method public o()V
    .locals 2

    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method public p()V
    .locals 2

    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    return-void
.end method

.method public q()Lbf/bE;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/layer/m;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    invoke-virtual {p0, v0}, Lbf/am;->a(Lcom/google/googlenav/layer/m;)Lbf/bE;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;Z)V

    return-object v0
.end method

.method public r()V
    .locals 1

    invoke-virtual {p0}, Lbf/am;->z()Lbf/bE;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    return-void
.end method

.method public s()V
    .locals 8

    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/googlenav/layer/m;

    new-instance v0, Lbf/D;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v6}, Lbf/D;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public t()Lbf/bx;
    .locals 5

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    new-instance v0, Lbf/bx;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v0, v1, v2, v3, v4}, Lbf/bx;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    return-object v0
.end method

.method public u()Lbf/bk;
    .locals 2

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/bk;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public v()Lbf/O;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/O;

    return-object v0
.end method

.method public w()Lbf/bK;
    .locals 1

    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bK;

    return-object v0
.end method

.method public x()Lbf/bU;
    .locals 1

    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bU;

    return-object v0
.end method

.method public y()Lbf/by;
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/by;

    return-object v0
.end method

.method public z()Lbf/bE;
    .locals 1

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bE;

    return-object v0
.end method
