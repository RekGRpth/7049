.class public Lbf/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/au;


# instance fields
.field private final a:Lcom/google/googlenav/ui/s;

.field private final b:LaN/u;

.field private final c:Lcom/google/googlenav/ui/wizard/jv;

.field private final d:Lbf/am;

.field private final e:Lcom/google/googlenav/ui/ak;

.field private final f:Lcom/google/googlenav/aA;

.field private final g:Lcom/google/android/maps/MapsActivity;

.field private final h:Lcom/google/googlenav/actionbar/b;

.field private i:Lbf/ay;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;Lcom/google/android/maps/MapsActivity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lbf/ay;->a:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    iput-object p1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->d:Lbf/am;

    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->b:LaN/u;

    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->e:Lcom/google/googlenav/ui/ak;

    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->f:Lcom/google/googlenav/aA;

    iput-object p2, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    new-instance v0, Lbf/aw;

    invoke-direct {v0, p0}, Lbf/aw;-><init>(Lbf/av;)V

    iput-object v0, p0, Lbf/av;->h:Lcom/google/googlenav/actionbar/b;

    return-void
.end method

.method static synthetic a(Lbf/av;)Lbf/ay;
    .locals 1

    iget-object v0, p0, Lbf/av;->i:Lbf/ay;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lbf/ay;->a:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    :goto_0
    return-void

    :pswitch_1
    sget-object v0, Lbf/ay;->c:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lbf/ay;->d:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lbf/ay;->b:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lbf/ay;->f:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lbf/ay;->e:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f11000b
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/view/MenuItem;Z)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/av;->i:Lbf/ay;

    sget-object v1, Lbf/ay;->b:Lbf/ay;

    if-ne v0, v1, :cond_3

    invoke-interface {p1}, Landroid/view/MenuItem;->expandActionView()Z

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Landroid/view/MenuItem;->collapseActionView()Z

    goto :goto_1
.end method

.method static synthetic b(Lbf/av;)Lbf/am;
    .locals 1

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    return-object v0
.end method

.method private b()Lbf/i;
    .locals 4

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    sget-object v2, Lbf/ax;->a:[I

    iget-object v3, p0, Lbf/av;->i:Lbf/ay;

    invoke-virtual {v3}, Lbf/ay;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->v()Lbf/O;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v2, 0x1a

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->C()Lbf/aJ;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const/16 v2, 0x17

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->x()Lbf/bU;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(Landroid/view/MenuItem;)V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    const/4 v2, 0x0

    iget-object v3, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    iget-object v2, p0, Lbf/av;->h:Lcom/google/googlenav/actionbar/b;

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    :cond_1
    return-void
.end method

.method static synthetic c(Lbf/av;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/actionbar/b;
    .locals 1

    iget-object v0, p0, Lbf/av;->h:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lbf/i;->aG()I

    move-result v1

    move v6, v1

    :goto_0
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lbf/i;->aH()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v5, v1

    :goto_1
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lbf/i;->aI()Ljava/lang/CharSequence;

    move-result-object v1

    :goto_2
    iget-object v2, p0, Lbf/av;->i:Lbf/ay;

    sget-object v7, Lbf/ay;->a:Lbf/ay;

    if-ne v2, v7, :cond_0

    iget-object v2, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->P()Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_0
    move v2, v4

    :goto_3
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v7

    invoke-virtual {v7, v6, v5, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    :cond_1
    const v1, 0x7f1004be

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->az()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    const v1, 0x7f1002f0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lbf/i;->N()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_3
    const v1, 0x7f1004c4

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lbf/i;->M()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_4
    const v1, 0x7f1004c5

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_5

    instance-of v2, v0, Lbf/O;

    if-eqz v2, :cond_5

    check-cast v0, Lbf/O;

    invoke-virtual {v0}, Lbf/O;->bk()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_5
    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v1

    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_6

    if-eqz v1, :cond_11

    const v0, 0x7f020262

    :goto_4
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-direct {p0, v2, v1}, Lbf/av;->a(Landroid/view/MenuItem;Z)V

    :cond_6
    const v0, 0x7f100499

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_7

    if-eqz v1, :cond_12

    const v0, 0x7f020235

    :goto_5
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_7
    const v0, 0x7f1004c2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_8

    if-eqz v1, :cond_13

    const v0, 0x7f020254

    :goto_6
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_8
    const v0, 0x7f1004c3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_9

    if-eqz v1, :cond_14

    const v0, 0x7f020245

    :goto_7
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_9
    const v0, 0x7f1004bf

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_15

    if-eqz v1, :cond_15

    move v0, v4

    :goto_8
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_a
    const v0, 0x7f1004c0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v2

    if-eqz v2, :cond_b

    if-nez v1, :cond_b

    move v3, v4

    :cond_b
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_c
    return v4

    :cond_d
    const v1, 0x7f020222

    move v6, v1

    goto/16 :goto_0

    :cond_e
    const/16 v1, 0x2a8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto/16 :goto_1

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_10
    move v2, v3

    goto/16 :goto_3

    :cond_11
    const v0, 0x7f020263

    goto/16 :goto_4

    :cond_12
    const v0, 0x7f020236

    goto :goto_5

    :cond_13
    const v0, 0x7f020255

    goto :goto_6

    :cond_14
    const v0, 0x7f020246

    goto :goto_7

    :cond_15
    move v0, v3

    goto :goto_8
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lbf/i;->aF()I

    move-result v0

    :goto_0
    iget-object v4, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v4}, Lbf/am;->w()Lbf/bK;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bK;->aF()I

    move-result v0

    :cond_0
    invoke-direct {p0, v0}, Lbf/av;->a(I)V

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {p1}, LaP/a;->a(Landroid/view/Menu;)V

    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v4

    const v0, 0x7f1004bf

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    move v0, v1

    :goto_1
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    const v0, 0x7f1004c0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez v4, :cond_2

    move v2, v1

    :cond_2
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    const v0, 0x7f10012a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lbf/i;->aJ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_4
    invoke-static {p1}, LaP/a;->b(Landroid/view/Menu;)V

    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v0}, Lbf/av;->b(Landroid/view/MenuItem;)V

    :cond_5
    return v1

    :cond_6
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()I

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 7

    const/16 v6, 0x36

    const/16 v5, 0x34

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_1

    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->h()V

    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    :goto_0
    :sswitch_0
    return v0

    :cond_0
    iget-object v1, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->b(Z)V

    goto :goto_0

    :cond_1
    const-string v3, "m"

    invoke-static {p1, v3}, LaP/a;->a(Landroid/view/MenuItem;Ljava/lang/String;)V

    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v3}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v3}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v3

    if-nez v3, :cond_5

    const v3, 0x7f10049c

    if-ne v2, v3, :cond_4

    :cond_3
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const/16 v2, 0x32d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_4
    const v3, 0x7f100499

    if-eq v2, v3, :cond_3

    const v3, 0x7f1004c2

    if-eq v2, v3, :cond_3

    const v3, 0x7f1004c3

    if-eq v2, v3, :cond_3

    :cond_5
    invoke-direct {p0}, Lbf/av;->b()Lbf/i;

    move-result-object v3

    sparse-switch v2, :sswitch_data_0

    :cond_6
    :goto_1
    move v0, v1

    goto :goto_0

    :sswitch_1
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lbf/i;->aY()Z

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0

    :sswitch_2
    if-eqz v3, :cond_8

    new-instance v2, Lat/a;

    invoke-direct {v2, v5, v5, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {v3, v2}, Lbf/i;->e(Lat/a;)Z

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_0

    :sswitch_3
    if-eqz v3, :cond_9

    new-instance v2, Lat/a;

    invoke-direct {v2, v6, v6, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {v3, v2}, Lbf/i;->e(Lat/a;)Z

    goto :goto_0

    :cond_9
    move v0, v1

    goto :goto_0

    :sswitch_4
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lbf/i;->av()I

    move-result v2

    if-ne v2, v0, :cond_a

    const/16 v1, 0xec

    const/4 v2, -0x1

    invoke-virtual {v3, v1, v2, v4}, Lbf/i;->a(IILjava/lang/Object;)Z

    goto :goto_0

    :cond_a
    move v0, v1

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v2, v1}, Lbf/am;->b(Z)V

    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->au()V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->onSearchRequested()Z

    goto :goto_1

    :sswitch_7
    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->a()V

    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v0, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v1

    iget-object v2, p0, Lbf/av;->b:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v2, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const/16 v3, 0x13c

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/ui/s;->a(ZI)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(I)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v1, p0, Lbf/av;->e:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;Z)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v1, p0, Lbf/av;->f:Lcom/google/googlenav/aA;

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    goto/16 :goto_0

    :sswitch_e
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->J()V

    goto/16 :goto_0

    :sswitch_f
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->K()V

    goto/16 :goto_0

    :sswitch_10
    iget-object v1, p0, Lbf/av;->f:Lcom/google/googlenav/aA;

    invoke-static {}, Lcom/google/googlenav/K;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "m"

    const-string v3, ""

    invoke-static {v2, v3}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->q(Z)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const-string v2, "offline"

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_0
        0x67 -> :sswitch_0
        0x7f10012a -> :sswitch_1
        0x7f1002f0 -> :sswitch_3
        0x7f100499 -> :sswitch_7
        0x7f10049c -> :sswitch_6
        0x7f1004ae -> :sswitch_7
        0x7f1004b7 -> :sswitch_10
        0x7f1004be -> :sswitch_5
        0x7f1004bf -> :sswitch_11
        0x7f1004c0 -> :sswitch_12
        0x7f1004c1 -> :sswitch_d
        0x7f1004c2 -> :sswitch_9
        0x7f1004c3 -> :sswitch_b
        0x7f1004c4 -> :sswitch_2
        0x7f1004c5 -> :sswitch_4
        0x7f1004c6 -> :sswitch_a
        0x7f1004c7 -> :sswitch_8
        0x7f1004c8 -> :sswitch_c
        0x7f1004d2 -> :sswitch_e
        0x7f1004d3 -> :sswitch_f
    .end sparse-switch
.end method
