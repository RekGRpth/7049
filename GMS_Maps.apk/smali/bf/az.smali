.class public Lbf/az;
.super Lbf/h;
.source "SourceFile"


# instance fields
.field d:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/dg;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/h;-><init>(Lbf/i;)V

    iput-object p1, p0, Lbf/az;->d:Lcom/google/googlenav/ui/wizard/dg;

    return-void
.end method


# virtual methods
.method public a(LaN/B;)Lcom/google/googlenav/ui/view/d;
    .locals 4

    invoke-virtual {p0}, Lbf/az;->b()Lcom/google/googlenav/ui/view/d;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbf/az;->b:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->f()Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lbf/az;->d:Lcom/google/googlenav/ui/wizard/dg;

    invoke-virtual {v0, p1, v2, v1, v3}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LaN/B;Landroid/view/View;Lcom/google/googlenav/ui/view/d;Lcom/google/googlenav/ui/view/c;)V

    :cond_0
    return-object v1
.end method

.method public b()Lcom/google/googlenav/ui/view/d;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lbf/az;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x50d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->e:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    new-array v2, v5, [Lcom/google/googlenav/ui/aW;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/az;->b(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v0, v4}, Lbf/az;->c(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, v0, v4, v4}, Lbf/az;->a(Landroid/view/View;Lam/f;Lam/f;)V

    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Lam/f;)V

    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    invoke-virtual {p0, v0, v5}, Lbf/az;->a(Landroid/view/View;Z)V

    sget-object v0, Lbf/az;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/az;->d:Lcom/google/googlenav/ui/wizard/dg;

    invoke-virtual {p0, v0, v1}, Lbf/az;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0
.end method
