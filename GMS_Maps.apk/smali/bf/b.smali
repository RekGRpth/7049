.class Lbf/b;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/googlenav/F;

.field final synthetic f:Lbf/g;

.field final synthetic g:Lcom/google/googlenav/android/aa;

.field final synthetic h:Lbf/a;


# direct methods
.method constructor <init>(Lbf/a;Las/c;ILcom/google/googlenav/F;Lbf/g;Lcom/google/googlenav/android/aa;)V
    .locals 0

    iput-object p1, p0, Lbf/b;->h:Lbf/a;

    iput p3, p0, Lbf/b;->a:I

    iput-object p4, p0, Lbf/b;->b:Lcom/google/googlenav/F;

    iput-object p5, p0, Lbf/b;->f:Lbf/g;

    iput-object p6, p0, Lbf/b;->g:Lcom/google/googlenav/android/aa;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ah()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lbf/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0xf

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ay()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v1, "SAVED_BGFS_EXTRA_3"

    invoke-direct {p0, v0, v1, p3}, Lbf/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-static {v1, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p3, v0, p2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "-Error saving featureSet"

    invoke-static {p3, v0, v1}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "-OOME saving featureSet"

    invoke-static {p3, v0, v1}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SAVED_BGFS_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lbf/b;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lbf/b;->b:Lcom/google/googlenav/F;

    if-nez v0, :cond_1

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    iget-object v0, p0, Lbf/b;->h:Lbf/a;

    invoke-static {v0}, Lbf/a;->a(Lbf/a;)Ljava/util/Map;

    move-result-object v0

    iget v1, p0, Lbf/b;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v0, p0, Lbf/b;->f:Lbf/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/b;->g:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/b;->g:Lcom/google/googlenav/android/aa;

    new-instance v2, Lbf/c;

    invoke-direct {v2, p0}, Lbf/c;-><init>(Lbf/b;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lbf/b;->a:I

    packed-switch v0, :pswitch_data_0

    :goto_2
    iget-object v0, p0, Lbf/b;->h:Lbf/a;

    invoke-static {v0}, Lbf/a;->a(Lbf/a;)Ljava/util/Map;

    move-result-object v0

    iget v1, p0, Lbf/b;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lbf/b;->b:Lcom/google/googlenav/F;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lbf/b;->b:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/aZ;

    invoke-direct {p0, v0, v2, v1}, Lbf/b;->a(Lcom/google/googlenav/aZ;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lbf/b;->f:Lbf/g;

    invoke-interface {v0}, Lbf/g;->a()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
