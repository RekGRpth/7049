.class public Lbf/bH;
.super Lbf/m;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    return-void
.end method

.method static synthetic a(Lbf/bH;)V
    .locals 0

    invoke-direct {p0}, Lbf/bH;->bG()V

    return-void
.end method

.method private bG()V
    .locals 3

    iget-object v0, p0, Lbf/bH;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/16 v1, 0x26

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bH;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-super {p0}, Lbf/m;->m()V

    return-void
.end method

.method private c()V
    .locals 1

    invoke-virtual {p0}, Lbf/bH;->a()V

    iget-object v0, p0, Lbf/bH;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/i;->aR()V

    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/bH;->d:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    return-void
.end method

.method private f()Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cp;

    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lbf/bH;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    iput-object p1, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    check-cast p1, Lcom/google/googlenav/bN;

    invoke-virtual {p1}, Lcom/google/googlenav/bN;->a()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p0, v0}, Lbf/bH;->b(I)V

    invoke-direct {p0}, Lbf/bH;->d()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 2

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lbf/bH;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x25

    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/bH;->b(ILjava/lang/Object;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/16 v0, 0x27

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public aF()I
    .locals 1

    const v0, 0x7f11000d

    return v0
.end method

.method protected aT()Z
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/bH;->b(B)V

    invoke-super {p0}, Lbf/m;->aT()Z

    move-result v0

    return v0
.end method

.method public aW()V
    .locals 0

    invoke-super {p0}, Lbf/m;->aW()V

    invoke-virtual {p0}, Lbf/bH;->aR()V

    return-void
.end method

.method public aX()V
    .locals 0

    invoke-virtual {p0}, Lbf/bH;->aQ()V

    invoke-super {p0}, Lbf/m;->aX()V

    return-void
.end method

.method public aa()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected aq()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/cu;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/cu;-><init>(Lbf/bH;)V

    iput-object v0, p0, Lbf/bH;->r:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x18

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, Lbf/bH;->R()V

    iget-object v0, p0, Lbf/bH;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->n(Lbf/i;)V

    invoke-direct {p0}, Lbf/bH;->d()V

    return-void
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(Lat/a;)Z
    .locals 6

    const/16 v5, 0x36

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/bH;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lat/a;->b()I

    move-result v3

    if-eq v3, v5, :cond_2

    const/16 v0, 0x34

    if-ne v3, v0, :cond_0

    :cond_2
    iget-object v0, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0}, Lbf/bH;->ac()Z

    move-result v4

    if-eqz v4, :cond_3

    if-ne v3, v5, :cond_5

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_3
    :goto_1
    if-gez v0, :cond_4

    move v0, v1

    :cond_4
    iget-object v1, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->a(B)V

    invoke-virtual {p0, v0}, Lbf/bH;->b(I)V

    invoke-virtual {p0}, Lbf/bH;->an()Z

    invoke-virtual {p0, v2}, Lbf/bH;->k(Z)V

    move v1, v2

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 12

    iget-object v0, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->b()Lcom/google/googlenav/bV;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/googlenav/cp;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/googlenav/cp;

    invoke-virtual {v4}, Lcom/google/googlenav/bV;->c()I

    move-result v8

    invoke-virtual {v4}, Lcom/google/googlenav/bV;->d()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->h()Lbf/bG;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bG;->a()Lcom/google/googlenav/cm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->l()I

    move-result v10

    if-gez v9, :cond_0

    const/4 v0, 0x2

    new-array v7, v0, [Lcom/google/googlenav/ui/aH;

    const/4 v9, 0x0

    new-instance v0, Lcom/google/googlenav/ui/aK;

    const/4 v2, 0x0

    add-int/lit8 v3, v8, 0x1

    const v5, -0x777778

    const/4 v6, 0x1

    invoke-static {v6}, Lcom/google/googlenav/ui/aK;->a(Z)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aK;-><init>([Lcom/google/googlenav/cp;IILcom/google/googlenav/bV;II)V

    aput-object v0, v7, v9

    const/4 v9, 0x1

    new-instance v0, Lcom/google/googlenav/ui/aK;

    invoke-virtual {v4}, Lcom/google/googlenav/bV;->a()I

    move-result v3

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/googlenav/ui/aK;->a(Z)I

    move-result v6

    move v2, v8

    move v5, v10

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aK;-><init>([Lcom/google/googlenav/cp;IILcom/google/googlenav/bV;II)V

    aput-object v0, v7, v9

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x3

    new-array v7, v0, [Lcom/google/googlenav/ui/aH;

    const/4 v11, 0x0

    new-instance v0, Lcom/google/googlenav/ui/aK;

    const/4 v2, 0x0

    add-int/lit8 v3, v8, 0x1

    const v5, -0x777778

    const/4 v6, 0x1

    invoke-static {v6}, Lcom/google/googlenav/ui/aK;->a(Z)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aK;-><init>([Lcom/google/googlenav/cp;IILcom/google/googlenav/bV;II)V

    aput-object v0, v7, v11

    const/4 v11, 0x1

    new-instance v0, Lcom/google/googlenav/ui/aK;

    add-int/lit8 v3, v9, 0x1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/googlenav/ui/aK;->a(Z)I

    move-result v6

    move v2, v8

    move v5, v10

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aK;-><init>([Lcom/google/googlenav/cp;IILcom/google/googlenav/bV;II)V

    aput-object v0, v7, v11

    const/4 v8, 0x2

    new-instance v0, Lcom/google/googlenav/ui/aK;

    invoke-virtual {v4}, Lcom/google/googlenav/bV;->a()I

    move-result v3

    const v5, -0x777778

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/googlenav/ui/aK;->a(Z)I

    move-result v6

    move v2, v9

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aK;-><init>([Lcom/google/googlenav/cp;IILcom/google/googlenav/bV;II)V

    aput-object v0, v7, v8

    move-object v0, v7

    goto :goto_0
.end method

.method protected f(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->a()I

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lbf/bH;->ae()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x26

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bH;->c(ILjava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lbf/bH;->aa()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/bH;->h()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/bH;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p0}, Lbf/bH;->a()V

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/Y;->a(I)V

    iget-object v0, p0, Lbf/bH;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;B)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, v2, v3}, Lbf/bH;->c(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, v2, v3}, Lbf/bH;->b(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v2, v3}, Lbf/bH;->a(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lbf/bH;->c()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x1f -> :sswitch_0
        0x25 -> :sswitch_1
        0x26 -> :sswitch_2
        0x27 -> :sswitch_3
    .end sparse-switch
.end method

.method protected k(Z)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbf/bH;->b(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/bH;->d:LaN/u;

    iget-object v1, p0, Lbf/bH;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/bH;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bH;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    goto :goto_0
.end method

.method protected m()V
    .locals 4

    invoke-direct {p0}, Lbf/bH;->f()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ch;

    new-instance v3, Lbf/bI;

    invoke-direct {v3, p0, v0}, Lbf/bI;-><init>(Lbf/bH;Lcom/google/googlenav/bZ;)V

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/ch;-><init>(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ci;)V

    invoke-virtual {v1, v2}, Law/h;->c(Law/g;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lbf/bH;->bG()V

    goto :goto_0
.end method
