.class public Lbf/bK;
.super Lbf/F;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ui/view/d;

.field private B:LaS/a;

.field private C:Lcom/google/googlenav/F;

.field private D:Z

.field private E:Lbm/j;

.field private final F:Lbf/bS;

.field private v:Z

.field private w:Z

.field private x:Lcom/google/googlenav/ui/view/r;

.field private y:Lcom/google/googlenav/ui/view/android/bF;

.field private z:Lcom/google/googlenav/ui/view/d;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/bK;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lbf/F;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    invoke-direct {p0}, Lbf/bK;->be()Lbf/bS;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->F:Lbf/bS;

    return-void
.end method

.method static synthetic a(Lbf/bK;Lcom/google/googlenav/ui/view/d;)Lcom/google/googlenav/ui/view/d;
    .locals 0

    iput-object p1, p0, Lbf/bK;->z:Lcom/google/googlenav/ui/view/d;

    return-object p1
.end method

.method static synthetic a(Lbf/bK;)V
    .locals 0

    invoke-direct {p0}, Lbf/bK;->bl()V

    return-void
.end method

.method static synthetic a(Lbf/bK;Landroid/content/res/Configuration;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/bK;->b(Landroid/content/res/Configuration;)V

    return-void
.end method

.method private b(Landroid/content/res/Configuration;)V
    .locals 3

    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/r;->a(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/r;->k()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(II)V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    iget-object v1, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/r;->h()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/i;)V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->b()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lbf/bK;)V
    .locals 0

    invoke-direct {p0}, Lbf/bK;->bf()V

    return-void
.end method

.method private be()Lbf/bS;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbf/bT;

    invoke-direct {v0, p0}, Lbf/bT;-><init>(Lbf/bK;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lbf/bQ;

    invoke-direct {v0, p0}, Lbf/bQ;-><init>(Lbf/bK;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lbf/bO;

    invoke-direct {v0, p0}, Lbf/bO;-><init>(Lbf/bK;)V

    goto :goto_0
.end method

.method private bf()V
    .locals 3

    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->a()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->i()Z

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/r;->k()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(II)V

    invoke-direct {p0}, Lbf/bK;->bo()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/bK;->b(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    iget-object v1, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/r;->h()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/i;)V

    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->c()V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    iget-object v1, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/r;->g()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/h;)V

    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->e()V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->b()V

    return-void
.end method

.method private bg()Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/bK;->f()V

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lbf/bK;->l()V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->o()V

    goto :goto_0
.end method

.method private bh()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->s()V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->l()V

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->d()V

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->f()V

    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->b()V

    iput-boolean v1, p0, Lbf/bK;->v:Z

    iput-boolean v1, p0, Lbf/bK;->w:Z

    return-void
.end method

.method private bi()Lax/w;
    .locals 1

    iget-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    return-object v0
.end method

.method private bj()V
    .locals 0

    return-void
.end method

.method private bk()Z
    .locals 2

    iget-object v0, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ad()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bl()V
    .locals 4

    const/4 v3, 0x1

    iput-boolean v3, p0, Lbf/bK;->D:Z

    new-instance v0, Lbf/bM;

    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    iget-object v2, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v3}, Lbf/bM;-><init>(Lbf/bK;Las/c;Lcom/google/googlenav/android/aa;Z)V

    iput-object v0, p0, Lbf/bK;->E:Lbm/j;

    iget-object v0, p0, Lbf/bK;->E:Lbm/j;

    invoke-virtual {v0}, Lbm/j;->g()V

    return-void
.end method

.method private bm()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/16 v1, 0x14

    new-instance v2, Lbf/bN;

    invoke-direct {v2, p0}, Lbf/bN;-><init>(Lbf/bK;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->A:Lcom/google/googlenav/ui/view/d;

    return-void
.end method

.method private bn()V
    .locals 1

    iget-object v0, p0, Lbf/bK;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bK;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_0
    return-void
.end method

.method private bo()Landroid/content/res/Configuration;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lbf/bK;)V
    .locals 0

    invoke-direct {p0}, Lbf/bK;->bm()V

    return-void
.end method

.method static synthetic d(Lbf/bK;)Lcom/google/googlenav/ui/view/android/bF;
    .locals 1

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    return-object v0
.end method

.method static synthetic e(Lbf/bK;)Lcom/google/googlenav/ui/view/r;
    .locals 1

    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    return-object v0
.end method

.method static synthetic f(Lbf/bK;)Lcom/google/googlenav/ui/view/d;
    .locals 1

    iget-object v0, p0, Lbf/bK;->z:Lcom/google/googlenav/ui/view/d;

    return-object v0
.end method

.method static synthetic g(Lbf/bK;)V
    .locals 0

    invoke-direct {p0}, Lbf/bK;->bn()V

    return-void
.end method

.method static synthetic h(Lbf/bK;)Landroid/content/res/Configuration;
    .locals 1

    invoke-direct {p0}, Lbf/bK;->bo()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method private h(I)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/bK;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-ltz p1, :cond_1

    iget-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, Lbf/bK;->a(IZZ)V

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lbf/bK;->b(ILjava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/bK;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected T()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x4d3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bK;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LT/a;

    invoke-virtual {v0, p1}, LT/a;->a(Landroid/content/res/Configuration;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lbf/bK;->b(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 2

    iput-object p1, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    invoke-interface {p1}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bK;->b(I)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 1

    invoke-virtual {p0}, Lbf/bK;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/b;->b(Ljava/io/DataOutput;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lbf/bK;->bk()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bK;->c(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lbf/bK;->bk()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "sb"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    :cond_1
    :sswitch_2
    invoke-direct {p0}, Lbf/bK;->bk()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p2}, Lbf/bK;->h(I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lbf/bK;->W()V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lbf/bK;->d()V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lbf/bK;->bj()V

    goto :goto_0

    :sswitch_6
    iget-object v1, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v1}, LaS/a;->r()V

    goto :goto_0

    :sswitch_7
    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->h()V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p0}, Lbf/bK;->f()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xf1 -> :sswitch_0
        0x3f9 -> :sswitch_3
        0xb54 -> :sswitch_4
        0xb55 -> :sswitch_8
        0xb56 -> :sswitch_5
        0xb57 -> :sswitch_6
        0xb58 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    const/16 v0, 0x1b

    invoke-super {p0, p1, v0}, Lbf/F;->a(Lcom/google/googlenav/ui/view/t;I)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    invoke-static {p1, v0}, Lax/b;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .locals 1

    const v0, 0x7f110022

    return v0
.end method

.method public aK()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lbf/bK;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x4a7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .locals 2

    iget-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    iput-object v0, p0, Lbf/bK;->C:Lcom/google/googlenav/F;

    invoke-virtual {p0}, Lbf/bK;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aJ()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    iget-object v0, p0, Lbf/bK;->C:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lax/w;->c(Z)V

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/r;->l()Lcom/google/googlenav/ui/view/r;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->B:LaS/a;

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->s()V

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-direct {p0}, Lbf/bK;->bi()Lax/w;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, LaS/a;->a(Lax/w;Lcom/google/googlenav/ui/e;)V

    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->a()V

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 0

    invoke-direct {p0}, Lbf/bK;->bh()V

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    return-void
.end method

.method public aV()V
    .locals 1

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->l()V

    return-void
.end method

.method public aW()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Lbf/F;->aW()V

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/googlenav/ui/e;)V

    iget-boolean v0, p0, Lbf/bK;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lbf/bK;->v:Z

    iput-boolean v1, p0, Lbf/bK;->w:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->k()V

    iput-boolean v1, p0, Lbf/bK;->v:Z

    goto :goto_0
.end method

.method public aX()V
    .locals 1

    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->m()V

    invoke-super {p0}, Lbf/F;->aX()V

    return-void
.end method

.method protected ap()V
    .locals 0

    return-void
.end method

.method protected aq()V
    .locals 1

    new-instance v0, LT/a;

    invoke-direct {v0, p0}, LT/a;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lbf/bK;->r:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x15

    return v0
.end method

.method public c()Z
    .locals 2

    iget-boolean v0, p0, Lbf/bK;->w:Z

    iget-object v1, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v1}, LaS/a;->h()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lbf/bK;->w:Z

    iget-boolean v0, p0, Lbf/bK;->w:Z

    return v0
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    iget-object v0, p0, Lbf/bK;->C:Lcom/google/googlenav/F;

    check-cast v0, Lax/b;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/b;B)V

    return-void
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 7

    const/4 v0, 0x0

    invoke-direct {p0}, Lbf/bK;->bi()Lax/w;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v3}, Lax/w;->ae()I

    move-result v4

    const/4 v1, 0x1

    if-lt v4, v1, :cond_0

    add-int/lit8 v0, v4, -0x1

    new-array v1, v0, [Lcom/google/googlenav/ui/aH;

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_2

    new-instance v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->b(Z)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {v0, v3, v5, v2, v6}, Lcom/google/googlenav/ui/aJ;-><init>(Lax/w;III)V

    aput-object v0, v1, v2

    aget-object v0, v1, v2

    check-cast v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->a(Z)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/aJ;->a(I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public f()V
    .locals 8

    iget-boolean v0, p0, Lbf/bK;->D:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x494

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x493

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lbf/bL;

    invoke-direct {v7, p0}, Lbf/bL;-><init>(Lbf/bK;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_0
.end method

.method protected f(Lat/a;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "m"

    const-string v2, "#"

    invoke-virtual {p0, v1, v2}, Lbf/bK;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-direct {p0, v1}, Lbf/bK;->h(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "l"

    const-string v3, "#"

    invoke-virtual {p0, v2, v3}, Lbf/bK;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bK;->c(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/bK;->ai()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lbf/bK;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    invoke-virtual {p0, v2}, Lbf/bK;->b(LaN/B;)I

    move-result v2

    if-ltz v2, :cond_3

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_0

    :cond_3
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    invoke-direct {p0}, Lbf/bK;->bg()Z

    move-result v0

    goto :goto_0

    :cond_4
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v2

    invoke-virtual {p1}, Lat/a;->a()I

    move-result v3

    invoke-virtual {v2, v3}, LaS/a;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public h()V
    .locals 0

    invoke-direct {p0}, Lbf/bK;->bg()Z

    return-void
.end method

.method protected l()V
    .locals 1

    invoke-super {p0}, Lbf/F;->l()V

    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->c()V

    return-void
.end method

.method protected n()V
    .locals 1

    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->d()V

    invoke-super {p0}, Lbf/F;->n()V

    return-void
.end method

.method public q()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/r;->l()Lcom/google/googlenav/ui/view/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->k()I

    move-result v0

    return v0
.end method
