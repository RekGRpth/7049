.class public Lbf/be;
.super Lbf/l;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/m;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/l;-><init>(Lbf/i;)V

    return-void
.end method

.method private a()Lbf/m;
    .locals 1

    iget-object v0, p0, Lbf/be;->a:Lbf/i;

    check-cast v0, Lbf/m;

    return-object v0
.end method

.method static synthetic a(Lbf/be;)Lbf/m;
    .locals 1

    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbf/be;Lcom/google/googlenav/ai;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lbf/be;->e(Lcom/google/googlenav/ai;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lbf/be;->a:Lbf/i;

    invoke-virtual {v0, v2}, Lbf/i;->f(Z)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/aV;

    iget-object v0, p0, Lbf/be;->a:Lbf/i;

    check-cast v0, Lbf/m;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/view/android/aV;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bn;->b(Z)V

    return-object v1
.end method

.method private c(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/dialog/r;
    .locals 2

    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    new-instance v1, Lbf/bf;

    invoke-direct {v1, p0, v0, p1}, Lbf/bf;-><init>(Lbf/be;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ai;)V

    return-object v1
.end method

.method private d(Lcom/google/googlenav/ai;)Lbf/bg;
    .locals 2

    new-instance v0, Lbf/bg;

    iget-object v1, p0, Lbf/be;->a:Lbf/i;

    invoke-direct {v0, p0, v1, p1}, Lbf/bg;-><init>(Lbf/be;Lbf/i;Lcom/google/googlenav/ai;)V

    return-object v0
.end method

.method private e(Lcom/google/googlenav/ai;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    invoke-static {p1, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Lbf/i;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f(Lcom/google/googlenav/ai;)Lbf/bj;
    .locals 3

    new-instance v0, Lbf/bj;

    iget-object v1, p0, Lbf/be;->a:Lbf/i;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbf/bj;-><init>(Lbf/be;Lbf/i;[Lcom/google/googlenav/ai;)V

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/ui/view/android/S;
    .locals 3

    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    invoke-virtual {v0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    sparse-switch p1, :sswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-direct {p0, v0}, Lbf/be;->b(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_1
    invoke-virtual {p0, v0}, Lbf/be;->a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    invoke-direct {p0, v0}, Lbf/be;->d(Lcom/google/googlenav/ai;)Lbf/bg;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    invoke-direct {p0, v0}, Lbf/be;->f(Lcom/google/googlenav/ai;)Lbf/bj;

    move-result-object v0

    goto :goto_0

    :sswitch_4
    invoke-direct {p0, v0}, Lbf/be;->c(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/dialog/r;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x7 -> :sswitch_3
        0x14 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;
    .locals 4

    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/a;

    invoke-virtual {v1, p1}, Lbf/m;->j(Lcom/google/googlenav/ai;)Z

    move-result v2

    invoke-virtual {v1, p1}, Lbf/m;->k(Lcom/google/googlenav/ai;)Z

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/view/dialog/a;-><init>(Lcom/google/googlenav/ai;Lbf/m;ZZ)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/android/aV;

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/ui/view/android/aV;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/ui/view/android/bn;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    goto :goto_0
.end method
