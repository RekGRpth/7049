.class final Lbf/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/x;


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lbf/bw;->a:I

    iput v0, p0, Lbf/bw;->b:I

    iput v0, p0, Lbf/bw;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lbf/bl;)V
    .locals 0

    invoke-direct {p0}, Lbf/bw;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbf/bw;->a:I

    return v0
.end method

.method public a(Lcom/google/googlenav/ai;)Ljava/lang/Iterable;
    .locals 8

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v2, v1

    :goto_0
    const/4 v0, 0x3

    if-gt v2, v0, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/googlenav/ai;->g(I)[Lcom/google/googlenav/aq;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget-object v5, v4, v0

    invoke-virtual {v5}, Lcom/google/googlenav/aq;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    new-instance v6, Lcom/google/googlenav/ui/bs;

    sget v7, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v6, v5, v7}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->M()Lcom/google/googlenav/aq;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v2, Lcom/google/googlenav/ui/bs;

    sget v4, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v2, v1, v4}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v0}, Lcom/google/googlenav/aq;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lbf/bw;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbf/bw;->a:I

    :goto_2
    return-object v3

    :cond_5
    iget v0, p0, Lbf/bw;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbf/bw;->b:I

    goto :goto_2

    :cond_6
    iget v0, p0, Lbf/bw;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbf/bw;->c:I

    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/googlenav/ai;

    invoke-virtual {p0, p1}, Lbf/bw;->a(Lcom/google/googlenav/ai;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lbf/bw;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lbf/bw;->c:I

    return v0
.end method
