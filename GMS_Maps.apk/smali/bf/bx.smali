.class public Lbf/bx;
.super Lbf/y;
.source "SourceFile"


# instance fields
.field private C:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .locals 7

    new-instance v5, Lcom/google/googlenav/layer/m;

    const-string v0, "s"

    invoke-direct {v5, v0}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LaN/p;->a()LaN/D;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/o;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/bx;->C:Z

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->a()LaB/a;

    return-void
.end method


# virtual methods
.method protected X()Z
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lbf/y;->X()Z

    iget-boolean v0, p0, Lbf/bx;->C:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bx;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bx;->b(LaN/B;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lbf/bx;->b(B)V

    invoke-virtual {p0}, Lbf/bx;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->a(I)V

    iget-object v0, p0, Lbf/bx;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->e(Lbf/i;)V

    invoke-virtual {p0}, Lbf/bx;->an()Z

    :cond_0
    iput-boolean v2, p0, Lbf/bx;->C:Z

    :cond_1
    return v2
.end method

.method public Y()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bx;->e(Z)V

    return-void
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aT()Z
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    iget-object v0, p0, Lbf/bx;->c:LaN/p;

    invoke-virtual {v0, p0}, LaN/p;->a(LaN/m;)V

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    invoke-virtual {p0}, Lbf/bx;->f()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    iget-object v0, p0, Lbf/bx;->c:LaN/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/m;)V

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method protected bG()V
    .locals 0

    return-void
.end method

.method public g(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lbf/y;->g(I)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
