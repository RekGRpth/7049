.class Lbf/d;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lbf/f;

.field final synthetic f:Lcom/google/googlenav/android/aa;

.field final synthetic g:Lbf/a;


# direct methods
.method constructor <init>(Lbf/a;Las/c;ILbf/f;Lcom/google/googlenav/android/aa;)V
    .locals 0

    iput-object p1, p0, Lbf/d;->g:Lbf/a;

    iput p3, p0, Lbf/d;->a:I

    iput-object p4, p0, Lbf/d;->b:Lbf/f;

    iput-object p5, p0, Lbf/d;->f:Lcom/google/googlenav/android/aa;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/j;Ljava/io/DataInput;)Lcom/google/googlenav/F;
    .locals 5

    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-direct {v1}, Lcom/google/googlenav/aZ;-><init>()V

    :try_start_0
    invoke-virtual {v1, p2}, Lcom/google/googlenav/aZ;->a(Ljava/io/DataInput;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "SAVED_BGFS_EXTRA_3"

    invoke-static {p1, v0}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ag;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v2, 0xf

    const-wide/high16 v3, -0x8000000000000000L

    invoke-static {v0, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/aZ;->a(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "-Error loading featureSet"

    invoke-static {p1, v0, v2}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "-Error saving featureSet"

    invoke-static {p1, v0, v2}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/aZ;->a(J)V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SAVED_BGFS_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lbf/d;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-static {v1, v2}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lbf/d;->a:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lbf/d;->g:Lbf/a;

    invoke-static {v1}, Lbf/a;->a(Lbf/a;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lbf/d;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lbf/d;->g:Lbf/a;

    iget-object v2, p0, Lbf/d;->b:Lbf/f;

    iget-object v3, p0, Lbf/d;->f:Lcom/google/googlenav/android/aa;

    invoke-static {v1, v0, v2, v3}, Lbf/a;->a(Lbf/a;Lcom/google/googlenav/F;Lbf/f;Lcom/google/googlenav/android/aa;)V

    return-void

    :pswitch_0
    invoke-direct {p0, v1, v2}, Lbf/d;->a(Lcom/google/googlenav/common/io/j;Ljava/io/DataInput;)Lcom/google/googlenav/F;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
