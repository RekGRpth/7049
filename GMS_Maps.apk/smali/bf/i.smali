.class public abstract Lbf/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/ui/Y;
.implements Lcom/google/googlenav/ui/e;
.implements Lcom/google/googlenav/ui/view/c;
.implements Lo/C;


# instance fields
.field private A:Lbf/j;

.field private B:I

.field private C:B

.field private D:Lcom/google/googlenav/settings/e;

.field protected final a:Lcom/google/googlenav/ui/bi;

.field protected final b:Lcom/google/googlenav/ui/s;

.field protected final c:LaN/p;

.field protected final d:LaN/u;

.field protected final e:Landroid/graphics/Point;

.field protected f:Lcom/google/googlenav/F;

.field protected g:Lcom/google/googlenav/ui/view/d;

.field protected h:Lcom/google/googlenav/E;

.field protected final i:Lcom/google/googlenav/ui/X;

.field protected j:Lcom/google/googlenav/ui/view/d;

.field protected k:Lcom/google/googlenav/ui/view/d;

.field protected l:Lcom/google/googlenav/ui/view/d;

.field protected m:Lcom/google/googlenav/ui/view/d;

.field protected n:Lam/f;

.field protected o:Z

.field protected p:Z

.field protected q:[Ljava/lang/Boolean;

.field protected r:Lcom/google/googlenav/ui/view/android/aL;

.field protected s:Lbf/w;

.field protected t:Lbh/a;

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lbf/k;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    iput v3, p0, Lbf/i;->u:I

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Boolean;

    iput-object v1, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    iput-boolean v2, p0, Lbf/i;->v:Z

    iput-boolean v2, p0, Lbf/i;->w:Z

    iput-boolean v4, p0, Lbf/i;->x:Z

    iput-boolean v2, p0, Lbf/i;->y:Z

    iput v3, p0, Lbf/i;->B:I

    iput-byte v4, p0, Lbf/i;->C:B

    iput-object v0, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    iput-object p1, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    iput-object p2, p0, Lbf/i;->c:LaN/p;

    iput-object p3, p0, Lbf/i;->d:LaN/u;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    iput-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    iput-object p4, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-static {}, Lbf/al;->a()Lbf/al;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    :cond_0
    invoke-virtual {v1, p0, v0}, Lbf/al;->a(Lbf/i;LaB/s;)Lbf/w;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->s:Lbf/w;

    invoke-virtual {p0}, Lbf/i;->i()Lbh/a;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->t:Lbh/a;

    iput-object p5, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    :cond_0
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    :cond_1
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/r;III)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p4}, Lbf/i;->c(I)Lcom/google/googlenav/e;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/r;II)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    return-void
.end method

.method private b(LaN/B;I)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Lbf/i;->h(I)I

    move-result v2

    iget-object v3, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v3

    invoke-virtual {v3, v2, p1, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v3

    invoke-interface {v3}, LaH/m;->g()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v3

    invoke-interface {v3}, LaH/m;->r()LaH/h;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v4, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v4

    invoke-virtual {v3}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v4, v2, v3, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private b(LaN/B;LaN/B;I)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p3, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p3}, Lbf/i;->h(I)I

    move-result v2

    iget-object v3, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v3

    invoke-virtual {v3, v2, p1, v1}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v3

    invoke-virtual {v3, v2, p2, v1}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private b(Lcom/google/googlenav/E;LaN/B;)Z
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->d()Lcom/google/googlenav/ui/aR;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/googlenav/ai;

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v0}, LaN/g;->a()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iget-object v1, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-interface {v1}, Lcom/google/googlenav/ui/X;->d()Lcom/google/googlenav/ui/aR;

    move-result-object v1

    check-cast v0, LaN/M;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aI;LaN/Y;)[J

    move-result-object v0

    invoke-static {v0, p2, v2}, LaN/M;->a([JLaN/B;LaN/Y;)Z

    move-result v0

    goto :goto_0

    :cond_3
    invoke-interface {v0}, LaN/g;->a()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    iget-object v1, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-interface {v1}, Lcom/google/googlenav/ui/X;->d()Lcom/google/googlenav/ui/aR;

    move-result-object v1

    check-cast v0, LaN/N;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aI;LaN/Y;)[J

    move-result-object v0

    invoke-static {v0, p2, v2}, LaN/N;->b([JLaN/B;LaN/Y;)Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private be()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/i;->aY()Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lbf/i;->W()V

    invoke-virtual {p0}, Lbf/i;->aY()Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->W()V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    iput-object v1, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    :cond_0
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    iput-object v1, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    :cond_1
    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    :cond_0
    return-void
.end method

.method private f(Lcom/google/googlenav/ui/r;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/i;->O()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lbf/i;->i(Lcom/google/googlenav/ui/r;)V

    :cond_2
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lbf/i;->g(Lcom/google/googlenav/ui/r;)V

    :cond_3
    invoke-virtual {p0}, Lbf/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lbf/i;->h(Lcom/google/googlenav/ui/r;)V

    goto :goto_0
.end method

.method private g(Lcom/google/googlenav/ui/r;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_0
    return-void
.end method

.method private g(Lcom/google/googlenav/E;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/googlenav/ai;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bq()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(I)I
    .locals 1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private h(Lcom/google/googlenav/ui/r;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_0
    return-void
.end method

.method private i(Lcom/google/googlenav/ui/r;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_0
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected A()V
    .locals 1

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    return-void
.end method

.method protected B()V
    .locals 1

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    return-void
.end method

.method protected C()V
    .locals 3

    iget-boolean v0, p0, Lbf/i;->o:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/i;->S()V

    :goto_1
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p0}, Lbf/i;->V()V

    goto :goto_1

    :pswitch_0
    invoke-virtual {p0}, Lbf/i;->r()V

    invoke-virtual {p0}, Lbf/i;->al()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lbf/i;->r()V

    invoke-virtual {p0}, Lbf/i;->an()Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lbf/i;->an()Z

    invoke-virtual {p0}, Lbf/i;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->r()V

    invoke-virtual {p0}, Lbf/i;->ap()V

    invoke-virtual {p0}, Lbf/i;->A()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lbf/i;->an()Z

    invoke-virtual {p0}, Lbf/i;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->r()V

    invoke-virtual {p0}, Lbf/i;->aq()V

    invoke-virtual {p0}, Lbf/i;->B()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected D()V
    .locals 0

    return-void
.end method

.method protected E()V
    .locals 0

    return-void
.end method

.method public F()I
    .locals 2

    invoke-virtual {p0}, Lbf/i;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0}, Lbf/i;->x()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected G()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public H()Z
    .locals 1

    iget-boolean v0, p0, Lbf/i;->v:Z

    return v0
.end method

.method public I()Z
    .locals 1

    iget-boolean v0, p0, Lbf/i;->w:Z

    return v0
.end method

.method public J()V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lbf/i;->d(Z)V

    invoke-virtual {p0}, Lbf/i;->l()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v1}, Lbf/i;->e(Z)V

    invoke-virtual {p0}, Lbf/i;->m()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_0
.end method

.method protected K()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lbf/i;->d:LaN/u;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    invoke-virtual {v1, v0}, LaN/u;->a(Lo/D;)V

    goto :goto_0
.end method

.method public L()Z
    .locals 1

    invoke-virtual {p0}, Lbf/i;->O()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Z
    .locals 1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Z
    .locals 2

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected O()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected P()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lbf/i;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lbf/i;->ax()Z

    move-result v0

    goto :goto_0
.end method

.method protected Q()Z
    .locals 1

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ax()Z

    move-result v0

    return v0
.end method

.method protected R()V
    .locals 1

    iget-object v0, p0, Lbf/i;->z:Lbf/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->z:Lbf/k;

    invoke-interface {v0, p0}, Lbf/k;->c(Lbf/i;)V

    :cond_0
    return-void
.end method

.method protected S()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {p0}, Lbf/i;->U()I

    move-result v1

    invoke-virtual {p0}, Lbf/i;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    return-void
.end method

.method protected T()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected U()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method protected V()V
    .locals 1

    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    :cond_0
    return-void
.end method

.method protected W()V
    .locals 2

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    :goto_0
    invoke-virtual {p0}, Lbf/i;->r()V

    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->S()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lbf/i;->V()V

    goto :goto_1
.end method

.method protected X()Z
    .locals 1

    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0}, Lbh/a;->a()Z

    move-result v0

    return v0
.end method

.method public Y()V
    .locals 0

    return-void
.end method

.method public Z()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    invoke-virtual {p0}, Lbf/i;->al()V

    invoke-virtual {p0}, Lbf/i;->r()V

    return-void
.end method

.method protected a(Lcom/google/googlenav/E;LaN/B;)J
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lbf/i;->d(Lcom/google/googlenav/E;)I

    move-result v1

    invoke-virtual {p0, p1}, Lbf/i;->e(Lcom/google/googlenav/E;)I

    move-result v4

    invoke-virtual {p0, p1}, Lbf/i;->f(Lcom/google/googlenav/E;)I

    move-result v5

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    :goto_0
    if-nez v2, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0, v2, v1, v3}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    :cond_1
    invoke-virtual {p2, v0, v3}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v0

    :goto_1
    return-wide v0

    :pswitch_1
    iget-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/bi;->b(B)I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v2, v1, 0x2

    iget-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/bi;->a(B)I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    :pswitch_2
    neg-int v1, v5

    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    :pswitch_3
    neg-int v1, v4

    div-int/lit8 v2, v1, 0x2

    neg-int v1, v5

    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_2
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method protected a(LaN/B;IILaN/Y;)LaN/B;
    .locals 8

    mul-int v0, p2, p2

    mul-int v1, p3, p3

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    int-to-double v2, p3

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    iget-object v4, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v4}, LaN/u;->e()F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L

    mul-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v4, v0

    double-to-int v4, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    if-nez v4, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1, v4, v0, p4}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public a(LaN/B;LaN/Y;)LaN/B;
    .locals 11

    const/4 v9, 0x0

    invoke-virtual {p0}, Lbf/i;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v1

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v2

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {p0, v1}, Lbf/i;->e(Lcom/google/googlenav/E;)I

    move-result v3

    invoke-virtual {p0, v1}, Lbf/i;->f(Lcom/google/googlenav/E;)I

    move-result v4

    aget v5, v2, v9

    const/4 v6, 0x1

    aget v6, v2, v6

    invoke-virtual {p0, v1}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v7

    invoke-virtual {p0}, Lbf/i;->q()I

    move-result v8

    invoke-virtual {p0, v9}, Lbf/i;->c(Z)I

    move-result v9

    move-object v2, p1

    move-object v10, p2

    invoke-virtual/range {v0 .. v10}, LaN/u;->a(Lcom/google/googlenav/E;LaN/B;IIIIIIILaN/Y;)LaN/B;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method protected a(Lcom/google/googlenav/E;)LaN/B;
    .locals 3

    invoke-interface {p1}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LaN/g;->a()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    invoke-interface {v0}, LaN/g;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/E;LaN/Y;)LaN/B;
    .locals 3

    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lbf/i;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lbf/i;->p()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lbf/i;->a(LaN/B;IILaN/Y;)LaN/B;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public a(LaN/B;)Lo/D;
    .locals 1

    iget-object v0, p0, Lbf/i;->c:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v0

    return-object v0
.end method

.method public final a(B)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lbf/i;->l()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lbf/i;->m()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lbf/i;->Z()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lbf/i;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lbf/i;->m()V

    return-void
.end method

.method protected a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const-string v0, "0"

    :goto_1
    const/16 v2, 0x12

    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "a="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x3

    if-nez p4, :cond_0

    const/4 v1, 0x0

    :goto_2
    aput-object v1, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "dd"

    goto :goto_1

    :sswitch_2
    const-string v0, "ct"

    goto :goto_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0xb -> :sswitch_2
        0x10 -> :sswitch_2
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lam/f;)V
    .locals 0

    iput-object p1, p0, Lbf/i;->n:Lam/f;

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 0

    return-void
.end method

.method public a(Lbf/j;)V
    .locals 0

    iput-object p1, p0, Lbf/i;->A:Lbf/j;

    return-void
.end method

.method public a(Lbf/k;)V
    .locals 0

    iput-object p1, p0, Lbf/i;->z:Lbf/k;

    return-void
.end method

.method public final a(Lcom/google/googlenav/E;I)V
    .locals 1

    invoke-virtual {p0, p2}, Lbf/i;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/R;

    invoke-direct {v0}, Lcom/google/googlenav/R;-><init>()V

    invoke-virtual {p0, v0, p1, p2}, Lbf/i;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    invoke-virtual {v0}, Lcom/google/googlenav/R;->a()V

    :cond_0
    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/F;)V
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .locals 1

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(I)Lcom/google/googlenav/R;

    invoke-virtual {p1, p3}, Lcom/google/googlenav/R;->b(I)Lcom/google/googlenav/R;

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 1

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbf/i;->c(Lcom/google/googlenav/ui/r;)V

    :cond_0
    invoke-virtual {p0}, Lbf/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lbf/i;->f(Lcom/google/googlenav/ui/r;)V

    :cond_1
    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V
    .locals 5

    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbf/i;->c:LaN/p;

    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v1, v0, v2}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/googlenav/E;->c()B

    move-result v2

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/bi;->a(Lam/e;BII)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/F;)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p2}, Lcom/google/googlenav/F;->f()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-interface {p2, v0}, Lcom/google/googlenav/F;->c(I)I

    move-result v2

    invoke-interface {p2, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v2}, Lbf/i;->g(Lcom/google/googlenav/E;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1, v2}, Lbf/i;->a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lbf/i;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/R;

    invoke-direct {v0}, Lcom/google/googlenav/R;-><init>()V

    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lbf/i;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    invoke-virtual {v0}, Lcom/google/googlenav/R;->a()V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .locals 6

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->e()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1, p2}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v2

    int-to-long v4, p3

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    invoke-static {p0, v1, v0, v2, v3}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method protected a(ZLaN/Y;)V
    .locals 3

    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/googlenav/E;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0, p2}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/Y;)LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->R()V

    if-eqz p1, :cond_2

    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v2, v1, p2}, LaN/u;->d(LaN/B;LaN/Y;)V

    :cond_0
    :goto_0
    sget-object v1, LaE/d;->a:LaE/d;

    invoke-virtual {v1}, LaE/d;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-nez v1, :cond_3

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(LaN/B;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v2, v1}, LaN/u;->c(LaN/B;)V

    iget-object v1, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v1, p2}, LaN/u;->a(LaN/Y;)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/intersectionexplorer/d;->a(LaN/B;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected final a(CI)Z
    .locals 2

    new-instance v0, Lat/a;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p1, p2, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {p0, v0}, Lbf/i;->a(Lat/a;)Z

    move-result v0

    return v0
.end method

.method protected a(I)Z
    .locals 3

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbf/am;->a(Lbf/i;I)V

    const-string v0, "o"

    const-string v1, "c"

    invoke-virtual {p0}, Lbf/i;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v1, v2}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 2

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    check-cast p3, Ljava/lang/String;

    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LaunchUrl"

    const-string v1, "missing url"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p3}, Lbf/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public a(LaN/B;I)Z
    .locals 3

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p2

    if-nez v0, :cond_2

    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {p0, p1, p2}, Lbf/i;->b(LaN/B;I)Z

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v1, v0, p2

    :cond_2
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public a(LaN/B;LaN/B;I)Z
    .locals 3

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p3

    if-nez v0, :cond_2

    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {p0, p1, p2, p3}, Lbf/i;->b(LaN/B;LaN/B;I)Z

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v1, v0, p3

    :cond_2
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public a(LaN/g;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lat/a;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbf/i;->f(Lat/a;)Z

    move-result v0

    return v0
.end method

.method public a(Lat/b;)Z
    .locals 1

    invoke-virtual {p0}, Lbf/i;->aj()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lbf/i;->a(Lat/b;Z)Z

    move-result v0

    return v0
.end method

.method protected a(Lat/b;Z)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lat/b;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v3

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LaN/u;->b(II)LaN/B;

    move-result-object v2

    invoke-virtual {p0, v2}, Lbf/i;->b(LaN/B;)I

    move-result v2

    if-ltz v2, :cond_2

    invoke-virtual {p0, v2}, Lbf/i;->a(I)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lbf/i;->al()V

    goto :goto_0

    :cond_3
    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    const/4 v2, -0x1

    const-string v3, "c"

    const-string v4, "c"

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/i;->Z()V

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v1

    goto :goto_1

    :cond_5
    move v2, v0

    goto :goto_1
.end method

.method public a(Lbf/i;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v1

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v2

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/i;->aB()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z
    .locals 9

    const/4 v7, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-interface {p1}, Lcom/google/googlenav/E;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    invoke-virtual {p0, p1}, Lbf/i;->d(Lcom/google/googlenav/E;)I

    move-result v3

    invoke-virtual {p0, p1}, Lbf/i;->e(Lcom/google/googlenav/E;)I

    move-result v4

    invoke-virtual {p0, p1}, Lbf/i;->f(Lcom/google/googlenav/E;)I

    move-result v5

    const/4 v0, 0x3

    if-ne v3, v0, :cond_2

    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->b(B)I

    move-result v6

    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->a(B)I

    move-result v7

    :goto_1
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    move-object v1, p2

    move-object v8, p3

    invoke-virtual/range {v0 .. v8}, Lbf/am;->a(LaN/B;LaN/B;IIIIILam/e;)Z

    move-result v7

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 5

    const/16 v4, 0x36

    const/16 v3, 0x34

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_0

    new-instance v0, Lat/a;

    invoke-direct {v0, v3, v3, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {p0, v0}, Lbf/i;->e(Lat/a;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_1

    new-instance v0, Lat/a;

    invoke-direct {v0, v4, v4, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {p0, v0}, Lbf/i;->e(Lat/a;)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lbf/i;->be()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/i;->aY()Z

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_5

    iget-object v1, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->n(Z)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_3

    invoke-virtual {p0}, Lbf/i;->m()V

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const-string v2, "s"

    const-string v3, "c"

    invoke-virtual {p0}, Lbf/i;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aA()I
    .locals 4

    const/4 v0, 0x0

    move v2, v0

    move v1, v0

    :goto_0
    const/4 v0, 0x4

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_1
    mul-int/lit8 v3, v2, 0x2

    shl-int/2addr v0, v3

    or-int/2addr v0, v1

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aC()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aD()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aE()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()I

    move-result v0

    return v0
.end method

.method public aG()I
    .locals 1

    const v0, 0x7f020222

    return v0
.end method

.method public aH()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0x2a8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aI()Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0xe5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public aL()Lam/f;
    .locals 1

    iget-object v0, p0, Lbf/i;->n:Lam/f;

    return-object v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected aN()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final aO()I
    .locals 1

    iget v0, p0, Lbf/i;->u:I

    return v0
.end method

.method protected aP()V
    .locals 5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-static {p0}, Lbf/am;->m(Lbf/i;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v3}, Lbf/i;->a(Ljava/io/DataOutput;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Layer Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Error saving"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Layer Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Error saving"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public aQ()V
    .locals 1

    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    iput v0, p0, Lbf/i;->B:I

    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    iput-byte v0, p0, Lbf/i;->C:B

    return-void
.end method

.method public aR()V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lbf/i;->ah()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lbf/i;->B:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lbf/i;->B:I

    invoke-virtual {p0, v0}, Lbf/i;->b(I)V

    :cond_0
    iget-byte v0, p0, Lbf/i;->C:B

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    iget-byte v1, p0, Lbf/i;->C:B

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    :cond_1
    invoke-virtual {p0}, Lbf/i;->y()V

    :cond_2
    iput v2, p0, Lbf/i;->B:I

    const/4 v0, 0x0

    iput-byte v0, p0, Lbf/i;->C:B

    return-void
.end method

.method public final aS()Z
    .locals 1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v0}, Lbf/i;->b(Lcom/google/googlenav/F;)V

    :cond_0
    invoke-virtual {p0}, Lbf/i;->aT()Z

    move-result v0

    return v0
.end method

.method protected aT()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 0

    return-void
.end method

.method public aV()V
    .locals 0

    invoke-virtual {p0}, Lbf/i;->j()V

    return-void
.end method

.method public aW()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/i;->o:Z

    invoke-virtual {p0}, Lbf/i;->y()V

    invoke-virtual {p0}, Lbf/i;->j()V

    return-void
.end method

.method public aX()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/i;->o:Z

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(I)V

    invoke-virtual {p0}, Lbf/i;->r()V

    invoke-virtual {p0}, Lbf/i;->al()V

    invoke-direct {p0}, Lbf/i;->d()V

    invoke-virtual {p0}, Lbf/i;->V()V

    invoke-direct {p0}, Lbf/i;->f()V

    return-void
.end method

.method public final aY()Z
    .locals 2

    const/16 v0, 0x23

    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lbf/i;->a(CI)Z

    move-result v0

    return v0
.end method

.method public aZ()Z
    .locals 1

    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/S;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->P_()Z

    move-result v0

    goto :goto_0
.end method

.method public aa()Z
    .locals 1

    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ab()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected ac()Z
    .locals 1

    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ad()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final ae()Z
    .locals 2

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final af()Z
    .locals 2

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ag()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ah()Z
    .locals 1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ai()Z
    .locals 1

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aj()Z
    .locals 2

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->ah()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ak()Lcom/google/googlenav/ui/view/d;
    .locals 1

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    return-object v0
.end method

.method public al()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    iput-object v1, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    iput-object v1, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    invoke-interface {v0, p0}, Lbf/j;->b(Lbf/i;)V

    :cond_0
    iput-object v1, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    :cond_1
    return-void
.end method

.method protected am()V
    .locals 0

    return-void
.end method

.method public an()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/i;->g(Z)Z

    move-result v0

    return v0
.end method

.method public ao()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract ap()V
.end method

.method protected abstract aq()V
.end method

.method public ar()Lcom/google/googlenav/F;
    .locals 1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    return-object v0
.end method

.method public as()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v5

    move v4, v3

    move v1, v3

    :goto_0
    if-ge v4, v5, :cond_2

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v4}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v6

    if-eqz v6, :cond_0

    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    :cond_0
    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_4

    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    :cond_1
    move v1, v3

    :cond_2
    if-ne v1, v2, :cond_3

    move v0, v2

    :goto_2
    return v0

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public at()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v3, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v3}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public au()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract av()I
.end method

.method public aw()LaN/Y;
    .locals 1

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public ax()Z
    .locals 1

    iget-boolean v0, p0, Lbf/i;->o:Z

    return v0
.end method

.method public ay()Z
    .locals 1

    iget-boolean v0, p0, Lbf/i;->p:Z

    return v0
.end method

.method protected az()Z
    .locals 1

    iget-boolean v0, p0, Lbf/i;->y:Z

    return v0
.end method

.method public b(LaN/B;)I
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v3}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    :goto_1
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v3}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    :cond_3
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lbf/i;->b(Lcom/google/googlenav/E;LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b(B)V
    .locals 2

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lbf/i;->d(Z)V

    :cond_0
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lbf/i;->e(Z)V

    :cond_1
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b(I)V
    .locals 5

    const/4 v4, -0x1

    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v0

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v1

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, p1}, Lcom/google/googlenav/F;->a(I)V

    invoke-direct {p0}, Lbf/i;->a()V

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->as()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const/16 v3, 0x17

    if-eq v2, v3, :cond_0

    const/16 v3, 0x18

    if-ne v2, v3, :cond_2

    :cond_0
    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v2

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v0

    if-eq v1, v0, :cond_2

    :cond_1
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    :cond_2
    if-ne p1, v4, :cond_3

    invoke-virtual {p0}, Lbf/i;->al()V

    :cond_3
    if-eq p1, v4, :cond_4

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lbf/i;->f(I)V

    :cond_4
    invoke-virtual {p0}, Lbf/i;->K()V

    return-void
.end method

.method protected b(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lbf/i;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lbf/i;->n()V

    return-void
.end method

.method public b(Lcom/google/googlenav/F;)V
    .locals 0

    invoke-virtual {p0, p1}, Lbf/i;->a(Lcom/google/googlenav/F;)V

    invoke-virtual {p0}, Lbf/i;->R()V

    return-void
.end method

.method protected b(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method protected b(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p2}, Lbf/i;->a(Lcom/google/googlenav/E;)LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/d;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbf/i;->c:LaN/p;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    :goto_1
    if-nez p1, :cond_4

    move v1, v0

    :goto_2
    if-nez p1, :cond_5

    :goto_3
    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    add-int/2addr v1, v3

    invoke-virtual {p0, p2}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v0, v3

    invoke-virtual {p0, p2}, Lbf/i;->c(Lcom/google/googlenav/E;)I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Point;->set(II)V

    if-nez p1, :cond_2

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lbf/i;->c:LaN/p;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->c()I

    move-result v1

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->d()I

    move-result v0

    goto :goto_3
.end method

.method protected b(Z)V
    .locals 1

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbf/i;->a(ZLaN/Y;)V

    return-void
.end method

.method public b(LaN/g;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lbf/i;->a(LaN/g;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LaN/g;->b()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lbf/i;->a(LaN/B;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Lat/a;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbf/i;->c(Lat/a;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ba()Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method public bb()Lbh/a;
    .locals 1

    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    return-object v0
.end method

.method public bc()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public bd()V
    .locals 1

    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    invoke-interface {v0, p0}, Lbf/j;->a(Lbf/i;)V

    :cond_0
    return-void
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->G()I

    move-result v2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Lcom/google/googlenav/ui/bi;->c:I

    :goto_0
    add-int/2addr v1, v2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public final c(I)Lcom/google/googlenav/e;
    .locals 1

    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0, p1}, Lbh/a;->b(I)Lcom/google/googlenav/e;

    move-result-object v0

    return-object v0
.end method

.method protected c(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lbf/i;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lbf/i;->l()V

    return-void
.end method

.method protected c(Lcom/google/googlenav/ui/r;)V
    .locals 4

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p0}, Lbf/i;->aj()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/googlenav/E;->c()B

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbf/i;->c:LaN/p;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->c()I

    move-result v3

    invoke-direct {p0, p1, v1, v2, v3}, Lbf/i;->a(Lcom/google/googlenav/ui/r;III)V

    :cond_0
    invoke-virtual {p0, p1, v0}, Lbf/i;->b(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V

    :cond_1
    return-void
.end method

.method public c(LaN/B;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public c(LaN/g;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lbf/i;->a(LaN/g;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LaN/g;->b()LaN/B;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lbf/i;->a(LaN/B;I)Z

    move-result v0

    goto :goto_0
.end method

.method protected c(Lat/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected d(Lcom/google/googlenav/E;)I
    .locals 1

    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/E;)I

    move-result v0

    return v0
.end method

.method protected d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    return-object v0
.end method

.method public d(I)V
    .locals 7

    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v0, v6, [Ljava/lang/Boolean;

    iput-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    mul-int/lit8 v0, v3, 0x2

    const/4 v4, 0x2

    shl-int/2addr v4, v0

    and-int/2addr v4, p1

    if-eqz v4, :cond_0

    iget-object v4, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    new-instance v5, Ljava/lang/Boolean;

    shl-int v0, v1, v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {v5, v0}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v5, v4, v3

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    return-void
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .locals 5

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->c(I)I

    move-result v1

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, p0, Lbf/i;->c:LaN/p;

    iget-object v4, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v3, v2, v4}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {p0, p1, v2, v3, v1}, Lbf/i;->a(Lcom/google/googlenav/ui/r;III)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/i;->v:Z

    return-void
.end method

.method public d(Lat/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected e(Lcom/google/googlenav/E;)I
    .locals 1

    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0, p1}, Lbh/a;->a(Lcom/google/googlenav/E;)I

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 0

    iput p1, p0, Lbf/i;->u:I

    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .locals 1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, p1, v0}, Lbf/i;->a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/F;)V

    return-void
.end method

.method public e(Z)V
    .locals 1

    iput-boolean p1, p0, Lbf/i;->w:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    return-void
.end method

.method public e(Lat/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected f(Lcom/google/googlenav/E;)I
    .locals 1

    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0, p1}, Lbh/a;->b(Lcom/google/googlenav/E;)I

    move-result v0

    return v0
.end method

.method public final f(I)V
    .locals 1

    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lbf/i;->a(Lcom/google/googlenav/E;I)V

    return-void
.end method

.method public f(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/i;->x:Z

    return-void
.end method

.method protected abstract f(Lat/a;)Z
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Lcom/google/googlenav/common/util/l;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    move v0, v1

    move-object v1, v2

    :goto_1
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public g(I)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g(Z)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lbf/i;->al()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    if-ne v1, v2, :cond_2

    if-eqz p1, :cond_5

    :cond_2
    invoke-virtual {p0}, Lbf/i;->am()V

    iget-object v2, p0, Lbf/i;->s:Lbf/w;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbf/i;->s:Lbf/w;

    invoke-interface {v2}, Lbf/w;->b()Lcom/google/googlenav/ui/view/d;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lbf/i;->al()V

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    iput-object v1, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    invoke-interface {v0, p0, v2}, Lbf/j;->a(Lbf/i;Lcom/google/googlenav/ui/view/d;)V

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lbf/i;->f(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lbf/i;->n()V

    return-void
.end method

.method public h(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/i;->p:Z

    return-void
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/a;

    invoke-direct {v0, p0}, Lbh/a;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected i(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/i;->y:Z

    return-void
.end method

.method protected j()V
    .locals 1

    invoke-virtual {p0}, Lbf/i;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/i;->b()V

    :goto_0
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/i;->S()V

    :goto_1
    invoke-virtual {p0}, Lbf/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lbf/i;->c()V

    :goto_2
    return-void

    :cond_0
    invoke-direct {p0}, Lbf/i;->d()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lbf/i;->V()V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lbf/i;->f()V

    goto :goto_2
.end method

.method public j(Z)V
    .locals 1

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/S;->a(Z)V

    :cond_0
    return-void
.end method

.method public k()Lcom/google/googlenav/settings/e;
    .locals 1

    iget-object v0, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    return-object v0
.end method

.method protected l()V
    .locals 7

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    invoke-virtual {p0}, Lbf/i;->y()V

    const/16 v0, 0x19

    invoke-virtual {p0, v0}, Lbf/i;->f(I)V

    invoke-virtual {p0}, Lbf/i;->t()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x72

    const-string v2, ""

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    iget-object v1, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/i;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->b(LaN/B;)V

    :cond_1
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lbf/i;)V

    :cond_2
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ia;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/ia;->a(Lbf/i;)V

    :cond_3
    return-void
.end method

.method protected m()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    invoke-virtual {p0}, Lbf/i;->y()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(Z)V

    :cond_0
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    :cond_1
    return-void
.end method

.method protected n()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    invoke-virtual {p0}, Lbf/i;->y()V

    return-void
.end method

.method public o()B
    .locals 1

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    return v0
.end method

.method protected p()I
    .locals 1

    invoke-virtual {p0}, Lbf/i;->q()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public q()I
    .locals 2

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->d()Lcom/google/googlenav/ui/android/ac;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ac;->a()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method protected r()V
    .locals 1

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Boolean;

    iput-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    return-void
.end method

.method public s()Lcom/google/googlenav/E;
    .locals 1

    invoke-virtual {p0}, Lbf/i;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_0
.end method

.method protected t()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected u()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected v()V
    .locals 0

    return-void
.end method

.method protected w()V
    .locals 0

    return-void
.end method

.method protected x()I
    .locals 1

    invoke-virtual {p0}, Lbf/i;->au()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/i;->C()V

    :goto_0
    invoke-virtual {p0}, Lbf/i;->D()V

    invoke-virtual {p0}, Lbf/i;->E()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lbf/i;->z()V

    goto :goto_0
.end method

.method protected z()V
    .locals 3

    iget-boolean v0, p0, Lbf/i;->o:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/i;->v()V

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lbf/i;->aq()V

    iget-object v1, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lbf/i;->B()V

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    invoke-virtual {p0}, Lbf/i;->ap()V

    iget-object v1, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lbf/i;->A()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Lbf/i;->an()Z

    invoke-virtual {p0}, Lbf/i;->r()V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lbf/i;->al()V

    invoke-virtual {p0}, Lbf/i;->r()V

    goto :goto_1
.end method
