.class public abstract Lbf/m;
.super Lbf/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/bb;


# static fields
.field public static final u:Lcom/google/common/base/x;

.field protected static z:Lcom/google/googlenav/ui/view/android/au;


# instance fields
.field protected A:Lbf/be;

.field private B:Lcom/google/googlenav/ui/a;

.field private final C:Ljava/util/Hashtable;

.field private D:Lcom/google/googlenav/ui/view/dialog/aq;

.field private E:Lcom/google/googlenav/ai;

.field protected v:I

.field protected w:Z

.field protected x:Lcom/google/googlenav/ui/br;

.field protected y:LaB/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbf/n;

    invoke-direct {v0}, Lbf/n;-><init>()V

    sput-object v0, Lbf/m;->u:Lcom/google/common/base/x;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct/range {p0 .. p5}, Lbf/i;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    iput v0, p0, Lbf/m;->v:I

    iput-boolean v0, p0, Lbf/m;->w:Z

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    iput-object v1, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    iput-object v1, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    iput-object v0, p0, Lbf/m;->y:LaB/s;

    iget-object v0, p0, Lbf/m;->y:LaB/s;

    invoke-static {v0}, Lbf/m;->a(LaB/s;)Lcom/google/googlenav/ui/br;

    move-result-object v0

    iput-object v0, p0, Lbf/m;->x:Lcom/google/googlenav/ui/br;

    new-instance v0, Lbf/be;

    invoke-direct {v0, p0}, Lbf/be;-><init>(Lbf/m;)V

    iput-object v0, p0, Lbf/m;->A:Lbf/be;

    return-void
.end method

.method static a(Lax/y;Lr/n;)Lax/y;
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v0

    invoke-virtual {v0}, Lo/D;->b()I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v0

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p1, v0}, Lr/n;->c(Lo/r;)Lo/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    invoke-static {p0, v0}, Lax/y;->a(Lax/y;Lo/D;)Lax/y;

    move-result-object p0

    goto :goto_0
.end method

.method static a(Lcom/google/googlenav/ai;ZZ)Lax/y;
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p0}, Lax/y;->c(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    :goto_0
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v1

    invoke-static {v0, v1}, Lbf/m;->a(Lax/y;Lr/n;)Lax/y;

    move-result-object v0

    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p0}, Lax/y;->a(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lax/y;->b(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LaB/s;)Lcom/google/googlenav/ui/br;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;
    .locals 3

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-virtual {p0}, Lcom/google/googlenav/ap;->a()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private a()V
    .locals 1

    invoke-direct {p0}, Lbf/m;->bH()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->H()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lbf/m;Lcom/google/googlenav/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/m;->n(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method private a(ZLjava/lang/String;ILjava/lang/String;)V
    .locals 6

    if-eqz p1, :cond_0

    const-string v0, "e"

    :goto_0
    const/16 v2, 0x54

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    if-nez p4, :cond_1

    const/4 v1, 0x0

    :goto_1
    aput-object v1, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "c"

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private a(Lcom/google/googlenav/ai;IILjava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_1

    const/16 v1, 0x11

    if-eq p2, v1, :cond_1

    invoke-virtual {p0}, Lbf/m;->ae()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eq p2, v0, :cond_1

    :cond_0
    const/16 v1, 0x578

    if-ne p2, v1, :cond_2

    const/4 v1, -0x1

    if-ne p3, v1, :cond_1

    if-eqz p4, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lbf/m;Lcom/google/googlenav/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/m;->m(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method private static b(Lcom/google/googlenav/ai;I)V
    .locals 9

    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    invoke-static {p0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    cmp-long v6, v0, v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "u="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v8

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is_ad="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private b()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lbf/m;->ae()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bG()Z
    .locals 1

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/m;->bA()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bH()Z
    .locals 1

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bI()V
    .locals 2

    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/m;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aq;->a(Z)V

    :cond_0
    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aq;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    :cond_1
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lbf/m;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const-string v0, "f"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "s"

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/ui/wizard/A;)V
    .locals 14

    const/4 v13, 0x7

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, -0x1

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    invoke-static {v1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    const-string v6, "sd"

    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-wide/16 v8, -0x1

    cmp-long v0, v2, v8

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cid="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v7, v12

    if-eqz v4, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v7, v11

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lbb/a;->a(Lcom/google/googlenav/ai;J)V

    :cond_0
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lcom/google/googlenav/n;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/n;

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v2

    if-ne v2, v10, :cond_1

    new-array v0, v11, [Lcom/google/googlenav/ai;

    aput-object v1, v0, v12

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v2

    invoke-static {v0, v2, v10, v10}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    :cond_1
    if-eqz p1, :cond_5

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_2
    :goto_2
    invoke-virtual {p0, v11, v1}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    invoke-virtual {p0, v13}, Lbf/m;->f(I)V

    return-void

    :cond_3
    const-string v0, ""

    goto :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v13, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lcom/google/googlenav/bz;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x1a

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lcom/google/googlenav/T;

    if-eqz v0, :cond_2

    const/16 v0, 0x12

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ai;->a(B)V

    new-array v0, v11, [Lcom/google/googlenav/ai;

    aput-object v1, v0, v12

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v2

    invoke-static {v0, v2, v10, v10}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v13, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_2
.end method

.method public static c(Lcom/google/googlenav/ai;)Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aY()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    iget-object v0, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v1

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->b()I

    move-result v2

    iget-object v3, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/google/googlenav/ai;)Z
    .locals 4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    sget-object v2, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/au;->h()Lcom/google/googlenav/as;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/as;->f()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->dismiss()V

    const/4 v0, 0x0

    sput-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    goto :goto_0

    :cond_1
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->show()V

    goto :goto_0
.end method

.method public static f(Lcom/google/googlenav/ai;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x90

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static i(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 6

    invoke-static {p0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->cg()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    :goto_0
    if-eqz v1, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&gmmsmh=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "is_ad="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cb="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "1"

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private j(I)V
    .locals 10

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x54

    const-string v5, "p"

    const/4 v0, 0x4

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "f="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lbf/m;->v:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "t="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v7, 0x2

    const-wide/16 v8, -0x1

    cmp-long v0, v1, v8

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cid="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v6, v7

    const/4 v1, 0x3

    if-eqz v3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v6, v1

    invoke-static {v6}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private k(I)V
    .locals 4

    const/4 v0, 0x0

    if-gez p1, :cond_0

    move p1, v0

    :cond_0
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    return-void
.end method

.method private l(Lcom/google/googlenav/ai;)V
    .locals 3

    if-eqz p1, :cond_0

    const/4 v0, 0x5

    const-string v1, "0"

    invoke-static {p1}, Lbf/m;->i(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private m(Lcom/google/googlenav/ai;)V
    .locals 9

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    if-eqz v4, :cond_0

    const/16 v0, 0x90

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->an()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v6

    invoke-interface {v6, v5}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    if-nez v0, :cond_3

    const/4 v2, 0x1

    move v3, v2

    :goto_1
    if-eqz v3, :cond_2

    new-instance v0, LaR/D;

    invoke-direct {v0}, LaR/D;-><init>()V

    invoke-virtual {v0, v5}, LaR/D;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LaR/D;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v2

    invoke-virtual {v0, v2}, LaR/D;->a(LaN/B;)V

    :cond_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, LaR/D;->a(J)V

    invoke-virtual {v0}, LaR/D;->f()I

    move-result v2

    const/4 v5, -0x1

    if-ne v2, v5, :cond_4

    :goto_2
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, LaR/D;->a(I)V

    invoke-interface {v6, v0}, LaR/u;->a(LaR/t;)Z

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lbf/s;

    invoke-direct {v1, p0, v6, v4}, Lbf/s;-><init>(Lbf/m;LaR/u;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->run()V

    goto :goto_0

    :cond_3
    move v3, v1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method private n(Lcom/google/googlenav/ai;)V
    .locals 3

    if-eqz p1, :cond_0

    iput-object p1, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    :cond_0
    iget-object v0, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aq;

    iget-object v1, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/dialog/aq;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aq;->show()V

    :cond_1
    return-void
.end method


# virtual methods
.method protected D()V
    .locals 4

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v1, v1, LaB/p;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/m;->y:LaB/s;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ai;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    sget-object v3, Lbf/m;->u:Lcom/google/common/base/x;

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    goto :goto_0
.end method

.method protected E()V
    .locals 4

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v1, v1, LaB/p;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/m;->y:LaB/s;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ai;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lbf/t;

    invoke-direct {v3, p0}, Lbf/t;-><init>(Lbf/m;)V

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->b(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    goto :goto_0
.end method

.method public J()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/m;->w:Z

    invoke-super {p0}, Lbf/i;->J()V

    return-void
.end method

.method protected final a(ILcom/google/googlenav/ai;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lai/a;->a(Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method public a(LaN/B;Lcom/google/googlenav/ui/wizard/dF;)V
    .locals 2

    invoke-virtual {p0}, Lbf/m;->aQ()V

    iget-object v0, p0, Lbf/m;->d:LaN/u;

    const/16 v1, 0x13

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lbf/i;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    check-cast p2, Lcom/google/googlenav/ai;

    if-eqz p2, :cond_1

    invoke-static {p3}, Lcom/google/googlenav/Q;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Z)Lcom/google/googlenav/R;

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_0
    :goto_0
    const/16 v0, 0xa

    if-ne p3, v0, :cond_1

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ai()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/googlenav/ai;I)V
    .locals 11

    const/4 v10, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x45

    const-string v5, "n"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    cmp-long v8, v0, v8

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v6, v7

    if-eqz v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "u="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v6, v10

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "entry="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lbf/m;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v6}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v5, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v10, p1}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lbb/a;->a(Lcom/google/googlenav/ai;J)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lax/y;->a(Ljava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-direct {p0}, Lbf/m;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v4, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/ai;Z)V
    .locals 4

    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lbf/aQ;->d(Z)V

    :cond_0
    const/4 v0, 0x0

    new-instance v1, Lbf/r;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3, v0}, Lbf/r;-><init>(Lbf/m;Las/c;Lcom/google/googlenav/android/aa;Z)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Lbm/j;->a(J)V

    invoke-virtual {v1}, Lbm/j;->g()V

    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/A;)V
    .locals 5

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lbf/m;->c(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {p0}, Lbf/m;->bv()Z

    move-result v1

    invoke-virtual {p0}, Lbf/m;->bw()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lbf/m;->a(Lcom/google/googlenav/ai;ZZ)Lax/y;

    move-result-object v1

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const/4 v3, 0x0

    invoke-direct {p0}, Lbf/m;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbl/o;->c(Lbf/m;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p0}, Lbf/m;->bo()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x11

    :goto_0
    invoke-virtual {p0, v0, p1}, Lbf/m;->b(ILjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->R()V

    iget-object v0, p0, Lbf/m;->d:LaN/u;

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/m;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->b(LaN/B;)V

    sget-object v0, LaE/d;->a:LaE/d;

    invoke-virtual {v0}, LaE/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(LaN/B;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/m;->ae()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    goto :goto_0

    :cond_2
    const/4 v0, 0x7

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/m;->an()Z

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/aB;)V
    .locals 3

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-static {p1}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, p2, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Lcom/google/googlenav/aB;Z)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x0

    instance-of v0, p0, Lbf/by;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1a

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_0
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/google/googlenav/bd;

    const-string v4, ""

    invoke-direct {v3, v6, v4, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/googlenav/bc;

    const/16 v4, 0x130

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object p1, v5, v6

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v7, v4, v2}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v2, "*"

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v2, 0x5f8

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v2, "20"

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void

    :cond_1
    instance-of v0, p0, Lbf/bk;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x11

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 11

    const/16 v10, 0x54

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v4

    invoke-direct {p0, v4, p1, p2, p3}, Lbf/m;->a(Lcom/google/googlenav/ai;IILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-super {p0, p1, p2, p3}, Lbf/i;->a(IILjava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actionIndex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", layerType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/m;->av()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", index:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v1, "BPL-onAction"

    invoke-static {v1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return v6

    :cond_2
    invoke-static {v4}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_20

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    :goto_1
    sparse-switch p1, :sswitch_data_0

    :cond_3
    :goto_2
    invoke-super {p0, p1, p2, p3}, Lbf/i;->a(IILjava/lang/Object;)Z

    move-result v6

    goto :goto_0

    :sswitch_0
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v4, v1, v2}, Lbb/a;->a(Lcom/google/googlenav/ai;J)V

    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    move v5, v6

    :cond_5
    invoke-virtual {p0, v4, v5}, Lbf/m;->b(Lcom/google/googlenav/ai;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbl/o;->b(Lbf/m;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "m"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lbf/m;->an()Z

    invoke-virtual {p0, v6}, Lbf/m;->b(Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v8}, Lbf/m;->a(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbf/m;->n()V

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "21"

    invoke-virtual {v1, v8, v2, v0, v6}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "ct"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;)V

    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "rp"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    move-object v2, v0

    :goto_3
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bw()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "a=s"

    aput-object v1, v0, v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "i="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    aput-object v7, v0, v9

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {p0}, Lbf/m;->av()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "ac"

    const-string v1, "rp"

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move-object v2, v8

    goto :goto_3

    :sswitch_6
    invoke-direct {p0, p2}, Lbf/m;->j(I)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {v0, p3, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aX()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "kml"

    invoke-static {v9, v0}, Lbm/m;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Street View"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    if-eqz p3, :cond_b

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v1, v0

    :goto_4
    const/4 v0, 0x3

    if-ne v1, v0, :cond_a

    move v5, v6

    :cond_a
    if-eqz v5, :cond_c

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bt()Ljava/lang/String;

    move-result-object v0

    :goto_5
    if-nez v0, :cond_d

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->b(LaN/B;)V

    :goto_6
    invoke-virtual {p0, v1}, Lbf/m;->h(I)V

    invoke-virtual {p0, v9, v4}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    const-string v0, "st"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move v1, v6

    goto :goto_4

    :cond_c
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_d
    if-eqz v5, :cond_e

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bu()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bv()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;I)V

    goto :goto_6

    :cond_e
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;LaN/B;)V

    goto :goto_6

    :sswitch_9
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    const-string v0, "d"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/m;->n()V

    check-cast p3, Lcom/google/googlenav/ui/wizard/A;

    invoke-virtual {p0, p3}, Lbf/m;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    invoke-virtual {p0, v4, v5}, Lbf/m;->a(Lcom/google/googlenav/ai;I)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    invoke-virtual {p0, v4, v9}, Lbf/m;->a(Lcom/google/googlenav/ai;I)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ai;)V

    const/16 v0, 0x4f

    const-string v1, "o"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "f=p"

    aput-object v3, v2, v5

    aput-object v7, v2, v6

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "sr"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p0, p2}, Lbf/m;->i(I)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v0, "Make a reservation"

    const-string v1, "missing url"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lbf/m;->f(I)V

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto/16 :goto_0

    :sswitch_10
    if-eqz p3, :cond_11

    check-cast p3, Lcom/google/googlenav/ai;

    :cond_10
    :goto_7
    invoke-virtual {p3}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x90

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v0, LaR/D;

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    move-object v3, v8

    invoke-direct/range {v0 .. v5}, LaR/D;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LaR/D;->a(Ljava/util/List;)V

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, LaR/H;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/H;

    move-result-object v2

    iget-object v3, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v3

    invoke-virtual {p0}, Lbf/m;->bn()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v2, v3, v4}, LaR/n;->a(LaR/D;LaR/H;Lbf/am;Ljava/lang/String;)LaR/D;

    move-result-object v0

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_12

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "sp"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1f

    invoke-virtual {p0, p2}, Lbf/m;->b(I)V

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object p3

    if-nez p3, :cond_10

    goto/16 :goto_0

    :cond_12
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "up"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_11
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "Review"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    const-string v2, "pp"

    invoke-virtual {v0, v4, v1, v5, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "Photo Upload"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_14
    if-nez p3, :cond_15

    const-string p3, "mo"

    :goto_8
    new-instance v0, Lbf/o;

    invoke-direct {v0, p0}, Lbf/o;-><init>(Lbf/m;)V

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v4, v8, p3, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    goto/16 :goto_0

    :cond_15
    check-cast p3, Ljava/lang/String;

    goto :goto_8

    :sswitch_13
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lbf/m;->j(I)V

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->D()Lcom/google/googlenav/ui/wizard/fk;

    move-result-object v0

    invoke-virtual {v0, v4, p2, p0}, Lcom/google/googlenav/ui/wizard/fk;->a(Lcom/google/googlenav/ai;ILbf/m;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/aQ;->a()Z

    move-result v2

    if-nez v2, :cond_16

    move v5, v6

    :cond_16
    invoke-virtual {v0, v5}, Lbf/aQ;->a(Z)V

    const-string v0, "u"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_0

    :sswitch_15
    new-instance v3, Lcom/google/googlenav/h;

    invoke-direct {v3, v4}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/ai;)V

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bT()Z

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_17
    const-string v4, "cppi"

    :goto_9
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    move v7, v6

    invoke-virtual/range {v1 .. v8}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    goto/16 :goto_0

    :cond_18
    const-string v4, "cppn"

    goto :goto_9

    :sswitch_16
    new-instance v0, Lcom/google/googlenav/ui/wizard/ep;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/ep;-><init>()V

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->m()Lbf/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    new-instance v1, Lcom/google/googlenav/h;

    invoke-direct {v1, v4}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    iput-boolean v6, v0, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const/16 v1, 0x13c

    invoke-virtual {v0, v1}, Lbf/am;->d(I)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/s;->b(Z)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/bZ;)V

    const-string v0, "c"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/aQ;->b()Z

    move-result v2

    if-nez v2, :cond_19

    move v5, v6

    :cond_19
    invoke-virtual {v0, v5}, Lbf/aQ;->b(Z)V

    const-string v0, "d"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_0

    :sswitch_1a
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/aQ;->c()Z

    move-result v2

    if-nez v2, :cond_1a

    move v5, v6

    :cond_1a
    invoke-virtual {v0, v5}, Lbf/aQ;->c(Z)V

    const-string v0, "h"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    check-cast p3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v1, p3, p0, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/ui/s;)V

    goto/16 :goto_0

    :sswitch_1c
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/aQ;->d()Z

    move-result v2

    if-nez v2, :cond_1b

    move v5, v6

    :cond_1b
    invoke-virtual {v0, v5}, Lbf/aQ;->d(Z)V

    const-string v0, "g"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_0

    :sswitch_1d
    check-cast p3, Ljava/lang/String;

    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p3, v5, p2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    :cond_1c
    const-string v0, "ac"

    const-string v1, "mi"

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1e
    check-cast p3, [Lcom/google/googlenav/aw;

    check-cast p3, [Lcom/google/googlenav/aw;

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p3, p2, v5}, Lcom/google/googlenav/ui/wizard/jv;->a([Lcom/google/googlenav/aw;IZ)V

    goto/16 :goto_0

    :sswitch_1f
    check-cast p3, [Lcom/google/googlenav/aw;

    check-cast p3, [Lcom/google/googlenav/aw;

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p3, p2, v6}, Lcom/google/googlenav/ui/wizard/jv;->a([Lcom/google/googlenav/aw;IZ)V

    goto/16 :goto_0

    :sswitch_20
    check-cast p3, Ljava/lang/String;

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Lcom/google/googlenav/ui/wizard/jv;->b(Ljava/lang/String;I)V

    goto/16 :goto_0

    :sswitch_21
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Lbf/m;->a(Lcom/google/googlenav/E;I)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/au;

    new-instance v1, Lbf/p;

    invoke-direct {v1, p0}, Lbf/p;-><init>(Lbf/m;)V

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/android/au;-><init>(Lcom/google/googlenav/ui/e;)V

    sput-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    check-cast p3, Lcom/google/googlenav/as;

    invoke-virtual {p0}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/as;LaB/s;)V

    goto/16 :goto_0

    :sswitch_22
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/s;->e(Z)V

    goto/16 :goto_0

    :sswitch_23
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    check-cast p3, Lcom/google/googlenav/ai;

    invoke-virtual {v0, p3, v9, v5}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    goto/16 :goto_0

    :sswitch_24
    invoke-virtual {p0}, Lbf/m;->aX()V

    if-eqz p3, :cond_1d

    check-cast p3, [Ljava/lang/String;

    check-cast p3, [Ljava/lang/String;

    aget-object v0, p3, v5

    aget-object v1, p3, v6

    invoke-virtual {p0, v0, v1}, Lbf/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1d
    invoke-virtual {p0, v8, v8}, Lbf/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_25
    check-cast p3, Ljava/lang/String;

    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "LaunchUrl"

    const-string v1, "missing url"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_a
    const/16 v0, 0x19

    if-ne p1, v0, :cond_1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbl/o;->a(Lbf/m;)V

    goto/16 :goto_0

    :cond_1e
    invoke-virtual {p0, p3}, Lbf/m;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/m;->a()V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p3, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto :goto_a

    :sswitch_26
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0, v6, v4}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    goto/16 :goto_0

    :sswitch_27
    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_2

    :sswitch_28
    iput-boolean v6, p0, Lbf/m;->w:Z

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lbf/m;->i(I)V

    goto/16 :goto_0

    :sswitch_29
    invoke-direct {p0, p2}, Lbf/m;->k(I)V

    goto/16 :goto_0

    :sswitch_2a
    invoke-virtual {p0, v8}, Lbf/m;->a(Ljava/lang/Object;)V

    check-cast p3, Lcom/google/googlenav/bZ;

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p3}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2b
    check-cast p3, Ljava/lang/String;

    new-instance v0, Lbf/q;

    invoke-direct {v0, p0}, Lbf/q;-><init>(Lbf/m;)V

    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p3}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto/16 :goto_0

    :cond_1f
    move-object p3, v4

    goto/16 :goto_7

    :cond_20
    move-object v7, v8

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_0
        0x4 -> :sswitch_d
        0x5 -> :sswitch_25
        0x6 -> :sswitch_c
        0xf -> :sswitch_e
        0x10 -> :sswitch_0
        0x11 -> :sswitch_1d
        0x12 -> :sswitch_22
        0x18 -> :sswitch_27
        0x19 -> :sswitch_25
        0xc9 -> :sswitch_18
        0x14e -> :sswitch_17
        0x258 -> :sswitch_8
        0x25a -> :sswitch_2
        0x25b -> :sswitch_9
        0x25c -> :sswitch_a
        0x25d -> :sswitch_b
        0x262 -> :sswitch_26
        0x2bf -> :sswitch_3
        0x2c0 -> :sswitch_4
        0x2c1 -> :sswitch_7
        0x2c3 -> :sswitch_f
        0x2ce -> :sswitch_6
        0x3fa -> :sswitch_23
        0x4b0 -> :sswitch_28
        0x4b1 -> :sswitch_29
        0x578 -> :sswitch_10
        0x5dc -> :sswitch_5
        0x6a4 -> :sswitch_11
        0x713 -> :sswitch_24
        0x76c -> :sswitch_14
        0x76d -> :sswitch_13
        0x834 -> :sswitch_15
        0x843 -> :sswitch_16
        0x900 -> :sswitch_19
        0x902 -> :sswitch_1a
        0x906 -> :sswitch_1b
        0x907 -> :sswitch_20
        0x908 -> :sswitch_1e
        0x909 -> :sswitch_12
        0x90a -> :sswitch_1f
        0x90b -> :sswitch_21
        0x90c -> :sswitch_1c
        0x90d -> :sswitch_2b
        0xfa5 -> :sswitch_2a
    .end sparse-switch
.end method

.method public aU()V
    .locals 1

    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    if-eqz v0, :cond_0

    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->dismiss()V

    const/4 v0, 0x0

    sput-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    :cond_0
    iget-object v0, p0, Lbf/m;->y:LaB/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/m;->y:LaB/s;

    invoke-virtual {v0}, LaB/s;->a()V

    :cond_1
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    invoke-super {p0}, Lbf/i;->aU()V

    return-void
.end method

.method public aV()V
    .locals 1

    invoke-super {p0}, Lbf/i;->aV()V

    invoke-direct {p0}, Lbf/m;->bH()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->I()V

    :cond_0
    return-void
.end method

.method public aW()V
    .locals 1

    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/m;->b(B)V

    :cond_0
    invoke-super {p0}, Lbf/i;->aW()V

    return-void
.end method

.method protected ap()V
    .locals 3

    iget-object v0, p0, Lbf/m;->A:Lbf/be;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No DetailsDialog for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lbf/m;->v:I

    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lbf/m;->h(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    iput v0, p0, Lbf/m;->v:I

    :cond_2
    iget-object v0, p0, Lbf/m;->A:Lbf/be;

    iget v1, p0, Lbf/m;->v:I

    invoke-virtual {v0, v1}, Lbf/be;->a(I)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    iput-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lbf/m;->by()V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .locals 1

    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method b(Lcom/google/googlenav/ai;Z)Z
    .locals 4

    invoke-direct {p0}, Lbf/m;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-static {p1, v0}, Lbf/m;->b(Lcom/google/googlenav/ai;I)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aO()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lbf/u;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lbf/u;-><init>(Lbf/m;Lcom/google/googlenav/ai;Lbf/n;)V

    invoke-interface {v0, v1, v2, p2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Lcom/google/googlenav/aB;Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/ui/wizard/A;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0, v1}, Lbf/m;->j(Z)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/m;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lbf/m;->n(Lcom/google/googlenav/ai;)V

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v1}, Lbf/m;->j(Z)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/m;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/m;->aR()V

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lbf/m;->bI()V

    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/m;->A()V

    :cond_2
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/m;->aR()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x24
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bA()Z
    .locals 2

    iget v0, p0, Lbf/m;->v:I

    if-eqz v0, :cond_0

    iget v0, p0, Lbf/m;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bB()LaB/o;
    .locals 1

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    return-object v0
.end method

.method protected bC()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected bD()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lbf/m;->bC()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lbf/m;->b(I)V

    const/4 v0, 0x0

    iput v0, p0, Lbf/m;->v:I

    invoke-virtual {p0}, Lbf/m;->m()V

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->z()I

    move-result v0

    if-le v0, v2, :cond_0

    iput-boolean v2, p0, Lbf/m;->w:Z

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lbf/m;->i(I)V

    :cond_0
    return-void
.end method

.method bE()Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    return-object v0
.end method

.method protected bF()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v2

    if-nez v2, :cond_1

    instance-of v2, v0, Lcom/google/googlenav/W;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public bc()Ljava/lang/String;
    .locals 7

    const-wide/16 v5, -0x1

    const-wide v3, 0x412e848000000000L

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Place Page"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/maps/place?cid="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    cmp-long v3, v1, v5

    if-eqz v3, :cond_5

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/m;->ag()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Map"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "ll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, "&z="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbf/m;->ag()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    cmp-long v3, v1, v5

    if-eqz v3, :cond_4

    const-string v3, "&q=cid:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public be()Lcom/google/googlenav/ui/br;
    .locals 1

    iget-object v0, p0, Lbf/m;->x:Lcom/google/googlenav/ui/br;

    return-object v0
.end method

.method public bf()Lcom/google/googlenav/ui/a;
    .locals 1

    iget-object v0, p0, Lbf/m;->B:Lcom/google/googlenav/ui/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/a;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/a;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lbf/m;->B:Lcom/google/googlenav/ui/a;

    :cond_0
    iget-object v0, p0, Lbf/m;->B:Lcom/google/googlenav/ui/a;

    return-object v0
.end method

.method public bg()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bh()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bi()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bj()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bk()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bl()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bm()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected bn()Ljava/lang/String;
    .locals 1

    const-string v0, "p"

    return-object v0
.end method

.method protected bo()V
    .locals 0

    return-void
.end method

.method public bp()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/m;->bA()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lbf/m;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iput v2, p0, Lbf/m;->v:I

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/m;->f(Z)V

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->w()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ak()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lbf/m;->A:Lbf/be;

    invoke-virtual {v1, v2}, Lbf/be;->a(I)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v1

    iput-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->dismiss()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->w()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->invalidateOptionsMenu()V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/bn;->b(Z)V

    :cond_4
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    goto :goto_0
.end method

.method public bq()V
    .locals 2

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Lcom/google/googlenav/ai;)V

    :cond_0
    invoke-virtual {p0}, Lbf/m;->bp()V

    invoke-virtual {p0}, Lbf/m;->D()V

    invoke-virtual {p0}, Lbf/m;->E()V

    return-void
.end method

.method protected br()V
    .locals 1

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    :cond_0
    return-void
.end method

.method public bs()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    return-object v0
.end method

.method public bt()V
    .locals 2

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    invoke-virtual {p0}, Lbf/m;->aR()V

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->f(Lbf/i;)V

    :cond_0
    invoke-virtual {p0}, Lbf/m;->A()V

    return-void
.end method

.method public bu()Lcom/google/googlenav/ai;
    .locals 1

    invoke-virtual {p0}, Lbf/m;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    return-object v0
.end method

.method protected bv()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected bw()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected bx()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected by()V
    .locals 5

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bR()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/googlenav/av;->b:Lcom/google/googlenav/av;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/av;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/googlenav/f;

    new-instance v3, Lbf/v;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v0, v4}, Lbf/v;-><init>(Lbf/m;Lcom/google/googlenav/ai;Lbf/n;)V

    invoke-direct {v2, v3, v1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/f;->a(Z)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method protected bz()Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lbf/m;->bG()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lbf/m;->i(I)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public c(Lcom/google/googlenav/ai;Z)Lam/f;
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_2

    invoke-virtual {v1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbf/m;->a:Lcom/google/googlenav/ui/bi;

    const v2, 0x7f02000e

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/bi;->b(I)Lam/f;

    move-result-object v0

    invoke-static {v0}, Lam/j;->d(Lam/f;)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->c()Lam/e;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v3

    invoke-interface {v0}, Lam/f;->a()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/googlenav/e;->e()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-interface {v0}, Lam/f;->b()I

    move-result v5

    invoke-virtual {v1}, Lcom/google/googlenav/e;->f()I

    move-result v1

    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    invoke-interface {v2, v3, v4, v1}, Lam/e;->a(Lam/f;II)V

    goto :goto_0
.end method

.method protected c(Ljava/lang/String;)V
    .locals 5

    sget-object v0, Lbm/l;->d:Lbm/l;

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lbm/l;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbf/m;->bF()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x73

    const-string v2, "a"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Lat/a;)Z
    .locals 3

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Lcom/google/googlenav/ai;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {p1}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v2, 0x90

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v2

    invoke-interface {v2}, LaR/n;->e()LaR/u;

    move-result-object v2

    invoke-interface {v2, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public h()V
    .locals 1

    invoke-virtual {p0}, Lbf/m;->bz()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lbf/i;->h()V

    goto :goto_0
.end method

.method protected h(I)V
    .locals 2

    invoke-virtual {p0}, Lbf/m;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lbf/m;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v0

    invoke-static {v0, v1, p1}, Lba/c;->a(ILjava/lang/String;I)V

    return-void
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/b;

    invoke-direct {v0, p0}, Lbh/b;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected i(I)V
    .locals 1

    iget v0, p0, Lbf/m;->v:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lbf/m;->j(I)V

    iput p1, p0, Lbf/m;->v:I

    iget v0, p0, Lbf/m;->v:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    :cond_1
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/m;->e(Z)V

    invoke-virtual {p0}, Lbf/m;->y()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/m;->r()V

    goto :goto_1
.end method

.method public j(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p0}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/m;->a(LaN/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected m()V
    .locals 2

    invoke-virtual {p0}, Lbf/m;->bx()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/16 v0, 0x1f

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    :cond_0
    invoke-super {p0}, Lbf/i;->m()V

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    :cond_1
    invoke-direct {p0}, Lbf/m;->f()V

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lbf/m;->f(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->v()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lbf/m;->l(Lcom/google/googlenav/ai;)V

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bS()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, v0}, Lbf/m;->m(Lcom/google/googlenav/ai;)V

    :cond_3
    return-void
.end method

.method protected r()V
    .locals 0

    invoke-super {p0}, Lbf/i;->r()V

    invoke-direct {p0}, Lbf/m;->bI()V

    return-void
.end method

.method protected u()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
