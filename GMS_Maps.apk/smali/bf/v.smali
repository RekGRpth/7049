.class Lbf/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/g;


# instance fields
.field final synthetic a:Lbf/m;

.field private final b:Lcom/google/googlenav/ai;


# direct methods
.method private constructor <init>(Lbf/m;Lcom/google/googlenav/ai;)V
    .locals 0

    iput-object p1, p0, Lbf/v;->a:Lbf/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    return-void
.end method

.method synthetic constructor <init>(Lbf/m;Lcom/google/googlenav/ai;Lbf/n;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbf/v;-><init>(Lbf/m;Lcom/google/googlenav/ai;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    const/16 v7, 0x9e

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-virtual {p2, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    iget-object v6, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v6, v5}, Lcom/google/googlenav/ai;->i(I)Lcom/google/googlenav/au;

    move-result-object v6

    if-nez v6, :cond_3

    iget-object v0, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ai;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/16 v0, 0x4c

    if-ne v5, v0, :cond_2

    iget-object v0, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-static {v4}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/au;

    move-result-object v4

    sget-object v5, LbK/bl;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {v4, v5}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v0, v4}, Lbl/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    move v0, v1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    sget-object v2, Lcom/google/googlenav/av;->c:Lcom/google/googlenav/av;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/av;)V

    iget-object v1, p0, Lbf/v;->a:Lbf/m;

    iget-object v2, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    invoke-static {v1, v2}, Lbf/m;->b(Lbf/m;Lcom/google/googlenav/ai;)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/v;->a:Lbf/m;

    iget-object v0, v0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lbf/v;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/v;->a:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bp()V

    goto :goto_0
.end method
