.class public Lbf/x;
.super Lbf/bk;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V
    .locals 1

    invoke-direct/range {p0 .. p8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    const/4 v0, 0x0

    invoke-virtual {p5, v0}, Lcom/google/googlenav/n;->a(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbf/x;->b(B)V

    return-void
.end method


# virtual methods
.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aC()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method public h()V
    .locals 5

    const/16 v4, 0x2a

    const/16 v3, 0x1c

    invoke-virtual {p0}, Lbf/x;->bz()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    const/16 v2, 0xb

    if-eq v1, v2, :cond_1

    if-eq v1, v3, :cond_1

    if-eq v1, v4, :cond_1

    invoke-super {p0}, Lbf/bk;->h()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    if-ne v1, v3, :cond_2

    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "recent"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    goto :goto_0

    :cond_2
    if-ne v1, v4, :cond_3

    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    check-cast v0, Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bD()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_0
        0xd -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method protected l()V
    .locals 0

    return-void
.end method
