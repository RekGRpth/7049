.class public Lbg/b;
.super Lbf/am;
.source "SourceFile"

# interfaces
.implements Lbf/j;
.implements Lbf/k;
.implements Lcom/google/android/maps/driveabout/vector/y;
.implements Ln/u;


# static fields
.field public static final k:LaN/Y;


# instance fields
.field private final l:Lcom/google/common/collect/Q;

.field private final m:Ljava/util/Map;

.field private n:Lcom/google/android/maps/driveabout/vector/c;

.field private final o:Ljava/lang/Object;

.field private p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private q:Lcom/google/android/maps/driveabout/vector/f;

.field private r:Lcom/google/android/maps/driveabout/vector/aA;

.field private final s:Lbg/k;

.field private t:Lcom/google/android/maps/driveabout/vector/az;

.field private u:Z

.field private final v:Ljava/lang/Object;

.field private final w:Ljava/lang/Object;

.field private x:J

.field private y:J

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x12

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, Lbg/b;->k:LaN/Y;

    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V
    .locals 3

    invoke-direct/range {p0 .. p13}, Lbf/am;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V

    invoke-static {}, Lcom/google/common/collect/am;->e()Lcom/google/common/collect/am;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbg/b;->o:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbg/b;->v:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbg/b;->x:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbg/b;->y:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbg/b;->z:Z

    new-instance v0, Lbg/k;

    invoke-direct {v0}, Lbg/k;-><init>()V

    iput-object v0, p0, Lbg/b;->s:Lbg/k;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Ln/s;->a(Ln/u;)V

    :cond_0
    new-instance v0, Lbg/c;

    const-string v1, "LayerTileCompactor"

    invoke-direct {v0, p0, v1}, Lbg/c;-><init>(Lbg/b;Ljava/lang/String;)V

    sget-object v1, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v2, Lbg/d;

    invoke-direct {v2, p0, v0}, Lbg/d;-><init>(Lbg/b;LR/c;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;Lbg/c;)V
    .locals 0

    invoke-direct/range {p0 .. p13}, Lbg/b;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V

    return-void
.end method

.method static synthetic a(Lbg/b;J)J
    .locals 0

    iput-wide p1, p0, Lbg/b;->y:J

    return-wide p1
.end method

.method static synthetic a(Lbg/b;Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/c;
    .locals 0

    iput-object p1, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    return-object p1
.end method

.method private static a(Landroid/view/View;)Lcom/google/android/maps/driveabout/vector/f;
    .locals 3

    const/16 v2, 0x8

    const v0, 0x7f100048

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_0

    const v1, 0x7f10001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_0

    instance-of v0, v0, Lcom/google/googlenav/ui/android/BubbleButton;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/af;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/af;-><init>(Landroid/view/View;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic a(Lbg/b;Lbf/i;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lbg/b;->a(Lbf/i;I)V

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V
    .locals 0

    return-void
.end method

.method private a(Lbf/i;Lcom/google/googlenav/E;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    invoke-interface {p2}, Lcom/google/googlenav/E;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Lcom/google/googlenav/E;->c()B

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lbf/bk;

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {p1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/aZ;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2}, Lcom/google/googlenav/E;->e()Z

    move-result v1

    goto :goto_1
.end method

.method static synthetic a(Lbg/b;)Z
    .locals 1

    iget-boolean v0, p0, Lbg/b;->u:Z

    return v0
.end method

.method static synthetic a(Lbg/b;Lbf/i;Lcom/google/googlenav/E;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lbg/b;->a(Lbf/i;Lcom/google/googlenav/E;)Z

    move-result v0

    return v0
.end method

.method private ad()V
    .locals 1

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    :cond_0
    return-void
.end method

.method private ae()V
    .locals 3

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g()LS/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->L()Z

    move-result v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->at()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LS/b;->a(ZZ)V

    :cond_0
    return-void
.end method

.method private af()V
    .locals 2

    const-string v0, "LayerTransit"

    invoke-virtual {p0, v0}, Lbg/b;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbg/q;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbg/q;->bH()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    check-cast v1, Lo/o;

    invoke-virtual {v0, v1}, Lbg/q;->a(Lo/o;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lbg/b;->h(Lbf/i;)V

    goto :goto_0
.end method

.method private ag()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    instance-of v0, v0, Lbf/O;

    if-eqz v0, :cond_0

    const-string v0, "dd"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "1"

    goto :goto_0
.end method

.method private static b(Lbf/i;I)Lbg/j;
    .locals 11

    const/4 v9, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1}, Lbf/i;->c(I)Lcom/google/googlenav/e;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/e;->b()I

    move-result v5

    invoke-virtual {v1}, Lcom/google/googlenav/e;->c()I

    move-result v6

    invoke-virtual {v1}, Lcom/google/googlenav/e;->d()Z

    move-result v9

    :goto_1
    instance-of v0, p0, Lbf/aA;

    if-eqz v0, :cond_3

    move-object v1, v2

    check-cast v1, Lbf/aF;

    new-instance v0, Lbg/n;

    invoke-virtual {p0, v2}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v7

    invoke-virtual {p0, v2}, Lbf/i;->c(Lcom/google/googlenav/E;)I

    move-result v8

    invoke-virtual {v1}, Lbf/aF;->h()I

    move-result v10

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lbg/n;-><init>(Lbf/i;Lcom/google/googlenav/E;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIZI)V

    move-object v4, v0

    :goto_2
    instance-of v0, p0, Lbf/O;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    :cond_2
    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v5

    move-object v1, p0

    check-cast v1, Lbf/O;

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lbg/j;->b(Z)V

    new-instance v0, Lbg/f;

    move-object v2, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lbg/f;-><init>(Lbf/O;Lbf/i;ILbg/j;Lo/T;)V

    invoke-virtual {v4, v0}, Lbg/j;->a(LF/I;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lbg/j;

    invoke-virtual {p0, v2}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v7

    invoke-virtual {p0, v2}, Lbf/i;->c(Lcom/google/googlenav/E;)I

    move-result v8

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lbg/j;-><init>(Lbf/i;Lcom/google/googlenav/E;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIZ)V

    move-object v4, v0

    goto :goto_2

    :cond_4
    move v6, v9

    move v5, v9

    move-object v3, v4

    goto :goto_1
.end method

.method static synthetic b(Lbg/b;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbg/b;->v:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lbg/b;Lbf/i;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lbg/b;->a(Lbf/i;I)V

    return-void
.end method

.method static synthetic c(Lbg/b;)J
    .locals 2

    iget-wide v0, p0, Lbg/b;->y:J

    return-wide v0
.end method

.method static synthetic d(Lbg/b;)LaN/p;
    .locals 1

    iget-object v0, p0, Lbg/b;->e:LaN/p;

    return-object v0
.end method

.method static synthetic e(Lbg/b;)J
    .locals 2

    iget-wide v0, p0, Lbg/b;->x:J

    return-wide v0
.end method

.method static synthetic f(Lbg/b;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic g(Lbg/b;)Lbf/C;
    .locals 1

    invoke-virtual {p0}, Lbg/b;->h()Lbf/C;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbg/b;->b(Ljava/lang/String;)Lbf/y;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lbg/q;

    invoke-virtual {v0, p1}, Lbg/q;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/layer/m;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2, v0, v2}, Lbg/b;->a(Lcom/google/googlenav/layer/m;ZZZ)Lbf/y;

    goto :goto_0
.end method

.method static synthetic h(Lbg/b;)Lcom/google/common/collect/Q;
    .locals 1

    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    return-object v0
.end method

.method static synthetic i(Lbg/b;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbg/b;->o:Ljava/lang/Object;

    return-object v0
.end method

.method private o(Lbf/i;)V
    .locals 8

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbf/i;->e()[Lcom/google/googlenav/ui/aI;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1}, Lbg/b;->p(Lbf/i;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    array-length v1, v3

    invoke-static {v1}, Lcom/google/common/collect/Maps;->a(I)Ljava/util/HashMap;

    move-result-object v4

    array-length v5, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_5

    aget-object v6, v3, v2

    if-eqz v6, :cond_4

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbg/a;

    :cond_2
    if-nez v1, :cond_3

    new-instance v1, Lbg/a;

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v1, v7, v6}, Lbg/a;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/aI;)V

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v7, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_3
    invoke-interface {v4, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbg/a;

    iget-object v2, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private p(Lbf/i;)V
    .locals 3

    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbg/a;

    iget-object v2, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private q(Lbf/i;)V
    .locals 4

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v2, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    sget-object v2, Lah/c;->a:Lah/c;

    invoke-direct {p0, v0, v1, v2}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_2

    move-object v0, p1

    check-cast v0, Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v2, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    sget-object v2, Lah/c;->c:Lah/c;

    invoke-direct {p0, v0, v1, v2}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    if-ltz v2, :cond_3

    invoke-virtual {p1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/lang/Object;)LF/H;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    iget-object v1, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v3, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    sget-object v3, Lah/c;->b:Lah/c;

    invoke-direct {p0, v2, v1, v3}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V

    :goto_1
    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lbg/b;->b(Lbf/i;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public F()Z
    .locals 12

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lbg/b;->f:LaN/u;

    invoke-virtual {v0}, LaN/u;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iget-object v0, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->g()[LaN/P;

    move-result-object v4

    array-length v6, v4

    move v0, v5

    :goto_1
    if-ge v0, v6, :cond_2

    aget-object v7, v4, v0

    iget-object v8, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v8}, LaN/p;->a()LaN/D;

    move-result-object v8

    int-to-long v9, v5

    add-long/2addr v9, v1

    invoke-virtual {v8, v7, v3, v9, v10}, LaN/D;->a(LaN/P;ZJ)Ljava/util/Vector;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->k()V

    invoke-super {p0}, Lbf/am;->F()Z

    move-result v0

    iget-boolean v1, p0, Lbg/b;->z:Z

    if-eqz v1, :cond_3

    iput-boolean v5, p0, Lbg/b;->z:Z

    move v0, v3

    :cond_3
    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lbg/b;->d:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lbg/b;->ab()V

    :cond_4
    if-eqz v0, :cond_0

    iget-object v1, p0, Lbg/b;->o:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->f()Lo/D;

    move-result-object v0

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v2

    if-eqz v0, :cond_5

    invoke-virtual {v2, v0}, Ln/s;->d(Lo/D;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lbg/b;->U()V

    :cond_5
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v4, v5

    move v2, v5

    :goto_2
    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v4, v0, :cond_c

    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    iget-object v1, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v1, v0}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/vector/A;

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    invoke-virtual {p0, v0}, Lbg/b;->n(Lbf/i;)V

    invoke-virtual {v0}, Lbf/i;->ax()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v0}, Lbf/i;->aj()Z

    move-result v6

    if-eqz v6, :cond_8

    move v6, v3

    :goto_4
    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v9

    move v7, v5

    move v8, v2

    :goto_5
    invoke-interface {v9}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge v7, v2, :cond_e

    invoke-interface {v9, v7}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/lang/Object;)LF/H;

    move-result-object v2

    check-cast v2, Lbg/j;

    invoke-interface {v10}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v11

    if-nez v11, :cond_9

    if-eqz v2, :cond_9

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(LF/H;)V

    :cond_7
    :goto_6
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_5

    :cond_8
    move v6, v5

    goto :goto_4

    :cond_9
    invoke-virtual {v0, v7}, Lbf/i;->c(I)Lcom/google/googlenav/e;

    move-result-object v11

    if-eqz v2, :cond_7

    invoke-virtual {v2, v11}, Lbg/j;->a(Lcom/google/googlenav/e;)Z

    move-result v11

    if-nez v11, :cond_a

    invoke-virtual {v2}, Lbg/j;->e()Lo/T;

    move-result-object v11

    invoke-interface {v10}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v10

    invoke-static {v10}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v10

    invoke-virtual {v11, v10}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    :cond_a
    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(LF/H;)V

    invoke-static {v0, v7}, Lbg/b;->b(Lbf/i;I)Lbg/j;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    :cond_b
    if-eqz v6, :cond_7

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    if-ne v2, v7, :cond_7

    move v8, v3

    goto :goto_6

    :cond_c
    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    if-eqz v2, :cond_d

    invoke-virtual {v0}, Lbf/i;->ak()Lcom/google/googlenav/ui/view/d;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbg/b;->a(Lbf/i;Lcom/google/googlenav/ui/view/d;)V

    :cond_d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbg/b;->x:J

    iget-object v1, p0, Lbg/b;->w:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-direct {p0}, Lbg/b;->ad()V

    move v5, v3

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_e
    move v1, v8

    goto/16 :goto_3
.end method

.method public G()V
    .locals 2

    iget-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/az;->e()Z

    move-result v0

    iget-object v1, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v1}, LaN/p;->a()LaN/D;

    move-result-object v1

    invoke-virtual {v1}, LaN/D;->m()Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/az;->b(Z)V

    invoke-direct {p0}, Lbg/b;->ad()V

    :cond_0
    return-void
.end method

.method protected T()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbg/b;->a(Z)Lbf/by;

    invoke-virtual {p0}, Lbg/b;->s()V

    return-void
.end method

.method public U()V
    .locals 1

    invoke-super {p0}, Lbf/am;->U()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->z:Z

    invoke-virtual {p0}, Lbg/b;->F()Z

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;
    .locals 10

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lbg/b;->a(I)V

    new-instance v0, Lbf/ak;

    iget-object v1, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbg/b;->e:LaN/p;

    iget-object v3, p0, Lbg/b;->f:LaN/u;

    iget-object v4, p0, Lbg/b;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbg/b;->h:LaN/k;

    move-object v6, p1

    move v7, p2

    move v8, p3

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lbf/ak;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/ai;ZBZ)V

    iget-object v1, p0, Lbg/b;->o:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v2}, Lbg/b;->b(Lbf/i;I)Lbg/j;

    move-result-object v2

    iput-object v2, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0, p5}, Lbg/b;->a(Lbf/i;Z)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast p1, Lcom/google/googlenav/W;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {p0, v1}, Lbg/b;->g(Ljava/lang/String;)V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/layer/m;)Lbf/bE;
    .locals 8

    new-instance v0, Lbg/o;

    iget-object v1, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbg/b;->e:LaN/p;

    iget-object v3, p0, Lbg/b;->f:LaN/u;

    iget-object v4, p0, Lbg/b;->g:Lcom/google/googlenav/ui/X;

    new-instance v6, LaN/k;

    invoke-direct {v6}, LaN/k;-><init>()V

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lbg/o;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .locals 9

    new-instance v0, Lbg/q;

    iget-object v1, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbg/b;->e:LaN/p;

    iget-object v3, p0, Lbg/b;->f:LaN/u;

    iget-object v4, p0, Lbg/b;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbg/b;->h:LaN/k;

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-object v5, p1

    move v8, p2

    invoke-direct/range {v0 .. v8}, Lbg/q;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;Z)V

    return-object v0
.end method

.method public a(Lbf/i;)V
    .locals 1

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p()V

    invoke-direct {p0, p1}, Lbg/b;->q(Lbf/i;)V

    goto :goto_0
.end method

.method public a(Lbf/i;Lcom/google/googlenav/ui/view/d;)V
    .locals 2

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    invoke-virtual {p2}, Lcom/google/googlenav/ui/view/d;->d()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbg/b;->a(Landroid/view/View;)Lcom/google/android/maps/driveabout/vector/f;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/h;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    move-object v0, p1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/h;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/f;->a(Lcom/google/android/maps/driveabout/vector/h;)V

    :cond_2
    invoke-direct {p0, p1}, Lbg/b;->q(Lbf/i;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V
    .locals 11

    const/4 v10, 0x2

    const v9, 0x338cc6ef

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v0, p0, Lbg/b;->s:Lbg/k;

    iget-object v1, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1}, Lbg/k;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0}, Lcom/google/common/collect/Q;->clear()V

    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iput-object v8, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    iput-object v8, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    iput-object v8, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    iput-object v8, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->d(Z)V

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    iget-object v3, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v3, v0}, Lcom/google/common/collect/Q;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v3

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    if-ne v4, v10, :cond_3

    invoke-virtual {v3, v7}, Lcom/google/android/maps/driveabout/vector/A;->b(I)V

    :goto_2
    invoke-virtual {p1, v10}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBaseDistancePenaltyFactorForLabelOverlay(I)V

    iget-object v4, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v4, v0, v3}, Lcom/google/common/collect/Q;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v4, v3}, Lbg/k;->a(Lcom/google/android/maps/driveabout/vector/A;)V

    :cond_2
    invoke-virtual {p0, v0}, Lbg/b;->c(Lbf/i;)V

    invoke-direct {p0, v0}, Lbg/b;->o(Lbf/i;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(I)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v7}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c(Z)Lcom/google/android/maps/driveabout/vector/aA;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0, v7}, Lcom/google/android/maps/driveabout/vector/aA;->b(Z)V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/google/android/maps/driveabout/vector/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->c(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f02045d

    const v6, 0x7f02045e

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->c(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f020459

    const v6, 0x7f02045a

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f02045f

    const v6, 0x7f020460

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x73217bce

    invoke-virtual {v4, v5, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v5, 0x7f02045b

    const v6, 0x7f02045c

    invoke-virtual {v2, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v5, 0x73217bce

    invoke-virtual {v2, v5, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f020461

    const v6, 0x7f020462

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x73aaaaaa

    const v6, 0x33cccccc

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a([Lcom/google/android/maps/driveabout/vector/aC;)V

    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const v2, 0x7f0b00ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f0c0005

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const v4, 0x7f0c0006

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(FII)V

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->x:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const/high16 v1, 0x41600000

    const/high16 v2, 0x41200000

    const v3, 0x3f4ccccd

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(FFF)V

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    new-instance v1, Lbg/e;

    invoke-direct {v1, p0}, Lbg/e;-><init>(Lbg/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->d(I)V

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/az;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, Lbg/l;

    invoke-direct {v1, p0, v8}, Lbg/l;-><init>(Lbg/b;Lbg/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lbg/b;->U()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->d()V

    goto :goto_0
.end method

.method public a(Ln/s;)V
    .locals 0

    return-void
.end method

.method public a(Ln/s;Lo/y;)V
    .locals 1

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->z:Z

    invoke-direct {p0}, Lbg/b;->ad()V

    goto :goto_0
.end method

.method public a(Lo/S;)V
    .locals 2

    invoke-virtual {p1}, Lo/S;->a()Lo/T;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbf/C;->aa()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lbg/b;->h(Lbf/i;)V

    :cond_0
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    :cond_1
    return-void
.end method

.method protected a(Lbf/i;ZZ)Z
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lbf/am;->a(Lbf/i;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    new-instance v1, Lbg/m;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lbg/m;-><init>(Lbg/b;Lbg/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    iget-object v1, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v1, p1, v0}, Lcom/google/common/collect/Q;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lbg/b;->c(Lbf/i;)V

    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v1, v0}, Lbg/k;->a(Lcom/google/android/maps/driveabout/vector/A;)V

    :cond_1
    invoke-virtual {p1, p0}, Lbf/i;->a(Lbf/k;)V

    :cond_2
    invoke-virtual {p1, p0}, Lbf/i;->a(Lbf/j;)V

    invoke-direct {p0, p1}, Lbg/b;->o(Lbf/i;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public ab()V
    .locals 2

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    invoke-direct {p0}, Lbg/b;->ad()V

    :cond_0
    return-void
.end method

.method public ac()Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .locals 1

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    return-object v0
.end method

.method public b(Lbf/i;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p()V

    :cond_0
    iput-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {p0}, Lbg/b;->af()V

    iget-object v1, p0, Lbg/b;->o:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 5

    const/16 v0, 0x12

    invoke-direct {p0}, Lbg/b;->ag()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "a=d"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v1

    invoke-static {v1}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v1

    sget-object v2, Lbg/b;->k:LaN/Y;

    invoke-virtual {v0, v1, v2}, LaN/u;->d(LaN/B;LaN/Y;)V

    return-void
.end method

.method public b(Ln/s;)V
    .locals 1

    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->z:Z

    invoke-direct {p0}, Lbg/b;->ad()V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->u:Z

    iget-object v1, p0, Lbg/b;->v:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbg/b;->v:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lbg/b;->w:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public c(Lbf/i;)V
    .locals 6

    invoke-virtual {p0, p1}, Lbg/b;->l(Lbf/i;)V

    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v4

    if-ge v1, v4, :cond_3

    invoke-interface {v2, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    new-instance v1, Lbg/h;

    invoke-direct {v1, p0, v3, v2, p1}, Lbg/h;-><init>(Lbg/b;Ljava/util/Map;Lcom/google/googlenav/F;Lbf/i;)V

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/J;Z)V

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p1, v1}, Lbg/b;->b(Lbf/i;I)Lbg/j;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {v2, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lbg/b;->a(Lbf/i;Lcom/google/googlenav/E;)Z

    move-result v1

    invoke-virtual {v4, v1}, LF/H;->a(Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    goto :goto_1
.end method

.method protected d()V
    .locals 0

    invoke-super {p0}, Lbf/am;->d()V

    invoke-direct {p0}, Lbg/b;->ae()V

    return-void
.end method

.method protected d(Z)V
    .locals 1

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setShouldUpdateFeatureCluster(Z)V

    :cond_0
    return-void
.end method

.method public e(Lbf/i;)V
    .locals 2

    invoke-super {p0, p1}, Lbf/am;->e(Lbf/i;)V

    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v1, v0}, Lbg/k;->c(Lcom/google/android/maps/driveabout/vector/A;)V

    :cond_0
    invoke-direct {p0}, Lbg/b;->ae()V

    return-void
.end method

.method protected i(Lbf/i;)V
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lbf/am;->i(Lbf/i;)V

    invoke-virtual {p1, v0}, Lbf/i;->a(Lbf/k;)V

    invoke-virtual {p1, v0}, Lbf/i;->a(Lbf/j;)V

    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v1, v0}, Lbg/k;->b(Lcom/google/android/maps/driveabout/vector/A;)V

    :cond_0
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lbg/b;->p(Lbf/i;)V

    return-void
.end method

.method public n(Lbf/i;)V
    .locals 0

    invoke-super {p0, p1}, Lbf/am;->n(Lbf/i;)V

    invoke-direct {p0, p1}, Lbg/b;->o(Lbf/i;)V

    return-void
.end method
