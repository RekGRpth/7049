.class Lbg/c;
.super LR/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbg/b;


# direct methods
.method constructor <init>(Lbg/b;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lbg/c;->a:Lbg/b;

    invoke-direct {p0, p2}, LR/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public l()V
    .locals 8

    const-wide/16 v6, 0xbb8

    :cond_0
    :goto_0
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->a(Lbg/b;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->b(Lbg/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v2

    :try_start_1
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->c(Lbg/b;)J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->b(Lbg/b;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->a(Lbg/b;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v2}, Lbg/b;->c(Lbg/b;)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_2

    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v2}, Lbg/b;->d(Lbg/b;)LaN/p;

    move-result-object v2

    invoke-virtual {v2}, LaN/p;->a()LaN/D;

    move-result-object v2

    invoke-virtual {v2}, LaN/D;->c()V

    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    add-long v3, v0, v6

    invoke-static {v2, v3, v4}, Lbg/b;->a(Lbg/b;J)J

    :cond_2
    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v2}, Lbg/b;->e(Lbg/b;)J

    move-result-wide v2

    add-long/2addr v2, v6

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->f(Lbg/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->f(Lbg/b;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    :try_start_4
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method
