.class Lbg/k;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/List;

.field private e:Lcom/google/android/maps/driveabout/vector/VectorMapView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbg/k;->d:Ljava/util/List;

    return-void
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lbg/k;->e:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/k;->e:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    :cond_0
    return-void
.end method

.method private declared-synchronized h()[Lcom/google/android/maps/driveabout/vector/A;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    iget-object v1, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/maps/driveabout/vector/A;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/A;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/A;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/A;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/A;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lbg/k;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lbg/k;->e:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez p1, :cond_0

    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;FFLo/T;LC/a;I)V
    .locals 10

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v8

    array-length v9, v8

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_0

    aget-object v0, v8, v7

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/util/List;FFLo/T;LC/a;I)V

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/A;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public a_(FFLo/T;LC/a;)Z
    .locals 3

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/A;->a_(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized b(Lcom/google/android/maps/driveabout/vector/A;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lbg/k;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(FFLC/a;)Z
    .locals 3

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/A;->b(FFLC/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized b(LC/a;LD/a;)Z
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2}, Lcom/google/android/maps/driveabout/vector/A;->b(LC/a;LD/a;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(LD/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/A;->c(LD/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized c(Lcom/google/android/maps/driveabout/vector/A;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lbg/k;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(FFLo/T;LC/a;)Z
    .locals 3

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d(FFLo/T;LC/a;)Z
    .locals 3

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/A;->d(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public i_()V
    .locals 2

    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/A;->i_()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public j_()Z
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/A;->j_()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public l()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
