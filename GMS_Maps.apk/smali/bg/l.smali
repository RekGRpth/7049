.class Lbg/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/e;


# instance fields
.field final synthetic a:Lbg/b;


# direct methods
.method private constructor <init>(Lbg/b;)V
    .locals 0

    iput-object p1, p0, Lbg/l;->a:Lbg/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lbg/b;Lbg/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lbg/l;-><init>(Lbg/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/d;Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 12

    const/4 v6, 0x0

    const/4 v3, 0x1

    instance-of v0, p2, LF/l;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbg/l;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->i(Lbg/b;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lbg/l;->a:Lbg/b;

    invoke-static {v0, p2}, Lbg/b;->a(Lbg/b;Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/c;

    check-cast p2, LF/l;

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v0

    invoke-virtual {v0}, Lo/U;->a()Lo/o;

    move-result-object v0

    invoke-virtual {v0}, Lo/o;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v1

    invoke-virtual {v1}, Lo/U;->b()Lo/aq;

    move-result-object v1

    invoke-virtual {v1}, Lo/aq;->k()Lo/aB;

    move-result-object v2

    sget-object v1, Lo/av;->c:Lo/av;

    invoke-virtual {v2, v1}, Lo/aB;->b(Lo/av;)Z

    move-result v4

    new-instance v1, Lcom/google/googlenav/W;

    invoke-virtual {p2}, LF/l;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_0

    const-string v8, ":"

    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v8

    invoke-virtual {v8}, Lo/U;->c()Lo/T;

    move-result-object v8

    invoke-static {v8}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v8

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v9

    invoke-virtual {v9}, Lo/U;->d()Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0xa

    const/16 v11, 0x20

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v5, v0, v8, v9}, Lcom/google/googlenav/W;-><init>(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v0

    invoke-virtual {v0}, Lo/U;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v0

    invoke-virtual {v0}, Lo/U;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(Ljava/lang/String;)V

    :cond_1
    if-eqz v4, :cond_2

    sget-object v0, Lo/av;->c:Lo/av;

    invoke-virtual {v2, v0}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v0

    check-cast v0, Lo/E;

    invoke-virtual {v0}, Lo/E;->c()Lo/D;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(Lo/D;)V

    :cond_2
    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v0

    invoke-virtual {v0}, Lo/U;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->b(Z)V

    :cond_3
    const/16 v0, 0x14

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(B)V

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v0

    invoke-virtual {v0}, Lo/U;->g()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->f(I)V

    invoke-virtual {p2}, LF/l;->a()Lo/U;

    move-result-object v0

    invoke-virtual {v0}, Lo/U;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->c(Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lbg/l;->a:Lbg/b;

    invoke-virtual {v0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbg/l;->a:Lbg/b;

    invoke-virtual {v0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_4

    move v6, v3

    :cond_4
    iget-object v0, p0, Lbg/l;->a:Lbg/b;

    if-eqz v6, :cond_5

    const/4 v3, 0x2

    :cond_5
    invoke-virtual/range {v0 .. v5}, Lbg/b;->a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;

    monitor-exit v7

    :cond_6
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
