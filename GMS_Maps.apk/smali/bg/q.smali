.class public Lbg/q;
.super Lbf/y;
.source "SourceFile"


# instance fields
.field private final C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private D:Lcom/google/android/maps/driveabout/vector/aZ;

.field private E:Lo/aD;

.field private F:Lo/o;

.field private final G:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;Z)V
    .locals 1

    invoke-direct/range {p0 .. p6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    iput-object p7, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v0, Lo/aE;

    invoke-direct {v0}, Lo/aE;-><init>()V

    invoke-virtual {v0}, Lo/aE;->b()Lo/aD;

    move-result-object v0

    iput-object v0, p0, Lbg/q;->E:Lo/aD;

    iput-boolean p8, p0, Lbg/q;->G:Z

    return-void
.end method

.method private bK()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/q;->D:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v1, p0, Lbg/q;->E:Lo/aD;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lo/at;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    :cond_0
    return-void
.end method

.method private bL()Z
    .locals 1

    iget-object v0, p0, Lbg/q;->F:Lo/o;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ab;)V
    .locals 2

    new-instance v0, Lo/aE;

    invoke-direct {v0}, Lo/aE;-><init>()V

    iget-object v1, p0, Lbg/q;->E:Lo/aD;

    invoke-virtual {v0, v1}, Lo/aE;->a(Lo/aD;)Lo/aE;

    move-result-object v0

    invoke-virtual {v0}, Lo/aE;->a()Lo/aE;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ab;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lo/aE;->a(I)Lo/aE;

    move-result-object v0

    invoke-virtual {v0}, Lo/aE;->b()Lo/aD;

    move-result-object v0

    iput-object v0, p0, Lbg/q;->E:Lo/aD;

    invoke-direct {p0}, Lbg/q;->bK()V

    return-void
.end method

.method public a(Lo/o;)V
    .locals 2

    new-instance v0, Lo/aE;

    invoke-direct {v0}, Lo/aE;-><init>()V

    iget-object v1, p0, Lbg/q;->E:Lo/aD;

    invoke-virtual {v0, v1}, Lo/aE;->a(Lo/aD;)Lo/aE;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/aE;->a(Lo/o;)Lo/aE;

    move-result-object v0

    invoke-virtual {v0}, Lo/aE;->b()Lo/aD;

    move-result-object v0

    iput-object v0, p0, Lbg/q;->E:Lo/aD;

    invoke-direct {p0}, Lbg/q;->bK()V

    iput-object p1, p0, Lbg/q;->F:Lo/o;

    return-void
.end method

.method public aE()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aT()Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbg/q;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->q()Lo/o;

    move-result-object v0

    iput-object v0, p0, Lbg/q;->F:Lo/o;

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lbg/q;->D:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0}, Lbg/q;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->q()Lo/o;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbg/q;->a(Lo/o;)V

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v2, p0, Lbg/q;->D:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    invoke-direct {p0}, Lbg/q;->bL()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lbg/q;->k(Z)V

    return v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aU()V
    .locals 2

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/q;->D:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbg/q;->k(Z)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    check-cast v0, Lo/o;

    invoke-virtual {p0, v0}, Lbg/q;->a(Lo/o;)V

    :cond_0
    :try_start_0
    invoke-static {p1}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbg/q;->a(Lo/o;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public bJ()Z
    .locals 1

    invoke-direct {p0}, Lbg/q;->bL()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected k(Z)V
    .locals 2

    invoke-super {p0, p1}, Lbf/y;->k(Z)V

    iget-object v0, p0, Lbg/q;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbg/q;->bH()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->g(Z)V

    iget-object v0, p0, Lbg/q;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->x()Lbf/bU;

    move-result-object v0

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbg/q;->G:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbg/q;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ay()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/bU;->bJ()V

    goto :goto_0
.end method
