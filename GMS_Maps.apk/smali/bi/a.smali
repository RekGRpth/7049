.class public final Lbi/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lbi/t;

.field public final b:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbi/b;

    invoke-direct {v0}, Lbi/b;-><init>()V

    sput-object v0, Lbi/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIF)V
    .locals 1

    new-instance v0, Lbi/t;

    invoke-direct {v0, p1, p2}, Lbi/t;-><init>(II)V

    invoke-direct {p0, v0, p3}, Lbi/a;-><init>(Lbi/t;F)V

    return-void
.end method

.method public constructor <init>(Lbi/t;F)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000

    invoke-static {p2, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Progress out of range [0.0, 1.0] :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lbi/a;->a:Lbi/t;

    iput p2, p0, Lbi/a;->b:F

    return-void
.end method


# virtual methods
.method public a(Lbi/a;)I
    .locals 2

    iget-object v0, p0, Lbi/a;->a:Lbi/t;

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->a(Lbi/t;)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lbi/a;->b:F

    iget v1, p1, Lbi/a;->b:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lbi/a;

    invoke-virtual {p0, p1}, Lbi/a;->a(Lbi/a;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Lbi/a;

    if-eqz v1, :cond_0

    check-cast p1, Lbi/a;

    invoke-virtual {p0, p1}, Lbi/a;->a(Lbi/a;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0}, Lbi/t;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x899

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lbi/a;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "index"

    iget-object v2, p0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "progress"

    iget v2, p0, Lbi/a;->b:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lbi/a;->a:Lbi/t;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lbi/a;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method
