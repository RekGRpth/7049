.class public Lbi/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lax/w;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lax/w;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbi/e;->a:Lax/w;

    return-void
.end method

.method private a(Lbi/g;)F
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lbi/g;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->r()F

    move-result v2

    add-float/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method private a(Lax/t;I)V
    .locals 4

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v1, Lbi/g;

    new-instance v2, Lbi/p;

    iget-object v3, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v3, p1, p2}, Lbi/p;-><init>(Lax/w;Lax/t;I)V

    invoke-direct {v1, v2}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lax/t;ILjava/util/List;)V
    .locals 4

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v1, Lbi/g;

    new-instance v2, Lbi/m;

    iget-object v3, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v3, p1, p2, p3}, Lbi/m;-><init>(Lax/w;Lax/t;ILjava/util/List;)V

    invoke-direct {v1, v2}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lax/t;Lax/t;ILcom/google/common/collect/P;)V
    .locals 9

    const/4 v6, 0x0

    new-instance v7, Lbi/g;

    new-instance v0, Lbi/j;

    iget-object v1, p0, Lbi/e;->a:Lax/w;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lbi/j;-><init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V

    invoke-direct {v7, v0}, Lbi/g;-><init>(Lbi/h;)V

    invoke-virtual {p2}, Lax/t;->Q()[Lax/u;

    move-result-object v8

    :goto_0
    array-length v0, v8

    if-ge v6, v0, :cond_1

    aget-object v2, v8, v6

    add-int/lit8 v0, v6, 0x1

    array-length v1, v8

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v6, 0x1

    aget-object v3, v8, v0

    :goto_1
    new-instance v0, Lbi/r;

    add-int/lit8 v1, v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object v1, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lbi/r;-><init>(Lax/t;Lax/u;Lax/u;ILjava/util/List;)V

    invoke-virtual {v7, v0}, Lbi/g;->a(Lbi/h;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lax/t;Lax/t;ILjava/util/List;)V
    .locals 8

    iget-object v6, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v7, Lbi/g;

    new-instance v0, Lbi/l;

    iget-object v1, p0, Lbi/e;->a:Lax/w;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbi/l;-><init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V

    invoke-direct {v7, v0}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private b()V
    .locals 7

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->a(Lbi/g;)F

    move-result v5

    const/4 v0, 0x0

    move v3, v2

    move v4, v0

    :goto_1
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->a()I

    move-result v0

    if-ge v3, v0, :cond_0

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0, v3}, Lbi/g;->a(I)Lbi/h;

    move-result-object v0

    sub-float v6, v5, v4

    invoke-virtual {v0, v6}, Lbi/h;->c(F)V

    invoke-virtual {v0}, Lbi/h;->r()F

    move-result v0

    add-float/2addr v4, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Lax/t;I)V
    .locals 4

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v1, Lbi/g;

    new-instance v2, Lbi/k;

    iget-object v3, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v3, p1, p2}, Lbi/k;-><init>(Lax/w;Lax/t;I)V

    invoke-direct {v1, v2}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private b(Lax/t;Lax/t;ILjava/util/List;)V
    .locals 8

    iget-object v6, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v7, Lbi/g;

    new-instance v0, Lbi/o;

    iget-object v1, p0, Lbi/e;->a:Lax/w;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbi/o;-><init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V

    invoke-direct {v7, v0}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private b(Lbi/g;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v1

    invoke-virtual {v1}, Lbi/h;->b()Lbi/q;

    move-result-object v2

    sget-object v3, Lbi/q;->b:Lbi/q;

    if-ne v2, v3, :cond_1

    invoke-virtual {v1}, Lbi/h;->a()Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->x()I

    move-result v1

    invoke-direct {p0, p1}, Lbi/e;->a(Lbi/g;)F

    move-result v2

    :goto_0
    invoke-virtual {p1}, Lbi/g;->a()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v3

    invoke-virtual {v3}, Lbi/h;->p()Z

    move-result v4

    if-nez v4, :cond_0

    if-lez v1, :cond_0

    int-to-float v4, v1

    invoke-virtual {v3}, Lbi/h;->r()F

    move-result v3

    mul-float/2addr v3, v4

    div-float/2addr v3, v2

    float-to-int v3, v3

    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v4

    invoke-virtual {v4, v3}, Lbi/h;->a(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(Lbi/g;)I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lbi/g;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->q()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method private c()V
    .locals 9

    const/4 v2, 0x0

    invoke-direct {p0}, Lbi/e;->e()V

    invoke-direct {p0}, Lbi/e;->d()I

    move-result v7

    move v1, v2

    move v3, v2

    :goto_0
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->c(Lbi/g;)I

    move-result v8

    move v4, v2

    move v5, v2

    :goto_1
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->a()I

    move-result v0

    if-ge v4, v0, :cond_0

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0, v4}, Lbi/g;->a(I)Lbi/h;

    move-result-object v0

    sub-int v6, v8, v5

    invoke-virtual {v0, v6}, Lbi/h;->e(I)V

    sub-int v6, v7, v3

    invoke-virtual {v0, v6}, Lbi/h;->f(I)V

    invoke-virtual {v0}, Lbi/h;->q()I

    move-result v0

    add-int v6, v3, v0

    add-int v3, v5, v0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v3

    move v3, v6

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private d()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->c(Lbi/g;)I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return v2
.end method

.method private e()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->b(Lbi/g;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private f()V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x0

    new-instance v2, Lbi/f;

    iget-object v0, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v0}, Lbi/f;-><init>(Lax/b;)V

    invoke-virtual {v2}, Lbi/f;->j()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Lbi/f;->d()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->a()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v4

    invoke-virtual {v2}, Lbi/f;->i()Lcom/google/common/collect/P;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v0, v3, v4, v5}, Lbi/e;->a(Lax/t;Lax/t;ILjava/util/List;)V

    invoke-virtual {v2}, Lbi/f;->h()V

    :goto_0
    invoke-virtual {v2}, Lbi/f;->j()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v4

    invoke-virtual {v2}, Lbi/f;->i()Lcom/google/common/collect/P;

    move-result-object v5

    invoke-direct {p0, v0, v3, v4, v5}, Lbi/e;->a(Lax/t;Lax/t;ILcom/google/common/collect/P;)V

    invoke-virtual {v2}, Lbi/f;->h()V

    :cond_0
    :goto_1
    invoke-virtual {v2}, Lbi/f;->j()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v2}, Lbi/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v3

    invoke-virtual {v2}, Lbi/f;->i()Lcom/google/common/collect/P;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lbi/e;->a(Lax/t;ILjava/util/List;)V

    :goto_2
    invoke-virtual {v2}, Lbi/f;->h()V

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->g()I

    move-result v4

    invoke-direct {p0, v0, v3, v4, v1}, Lbi/e;->b(Lax/t;Lax/t;ILjava/util/List;)V

    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v4

    invoke-virtual {v2}, Lbi/f;->i()Lcom/google/common/collect/P;

    move-result-object v5

    invoke-direct {p0, v0, v3, v4, v5}, Lbi/e;->a(Lax/t;Lax/t;ILcom/google/common/collect/P;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Lbi/f;->k()Lcom/google/googlenav/ui/view/android/rideabout/m;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    if-ne v0, v1, :cond_4

    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lbi/e;->a(Lax/t;I)V

    :cond_3
    :goto_3
    return-void

    :cond_4
    invoke-virtual {v2}, Lbi/f;->k()Lcom/google/googlenav/ui/view/android/rideabout/m;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    if-ne v0, v1, :cond_3

    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lbi/e;->b(Lax/t;I)V

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a()Lbi/d;
    .locals 2

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-direct {p0}, Lbi/e;->f()V

    invoke-direct {p0}, Lbi/e;->c()V

    invoke-direct {p0}, Lbi/e;->b()V

    new-instance v0, Lbi/d;

    iget-object v1, p0, Lbi/e;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Lbi/d;-><init>(Ljava/util/List;)V

    return-object v0
.end method
