.class public Lbi/r;
.super Lbi/s;
.source "SourceFile"


# instance fields
.field private final e:Lax/u;

.field private final f:Lax/u;


# direct methods
.method public constructor <init>(Lax/t;Lax/u;Lax/u;ILjava/util/List;)V
    .locals 6

    sget-object v1, Lbi/q;->c:Lbi/q;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lbi/s;-><init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V

    iput-object p2, p0, Lbi/r;->e:Lax/u;

    iput-object p3, p0, Lbi/r;->f:Lax/u;

    invoke-virtual {p2}, Lax/u;->e()Lax/v;

    move-result-object v0

    iput-object v0, p0, Lbi/r;->d:Lax/v;

    invoke-virtual {p0}, Lbi/r;->C()V

    return-void
.end method


# virtual methods
.method protected C()V
    .locals 1

    invoke-virtual {p0}, Lbi/r;->s()V

    invoke-virtual {p0}, Lbi/r;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbi/r;->y()F

    move-result v0

    invoke-virtual {p0, v0}, Lbi/r;->a(F)V

    :cond_0
    return-void
.end method

.method public E()Z
    .locals 1

    iget-object v0, p0, Lbi/r;->d:Lax/v;

    iget-boolean v0, v0, Lax/v;->a:Z

    return v0
.end method

.method public F()Z
    .locals 1

    iget-object v0, p0, Lbi/r;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->R()Z

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/r;->d:Lax/v;

    iget-object v0, v0, Lax/v;->i:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/r;->e:Lax/u;

    invoke-virtual {v0}, Lax/u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()I
    .locals 3

    const/16 v0, 0xf

    iget-object v1, p0, Lbi/r;->e:Lax/u;

    invoke-virtual {v1}, Lax/u;->b()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, Lbi/r;->e:Lax/u;

    invoke-virtual {v2}, Lax/u;->c()Ljava/util/Date;

    move-result-object v2

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    invoke-static {v1, v2}, Lbi/h;->a(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbi/r;->q()I

    move-result v1

    if-gt v1, v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/util/Date;
    .locals 2

    iget-object v0, p0, Lbi/r;->e:Lax/u;

    invoke-virtual {v0}, Lax/u;->b()Ljava/util/Date;

    move-result-object v0

    iget-object v1, p0, Lbi/r;->e:Lax/u;

    invoke-virtual {v1}, Lax/u;->c()Ljava/util/Date;

    move-result-object v1

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public o()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lbi/r;->f:Lax/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbi/r;->f:Lax/u;

    invoke-virtual {v0}, Lax/u;->b()Ljava/util/Date;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/r;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lbi/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public u()LaN/B;
    .locals 1

    iget-object v0, p0, Lbi/r;->e:Lax/u;

    invoke-virtual {v0}, Lax/u;->d()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public v()LaN/B;
    .locals 1

    iget-object v0, p0, Lbi/r;->f:Lax/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbi/r;->f:Lax/u;

    invoke-virtual {v0}, Lax/u;->d()LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/r;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->k()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/r;->f:Lax/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbi/r;->f:Lax/u;

    invoke-virtual {v0}, Lax/u;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/r;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
