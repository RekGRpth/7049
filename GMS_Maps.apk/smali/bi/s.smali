.class public abstract Lbi/s;
.super Lbi/h;
.source "SourceFile"


# instance fields
.field protected d:Lax/v;

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lbi/h;-><init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V

    if-eqz p3, :cond_0

    invoke-direct {p0}, Lbi/s;->C()V

    invoke-direct {p0}, Lbi/s;->E()V

    :cond_0
    return-void
.end method

.method private C()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    iget-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x58d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->g:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    iput-boolean v3, p0, Lbi/s;->f:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x58c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v2}, Lax/t;->x()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method private E()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    iget-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x58e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->h:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    iput-boolean v3, p0, Lbi/s;->e:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x58f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v2}, Lax/t;->t()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public D()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lbi/s;->N()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public F()Z
    .locals 1

    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->R()Z

    move-result v0

    return v0
.end method

.method public G()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-static {v0}, Lbi/h;->a(Lax/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbi/s;->a:Lax/t;

    iget-object v1, p0, Lbi/s;->b:Lax/w;

    invoke-virtual {v0, v1}, Lax/t;->a(Lax/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()I
    .locals 2

    const/4 v0, -0x1

    iget-object v1, p0, Lbi/s;->d:Lax/v;

    iget v1, v1, Lax/v;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget v0, v0, Lax/v;->b:I

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/rideabout/r;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public J()I
    .locals 2

    const/4 v0, -0x1

    iget-object v1, p0, Lbi/s;->d:Lax/v;

    iget v1, v1, Lax/v;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget v0, v0, Lax/v;->c:I

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/rideabout/r;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public K()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-object v0, v0, Lax/v;->f:Ljava/lang/String;

    return-object v0
.end method

.method public L()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-object v0, v0, Lax/v;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected M()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected N()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    return-object v0
.end method

.method public O()Z
    .locals 1

    iget-boolean v0, p0, Lbi/s;->e:Z

    return v0
.end method

.method public P()Z
    .locals 1

    iget-boolean v0, p0, Lbi/s;->f:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-object v0, v0, Lax/v;->e:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-wide v0, v0, Lax/v;->h:J

    return-wide v0
.end method

.method public w()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->Q()[Lax/u;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lax/u;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
