.class public final Lbi/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbi/u;

    invoke-direct {v0}, Lbi/u;-><init>()V

    sput-object v0, Lbi/t;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbi/t;->a:I

    iput p2, p0, Lbi/t;->b:I

    return-void
.end method


# virtual methods
.method public a(Lbi/t;)I
    .locals 2

    iget v0, p0, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->a:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lbi/t;->b:I

    iget v1, p1, Lbi/t;->b:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(II)Z
    .locals 1

    iget v0, p0, Lbi/t;->a:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lbi/t;->b:I

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lbi/t;

    invoke-virtual {p0, p1}, Lbi/t;->a(Lbi/t;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lbi/t;

    if-eqz v0, :cond_0

    check-cast p1, Lbi/t;

    iget v0, p1, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->b:I

    invoke-virtual {p0, v0, v1}, Lbi/t;->a(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lbi/t;->a:I

    add-int/lit16 v0, v0, 0x66b

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lbi/t;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "stageIndex"

    iget v2, p0, Lbi/t;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "stageSegmentIndex"

    iget v2, p0, Lbi/t;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lbi/t;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lbi/t;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
