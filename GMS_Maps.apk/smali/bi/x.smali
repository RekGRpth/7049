.class public Lbi/x;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lax/t;I[LaN/B;)Lbi/y;
    .locals 8

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lax/t;->Q()[Lax/u;

    move-result-object v5

    aget-object v1, v5, p1

    invoke-virtual {v1}, Lax/u;->d()LaN/B;

    move-result-object v6

    move v1, v0

    move v2, v3

    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_2

    aget-object v4, p2, v0

    invoke-static {v6, v4}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v4

    cmpg-float v7, v4, v2

    if-ltz v7, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move v1, v0

    move v2, v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    aget-object v2, p2, v1

    if-lez v1, :cond_3

    add-int/lit8 v0, v1, -0x1

    aget-object v0, p2, v0

    :goto_1
    invoke-static {v2, v6, v0}, LX/g;->d(LaN/B;LaN/B;LaN/B;)F

    move-result v4

    invoke-static {v6, v2, v0}, LX/g;->d(LaN/B;LaN/B;LaN/B;)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    add-int/lit8 v0, v1, -0x1

    invoke-static {v0, p1, v3}, Lbi/y;->a(IIF)Lbi/y;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_3
    if-lez p1, :cond_4

    add-int/lit8 v0, p1, -0x1

    aget-object v0, v5, v0

    invoke-virtual {v0}, Lax/u;->d()LaN/B;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lax/t;->q()LaN/B;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-static {v1, p1, v3}, Lbi/y;->a(IIF)Lbi/y;

    move-result-object v0

    goto :goto_2
.end method

.method private static a(Lax/t;[LaN/B;)Ljava/util/HashMap;
    .locals 10

    const/4 v2, 0x0

    invoke-virtual {p0}, Lax/t;->Q()[Lax/u;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v5

    invoke-static {v4, p1}, Lbi/x;->a([Lax/u;[LaN/B;)[I

    move-result-object v6

    move v1, v2

    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_7

    aget-object v0, v4, v1

    invoke-virtual {v0}, Lax/u;->d()LaN/B;

    move-result-object v7

    move v3, v2

    :goto_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_3

    aget-object v0, p1, v3

    add-int/lit8 v8, v3, 0x1

    aget-object v8, p1, v8

    invoke-static {v7, v0, v8}, LX/g;->a(LaN/B;LaN/B;LaN/B;)F

    move-result v0

    invoke-static {v3, v1, v0}, Lbi/y;->a(IIF)Lbi/y;

    move-result-object v8

    const/4 v9, 0x0

    cmpg-float v0, v0, v9

    if-gez v0, :cond_0

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v0

    :cond_1
    invoke-virtual {v0, v8}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v8

    const/4 v9, 0x2

    if-le v8, v9, :cond_2

    invoke-virtual {v0}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    aget v3, v6, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    if-eqz v0, :cond_4

    invoke-static {v0}, Lbi/x;->a(Ljava/util/TreeSet;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v0

    invoke-static {p0, v1, p1}, Lbi/x;->a(Lax/t;I[LaN/B;)Lbi/y;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_7
    return-object v5
.end method

.method static a(Ljava/util/HashMap;I)Ljava/util/HashMap;
    .locals 8

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/y;

    iget v2, v0, Lbi/y;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    move-object v0, v4

    :goto_2
    return-object v0

    :cond_2
    invoke-static {p0, p1}, Lbi/x;->b(Ljava/util/HashMap;I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/y;

    iget v2, v0, Lbi/y;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    move-object v0, v4

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    move-object v0, v4

    goto :goto_2

    :cond_5
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [Lbi/y;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move v2, v3

    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_6

    aget-object v1, v5, v2

    if-eqz v1, :cond_7

    aget-object v7, v5, v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbi/y;

    invoke-virtual {v7, v1}, Lbi/y;->a(Lbi/y;)I

    move-result v1

    if-lez v1, :cond_8

    :cond_7
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbi/y;

    aput-object v1, v5, v2

    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    :cond_9
    :goto_5
    array-length v0, v5

    if-ge v3, v0, :cond_a

    aget-object v0, v5, v3

    iget v0, v0, Lbi/y;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aget-object v1, v5, v3

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_a
    move-object v0, v4

    goto/16 :goto_2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method private static a(Ljava/util/TreeSet;)Ljava/util/HashSet;
    .locals 4

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/y;

    iget v3, v0, Lbi/y;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v0, v0, Lbi/y;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static a(Ljava/util/HashMap;IILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4

    if-lt p1, p2, :cond_1

    invoke-virtual {p4, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    add-int/lit8 v0, p1, 0x1

    invoke-static {p0, v0, p2, p3, p4}, Lbi/x;->a(Ljava/util/HashMap;IILjava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/y;

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbi/y;

    iget v1, v1, Lbi/y;->a:I

    iget v3, v0, Lbi/y;->a:I

    if-gt v1, v3, :cond_4

    :cond_5
    invoke-static {p3}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, p1, 0x1

    invoke-static {p0, v0, p2, v1, p4}, Lbi/x;->a(Ljava/util/HashMap;IILjava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method private static a(Ljava/util/HashMap;Ljava/util/HashSet;[LaN/B;ILcom/google/common/collect/P;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v2, -0x1

    add-int/lit8 v4, p3, 0x1

    move v1, v3

    :goto_0
    add-int/lit8 v0, v4, -0x1

    if-ge v3, v0, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/y;

    if-nez v0, :cond_0

    move v0, v2

    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v3

    goto :goto_1

    :cond_1
    :goto_2
    iget v2, v0, Lbi/y;->a:I

    if-gt v1, v2, :cond_2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aget-object v5, p2, v1

    invoke-virtual {p4, v2, v5}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move v0, v3

    goto :goto_1

    :cond_3
    :goto_3
    array-length v0, p2

    if-ge v1, v0, :cond_4

    add-int/lit8 v0, v2, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aget-object v3, p2, v1

    invoke-virtual {p4, v0, v3}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    return-void
.end method

.method public static a([LaN/B;Lax/t;Lcom/google/common/collect/P;)V
    .locals 5

    invoke-virtual {p2}, Lcom/google/common/collect/P;->e()V

    invoke-static {p1, p0}, Lbi/x;->a(Lax/t;[LaN/B;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/y;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lax/t;->Q()[Lax/u;

    move-result-object v0

    array-length v0, v0

    invoke-static {v1, v0}, Lbi/x;->a(Ljava/util/HashMap;I)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v1, v2, p0, v0, p2}, Lbi/x;->a(Ljava/util/HashMap;Ljava/util/HashSet;[LaN/B;ILcom/google/common/collect/P;)V

    return-void
.end method

.method private static a([Lax/u;[LaN/B;)[I
    .locals 8

    const/4 v1, 0x0

    array-length v0, p0

    new-array v5, v0, [I

    move v0, v1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_3

    const/4 v2, 0x0

    aget-object v3, p0, v0

    invoke-virtual {v3}, Lax/u;->d()LaN/B;

    move-result-object v6

    move v3, v2

    move v2, v1

    :goto_1
    array-length v4, p1

    if-ge v2, v4, :cond_2

    aget-object v4, p1, v2

    invoke-static {v6, v4}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v4

    cmpg-float v7, v4, v3

    if-ltz v7, :cond_0

    if-nez v2, :cond_1

    :cond_0
    aput v2, v5, v0

    move v3, v4

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-object v5
.end method

.method static b(Ljava/util/HashMap;I)Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v2, p1, v1, v0}, Lbi/x;->a(Ljava/util/HashMap;IILjava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0
.end method
