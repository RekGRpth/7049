.class public final Lbi/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:F


# direct methods
.method public constructor <init>(IIF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbi/y;->a:I

    iput p2, p0, Lbi/y;->b:I

    iput p3, p0, Lbi/y;->c:F

    return-void
.end method

.method public static a(IIF)Lbi/y;
    .locals 1

    new-instance v0, Lbi/y;

    invoke-direct {v0, p0, p1, p2}, Lbi/y;-><init>(IIF)V

    return-object v0
.end method


# virtual methods
.method public a(Lbi/y;)I
    .locals 3

    iget v0, p0, Lbi/y;->c:F

    iget v1, p1, Lbi/y;->c:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3c23d70a

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lbi/y;

    invoke-virtual {p0, p1}, Lbi/y;->a(Lbi/y;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lbi/y;

    if-eqz v2, :cond_2

    iget v2, p0, Lbi/y;->a:I

    check-cast p1, Lbi/y;

    iget v3, p1, Lbi/y;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lbi/y;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "beginIndex"

    iget v2, p0, Lbi/y;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "stopIndex"

    iget v2, p0, Lbi/y;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "distance"

    iget v2, p0, Lbi/y;->c:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
