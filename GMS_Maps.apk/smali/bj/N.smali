.class public Lbj/N;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/ui/br;

.field private final c:Lcom/google/googlenav/ai;

.field private final d:Lcom/google/googlenav/ui/view/c;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbj/N;->a:I

    iput-object p2, p0, Lbj/N;->b:Lcom/google/googlenav/ui/br;

    iput-object p3, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    iput-object p4, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    invoke-virtual {p0}, Lbj/N;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2d

    invoke-virtual {p3, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    return-void
.end method

.method private a(Lbj/O;)V
    .locals 6

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/google/googlenav/ap;->b()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->I()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v4, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    move v3, v1

    :goto_1
    if-eqz v3, :cond_5

    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->at()Lcom/google/googlenav/ay;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    iget-object v1, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lbm/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p1, Lbj/O;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1002a1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x3ca

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/16 v1, 0x909

    const/4 v3, -0x1

    const-string v4, "lp"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    iget-object v1, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    iget-object v3, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    invoke-static {v1, v3, v0}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    iget-object v0, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_2

    :cond_5
    if-eqz v4, :cond_1

    invoke-static {v4}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v2

    iget-object v3, p0, Lbj/N;->b:Lcom/google/googlenav/ui/br;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v3

    invoke-virtual {v3, v2}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-static {v3, v0, v1}, Lbf/aS;->b(Lcom/google/googlenav/ai;ZZ)Lcom/google/googlenav/ui/view/a;

    move-result-object v1

    iget-object v3, p1, Lbj/O;->b:Landroid/widget/ImageView;

    iget-object v4, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    invoke-static {v3, v4, v1}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    iget-object v1, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-static {v1, v0}, Lbf/aS;->b(Lcom/google/googlenav/ai;Z)V

    iget-object v0, p0, Lbj/N;->b:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    iget-object v1, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/N;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/O;

    invoke-direct {v1, p0}, Lbj/O;-><init>(Lbj/N;)V

    const v0, 0x7f1002a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/O;->a:Landroid/view/ViewGroup;

    const v0, 0x7f1002a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/O;->b:Landroid/widget/ImageView;

    const v0, 0x7f10019c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    iput-object v0, v1, Lbj/O;->c:Lcom/google/googlenav/ui/view/android/DistanceView;

    const v0, 0x7f10019d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    iput-object v0, v1, Lbj/O;->d:Lcom/google/googlenav/ui/view/android/HeadingView;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->e:Landroid/widget/TextView;

    const v0, 0x7f1001a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->f:Landroid/widget/TextView;

    const v0, 0x7f1001a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->g:Landroid/widget/TextView;

    const v0, 0x7f1002aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->h:Landroid/widget/TextView;

    const v0, 0x7f100230

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lbj/O;->i:Landroid/widget/Button;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 6

    const/4 v1, 0x0

    check-cast p2, Lbj/O;

    invoke-direct {p0, p2}, Lbj/N;->a(Lbj/O;)V

    iget-object v0, p2, Lbj/O;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/O;->c:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v2, p2, Lbj/O;->d:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v3, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-static {v0, v2, v3}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    iget-object v0, p2, Lbj/O;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/O;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bz()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_0
    iget-object v2, p2, Lbj/O;->h:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lbj/N;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x422

    :goto_1
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iget-object v2, p2, Lbj/O;->i:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/O;->i:Landroid/widget/Button;

    iget-object v2, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    new-instance v3, Lcom/google/googlenav/ui/view/a;

    const/16 v4, 0x6a4

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    :goto_2
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    const/16 v0, 0x3f8

    goto :goto_1

    :cond_2
    iget-object v0, p2, Lbj/O;->i:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400e3

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 4

    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
