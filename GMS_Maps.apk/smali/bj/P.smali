.class public Lbj/P;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:I

.field final b:Lcom/google/googlenav/ai;

.field final c:Lbf/aQ;

.field final d:Lbf/i;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ai;Lbf/aQ;Lbf/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbj/P;->a:I

    iput-object p2, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    iput-object p3, p0, Lbj/P;->c:Lbf/aQ;

    iput-object p4, p0, Lbj/P;->d:Lbf/i;

    invoke-virtual {p0}, Lbj/P;->e()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbj/P;->d()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x20

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_1
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v0, 0x26

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_2
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ci()Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_3

    const/16 v0, 0x30

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_3
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/P;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    new-instance v1, Lbj/S;

    invoke-direct {v1}, Lbj/S;-><init>()V

    iput-object p1, v1, Lbj/S;->a:Landroid/view/View;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->b:Landroid/widget/TextView;

    const v0, 0x7f100234

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/S;->c:Landroid/view/View;

    const v0, 0x7f100040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/S;->d:Landroid/view/View;

    const v0, 0x7f1002ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->e:Landroid/widget/TextView;

    const v0, 0x7f1002ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->f:Landroid/widget/TextView;

    const v0, 0x7f1002ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->g:Landroid/widget/TextView;

    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->h:Landroid/widget/TextView;

    const v0, 0x7f1002b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->i:Landroid/widget/TextView;

    const v0, 0x7f1002b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->j:Landroid/widget/TextView;

    const v0, 0x7f1002af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->k:Landroid/widget/TextView;

    const v0, 0x7f1002b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->l:Landroid/widget/TextView;

    const v0, 0x7f1002b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->m:Landroid/widget/TextView;

    const v0, 0x7f1002b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->n:Landroid/widget/TextView;

    const v0, 0x7f1002b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->o:Landroid/widget/TextView;

    const v0, 0x7f1002b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/S;->p:Landroid/widget/ImageView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 11

    const/16 v10, 0x8

    const/4 v6, 0x0

    const/4 v1, 0x0

    check-cast p2, Lbj/S;

    iget-object v0, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    :cond_0
    iget-object v2, p2, Lbj/S;->f:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v0, :cond_4

    move-object v2, v1

    :goto_0
    iget-object v3, p2, Lbj/S;->e:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-static {v2, v6}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v7

    iget-object v2, p2, Lbj/S;->h:Landroid/widget/TextView;

    invoke-static {v2, v7}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v7, :cond_5

    move-object v2, v1

    :goto_1
    iget-object v3, p2, Lbj/S;->g:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ci()Lcom/google/googlenav/ar;

    move-result-object v8

    if-eqz v8, :cond_f

    invoke-virtual {v8}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aX:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    const/16 v2, 0x1df

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    :goto_2
    iget-object v4, p2, Lbj/S;->j:Landroid/widget/TextView;

    invoke-static {v4, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p2, Lbj/S;->i:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-eqz v8, :cond_1

    iget-object v2, p2, Lbj/S;->j:Landroid/widget/TextView;

    new-instance v3, Lbj/R;

    invoke-virtual {v8}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, p1, v4}, Lbj/R;-><init>(Lbj/P;Lcom/google/googlenav/ui/e;Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual {p0}, Lbj/P;->e()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0}, Lbj/P;->d()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v3, :cond_6

    if-eqz v2, :cond_6

    const/16 v3, 0x122

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    :cond_2
    :goto_3
    iget-object v4, p0, Lbj/P;->c:Lbf/aQ;

    invoke-virtual {v4}, Lbf/aQ;->b()Z

    move-result v4

    if-eqz v4, :cond_7

    :goto_4
    iget-object v3, p2, Lbj/S;->l:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v2, :cond_8

    move-object v3, v1

    :goto_5
    iget-object v4, p2, Lbj/S;->k:Landroid/widget/TextView;

    invoke-static {v4, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->by()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0x5f4

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    :goto_6
    iget-object v5, p2, Lbj/S;->n:Landroid/widget/TextView;

    invoke-static {v5, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v4, :cond_a

    move-object v5, v1

    :goto_7
    iget-object v9, p2, Lbj/S;->m:Landroid/widget/TextView;

    invoke-static {v9, v5}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v0, :cond_3

    if-nez v7, :cond_3

    if-nez v2, :cond_3

    if-nez v4, :cond_3

    if-eqz v8, :cond_b

    :cond_3
    iget-object v0, p2, Lbj/S;->b:Landroid/widget/TextView;

    const/16 v2, 0xeb

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/S;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_8
    if-eqz v3, :cond_e

    iget-object v0, p2, Lbj/S;->d:Landroid/view/View;

    new-instance v1, Lbj/Q;

    invoke-direct {v1, p0, p1}, Lbj/Q;-><init>(Lbj/P;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbj/P;->c:Lbf/aQ;

    invoke-virtual {v0}, Lbf/aQ;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x3c2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_9
    iget-object v1, p0, Lbj/P;->c:Lbf/aQ;

    invoke-virtual {v1}, Lbf/aQ;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    const v1, 0x7f020223

    :goto_a
    iget-object v2, p2, Lbj/S;->o:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/S;->p:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/ImageView;I)V

    return-void

    :cond_4
    const/16 v2, 0x37c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_0

    :cond_5
    const/16 v2, 0x40

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_1

    :cond_6
    if-nez v2, :cond_2

    if-eqz v3, :cond_2

    const/16 v2, 0x30d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_7
    move-object v2, v3

    goto/16 :goto_4

    :cond_8
    const/16 v3, 0x1e2

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    goto/16 :goto_5

    :cond_9
    move-object v4, v1

    goto/16 :goto_6

    :cond_a
    const/16 v5, 0x30a

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    sget-object v9, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v5, v9}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    goto/16 :goto_7

    :cond_b
    iget-object v0, p2, Lbj/S;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p2, Lbj/S;->c:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    :cond_c
    const/16 v0, 0x3c3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    :cond_d
    const v1, 0x7f020224

    goto :goto_a

    :cond_e
    iget-object v0, p2, Lbj/S;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p2, Lbj/S;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundResource(I)V

    move-object v0, v1

    move v1, v6

    goto :goto_a

    :cond_f
    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_2
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400e4

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected d()Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbf/aS;->a(Lcom/google/googlenav/ai;ZZ)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v0, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    iget-object v3, v0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/aW;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected e()Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bc()Lcom/google/googlenav/ac;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lbf/aS;->a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
