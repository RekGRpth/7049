.class public Lbj/Y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/bZ;

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bZ;III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbj/Y;->a:Lcom/google/googlenav/bZ;

    iput p2, p0, Lbj/Y;->b:I

    iput p3, p0, Lbj/Y;->c:I

    iput p4, p0, Lbj/Y;->d:I

    return-void
.end method

.method static synthetic a(Lbj/Y;)I
    .locals 1

    iget v0, p0, Lbj/Y;->b:I

    return v0
.end method

.method private static a(Landroid/widget/ImageView;)V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private static a(Landroid/widget/ImageView;J)V
    .locals 1

    invoke-static {p1, p2}, Lcom/google/googlenav/ui/view/dialog/ba;->a(J)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private static a(Lbj/aa;)V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lbj/aa;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbj/aa;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbj/aa;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbj/aa;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbj/aa;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lbj/Y;)I
    .locals 1

    iget v0, p0, Lbj/Y;->d:I

    return v0
.end method

.method static synthetic c(Lbj/Y;)Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lbj/Y;->a:Lcom/google/googlenav/bZ;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/Y;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/aa;

    invoke-direct {v1}, Lbj/aa;-><init>()V

    invoke-static {v1, p1}, Lbj/aa;->a(Lbj/aa;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f100051

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/aa;->b(Lbj/aa;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f1002e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aa;->a:Landroid/widget/ImageView;

    const v0, 0x7f1002e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aa;->b:Landroid/widget/ImageView;

    const v0, 0x7f1002e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aa;->c:Landroid/widget/ImageView;

    const v0, 0x7f1002e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aa;->d:Landroid/widget/ImageView;

    const v0, 0x7f1002e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aa;->e:Landroid/widget/ImageView;

    const v0, 0x7f100092

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/aa;->a(Lbj/aa;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f10019c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    invoke-static {v1, v0}, Lbj/aa;->a(Lbj/aa;Lcom/google/googlenav/ui/view/android/DistanceView;)Lcom/google/googlenav/ui/view/android/DistanceView;

    const v0, 0x7f10019d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    invoke-static {v1, v0}, Lbj/aa;->a(Lbj/aa;Lcom/google/googlenav/ui/view/android/HeadingView;)Lcom/google/googlenav/ui/view/android/HeadingView;

    const v0, 0x7f1002df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/aa;->b(Lbj/aa;Landroid/widget/TextView;)Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    return-object v1
.end method

.method public a(Lbj/aa;Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V
    .locals 6

    const/16 v2, 0x8

    const/4 v0, 0x1

    const/4 v5, 0x0

    if-eqz p2, :cond_2

    invoke-static {p1}, Lbj/aa;->a(Lbj/aa;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-static {p1}, Lbj/aa;->a(Lbj/aa;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lbj/Z;

    invoke-direct {v2, p0, p3}, Lbj/Z;-><init>(Lbj/Y;Lcom/google/googlenav/ui/e;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p2, p1}, Lbj/Y;->a(Lcom/google/googlenav/bZ;Lbj/aa;)V

    invoke-static {p1}, Lbj/aa;->b(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lbj/aa;->b(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p1}, Lbj/aa;->c(Lbj/aa;)Lcom/google/googlenav/ui/view/android/DistanceView;

    move-result-object v1

    invoke-static {p1}, Lbj/aa;->d(Lbj/aa;)Lcom/google/googlenav/ui/view/android/HeadingView;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/googlenav/bZ;->j()LaN/B;

    move-result-object v3

    invoke-static {v1, v2, v3}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/google/googlenav/bZ;->l()I

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "i "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-static {p1}, Lbj/aa;->b(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f020010

    invoke-direct {v2, v3, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;I)V

    const/16 v3, 0x21

    invoke-virtual {v1, v2, v5, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/googlenav/bZ;->k()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {p2, v5}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_0
    invoke-virtual {p2}, Lcom/google/googlenav/bZ;->k()I

    move-result v2

    if-ge v0, v2, :cond_1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {p2, v0}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lbj/aa;->e(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lbj/aa;->e(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_2
    invoke-static {p1}, Lbj/aa;->b(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x4bd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lbj/aa;->b(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p1}, Lbj/aa;->c(Lbj/aa;)Lcom/google/googlenav/ui/view/android/DistanceView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setVisibility(I)V

    invoke-static {p1}, Lbj/aa;->d(Lbj/aa;)Lcom/google/googlenav/ui/view/android/HeadingView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setVisibility(I)V

    invoke-static {p1}, Lbj/aa;->e(Lbj/aa;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p1}, Lbj/aa;->a(Lbj/aa;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    invoke-static {p1}, Lbj/Y;->a(Lbj/aa;)V

    goto :goto_1
.end method

.method a(Lcom/google/googlenav/bZ;Lbj/aa;)V
    .locals 7

    const/4 v6, 0x2

    const/16 v2, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Long;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    array-length v1, v0

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {p2}, Lbj/Y;->a(Lbj/aa;)V

    iget-object v1, p2, Lbj/aa;->a:Landroid/widget/ImageView;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/view/dialog/ba;->a(J)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p2, Lbj/aa;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p2, Lbj/aa;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p2, Lbj/aa;->b:Landroid/widget/ImageView;

    aget-object v2, v0, v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v1, p2, Lbj/aa;->e:Landroid/widget/ImageView;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v0, p2, Lbj/aa;->c:Landroid/widget/ImageView;

    invoke-static {v0}, Lbj/Y;->a(Landroid/widget/ImageView;)V

    iget-object v0, p2, Lbj/aa;->d:Landroid/widget/ImageView;

    invoke-static {v0}, Lbj/Y;->a(Landroid/widget/ImageView;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p2, Lbj/aa;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p2, Lbj/aa;->b:Landroid/widget/ImageView;

    aget-object v2, v0, v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v1, p2, Lbj/aa;->c:Landroid/widget/ImageView;

    aget-object v2, v0, v5

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v1, p2, Lbj/aa;->d:Landroid/widget/ImageView;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v0, p2, Lbj/aa;->e:Landroid/widget/ImageView;

    invoke-static {v0}, Lbj/Y;->a(Landroid/widget/ImageView;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p2, Lbj/aa;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p2, Lbj/aa;->b:Landroid/widget/ImageView;

    aget-object v2, v0, v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v1, p2, Lbj/aa;->c:Landroid/widget/ImageView;

    aget-object v2, v0, v5

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v1, p2, Lbj/aa;->d:Landroid/widget/ImageView;

    aget-object v2, v0, v6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    iget-object v1, p2, Lbj/aa;->e:Landroid/widget/ImageView;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lbj/Y;->a(Landroid/widget/ImageView;J)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 1

    check-cast p2, Lbj/aa;

    iget-object v0, p0, Lbj/Y;->a:Lcom/google/googlenav/bZ;

    invoke-virtual {p0, p2, v0, p1}, Lbj/Y;->a(Lbj/aa;Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400ff

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
