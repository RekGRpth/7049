.class final enum Lbj/aB;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbj/aB;

.field public static final enum b:Lbj/aB;

.field public static final enum c:Lbj/aB;

.field private static final synthetic d:[Lbj/aB;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lbj/aB;

    const-string v1, "EndofQuestion"

    invoke-direct {v0, v1, v2}, Lbj/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbj/aB;->a:Lbj/aB;

    new-instance v0, Lbj/aB;

    const-string v1, "FollowupQuestion"

    invoke-direct {v0, v1, v3}, Lbj/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbj/aB;->b:Lbj/aB;

    new-instance v0, Lbj/aB;

    const-string v1, "AltPhonesDialog"

    invoke-direct {v0, v1, v4}, Lbj/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbj/aB;->c:Lbj/aB;

    const/4 v0, 0x3

    new-array v0, v0, [Lbj/aB;

    sget-object v1, Lbj/aB;->a:Lbj/aB;

    aput-object v1, v0, v2

    sget-object v1, Lbj/aB;->b:Lbj/aB;

    aput-object v1, v0, v3

    sget-object v1, Lbj/aB;->c:Lbj/aB;

    aput-object v1, v0, v4

    sput-object v0, Lbj/aB;->d:[Lbj/aB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbj/aB;
    .locals 1

    const-class v0, Lbj/aB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbj/aB;

    return-object v0
.end method

.method public static values()[Lbj/aB;
    .locals 1

    sget-object v0, Lbj/aB;->d:[Lbj/aB;

    invoke-virtual {v0}, [Lbj/aB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbj/aB;

    return-object v0
.end method
