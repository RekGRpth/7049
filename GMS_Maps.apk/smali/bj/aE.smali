.class public Lbj/aE;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# static fields
.field private static final b:Lcom/google/googlenav/ui/aV;


# instance fields
.field protected final a:Lbj/aP;

.field private final c:I

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lcom/google/googlenav/ui/aa;

.field private final g:Lbf/aQ;

.field private final h:Z

.field private final i:Z

.field private final j:Lcom/google/googlenav/ai;

.field private final k:I

.field private l:Lcom/google/googlenav/ui/e;

.field private m:Lbj/aM;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    sput-object v0, Lbj/aE;->b:Lcom/google/googlenav/ui/aV;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZ)V
    .locals 10

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZZ)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZLcom/google/googlenav/ai;ILcom/google/googlenav/ui/e;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p6, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Lbj/aP;

    invoke-direct {v1, v0, p1, p2}, Lbj/aP;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iput-object p3, p0, Lbj/aE;->d:Ljava/util/List;

    iput-object p4, p0, Lbj/aE;->e:Ljava/util/List;

    iput-object p5, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    iput-object p6, p0, Lbj/aE;->g:Lbf/aQ;

    iput p7, p0, Lbj/aE;->c:I

    iput-boolean p8, p0, Lbj/aE;->h:Z

    iput-object p9, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    iput p10, p0, Lbj/aE;->k:I

    iput-object p11, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    iput-boolean p12, p0, Lbj/aE;->i:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZZ)V
    .locals 13

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZLcom/google/googlenav/ai;ILcom/google/googlenav/ui/e;Z)V

    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;
    .locals 6

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/16 v0, 0x28

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x2ba

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic a(Lbj/aE;)V
    .locals 0

    invoke-direct {p0}, Lbj/aE;->e()V

    return-void
.end method

.method static synthetic a(Lbj/aE;Lbj/aM;)V
    .locals 0

    invoke-direct {p0, p1}, Lbj/aE;->c(Lbj/aM;)V

    return-void
.end method

.method static synthetic a(Lbj/aE;Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    return-void
.end method

.method private a(Lbj/aL;)V
    .locals 4

    new-instance v1, Lbj/aN;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/aN;-><init>(Lbj/aF;)V

    const v0, 0x7f040134

    iget-object v2, p1, Lbj/aL;->j:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aN;->a:Landroid/view/ViewGroup;

    iget-object v0, p1, Lbj/aL;->j:Landroid/view/ViewGroup;

    iget-object v2, v1, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, v1, Lbj/aN;->a:Landroid/view/ViewGroup;

    const v2, 0x7f1002a9

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aN;->b:Landroid/widget/ImageView;

    iget-object v0, p1, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lbj/aM;I)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lbj/aE;->b(Lbj/aM;I)Lbj/aL;

    move-result-object v0

    iget-object v1, p1, Lbj/aM;->d:Landroid/view/ViewGroup;

    iget-object v2, v0, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p1, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lbj/aM;->i:Ljava/util/List;

    invoke-direct {p0, p2}, Lbj/aE;->a(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lcom/google/googlenav/ai;Landroid/widget/ImageView;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v0

    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    check-cast v0, Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v2

    invoke-virtual {v2, v1}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Lbj/aK;

    invoke-direct {v0, p0, v2, v1, p2}, Lbj/aK;-><init>(Lbj/aE;LaB/s;Lcom/google/googlenav/ui/bs;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v1, v0}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-static {p2, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V
    .locals 1

    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/e;Landroid/view/ViewGroup;[Lcom/google/googlenav/aw;I)V
    .locals 1

    new-instance v0, Lbj/aJ;

    invoke-direct {v0, p0, p1, p4, p3}, Lbj/aJ;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;I[Lcom/google/googlenav/aw;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;Ljava/util/List;[Lcom/google/googlenav/aw;Z)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p2, Lbj/aL;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-static {p3}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez p6, :cond_5

    move v0, v2

    :goto_0
    iget v1, p0, Lbj/aE;->k:I

    if-lez v1, :cond_6

    iget-object v1, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_6

    move v1, v2

    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_7

    :cond_0
    move v0, v2

    :goto_2
    iget-object v1, p2, Lbj/aL;->b:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    :cond_1
    iget-boolean v0, p0, Lbj/aE;->h:Z

    if-nez v0, :cond_2

    :cond_2
    iget-boolean v0, p0, Lbj/aE;->h:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/bm;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lbj/aQ;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/bm;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lbj/aQ;->c:Ljava/lang/String;

    iget-object v0, p3, Lbj/aQ;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, ""

    iput-object v0, p3, Lbj/aQ;->b:Ljava/lang/String;

    :cond_3
    iget-object v0, p2, Lbj/aL;->d:Landroid/widget/ImageView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->a(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aL;->c:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->b(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aL;->e:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->c(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aL;->f:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2, p3}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lbj/aL;->k:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    iget-object v1, p2, Lbj/aL;->l:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Lcom/google/googlenav/ai;Landroid/widget/ImageView;)V

    iget-object v0, p2, Lbj/aL;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/aL;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/aL;->k:Landroid/view/ViewGroup;

    new-instance v1, Lbj/aG;

    invoke-direct {v1, p0, p1}, Lbj/aG;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    invoke-static {p3}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p2, Lbj/aL;->g:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->d(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    :goto_3
    if-eqz p4, :cond_c

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_9

    iget-object v0, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_4
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_9

    invoke-direct {p0, p2}, Lbj/aE;->a(Lbj/aL;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    move v0, v3

    goto/16 :goto_0

    :cond_6
    move v1, v3

    goto/16 :goto_1

    :cond_7
    move v0, v3

    goto/16 :goto_2

    :cond_8
    iget-object v0, p2, Lbj/aL;->h:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->e(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aL;->i:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->f(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    goto :goto_3

    :cond_9
    move v4, v3

    :goto_5
    iget-object v0, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_b

    iget-object v0, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aN;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lt v4, v1, :cond_a

    iget-object v0, v0, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lbj/aE;->a(Landroid/view/View;Z)V

    :goto_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_a
    iget-object v1, v0, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v2}, Lbj/aE;->a(Landroid/view/View;Z)V

    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/bs;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Lbj/aN;Lcom/google/googlenav/ui/bs;)Z

    move-result v1

    iget-object v5, v0, Lbj/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v5, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v0, v0, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, p1, v0, p5, v4}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Landroid/view/ViewGroup;[Lcom/google/googlenav/aw;I)V

    goto :goto_6

    :cond_b
    iget-object v0, p2, Lbj/aL;->j:Landroid/view/ViewGroup;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_d

    :goto_7
    invoke-direct {p0, v0, v2}, Lbj/aE;->a(Landroid/view/View;Z)V

    :cond_c
    return-void

    :cond_d
    move v2, v3

    goto :goto_7
.end method

.method private a(I)Z
    .locals 1

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v0}, Lbj/aP;->a()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aQ;

    invoke-static {v0}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z
    .locals 1

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_0
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lbj/aL;Lbj/aQ;)Z
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p2, Lbj/aQ;->b:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-static {p2}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/ui/bi;->a(ZI)Lam/f;

    move-result-object v0

    iget-object v1, p1, Lbj/aL;->d:Landroid/widget/ImageView;

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    return v3

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/googlenav/ui/bs;

    iget-object v0, p2, Lbj/aQ;->b:Ljava/lang/String;

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    check-cast v0, Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v2

    invoke-virtual {v2, v1}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    new-instance v2, Lbj/aH;

    invoke-direct {v2, p0, v1, p1}, Lbj/aH;-><init>(Lbj/aE;Lcom/google/googlenav/ui/bs;Lbj/aL;)V

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lbj/aL;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method private a(Lbj/aM;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v1}, Lbj/aP;->a()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lbj/aM;->b:Landroid/widget/TextView;

    sget-object v3, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v1, v3}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->b:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lbj/aN;Lcom/google/googlenav/ui/bs;)Z
    .locals 5

    const/16 v4, 0x30

    const/4 v0, 0x1

    iget-object v1, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p1, Lbj/aN;->b:Landroid/widget/ImageView;

    iget-object v2, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    invoke-interface {v2, p2}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v2

    invoke-interface {v2}, Lam/f;->a()I

    move-result v2

    iget-object v3, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    invoke-interface {v3, p2}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v3

    invoke-interface {v3}, Lam/f;->b()I

    move-result v3

    mul-int/lit8 v2, v2, 0x30

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    div-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMaxWidth(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMaxHeight(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    iget-object v1, p1, Lbj/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method private static a(Lbj/aQ;)Z
    .locals 1

    iget-object v0, p0, Lbj/aQ;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v0, p3, Lbj/aQ;->f:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_7

    move v0, v1

    :goto_0
    iget-object v3, p0, Lbj/aE;->g:Lbf/aQ;

    if-eqz v3, :cond_8

    move v3, v1

    :goto_1
    iget-object v4, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v4, :cond_9

    move v4, v1

    :goto_2
    iget-object v5, p3, Lbj/aQ;->h:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    iget-object v7, p3, Lbj/aQ;->g:Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-le v5, v7, :cond_a

    move v5, v1

    :goto_3
    iget-boolean v7, p0, Lbj/aE;->h:Z

    if-nez v7, :cond_0

    if-nez v4, :cond_b

    if-nez v3, :cond_b

    if-eqz v5, :cond_b

    :cond_0
    iget-object v4, p3, Lbj/aQ;->h:Ljava/lang/CharSequence;

    :goto_4
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_c

    move v5, v1

    :goto_5
    if-eqz v0, :cond_1

    iget-object v7, p3, Lbj/aQ;->f:Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    iget-object v7, p3, Lbj/aQ;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v8, Lbj/aI;

    invoke-direct {v8, p0, p1, p2, v7}, Lbj/aI;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;Lbj/aL;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    if-eqz v5, :cond_2

    invoke-virtual {v6, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    if-nez v3, :cond_3

    iget-object v3, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    iget-object v3, p2, Lbj/aL;->f:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v0, :cond_5

    if-eqz v5, :cond_6

    :cond_5
    move v2, v1

    :cond_6
    return v2

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v3, v2

    goto :goto_1

    :cond_9
    move v4, v2

    goto :goto_2

    :cond_a
    move v5, v2

    goto :goto_3

    :cond_b
    iget-object v4, p3, Lbj/aQ;->g:Ljava/lang/CharSequence;

    goto :goto_4

    :cond_c
    move v5, v2

    goto :goto_5
.end method

.method private a(Lcom/google/googlenav/ui/e;Lbj/aM;)Z
    .locals 8

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v0, p2, Lbj/aM;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    iget-object v0, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-direct {p0, p2}, Lbj/aE;->b(Lbj/aM;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_2
    iget-object v0, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    iget-object v0, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aO;

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v3, v1, :cond_2

    iget-object v0, v0, Lbj/aO;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v4}, Lbj/aE;->a(Landroid/view/View;Z)V

    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v2, v0, Lbj/aO;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v5}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v2, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v2, v2, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v3, v2, :cond_3

    iget-object v2, v0, Lbj/aO;->b:Landroid/view/View;

    invoke-direct {p0, v2, v5}, Lbj/aE;->a(Landroid/view/View;Z)V

    :cond_3
    iget-object v6, v0, Lbj/aO;->c:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    sget-object v7, Lbj/aE;->b:Lcom/google/googlenav/ui/aV;

    invoke-static {v6, v2, v7}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v0, v0, Lbj/aO;->a:Landroid/view/ViewGroup;

    new-instance v2, Lbj/aF;

    invoke-direct {v2, p0, p1, v1}, Lbj/aF;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;Landroid/util/Pair;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    :cond_4
    move v4, v5

    goto/16 :goto_0
.end method

.method static synthetic b(Lbj/aE;)I
    .locals 1

    iget v0, p0, Lbj/aE;->k:I

    return v0
.end method

.method private b(Lbj/aM;I)Lbj/aL;
    .locals 4

    const/4 v1, 0x0

    new-instance v2, Lbj/aL;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lbj/aL;-><init>(Lbj/aF;)V

    invoke-direct {p0, p2}, Lbj/aE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f040143

    :goto_0
    iget-object v3, p1, Lbj/aM;->d:Landroid/view/ViewGroup;

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/aL;->b:Landroid/view/View;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100349

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->c:Landroid/widget/TextView;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100348

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lbj/aL;->d:Landroid/widget/ImageView;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aL;->k:Landroid/view/ViewGroup;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lbj/aL;->l:Landroid/widget/ImageView;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->m:Landroid/widget/TextView;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->n:Landroid/widget/TextView;

    :cond_0
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->f:Landroid/widget/TextView;

    invoke-direct {p0, p2}, Lbj/aE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100380

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->g:Landroid/widget/TextView;

    :goto_1
    iput p2, v2, Lbj/aL;->o:I

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aL;->j:Landroid/view/ViewGroup;

    move v0, v1

    :goto_2
    const/16 v1, 0xa

    if-ge v0, v1, :cond_3

    invoke-direct {p0, v2}, Lbj/aE;->a(Lbj/aL;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    const v0, 0x7f040133

    goto/16 :goto_0

    :cond_2
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->h:Landroid/widget/TextView;

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->i:Landroid/widget/TextView;

    goto :goto_1

    :cond_3
    return-object v2
.end method

.method private b(Lbj/aM;)V
    .locals 4

    new-instance v1, Lbj/aO;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/aO;-><init>(Lbj/aF;)V

    const v0, 0x7f04013f

    iget-object v2, p1, Lbj/aM;->g:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    iget-object v0, p1, Lbj/aM;->g:Landroid/view/ViewGroup;

    iget-object v2, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10036b

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aO;->b:Landroid/view/View;

    iget-object v0, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10036a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/aO;->c:Landroid/widget/TextView;

    iget-object v0, p1, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private b(Lbj/aL;Lbj/aQ;)Z
    .locals 5

    iget-object v0, p2, Lbj/aQ;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v0, p2, Lbj/aQ;->k:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/aV;->bg:Lcom/google/googlenav/ui/aV;

    :goto_1
    iget-object v2, p1, Lbj/aL;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lbj/aQ;->c:Ljava/lang/String;

    invoke-static {p2}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    invoke-static {v2, v3, v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/googlenav/ui/aV;->bf:Lcom/google/googlenav/ui/aV;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-static {p0}, Lbj/aE;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lbj/aE;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private c(Lbj/aM;)V
    .locals 0

    iput-object p1, p0, Lbj/aE;->m:Lbj/aM;

    return-void
.end method

.method private c(Lbj/aL;Lbj/aQ;)Z
    .locals 4

    iget-object v0, p2, Lbj/aQ;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p1, Lbj/aL;->e:Landroid/widget/TextView;

    iget-object v3, p2, Lbj/aQ;->d:Ljava/lang/String;

    invoke-static {p2}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    :goto_1
    invoke-static {v2, v3, v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    goto :goto_1
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lbj/aE;)Z
    .locals 1

    iget-boolean v0, p0, Lbj/aE;->h:Z

    return v0
.end method

.method private d(Lbj/aL;Lbj/aQ;)Z
    .locals 3

    iget-object v0, p2, Lbj/aQ;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p1, Lbj/aL;->g:Landroid/widget/TextView;

    iget-object v2, p2, Lbj/aQ;->e:Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbj/aE;->m:Lbj/aM;

    return-void
.end method

.method private e(Lbj/aL;Lbj/aQ;)Z
    .locals 2

    iget-object v0, p1, Lbj/aL;->h:Landroid/widget/TextView;

    iget-object v1, p2, Lbj/aQ;->i:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private f(Lbj/aL;Lbj/aQ;)Z
    .locals 2

    iget-object v0, p1, Lbj/aL;->i:Landroid/widget/TextView;

    iget-object v1, p2, Lbj/aQ;->j:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/aE;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lbj/aE;->i:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lbj/aE;->g:Lbf/aQ;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    new-instance v2, Lbj/aM;

    invoke-direct {v2}, Lbj/aM;-><init>()V

    const v0, 0x7f10025e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10025f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aM;->b:Landroid/widget/TextView;

    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100234

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/aM;->c:Landroid/view/View;

    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100260

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aM;->d:Landroid/view/ViewGroup;

    move v0, v1

    :goto_1
    const/16 v3, 0x8

    if-ge v0, v3, :cond_3

    invoke-direct {p0, v2, v0}, Lbj/aE;->a(Lbj/aM;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100261

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aM;->e:Landroid/widget/TextView;

    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100235

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/aM;->f:Landroid/view/View;

    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100262

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aM;->g:Landroid/view/ViewGroup;

    move v0, v1

    :goto_2
    const/4 v1, 0x5

    if-ge v0, v1, :cond_4

    invoke-direct {p0, v2}, Lbj/aE;->b(Lbj/aM;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    return-object v2
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 11

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    check-cast p2, Lbj/aM;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    :cond_0
    invoke-virtual {p2, p0}, Lbj/aM;->a(Lbj/aE;)V

    invoke-direct {p0, p2}, Lbj/aE;->a(Lbj/aM;)Z

    move-result v0

    iget-object v1, p2, Lbj/aM;->b:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v1, p2, Lbj/aM;->c:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lbj/aM;->c:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    :cond_1
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v0}, Lbj/aP;->a()I

    move-result v0

    iget-object v1, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_2

    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v1}, Lbj/aP;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-direct {p0, p2, v0}, Lbj/aE;->a(Lbj/aM;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v7

    :goto_1
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p2, Lbj/aM;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aQ;

    invoke-static {v0}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eq v0, v2, :cond_3

    invoke-direct {p0, p2, v1}, Lbj/aE;->b(Lbj/aM;I)Lbj/aL;

    move-result-object v2

    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p2, Lbj/aM;->i:Ljava/util/List;

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aQ;

    invoke-static {v0}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p2, Lbj/aM;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    iget-object v0, p2, Lbj/aM;->d:Landroid/view/ViewGroup;

    iget-object v2, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move v8, v7

    :goto_2
    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_b

    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbj/aL;

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v0}, Lbj/aP;->a()I

    move-result v0

    if-ge v8, v0, :cond_a

    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    move-object v4, v9

    :goto_3
    iget-object v0, p0, Lbj/aE;->e:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbj/aE;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    move-object v5, v9

    :goto_4
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbj/aQ;

    iget-object v1, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    if-nez v8, :cond_9

    move v6, v10

    :goto_5
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;Ljava/util/List;[Lcom/google/googlenav/aw;Z)V

    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v10}, Lbj/aE;->a(Landroid/view/View;Z)V

    :goto_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lbj/aE;->e:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/aw;

    move-object v5, v0

    goto :goto_4

    :cond_9
    move v6, v7

    goto :goto_5

    :cond_a
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v7}, Lbj/aE;->a(Landroid/view/View;Z)V

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    invoke-direct {p0, v0, p2}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Lbj/aM;)Z

    move-result v0

    iget-object v1, p2, Lbj/aM;->e:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    iget-object v1, p2, Lbj/aM;->f:Landroid/view/View;

    if-eqz v1, :cond_c

    iget-object v1, p2, Lbj/aM;->f:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    :cond_c
    iget-object v1, p2, Lbj/aM;->g:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400ba

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbj/aE;->m:Lbj/aM;

    if-eqz v0, :cond_2

    move v3, v4

    :goto_0
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move v5, v4

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/ab;

    iget-object v2, p0, Lbj/aE;->m:Lbj/aM;

    iget-object v2, v2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbj/aL;

    iget-object v2, v2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbj/aN;

    iget-object v2, v2, Lbj/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    const/16 v1, 0x18

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    :cond_2
    return-void
.end method
