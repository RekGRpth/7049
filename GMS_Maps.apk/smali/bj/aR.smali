.class public Lbj/aR;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field protected final a:Lcom/google/googlenav/ai;

.field final b:Ljava/lang/CharSequence;

.field final c:Z

.field final d:Ljava/lang/CharSequence;

.field final e:Z

.field final f:I

.field final g:I

.field final h:Lbj/aS;

.field final i:Z

.field private final j:Lcom/google/googlenav/ui/e;

.field private final k:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/i;IZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbj/aR;->a:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lbj/aR;->j:Lcom/google/googlenav/ui/e;

    iput p3, p0, Lbj/aR;->k:I

    iput-boolean p4, p0, Lbj/aR;->i:Z

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Lbf/i;->au()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbj/aR;->c:Z

    const/16 v0, 0x50b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/aR;->b:Ljava/lang/CharSequence;

    iget-boolean v0, p0, Lbj/aR;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    invoke-virtual {p2, v1}, Lbf/i;->a(LaN/g;)Z

    move-result v0

    iput-boolean v0, p0, Lbj/aR;->e:Z

    const/16 v0, 0xf2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/aR;->d:Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Lbf/i;->b(LaN/g;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2, v1}, Lbf/i;->c(LaN/g;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xf

    iput v0, p0, Lbj/aR;->f:I

    const/4 v0, 0x4

    iput v0, p0, Lbj/aR;->g:I

    invoke-static {p1, p2}, Lbf/aS;->a(Lcom/google/googlenav/ai;Lbf/i;)Ljava/util/List;

    :cond_2
    :goto_1
    invoke-virtual {p0, p2}, Lbj/aR;->a(Lbf/i;)Lbj/aS;

    move-result-object v0

    iput-object v0, p0, Lbj/aR;->h:Lbj/aS;

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/16 v0, 0x25b

    iput v0, p0, Lbj/aR;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lbj/aR;->g:I

    iget-boolean v0, p0, Lbj/aR;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_1
.end method

.method static synthetic a(Lbj/aR;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lbj/aR;->j:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;ZC)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->n()Lam/g;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p3}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->o()Lam/g;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lbj/aS;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 10

    iget-object v1, p1, Lbj/aS;->a:Ljava/lang/CharSequence;

    iget-object v2, p1, Lbj/aS;->b:Ljava/lang/String;

    iget-char v3, p1, Lbj/aS;->c:C

    iget v4, p1, Lbj/aS;->e:I

    iget v5, p1, Lbj/aS;->f:I

    iget-boolean v6, p1, Lbj/aS;->d:Z

    move-object v0, p0

    move-object v7, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lbj/aR;->a(Ljava/lang/CharSequence;Ljava/lang/String;CIIZLandroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/lang/String;CIIZLandroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 1

    invoke-virtual {p8, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p7, p2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p6, :cond_1

    new-instance v0, Lbj/aU;

    invoke-direct {v0, p0, p4, p5}, Lbj/aU;-><init>(Lbj/aR;II)V

    invoke-virtual {p7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual {p7, p6}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0, p9, p6, p3}, Lbj/aR;->a(Landroid/widget/ImageView;ZC)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/aR;->k:I

    return v0
.end method

.method protected a(Lbf/i;)Lbj/aS;
    .locals 7

    const/16 v0, 0x68

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lbj/aR;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lbf/i;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lbj/aR;->a:Lcom/google/googlenav/ai;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    :goto_0
    new-instance v0, Lbj/aS;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    sget-char v3, Lcom/google/googlenav/ui/bi;->aV:C

    const/16 v5, 0x10

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v6}, Lbj/aS;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;CZII)V

    return-object v0

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 6

    const v5, 0x7f100414

    const v4, 0x7f100022

    const/4 v3, 0x0

    new-instance v1, Lbj/aT;

    invoke-direct {v1}, Lbj/aT;-><init>()V

    iget-boolean v0, p0, Lbj/aR;->i:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    const v0, 0x7f100024

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    iget-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    const v2, 0x7f1001a4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aT;->b:Landroid/view/View;

    iget-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    const v2, 0x7f1001a6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aT;->c:Landroid/view/View;

    iget-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    const v2, 0x7f1001a8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aT;->d:Landroid/view/View;

    iget-object v0, v1, Lbj/aT;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/aT;->e:Landroid/widget/TextView;

    iget-object v0, v1, Lbj/aT;->c:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/aT;->f:Landroid/widget/TextView;

    iget-object v0, v1, Lbj/aT;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/aT;->g:Landroid/widget/TextView;

    iget-object v0, v1, Lbj/aT;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aT;->h:Landroid/widget/ImageView;

    iget-object v0, v1, Lbj/aT;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aT;->i:Landroid/widget/ImageView;

    iget-object v0, v1, Lbj/aT;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aT;->j:Landroid/widget/ImageView;

    iget-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lbj/aT;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lbj/aT;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lbj/aT;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    const v2, 0x7f100234

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, v1, Lbj/aT;->a:Landroid/view/View;

    const v2, 0x7f100235

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f10002f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100233

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 10

    check-cast p2, Lbj/aT;

    iget-object v1, p0, Lbj/aR;->b:Ljava/lang/CharSequence;

    const/16 v0, 0x50a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-char v3, Lcom/google/googlenav/ui/bi;->aX:C

    const/4 v4, 0x1

    const/4 v5, -0x1

    iget-boolean v6, p0, Lbj/aR;->c:Z

    iget-object v7, p2, Lbj/aT;->b:Landroid/view/View;

    iget-object v8, p2, Lbj/aT;->e:Landroid/widget/TextView;

    iget-object v9, p2, Lbj/aT;->h:Landroid/widget/ImageView;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lbj/aR;->a(Ljava/lang/CharSequence;Ljava/lang/String;CIIZLandroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    iget-object v1, p0, Lbj/aR;->d:Ljava/lang/CharSequence;

    const/16 v0, 0x1ba

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-char v3, Lcom/google/googlenav/ui/bi;->aW:C

    iget v4, p0, Lbj/aR;->f:I

    iget v5, p0, Lbj/aR;->g:I

    iget-boolean v6, p0, Lbj/aR;->e:Z

    iget-object v7, p2, Lbj/aT;->c:Landroid/view/View;

    iget-object v8, p2, Lbj/aT;->f:Landroid/widget/TextView;

    iget-object v9, p2, Lbj/aT;->i:Landroid/widget/ImageView;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lbj/aR;->a(Ljava/lang/CharSequence;Ljava/lang/String;CIIZLandroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    iget-object v0, p0, Lbj/aR;->h:Lbj/aS;

    iget-object v1, p2, Lbj/aT;->d:Landroid/view/View;

    iget-object v2, p2, Lbj/aT;->g:Landroid/widget/TextView;

    iget-object v3, p2, Lbj/aT;->j:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1, v2, v3}, Lbj/aR;->a(Lbj/aS;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400b3

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lbj/aR;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbj/aR;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbj/aR;->h:Lbj/aS;

    iget-boolean v0, v0, Lbj/aS;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
