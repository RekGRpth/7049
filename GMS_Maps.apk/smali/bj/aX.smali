.class public Lbj/aX;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/ai;

.field private c:Lcom/google/googlenav/ui/e;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;IZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lbj/aX;->a:I

    iput-object p1, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    iput-boolean p3, p0, Lbj/aX;->d:Z

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/e;ILjava/lang/Object;)Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lbj/aY;

    invoke-direct {v0, p0, p1, p2, p3}, Lbj/aY;-><init>(Lbj/aX;Lcom/google/googlenav/ui/e;ILjava/lang/Object;)V

    return-object v0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private a(Lbj/ba;Lcom/google/googlenav/as;)Z
    .locals 3

    invoke-virtual {p2}, Lcom/google/googlenav/as;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/as;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lbj/ba;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/googlenav/as;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aX:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    iget-object v0, p1, Lbj/ba;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbj/aX;->c:Lcom/google/googlenav/ui/e;

    const/16 v2, 0x90b

    invoke-direct {p0, v1, v2, p2}, Lbj/aX;->a(Lcom/google/googlenav/ui/e;ILjava/lang/Object;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2}, Lcom/google/googlenav/as;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "--"

    :goto_1
    iget-object v1, p1, Lbj/ba;->e:Landroid/widget/TextView;

    sget-object v2, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/google/googlenav/as;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lbj/bb;)Z
    .locals 8

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->cc()Ljava/util/Vector;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move v3, v0

    :cond_1
    return v3

    :cond_2
    iget-object v1, p1, Lbj/bb;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    move v1, v0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ar;

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    new-instance v2, Landroid/widget/TextView;

    iget-object v5, p1, Lbj/bb;->h:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v5, ", "

    sget-object v6, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v5, v6}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v5, p1, Lbj/bb;->h:Landroid/view/ViewGroup;

    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    invoke-virtual {v0}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v2

    :goto_2
    new-instance v5, Lbj/aZ;

    iget-object v6, p1, Lbj/bb;->h:Landroid/view/ViewGroup;

    invoke-direct {v5, v6}, Lbj/aZ;-><init>(Landroid/view/ViewGroup;)V

    iget-object v6, v5, Lbj/aZ;->b:Landroid/widget/TextView;

    sget-object v7, Lcom/google/googlenav/ui/aV;->bc:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v7}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, v5, Lbj/aZ;->a:Landroid/view/ViewGroup;

    iget-object v5, p0, Lbj/aX;->c:Lcom/google/googlenav/ui/e;

    const/4 v6, 0x5

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v5, v6, v0}, Lbj/aX;->a(Lcom/google/googlenav/ui/e;ILjava/lang/Object;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    move v2, v3

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method private b(Lbj/bb;)Z
    .locals 3

    iget-object v0, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cb()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cb()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lbj/bb;->b:Landroid/widget/TextView;

    sget-object v2, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Lbj/bb;)Z
    .locals 4

    iget-object v0, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ca()Lcom/google/googlenav/ar;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lbj/bb;->g:Lbj/aZ;

    iget-object v0, v0, Lbj/aZ;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ca()Lcom/google/googlenav/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aV:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    iget-object v0, p1, Lbj/bb;->g:Lbj/aZ;

    iget-object v0, v0, Lbj/aZ;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbj/aX;->c:Lcom/google/googlenav/ui/e;

    const/4 v2, 0x5

    iget-object v3, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ca()Lcom/google/googlenav/ar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lbj/aX;->a(Lcom/google/googlenav/ui/e;ILjava/lang/Object;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Lbj/bb;)Z
    .locals 10

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cd()Ljava/util/Vector;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/as;

    iget-object v4, p1, Lbj/bb;->e:[Lbj/ba;

    aget-object v4, v4, v1

    invoke-direct {p0, v4, v0}, Lbj/aX;->a(Lbj/ba;Lcom/google/googlenav/as;)Z

    move-result v0

    iget-object v7, v4, Lbj/ba;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v7, v0}, Lbj/aX;->a(Landroid/view/View;Z)V

    if-eqz v0, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v4, Lbj/ba;->c:Landroid/widget/TextView;

    invoke-direct {p0, v0, v2}, Lbj/aX;->a(Landroid/view/View;Z)V

    iget-object v0, v4, Lbj/ba;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, Lbj/aX;->a(Landroid/view/View;Z)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/as;

    iget-object v8, p1, Lbj/bb;->e:[Lbj/ba;

    aget-object v4, v8, v4

    iget-object v8, v4, Lbj/ba;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/googlenav/as;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v3

    :goto_3
    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lcom/google/googlenav/as;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v9, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v9}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    invoke-virtual {v0}, Lcom/google/googlenav/as;->c()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-direct {p0, v8, v4}, Lbj/aX;->a(Landroid/view/View;Z)V

    move-object v1, v0

    goto :goto_2

    :cond_2
    move v4, v2

    goto :goto_3

    :cond_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    :goto_5
    const/4 v1, 0x5

    if-ge v0, v1, :cond_4

    iget-object v1, p1, Lbj/bb;->e:[Lbj/ba;

    aget-object v1, v1, v0

    iget-object v4, v1, Lbj/ba;->c:Landroid/widget/TextView;

    invoke-direct {p0, v4, v2}, Lbj/aX;->a(Landroid/view/View;Z)V

    iget-object v1, v1, Lbj/ba;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v2}, Lbj/aX;->a(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    move v2, v3

    :cond_5
    return v2

    :cond_6
    move-object v0, v1

    goto :goto_4
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/aX;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 5

    iget-boolean v0, p0, Lbj/aX;->d:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    new-instance v1, Lbj/bb;

    invoke-direct {v1}, Lbj/bb;-><init>()V

    const v0, 0x7f100264

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    iget-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10025f

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/bb;->b:Landroid/widget/TextView;

    iget-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10001f

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/bb;->c:Landroid/view/View;

    iget-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100265

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/bb;->d:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    iget-object v2, v1, Lbj/bb;->e:[Lbj/ba;

    new-instance v3, Lbj/ba;

    iget-object v4, v1, Lbj/bb;->d:Landroid/view/ViewGroup;

    invoke-direct {v3, v4}, Lbj/ba;-><init>(Landroid/view/ViewGroup;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100266

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/bb;->f:Landroid/view/ViewGroup;

    new-instance v0, Lbj/aZ;

    iget-object v2, v1, Lbj/bb;->f:Landroid/view/ViewGroup;

    invoke-direct {v0, v2}, Lbj/aZ;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, v1, Lbj/bb;->g:Lbj/aZ;

    iget-object v0, v1, Lbj/bb;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100267

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/bb;->h:Landroid/view/ViewGroup;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lbj/bb;

    iput-object p1, p0, Lbj/aX;->c:Lcom/google/googlenav/ui/e;

    iget-object v0, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bX()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lbj/bb;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbj/aX;->a(Landroid/view/View;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbj/aX;->b:Lcom/google/googlenav/ai;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    invoke-direct {p0, p2}, Lbj/aX;->b(Lbj/bb;)Z

    move-result v0

    iget-object v1, p2, Lbj/bb;->b:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lbj/aX;->a(Landroid/view/View;Z)V

    iget-object v1, p2, Lbj/bb;->c:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aX;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/bb;->d:Landroid/view/ViewGroup;

    invoke-direct {p0, p2}, Lbj/aX;->d(Lbj/bb;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aX;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/bb;->f:Landroid/view/ViewGroup;

    invoke-direct {p0, p2}, Lbj/aX;->c(Lbj/bb;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aX;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/bb;->h:Landroid/view/ViewGroup;

    invoke-direct {p0, p2}, Lbj/aX;->a(Lbj/bb;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aX;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400bc

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
