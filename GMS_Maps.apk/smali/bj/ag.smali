.class public Lbj/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/cm;

.field private b:Z

.field private final c:I

.field private final d:Z

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/cm;ZII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbj/ag;->b:Z

    iput-object p1, p0, Lbj/ag;->a:Lcom/google/googlenav/cm;

    iput-boolean p2, p0, Lbj/ag;->d:Z

    iput p3, p0, Lbj/ag;->c:I

    iput p4, p0, Lbj/ag;->e:I

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    const v0, 0x7f040156

    const/4 v1, 0x0

    invoke-static {v0, v1, v7}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f1003c6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1003c4

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/16 v4, 0x1fd

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object p2, v5, v7

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0f011e

    invoke-direct {v4, p1, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v3, v4, v7, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lbj/ai;

    invoke-direct {v0, p0, v1}, Lbj/ai;-><init>(Lbj/ag;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    return-object v2
.end method

.method private a(Landroid/content/Context;Lcom/google/googlenav/co;Lcom/google/googlenav/ui/android/ak;)V
    .locals 7

    const v6, 0x7f0f011e

    const/16 v5, 0x21

    invoke-virtual {p3}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/co;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/16 v2, 0x4ae

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    invoke-virtual {p3}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v0

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, p1, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p3, v2, v1, v0, v5}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/Object;III)V

    invoke-virtual {p2}, Lcom/google/googlenav/co;->b()Lcom/google/googlenav/cc;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p3, p2}, Lbj/ag;->a(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;Lcom/google/googlenav/co;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/google/googlenav/co;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "\n"

    invoke-virtual {p3, v0}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    invoke-virtual {p3}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v0

    invoke-direct {p0, p1, p3, p2}, Lbj/ag;->b(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;Lcom/google/googlenav/co;)V

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v1, p1, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p3}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v2

    invoke-virtual {p3, v1, v0, v2, v5}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/Object;III)V

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;II)V
    .locals 3

    const/16 v2, 0x21

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0f0120

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2, v0, p3, p4, v2}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/Object;III)V

    new-instance v0, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v0}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {p2, v0, p3, p4, v2}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/Object;III)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;Lcom/google/googlenav/co;)V
    .locals 7

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v0

    :goto_0
    const/4 v3, 0x3

    if-ge v0, v3, :cond_2

    invoke-virtual {p3}, Lcom/google/googlenav/co;->d()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p3, v1}, Lcom/google/googlenav/co;->a(I)Lcom/google/googlenav/cc;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cc;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    if-lez v0, :cond_0

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    invoke-virtual {v3}, Lcom/google/googlenav/cc;->c()J

    move-result-wide v3

    const-wide/16 v5, 0x3c

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-lez v0, :cond_3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbj/ag;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/android/ak;->a(Landroid/view/View;)Lcom/google/googlenav/ui/android/ak;

    invoke-virtual {p2}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v0

    const-string v1, "("

    invoke-virtual {p2, v1}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    invoke-direct {p0, p1, p2, p3}, Lbj/ag;->b(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;Lcom/google/googlenav/co;)V

    const-string v1, ")"

    invoke-virtual {p2, v1}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0f011f

    invoke-direct {v1, p1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {p2, v1, v0, v2, v3}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/Object;III)V

    :cond_3
    return-void
.end method

.method public static a(Landroid/widget/TextView;ILcom/google/googlenav/cm;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "placeholder"

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Lbj/aj;

    invoke-direct {v1, p2, p1}, Lbj/aj;-><init>(Lcom/google/googlenav/cm;I)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lbj/ag;Lbj/ak;)V
    .locals 0

    invoke-direct {p0, p1}, Lbj/ag;->a(Lbj/ak;)V

    return-void
.end method

.method private a(Lbj/ak;)V
    .locals 1

    iget-boolean v0, p0, Lbj/ag;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lbj/ag;->b(Lbj/ak;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lbj/ag;->c(Lbj/ak;)V

    goto :goto_0
.end method

.method static synthetic a(Lbj/ag;)Z
    .locals 1

    iget-boolean v0, p0, Lbj/ag;->b:Z

    return v0
.end method

.method static synthetic a(Lbj/ag;Z)Z
    .locals 0

    iput-boolean p1, p0, Lbj/ag;->b:Z

    return p1
.end method

.method static synthetic b(Lbj/ag;)I
    .locals 1

    iget v0, p0, Lbj/ag;->e:I

    return v0
.end method

.method private b(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;Lcom/google/googlenav/co;)V
    .locals 6

    const/4 v5, 0x3

    invoke-virtual {p3}, Lcom/google/googlenav/co;->d()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x4ba

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lcom/google/googlenav/co;->d()I

    move-result v0

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p3, v0}, Lcom/google/googlenav/co;->a(I)Lcom/google/googlenav/cc;

    move-result-object v2

    if-lez v0, :cond_2

    const-string v3, " "

    invoke-virtual {p2, v3}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    :cond_2
    invoke-virtual {p2}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/googlenav/cc;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    invoke-virtual {v2}, Lcom/google/googlenav/cc;->e()I

    move-result v2

    if-ne v2, v5, :cond_3

    invoke-virtual {p2}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v2

    invoke-direct {p0, p1, p2, v3, v2}, Lbj/ag;->a(Landroid/content/Context;Lcom/google/googlenav/ui/android/ak;II)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b(Lbj/ak;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Lbj/ak;->a(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lbj/ak;->i(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lbj/ak;->j(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lbj/ak;->k(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private c(Lbj/ak;)V
    .locals 3

    const/16 v2, 0x8

    invoke-static {p1}, Lbj/ak;->i(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lbj/ak;->j(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lbj/ak;->k(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/ag;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/ak;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/ak;-><init>(Lbj/ah;)V

    const v0, 0x7f100452

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->a(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f100454

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbj/ak;->a(Lbj/ak;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    const v0, 0x7f100033

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/ak;->a(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100453

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/ak;->b(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100231

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->b(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f1001c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->c(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f100450

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->d(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f10044f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->e(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f100455

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/ak;->c(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100457

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/ak;->d(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100458

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    invoke-static {v1, v0}, Lbj/ak;->a(Lbj/ak;Lcom/google/googlenav/ui/android/MultipleTextLineLayout;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    const v0, 0x7f10045a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    invoke-static {v1, v0}, Lbj/ak;->b(Lbj/ak;Lcom/google/googlenav/ui/android/MultipleTextLineLayout;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    const v0, 0x7f100456

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->f(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f100459

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->g(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f10045b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->h(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f100296

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->i(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    const v0, 0x7f10002f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lbj/ak;->j(Lbj/ak;Landroid/view/View;)Landroid/view/View;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/cm;Lbj/ak;Lcom/google/googlenav/ui/e;)V
    .locals 7

    const/4 v0, 0x0

    invoke-static {p2}, Lbj/ak;->l(Lbj/ak;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->j()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-eqz v5, :cond_0

    invoke-static {p2}, Lbj/ak;->m(Lbj/ak;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/dialog/ba;->a(J)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-static {p2}, Lbj/ak;->m(Lbj/ak;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->n(Lbj/ak;)Landroid/widget/TextView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->d()I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p2}, Lbj/ak;->o(Lbj/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/android/ak;

    invoke-direct {v3, v2}, Lcom/google/googlenav/ui/android/ak;-><init>(Landroid/content/Context;)V

    const/16 v4, 0x4bb

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0f011e

    invoke-direct {v4, v2, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/ak;->b()I

    move-result v2

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v0, v2, v5}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/Object;III)V

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;->removeAllViews()V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;->a(Lcom/google/googlenav/ui/android/ak;)V

    :goto_1
    return-void

    :cond_0
    invoke-static {p2}, Lbj/ak;->m(Lbj/ak;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->i()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/view/dialog/ba;->a(J)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-static {p2}, Lbj/ak;->n(Lbj/ak;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {p2}, Lbj/ak;->n(Lbj/ak;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v3, v4, p1}, Lbj/ag;->a(Landroid/widget/TextView;ILcom/google/googlenav/cm;)V

    invoke-static {p2}, Lbj/ak;->m(Lbj/ak;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->n(Lbj/ak;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lbj/ak;->o(Lbj/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/android/ak;

    invoke-direct {v3, v2}, Lcom/google/googlenav/ui/android/ak;-><init>(Landroid/content/Context;)V

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->d()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/cm;->a(I)Lcom/google/googlenav/co;

    move-result-object v4

    invoke-direct {p0, v2, v4, v3}, Lbj/ag;->a(Landroid/content/Context;Lcom/google/googlenav/co;Lcom/google/googlenav/ui/android/ak;)V

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->d()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v0, v4, :cond_2

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/android/ak;->a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;->removeAllViews()V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;->a(Lcom/google/googlenav/ui/android/ak;)V

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    check-cast p2, Lbj/ak;

    invoke-static {p2}, Lbj/ak;->a(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->b(Lbj/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/MultipleTextLineLayout;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->c(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->d(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p2}, Lbj/ag;->a(Lbj/ak;)V

    iget-boolean v0, p0, Lbj/ag;->d:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lbj/ak;->e(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->f(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->g(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-static {p2}, Lbj/ak;->h(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbj/ah;

    invoke-direct {v1, p0, p2}, Lbj/ah;-><init>(Lbj/ag;Lbj/ak;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbj/ag;->a:Lcom/google/googlenav/cm;

    invoke-virtual {p0, v0, p2, p1}, Lbj/ag;->a(Lcom/google/googlenav/cm;Lbj/ak;Lcom/google/googlenav/ui/e;)V

    return-void

    :cond_0
    invoke-static {p2}, Lbj/ak;->e(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->f(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p2}, Lbj/ak;->g(Lbj/ak;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0401bc

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lbj/ag;->a:Lcom/google/googlenav/cm;

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->a()Z

    move-result v0

    return v0
.end method
