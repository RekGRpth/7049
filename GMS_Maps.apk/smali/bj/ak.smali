.class Lbj/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/bB;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

.field private n:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lbj/ah;)V
    .locals 0

    invoke-direct {p0}, Lbj/ak;-><init>()V

    return-void
.end method

.method static synthetic a(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->c:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lbj/ak;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    iput-object p1, p0, Lbj/ak;->d:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic a(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    iput-object p1, p0, Lbj/ak;->e:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lbj/ak;Lcom/google/googlenav/ui/android/MultipleTextLineLayout;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .locals 0

    iput-object p1, p0, Lbj/ak;->m:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object p1
.end method

.method static synthetic b(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->g:Landroid/view/View;

    return-object p1
.end method

.method static synthetic b(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    iput-object p1, p0, Lbj/ak;->f:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic b(Lbj/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .locals 1

    iget-object v0, p0, Lbj/ak;->n:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object v0
.end method

.method static synthetic b(Lbj/ak;Lcom/google/googlenav/ui/android/MultipleTextLineLayout;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .locals 0

    iput-object p1, p0, Lbj/ak;->n:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object p1
.end method

.method static synthetic c(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->h:Landroid/view/View;

    return-object p1
.end method

.method static synthetic c(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    iput-object p1, p0, Lbj/ak;->k:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic d(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->p:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->i:Landroid/view/View;

    return-object p1
.end method

.method static synthetic d(Lbj/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    iput-object p1, p0, Lbj/ak;->l:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic e(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->q:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->j:Landroid/view/View;

    return-object p1
.end method

.method static synthetic f(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->o:Landroid/view/View;

    return-object p1
.end method

.method static synthetic g(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->p:Landroid/view/View;

    return-object p1
.end method

.method static synthetic h(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->q:Landroid/view/View;

    return-object p1
.end method

.method static synthetic i(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic i(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->b:Landroid/view/View;

    return-object p1
.end method

.method static synthetic j(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lbj/ak;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lbj/ak;->a:Landroid/view/View;

    return-object p1
.end method

.method static synthetic k(Lbj/ak;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbj/ak;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lbj/ak;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lbj/ak;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(Lbj/ak;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lbj/ak;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic n(Lbj/ak;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lbj/ak;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(Lbj/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .locals 1

    iget-object v0, p0, Lbj/ak;->m:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object v0
.end method
