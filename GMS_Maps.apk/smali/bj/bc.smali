.class public Lbj/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:I

.field private final b:Ljava/util/List;

.field private final c:Lcom/google/googlenav/ui/br;

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(IZLjava/util/List;Lcom/google/googlenav/ui/br;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbj/bc;->a:I

    iput-object p3, p0, Lbj/bc;->b:Ljava/util/List;

    iput-object p4, p0, Lbj/bc;->c:Lcom/google/googlenav/ui/br;

    iput-boolean p5, p0, Lbj/bc;->d:Z

    iput-boolean p2, p0, Lbj/bc;->e:Z

    return-void
.end method

.method private a(Lbj/bg;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/bs;
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x4

    const/16 v3, 0xa

    iget-object v0, p0, Lbj/bc;->c:Lcom/google/googlenav/ui/br;

    if-eqz v0, :cond_2

    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v1, Lcom/google/googlenav/ui/bs;

    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v3, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v1, v0, v3}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1}, Lbj/bg;->d()Landroid/widget/ImageView;

    move-result-object v3

    iget-object v0, p0, Lbj/bc;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object v0, v1

    :goto_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v2, v3

    :cond_0
    invoke-virtual {p1}, Lbj/bg;->c()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lbj/bg;->b()Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/e;Ljava/util/List;)V
    .locals 6

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    move v2, v3

    :goto_0
    iget-object v0, p0, Lbj/bc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/bg;

    iget-object v1, p0, Lbj/bc;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0, v1}, Lbj/bc;->a(Lbj/bg;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/bs;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbj/bg;->d()Landroid/widget/ImageView;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-boolean v1, p0, Lbj/bc;->d:Z

    if-nez v1, :cond_1

    const/4 v1, 0x3

    if-ge v2, v1, :cond_2

    :cond_1
    invoke-virtual {v0}, Lbj/bg;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-virtual {v0}, Lbj/bg;->a()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbj/be;

    const/4 v5, 0x0

    invoke-direct {v1, p1, v2, v5}, Lbj/be;-><init>(Lcom/google/googlenav/ui/e;ILbj/bd;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lbj/bg;->a()Landroid/view/View;

    move-result-object v1

    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lbj/bc;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lbj/bf;

    iget-object v3, p0, Lbj/bc;->c:Lcom/google/googlenav/ui/br;

    invoke-direct {v2, v3, v4}, Lbj/bf;-><init>(Lcom/google/googlenav/ui/br;Ljava/util/Map;)V

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    :cond_4
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/bc;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 7

    const/4 v3, 0x0

    iget-boolean v0, p0, Lbj/bc;->e:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    const v0, 0x7f10035f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbj/bc;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v2

    move v1, v3

    :goto_0
    iget-object v4, p0, Lbj/bc;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    const v4, 0x7f04013a

    invoke-static {v4, v0, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    new-instance v5, Lbj/bg;

    invoke-direct {v5, v4}, Lbj/bg;-><init>(Landroid/view/View;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lbj/bh;

    const v1, 0x7f100360

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v1, 0x7f100361

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v1, 0x7f100362

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const v1, 0x7f100363

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lbj/bh;-><init>(Landroid/view/View;Ljava/util/List;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    check-cast p2, Lbj/bh;

    invoke-virtual {p2}, Lbj/bh;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbj/bc;->a(Lcom/google/googlenav/ui/e;Ljava/util/List;)V

    invoke-virtual {p0}, Lbj/bc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbj/bc;->d:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lbj/bh;->c()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x505

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lbj/bh;->d()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p2}, Lbj/bh;->e()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Lbj/bh;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {p2}, Lbj/bh;->b()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbj/bd;

    invoke-direct {v1, p0, p1}, Lbj/bd;-><init>(Lbj/bc;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_2
    iget-object v0, p0, Lbj/bc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/android/bi;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lbj/bh;->c()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lbj/bh;->d()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p2}, Lbj/bh;->e()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040139

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lbj/bc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
