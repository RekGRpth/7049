.class public Lbj/bt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Ljava/lang/CharSequence;

.field final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lbj/bt;->a:Ljava/lang/CharSequence;

    iput p3, p0, Lbj/bt;->b:I

    return-void

    :cond_0
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/bu;

    invoke-direct {v1}, Lbj/bu;-><init>()V

    const v0, 0x7f100034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/bu;->a:Landroid/widget/TextView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lbj/bu;

    iget-object v0, p2, Lbj/bu;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/bt;->a:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040167

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lbj/bt;->b:I

    return v0
.end method
