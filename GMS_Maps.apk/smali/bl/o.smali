.class public Lbl/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lbl/b;

.field b:Lbl/h;

.field private final c:Ljava/util/Set;

.field private d:Z

.field private final e:Ljava/util/Queue;

.field private final f:Ljava/lang/Object;

.field private final g:Lcom/google/googlenav/ai;

.field private final h:Lbl/j;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ai;Lbl/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbl/o;->d:Z

    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->e:Ljava/util/Queue;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbl/o;->f:Ljava/lang/Object;

    iput-object p2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    iput-object p3, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {p0, p1}, Lbl/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Lbl/o;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lbl/h;Lbl/f;ILbl/e;)Z
    .locals 4

    const/4 v1, 0x0

    if-nez p2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lbl/h;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/d;

    iget-object v3, v0, Lbl/d;->a:Lbl/e;

    if-ne v3, p4, :cond_1

    iget v3, v0, Lbl/d;->b:I

    if-lez v3, :cond_1

    iget p3, v0, Lbl/d;->b:I

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lbl/f;->a(J)I

    move-result v0

    if-ge v0, p3, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private m()V
    .locals 3

    iget-object v1, p0, Lbl/o;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lbl/o;->b:Lbl/h;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    invoke-direct {p0}, Lbl/o;->m()V

    :cond_0
    return-void
.end method

.method public a(Lbf/m;)V
    .locals 1

    new-instance v0, Lbl/q;

    invoke-direct {v0, p0, p1}, Lbl/q;-><init>(Lbl/o;Lbf/m;)V

    invoke-virtual {p0, v0}, Lbl/o;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lbl/b;

    invoke-direct {v0, p1}, Lbl/b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lbl/o;->a:Lbl/b;

    invoke-virtual {p0}, Lbl/o;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    new-instance v0, Lbl/p;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbl/p;-><init>(Lbl/o;Las/c;)V

    invoke-virtual {v0}, Lbl/p;->g()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 3

    iget-object v1, p0, Lbl/o;->e:Ljava/util/Queue;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbl/o;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbl/o;->e:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 4

    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbl/o;->c()V

    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbl/o;->f()V

    :cond_1
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbl/o;->i()V

    :cond_2
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lbl/o;->k()Lbl/h;

    move-result-object v0

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lbl/a;->a(Lcom/google/googlenav/ai;Lbl/h;Z)V

    invoke-direct {p0}, Lbl/o;->m()V

    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lbf/m;)V
    .locals 3

    new-instance v0, Lbl/r;

    invoke-direct {v0, p0, p1}, Lbl/r;-><init>(Lbl/o;Lbf/m;)V

    invoke-virtual {p0, v0}, Lbl/o;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    iget-object v1, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbl/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v1, p0, Lbl/o;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method c()V
    .locals 6

    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v1, Lbl/e;->c:Lbl/e;

    invoke-virtual {v0, v1}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v2, v1}, Lbl/j;->a(Ljava/lang/String;)Lbl/g;

    move-result-object v2

    const/16 v3, 0xe

    sget-object v4, Lbl/e;->c:Lbl/e;

    invoke-direct {p0, v0, v2, v3, v4}, Lbl/o;->a(Lbl/h;Lbl/f;ILbl/e;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lbl/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0, v2}, Lbl/h;->a(Lbl/g;)V

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lbl/h;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lbl/h;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v3

    invoke-virtual {v3}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    goto :goto_0
.end method

.method public c(Lbf/m;)V
    .locals 2

    new-instance v0, Lbl/s;

    invoke-direct {v0, p0, p1}, Lbl/s;-><init>(Lbl/o;Lbf/m;)V

    invoke-virtual {p0, v0}, Lbl/o;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    iget-object v1, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbl/j;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v1, p0, Lbl/o;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Lbl/h;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v3, Lbl/e;->c:Lbl/e;

    invoke-virtual {v0, v3}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lbl/o;->h:Lbl/j;

    iget-object v4, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v4}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbl/h;->a(Lbl/g;)V

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()V
    .locals 5

    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v2, Lbl/e;->c:Lbl/e;

    invoke-virtual {v0, v2}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    iget-object v3, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v3}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v4

    invoke-virtual {v4}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method f()V
    .locals 4

    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v1, Lbl/e;->f:Lbl/e;

    invoke-virtual {v0, v1}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbl/o;->h:Lbl/j;

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbl/o;->h:Lbl/j;

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbl/j;->b(Ljava/lang/String;)Lbl/f;

    move-result-object v1

    const/16 v2, 0xe

    sget-object v3, Lbl/e;->f:Lbl/e;

    invoke-direct {p0, v0, v1, v2, v3}, Lbl/o;->a(Lbl/h;Lbl/f;ILbl/e;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lbl/h;->a(Lbl/f;)V

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    :cond_0
    return-void
.end method

.method public g()Lbl/h;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v3, Lbl/e;->f:Lbl/e;

    invoke-virtual {v0, v3}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lbl/o;->h:Lbl/j;

    iget-object v4, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v4}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbl/h;->a(Lbl/f;)V

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method h()Lbl/h;
    .locals 5

    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v2, Lbl/e;->e:Lbl/e;

    invoke-virtual {v0, v2}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    iget-object v3, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v3}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method i()V
    .locals 6

    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v1, Lbl/e;->d:Lbl/e;

    invoke-virtual {v0, v1}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-static {v0, v2}, LaV/g;->a(LaH/m;LaN/B;)I

    move-result v2

    const/4 v0, -0x1

    if-eq v2, v0, :cond_1

    invoke-virtual {v1}, Lbl/h;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/d;

    iget-object v4, v0, Lbl/d;->a:Lbl/e;

    sget-object v5, Lbl/e;->d:Lbl/e;

    if-ne v4, v5, :cond_0

    iget v0, v0, Lbl/d;->b:I

    if-lt v0, v2, :cond_0

    iput-object v1, p0, Lbl/o;->b:Lbl/h;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public j()Z
    .locals 1

    invoke-virtual {p0}, Lbl/o;->k()Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Lbl/h;
    .locals 2

    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->b:Lbl/h;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method l()V
    .locals 2

    :goto_0
    iget-object v1, p0, Lbl/o;->e:Ljava/util/Queue;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbl/o;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbl/o;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lbl/o;->b()V

    return-void

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
