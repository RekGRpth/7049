.class Lbm/p;
.super Law/t;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbm/m;


# direct methods
.method constructor <init>(Lbm/m;I[BZZZLjava/lang/Object;)V
    .locals 7

    iput-object p1, p0, Lbm/p;->a:Lbm/m;

    move-object v0, p0

    move v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Law/t;-><init>(I[BZZZLjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public d_()V
    .locals 4

    const-class v1, Lbm/m;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lbm/m;->f()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lbm/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbm/m;->h()Lbm/q;

    move-result-object v0

    invoke-interface {v0}, Lbm/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbm/m;->h()Lbm/q;

    move-result-object v0

    invoke-interface {v0}, Lbm/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v2, p0, Lbm/p;->a:Lbm/m;

    invoke-static {v2}, Lbm/m;->a(Lbm/m;)I

    move-result v2

    invoke-static {v0, v2}, Lbm/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {}, Lbm/m;->h()Lbm/q;

    move-result-object v2

    invoke-interface {v2, v0}, Lbm/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lbm/p;->a:Lbm/m;

    invoke-virtual {v0}, Lbm/m;->e()V

    :cond_1
    monitor-exit v1

    return-void

    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lbm/m;

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".uploadEventLog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    const-string v2, "USER_EVENTSUserEventReporter"

    invoke-static {v2, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public u_()V
    .locals 1

    iget-object v0, p0, Lbm/p;->a:Lbm/m;

    invoke-virtual {v0}, Lbm/m;->e()V

    return-void
.end method
