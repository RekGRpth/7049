.class public Lbn/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.google.android.apps.genie.geniewidget.weather/weather/daily/0"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbn/a;->a:Landroid/net/Uri;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lbn/b;
    .locals 14

    const/4 v6, 0x0

    sget-object v0, Lbn/c;->a:Lbn/c;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbn/a;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "timestamp"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v0, "iconUrl"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    const-string v0, "temperature"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    const-string v0, "location"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-wide/16 v2, -0x1

    move-object v0, v6

    move-wide v4, v2

    move-object v3, v6

    move-object v2, v6

    :goto_0
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    cmp-long v11, v11, v4

    if-lez v11, :cond_1

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-static {v0}, Lbn/a;->a(Ljava/lang/String;)Lbn/c;

    move-result-object v4

    new-instance v0, Lbn/b;

    invoke-direct {v0, v4, v3, v2}, Lbn/b;-><init>(Lbn/c;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v6

    :goto_3
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v6

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_3

    :cond_1
    move-object v13, v0

    move-object v0, v3

    move-object v3, v2

    move-object v2, v13

    goto :goto_1

    :cond_2
    move-object v13, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v13

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Lbn/c;
    .locals 1

    if-nez p0, :cond_0

    sget-object v0, Lbn/c;->a:Lbn/c;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "chance_of_rain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbn/c;->b:Lbn/c;

    goto :goto_0

    :cond_1
    const-string v0, "chance_snow"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lbn/c;->c:Lbn/c;

    goto :goto_0

    :cond_2
    const-string v0, "chance_storm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lbn/c;->d:Lbn/c;

    goto :goto_0

    :cond_3
    const-string v0, "clear_day"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lbn/c;->e:Lbn/c;

    goto :goto_0

    :cond_4
    const-string v0, "clear_night"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lbn/c;->g:Lbn/c;

    goto :goto_0

    :cond_5
    const-string v0, "heavy_rain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lbn/c;->k:Lbn/c;

    goto :goto_0

    :cond_6
    const-string v0, "icy_sleet"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lbn/c;->l:Lbn/c;

    goto :goto_0

    :cond_7
    const-string v0, "partly_cloudy"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lbn/c;->n:Lbn/c;

    goto :goto_0

    :cond_8
    const-string v0, "rain_day"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lbn/c;->o:Lbn/c;

    goto :goto_0

    :cond_9
    const-string v0, "rain_night"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lbn/c;->q:Lbn/c;

    goto :goto_0

    :cond_a
    const-string v0, "snow_rain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lbn/c;->s:Lbn/c;

    goto :goto_0

    :cond_b
    const-string v0, "unknown"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Lbn/c;->a:Lbn/c;

    goto/16 :goto_0

    :cond_c
    const-string v0, "flurries"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lbn/c;->i:Lbn/c;

    goto/16 :goto_0

    :cond_d
    const-string v0, "windy"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lbn/c;->v:Lbn/c;

    goto/16 :goto_0

    :cond_e
    const-string v0, "clear"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    sget-object v0, Lbn/c;->f:Lbn/c;

    goto/16 :goto_0

    :cond_f
    const-string v0, "cloudy"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    sget-object v0, Lbn/c;->h:Lbn/c;

    goto/16 :goto_0

    :cond_10
    const-string v0, "fog"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    sget-object v0, Lbn/c;->j:Lbn/c;

    goto/16 :goto_0

    :cond_11
    const-string v0, "snow"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    sget-object v0, Lbn/c;->r:Lbn/c;

    goto/16 :goto_0

    :cond_12
    const-string v0, "sunny"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    sget-object v0, Lbn/c;->t:Lbn/c;

    goto/16 :goto_0

    :cond_13
    const-string v0, "thunderstorm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    sget-object v0, Lbn/c;->u:Lbn/c;

    goto/16 :goto_0

    :cond_14
    const-string v0, "rain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    sget-object v0, Lbn/c;->p:Lbn/c;

    goto/16 :goto_0

    :cond_15
    const-string v0, "mist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Lbn/c;->m:Lbn/c;

    goto/16 :goto_0

    :cond_16
    sget-object v0, Lbn/c;->a:Lbn/c;

    goto/16 :goto_0
.end method
