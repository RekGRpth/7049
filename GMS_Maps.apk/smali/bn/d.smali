.class public Lbn/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lbn/b;

.field private c:J

.field private d:Lbn/f;

.field private e:Lbn/c;

.field private final f:Lbo/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/google/googlenav/wallpaper/i;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbn/d;->a:Landroid/content/Context;

    new-instance v0, Lbn/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbn/f;-><init>(Lbn/d;Lbn/e;)V

    iput-object v0, p0, Lbn/d;->d:Lbn/f;

    new-instance v0, Lbo/a;

    invoke-direct {v0, p1, p2, p3, p4}, Lbo/a;-><init>(Landroid/content/Context;IILcom/google/googlenav/wallpaper/i;)V

    iput-object v0, p0, Lbn/d;->f:Lbo/a;

    return-void
.end method

.method static synthetic a(Lbn/d;J)J
    .locals 0

    iput-wide p1, p0, Lbn/d;->c:J

    return-wide p1
.end method

.method static synthetic a(Lbn/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbn/d;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lbn/d;Lbn/b;)Lbn/b;
    .locals 0

    iput-object p1, p0, Lbn/d;->b:Lbn/b;

    return-object p1
.end method

.method private a()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v3

    iget-wide v5, p0, Lbn/d;->c:J

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x927c0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lbn/d;->d:Lbn/f;

    invoke-virtual {v3}, Lbn/f;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v3, v4, :cond_2

    move v3, v1

    :goto_1
    iget-object v4, p0, Lbn/d;->d:Lbn/f;

    invoke-virtual {v4}, Lbn/f;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v4, v5, :cond_0

    new-instance v4, Lbn/f;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lbn/f;-><init>(Lbn/d;Lbn/e;)V

    iput-object v4, p0, Lbn/d;->d:Lbn/f;

    :cond_0
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    :goto_2
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method static synthetic a(Lbn/d;Lbn/c;)Z
    .locals 1

    invoke-direct {p0, p1}, Lbn/d;->b(Lbn/c;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lbn/d;)Lbn/c;
    .locals 1

    iget-object v0, p0, Lbn/d;->e:Lbn/c;

    return-object v0
.end method

.method private b(Lbn/c;)Z
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Lbn/c;->a:Lbn/c;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lbn/d;)Lbo/a;
    .locals 1

    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    return-object v0
.end method


# virtual methods
.method public a(F)V
    .locals 1

    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    invoke-virtual {v0, p1}, Lbo/a;->a(F)V

    return-void
.end method

.method public a(Landroid/graphics/Canvas;IIZ)V
    .locals 2

    invoke-direct {p0}, Lbn/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbn/d;->d:Lbn/f;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lbn/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    iget-object v0, p0, Lbn/d;->b:Lbn/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbn/d;->b:Lbn/b;

    iget-object v0, v0, Lbn/b;->a:Lbn/c;

    if-eqz v0, :cond_1

    sget-object v0, Lbn/c;->a:Lbn/c;

    iget-object v1, p0, Lbn/d;->b:Lbn/b;

    iget-object v1, v1, Lbn/b;->a:Lbn/c;

    if-ne v0, v1, :cond_3

    :cond_1
    iget-object v0, p0, Lbn/d;->e:Lbn/c;

    if-eqz v0, :cond_2

    sget-object v0, Lbn/c;->a:Lbn/c;

    iget-object v1, p0, Lbn/d;->e:Lbn/c;

    if-ne v0, v1, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbo/a;->a(Landroid/graphics/Canvas;IIZ)V

    goto :goto_0
.end method

.method public a(Lbn/c;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lbn/d;->e:Lbn/c;

    invoke-direct {p0, p1}, Lbn/d;->b(Lbn/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    new-instance v1, Lbn/b;

    invoke-direct {v1, p1, v2, v2}, Lbn/b;-><init>(Lbn/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbo/a;->a(Lbn/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbn/d;->b:Lbn/b;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    invoke-virtual {v0, v2}, Lbo/a;->a(Lbn/b;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    iget-object v1, p0, Lbn/d;->b:Lbn/b;

    invoke-virtual {v0, v1}, Lbo/a;->a(Lbn/b;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lbn/d;->f:Lbo/a;

    invoke-virtual {v0, p1}, Lbo/a;->a(Z)V

    return-void
.end method
