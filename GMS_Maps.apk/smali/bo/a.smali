.class public Lbo/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbo/f;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lbt/e;

.field private c:Lbs/a;

.field private d:Lbp/d;

.field private e:Lbq/b;

.field private f:Lbr/a;

.field private g:F

.field private h:Z

.field private i:Z

.field private final j:I

.field private final k:I

.field private l:Lbu/a;

.field private m:Lcom/google/googlenav/wallpaper/i;

.field private final n:Landroid/os/Handler;

.field private o:Lbo/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/google/googlenav/wallpaper/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lbu/a;

    invoke-direct {v0}, Lbu/a;-><init>()V

    iput-object v0, p0, Lbo/a;->l:Lbu/a;

    new-instance v0, Lbo/b;

    invoke-direct {v0, p0}, Lbo/b;-><init>(Lbo/a;)V

    iput-object v0, p0, Lbo/a;->n:Landroid/os/Handler;

    iput-object p1, p0, Lbo/a;->a:Landroid/content/Context;

    iput-object p4, p0, Lbo/a;->m:Lcom/google/googlenav/wallpaper/i;

    iput p2, p0, Lbo/a;->j:I

    iput p3, p0, Lbo/a;->k:I

    new-instance v0, Lbo/d;

    invoke-direct {v0, p0}, Lbo/d;-><init>(Lbo/f;)V

    iput-object v0, p0, Lbo/a;->o:Lbo/d;

    iget-object v0, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v0}, Lbo/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbo/a;->b()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lbo/a;)Lcom/google/googlenav/wallpaper/i;
    .locals 1

    iget-object v0, p0, Lbo/a;->m:Lcom/google/googlenav/wallpaper/i;

    return-object v0
.end method

.method private b()V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lbt/e;

    iget-object v1, p0, Lbo/a;->a:Landroid/content/Context;

    iget v2, p0, Lbo/a;->j:I

    iget v3, p0, Lbo/a;->k:I

    invoke-direct {p0}, Lbo/a;->c()[Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbt/e;-><init>(Landroid/content/Context;II[Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lbo/a;->b:Lbt/e;

    new-instance v0, Lbs/a;

    iget v1, p0, Lbo/a;->j:I

    iget v2, p0, Lbo/a;->k:I

    invoke-direct {v0, v1, v2}, Lbs/a;-><init>(II)V

    iput-object v0, p0, Lbo/a;->c:Lbs/a;

    new-instance v0, Lbp/d;

    iget v1, p0, Lbo/a;->j:I

    iget v2, p0, Lbo/a;->k:I

    iget-object v3, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v3}, Lbo/d;->c()[Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v4, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v4}, Lbo/d;->e()[Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v5, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v5}, Lbo/d;->d()[Landroid/graphics/Bitmap;

    move-result-object v5

    aget-object v5, v5, v6

    invoke-direct/range {v0 .. v5}, Lbp/d;-><init>(II[Landroid/graphics/Bitmap;[Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lbo/a;->d:Lbp/d;

    new-instance v0, Lbq/b;

    iget v1, p0, Lbo/a;->j:I

    iget v2, p0, Lbo/a;->k:I

    iget-object v3, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v3}, Lbo/d;->f()[Landroid/graphics/Bitmap;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-direct {v0, v1, v2, v3}, Lbq/b;-><init>(IILandroid/graphics/Bitmap;)V

    iput-object v0, p0, Lbo/a;->e:Lbq/b;

    new-instance v0, Lbr/a;

    iget-object v1, p0, Lbo/a;->a:Landroid/content/Context;

    iget v2, p0, Lbo/a;->j:I

    const/high16 v3, 0x41c80000

    iget-object v4, p0, Lbo/a;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    iget-object v4, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v4}, Lbo/d;->g()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/a;-><init>(Landroid/content/Context;IFLandroid/graphics/Bitmap;)V

    iput-object v0, p0, Lbo/a;->f:Lbr/a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbo/a;->i:Z

    iget-object v0, p0, Lbo/a;->m:Lcom/google/googlenav/wallpaper/i;

    invoke-interface {v0}, Lcom/google/googlenav/wallpaper/i;->a()V

    return-void
.end method

.method private c()[Landroid/graphics/Bitmap;
    .locals 10

    const/16 v9, 0x60

    const/4 v8, 0x3

    const/4 v1, 0x0

    iget-object v0, p0, Lbo/a;->o:Lbo/d;

    invoke-virtual {v0}, Lbo/d;->b()[Landroid/graphics/Bitmap;

    move-result-object v0

    aget-object v3, v0, v1

    const/16 v0, 0x9

    new-array v4, v0, [Landroid/graphics/Bitmap;

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_1

    move v0, v1

    :goto_1
    if-ge v0, v8, :cond_0

    mul-int/lit8 v5, v2, 0x3

    add-int/2addr v5, v0

    mul-int/lit8 v6, v0, 0x60

    mul-int/lit8 v7, v2, 0x60

    invoke-static {v3, v6, v7, v9, v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-object v4
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lbo/a;->b()V

    return-void
.end method

.method public a(F)V
    .locals 2

    iput p1, p0, Lbo/a;->g:F

    iget-boolean v0, p0, Lbo/a;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    iget v1, p0, Lbo/a;->g:F

    invoke-virtual {v0, v1}, Lbt/e;->b(F)V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    iget v1, p0, Lbo/a;->g:F

    invoke-virtual {v0, v1}, Lbs/a;->a(F)V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    iget v1, p0, Lbo/a;->g:F

    invoke-virtual {v0, v1}, Lbp/d;->a(F)V

    iget-object v0, p0, Lbo/a;->f:Lbr/a;

    iget v1, p0, Lbo/a;->g:F

    invoke-virtual {v0, v1}, Lbr/a;->a(F)V

    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Canvas;IIZ)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lbo/a;->i:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    sub-int v0, p2, p3

    if-gez v0, :cond_2

    int-to-float v0, v0

    iget v1, p0, Lbo/a;->g:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    add-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0, p1, p4}, Lbq/b;->a(Landroid/graphics/Canvas;Z)V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0, p1, p4}, Lbt/e;->a(Landroid/graphics/Canvas;Z)V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0, p1, p4}, Lbs/a;->a(Landroid/graphics/Canvas;Z)V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0, p1, p4}, Lbp/d;->a(Landroid/graphics/Canvas;Z)V

    iget-object v0, p0, Lbo/a;->f:Lbr/a;

    invoke-virtual {v0, p1}, Lbr/a;->a(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-boolean v0, p0, Lbo/a;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbo/a;->n:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lbo/a;->n:Landroid/os/Handler;

    const-wide/16 v1, 0x6

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_2
    div-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method public a(Lbn/b;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lbo/a;->i:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lbn/c;->a:Lbn/c;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lbn/b;->a:Lbn/c;

    if-eqz v1, :cond_1

    iget-object v0, p1, Lbn/b;->a:Lbn/c;

    iget-object v1, p0, Lbo/a;->f:Lbr/a;

    iget-object v2, p1, Lbn/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbr/a;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lbo/a;->f:Lbr/a;

    iget-object v2, p1, Lbn/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbr/a;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lbo/a;->f:Lbr/a;

    invoke-virtual {v1, v0}, Lbr/a;->a(Lbn/c;)V

    :cond_1
    sget-object v1, Lbo/c;->a:[I

    invoke-virtual {v0}, Lbn/c;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Lbo/a;->m:Lcom/google/googlenav/wallpaper/i;

    invoke-interface {v0}, Lcom/google/googlenav/wallpaper/i;->a()V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    sget-object v1, Lbp/f;->b:Lbp/f;

    sget-object v2, Lbp/c;->a:Lbp/c;

    invoke-virtual {v0, v1, v2, v3}, Lbp/d;->a(Lbp/f;Lbp/c;Z)V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    sget-object v1, Lbp/f;->a:Lbp/f;

    sget-object v2, Lbp/c;->a:Lbp/c;

    invoke-virtual {v0, v1, v2, v3}, Lbp/d;->a(Lbp/f;Lbp/c;Z)V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    sget-object v1, Lbp/f;->a:Lbp/f;

    invoke-virtual {v0, v1}, Lbq/b;->a(Lbp/f;)V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    sget-object v1, Lbp/f;->a:Lbp/f;

    sget-object v2, Lbp/c;->c:Lbp/c;

    invoke-virtual {v0, v1, v2, v3}, Lbp/d;->a(Lbp/f;Lbp/c;Z)V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    sget-object v1, Lbt/f;->a:Lbt/f;

    invoke-virtual {v0, v1}, Lbt/e;->a(Lbt/f;)V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    sget-object v1, Lbt/f;->b:Lbt/f;

    invoke-virtual {v0, v1}, Lbt/e;->a(Lbt/f;)V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    sget-object v1, Lbt/f;->a:Lbt/f;

    invoke-virtual {v0, v1}, Lbt/e;->a(Lbt/f;)V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->a:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->a:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_9
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->b:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_a
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->b:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_b
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->c:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_c
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    sget-object v1, Lbp/f;->b:Lbp/f;

    sget-object v2, Lbp/c;->b:Lbp/c;

    sget-object v3, Lbp/e;->c:Lbp/e;

    invoke-virtual {v0, v1, v2, v3, v4}, Lbp/d;->a(Lbp/f;Lbp/c;Lbp/e;Z)V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->a:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_d
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    sget-object v1, Lbp/f;->b:Lbp/f;

    sget-object v2, Lbp/c;->c:Lbp/c;

    sget-object v3, Lbp/e;->a:Lbp/e;

    invoke-virtual {v0, v1, v2, v3, v4}, Lbp/d;->a(Lbp/f;Lbp/c;Lbp/e;Z)V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    sget-object v1, Lbs/c;->c:Lbs/c;

    invoke-virtual {v0, v1}, Lbs/a;->a(Lbs/c;)V

    goto/16 :goto_1

    :pswitch_e
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto/16 :goto_1

    :pswitch_f
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto/16 :goto_1

    :pswitch_10
    iget-object v0, p0, Lbo/a;->e:Lbq/b;

    invoke-virtual {v0}, Lbq/b;->a()V

    iget-object v0, p0, Lbo/a;->d:Lbp/d;

    invoke-virtual {v0}, Lbp/d;->a()V

    iget-object v0, p0, Lbo/a;->b:Lbt/e;

    invoke-virtual {v0}, Lbt/e;->a()V

    iget-object v0, p0, Lbo/a;->c:Lbs/a;

    invoke-virtual {v0}, Lbs/a;->a()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lbo/a;->h:Z

    return-void
.end method
