.class public Lbp/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field private final b:Lbu/f;

.field private final c:Landroid/graphics/Paint;

.field private final d:I

.field private final e:I

.field private final f:Landroid/graphics/Bitmap;

.field private final g:Landroid/graphics/Bitmap;

.field private final h:Landroid/graphics/Bitmap;

.field private final i:Lbp/b;

.field private final j:Lbu/b;

.field private final k:Lbu/b;

.field private l:Lbu/f;

.field private m:Landroid/graphics/Bitmap;

.field private n:Z

.field private o:I

.field private p:Z

.field private q:Lbp/c;

.field private r:Lbp/e;


# direct methods
.method public constructor <init>(Lbp/b;IILandroid/graphics/Paint;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lbp/c;Lbu/f;Landroid/graphics/Bitmap;Z)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0xff

    iput v1, p0, Lbp/a;->o:I

    iput-object p1, p0, Lbp/a;->i:Lbp/b;

    iput p2, p0, Lbp/a;->d:I

    iput p3, p0, Lbp/a;->e:I

    iput-object p4, p0, Lbp/a;->c:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iput-object p5, p0, Lbp/a;->f:Landroid/graphics/Bitmap;

    iput-object p6, p0, Lbp/a;->g:Landroid/graphics/Bitmap;

    iput-object p7, p0, Lbp/a;->q:Lbp/c;

    invoke-direct {p0}, Lbp/a;->c()V

    new-instance v1, Lbu/f;

    const-wide/high16 v2, 0x3fe0000000000000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    add-double/2addr v2, v4

    move-object/from16 v0, p8

    iget v4, v0, Lbu/f;->a:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x3fe0000000000000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    add-double/2addr v4, v6

    move-object/from16 v0, p8

    iget v6, v0, Lbu/f;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lbu/f;-><init>(DD)V

    iput-object v1, p0, Lbp/a;->l:Lbu/f;

    move-object/from16 v0, p9

    iput-object v0, p0, Lbp/a;->h:Landroid/graphics/Bitmap;

    move/from16 v0, p10

    iput-boolean v0, p0, Lbp/a;->n:Z

    new-instance v1, Lbu/b;

    const/high16 v2, 0x40000000

    invoke-direct {v1, v2}, Lbu/b;-><init>(F)V

    iput-object v1, p0, Lbp/a;->j:Lbu/b;

    new-instance v1, Lbu/b;

    invoke-direct {v1}, Lbu/b;-><init>()V

    iput-object v1, p0, Lbp/a;->k:Lbu/b;

    new-instance v1, Lbu/f;

    iget-object v2, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    iget-object v5, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v5, p2

    int-to-double v5, v5

    mul-double/2addr v3, v5

    double-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    iget-object v6, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v6, p3

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-float v4, v4

    add-float/2addr v3, v4

    invoke-direct {v1, v2, v3}, Lbu/f;-><init>(FF)V

    iput-object v1, p0, Lbp/a;->b:Lbu/f;

    iget-object v1, p0, Lbp/a;->j:Lbu/b;

    const/high16 v2, 0x40000000

    invoke-virtual {v1, v2}, Lbu/b;->a(F)V

    iget-object v1, p0, Lbp/a;->k:Lbu/b;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbu/b;->a(F)V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lbp/a;->g:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    sget-object v0, Lbp/c;->a:Lbp/c;

    iget-object v1, p0, Lbp/a;->q:Lbp/c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbp/a;->f:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lbp/c;
    .locals 1

    iget-object v0, p0, Lbp/a;->q:Lbp/c;

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;Z)V
    .locals 8

    const/4 v7, 0x0

    const/high16 v3, 0x41200000

    const v6, 0x3f733333

    const/4 v5, 0x0

    const/high16 v4, 0x40000000

    iget-object v0, p0, Lbp/a;->j:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->a()V

    iget-object v0, p0, Lbp/a;->k:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->a()V

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v1, v0, Lbu/f;->a:F

    iget-object v2, p0, Lbp/a;->l:Lbu/f;

    iget v2, v2, Lbu/f;->a:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Lbu/f;->a:F

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v1, v0, Lbu/f;->b:F

    iget-object v2, p0, Lbp/a;->l:Lbu/f;

    iget v2, v2, Lbu/f;->b:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Lbu/f;->b:F

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v0, v0, Lbu/f;->a:F

    iget v1, p0, Lbp/a;->d:I

    iget-object v2, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget-object v1, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    iput v1, v0, Lbu/f;->a:F

    :cond_0
    :goto_0
    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v0, v0, Lbu/f;->b:F

    iget v1, p0, Lbp/a;->e:I

    int-to-float v1, v1

    iget-object v2, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget-object v1, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iput v1, v0, Lbu/f;->b:F

    :cond_1
    :goto_1
    iget-object v0, p0, Lbp/a;->k:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    const v1, 0x3d4ccccd

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v0, v0, Lbu/f;->a:F

    iget-object v1, p0, Lbp/a;->b:Lbu/f;

    iget v1, v1, Lbu/f;->b:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lbp/a;->j:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    iget-object v1, p0, Lbp/a;->j:Lbu/b;

    invoke-virtual {v1}, Lbu/b;->b()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    double-to-float v0, v0

    iget-boolean v1, p0, Lbp/a;->n:Z

    if-eqz v1, :cond_2

    float-to-double v0, v0

    const-wide v2, 0x3feff7ced916872bL

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lbp/a;->p:Z

    if-nez v0, :cond_2

    const/16 v0, 0xc8

    iput v0, p0, Lbp/a;->o:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbp/a;->p:Z

    :cond_2
    iget v0, p0, Lbp/a;->o:I

    int-to-float v0, v0

    iget-object v1, p0, Lbp/a;->r:Lbp/e;

    invoke-virtual {v1}, Lbp/e;->a()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lbp/a;->o:I

    iget v0, p0, Lbp/a;->o:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_3

    iput v7, p0, Lbp/a;->o:I

    iput-boolean v7, p0, Lbp/a;->p:Z

    :cond_3
    iget-boolean v0, p0, Lbp/a;->p:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const v0, 0x3f666666

    iget-object v1, p0, Lbp/a;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    div-float/2addr v1, v4

    invoke-virtual {p1, v1, v5}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lbp/a;->c:Landroid/graphics/Paint;

    iget v1, p0, Lbp/a;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lbp/a;->h:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lbp/a;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_4
    iget-object v0, p0, Lbp/a;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lbp/a;->k:Lbu/b;

    invoke-virtual {v1}, Lbu/b;->b()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p1, v6, v6}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget-object v2, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, p0, Lbp/a;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v0, v0, Lbu/f;->a:F

    iget-object v1, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v1, p0, Lbp/a;->d:I

    iget-object v2, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Lbu/f;->a:F

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v0, v0, Lbu/f;->b:F

    iget-object v1, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, Lbp/a;->b:Lbu/f;

    iget v1, p0, Lbp/a;->e:I

    int-to-float v1, v1

    iget-object v2, p0, Lbp/a;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iput v1, v0, Lbu/f;->b:F

    goto/16 :goto_1
.end method

.method public a(Lbu/f;Lbp/c;Lbp/e;Z)V
    .locals 7

    const-wide/high16 v5, 0x3fe0000000000000L

    iget-object v0, p0, Lbp/a;->l:Lbu/f;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    add-double/2addr v1, v5

    iget v3, p1, Lbu/f;->a:F

    float-to-double v3, v3

    mul-double/2addr v1, v3

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    add-double/2addr v3, v5

    iget v5, p1, Lbu/f;->b:F

    float-to-double v5, v5

    mul-double/2addr v3, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lbu/f;->a(DD)V

    iput-boolean p4, p0, Lbp/a;->n:Z

    iput-object p2, p0, Lbp/a;->q:Lbp/c;

    iput-object p3, p0, Lbp/a;->r:Lbp/e;

    invoke-direct {p0}, Lbp/a;->c()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbp/a;->a:Z

    iget-object v0, p0, Lbp/a;->j:Lbu/b;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbp/a;->j:Lbu/b;

    const-wide/high16 v1, 0x4020000000000000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    const-wide/high16 v5, 0x4034000000000000L

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    iget-object v0, p0, Lbp/a;->k:Lbu/b;

    const/high16 v1, 0x437f0000

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbp/a;->k:Lbu/b;

    iget-object v1, p0, Lbp/a;->j:Lbu/b;

    invoke-virtual {v1}, Lbu/b;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    return-void
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbp/a;->a:Z

    iget-object v0, p0, Lbp/a;->j:Lbu/b;

    const/high16 v1, 0x40000000

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbp/a;->k:Lbu/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    return-void
.end method
