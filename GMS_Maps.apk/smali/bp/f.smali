.class public final enum Lbp/f;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbp/f;

.field public static final enum b:Lbp/f;

.field private static final synthetic e:[Lbp/f;


# instance fields
.field private final c:F

.field private final d:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lbp/f;

    const-string v1, "WIND_SLOW"

    const/high16 v2, 0x3f800000

    const/high16 v3, 0x40000000

    invoke-direct {v0, v1, v4, v2, v3}, Lbp/f;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lbp/f;->a:Lbp/f;

    new-instance v0, Lbp/f;

    const-string v1, "WIND_FAST"

    const/high16 v2, 0x40400000

    const/high16 v3, 0x40c00000

    invoke-direct {v0, v1, v5, v2, v3}, Lbp/f;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lbp/f;->b:Lbp/f;

    const/4 v0, 0x2

    new-array v0, v0, [Lbp/f;

    sget-object v1, Lbp/f;->a:Lbp/f;

    aput-object v1, v0, v4

    sget-object v1, Lbp/f;->b:Lbp/f;

    aput-object v1, v0, v5

    sput-object v0, Lbp/f;->e:[Lbp/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lbp/f;->c:F

    iput p4, p0, Lbp/f;->d:F

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbp/f;
    .locals 1

    const-class v0, Lbp/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbp/f;

    return-object v0
.end method

.method public static values()[Lbp/f;
    .locals 1

    sget-object v0, Lbp/f;->e:[Lbp/f;

    invoke-virtual {v0}, [Lbp/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbp/f;

    return-object v0
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lbp/f;->c:F

    return v0
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lbp/f;->d:F

    return v0
.end method
