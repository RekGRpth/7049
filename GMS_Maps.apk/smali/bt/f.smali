.class public final enum Lbt/f;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbt/f;

.field public static final enum b:Lbt/f;

.field private static final synthetic d:[Lbt/f;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lbt/f;

    const-string v1, "SNOW_LIGHT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v3, v2}, Lbt/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbt/f;->a:Lbt/f;

    new-instance v0, Lbt/f;

    const-string v1, "SNOW_HEAVY"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v4, v2}, Lbt/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbt/f;->b:Lbt/f;

    const/4 v0, 0x2

    new-array v0, v0, [Lbt/f;

    sget-object v1, Lbt/f;->a:Lbt/f;

    aput-object v1, v0, v3

    sget-object v1, Lbt/f;->b:Lbt/f;

    aput-object v1, v0, v4

    sput-object v0, Lbt/f;->d:[Lbt/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lbt/f;->c:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbt/f;
    .locals 1

    const-class v0, Lbt/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbt/f;

    return-object v0
.end method

.method public static values()[Lbt/f;
    .locals 1

    sget-object v0, Lbt/f;->d:[Lbt/f;

    invoke-virtual {v0}, [Lbt/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbt/f;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbt/f;->c:I

    return v0
.end method
