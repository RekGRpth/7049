.class public Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;
.super Landroid/support/v4/app/f;
.source "SourceFile"


# static fields
.field private static P:Lcom/google/commerce/wireless/topiary/D;

.field private static final ac:Lcom/google/commerce/wireless/topiary/T;

.field private static final ad:Lcom/google/commerce/wireless/topiary/T;

.field private static final ae:Lcom/google/commerce/wireless/topiary/T;

.field private static final af:Lcom/google/commerce/wireless/topiary/T;


# instance fields
.field protected N:Lcom/google/android/apps/common/offerslib/d;

.field protected O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

.field private final Q:Lcom/google/android/apps/common/offerslib/i;

.field private R:Lcom/google/commerce/wireless/topiary/HybridWebView;

.field private S:Lcom/google/android/apps/common/offerslib/c;

.field private T:Ljava/lang/String;

.field private U:Lcom/google/android/apps/common/offerslib/a;

.field private V:Landroid/accounts/Account;

.field private W:Lcom/google/android/apps/common/offerslib/v;

.field private X:Ljava/lang/String;

.field private Y:Landroid/widget/RelativeLayout;

.field private Z:Landroid/view/View;

.field private aa:Landroid/os/Handler;

.field private ab:I

.field private ag:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ac:Lcom/google/commerce/wireless/topiary/T;

    invoke-static {v2, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ad:Lcom/google/commerce/wireless/topiary/T;

    invoke-static {v1, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ae:Lcom/google/commerce/wireless/topiary/T;

    invoke-static {v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->af:Lcom/google/commerce/wireless/topiary/T;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/f;-><init>()V

    new-instance v0, Lcom/google/android/apps/common/offerslib/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/common/offerslib/i;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Lcom/google/android/apps/common/offerslib/f;)V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Q:Lcom/google/android/apps/common/offerslib/i;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->aa:Landroid/os/Handler;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->aa:Landroid/os/Handler;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/a;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ad:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->af:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ac:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ae:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_0
.end method

.method private static a(ZZ)Lcom/google/commerce/wireless/topiary/T;
    .locals 12

    const/4 v3, 0x1

    new-instance v0, Lcom/google/commerce/wireless/topiary/T;

    if-eqz p1, :cond_0

    const-string v1, "weblogin:service=sierrasandbox"

    :goto_0
    if-eqz p1, :cond_1

    const-string v2, "https://sandbox.google.com/checkout"

    :goto_1
    sget-object v5, Lcom/google/commerce/wireless/topiary/V;->b:Lcom/google/commerce/wireless/topiary/V;

    new-instance v6, Ljava/util/ArrayList;

    new-array v9, v3, [Lcom/google/commerce/wireless/topiary/U;

    const/4 v10, 0x0

    new-instance v11, Lcom/google/commerce/wireless/topiary/U;

    if-eqz p1, :cond_2

    const-string v4, "https://sandbox.google.com/checkout"

    move-object v8, v4

    :goto_2
    if-eqz p0, :cond_3

    const-string v4, "FSS"

    move-object v7, v4

    :goto_3
    if-eqz p0, :cond_4

    const/16 v4, 0x708

    :goto_4
    invoke-direct {v11, v8, v7, v4}, Lcom/google/commerce/wireless/topiary/U;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v11, v9, v10

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/commerce/wireless/topiary/T;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/commerce/wireless/topiary/V;Ljava/util/List;)V

    return-object v0

    :cond_0
    const-string v1, "Google Checkout"

    goto :goto_0

    :cond_1
    const-string v2, "https://checkout.google.com"

    goto :goto_1

    :cond_2
    const-string v4, "https://checkout.google.com"

    move-object v8, v4

    goto :goto_2

    :cond_3
    const-string v4, "SSID"

    move-object v7, v4

    goto :goto_3

    :cond_4
    const v4, 0x127500

    goto :goto_4
.end method

.method private a(ILjava/lang/String;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->c:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/apps/common/offerslib/d;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;ILjava/lang/String;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->b:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->a:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->c:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/common/offerslib/a;Landroid/accounts/Account;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->a()V

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    :cond_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "OfferDetailsFragment"

    const-string v2, "Clearing cookies and preloading offer details."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v1}, Lcom/google/commerce/wireless/topiary/D;->c()V

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-static {v1}, Lcom/google/android/apps/common/offerslib/z;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/common/offerslib/f;

    invoke-direct {v3}, Lcom/google/android/apps/common/offerslib/f;-><init>()V

    invoke-virtual {v2, v1, v0, p2, v3}, Lcom/google/commerce/wireless/topiary/D;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/E;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Z)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->clearHistory()V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->requestFocus(I)Z

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dispatchEvent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "dispatchEvent"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/common/offerslib/A;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v1, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/accounts/Account;Landroid/content/Context;)Z
    .locals 5

    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v0, "preferences_last_account_used_to_display_offer_details"

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "OfferDetailsFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Comparing current account :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to last used account :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    if-eqz p1, :cond_5

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "preferences_last_account_used_to_display_offer_details"

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->W:Lcom/google/android/apps/common/offerslib/v;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/common/offerslib/v;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_3
    return v1

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_5
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "preferences_last_account_used_to_display_offer_details"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;)Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ag:Ljava/util/HashMap;

    return-object v0
.end method

.method private c(Z)V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Z:Landroid/view/View;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Lcom/google/android/apps/common/offerslib/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Lcom/google/commerce/wireless/topiary/HybridWebView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Lcom/google/android/apps/common/offerslib/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Q:Lcom/google/android/apps/common/offerslib/i;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v()Lcom/google/commerce/wireless/topiary/D;
    .locals 1

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    return-object v0
.end method

.method private w()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/accounts/Account;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    :cond_3
    const-string v0, "OfferDetailsFragment"

    const-string v1, "Account not authenticated, clearing all cookies."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/D;->c()V

    goto :goto_0
.end method

.method private x()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Z:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/apps/common/offerslib/u;

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->s()Landroid/accounts/Account;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    new-instance v5, Lcom/google/android/apps/common/offerslib/g;

    invoke-direct {v5, p0}, Lcom/google/android/apps/common/offerslib/g;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/common/offerslib/u;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;Lcom/google/commerce/wireless/topiary/z;)V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0, v6}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, -0x2

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/widget/ProgressBar;

    invoke-direct {v2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0x11

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Z:Landroid/view/View;

    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    const/16 v0, 0x4d

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ag:Ljava/util/HashMap;

    const-string v1, "scan_qr"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "SCAN_RESULT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/accounts/Account;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->z()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OfferDetailsFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->w()V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.apps.offers.VIEW_OFFER_DETAILS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "processIntent ACTION_SHOW_OFFER"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    const-string v0, "offer_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    const-string v0, "offer_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "offer_id"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offer_namespace"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "offer_namespace"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->x()Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/common/offerslib/d;->i(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Landroid/location/Location;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "user_location"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-static {p1, v1, v0}, Lcom/google/android/apps/common/offerslib/z;->a(Landroid/content/Intent;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    return-void

    :cond_1
    const-string v2, "com.google.android.apps.offers.VIEW_MY_OFFERS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "processIntent ACTION_SHOW_MY_OFFERS"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/common/offerslib/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v2, "com.google.android.apps.offers.VIEW_CUSTOM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "processIntent ACTION_SHOW_CUSTOM_VIEW"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    const-string v0, "page_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "page_name"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lcom/google/android/apps/common/offerslib/z;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v2, "com.google.android.apps.offers.PROCESS_QR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processIntent ACTION_PROCESS_QR - got qr: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "SCAN_RESULT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    const-string v0, "SCAN_RESULT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "SCAN_RESULT"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lcom/google/android/apps/common/offerslib/z;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "processIntent action not recognized"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "action not recognized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/f;->c(Landroid/os/Bundle;)V

    if-eqz p1, :cond_3

    const-string v0, "param_app_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "param_app_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/offerslib/a;

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    :cond_0
    const-string v0, "param_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "param_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    :cond_1
    const-string v0, "param_requested_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "param_requested_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    :cond_2
    const-string v0, "param_offer_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "param_offer_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    :cond_3
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    :cond_4
    new-instance v0, Lcom/google/android/apps/common/offerslib/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/common/offerslib/v;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->W:Lcom/google/android/apps/common/offerslib/v;

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->z()V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/apps/common/offerslib/d;

    invoke-direct {v0}, Lcom/google/android/apps/common/offerslib/d;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/d;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/apps/common/offerslib/c;

    invoke-direct {v0}, Lcom/google/android/apps/common/offerslib/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    const-string v1, "param_internal_url_whitelist"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/c;->a(Ljava/lang/String;)V

    :cond_6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ag:Ljava/util/HashMap;

    if-eqz p1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Q:Lcom/google/android/apps/common/offerslib/i;

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/f;->d(Landroid/os/Bundle;)V

    const-string v0, "param_app_settings"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v0, "param_account"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "param_requested_url"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "param_internal_url_whitelist"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "param_offer_type"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    return-void
.end method

.method public f()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/f;->f()V

    return-void
.end method

.method public j()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->aa:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a()V

    iput-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-super {p0}, Landroid/support/v4/app/f;->j()V

    return-void
.end method

.method public r()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->d()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected s()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    return-object v0
.end method

.method t()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.zxing.client.android.SCAN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "SCAN_MODE"

    const-string v2, "QR_CODE_MODE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x4d

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method u()V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    if-ne v1, v0, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
