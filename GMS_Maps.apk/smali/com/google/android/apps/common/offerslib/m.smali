.class Lcom/google/android/apps/common/offerslib/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/common/offerslib/i;


# direct methods
.method constructor <init>(Lcom/google/android/apps/common/offerslib/i;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iput-object p2, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    const-string v1, "onEvent"

    # invokes: Lcom/google/android/apps/common/offerslib/i;->verifyFragment(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/common/offerslib/i;->access$300(Lcom/google/android/apps/common/offerslib/i;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "show_my_offers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "show_offer_list"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "navigate_back"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "refresh_my_offers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->d(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "is_location_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->e(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "is_nfc_capable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->f(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "is_device_nfc_capable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->g(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "request_enable_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->h(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "acquire_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-static {v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "acquire_location"

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    new-instance v2, Lcom/google/android/apps/common/offerslib/n;

    invoke-direct {v2, p0}, Lcom/google/android/apps/common/offerslib/n;-><init>(Lcom/google/android/apps/common/offerslib/m;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/offerslib/d;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Lcom/google/android/apps/common/offerslib/t;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "scan_qr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-static {v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "scan_qr"

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/d;->j(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "update_title"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/offerslib/d;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    const-string v1, "preview_rendered"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v1, v1, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v2, v2, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-static {v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/offerslib/d;->c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unrecognized event in onEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
