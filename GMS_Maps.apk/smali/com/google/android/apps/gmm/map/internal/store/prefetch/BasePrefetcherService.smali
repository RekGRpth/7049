.class public abstract Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static final a:J

.field private static d:I


# instance fields
.field protected b:Lr/I;

.field protected c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

.field private volatile e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field private final f:Landroid/os/IBinder;

.field private volatile g:J

.field private volatile h:J

.field private i:J

.field private j:Landroid/os/Looper;

.field private k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

.field private l:Lcom/google/googlenav/android/F;

.field private m:Landroid/net/wifi/WifiManager$WifiLock;

.field private n:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x20

    sput v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x14

    :goto_0
    sput-wide v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a:J

    return-void

    :cond_0
    const-wide/16 v0, 0xa

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/j;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->f:Landroid/os/IBinder;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    return-object p1
.end method

.method private a(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private a(ILjava/lang/Object;J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v1, v0, p3, p4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private a(J)V
    .locals 4

    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFECHED_FINISHED"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 10

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x3

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->c()Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v2}, Lr/I;->d()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-interface {p3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    :goto_1
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v7

    goto :goto_0

    :cond_3
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->b()V

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    if-ne v2, v4, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/e;->c()I

    move-result v2

    :goto_2
    new-array v4, v7, [Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "l="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->f()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "x="

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->e()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "m="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v1

    const-string v3, "s"

    invoke-virtual {p0, v1, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;ILcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;J)V

    invoke-direct {p0, v7, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_4
    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/e;->b()I

    move-result v2

    goto :goto_2

    :cond_5
    move v0, v3

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->p()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILjava/lang/Object;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Li/f;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Li/f;)V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;)V
    .locals 5

    const-string v0, ""

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/g;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "r="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x60

    const-string v2, "c"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v0, "u"

    goto :goto_0

    :pswitch_1
    const-string v0, "n"

    goto :goto_0

    :pswitch_2
    const-string v0, "o"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .locals 9

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v0}, Lr/I;->j()J

    move-result-wide v3

    const-wide/16 v5, 0x190

    cmp-long v0, v3, v5

    if-gez v0, :cond_1

    const-wide/16 v3, 0x3e8

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->q()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->e()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->y()V

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v2}, Lr/I;->d()Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b()I

    move-result v0

    if-lez v0, :cond_9

    :cond_6
    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    sget v4, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    if-ge v0, v4, :cond_9

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->c()Lo/aq;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lr/c;->c:Lr/c;

    :goto_4
    invoke-static {v0}, Lr/u;->a(Lr/c;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v0, v4}, Lr/I;->a(Lo/aq;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_7
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    sget-object v0, Lr/c;->e:Lr/c;

    goto :goto_4

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lr/c;->c:Lr/c;

    move-object v2, v0

    :goto_5
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v8, v0, v5, v2, v1}, Lr/I;->a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->a(Lr/v;)V

    goto :goto_6

    :cond_a
    sget-object v0, Lr/c;->e:Lr/c;

    move-object v2, v0

    goto :goto_5

    :cond_b
    const-string v0, "b"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    :cond_c
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    goto/16 :goto_2

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->r()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .locals 6

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v3

    if-ne v2, v3, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(J)V

    :cond_0
    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h:J

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->b()V

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->g()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v2

    const-string v3, "f"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbm/m;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    return-void
.end method

.method private b(Li/f;)V
    .locals 0

    invoke-interface {p1}, Li/f;->a()V

    return-void
.end method

.method private c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 7

    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/e;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/b;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    const/4 v0, 0x7

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    new-instance v6, Lcom/google/android/apps/gmm/map/internal/store/prefetch/c;

    invoke-direct {v6, p0, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/c;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->s()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    return-object v0
.end method

.method private d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;

    invoke-direct {v4, p0, p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/e;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/e;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    invoke-direct {v3, p1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Li/f;)V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->h()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->e()I

    move-result v2

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/p;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;I)V

    invoke-interface {v4, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    if-nez v2, :cond_0

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    sget-object v2, Lr/c;->c:Lr/c;

    invoke-virtual {v1, v3, v2, v0}, Lr/I;->a(Lo/ar;Lr/c;Ls/e;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->u()V

    return-void
.end method

.method public static l()I
    .locals 1

    sget v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    return v0
.end method

.method static synthetic m()I
    .locals 1

    sget v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    return v0
.end method

.method private n()Landroid/os/PowerManager$WakeLock;
    .locals 3

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "PrefetcherService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    return-object v0
.end method

.method private o()Landroid/net/wifi/WifiManager$WifiLock;
    .locals 3

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    const/4 v1, 0x1

    const-string v2, "PrefetcherService"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    goto :goto_0
.end method

.method private q()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    invoke-virtual {v1}, Lcom/google/googlenav/android/F;->a()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x60

    const-string v2, "n"

    const-string v3, "p"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r()V
    .locals 6

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->c()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    new-instance v2, Lcom/google/googlenav/prefetch/android/l;

    invoke-direct {v2, v1}, Lcom/google/googlenav/prefetch/android/l;-><init>(Ljava/util/List;)V

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method private s()V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->c()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    new-instance v2, Lcom/google/googlenav/prefetch/android/l;

    invoke-direct {v2, v1}, Lcom/google/googlenav/prefetch/android/l;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v2, v0, v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method private t()Z
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/e;->d()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    sub-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    return-void
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    :cond_2
    return-void
.end method

.method private w()J
    .locals 6

    const-wide/16 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFECHED_FINISHED"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method private x()J
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFETCH_NOT_START_CONDITION_LOG_TIME"

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private y()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->b()V

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 2

    const/4 v0, 0x5

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/16 v0, 0x60

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Li/f;)V
    .locals 1

    const/16 v0, 0x9

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    return-void
.end method

.method protected abstract b()V
.end method

.method public b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 2

    const/16 v0, 0x8

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    return-void
.end method

.method protected abstract c()V
.end method

.method protected abstract d()V
.end method

.method protected e()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/android/F;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/F;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/I;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-static {}, Lcom/google/googlenav/prefetch/android/h;->d()Lcom/google/googlenav/prefetch/android/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->w()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->x()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->o()Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a()V

    goto :goto_0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    return-void
.end method

.method public g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;
    .locals 6

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->g()J

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;

    iget-wide v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    iget-wide v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h:J

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;-><init>(JJLjava/lang/String;)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(J)V

    iput-wide v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    :cond_0
    return-void
.end method

.method public i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFETCH_NOT_START_CONDITION_LOG_TIME"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method

.method public k()Z
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->f:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PrefetcherService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onDestroy()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/i;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/i;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/b;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0
.end method
