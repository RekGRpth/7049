.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field final synthetic d:I

.field final synthetic e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field private f:I

.field private g:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/p;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;I)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->a:I

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    return-void
.end method


# virtual methods
.method public a(Lo/aq;ILo/ap;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    if-eqz p2, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    const/4 v0, 0x5

    :goto_1
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(II)V

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->i()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    sget-object v2, Lr/c;->c:Lr/c;

    invoke-virtual {v0, v1, v2, p0}, Lr/I;->a(Lo/ar;Lr/c;Ls/e;)V

    goto :goto_0
.end method
