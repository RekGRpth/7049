.class public final enum Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;


# instance fields
.field private final d:C


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v1, "AUTOMATIC"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v1, "FORCE"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v1, "OFFLINE_MAP"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->e:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IC)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-char p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->d:C

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;
    .locals 1

    const-class v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;
    .locals 1

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->e:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    return-object v0
.end method


# virtual methods
.method public a()C
    .locals 1

    iget-char v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->d:C

    return v0
.end method
