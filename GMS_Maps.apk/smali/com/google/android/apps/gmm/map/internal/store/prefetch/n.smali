.class final Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Li/f;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Li/f;)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    move-result-object v0

    const/4 v1, 0x6

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->stopSelf()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
