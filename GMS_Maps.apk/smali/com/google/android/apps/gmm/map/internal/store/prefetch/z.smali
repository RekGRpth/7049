.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    check-cast p2, Lcom/google/android/apps/gmm/map/internal/store/prefetch/j;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/j;->a()Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    return-void
.end method
