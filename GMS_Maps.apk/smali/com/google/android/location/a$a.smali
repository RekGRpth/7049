.class Lcom/google/android/location/a$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:[Z

.field private final c:[D

.field private final d:[D

.field private e:I


# direct methods
.method private constructor <init>()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/a$a;->a:[Ljava/lang/String;

    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/google/android/location/a$a;->b:[Z

    new-array v0, v3, [D

    iput-object v0, p0, Lcom/google/android/location/a$a;->c:[D

    new-array v0, v3, [D

    iput-object v0, p0, Lcom/google/android/location/a$a;->d:[D

    iput v1, p0, Lcom/google/android/location/a$a;->e:I

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/location/a$a;->b:[Z

    aput-boolean v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/a$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a$a;->b(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V

    return-void
.end method

.method private a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z
    .locals 11

    const/4 v8, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v8

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->a()Ljava/lang/String;

    move-result-object v10

    invoke-interface {p2}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v4

    invoke-interface {p2}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v6

    move v9, v8

    :goto_1
    const/4 v0, 0x5

    if-ge v9, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/a$a;->b:[Z

    aget-boolean v0, v0, v9

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a$a;->a:[Ljava/lang/String;

    aget-object v0, v0, v9

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a$a;->c:[D

    aget-wide v0, v0, v9

    iget-object v2, p0, Lcom/google/android/location/a$a;->d:[D

    aget-wide v2, v2, v9

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4049000000000000L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a$a;->b(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V

    move v0, v8

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V
    .locals 4

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a$a;->a:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/location/a$a;->e:I

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/google/android/location/a$a;->c:[D

    iget v1, p0, Lcom/google/android/location/a$a;->e:I

    invoke-interface {p2}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v2

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/location/a$a;->d:[D

    iget v1, p0, Lcom/google/android/location/a$a;->e:I

    invoke-interface {p2}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/location/a$a;->b:[Z

    iget v1, p0, Lcom/google/android/location/a$a;->e:I

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    iget v0, p0, Lcom/google/android/location/a$a;->e:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/google/android/location/a$a;->e:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a$a;->a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v0

    return v0
.end method
