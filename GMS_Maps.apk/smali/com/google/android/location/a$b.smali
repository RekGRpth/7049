.class Lcom/google/android/location/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private a:Lcom/google/android/location/os/g;

.field private b:J

.field private c:J

.field private final d:Lcom/google/android/location/b/g;

.field private e:Lcom/google/android/location/a$a;


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/g;)V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    iput-wide v2, p0, Lcom/google/android/location/a$b;->b:J

    iput-wide v2, p0, Lcom/google/android/location/a$b;->c:J

    new-instance v0, Lcom/google/android/location/a$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/a$a;-><init>(Lcom/google/android/location/a$1;)V

    iput-object v0, p0, Lcom/google/android/location/a$b;->e:Lcom/google/android/location/a$a;

    iput-object p1, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/b/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    return-object v0
.end method

.method a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V
    .locals 2

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/a$b;->c:J

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/a$b;->e:Lcom/google/android/location/a$a;

    invoke-static {v0, p2, p1}, Lcom/google/android/location/a$a;->a(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V

    :cond_1
    if-eqz p3, :cond_2

    iget-wide v0, p3, Lcom/google/android/location/e/E;->a:J

    iput-wide v0, p0, Lcom/google/android/location/a$b;->b:J

    iget-object v0, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p3}, Lcom/google/android/location/b/g;->c(Lcom/google/android/location/e/E;)V

    :cond_2
    return-void
.end method

.method a(Lcom/google/android/location/e/E;JJ)Z
    .locals 1

    invoke-virtual/range {p0 .. p5}, Lcom/google/android/location/a$b;->b(Lcom/google/android/location/e/E;JJ)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lcom/google/android/location/e/E;JJD)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/location/a$b;->b(Lcom/google/android/location/e/E;JJ)I

    move-result v1

    int-to-double v1, v1

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v1, v3

    cmpl-double v1, v1, p6

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a$b;->e:Lcom/google/android/location/a$a;

    invoke-static {v0, p1, p2}, Lcom/google/android/location/a$a;->b(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v0

    return v0
.end method

.method b(Lcom/google/android/location/e/E;JJ)I
    .locals 6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    invoke-virtual {v3, v2}, Lcom/google/android/location/b/g;->c(Ljava/lang/Long;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    sub-long v2, p2, v2

    cmp-long v2, v2, p4

    if-lez v2, :cond_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method b()Lcom/google/android/location/os/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    return-object v0
.end method

.method c()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v0

    goto :goto_0
.end method

.method d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a$b;->c:J

    return-wide v0
.end method

.method e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a$b;->b:J

    return-wide v0
.end method
