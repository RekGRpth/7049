.class public final enum Lcom/google/android/location/a$c;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/a$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/a$c;

.field public static final enum b:Lcom/google/android/location/a$c;

.field public static final enum c:Lcom/google/android/location/a$c;

.field public static final enum d:Lcom/google/android/location/a$c;

.field public static final enum e:Lcom/google/android/location/a$c;

.field public static final enum f:Lcom/google/android/location/a$c;

.field public static final enum g:Lcom/google/android/location/a$c;

.field public static final enum h:Lcom/google/android/location/a$c;

.field public static final enum i:Lcom/google/android/location/a$c;

.field private static final synthetic j:[Lcom/google/android/location/a$c;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "GPS_WAIT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "SCAN_WAIT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->d:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "POST_SCAN_GPS_WAIT"

    invoke-direct {v0, v1, v7}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->e:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "UPLOAD_WAIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "CALIBRATION_WAIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->g:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "SENSOR_COLLECTION_WAIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->h:Lcom/google/android/location/a$c;

    new-instance v0, Lcom/google/android/location/a$c;

    const-string v1, "IN_OUT_DOOR_COLLECTION_WAIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/a$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/a$c;->i:Lcom/google/android/location/a$c;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/location/a$c;->d:Lcom/google/android/location/a$c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/location/a$c;->e:Lcom/google/android/location/a$c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/location/a$c;->g:Lcom/google/android/location/a$c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/location/a$c;->h:Lcom/google/android/location/a$c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/location/a$c;->i:Lcom/google/android/location/a$c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/a$c;->j:[Lcom/google/android/location/a$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/a$c;
    .locals 1

    const-class v0, Lcom/google/android/location/a$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a$c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/a$c;
    .locals 1

    sget-object v0, Lcom/google/android/location/a$c;->j:[Lcom/google/android/location/a$c;

    invoke-virtual {v0}, [Lcom/google/android/location/a$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/a$c;

    return-object v0
.end method
