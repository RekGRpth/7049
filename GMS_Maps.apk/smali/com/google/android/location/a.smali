.class abstract Lcom/google/android/location/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a$1;,
        Lcom/google/android/location/a$a;,
        Lcom/google/android/location/a$b;,
        Lcom/google/android/location/a$c;
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/String;

.field final b:Lcom/google/android/location/os/i;

.field final c:Lcom/google/android/location/b/f;

.field final d:Lcom/google/android/location/y;

.field final e:Lcom/google/android/location/a$b;

.field protected f:Lcom/google/android/location/a$c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/a;->b:Lcom/google/android/location/os/i;

    iput-object p3, p0, Lcom/google/android/location/a;->c:Lcom/google/android/location/b/f;

    iput-object p4, p0, Lcom/google/android/location/a;->d:Lcom/google/android/location/y;

    iput-object p5, p0, Lcom/google/android/location/a;->e:Lcom/google/android/location/a$b;

    iput-object p6, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    iput-object p1, p0, Lcom/google/android/location/a;->a:Ljava/lang/String;

    return-void
.end method

.method protected static a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    const/4 v1, 0x1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    if-eqz p4, :cond_4

    :cond_0
    move v0, v1

    :goto_0
    const/4 v3, 0x3

    invoke-static {p2, p0, p1, p5, v0}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;JZZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {p2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v3

    add-long/2addr v3, p0

    invoke-interface {p2}, Lcom/google/android/location/os/g;->g()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const/16 v0, 0xe

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    if-eqz p3, :cond_2

    invoke-static {p3, p0, p1}, Lcom/google/android/location/e/f;->a(Lcom/google/android/location/e/e;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p4, p0, p1, v1}, Lcom/google/android/location/e/E;->a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->U:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v1, p6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x63

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v2

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/location/os/g;JZZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    const/4 v6, 0x1

    const-wide v4, 0x416312d000000000L

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->x:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-interface {p0}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-interface {p0}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->Q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x6

    invoke-interface {p0}, Lcom/google/android/location/os/g;->g()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x11

    invoke-virtual {v1, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x3

    invoke-interface {p0}, Lcom/google/android/location/os/g;->a()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {p0}, Lcom/google/android/location/os/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    invoke-interface {p0}, Lcom/google/android/location/os/g;->e()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-interface {p0}, Lcom/google/android/location/os/g;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xd

    invoke-interface {p0}, Lcom/google/android/location/os/g;->k()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    if-eqz p4, :cond_2

    invoke-interface {p0}, Lcom/google/android/location/os/g;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xa

    invoke-interface {p0}, Lcom/google/android/location/os/g;->m()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    return-object v1
.end method

.method protected static a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lcom/google/android/location/e/E;->a:J

    invoke-interface {p1}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    const-wide/32 v3, 0xafc80

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z
    .locals 8

    invoke-interface {p0}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v0

    invoke-interface {p0}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    int-to-double v2, p2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fault from instance of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 6

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    iget-object v2, p0, Lcom/google/android/location/a;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/location/a$1;->a:[I

    iget-object v5, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v5}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    if-eq v1, v2, :cond_1

    :cond_1
    if-nez v0, :cond_0

    return-void

    :pswitch_0
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->a(J)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->b(J)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->c(J)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->d(J)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->e(J)Z

    move-result v0

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->f(J)Z

    move-result v0

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->g(J)Z

    move-result v0

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->h(J)Z

    move-result v0

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->i(J)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method a(I)V
    .locals 0

    return-void
.end method

.method a(IIZ)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/os/g;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/os/h;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method a(Z)V
    .locals 0

    return-void
.end method

.method protected a(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(Lcom/google/android/location/e/E;)V
    .locals 0

    return-void
.end method

.method b(Z)V
    .locals 0

    return-void
.end method

.method b()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method c(Z)V
    .locals 0

    return-void
.end method

.method protected c(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected d(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected e(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected f(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected g(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected h(J)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected i(J)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
