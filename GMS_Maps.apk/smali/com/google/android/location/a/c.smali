.class Lcom/google/android/location/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/c$1;,
        Lcom/google/android/location/a/c$a;,
        Lcom/google/android/location/a/c$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/a/a;

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/a/k;

.field private final d:Lcom/google/android/location/a/d;

.field private final e:Lcom/google/android/location/a/c$a;

.field private f:Z

.field private g:Lcom/google/android/location/c/r;


# direct methods
.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/a/c$a;Lcom/google/android/location/a/k;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/a/a;

    invoke-direct {v0}, Lcom/google/android/location/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/c;->a:Lcom/google/android/location/a/a;

    new-instance v0, Lcom/google/android/location/a/d;

    invoke-direct {v0}, Lcom/google/android/location/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/c;->d:Lcom/google/android/location/a/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/a/c;->f:Z

    iput-object p1, p0, Lcom/google/android/location/a/c;->b:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/a/c;->e:Lcom/google/android/location/a/c$a;

    iput-object p3, p0, Lcom/google/android/location/a/c;->c:Lcom/google/android/location/a/k;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/c;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/a/c;->f:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/a/c;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/a/c;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/a/c;)Lcom/google/android/location/os/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/c;->b:Lcom/google/android/location/os/i;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/c;->c:Lcom/google/android/location/a/k;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/c$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/c;->e:Lcom/google/android/location/a/c$a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/c;->d:Lcom/google/android/location/a/d;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/c;->a:Lcom/google/android/location/a/a;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/a/c;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/a/c;->f:Z

    iget-object v0, p0, Lcom/google/android/location/a/c;->g:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    :cond_0
    return-void
.end method

.method a(I)V
    .locals 7

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/location/a/c;->f:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/location/a/c;->f:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/a/c;->b:Lcom/google/android/location/os/i;

    new-array v1, v3, [Lcom/google/android/location/c/F;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v4, v1, v3

    invoke-static {v1}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v1

    const-wide/16 v3, 0xe10

    new-instance v5, Lcom/google/android/location/a/c$b;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/google/android/location/a/c$b;-><init>(Lcom/google/android/location/a/c;Lcom/google/android/location/a/c$1;)V

    const-string v6, "SignalCollector"

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/a/c;->g:Lcom/google/android/location/c/r;

    iget-object v0, p0, Lcom/google/android/location/a/c;->g:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    goto :goto_0
.end method
