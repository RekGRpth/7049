.class Lcom/google/android/location/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:D

.field final b:D

.field final c:D

.field private final d:[D

.field private final e:[D

.field private final f:[D

.field private final g:[D

.field private final h:[D

.field private final i:[D

.field private final j:[D


# direct methods
.method constructor <init>(DDD[D[D[D[D[D[D)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/location/a/e;->a:D

    iput-wide p3, p0, Lcom/google/android/location/a/e;->b:D

    iput-wide p5, p0, Lcom/google/android/location/a/e;->c:D

    iput-object p7, p0, Lcom/google/android/location/a/e;->d:[D

    iput-object p8, p0, Lcom/google/android/location/a/e;->e:[D

    array-length v0, p8

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/location/a/e;->f:[D

    iget-object v0, p0, Lcom/google/android/location/a/e;->f:[D

    array-length v1, p8

    invoke-static {p8, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p9, p0, Lcom/google/android/location/a/e;->g:[D

    iput-object p10, p0, Lcom/google/android/location/a/e;->h:[D

    iput-object p11, p0, Lcom/google/android/location/a/e;->i:[D

    iput-object p12, p0, Lcom/google/android/location/a/e;->j:[D

    return-void
.end method

.method private a(D[D)D
    .locals 4

    array-length v1, p3

    int-to-double v2, v1

    mul-double/2addr v2, p1

    double-to-int v0, v2

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    aget-wide v0, p3, v0

    return-wide v0
.end method


# virtual methods
.method a(D)D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/e;->d:[D

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/a/e;->a(D[D)D

    move-result-wide v0

    return-wide v0
.end method

.method a(I)D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/e;->e:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method b(I)D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/e;->g:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method c(I)D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/e;->h:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method d(I)D
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/a/e;->i:[D

    aget-wide v0, v0, p1

    iget-object v2, p0, Lcom/google/android/location/a/e;->j:[D

    aget-wide v2, v2, p1

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActivityFeatures [accelStandardDeviationRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/e;->a:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accelMeanCrossingRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/e;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyStandardDeviationRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/e;->c:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->e:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyTangentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->g:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dominantFrequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->h:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", earlyMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->i:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lateMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->j:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
