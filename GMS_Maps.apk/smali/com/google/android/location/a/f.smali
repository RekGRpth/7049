.class public Lcom/google/android/location/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/android/location/a/f;->a:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/location/a/f;->b:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/google/android/location/e/c;Lcom/google/android/location/e/c;)Z
    .locals 3

    iget-object v0, p1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    iget-object v1, p2, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v0

    iget-object v1, p1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    div-int/lit16 v1, v1, 0x3e8

    const/16 v2, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/c;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/c;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/c;",
            ">;)Z"
        }
    .end annotation

    iget-wide v1, p1, Lcom/google/android/location/e/c;->e:J

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/c;

    iget-wide v3, v0, Lcom/google/android/location/e/c;->e:J

    sub-long v0, v1, v3

    const-wide/32 v2, 0xd6d8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/e/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/f;->a(Lcom/google/android/location/e/c;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/c;)Lcom/google/android/location/a/n$b;
    .locals 5

    const-wide/high16 v3, 0x3fe0000000000000L

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/location/a/f;->b(Lcom/google/android/location/e/c;)V

    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/c;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/f;->a(Lcom/google/android/location/e/c;Lcom/google/android/location/e/c;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/location/a/f;->b(Lcom/google/android/location/e/c;)V

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/e/B;->b:Lcom/google/android/location/e/B;

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/e/B;->a:Lcom/google/android/location/e/B;

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method
