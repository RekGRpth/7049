.class Lcom/google/android/location/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/a/h;->a:Lcom/google/android/location/b/i;

    return-void
.end method

.method private a(Lcom/google/android/location/a/n$a;Z)Lcom/google/android/location/e/w;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/location/a/n$a;->b()Lcom/google/android/location/e/t;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, v1, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    :goto_1
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    goto :goto_0

    :cond_2
    iget-object v1, v1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    goto :goto_1
.end method

.method private a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;
    .locals 2

    invoke-direct {p0, p1, p3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Z)Lcom/google/android/location/e/w;

    move-result-object v0

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Z)Lcom/google/android/location/e/w;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v1

    if-eqz p3, :cond_2

    const/16 v0, 0x1388

    :goto_1
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x3e8

    goto :goto_1
.end method

.method private a(Ljava/util/Set;)Ljava/lang/Double;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Double;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/a/h;->a:Lcom/google/android/location/b/i;

    invoke-virtual {v3, v0}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;)Lcom/google/android/location/b/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget v0, v0, Lcom/google/android/location/e/C;->c:I

    div-int/lit16 v0, v0, 0x3e8

    if-eqz v1, :cond_0

    int-to-double v3, v0

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_2

    :cond_0
    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Double;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    int-to-double v1, v1

    int-to-double v3, v0

    div-double v0, v1, v3

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/E;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/E;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method private a(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v2, Lcom/google/android/location/a/m$a;->e:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    sget-object v2, Lcom/google/android/location/a/m$a;->f:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    sget-object v2, Lcom/google/android/location/a/m$a;->g:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    sget-object v2, Lcom/google/android/location/a/m$a;->h:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    return-void
.end method

.method private a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private b(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/e/E;)Ljava/util/Set;

    move-result-object v1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/e/E;)Ljava/util/Set;

    move-result-object v2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/e/E;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/a/m$a;->a:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {p0, p2, v4, v1}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    sget-object v1, Lcom/google/android/location/a/m$a;->b:Lcom/google/android/location/a/m$a;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-interface {p2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/location/a/m$a;->c:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, v3}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v3

    invoke-direct {p0, p2, v1, v3}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    invoke-direct {p0, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/a/m$a;->d:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v3, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t compute features for history of size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/h;->b(Ljava/util/List;Ljava/util/Map;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method
