.class Lcom/google/android/location/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a([D[D)D
    .locals 7

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    aget-wide v3, p1, v0

    aget-wide v5, p2, v0

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method a([D)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([D)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    array-length v2, p1

    mul-int/lit8 v3, v2, 0x2

    new-array v4, v3, [D

    invoke-virtual {p0, v2}, Lcom/google/android/location/a/i;->a(I)[D

    move-result-object v5

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-wide v6, p1, v0

    aget-wide v8, v5, v0

    mul-double/2addr v6, v8

    aput-wide v6, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_1

    const-wide/16 v5, 0x0

    aput-wide v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    array-length v0, v4

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v6, v4

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_2

    aget-wide v7, v4, v0

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    new-instance v0, Lcom/google/android/location/a/g;

    invoke-direct {v0}, Lcom/google/android/location/a/g;-><init>()V

    invoke-virtual {v0, v5}, Lcom/google/android/location/a/g;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const-wide/high16 v6, 0x3ff0000000000000L

    mul-int v0, v3, v3

    int-to-double v8, v0

    div-double/2addr v6, v8

    new-instance v3, Ljava/lang/Double;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    iget-wide v8, v0, Lcom/google/android/location/a/g$a;->a:D

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    iget-wide v0, v0, Lcom/google/android/location/a/g$a;->a:D

    mul-double/2addr v0, v8

    invoke-direct {v3, v0, v1}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_3

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    new-instance v3, Ljava/lang/Double;

    iget-wide v8, v0, Lcom/google/android/location/a/g$a;->a:D

    iget-wide v10, v0, Lcom/google/android/location/a/g$a;->a:D

    mul-double/2addr v8, v10

    iget-wide v10, v0, Lcom/google/android/location/a/g$a;->b:D

    iget-wide v12, v0, Lcom/google/android/location/a/g$a;->b:D

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    invoke-direct {v3, v8, v9}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    return-object v5
.end method

.method a(I)[D
    .locals 12

    const-wide/high16 v10, 0x4010000000000000L

    const-wide/high16 v8, 0x4000000000000000L

    new-array v1, p1, [D

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    int-to-double v2, v0

    add-int/lit8 v4, p1, -0x1

    int-to-double v4, v4

    div-double/2addr v4, v8

    sub-double/2addr v2, v4

    neg-double v2, v2

    int-to-double v4, v0

    add-int/lit8 v6, p1, -0x1

    int-to-double v6, v6

    div-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-int/lit8 v4, p1, -0x1

    int-to-double v4, v4

    div-double/2addr v4, v10

    add-int/lit8 v6, p1, -0x1

    int-to-double v6, v6

    div-double/2addr v6, v10

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method a([[DLjava/util/List;JIZ)[D
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[D",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;JIZ)[D"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/a/i;->a([[DLjava/util/List;IJ)[[D

    move-result-object v3

    new-array v4, p5, [D

    new-array v5, p5, [D

    const/4 v0, 0x3

    new-array v1, v0, [D

    fill-array-data v1, :array_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p5, :cond_1

    aget-object v2, v3, v0

    invoke-virtual {p0, v2, v2}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    aput-wide v6, v4, v0

    if-eqz p6, :cond_0

    if-lez v0, :cond_0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/location/a/i;->b([D[D)[D

    move-result-object v1

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v1, v1}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    aput-wide v7, v5, v6

    :cond_0
    add-int/lit8 v0, v0, 0x1

    move-object v1, v2

    goto :goto_0

    :cond_1
    if-eqz p6, :cond_2

    mul-int/lit8 v0, p5, 0x2

    add-int/lit8 v0, v0, -0x1

    :goto_1
    new-array v2, v0, [D

    invoke-virtual {p0, v4}, Lcom/google/android/location/a/i;->a([D)Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v0, p5

    goto :goto_1

    :cond_3
    if-eqz p6, :cond_4

    invoke-virtual {p0, v5}, Lcom/google/android/location/a/i;->a([D)Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_4

    add-int v4, p5, v1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    aput-wide v5, v2, v4

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    return-object v2

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
    .end array-data
.end method

.method a([[DLjava/util/List;IJ)[[D
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[D",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;IJ)[[D"
        }
    .end annotation

    const/4 v2, 0x0

    aget-object v2, p1, v2

    array-length v2, v2

    move/from16 v0, p3

    filled-new-array {v0, v2}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    move-wide/from16 v0, p4

    long-to-double v3, v0

    move/from16 v0, p3

    int-to-double v5, v0

    div-double v6, v3, v5

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v8, v3, [D

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    array-length v3, v8

    if-ge v4, v3, :cond_0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-double v9, v9

    aput-wide v9, v8, v4

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v3, 0x0

    move v5, v3

    move v3, v4

    :goto_1
    move/from16 v0, p3

    if-ge v5, v0, :cond_7

    const/4 v4, 0x0

    aget-wide v9, v8, v4

    int-to-double v11, v5

    mul-double/2addr v11, v6

    add-double/2addr v9, v11

    :goto_2
    aget-wide v11, v8, v3

    const-wide v13, 0x3eb0c6f7a0b5ed8dL

    add-double/2addr v11, v13

    cmpg-double v4, v11, v9

    if-gez v4, :cond_1

    array-length v4, v8

    if-ge v3, v4, :cond_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    array-length v4, v8

    if-ne v3, v4, :cond_3

    array-length v4, v8

    add-int/lit8 v4, v4, -0x1

    aget-object v4, p1, v4

    aput-object v4, v2, v5

    :cond_2
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    :cond_3
    aget-wide v11, v8, v3

    sub-double v11, v9, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->abs(D)D

    move-result-wide v11

    const-wide v13, 0x3ec0c6f7a0b5ed8dL

    cmpg-double v4, v11, v13

    if-ltz v4, :cond_4

    if-nez v3, :cond_5

    :cond_4
    aget-object v4, p1, v3

    aput-object v4, v2, v5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v3, -0x1

    aget-wide v11, v8, v4

    sub-double v11, v9, v11

    aget-wide v13, v8, v3

    sub-double v9, v13, v9

    add-double v13, v11, v9

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v13

    const-wide v15, 0x3eb0c6f7a0b5ed8dL

    cmpg-double v4, v13, v15

    if-gez v4, :cond_6

    aget-object v4, p1, v3

    aput-object v4, v2, v5

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    :goto_4
    aget-object v13, p1, v3

    array-length v13, v13

    if-ge v4, v13, :cond_2

    aget-object v13, v2, v5

    add-int/lit8 v14, v3, -0x1

    aget-object v14, p1, v14

    aget-wide v14, v14, v4

    mul-double/2addr v14, v9

    aget-object v16, p1, v3

    aget-wide v16, v16, v4

    mul-double v16, v16, v11

    add-double v14, v14, v16

    add-double v16, v11, v9

    div-double v14, v14, v16

    aput-wide v14, v13, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_7
    return-object v2
.end method

.method b([D[D)[D
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p0, p2, p2}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v1

    array-length v3, p1

    new-array v3, v3, [D

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3eb0c6f7a0b5ed8dL

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-wide v1, p1, v0

    aput-wide v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v4

    div-double v1, v4, v1

    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_1

    aget-wide v4, p1, v0

    aget-wide v6, p2, v0

    mul-double/2addr v6, v1

    sub-double/2addr v4, v6

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v3
.end method
