.class public Lcom/google/android/location/a/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/n$a;,
        Lcom/google/android/location/a/n$b;
    }
.end annotation


# static fields
.field static final a:Lcom/google/android/location/a/n$b;


# instance fields
.field private final b:Lcom/google/android/location/a/m;

.field private final c:Lcom/google/android/location/a/f;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/location/a/h;

.field private final f:Lcom/google/android/location/os/c;

.field private g:Lcom/google/android/location/a/n$b;

.field private h:J

.field private i:Z

.field private j:Lcom/google/android/location/e/B;

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/e/B;->c:Lcom/google/android/location/e/B;

    const-wide/high16 v2, -0x4010000000000000L

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    sput-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/os/c;",
            ")V"
        }
    .end annotation

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/a/m;

    invoke-direct {v0}, Lcom/google/android/location/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/n;->b:Lcom/google/android/location/a/m;

    new-instance v0, Lcom/google/android/location/a/f;

    invoke-direct {v0}, Lcom/google/android/location/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/n;->c:Lcom/google/android/location/a/f;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    iput-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    iput-wide v1, p0, Lcom/google/android/location/a/n;->h:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/a/n;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    iput-wide v1, p0, Lcom/google/android/location/a/n;->k:J

    iput-object p2, p0, Lcom/google/android/location/a/n;->f:Lcom/google/android/location/os/c;

    new-instance v0, Lcom/google/android/location/a/h;

    invoke-direct {v0, p1}, Lcom/google/android/location/a/h;-><init>(Lcom/google/android/location/b/i;)V

    iput-object v0, p0, Lcom/google/android/location/a/n;->e:Lcom/google/android/location/a/h;

    return-void
.end method

.method private a(Lcom/google/android/location/a/n$a;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/a/n$a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method private a()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/a/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/a/n$a;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/a/n$a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    iget-wide v1, v0, Lcom/google/android/location/e/E;->a:J

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    iget-wide v3, v0, Lcom/google/android/location/e/E;->a:J

    sub-long v0, v1, v3

    const-wide/32 v2, 0xd6d8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;
    .locals 3

    if-nez p2, :cond_0

    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-gtz v0, :cond_1

    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/location/a/n$a;

    invoke-direct {v0, p2, p1}, Lcom/google/android/location/a/n$a;-><init>(Lcom/google/android/location/e/E;Lcom/google/android/location/e/t;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/a/n;->b(Lcom/google/android/location/a/n$a;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/a/n;->a(Lcom/google/android/location/a/n$a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/a/n;->e:Lcom/google/android/location/a/h;

    invoke-virtual {v1, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/n;->b:Lcom/google/android/location/a/m;

    invoke-virtual {v1, v0}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/a/n$a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/n;->a(Lcom/google/android/location/a/n$a;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private b()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/a/n;->f:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/a/n;->h:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x57e40

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;
    .locals 8

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a/n;->b(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/a/n;->c:Lcom/google/android/location/a/f;

    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/location/a/f;->a(Lcom/google/android/location/e/c;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    sget-object v2, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-ne v1, v2, :cond_3

    sget-object v2, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v2, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/google/android/location/a/n;->f:Lcom/google/android/location/os/c;

    invoke-interface {v2}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-ne v0, v4, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/a/n;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    :goto_2
    iget-object v1, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v4

    if-eq v1, v4, :cond_0

    iget-wide v4, p0, Lcom/google/android/location/a/n;->k:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0xc350

    cmp-long v1, v4, v6

    if-lez v1, :cond_7

    iput-wide v2, p0, Lcom/google/android/location/a/n;->k:J

    :cond_0
    :goto_3
    sget-object v1, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    :cond_1
    return-object v0

    :cond_2
    iget-object v0, p1, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    sget-object v4, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-ne v1, v4, :cond_5

    iget-boolean v4, p0, Lcom/google/android/location/a/n;->i:Z

    if-eqz v4, :cond_5

    invoke-direct {p0}, Lcom/google/android/location/a/n;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    goto :goto_2

    :cond_5
    if-ne v0, v1, :cond_6

    const/4 v1, 0x1

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/location/a/n;->i:Z

    iput-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    iput-wide v2, p0, Lcom/google/android/location/a/n;->h:J

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_3
.end method
