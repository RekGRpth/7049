.class public abstract Lcom/google/android/location/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b$a;
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Lcom/google/android/location/os/i;

.field protected final c:Ljava/util/Random;

.field d:J

.field e:J

.field private final f:Lcom/google/android/location/u;

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/os/i;ILcom/google/android/location/u;Ljava/util/Random;)V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/location/b;->d:J

    iput-wide v0, p0, Lcom/google/android/location/b;->e:J

    iput-object p1, p0, Lcom/google/android/location/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    iput p3, p0, Lcom/google/android/location/b;->g:I

    iput-object p4, p0, Lcom/google/android/location/b;->f:Lcom/google/android/location/u;

    iput-object p5, p0, Lcom/google/android/location/b;->c:Ljava/util/Random;

    return-void
.end method

.method private b(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    const-wide/16 v10, 0x0

    invoke-virtual {p0}, Lcom/google/android/location/b;->b()Lcom/google/android/location/e/u;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/b$a;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x493e0

    sub-long/2addr v4, v6

    cmp-long v1, v2, v10

    if-eqz v1, :cond_1

    const-wide/32 v6, 0x5265c00

    sub-long v6, v2, v6

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gez v1, :cond_1

    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    iget-wide v4, p0, Lcom/google/android/location/b;->d:J

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    :cond_0
    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/location/b;->a(JLcom/google/android/location/b$a;)Lcom/google/android/location/e/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/u;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b;->f:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-direct {p0, v2}, Lcom/google/android/location/b;->b(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/location/b;->a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(JLcom/google/android/location/b$a;)Lcom/google/android/location/e/u;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/location/b$a;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    iget-wide v0, p0, Lcom/google/android/location/b;->d:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-object v2, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    iget v3, p0, Lcom/google/android/location/b;->g:I

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/location/os/i;->a(IJ)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/location/b;->b(JLcom/google/android/location/b$a;)V

    iput-wide p1, p0, Lcom/google/android/location/b;->d:J

    iput-wide v0, p0, Lcom/google/android/location/b;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/b;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract b()Lcom/google/android/location/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract b(JLcom/google/android/location/b$a;)V
.end method
