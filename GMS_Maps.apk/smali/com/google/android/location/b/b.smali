.class public Lcom/google/android/location/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/b$1;,
        Lcom/google/android/location/b/b$a;
    }
.end annotation


# static fields
.field static final a:J

.field static final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# instance fields
.field final c:Lcom/google/android/location/os/i;

.field final d:Lcom/google/android/location/b/f;

.field final e:Lcom/google/android/location/b/g;

.field final f:Lcom/google/android/location/os/h;

.field final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field j:I

.field k:I

.field l:Z

.field m:Lcom/google/android/location/b/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-wide/32 v2, 0x2932e00

    const/4 v4, -0x1

    const-wide/32 v0, 0x5265c00

    invoke-static {v2, v3, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/b/b;->a:J

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/os/h;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    iput v1, p0, Lcom/google/android/location/b/b;->i:I

    iput v1, p0, Lcom/google/android/location/b/b;->j:I

    iput v1, p0, Lcom/google/android/location/b/b;->k:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    sget-object v0, Lcom/google/android/location/b/b$a;->a:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    iput-object p1, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    iput-object p3, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    iput-object p4, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    return-void
.end method

.method private a(JI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ac:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-lez p3, :cond_0

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-object v0
.end method

.method private a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/b/f;->a(JZ)V

    iget-object v2, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/b/g;->a(JZ)V

    return-void
.end method

.method private c()V
    .locals 1

    sget-object v0, Lcom/google/android/location/b/b$a;->a:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/b/b;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->k()V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->d()V

    return-void
.end method

.method private d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/b/b;->f()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/b/b;->g()V

    goto :goto_0
.end method

.method private f()V
    .locals 6

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/i;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->h()V

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    sget-object v0, Lcom/google/android/location/b/b$a;->b:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    return-void
.end method

.method private g()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v0}, Lcom/google/android/location/b/f;->d()Lcom/google/android/location/b/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v1}, Lcom/google/android/location/b/f;->c()Lcom/google/android/location/b/i;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/b/b;->a(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;)V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/b/b;->h()V

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/location/b/b;->j:I

    int-to-long v4, v4

    const-wide/16 v6, 0x2710

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    sget-object v0, Lcom/google/android/location/b/b$a;->c:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    :cond_0
    return-void
.end method

.method private i()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/b/b;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/b/b;->j()V

    :cond_0
    return v0
.end method

.method private j()V
    .locals 2

    iget v0, p0, Lcom/google/android/location/b/b;->j:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/location/b/b;->k:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/location/b/b;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3f333333

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/location/b/b;->j:I

    if-nez v1, :cond_1

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/location/b/b;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->k()V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->d()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 4

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput v1, p0, Lcom/google/android/location/b/b;->i:I

    iput v1, p0, Lcom/google/android/location/b/b;->j:I

    iput v1, p0, Lcom/google/android/location/b/b;->k:I

    sget-object v0, Lcom/google/android/location/b/b$a;->a:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    invoke-virtual {p0}, Lcom/google/android/location/b/b;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v3, 0x4

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/location/os/i;->a(IJ)V

    return-void
.end method

.method private l()Z
    .locals 11

    const/4 v4, 0x0

    const/4 v10, 0x4

    const/4 v2, 0x0

    const/4 v9, 0x2

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->an:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sget-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v9, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/location/b/b;->i:I

    if-ge v1, v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v1

    move-object v1, v3

    :goto_1
    iget v3, p0, Lcom/google/android/location/b/b;->i:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->L:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v3, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/location/b/b;->i:I

    if-ge v3, v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move v3, v0

    :cond_2
    if-lez v3, :cond_3

    move v2, v5

    :cond_3
    if-eqz v2, :cond_6

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz v1, :cond_4

    invoke-virtual {v3, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v3, v9, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    invoke-virtual {v0, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/i;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_6
    return v2

    :cond_7
    move-object v1, v4

    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method a(JZ)J
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0000000000000L

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L

    mul-double/2addr v0, v2

    const-wide v2, 0x41224f8000000000L

    mul-double/2addr v0, v2

    double-to-long v2, v0

    if-eqz p3, :cond_0

    const-wide/32 v0, 0x5265c00

    :goto_0
    add-long/2addr v0, p1

    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    sget-wide v0, Lcom/google/android/location/b/b;->a:J

    goto :goto_0
.end method

.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/b/b;->k()V

    return-void
.end method

.method public a(I)V
    .locals 2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/location/b/b$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    invoke-virtual {v1}, Lcom/google/android/location/b/b$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/b/b;->e()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/location/b/b;->c()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/location/b/b;->j()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput v3, p0, Lcom/google/android/location/b/b;->k:I

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->d()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/location/b/i;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/a;

    invoke-virtual {v1}, Lcom/google/android/location/b/a;->c()I

    move-result v1

    if-ge v1, v2, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/e/e;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/b/a;->b(J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->e()I

    move-result v6

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/google/android/location/b/i;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/a;

    invoke-virtual {v1}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/C;

    invoke-virtual {v1}, Lcom/google/android/location/b/a;->c()I

    move-result v1

    if-lt v1, v6, :cond_3

    invoke-virtual {v2}, Lcom/google/android/location/e/C;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2}, Lcom/google/android/location/e/C;->d()I

    move-result v2

    iget-object v9, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/b/b;->a(JI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/b/a;->b(J)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v0}, Lcom/google/android/location/b/g;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/g$b;

    invoke-virtual {v1}, Lcom/google/android/location/b/g$b;->b()I

    move-result v1

    if-ge v1, v6, :cond_7

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    iget-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-direct {p0, v8, v9, v3}, Lcom/google/android/location/b/b;->a(JI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_3

    :cond_8
    if-nez v1, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/b/g$b;->a(J)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/b/b;->i:I

    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    int-to-double v1, v0

    const-wide/high16 v3, 0x3ff0000000000000L

    mul-double/2addr v1, v3

    iget v3, p0, Lcom/google/android/location/b/b;->i:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/google/android/location/b/b;->j:I

    if-lez v0, :cond_a

    :cond_a
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    sget-object v1, Lcom/google/android/location/b/b$a;->b:Lcom/google/android/location/b/b$a;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/b/b;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/b/b;->c()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    sget-object v1, Lcom/google/android/location/b/b$a;->c:Lcom/google/android/location/b/b$a;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/location/b/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V

    iget-object v0, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/location/b/b;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/b/b;->k:I

    :cond_3
    invoke-direct {p0}, Lcom/google/android/location/b/b;->i()Z

    goto :goto_0
.end method

.method b()J
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v0}, Lcom/google/android/location/b/f;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v2}, Lcom/google/android/location/b/f;->b()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/b/b;->a(JZ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v2}, Lcom/google/android/location/b/g;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v4}, Lcom/google/android/location/b/g;->e()Z

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/location/b/b;->a(JZ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method
