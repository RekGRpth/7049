.class Lcom/google/android/location/b/c$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/b/c;->a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/e/A;

.field final synthetic b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field final synthetic c:Ljava/lang/Object;

.field final synthetic d:Lcom/google/android/location/b/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/b/c;Lcom/google/android/location/e/A;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    iput-object p2, p0, Lcom/google/android/location/b/c$2;->a:Lcom/google/android/location/e/A;

    iput-object p3, p0, Lcom/google/android/location/b/c$2;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p4, p0, Lcom/google/android/location/b/c$2;->c:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Lcom/google/android/location/b/c;)Ljava/io/File;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/b/c$2;->a:Lcom/google/android/location/e/A;

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    iget-object v0, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->e(Lcom/google/android/location/b/c;)Lcom/google/android/location/os/f;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v4}, Lcom/google/android/location/b/c;->c(Lcom/google/android/location/b/c;)[B

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v5}, Lcom/google/android/location/b/c;->d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/e/x;->a()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v5

    invoke-static {}, Lcom/google/common/base/Predicates;->alwaysTrue()Lcom/google/common/base/K;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v8}, Lcom/google/android/location/b/c;->e(Lcom/google/android/location/b/c;)Lcom/google/android/location/os/f;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    iget-object v1, p0, Lcom/google/android/location/b/c$2;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$2;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$2;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/c$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    monitor-enter p0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$2;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/b/c$2;->d:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$2;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/c$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
