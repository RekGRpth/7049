.class public Lcom/google/android/location/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/b/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/d",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/google/android/location/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/c",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/l;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/location/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/c",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/location/b/d;Lcom/google/android/location/b/c;Lcom/google/android/location/b/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/d",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/location/b/c",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/l;",
            ">;",
            "Lcom/google/android/location/b/c",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/j;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    iput-object p2, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    iput-object p3, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    return-void
.end method

.method public static a(Lcom/google/android/location/os/i;)Lcom/google/android/location/b/e;
    .locals 3

    invoke-interface {p0}, Lcom/google/android/location/os/i;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/location/os/i;->d()Ljava/io/File;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/google/android/location/b/e;->a(Ljava/util/concurrent/ExecutorService;Ljava/io/File;[BLcom/google/android/location/os/f;)Lcom/google/android/location/b/e;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;Ljava/io/File;[BLcom/google/android/location/os/f;)Lcom/google/android/location/b/e;
    .locals 11

    new-instance v3, Ljava/io/File;

    const-string v0, "macs"

    invoke-direct {v3, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/location/b/d;

    const/16 v1, 0x190

    sget-object v2, Lcom/google/android/location/b/k;->a:Lcom/google/android/location/b/k$b;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/b/d;-><init>(ILcom/google/android/location/b/k$b;Ljava/io/File;[BLcom/google/android/location/os/f;)V

    new-instance v7, Ljava/io/File;

    const-string v1, "selectors"

    invoke-direct {v7, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    invoke-interface {p3, v7}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v1, Lcom/google/android/location/b/c;

    const/4 v2, 0x2

    const/16 v3, 0x14

    new-instance v5, Lcom/google/android/location/e/m;

    invoke-direct {v5}, Lcom/google/android/location/e/m;-><init>()V

    sget-object v6, Lcom/google/android/location/b/k;->b:Lcom/google/android/location/b/k$b;

    move-object v4, p0

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/b/c;-><init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/e/x;Lcom/google/android/location/b/k$b;Ljava/io/File;[BLcom/google/android/location/os/f;)V

    new-instance v8, Ljava/io/File;

    const-string v2, "models"

    invoke-direct {v8, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    invoke-interface {p3, v8}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v2, Lcom/google/android/location/b/c;

    const/4 v3, 0x4

    const/16 v4, 0xa

    new-instance v6, Lcom/google/android/location/e/k;

    invoke-direct {v6}, Lcom/google/android/location/e/k;-><init>()V

    sget-object v7, Lcom/google/android/location/b/k;->b:Lcom/google/android/location/b/k$b;

    move-object v5, p0

    move-object v9, p2

    move-object v10, p3

    invoke-direct/range {v2 .. v10}, Lcom/google/android/location/b/c;-><init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/e/x;Lcom/google/android/location/b/k$b;Ljava/io/File;[BLcom/google/android/location/os/f;)V

    new-instance v3, Lcom/google/android/location/b/e;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/location/b/e;-><init>(Lcom/google/android/location/b/d;Lcom/google/android/location/b/c;Lcom/google/android/location/b/c;)V

    return-object v3
.end method

.method private a(Lcom/google/android/location/e/s;J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {p1}, Lcom/google/android/location/e/s;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2, p3}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    return-void
.end method

.method private a(Lcom/google/android/location/e/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 3

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {p1}, Lcom/google/android/location/e/s;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p3, p4}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    :cond_0
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x6

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    const/16 v3, 0x17

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/e/s;J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {p1}, Lcom/google/android/location/e/s;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2, p3}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    return-void
.end method

.method private b(Lcom/google/android/location/e/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 3

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {p1}, Lcom/google/android/location/e/s;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p3, p4}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    :cond_0
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 4

    invoke-static {p1}, Lcom/google/android/location/e/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v2, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0, p2, p3}, Lcom/google/android/location/b/d;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;J)Lcom/google/android/location/e/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/l;

    return-object v0
.end method

.method public a(JJ)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Lcom/google/android/location/b/d;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    invoke-virtual {v0}, Lcom/google/android/location/b/d;->a()V

    iget-object v0, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->a()V

    iget-object v0, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->a()V

    return-void
.end method

.method public a(J)V
    .locals 5

    const-wide/32 v0, 0x240c8400

    sub-long v0, p1, v0

    const-wide/32 v2, 0xa4cb800

    sub-long v2, p1, v2

    iget-object v4, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    invoke-virtual {v4, v2, v3, v2, v3}, Lcom/google/android/location/b/d;->a(JJ)V

    iget-object v2, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {v2, v0, v1, v0, v1}, Lcom/google/android/location/b/c;->a(JJ)V

    iget-object v2, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {v2, v0, v1, v0, v1}, Lcom/google/android/location/b/c;->a(JJ)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/location/b/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/location/b/e;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JLcom/google/android/location/e/s;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/location/b/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/16 v4, 0x17

    if-ne v3, v4, :cond_3

    invoke-virtual {p4}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/e/s$a;->a:Lcom/google/android/location/e/s$a;

    if-ne v2, v3, :cond_2

    invoke-direct {p0, p4, p2, p3}, Lcom/google/android/location/b/e;->b(Lcom/google/android/location/e/s;J)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/e/s$a;->b:Lcom/google/android/location/e/s$a;

    if-ne v2, v3, :cond_0

    invoke-direct {p0, p4, p2, p3}, Lcom/google/android/location/b/e;->a(Lcom/google/android/location/e/s;J)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {p4}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/e/s$a;->a:Lcom/google/android/location/e/s$a;

    if-ne v3, v4, :cond_4

    invoke-direct {p0, p4, v2, p2, p3}, Lcom/google/android/location/b/e;->b(Lcom/google/android/location/e/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p4}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/e/s$a;->b:Lcom/google/android/location/e/s$a;

    if-ne v3, v4, :cond_0

    invoke-direct {p0, p4, v2, p2, p3}, Lcom/google/android/location/b/e;->a(Lcom/google/android/location/e/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;J)Lcom/google/android/location/e/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/j;

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    invoke-virtual {v0}, Lcom/google/android/location/b/d;->b()V

    iget-object v0, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->b()V

    iget-object v0, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->b()V

    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/e;->a:Lcom/google/android/location/b/d;

    invoke-virtual {v0}, Lcom/google/android/location/b/d;->c()V

    iget-object v0, p0, Lcom/google/android/location/b/e;->b:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->c()V

    iget-object v0, p0, Lcom/google/android/location/b/e;->c:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->c()V

    return-void
.end method
