.class public Lcom/google/android/location/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/location/e/o;

.field public static final f:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<",
            "Lcom/google/android/location/e/w;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<",
            "Lcom/google/android/location/e/C;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lcom/google/android/location/e/o;

.field public final c:Lcom/google/android/location/os/h;

.field public final d:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field

.field private h:J

.field private i:Z

.field private j:Lcom/google/android/location/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/google/android/location/e/o;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    sput-object v0, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    new-instance v0, Lcom/google/android/location/b/f$1;

    invoke-direct {v0}, Lcom/google/android/location/b/f$1;-><init>()V

    sput-object v0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/e/v;

    new-instance v0, Lcom/google/android/location/b/f$2;

    invoke-direct {v0}, Lcom/google/android/location/b/f$2;-><init>()V

    sput-object v0, Lcom/google/android/location/b/f;->g:Lcom/google/android/location/e/v;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/h;J)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    iput-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iput-object p1, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    new-instance v0, Lcom/google/android/location/b/i;

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/location/e/y;->a:Lcom/google/android/location/e/v;

    sget-object v3, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/e/v;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/b/i;-><init>(ILcom/google/android/location/e/v;Lcom/google/android/location/e/v;)V

    iput-object v0, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    new-instance v0, Lcom/google/android/location/b/i;

    const/16 v1, 0x3e8

    sget-object v2, Lcom/google/android/location/e/y;->b:Lcom/google/android/location/e/v;

    sget-object v3, Lcom/google/android/location/b/f;->g:Lcom/google/android/location/e/v;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/b/i;-><init>(ILcom/google/android/location/e/v;Lcom/google/android/location/e/v;)V

    iput-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/b/f;->b(J)V

    return-void
.end method

.method public static a(JJJJ)J
    .locals 2

    add-long v0, p2, p0

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(ILjava/io/DataInputStream;Ljavax/crypto/SecretKey;[B)Ljava/io/DataInput;
    .locals 3

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/io/DataInputStream;

    invoke-static {p2, p3}, Lcom/google/android/location/os/b;->a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0xb

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {p4, v0}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incompatible version."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(J)V
    .locals 6

    const-wide/high16 v4, 0x404e000000000000L

    sget-object v0, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    iput-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x4038000000000000L

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    mul-double/2addr v0, v4

    const-wide v2, 0x408f400000000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    sub-long v0, p1, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/b/f;->a(JZ)V

    iget-object v0, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->b()V

    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->b()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/b/f;->h:J

    return-wide v0
.end method

.method public a(J)V
    .locals 5

    const-wide/32 v0, 0x240c8400

    sub-long v0, p1, v0

    iget-object v2, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    iget-object v3, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v3}, Lcom/google/android/location/os/h;->r()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/b/i;->a(JJ)V

    iget-object v2, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    iget-object v3, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v3}, Lcom/google/android/location/os/h;->q()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/b/i;->a(JJ)V

    return-void
.end method

.method public a(JZ)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/location/b/f;->h:J

    iput-boolean p3, p0, Lcom/google/android/location/b/f;->i:Z

    return-void
.end method

.method public a(Lcom/google/android/location/os/i;)V
    .locals 8

    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lcom/google/android/location/os/i;->d()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v4

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v6

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-interface {p1}, Lcom/google/android/location/os/i;->j()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/b/f;->a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;[BJJ)V

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v6, v7}, Lcom/google/android/location/b/f;->b(J)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, v6, v7}, Lcom/google/android/location/b/f;->b(J)V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-direct {p0, v6, v7}, Lcom/google/android/location/b/f;->b(J)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V
    .locals 17

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v1}, Lcom/google/android/location/os/h;->d()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v1}, Lcom/google/android/location/os/h;->e()I

    move-result v15

    const/4 v1, 0x0

    move v13, v1

    :goto_0
    const/4 v1, 0x3

    invoke-virtual {v14, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-ge v13, v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {v14, v1, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v16

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, -0x1

    const/4 v6, -0x1

    const v10, 0x9c40

    const/4 v7, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_b

    const/4 v7, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    mul-int/lit16 v2, v1, 0x3e8

    const/4 v1, 0x4

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    :cond_2
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_3

    const/16 v6, 0x8

    invoke-virtual {v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    :cond_3
    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_b

    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    mul-int/lit16 v10, v7, 0x3e8

    move v9, v1

    move v8, v2

    move v11, v3

    move v12, v5

    :goto_1
    const/4 v1, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    if-eq v6, v1, :cond_4

    const/4 v1, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/e/e;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/android/location/e/w;

    invoke-direct {v5, v12, v11, v8, v9}, Lcom/google/android/location/e/w;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    move/from16 v2, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/b/i;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    :cond_4
    const/4 v1, 0x3

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x3

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    move-wide v2, v1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;)Lcom/google/android/location/b/a;

    move-result-object v6

    if-nez v6, :cond_9

    if-eqz p2, :cond_9

    const/4 v1, 0x1

    move v5, v1

    :goto_3
    if-eqz v6, :cond_a

    if-eqz p2, :cond_5

    invoke-virtual {v6}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/C;

    invoke-virtual {v1}, Lcom/google/android/location/e/C;->e()Z

    move-result v1

    if-nez v1, :cond_a

    :cond_5
    const/4 v1, 0x1

    :goto_4
    if-nez v5, :cond_6

    if-eqz v1, :cond_7

    :cond_6
    new-instance v5, Lcom/google/android/location/e/C;

    move v6, v12

    move v7, v11

    invoke-direct/range {v5 .. v10}, Lcom/google/android/location/e/C;-><init>(IIIII)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move v9, v15

    move-object v10, v5

    move-wide/from16 v11, p3

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/location/b/i;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    :cond_7
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/e/F;->a(Ljava/lang/String;)J

    move-result-wide v1

    move-wide v2, v1

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    move v5, v1

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    move v9, v1

    move v8, v2

    move v11, v3

    move v12, v5

    goto/16 :goto_1
.end method

.method a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;[BJJ)V
    .locals 11

    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    const/16 v4, 0xb

    if-eq v3, v4, :cond_0

    const/16 v4, 0xa

    if-ne v3, v4, :cond_2

    :cond_0
    invoke-direct {p0, v3, v2, p2, p3}, Lcom/google/android/location/b/f;->a(ILjava/io/DataInputStream;Ljavax/crypto/SecretKey;[B)Ljava/io/DataInput;

    move-result-object v10

    invoke-interface {v10}, Ljava/io/DataInput;->readLong()J

    move-result-wide v2

    invoke-interface {v10}, Ljava/io/DataInput;->readLong()J

    move-result-wide v4

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-static/range {v2 .. v9}, Lcom/google/android/location/b/f;->a(JJJJ)J

    move-result-wide v4

    invoke-interface {v10}, Ljava/io/DataInput;->readBoolean()Z

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/location/b/f;->a(JZ)V

    invoke-interface {v10}, Ljava/io/DataInput;->readBoolean()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/google/android/location/e/o;->f:Lcom/google/android/location/e/v;

    invoke-interface {v4, v10}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/e/o;

    iput-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-wide v4, v4, Lcom/google/android/location/e/o;->e:J

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-static/range {v2 .. v9}, Lcom/google/android/location/b/f;->a(JJJJ)J

    move-result-wide v2

    new-instance v4, Lcom/google/android/location/e/o;

    iget-object v5, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-object v5, v5, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget-object v6, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-object v6, v6, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    invoke-direct {v4, v5, v6, v2, v3}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    iput-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    :cond_1
    iget-object v2, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v2, v10}, Lcom/google/android/location/b/i;->a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;

    iget-object v2, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v2, v10}, Lcom/google/android/location/b/i;->a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;

    :goto_0
    return-void

    :cond_2
    move-wide/from16 v0, p6

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/f;->b(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    move-wide/from16 v0, p6

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/f;->b(J)V

    throw v2
.end method

.method a(Ljava/io/OutputStream;[BJ)V
    .locals 5

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v2, p3, p4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-wide v3, p0, Lcom/google/android/location/b/f;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-boolean v3, p0, Lcom/google/android/location/b/f;->i:Z

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-object v3, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    sget-object v4, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    sget-object v3, Lcom/google/android/location/e/o;->f:Lcom/google/android/location/e/v;

    iget-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    invoke-interface {v3, v4, v2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    iget-object v4, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/location/b/i;->a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V

    iget-object v3, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    iget-object v4, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/location/b/i;->a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v2, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-static {p2, v2}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-void

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    goto :goto_0
.end method

.method public b(Lcom/google/android/location/os/i;)V
    .locals 4

    invoke-interface {p1}, Lcom/google/android/location/os/i;->d()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "nlp_state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-interface {p1, v1}, Lcom/google/android/location/os/i;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/location/b/f;->a(Ljava/io/OutputStream;[BJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/b/f;->i:Z

    return v0
.end method

.method public c()Lcom/google/android/location/b/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    return-object v0
.end method

.method public d()Lcom/google/android/location/b/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    return-object v0
.end method
