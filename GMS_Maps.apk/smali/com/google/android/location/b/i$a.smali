.class Lcom/google/android/location/b/i$a;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field final b:Lcom/google/android/location/b/h;


# direct methods
.method constructor <init>(ILcom/google/android/location/b/h;)V
    .locals 2

    const/high16 v0, 0x3f400000

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput p1, p0, Lcom/google/android/location/b/i$a;->a:I

    iput-object p2, p0, Lcom/google/android/location/b/i$a;->b:Lcom/google/android/location/b/h;

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/location/b/i$a;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/b/i$a;->a:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/b/i$a;->b:Lcom/google/android/location/b/h;

    invoke-virtual {v1}, Lcom/google/android/location/b/h;->a()V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
