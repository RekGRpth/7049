.class public Lcom/google/android/location/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/location/e/v",
        "<",
        "Lcom/google/android/location/b/i",
        "<TK;TP;>;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/b/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i$a",
            "<TK;",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/location/b/h;


# direct methods
.method public constructor <init>(ILcom/google/android/location/e/v;Lcom/google/android/location/e/v;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/location/e/v",
            "<TK;>;",
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/b/i;->b:Lcom/google/android/location/e/v;

    iput-object p3, p0, Lcom/google/android/location/b/i;->c:Lcom/google/android/location/e/v;

    new-instance v0, Lcom/google/android/location/b/h;

    invoke-direct {v0}, Lcom/google/android/location/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    new-instance v0, Lcom/google/android/location/b/i$a;

    iget-object v1, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/b/i$a;-><init>(ILcom/google/android/location/b/h;)V

    iput-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/location/b/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lcom/google/android/location/b/a",
            "<TP;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/i$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    iget-object v2, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/location/b/h;->a(Z)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)",
            "Lcom/google/android/location/b/a",
            "<TP;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/i$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/b/a;->a(J)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/location/b/h;->a(Z)V

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            ")",
            "Lcom/google/android/location/b/i",
            "<TK;TP;>;"
        }
    .end annotation

    :try_start_0
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/location/b/i;->b()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/i;->b:Lcom/google/android/location/e/v;

    invoke-interface {v0, p1}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/location/b/i;->c:Lcom/google/android/location/e/v;

    invoke-interface {v0, p1}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    iget-object v4, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v4, v3, v0}, Lcom/google/android/location/b/i$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/location/b/i;->b()V

    throw v0

    :cond_0
    return-object p0
.end method

.method a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(JJ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    invoke-virtual {v0}, Lcom/google/android/location/b/a;->b()J

    move-result-wide v2

    cmp-long v2, v2, p3

    if-gez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->b()V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/location/b/a;->a()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->b()V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<TK;TP;>;",
            "Ljava/io/DataOutput;",
            ")V"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->size()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v2, p0, Lcom/google/android/location/b/i;->b:Lcom/google/android/location/e/v;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    iget-object v2, p0, Lcom/google/android/location/b/i;->c:Lcom/google/android/location/e/v;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, p2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0

    check-cast p1, Lcom/google/android/location/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/b/i;->a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V

    return-void
.end method

.method public a(ZLjava/lang/Object;ILjava/lang/Object;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;ITP;J)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/b/i$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/location/b/a;

    invoke-direct {v0, p3, p4, p5, p6}, Lcom/google/android/location/b/a;-><init>(ILjava/lang/Object;J)V

    iget-object v1, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/location/b/i$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p3, p4, p5, p6}, Lcom/google/android/location/b/a;->a(ILjava/lang/Object;J)V

    goto :goto_0
.end method

.method public synthetic b(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/location/b/i;->a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->clear()V

    return-void
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    iget-object v1, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    iget v1, v1, Lcom/google/android/location/b/i$a;->a:I

    iget-object v2, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v2}, Lcom/google/android/location/b/i$a;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/b/h;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
