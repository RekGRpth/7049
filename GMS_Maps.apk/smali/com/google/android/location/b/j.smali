.class Lcom/google/android/location/b/j;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;",
        "Lcom/google/android/location/e/A",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .locals 2

    const/high16 v0, 0x3f400000

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput p1, p0, Lcom/google/android/location/b/j;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/b/j;->a:I

    return v0
.end method

.method public a(JJ)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/location/b/j;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/A;

    invoke-virtual {v1}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gez v3, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/location/b/j;->a(Ljava/util/Map$Entry;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/location/e/A;->c()J

    move-result-wide v3

    cmp-long v1, v3, p3

    if-gez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/location/b/j;->a(Ljava/util/Map$Entry;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected a(Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/e/A",
            "<TV;>;>;)V"
        }
    .end annotation

    return-void
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/e/A",
            "<TV;>;>;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/location/b/j;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/b/j;->a:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/location/b/j;->a(Ljava/util/Map$Entry;)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
