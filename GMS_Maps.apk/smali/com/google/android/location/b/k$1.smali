.class final Lcom/google/android/location/b/k$1;
.super Lcom/google/android/location/b/k$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/b/k;->a()Lcom/google/android/location/b/k$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/location/b/k$a",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/common/io/protocol/ProtoBufType;III)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/google/android/location/b/k$a;-><init>(ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/common/io/protocol/ProtoBufType;III)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Long;
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method protected bridge synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/lang/Long;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/location/b/k$1;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Long;Ljava/lang/String;)V

    return-void
.end method

.method protected b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/location/b/k$1;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/location/b/k$1;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
