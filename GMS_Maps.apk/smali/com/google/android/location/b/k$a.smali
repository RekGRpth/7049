.class abstract Lcom/google/android/location/b/k$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/b/k$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/location/b/k$b",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method protected constructor <init>(ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/common/io/protocol/ProtoBufType;III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/b/k$a;->a:I

    iput-object p2, p0, Lcom/google/android/location/b/k$a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iput-object p3, p0, Lcom/google/android/location/b/k$a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iput p4, p0, Lcom/google/android/location/b/k$a;->d:I

    iput p5, p0, Lcom/google/android/location/b/k$a;->e:I

    iput p6, p0, Lcom/google/android/location/b/k$a;->f:I

    return-void
.end method

.method private a(Lcom/google/android/location/b/j;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;)",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/b/k$a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/android/location/b/j;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/A;

    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v5, p0, Lcom/google/android/location/b/k$a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v4, v3, v5}, Lcom/google/android/location/b/k$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V

    iget v3, p0, Lcom/google/android/location/b/k$a;->e:I

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v3, p0, Lcom/google/android/location/b/k$a;->f:I

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->c()J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v0, p0, Lcom/google/android/location/b/k$a;->d:I

    invoke-virtual {v1, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(Lcom/google/android/location/b/j;Ljava/io/InputStream;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;",
            "Ljava/io/InputStream;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/io/InputStream;->read()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/b/k$a;->a:I

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version mismatch while reading LRU cache file (expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/location/b/k$a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/b/k$a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p2, v0}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget v0, p0, Lcom/google/android/location/b/k$a;->d:I

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget v3, p0, Lcom/google/android/location/b/k$a;->d:I

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/location/b/k$a;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v3}, Lcom/google/android/location/b/k$a;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v5

    iget v6, p0, Lcom/google/android/location/b/k$a;->e:I

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    iget v8, p0, Lcom/google/android/location/b/k$a;->f:I

    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    new-instance v3, Lcom/google/android/location/e/A;

    invoke-direct {v3, v5, v6, v7}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    invoke-virtual {v3, v8, v9}, Lcom/google/android/location/e/A;->b(J)V

    invoke-virtual {p1, v4, v3}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/location/b/j;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/b/k$a;->a(Lcom/google/android/location/b/j;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/b/k$a;->a:I

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "TK;TV;)V"
        }
    .end annotation
.end method

.method protected abstract c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")TV;"
        }
    .end annotation
.end method

.method protected abstract d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")TK;"
        }
    .end annotation
.end method
