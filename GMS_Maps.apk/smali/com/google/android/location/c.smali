.class public Lcom/google/android/location/c;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c$1;,
        Lcom/google/android/location/c$a;
    }
.end annotation


# instance fields
.field g:J

.field h:F

.field i:Z

.field j:Lcom/google/android/location/c$a;

.field k:Z

.field l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/os/g;",
            ">;"
        }
    .end annotation
.end field

.field m:Z

.field n:Lcom/google/android/location/os/g;

.field o:J

.field p:Lcom/google/android/location/e/E;

.field q:Z

.field r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field s:J

.field t:Lcom/google/android/location/e/E;

.field u:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;)V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v7, 0x0

    const-string v1, "ActiveCollector"

    sget-object v6, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c;->g:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c;->h:F

    iput-boolean v7, p0, Lcom/google/android/location/c;->i:Z

    sget-object v0, Lcom/google/android/location/c$a;->a:Lcom/google/android/location/c$a;

    iput-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    iput-boolean v7, p0, Lcom/google/android/location/c;->k:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    iput-boolean v7, p0, Lcom/google/android/location/c;->m:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c;->q:Z

    iput-wide v8, p0, Lcom/google/android/location/c;->s:J

    iput-wide v8, p0, Lcom/google/android/location/c;->u:J

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb80

    add-long/2addr v0, v2

    const-wide/32 v2, 0x112a880

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/c;->g:J

    return-void
.end method

.method private a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/c;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/c;->d:Lcom/google/android/location/y;

    iget-object v2, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/y;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method

.method private b(Lcom/google/android/location/os/g;)Z
    .locals 2

    invoke-interface {p1}, Lcom/google/android/location/os/g;->a()F

    move-result v0

    const/high16 v1, 0x42200000

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/location/os/g;->e()F

    move-result v0

    const/high16 v1, 0x3fc00000

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 3

    sget-object v0, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    iget-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    sget-object v1, Lcom/google/android/location/c$a;->a:Lcom/google/android/location/c$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    sget-object v1, Lcom/google/android/location/c$a;->b:Lcom/google/android/location/c$a;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/c;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    sget-object v0, Lcom/google/android/location/c$a;->a:Lcom/google/android/location/c$a;

    iput-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c;->g:J

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    return-void
.end method

.method private j(J)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/c;->u:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/c;->u:J

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/location/c;->u:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_0
    return-void
.end method

.method private k(J)Z
    .locals 6

    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/c$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v3}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return v1

    :pswitch_0
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-lez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/location/c;->m:Z

    if-eqz v2, :cond_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-lez v2, :cond_0

    :pswitch_3
    iget-boolean v2, p0, Lcom/google/android/location/c;->k:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/location/c;->q:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/location/c;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method a(I)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/c;->u:J

    :cond_0
    return-void
.end method

.method a(IIZ)V
    .locals 2

    iput-boolean p3, p0, Lcom/google/android/location/c;->i:Z

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    iput v0, p0, Lcom/google/android/location/c;->h:F

    :cond_0
    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/os/g;)V
    .locals 4

    const/16 v3, 0xa

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/location/c$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v1}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/location/c;->b(Lcom/google/android/location/os/g;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c;->m:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-static {v0, p1, v3}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/location/c;->b(Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-static {v0, p1, v3}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/location/c;->b(Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-static {v0, p1, v3}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    goto :goto_0

    :cond_6
    iput-object p1, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method a(Lcom/google/android/location/os/h;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/location/c;->k:Z

    :cond_0
    return-void
.end method

.method protected a(J)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method b(Lcom/google/android/location/e/E;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->d:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x3e8

    iget-wide v2, p1, Lcom/google/android/location/e/E;->a:J

    iget-wide v4, p0, Lcom/google/android/location/c;->o:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->n()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    goto :goto_0
.end method

.method b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/c;->q:Z

    return-void
.end method

.method protected b(J)Z
    .locals 6

    const-wide/32 v4, 0x112a880

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-gtz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/i;->b(I)V

    sget-object v1, Lcom/google/android/location/c$a;->b:Lcom/google/android/location/c$a;

    iput-object v1, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/c;->g:J

    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/c;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    sget-object v1, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c;->j(J)V

    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method c(Z)V
    .locals 0

    return-void
.end method

.method protected c(J)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/location/c;->m:Z

    if-eqz v2, :cond_1

    iput-wide p1, p0, Lcom/google/android/location/c;->o:J

    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->n()V

    sget-object v1, Lcom/google/android/location/a$c;->d:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c;->j(J)V

    move v0, v1

    goto :goto_0
.end method

.method d()Z
    .locals 2

    iget v1, p0, Lcom/google/android/location/c;->h:F

    iget-boolean v0, p0, Lcom/google/android/location/c;->i:Z

    if-eqz v0, :cond_0

    const v0, 0x3e99999a

    :goto_0
    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const v0, 0x3f333333

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected d(J)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    if-eqz v2, :cond_1

    sget-object v1, Lcom/google/android/location/a$c;->e:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c;->j(J)V

    move v0, v1

    goto :goto_0
.end method

.method protected e(J)Z
    .locals 8

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    :goto_0
    return v6

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    iget-object v3, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    iget-boolean v4, p0, Lcom/google/android/location/c;->i:Z

    const/16 v5, 0xd

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide p1, p0, Lcom/google/android/location/c;->s:J

    iget-object v0, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    iput-object v0, p0, Lcom/google/android/location/c;->t:Lcom/google/android/location/e/E;

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/c;->e:Lcom/google/android/location/a$b;

    iget-object v1, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    iget-object v3, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V

    sget-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/c;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v7}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    sget-object v0, Lcom/google/android/location/c$a;->c:Lcom/google/android/location/c$a;

    iput-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    move v0, v6

    :goto_1
    move v6, v0

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v2, 0x1d4c0

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c;->j(J)V

    move v0, v7

    goto :goto_1
.end method

.method protected f(J)Z
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v3, 0x1d4c0

    add-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/c;->j(J)V

    goto :goto_0
.end method
