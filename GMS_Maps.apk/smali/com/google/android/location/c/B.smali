.class Lcom/google/android/location/c/B;
.super Lcom/google/android/location/c/D;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/B$a;,
        Lcom/google/android/location/c/B$d;,
        Lcom/google/android/location/c/B$b;,
        Lcom/google/android/location/c/B$c;
    }
.end annotation


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[B

.field private final h:Landroid/os/PowerManager;

.field private final i:Landroid/content/Context;

.field private volatile j:Lcom/google/android/location/c/t;

.field private volatile k:Z

.field private final l:Ljava/lang/String;

.field private volatile m:Lcom/google/android/location/c/s;

.field private n:Ljava/lang/Object;

.field private final o:Lcom/google/android/location/c/B$c;

.field private p:Lcom/google/android/location/c/B$d;

.field private volatile q:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 10

    invoke-static {}, Lcom/google/android/location/c/B;->b()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/B;-><init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Ljava/lang/String;Lcom/google/android/location/k/a/c;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Ljava/lang/String;Lcom/google/android/location/k/a/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p7, p9, p3}, Lcom/google/android/location/c/D;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    iput-boolean v1, p0, Lcom/google/android/location/c/B;->k:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/B;->n:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/android/location/c/B;->q:Z

    const-string v0, "Session id should not be null. Please make sure you called the correct constructor."

    invoke-static {p8, v0}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/location/c/B;->i:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/location/c/B;->f:Ljava/lang/String;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/c/B;->h:Landroid/os/PowerManager;

    iput-object p5, p0, Lcom/google/android/location/c/B;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/location/c/B;->g:[B

    iput-object p8, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/location/c/B;->o:Lcom/google/android/location/c/B$c;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/B;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/B;->h:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    if-eqz p3, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p1, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p4, p3, v1}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;ILjava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/location/c/B;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/location/c/B;->n:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/c/s;

    iget-object v1, p0, Lcom/google/android/location/c/B;->h:Landroid/os/PowerManager;

    iget-object v2, p0, Lcom/google/android/location/c/B;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/c/B;->g:[B

    iget-object v4, p0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    iget-object v5, p0, Lcom/google/android/location/c/B;->b:Lcom/google/android/location/k/a/c;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/c/s;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    iput-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    :cond_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/s;->a(Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/s;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/B$d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    return-object v0
.end method

.method static b()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/c/B;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/c/D$a;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v4, "To many data in upload queue."

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/location/c/B;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/B;->k:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/B;->j:Lcom/google/android/location/c/t;

    return-object v0
.end method

.method private e()V
    .locals 3

    new-instance v0, Lcom/google/android/location/c/t;

    iget-object v1, p0, Lcom/google/android/location/c/B;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/c/B;->b:Lcom/google/android/location/k/a/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/c/t;-><init>(Landroid/content/Context;Lcom/google/android/location/k/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/c/B;->j:Lcom/google/android/location/c/t;

    new-instance v0, Lcom/google/android/location/c/B$b;

    const-string v1, "RemoteScanResultWriter.workerThread"

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/c/B$b;-><init>(Lcom/google/android/location/c/B;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/location/c/B$b;->start()V

    invoke-virtual {v0}, Lcom/google/android/location/c/B$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/B$d;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/c/B$d;-><init>(Lcom/google/android/location/c/B;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    return-void
.end method

.method static synthetic f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    invoke-virtual {v0}, Lcom/google/android/location/c/B$d;->a()V

    :cond_0
    return-void
.end method

.method protected declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/c/B;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/B;->q:Z

    invoke-direct {p0}, Lcom/google/android/location/c/B;->e()V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/B;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/c/B;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/c/B;->o:Lcom/google/android/location/c/B$c;

    sget-object v1, Lcom/google/android/location/c/B$c;->a:Lcom/google/android/location/c/B$c;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/B;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return v0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/B;->k:Z

    return-void
.end method
