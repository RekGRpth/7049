.class public Lcom/google/android/location/c/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/r;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/I$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/k/a/c;

.field private final b:Landroid/content/Context;

.field private c:Ljava/util/concurrent/CountDownLatch;

.field private d:Z

.field private e:Lcom/google/android/location/c/I$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/I;->d:Z

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/location/c/d;->a(Ljava/lang/String;Lcom/google/android/location/c/j;)V

    invoke-static/range {p7 .. p7}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/I;->a:Lcom/google/android/location/k/a/c;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/c/I;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/I;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/location/c/I$a;

    iget-object v1, p0, Lcom/google/android/location/c/I;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/c/I;->c:Ljava/util/concurrent/CountDownLatch;

    iget-object v8, p0, Lcom/google/android/location/c/I;->a:Lcom/google/android/location/k/a/c;

    move-object v2, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/I$a;-><init>(Landroid/content/Context;Lcom/google/android/location/c/j;Ljava/util/concurrent/CountDownLatch;Lcom/google/android/location/d/a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Integer;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/c/I;->d:Z

    if-nez v1, :cond_0

    :goto_0
    const-string v1, "Start should be called only once!"

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/I;->d:Z

    iget-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/I$a;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/I;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/I$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    invoke-static {v0}, Lcom/google/android/location/c/I$a;->a(Lcom/google/android/location/c/I$a;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/I;->e:Lcom/google/android/location/c/I$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/I$a;->a()V

    :cond_0
    return-void
.end method
