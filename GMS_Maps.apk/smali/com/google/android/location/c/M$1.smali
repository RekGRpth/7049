.class Lcom/google/android/location/c/M$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/M;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/M;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/M;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    iget-object v1, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-virtual {v0}, Lcom/google/android/location/c/M;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-static {v0}, Lcom/google/android/location/c/M;->a(Lcom/google/android/location/c/M;)V

    iget-object v0, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-static {v0}, Lcom/google/android/location/c/M;->b(Lcom/google/android/location/c/M;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-static {v2}, Lcom/google/android/location/c/M;->c(Lcom/google/android/location/c/M;)Lcom/google/android/location/d/e;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/location/d/e;->a(Ljava/util/List;)V

    iget-object v2, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-virtual {v2}, Lcom/google/android/location/c/M;->f()Lcom/google/android/location/c/k;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/location/c/k;->a(Ljava/util/List;J)V

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
