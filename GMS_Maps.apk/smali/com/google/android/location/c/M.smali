.class Lcom/google/android/location/c/M;
.super Lcom/google/android/location/c/E;
.source "SourceFile"


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Landroid/net/wifi/WifiManager;

.field private final e:Lcom/google/android/location/d/e;

.field private f:Landroid/net/wifi/WifiManager$WifiLock;

.field private final g:Landroid/content/BroadcastReceiver;

.field private volatile h:I

.field private i:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/d/e;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 1

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    new-instance v0, Lcom/google/android/location/c/M$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/M$1;-><init>(Lcom/google/android/location/c/M;)V

    iput-object v0, p0, Lcom/google/android/location/c/M;->g:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c/M;->h:I

    new-instance v0, Lcom/google/android/location/c/M$2;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/M$2;-><init>(Lcom/google/android/location/c/M;)V

    iput-object v0, p0, Lcom/google/android/location/c/M;->i:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/location/c/M;->c:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/location/c/M;->e:Lcom/google/android/location/d/e;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/M;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/c/M;->c()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/c/M;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/c/M;)Lcom/google/android/location/d/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/M;->e:Lcom/google/android/location/d/e;

    return-object v0
.end method

.method private c()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/location/c/M;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/M;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/location/c/M;->i()V

    invoke-virtual {p0}, Lcom/google/android/location/c/M;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/M;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/c/k;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    iget v1, p0, Lcom/google/android/location/c/M;->h:I

    invoke-interface {v0, v1}, Lcom/google/android/location/c/l;->a_(I)V

    :cond_0
    iget v0, p0, Lcom/google/android/location/c/M;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/c/M;->h:I

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x2

    const-string v2, "WifiScanner"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/c/M;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/c/M;->g:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/location/c/M;->c()V

    return-void
.end method

.method protected b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/M;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/c/M;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->f()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
