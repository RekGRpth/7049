.class public Lcom/google/android/location/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/k/a/c;

.field private final b:Ljavax/crypto/spec/SecretKeySpec;

.field private final c:Z

.field private final d:Ljavax/crypto/Mac;

.field private final e:Ljavax/crypto/Cipher;


# direct methods
.method private constructor <init>([B[BLcom/google/android/location/k/a/c;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v0, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/c/a;->b:Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {p3}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/a;->a:Lcom/google/android/location/k/a/c;

    if-eqz p2, :cond_0

    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-direct {p0, p2}, Lcom/google/android/location/c/a;->a([B)Ljavax/crypto/Mac;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/location/c/a;->c:Z

    :goto_0
    :try_start_0
    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    return-void

    :cond_0
    const-string v0, "AES"

    iput-object v1, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/c/a;->c:Z

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public static a([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;
    .locals 2

    new-instance v0, Lcom/google/android/location/c/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/location/c/a;-><init>([B[BLcom/google/android/location/k/a/c;)V

    return-object v0
.end method

.method private a([B)Ljavax/crypto/Mac;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "HmacSHA1"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "HmacSHA1"

    invoke-direct {v2, p1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;I)[B
    .locals 4

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-array v1, p2, [B

    :goto_0
    if-lez p2, :cond_0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v1, v2, p2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    sub-int/2addr p2, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a([BLjavax/crypto/spec/IvParameterSpec;I)[B
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create cipher."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    const-string v0, "encrypt"

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/location/c/a;->b:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0, p3, v1, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    iget-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "decrypt"

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to encrypt or decrypt."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;
    .locals 5

    const/16 v4, 0x20

    const/16 v3, 0x10

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    array-length v0, p0

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Key must be 32 bytes."

    invoke-static {v0, v2}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/location/c/a;

    invoke-static {p0, v1, v3}, Lcom/google/android/location/k/c;->a([BII)[B

    move-result-object v1

    invoke-static {p0, v3, v4}, Lcom/google/android/location/k/c;->a([BII)[B

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/location/c/a;-><init>([B[BLcom/google/android/location/k/a/c;)V

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private b()Ljavax/crypto/spec/IvParameterSpec;
    .locals 2

    const/16 v0, 0x10

    new-array v0, v0, [B

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    return-object v1
.end method


# virtual methods
.method public declared-synchronized a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "[B>;"
        }
    .end annotation

    const/4 v0, 0x0

    const/16 v4, 0x14

    const/16 v5, 0x10

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create HMAC generator."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    const/4 v1, 0x4

    const/high16 v2, 0x200000

    if-gt v3, v2, :cond_1

    if-gez v3, :cond_2

    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid data length or too long: %d bytes."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v2, :cond_9

    const/16 v0, 0x14

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/InputStream;I)[B

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, 0x4

    if-eqz v1, :cond_3

    array-length v2, v1

    if-eq v2, v4, :cond_4

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to read HMAC."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/16 v2, 0x10

    invoke-direct {p0, p1, v2}, Lcom/google/android/location/c/a;->a(Ljava/io/InputStream;I)[B

    move-result-object v4

    array-length v2, v4

    add-int/2addr v2, v0

    if-eqz v4, :cond_5

    array-length v0, v4

    if-eq v0, v5, :cond_6

    :cond_5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to read IV."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v0, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    :goto_0
    invoke-direct {p0, p1, v3}, Lcom/google/android/location/c/a;->a(Ljava/io/InputStream;I)[B

    move-result-object v4

    add-int/2addr v2, v3

    if-eqz v4, :cond_7

    array-length v5, v4

    if-eq v5, v3, :cond_7

    :cond_7
    iget-boolean v3, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    invoke-virtual {v3, v4}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v3

    invoke-static {v3, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v0, Ljava/io/IOException;

    const-string v1, "HMAC does not match."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    const/4 v1, 0x2

    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/location/c/a;->a([BLjavax/crypto/spec/IvParameterSpec;I)[B

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_9
    move v2, v1

    move-object v1, v0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/io/DataOutputStream;[B)V
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create HMAC, data can not be signed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-eqz p2, :cond_1

    :try_start_1
    array-length v1, p2

    const/high16 v2, 0x200000

    if-le v1, v2, :cond_2

    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid data: data is empty or too long."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/google/android/location/c/a;->b()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v1

    :goto_0
    const/4 v2, 0x1

    invoke-direct {p0, p2, v1, v2}, Lcom/google/android/location/c/a;->a([BLjavax/crypto/spec/IvParameterSpec;I)[B

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    :cond_3
    array-length v3, v2

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-boolean v3, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v3, :cond_4

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v1}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->write([B)V

    :cond_4
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/a;->c:Z

    return v0
.end method
