.class public Lcom/google/android/location/c/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/k/a/c;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private final e:Lcom/google/android/location/c/a;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/k/a/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c/c;->c:I

    iput-object p1, p0, Lcom/google/android/location/c/c;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/location/c/c;->e:Lcom/google/android/location/c/a;

    invoke-static {p3}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/c;->a:Lcom/google/android/location/k/a/c;

    return-void
.end method

.method private a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c/c;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;
    .locals 7

    const/4 v2, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v4, 0x6

    :try_start_0
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    if-nez v5, :cond_0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    move v3, v1

    :cond_0
    new-instance v4, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/c;->e:Lcom/google/android/location/c/a;

    invoke-virtual {v0}, Lcom/google/android/location/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_0
    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/c/c;->e:Lcom/google/android/location/c/a;

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/c/L;->a([B)[B

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V

    iget v0, p0, Lcom/google/android/location/c/c;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/c/c;->c:I

    iget v0, p0, Lcom/google/android/location/c/c;->c:I

    const/16 v2, 0x32

    if-lt v0, v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/c/c;->a()V

    :cond_2
    new-instance v0, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_3
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v2, v0

    :goto_2
    :try_start_4
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const-string v4, "File not found."

    invoke-direct {v1, v3, v0, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    if-eqz v2, :cond_5

    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_5
    :goto_3
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception v1

    move-object v2, v0

    :goto_4
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/location/c/c;->a()V

    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const-string v4, "Failed to write data to file"

    invoke-direct {v1, v3, v0, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-eqz v2, :cond_6

    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_6
    :goto_5
    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_6
    if-eqz v1, :cond_7

    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_7
    :goto_7
    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_7

    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catch_6
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catch_7
    move-exception v0

    move-object v2, v1

    goto :goto_2
.end method
