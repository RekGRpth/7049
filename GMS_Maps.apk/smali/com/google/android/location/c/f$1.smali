.class Lcom/google/android/location/c/f$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/f;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->c(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/f$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/location/c/f$1$1;-><init>(Lcom/google/android/location/c/f$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->e(Lcom/google/android/location/c/f;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to list files in directory:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    array-length v2, v4

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/c/f$1;->a()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/location/c/C;->c(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is locked."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/c/H;->a(Ljava/lang/String;)Lcom/google/android/location/c/H;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/c/H;->a()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v5}, Lcom/google/android/location/c/H;->b()I

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    :goto_1
    invoke-virtual {v5}, Lcom/google/android/location/c/H;->c()Z

    move-result v5

    if-nez v5, :cond_5

    if-nez v2, :cond_5

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0, v3}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/io/File;)Z

    :cond_3
    invoke-direct {p0}, Lcom/google/android/location/c/f$1;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load SessionSummary: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2, v4}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;[Ljava/lang/String;)Lcom/google/android/location/c/f$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_6

    invoke-direct {p0}, Lcom/google/android/location/c/f$1;->a()V

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-virtual {v2}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    :goto_2
    invoke-static {v3, v0}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Z)Z

    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    const-string v1, "Failed to lock working directory."

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0, v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/c/C;->b(Ljava/lang/String;)V

    throw v0
.end method
