.class Lcom/google/android/location/c/p;
.super Lcom/google/android/location/c/E;
.source "SourceFile"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/location/LocationListener;


# instance fields
.field private final c:Lcom/google/android/location/d/a;

.field private final d:Ljava/lang/String;

.field private e:Z

.field private final f:Z

.field private final g:Z

.field private h:Landroid/location/GpsStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZZLcom/google/android/location/d/a;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p5, p6, p7}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    iput-boolean v1, p0, Lcom/google/android/location/c/p;->e:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    iput-boolean p2, p0, Lcom/google/android/location/c/p;->f:Z

    iput-boolean p3, p0, Lcom/google/android/location/c/p;->g:Z

    if-nez p4, :cond_0

    new-instance v0, Lcom/google/android/location/d/a;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/d/a;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/c/p;->a:Lcom/google/android/location/k/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/k/a/c;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    return-void

    :cond_0
    iput-object p4, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 8

    iget-boolean v0, p0, Lcom/google/android/location/c/p;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Landroid/location/GpsStatus$Listener;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    const-string v2, "gps"

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/location/c/k;->getLooper()Landroid/os/Looper;

    move-result-object v7

    move-object v6, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->g()V

    :cond_2
    return-void
.end method

.method protected b()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/location/c/p;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/location/d/a;->b(Ljava/lang/String;Landroid/location/GpsStatus$Listener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->h()V

    :cond_2
    return-void
.end method

.method public onGpsStatusChanged(I)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/location/c/p;->g:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->h()V

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/location/d/a;->a(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/c/k;->a(Landroid/location/GpsStatus;J)V

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/location/c/p;->f:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->h()V

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/c/k;->a(Landroid/location/Location;J)V

    iget-boolean v0, p0, Lcom/google/android/location/c/p;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/p;->e:Z

    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v1

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getX()F

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getY()F

    move-result v3

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getZ()F

    move-result v4

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/location/c/k;->a(FFFF)V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
