.class Lcom/google/android/location/c/s$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/c/s;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field final synthetic c:Lcom/google/android/location/c/s;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/s;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iput p2, p0, Lcom/google/android/location/c/s$2;->a:I

    iput-object p3, p0, Lcom/google/android/location/c/s$2;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    invoke-static {v0}, Lcom/google/android/location/c/s;->c(Lcom/google/android/location/c/s;)Landroid/os/PowerManager;

    move-result-object v0

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/location/c/s;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    invoke-static {v0}, Lcom/google/android/location/c/s;->d(Lcom/google/android/location/c/s;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget v2, p0, Lcom/google/android/location/c/s$2;->a:I

    const/4 v3, 0x0

    const-string v4, "Failed to create lock file."

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, p0, Lcom/google/android/location/c/s$2;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/android/location/c/s;->a(Lcom/google/android/location/c/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget v3, p0, Lcom/google/android/location/c/s$2;->a:I

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget v3, p0, Lcom/google/android/location/c/s$2;->a:I

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
