.class Lcom/google/android/location/c/s;
.super Lcom/google/android/location/c/D;
.source "SourceFile"


# instance fields
.field private volatile e:Z

.field private final f:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final g:Landroid/os/PowerManager;

.field private volatile h:Ljava/lang/String;

.field private final i:[B

.field private volatile j:Z

.field private volatile k:Ljava/lang/String;

.field private l:Ljava/lang/Object;

.field private volatile m:Lcom/google/android/location/c/c;


# direct methods
.method constructor <init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V
    .locals 9

    invoke-direct {p0, p4, p5, p6}, Lcom/google/android/location/c/D;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/s;->e:Z

    new-instance v0, Lcom/google/android/location/c/s$1;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x32

    invoke-direct {v7, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    new-instance v8, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;

    invoke-direct {v8}, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;-><init>()V

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/s$1;-><init>(Lcom/google/android/location/c/s;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/s;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/s;->l:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/location/c/s;->g:Landroid/os/PowerManager;

    iput-object p2, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    :cond_0
    iput-object p3, p0, Lcom/google/android/location/c/s;->i:[B

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/location/c/s;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;[B)Lcom/google/android/location/c/D$a;
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/location/c/s;->c()Lcom/google/android/location/c/D$a;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/FileOutputStream;->write([B)V

    new-instance v0, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_3
    const-string v2, "Failed to save data: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/location/c/D$a;

    const/4 v4, 0x0

    invoke-direct {v0, v4, v3, v2}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_1
    :goto_3
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/location/c/s;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/location/c/D$a;
    .locals 5

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v1

    :goto_0
    if-nez v1, :cond_0

    const-string v1, "Failed to create dir: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcom/google/android/location/c/D$a;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v4, v0, v2}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/c/s;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/c/s;->c()Lcom/google/android/location/c/D$a;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/c/s;->e:Z

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/c/s;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/s;->j:Z

    return v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x6

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "No sequence number specified!"

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v4, Lcom/google/android/location/c/s$2;

    invoke-direct {v4, p0, v0, p1}, Lcom/google/android/location/c/s$2;-><init>(Lcom/google/android/location/c/s;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "Failed to write to file: work queue full."

    iget-object v3, p0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, v1}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/location/c/s;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/s;->g:Landroid/os/PowerManager;

    return-object v0
.end method

.method private c()Lcom/google/android/location/c/D$a;
    .locals 9

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/location/c/s;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/c/s;->j:Z

    if-eqz v1, :cond_0

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/c/s;->b(Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v1

    if-eqz v1, :cond_1

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_1
    const-string v1, "%d-%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v5

    const-wide v7, 0x412e848000000000L

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/c/s;->b(Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v1

    if-eqz v1, :cond_2

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/c/s;->j:Z

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;
    .locals 5

    invoke-direct {p0}, Lcom/google/android/location/c/s;->c()Lcom/google/android/location/c/D$a;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/s;->m:Lcom/google/android/location/c/c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/s;->i:[B

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const-string v3, "Encryption Key invalid."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/location/c/c;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/c/s;->i:[B

    iget-object v3, p0, Lcom/google/android/location/c/s;->b:Lcom/google/android/location/k/a/c;

    invoke-static {v2, v3}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/s;->b:Lcom/google/android/location/k/a/c;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/c/c;-><init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/k/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/c/s;->m:Lcom/google/android/location/c/c;

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/location/c/s;->m:Lcom/google/android/location/c/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/location/c/s;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/c/s;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/google/android/location/c/D$a;
    .locals 4

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    const-string v1, "sessionId in two writes should be consistent."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    :cond_1
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v0, "sessionId"

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c/s;->a(Ljava/lang/String;[B)Lcom/google/android/location/c/D$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object p1, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    return-void
.end method

.method a(Lcom/google/android/location/c/H;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/location/c/H;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/c/s;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/location/c/s;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    const/4 v0, 0x6

    invoke-virtual {p1, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, p1}, Lcom/google/android/location/c/s;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
