.class Lcom/google/android/location/c/u;
.super Lcom/google/android/location/c/D;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/location/c/H;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/location/c/H;-><init>(Z)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/c/D;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    iput-object v2, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lcom/google/android/location/c/u;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/android/location/c/u;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/c/u;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/u;->a:Lcom/google/android/location/c/l;

    iget-object v1, p0, Lcom/google/android/location/c/u;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/c/l;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x4

    iget-object v1, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_1

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {p2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/location/c/u;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
