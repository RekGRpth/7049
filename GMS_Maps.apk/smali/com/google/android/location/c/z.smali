.class Lcom/google/android/location/c/z;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/z$1;
    }
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/location/c/m;

.field private c:Lcom/google/android/location/c/o;

.field private d:Lcom/google/android/location/c/k;

.field private e:Lcom/google/android/location/k/a/c;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/c/z;->a:Landroid/os/Handler;

    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/c/l;)Lcom/google/android/location/c/D;
    .locals 9

    sget-object v0, Lcom/google/android/location/c/z$1;->a:[I

    invoke-interface {p2}, Lcom/google/android/location/c/j;->c()Lcom/google/android/location/c/j$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/c/j$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/location/c/u;

    iget-object v1, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    invoke-direct {v0, p3, v1}, Lcom/google/android/location/c/u;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/location/c/s;

    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    invoke-interface {p2}, Lcom/google/android/location/c/j;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/location/c/j;->f()[B

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    new-instance v6, Lcom/google/android/location/c/H;

    invoke-interface {p2}, Lcom/google/android/location/c/j;->k()Z

    move-result v4

    invoke-direct {v6, v4}, Lcom/google/android/location/c/H;-><init>(Z)V

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/c/s;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/location/c/B;

    sget-object v2, Lcom/google/android/location/c/B$c;->a:Lcom/google/android/location/c/B$c;

    new-instance v3, Lcom/google/android/location/c/H;

    invoke-interface {p2}, Lcom/google/android/location/c/j;->k()Z

    move-result v1

    invoke-direct {v3, v1}, Lcom/google/android/location/c/H;-><init>(Z)V

    const/4 v4, 0x0

    invoke-interface {p2}, Lcom/google/android/location/c/j;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2}, Lcom/google/android/location/c/j;->f()[B

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    move-object v1, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/B;-><init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/z;->b:Lcom/google/android/location/c/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/z;->d:Lcom/google/android/location/c/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/z;->d:Lcom/google/android/location/c/k;

    iget-object v1, p0, Lcom/google/android/location/c/z;->b:Lcom/google/android/location/c/m;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->a(Lcom/google/android/location/c/E;)V

    :cond_0
    return-void
.end method

.method a(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;)Z
    .locals 12

    move-object/from16 v0, p6

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/c/z;->a(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/c/l;)Lcom/google/android/location/c/D;

    move-result-object v2

    const/4 v11, 0x0

    new-instance v1, Lcom/google/android/location/c/o;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/location/c/j;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/c/o;-><init>(Lcom/google/android/location/c/D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/z;->c:Lcom/google/android/location/c/o;

    new-instance v1, Lcom/google/android/location/c/k;

    iget-object v2, p0, Lcom/google/android/location/c/z;->c:Lcom/google/android/location/c/o;

    iget-object v3, p0, Lcom/google/android/location/c/z;->a:Landroid/os/Handler;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/location/c/k;-><init>(Lcom/google/android/location/c/o;Landroid/os/Handler;ILcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/z;->d:Lcom/google/android/location/c/k;

    new-instance v1, Lcom/google/android/location/c/m;

    invoke-interface {p2}, Lcom/google/android/location/c/j;->b()Ljava/util/Set;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/location/c/j;->j()Ljava/util/Map;

    move-result-object v4

    invoke-interface {p2}, Lcom/google/android/location/c/j;->g()Z

    move-result v6

    iget-object v7, p0, Lcom/google/android/location/c/z;->d:Lcom/google/android/location/c/k;

    invoke-interface {p2}, Lcom/google/android/location/c/j;->h()Z

    move-result v8

    iget-object v10, p0, Lcom/google/android/location/c/z;->e:Lcom/google/android/location/k/a/c;

    move-object v2, p1

    move-object v5, p3

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/location/c/m;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/d/a;ZLcom/google/android/location/c/k;ZLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/z;->b:Lcom/google/android/location/c/m;

    iget-object v1, p0, Lcom/google/android/location/c/z;->b:Lcom/google/android/location/c/m;

    invoke-virtual {v1}, Lcom/google/android/location/c/m;->c()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {p2}, Lcom/google/android/location/c/j;->e()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/z;->b:Lcom/google/android/location/c/m;

    invoke-virtual {v1}, Lcom/google/android/location/c/m;->d()V

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/c/z;->b:Lcom/google/android/location/c/m;

    invoke-interface {p2}, Lcom/google/android/location/c/j;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/c/m;->a(J)V

    goto :goto_0

    :cond_1
    move v1, v11

    goto :goto_1
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/z;->c:Lcom/google/android/location/c/o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/z;->c:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    goto :goto_0
.end method

.method b()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/c/z;->a()V

    iget-object v0, p0, Lcom/google/android/location/c/z;->d:Lcom/google/android/location/c/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/z;->d:Lcom/google/android/location/c/k;

    invoke-virtual {v0}, Lcom/google/android/location/c/k;->a()V

    :cond_0
    return-void
.end method
