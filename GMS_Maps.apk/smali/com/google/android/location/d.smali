.class public Lcom/google/android/location/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/d$1;,
        Lcom/google/android/location/d$a;
    }
.end annotation


# static fields
.field private static final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Lcom/google/android/location/d$a;

.field b:Z

.field c:Z

.field d:Z

.field e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

.field f:J

.field g:J

.field h:Z

.field i:I

.field j:Z

.field k:J

.field l:Lcom/google/android/location/i/c$a;

.field private final n:Lcom/google/android/location/os/i;

.field private final o:Lcom/google/android/location/a/b;

.field private final p:Lcom/google/android/location/i/a;

.field private final q:Lcom/google/android/location/os/h;

.field private final r:Ljava/util/Random;

.field private final s:Ljava/util/Calendar;

.field private t:Z

.field private u:Z

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x1e

    const/4 v4, 0x0

    new-instance v0, Lcom/google/common/collect/aw;

    invoke-direct {v0}, Lcom/google/common/collect/aw;-><init>()V

    new-instance v1, Lcom/google/android/location/k/b;

    const/4 v2, 0x7

    const/16 v3, 0xa

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/k/b;

    const/16 v2, 0xf

    const/16 v3, 0x13

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;)V
    .locals 6

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/d;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;Ljava/util/Random;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;Ljava/util/Random;)V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    sget-object v0, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    iput-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    iput-boolean v1, p0, Lcom/google/android/location/d;->t:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->u:Z

    iput-boolean v1, p0, Lcom/google/android/location/d;->v:Z

    iput-boolean v1, p0, Lcom/google/android/location/d;->b:Z

    iput-boolean v1, p0, Lcom/google/android/location/d;->c:Z

    iput-boolean v1, p0, Lcom/google/android/location/d;->d:Z

    iput-object v4, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    iput-wide v2, p0, Lcom/google/android/location/d;->f:J

    iput-wide v2, p0, Lcom/google/android/location/d;->g:J

    iput-boolean v1, p0, Lcom/google/android/location/d;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/d;->i:I

    iput-boolean v1, p0, Lcom/google/android/location/d;->j:Z

    iput-wide v2, p0, Lcom/google/android/location/d;->k:J

    iput-object v4, p0, Lcom/google/android/location/d;->l:Lcom/google/android/location/i/c$a;

    iput-object p4, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/a/b;

    iput-object p1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    iput-object p3, p0, Lcom/google/android/location/d;->q:Lcom/google/android/location/os/h;

    iput-object p2, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/i/a;

    iput-object p5, p0, Lcom/google/android/location/d;->r:Ljava/util/Random;

    return-void
.end method

.method private a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;
    .locals 3

    sget-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JJ)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    add-long v1, p3, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    iget-object v3, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Lcom/google/android/location/k/b;->a(Ljava/util/Calendar;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/i/a;

    const-wide/32 v2, 0xe290

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/location/i/a;->e(JZ)Z

    move-result v1

    if-eqz v0, :cond_1

    if-nez v1, :cond_4

    :cond_1
    if-nez v1, :cond_2

    :cond_2
    if-nez v0, :cond_3

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;J)V

    :goto_1
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    invoke-direct {p0, v1, v0, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/util/Calendar;J)V
    .locals 3

    sget-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    invoke-static {p1, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;J)V

    return-void
.end method

.method private a(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V
    .locals 6

    invoke-virtual {p2, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p2, Lcom/google/android/location/k/b;->a:J

    invoke-static {p1, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/d;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;J)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p3

    iget-wide v2, p0, Lcom/google/android/location/d;->g:J

    const-wide/32 v4, 0x927c0

    add-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    add-long v0, p3, v2

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_2
    invoke-virtual {p2, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;J)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V

    goto :goto_0
.end method

.method private a(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/d;->f:J

    const-wide/16 v2, 0x7d0

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JZ)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->g(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/d;->b(JZ)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(JJ)V
    .locals 4

    iput-wide p1, p0, Lcom/google/android/location/d;->f:J

    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    return-void
.end method

.method private b(JZ)V
    .locals 6

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    iput-wide v4, p0, Lcom/google/android/location/d;->f:J

    invoke-direct {p0}, Lcom/google/android/location/d;->f()V

    iput-boolean v2, p0, Lcom/google/android/location/d;->d:Z

    iput-object v3, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    iput-boolean v2, p0, Lcom/google/android/location/d;->b:Z

    iput-boolean v2, p0, Lcom/google/android/location/d;->c:Z

    if-eqz p3, :cond_0

    iput-wide v4, p0, Lcom/google/android/location/d;->k:J

    iput-object v3, p0, Lcom/google/android/location/d;->l:Lcom/google/android/location/i/c$a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    iput-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    iput-wide p1, p0, Lcom/google/android/location/d;->g:J

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    return-void
.end method

.method private b(Ljava/util/Calendar;J)V
    .locals 2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/google/android/location/d;->b(JJ)V

    return-void
.end method

.method private b(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    invoke-static {p1, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    sget-object v0, Lcom/google/android/location/d;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;J)V

    goto :goto_0
.end method

.method private b(J)Z
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/d;->f:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    iget-wide v4, p0, Lcom/google/android/location/d;->f:J

    add-long/2addr v4, v2

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    invoke-direct {p0, v0}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/d;->b(JZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/d;->c()V

    invoke-direct {p0}, Lcom/google/android/location/d;->e()V

    iget-object v4, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    iget-wide v5, v0, Lcom/google/android/location/k/b;->b:J

    invoke-static {v4, v5, v6}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lcom/google/android/location/d;->s:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-direct {p0, v4, v5, v2, v3}, Lcom/google/android/location/d;->b(JJ)V

    sget-object v0, Lcom/google/android/location/d$a;->b:Lcom/google/android/location/d$a;

    iput-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v2, 0xa

    iget-wide v3, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/android/location/d;->a(JJ)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/d;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->j:Z

    :cond_0
    return-void
.end method

.method private c(J)Z
    .locals 5

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/location/d;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/d;->r:Ljava/util/Random;

    iget-object v2, p0, Lcom/google/android/location/d;->q:Lcom/google/android/location/os/h;

    invoke-virtual {v2}, Lcom/google/android/location/os/h;->k()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-long v1, v1

    add-long/2addr v1, p1

    iget-object v3, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/location/d;->b(JJ)V

    sget-object v1, Lcom/google/android/location/d$a;->c:Lcom/google/android/location/d$a;

    iput-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/d;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/d;->j:Z

    :cond_0
    return-void
.end method

.method private d(J)Z
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    sget-object v2, Lcom/google/android/location/os/i$b;->a:Lcom/google/android/location/os/i$b;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/location/d;->f()V

    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    iput-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/location/d;->h:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/d;->i:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/d;->q:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/d;->i:I

    iget-object v0, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/a/b;

    iget v2, p0, Lcom/google/android/location/d;->i:I

    invoke-virtual {v0, v2}, Lcom/google/android/location/a/b;->b(I)V

    iput-boolean v1, p0, Lcom/google/android/location/d;->h:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(J)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/location/d;->c:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/location/d;->u:Z

    if-eqz v2, :cond_1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/location/d;->c:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/location/d;->u:Z

    if-nez v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/d;->c()V

    iget-object v2, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/i/a;

    invoke-virtual {v2}, Lcom/google/android/location/i/a;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x1d4c0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    const-wide/32 v4, 0xe290

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/i/a;

    invoke-virtual {v4, v2, v3, v1}, Lcom/google/android/location/i/a;->c(JZ)Lcom/google/android/location/i/c$a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/d;->l:Lcom/google/android/location/i/c$a;

    iget-object v1, p0, Lcom/google/android/location/d;->l:Lcom/google/android/location/i/c$a;

    if-nez v1, :cond_3

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    add-long v1, v4, v2

    iput-wide v1, p0, Lcom/google/android/location/d;->k:J

    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const-string v2, "BurstCollectionTrigger"

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    iget-wide v1, p0, Lcom/google/android/location/d;->k:J

    iget-object v3, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/location/d;->b(JJ)V

    sget-object v1, Lcom/google/android/location/d$a;->e:Lcom/google/android/location/d$a;

    iput-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private f()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, -0x1

    iget-boolean v0, p0, Lcom/google/android/location/d;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/location/d;->i:I

    if-eq v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/a/b;

    iget v2, p0, Lcom/google/android/location/d;->i:I

    invoke-virtual {v0, v2}, Lcom/google/android/location/a/b;->c(I)V

    iput-boolean v1, p0, Lcom/google/android/location/d;->h:Z

    iput v3, p0, Lcom/google/android/location/d;->i:I

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private f(J)Z
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v1, p0, Lcom/google/android/location/d;->k:J

    sub-long/2addr v1, p1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/i/a;

    iget-object v4, p0, Lcom/google/android/location/d;->l:Lcom/google/android/location/i/c$a;

    invoke-virtual {v3, v4, v1, v2}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c$a;J)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const-string v2, "BurstCollectionTrigger"

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/d;->k:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/d;->l:Lcom/google/android/location/i/c$a;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    const/16 v2, 0xa

    iget-wide v3, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_0
.end method

.method private g()V
    .locals 6

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    iget-object v2, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/location/d$1;->a:[I

    iget-object v5, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    invoke-virtual {v5}, Lcom/google/android/location/d$a;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    if-eq v1, v2, :cond_1

    :cond_1
    if-nez v0, :cond_0

    return-void

    :pswitch_0
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->b(J)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->c(J)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->d(J)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->e(J)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->f(J)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private g(J)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->m()Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/location/d;->q:Lcom/google/android/location/os/h;

    invoke-virtual {v3}, Lcom/google/android/location/os/h;->i()Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/location/d;->t:Z

    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/location/d;->v:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    sget-object v3, Lcom/google/android/location/d$1;->a:[I

    iget-object v4, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    invoke-virtual {v4}, Lcom/google/android/location/d$a;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :pswitch_0
    if-nez v0, :cond_3

    :goto_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :pswitch_1
    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/location/d;->b:Z

    if-eqz v0, :cond_0

    :cond_4
    move v2, v1

    goto :goto_1

    :pswitch_2
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    sget-object v3, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    sget-object v3, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-ne v0, v3, :cond_6

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/location/d;->b:Z

    if-eqz v0, :cond_0

    :cond_6
    move v2, v1

    goto :goto_1

    :pswitch_3
    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/location/d;->b:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/location/d;->u:Z

    if-nez v0, :cond_7

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    move v2, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(I)V
    .locals 1

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    :cond_0
    return-void
.end method

.method public a(IIZ)V
    .locals 5

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    if-eqz p3, :cond_0

    float-to-double v1, v0

    const-wide v3, 0x3fc99999a0000000L

    cmpl-double v1, v1, v3

    if-gez v1, :cond_1

    :cond_0
    if-nez p3, :cond_3

    float-to-double v0, v0

    const-wide v2, 0x3fe3333340000000L

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/location/d;->v:Z

    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->b:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->c:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->d:Z

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/d;->u:Z

    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    return-void
.end method

.method public a()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v2, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/location/d;->c:Z

    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->e:Lcom/google/android/location/d$a;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->b:Z

    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/d;->t:Z

    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    return-void
.end method
