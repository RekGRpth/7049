.class Lcom/google/android/location/d/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field final a:Lcom/google/android/location/d/a$a;

.field final b:Ljava/lang/String;

.field final c:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/google/android/location/d/a$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/d/a$b;->b:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/location/d/a$b;->c:J

    iput-object p4, p0, Lcom/google/android/location/d/a$b;->a:Lcom/google/android/location/d/a$a;

    return-void
.end method


# virtual methods
.method public a(Ljava/text/Format;Ljava/util/Date;Ljava/io/PrintWriter;)V
    .locals 5

    const/4 v4, 0x2

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/d/a$b;->c:J

    add-long/2addr v0, v2

    invoke-virtual {p2, v0, v1}, Ljava/util/Date;->setTime(J)V

    invoke-virtual {p1, p2}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " @"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/location/d/a$b;->c:J

    invoke-virtual {p3, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    const-string v0, " "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/d/a$b;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d/a$b;->b:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, ": "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/d/a$b;->a:Lcom/google/android/location/d/a$a;

    invoke-virtual {v0}, Lcom/google/android/location/d/a$a;->ordinal()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/d/a$b;->b:Ljava/lang/String;

    goto :goto_0
.end method
