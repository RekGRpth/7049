.class public Lcom/google/android/location/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/d/a$b;,
        Lcom/google/android/location/d/a$a;
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/d/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/location/LocationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    if-eqz p2, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    goto :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/google/android/location/d/a$a;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/d/a$b;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/google/android/location/d/a$b;-><init>(Ljava/lang/String;JLcom/google/android/location/d/a$a;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    .locals 7

    const-string v0, "gps"

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/google/android/location/d/a$a;->a:Lcom/google/android/location/d/a$a;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Lcom/google/android/location/d/a$a;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p2}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    move-object v1, p2

    move-wide v2, p3

    move v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;ZLandroid/location/LocationListener;)V
    .locals 1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/location/d/a$a;->b:Lcom/google/android/location/d/a$a;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Lcom/google/android/location/d/a$a;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p3}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method public declared-synchronized a(Ljava/text/Format;JLjava/io/PrintWriter;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iget-object v0, p0, Lcom/google/android/location/d/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/a$b;

    invoke-virtual {v1, p2, p3}, Ljava/util/Date;->setTime(J)V

    invoke-virtual {v0, p1, v1, p4}, Lcom/google/android/location/d/a$b;->a(Ljava/text/Format;Ljava/util/Date;Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/location/GpsStatus$Listener;)Z
    .locals 1

    sget-object v0, Lcom/google/android/location/d/a$a;->c:Lcom/google/android/location/d/a$a;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Lcom/google/android/location/d/a$a;)V

    iget-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p2}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;Landroid/location/GpsStatus$Listener;)V
    .locals 1

    sget-object v0, Lcom/google/android/location/d/a$a;->d:Lcom/google/android/location/d/a$a;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Lcom/google/android/location/d/a$a;)V

    iget-object v0, p0, Lcom/google/android/location/d/a;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p2}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    return-void
.end method
