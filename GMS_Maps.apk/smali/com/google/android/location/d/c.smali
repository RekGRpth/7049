.class public Lcom/google/android/location/d/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/location/d/c;


# instance fields
.field private b:Lcom/google/android/location/h/h;


# direct methods
.method private constructor <init>(Lcom/google/android/location/h/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/d/c;->b:Lcom/google/android/location/h/h;

    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/location/d/c;
    .locals 3

    const-class v1, Lcom/google/android/location/d/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Please call init() before calling getInstance."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/google/android/location/h/h$a;)V
    .locals 3

    const-class v1, Lcom/google/android/location/d/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    new-instance v0, Lcom/google/android/location/h/h$a;

    invoke-direct {v0}, Lcom/google/android/location/h/h$a;-><init>()V

    invoke-static {p1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h$a;)V

    new-instance v0, Lcom/google/android/location/d/c;

    invoke-static {}, Lcom/google/android/location/h/h;->g()Lcom/google/android/location/h/h;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/location/d/c;-><init>(Lcom/google/android/location/h/h;)V

    sput-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/d/c;->b:Lcom/google/android/location/h/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
