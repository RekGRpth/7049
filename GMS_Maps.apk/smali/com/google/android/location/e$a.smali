.class final Lcom/google/android/location/e$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/os/g;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/os/g;",
            "Lcom/google/android/location/e/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/os/g;",
            "Lcom/google/android/location/e/E;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(III)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e$a;->a:J

    iput p1, p0, Lcom/google/android/location/e$a;->e:I

    iput p2, p0, Lcom/google/android/location/e$a;->f:I

    iput p3, p0, Lcom/google/android/location/e$a;->g:I

    return-void
.end method

.method private a(J)I
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-ltz v0, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private a(ZIILcom/google/android/location/os/g;)I
    .locals 3

    const/16 v1, 0x9

    if-ge p2, p3, :cond_0

    const/16 v0, 0x11

    :goto_0
    return v0

    :cond_0
    if-ne p2, p3, :cond_2

    if-eqz p1, :cond_1

    const/16 v0, 0x13

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_5

    if-nez p4, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    const/16 v2, 0x19

    invoke-static {v0, p4, v2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xa

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private a(I)V
    .locals 2

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    iget-object v1, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/location/os/g;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    goto :goto_0
.end method

.method public a(ZJJZLcom/google/android/location/os/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 12

    move-wide/from16 v0, p4

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/e$a;->a(J)I

    move-result v11

    const/4 v2, -0x1

    if-ne v11, v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v10, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v10, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x0

    move v9, v2

    :goto_1
    iget-object v2, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/os/g;

    iget-object v2, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/e/e;

    iget-object v2, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/location/e/E;

    move-object/from16 v0, p7

    invoke-direct {p0, p1, v9, v11, v0}, Lcom/google/android/location/e$a;->a(ZIILcom/google/android/location/os/g;)I

    move-result v8

    move-wide v2, p2

    move/from16 v7, p6

    invoke-static/range {v2 .. v8}, Lcom/google/android/location/a;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v10, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_1

    :cond_1
    move-object v2, v10

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .locals 11

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/e$a;->e:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/e$a;->a(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    iget-object v1, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    add-int/lit8 v3, v2, -0x2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v3

    invoke-interface {v1}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-interface {p1}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v5

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v7

    sub-long/2addr v5, v7

    add-long v7, v3, v5

    iget v1, p0, Lcom/google/android/location/e$a;->f:I

    add-int/lit16 v1, v1, 0x1f4

    int-to-long v9, v1

    cmp-long v1, v7, v9

    if-lez v1, :cond_3

    iget v1, p0, Lcom/google/android/location/e$a;->g:I

    int-to-long v7, v1

    cmp-long v1, v3, v7

    if-ltz v1, :cond_4

    iget v1, p0, Lcom/google/android/location/e$a;->g:I

    int-to-long v3, v1

    cmp-long v1, v5, v3

    if-ltz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    add-int/lit8 v0, v2, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/e$a;->a(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(JLcom/google/android/location/os/g;Lcom/google/android/location/a$b;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)Z
    .locals 8

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-eqz p6, :cond_6

    invoke-static {p6, p3}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/location/e$a;->a:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x2ee0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-wide/32 v4, 0x927c0

    const-wide v6, 0x3fd3333333333333L

    move-object v0, p4

    move-object v1, p6

    move-wide v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/E;JJD)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    if-eqz p5, :cond_7

    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-wide v1, p0, Lcom/google/android/location/e$a;->a:J

    sub-long v1, p1, v1

    const-wide/16 v3, 0x2ee0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_7

    if-nez v0, :cond_1

    invoke-virtual {p4, p5, p3}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v1

    if-nez v1, :cond_7

    :cond_1
    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_8

    :goto_3
    if-eqz v0, :cond_9

    :goto_4
    if-eqz p5, :cond_2

    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, p3, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    if-eqz p6, :cond_3

    iget-object v1, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v1, p3, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    if-nez p6, :cond_4

    if-eqz p5, :cond_5

    :cond_4
    iput-wide p1, p0, Lcom/google/android/location/e$a;->a:J

    :cond_5
    invoke-virtual {p4, p3, p5, p6}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    :cond_8
    const/4 p5, 0x0

    goto :goto_3

    :cond_9
    const/4 p6, 0x0

    goto :goto_4
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e$a;->a:J

    return-void
.end method
