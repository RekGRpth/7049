.class public Lcom/google/android/location/e/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/j;


# instance fields
.field private final a:Lcom/google/android/location/e/z;

.field private final b:Lcom/google/android/location/e/z;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/z;Lcom/google/android/location/e/z;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/e/a;->a:Lcom/google/android/location/e/z;

    iput-object p2, p0, Lcom/google/android/location/e/a;->b:Lcom/google/android/location/e/z;

    iput p3, p0, Lcom/google/android/location/e/a;->c:I

    return-void
.end method

.method private a(Ljava/util/Map;)Lcom/google/android/location/e/j$a;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/e/j$a;"
        }
    .end annotation

    const/4 v10, 0x1

    iget-object v0, p0, Lcom/google/android/location/e/a;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/z;->a(Ljava/util/Map;)[F

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/a;->b:Lcom/google/android/location/e/z;

    invoke-virtual {v1, p1}, Lcom/google/android/location/e/z;->a(Ljava/util/Map;)[F

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/location/f/d;->a([F)Lcom/google/android/location/f/d$a;

    move-result-object v8

    invoke-static {v1}, Lcom/google/android/location/f/d;->a([F)Lcom/google/android/location/f/d$a;

    move-result-object v9

    iget v0, v8, Lcom/google/android/location/f/d$a;->a:F

    float-to-double v0, v0

    iget v2, v9, Lcom/google/android/location/f/d$a;->a:F

    float-to-double v2, v2

    iget v4, v8, Lcom/google/android/location/f/d$a;->a:F

    iget v5, v8, Lcom/google/android/location/f/d$a;->b:F

    add-float/2addr v4, v5

    float-to-double v4, v4

    iget v6, v9, Lcom/google/android/location/f/d$a;->a:F

    iget v7, v9, Lcom/google/android/location/f/d$a;->b:F

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/e/i;->a(DDDD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/e/a;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v2}, Lcom/google/android/location/e/z;->a()I

    move-result v2

    if-eq v2, v10, :cond_0

    iget-object v2, p0, Lcom/google/android/location/e/a;->b:Lcom/google/android/location/e/z;

    invoke-virtual {v2}, Lcom/google/android/location/e/z;->a()I

    move-result v2

    if-ne v2, v10, :cond_1

    :cond_0
    const v0, 0x469c4000

    :goto_0
    new-instance v1, Lcom/google/android/location/e/j$a;

    iget v2, v8, Lcom/google/android/location/f/d$a;->a:F

    float-to-int v2, v2

    iget v3, v9, Lcom/google/android/location/f/d$a;->a:F

    float-to-int v3, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/location/e/j$a;-><init>(III)V

    return-object v1

    :cond_1
    const/high16 v2, 0x40000000

    double-to-float v0, v0

    mul-float/2addr v0, v2

    const/high16 v1, 0x447a0000

    mul-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/e/a;->c:I

    return v0
.end method

.method public a(Ljava/util/List;)Lcom/google/android/location/e/j$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Lcom/google/android/location/e/j$a;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/location/e/a;->a(Ljava/util/Map;)Lcom/google/android/location/e/j$a;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/e/a;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/e/a;

    iget v2, p0, Lcom/google/android/location/e/a;->c:I

    iget v3, p1, Lcom/google/android/location/e/a;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/location/e/a;->a:Lcom/google/android/location/e/z;

    iget-object v3, p1, Lcom/google/android/location/e/a;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/e/a;->b:Lcom/google/android/location/e/z;

    iget-object v3, p1, Lcom/google/android/location/e/a;->b:Lcom/google/android/location/e/z;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/location/e/a;->c:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/e/a;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v1}, Lcom/google/android/location/e/z;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/e/a;->b:Lcom/google/android/location/e/z;

    invoke-virtual {v1}, Lcom/google/android/location/e/z;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
