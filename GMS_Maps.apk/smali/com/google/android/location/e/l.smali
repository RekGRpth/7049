.class public Lcom/google/android/location/e/l;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/l$a;
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/e/z;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/z;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/z;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)Lcom/google/android/location/e/l$a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/e/l$a;"
        }
    .end annotation

    const/high16 v2, 0x3f800000

    iget-object v0, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Lcom/google/android/location/e/l$a;

    invoke-direct {v1}, Lcom/google/android/location/e/l$a;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/location/e/l$a;->a(Lcom/google/android/location/e/l$a;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/location/e/l$a;->a(Lcom/google/android/location/e/l$a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/e/l$a;->a(Ljava/lang/String;F)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/location/e/l$a;

    invoke-direct {v1}, Lcom/google/android/location/e/l$a;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/z;->a(Ljava/util/Map;)[F

    move-result-object v3

    array-length v0, v3

    int-to-float v0, v0

    div-float v4, v2, v0

    array-length v5, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_2

    aget v0, v3, v2

    iget-object v6, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v0, v4}, Lcom/google/android/location/e/l$a;->a(Ljava/lang/String;F)V

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/location/e/l$a;->c()V

    move-object v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/e/l;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/e/l;

    iget-object v2, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    iget-object v3, p1, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v1}, Lcom/google/android/location/e/z;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
