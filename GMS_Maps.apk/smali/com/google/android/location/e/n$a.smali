.class public final enum Lcom/google/android/location/e/n$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/e/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/e/n$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/e/n$a;

.field public static final enum b:Lcom/google/android/location/e/n$a;

.field public static final enum c:Lcom/google/android/location/e/n$a;

.field public static final enum d:Lcom/google/android/location/e/n$a;

.field private static final synthetic f:[Lcom/google/android/location/e/n$a;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/location/e/n$a;

    const-string v1, "Requests"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/location/e/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/location/e/n$a;->a:Lcom/google/android/location/e/n$a;

    new-instance v0, Lcom/google/android/location/e/n$a;

    const-string v1, "Results"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/e/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/location/e/n$a;->b:Lcom/google/android/location/e/n$a;

    new-instance v0, Lcom/google/android/location/e/n$a;

    const-string v1, "FromCachedData"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/location/e/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/location/e/n$a;->c:Lcom/google/android/location/e/n$a;

    new-instance v0, Lcom/google/android/location/e/n$a;

    const-string v1, "Jumps"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/location/e/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/location/e/n$a;->d:Lcom/google/android/location/e/n$a;

    new-array v0, v6, [Lcom/google/android/location/e/n$a;

    sget-object v1, Lcom/google/android/location/e/n$a;->a:Lcom/google/android/location/e/n$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/location/e/n$a;->b:Lcom/google/android/location/e/n$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/e/n$a;->c:Lcom/google/android/location/e/n$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/e/n$a;->d:Lcom/google/android/location/e/n$a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/location/e/n$a;->f:[Lcom/google/android/location/e/n$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/location/e/n$a;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/e/n$a;
    .locals 1

    const-class v0, Lcom/google/android/location/e/n$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/n$a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/e/n$a;
    .locals 1

    sget-object v0, Lcom/google/android/location/e/n$a;->f:[Lcom/google/android/location/e/n$a;

    invoke-virtual {v0}, [Lcom/google/android/location/e/n$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/e/n$a;

    return-object v0
.end method
