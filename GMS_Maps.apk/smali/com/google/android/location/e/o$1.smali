.class final Lcom/google/android/location/e/o$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/e/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/e/v",
        "<",
        "Lcom/google/android/location/e/o;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataInput;)Lcom/google/android/location/e/o;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/google/android/location/e/o$a;->values()[Lcom/google/android/location/e/o$a;

    move-result-object v1

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v1, v2, :cond_0

    sget-object v0, Lcom/google/android/location/e/w;->h:Lcom/google/android/location/e/v;

    invoke-interface {v0, p1}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    :cond_0
    new-instance v2, Lcom/google/android/location/e/o;

    invoke-interface {p1}, Ljava/io/DataInput;->readLong()J

    move-result-wide v3

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    return-object v2

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid status"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/google/android/location/e/o;Ljava/io/DataOutput;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    invoke-virtual {v0}, Lcom/google/android/location/e/o$a;->ordinal()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v1, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/location/e/w;->h:Lcom/google/android/location/e/v;

    iget-object v1, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    invoke-interface {v0, v1, p2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :cond_0
    iget-wide v0, p1, Lcom/google/android/location/e/o;->e:J

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0

    check-cast p1, Lcom/google/android/location/e/o;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/e/o$1;->a(Lcom/google/android/location/e/o;Ljava/io/DataOutput;)V

    return-void
.end method

.method public synthetic b(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/location/e/o$1;->a(Ljava/io/DataInput;)Lcom/google/android/location/e/o;

    move-result-object v0

    return-object v0
.end method
