.class public Lcom/google/android/location/e/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/e/o;

.field public final b:Lcom/google/android/location/e/D;

.field public final c:Lcom/google/android/location/e/c;

.field public final d:Lcom/google/android/location/e/g;


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/o;Lcom/google/android/location/e/D;Lcom/google/android/location/e/c;Lcom/google/android/location/e/g;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iput-object p2, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iput-object p3, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iput-object p4, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v1, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid Args"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v0, v0, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    goto :goto_0
.end method

.method public a(Ljava/io/PrintWriter;)V
    .locals 2

    const-string v0, "NetworkLocation [\n bestResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-nez v0, :cond_1

    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v0, "\n wifiResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    invoke-static {p1, v0}, Lcom/google/android/location/e/D;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/D;)V

    const-string v0, "\n cellResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    invoke-static {p1, v0}, Lcom/google/android/location/e/c;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/c;)V

    const-string v0, "\n glsResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    invoke-static {p1, v0}, Lcom/google/android/location/e/g;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/g;)V

    const-string v0, "\n]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v1, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v0, v1, :cond_2

    const-string v0, "WIFI"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v1, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v0, v1, :cond_3

    const-string v0, "CELL"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v1, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-ne v0, v1, :cond_0

    const-string v0, "GLS"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Lcom/google/android/location/e/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iget-object v0, v0, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1388

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "NetworkLocation [\n bestResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-nez v1, :cond_1

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    const-string v1, "\n wifiResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    invoke-static {v0, v1}, Lcom/google/android/location/e/D;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/D;)V

    const-string v1, "\n cellResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    invoke-static {v0, v1}, Lcom/google/android/location/e/c;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/c;)V

    const-string v1, "\n glsResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    invoke-static {v0, v1}, Lcom/google/android/location/e/g;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/g;)V

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v1, v2, :cond_2

    const-string v1, "WIFI"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v1, v2, :cond_3

    const-string v1, "CELL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-ne v1, v2, :cond_0

    const-string v1, "GLS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
