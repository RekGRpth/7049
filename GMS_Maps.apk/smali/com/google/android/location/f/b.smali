.class public Lcom/google/android/location/f/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f/b$a;
    }
.end annotation


# instance fields
.field a:I

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/f/b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/f/b;->a:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/f/b;->a:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b;
    .locals 5

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    new-instance v2, Lcom/google/android/location/f/b;

    invoke-direct {v2, v1}, Lcom/google/android/location/f/b;-><init>(I)V

    invoke-virtual {v2, v0}, Lcom/google/android/location/f/b;->a(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/location/f/b$a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b$a;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public a([F)F
    .locals 3

    iget v0, p0, Lcom/google/android/location/f/b;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not compute value of regression tree. No root of the tree was defined"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/location/f/b;->a:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/b$a;

    invoke-static {v0}, Lcom/google/android/location/f/b$a;->a(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/b$a$a;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v1, v2, :cond_1

    invoke-static {v0}, Lcom/google/android/location/f/b$a;->b(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/f/c;->a([F)F

    move-result v0

    return v0

    :cond_1
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->c(Lcom/google/android/location/f/b$a;)I

    move-result v1

    aget v1, p1, v1

    invoke-static {v0}, Lcom/google/android/location/f/b$a;->d(Lcom/google/android/location/f/b$a;)F

    move-result v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_2

    invoke-static {v0}, Lcom/google/android/location/f/b$a;->e(Lcom/google/android/location/f/b$a;)I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->f(Lcom/google/android/location/f/b$a;)I

    move-result v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/location/f/b;->a:I

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/f/b;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/f/b;

    iget v2, p0, Lcom/google/android/location/f/b;->a:I

    iget v3, p1, Lcom/google/android/location/f/b;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/location/f/b;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Root: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/f/b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    const-string v4, "(%d) %s\n"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/b$a;

    invoke-virtual {v0}, Lcom/google/android/location/f/b$a;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
