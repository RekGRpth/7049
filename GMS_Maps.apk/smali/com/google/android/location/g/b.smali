.class public Lcom/google/android/location/g/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field final d:[F


# direct methods
.method constructor <init>(III[F)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/g/b;->a:I

    iput p3, p0, Lcom/google/android/location/g/b;->c:I

    iput p2, p0, Lcom/google/android/location/g/b;->b:I

    iput-object p4, p0, Lcom/google/android/location/g/b;->d:[F

    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/g/b;
    .locals 8

    const/4 v7, 0x4

    const/4 v3, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    const/4 v3, 0x5

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    new-array v5, v4, [F

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {p0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v6

    aput v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/location/g/b;

    invoke-direct {v1, v2, v3, v0, v5}, Lcom/google/android/location/g/b;-><init>(III[F)V

    return-object v1
.end method
