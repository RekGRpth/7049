.class public Lcom/google/android/location/g/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(D)D
    .locals 4

    const-wide v0, 0x400921fb54442d18L

    mul-double/2addr v0, p0

    const-wide v2, 0x4066800000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(DDDD)D
    .locals 8

    invoke-static {p0, p1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    invoke-static {p4, p5}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v4

    invoke-static {p2, p3}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v2

    invoke-static {p6, p7}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->b(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(I)D
    .locals 4

    int-to-double v0, p0

    const-wide v2, 0x3e7ad7f29abcaf48L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/h;)D
    .locals 2

    iget v0, p0, Lcom/google/android/location/e/h;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D
    .locals 6

    invoke-static {p0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/h;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/h;)D

    move-result-wide v2

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v4

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/w;)D
    .locals 2

    iget v0, p0, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;)D
    .locals 8

    invoke-interface {p0}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v0

    invoke-interface {p0}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)I
    .locals 8

    iget v0, p0, Lcom/google/android/location/e/w$a;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w$a;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I
    .locals 8

    iget v0, p0, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(IILcom/google/android/location/e/w;)Lcom/google/android/location/e/h;
    .locals 12

    const-wide v10, 0x3f847ae147ae147bL

    iget v0, p2, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p2, Lcom/google/android/location/e/w;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    add-double v4, v0, v10

    move-wide v6, v2

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v4

    div-double v8, v10, v4

    add-double v6, v2, v10

    move-wide v4, v0

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v4

    div-double v4, v10, v4

    new-instance v6, Lcom/google/android/location/e/h;

    int-to-double v10, p0

    mul-double v7, v10, v8

    add-double/2addr v0, v7

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->b(D)I

    move-result v0

    int-to-double v7, p1

    mul-double/2addr v4, v7

    add-double v1, v4, v2

    invoke-static {v1, v2}, Lcom/google/android/location/g/c;->b(D)I

    move-result v1

    invoke-direct {v6, v0, v1}, Lcom/google/android/location/e/h;-><init>(II)V

    return-object v6
.end method

.method public static b(DDDD)D
    .locals 8

    sub-double v0, p4, p0

    const-wide/high16 v2, 0x3fe0000000000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    sub-double v2, p6, p2

    const-wide/high16 v4, 0x3fe0000000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v0

    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    invoke-static {p4, p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    mul-double/2addr v4, v2

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L

    :cond_0
    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L

    sub-double v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/h;)D
    .locals 2

    iget v0, p0, Lcom/google/android/location/e/h;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D
    .locals 4

    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/h;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)D
    .locals 8

    iget v0, p0, Lcom/google/android/location/e/w$a;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w$a;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/w;)D
    .locals 2

    iget v0, p0, Lcom/google/android/location/e/w;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .locals 8

    iget v0, p0, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(D)I
    .locals 2

    const-wide v0, 0x416312d000000000L

    mul-double/2addr v0, p0

    double-to-int v0, v0

    return v0
.end method

.method public static b(I)I
    .locals 1

    div-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static c(D)D
    .locals 4

    const-wide v0, -0x4006de04abbbd2e8L

    const-wide v2, 0x3ff921fb54442d18L

    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static c(DDDD)D
    .locals 12

    invoke-static {p2, p3}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    invoke-static/range {p6 .. p7}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v2

    invoke-static {p0, p1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v4

    invoke-static/range {p4 .. p5}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v6

    sub-double v8, v4, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f91df46a2529d39L

    cmpl-double v8, v8, v10

    if-gtz v8, :cond_0

    sub-double v8, v0, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f91df46a2529d39L

    cmpl-double v8, v8, v10

    if-lez v8, :cond_1

    :cond_0
    invoke-static/range {p0 .. p7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_1
    sub-double v8, v4, v6

    sub-double/2addr v0, v2

    add-double v2, v4, v6

    const-wide/high16 v4, 0x4000000000000000L

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v2, v2

    mul-double v4, v8, v8

    mul-double/2addr v2, v0

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    goto :goto_0
.end method

.method public static c(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .locals 6

    invoke-static {p0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v4

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static c(I)I
    .locals 1

    const v0, 0x20c49b

    if-le p0, v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    mul-int/lit16 v0, p0, 0x3e8

    goto :goto_0
.end method

.method public static c(Lcom/google/android/location/e/w;)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/location/e/w;->c:I

    const v2, 0x989680

    if-le v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/location/e/w;->a:I

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/location/e/w;->b:I

    if-eqz v1, :cond_0

    :cond_2
    iget v1, p0, Lcom/google/android/location/e/w;->a:I

    const v2, 0x35a4e900

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/e/w;->a:I

    const v2, -0x35a4e900

    if-lt v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/e/w;->b:I

    const v2, 0x6b49d200

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/e/w;->b:I

    const v2, -0x6b49d200

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(D)D
    .locals 2

    const-wide v0, 0x401921fb54442d18L

    invoke-static {p0, p1, v0, v1}, Ljava/lang/Math;->IEEEremainder(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static d(I)D
    .locals 4

    int-to-double v0, p0

    const-wide v2, 0x416312d000000000L

    div-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static d(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .locals 4

    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static e(D)I
    .locals 4

    const-wide v0, 0x4066800000000000L

    mul-double/2addr v0, p0

    const-wide v2, 0x400921fb54442d18L

    div-double/2addr v0, v2

    const-wide v2, 0x416312d000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method
