.class Lcom/google/android/location/g/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/f$b;,
        Lcom/google/android/location/g/f$a;
    }
.end annotation


# static fields
.field static final a:[[I

.field private static final b:Ljava/util/logging/Logger;

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/google/android/location/g/n;

.field private static final g:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/g/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/location/g/g;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v1, 0x0

    const/16 v5, 0x78

    const-class v0, Lcom/google/android/location/g/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/f;->c:Ljava/util/Set;

    filled-new-array {v5, v5}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    sput-object v0, Lcom/google/android/location/g/f;->a:[[I

    new-instance v0, Lcom/google/android/location/g/o;

    invoke-direct {v0}, Lcom/google/android/location/g/o;-><init>()V

    sput-object v0, Lcom/google/android/location/g/f;->d:Lcom/google/android/location/g/n;

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_0

    sget-object v3, Lcom/google/android/location/g/f;->a:[[I

    aget-object v3, v3, v2

    invoke-static {v2, v0}, Lcom/google/android/location/g/f;->a(II)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/location/g/f$1;

    invoke-direct {v0}, Lcom/google/android/location/g/f$1;-><init>()V

    sput-object v0, Lcom/google/android/location/g/f;->g:Ljava/util/Comparator;

    new-instance v0, Lcom/google/android/location/g/f$2;

    invoke-direct {v0}, Lcom/google/android/location/g/f$2;-><init>()V

    sput-object v0, Lcom/google/android/location/g/f;->h:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/b;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/g/g;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Lcom/google/android/location/g/g;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/g;

    iput-object p1, p0, Lcom/google/android/location/g/f;->e:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/e/C;)D
    .locals 2

    invoke-static {p0}, Lcom/google/android/location/g/f;->b(Lcom/google/android/location/e/C;)D

    move-result-wide v0

    return-wide v0
.end method

.method static a(II)I
    .locals 6

    const-wide/high16 v4, 0x400e000000000000L

    int-to-double v0, p0

    mul-double/2addr v0, v4

    int-to-double v2, p1

    mul-double/2addr v2, v4

    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private a(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/location/e/w;Lcom/google/android/location/g/g;)Lcom/google/android/location/e/u;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/e/w;",
            "Lcom/google/android/location/g/g;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Lcom/google/android/location/e/w;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v0, 0x0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget-object v5, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v6, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    if-ne v5, v6, :cond_0

    new-instance v5, Lcom/google/android/location/e/u;

    invoke-direct {v5, v1, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/google/android/location/g/f;->b(Lcom/google/android/location/e/C;)D

    move-result-wide v0

    add-double/2addr v2, v0

    move-wide v0, v2

    :goto_1
    move-wide v2, v0

    goto :goto_0

    :cond_0
    sget-object v6, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    if-ne v5, v6, :cond_1

    new-instance v5, Lcom/google/android/location/e/u;

    invoke-direct {v5, v1, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-wide v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "No lre nor minK results found. Returning matrixCenter"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/location/e/u;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-object v0

    :cond_3
    sget-object v0, Lcom/google/android/location/g/f;->h:Ljava/util/Comparator;

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-wide/16 v0, 0x0

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v6, v0

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v2, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/location/e/C;

    invoke-static {v2}, Lcom/google/android/location/g/f;->b(Lcom/google/android/location/e/C;)D

    move-result-wide v3

    add-double/2addr v6, v3

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/g/f;->a(Ljava/lang/Long;Lcom/google/android/location/e/C;ILcom/google/android/location/e/w;Lcom/google/android/location/g/g;)V

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    const-wide/high16 v1, 0x4020000000000000L

    cmpg-double v1, v6, v1

    if-gez v1, :cond_5

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    const/4 v8, 0x1

    iget-object v2, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/location/e/C;

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/g/f;->a(Ljava/lang/Long;Lcom/google/android/location/e/C;ILcom/google/android/location/e/w;Lcom/google/android/location/g/g;)V

    move v0, v8

    goto :goto_4

    :cond_5
    move v8, v0

    const-wide v1, 0x3fd999999999999aL

    const-wide/high16 v4, 0x400e000000000000L

    move-object v0, p4

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/g;->a(DLcom/google/android/location/e/w;D)Lcom/google/android/location/e/w$a;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "Not returning location as unable to find dominant circle."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_6
    invoke-direct {p0, v0, v9}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/w$a;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v8, :cond_7

    iget v1, v0, Lcom/google/android/location/e/w$a;->c:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L

    mul-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Lcom/google/android/location/e/w$a;->c:I

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/w;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "Not returning location as no APs within 75 meters of location."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_8
    const-wide v1, 0x3fd3333333333333L

    invoke-static {v6, v7, v1, v2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto/16 :goto_2
.end method

.method private a(Ljava/util/Map;)Lcom/google/android/location/e/w;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v3, v0

    move-wide v8, v0

    move-wide v1, v8

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget v6, v0, Lcom/google/android/location/e/C;->a:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    add-double/2addr v3, v6

    iget v0, v0, Lcom/google/android/location/e/C;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    add-double v0, v1, v6

    move-wide v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v5, v0

    div-double/2addr v3, v5

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v5, v0

    div-double v0, v1, v5

    new-instance v2, Lcom/google/android/location/e/w;

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->b(D)I

    move-result v3

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->b(D)I

    move-result v0

    const v1, 0x124f8

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/location/e/w;-><init>(III)V

    return-object v2
.end method

.method private a(I)Lcom/google/android/location/g/b;
    .locals 4

    const/4 v2, -0x1

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/location/g/b;

    invoke-direct {v0, p1, v2, v2, v1}, Lcom/google/android/location/g/b;-><init>(III[F)V

    iget-object v2, p0, Lcom/google/android/location/g/f;->e:Ljava/util/List;

    sget-object v3, Lcom/google/android/location/g/f;->g:Ljava/util/Comparator;

    invoke-static {v2, v0, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    if-gez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/location/g/f;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/b;

    iget v2, v0, Lcom/google/android/location/g/b;->b:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Lcom/google/android/location/g/f$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/f$a;",
            ">;)",
            "Lcom/google/android/location/g/f$a;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/location/g/f$a;

    invoke-direct {v1}, Lcom/google/android/location/g/f$a;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/f$a;

    invoke-virtual {v1}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private a()Lcom/google/android/location/g/n$a;
    .locals 4

    new-instance v0, Lcom/google/android/location/g/n$a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/g/f;->c:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    return-object v0
.end method

.method private a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private a(Ljava/lang/Long;Lcom/google/android/location/e/C;ILcom/google/android/location/e/w;Lcom/google/android/location/g/g;)V
    .locals 15

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->c(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v4, 0x400e000000000000L

    div-double/2addr v2, v4

    double-to-int v10, v2

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->d(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v4, 0x400e000000000000L

    div-double/2addr v2, v4

    double-to-int v11, v2

    move/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/google/android/location/g/f;->a(I)Lcom/google/android/location/g/b;

    move-result-object v12

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz v12, :cond_0

    iget-object v5, v12, Lcom/google/android/location/g/b;->d:[F

    array-length v5, v5

    if-lez v5, :cond_0

    iget-object v2, v12, Lcom/google/android/location/g/b;->d:[F

    array-length v4, v2

    iget v3, v12, Lcom/google/android/location/g/b;->c:I

    const/4 v2, 0x0

    :cond_0
    const/16 v5, -0x28

    move v9, v5

    :goto_0
    const/16 v5, 0x28

    if-gt v9, v5, :cond_a

    sub-int v5, v9, v10

    if-gez v5, :cond_1

    neg-int v5, v5

    :cond_1
    const/16 v6, -0x28

    move v8, v6

    :goto_1
    const/16 v6, 0x28

    if-gt v8, v6, :cond_9

    sub-int v6, v8, v11

    if-gez v6, :cond_2

    neg-int v6, v6

    :cond_2
    const/16 v7, 0x78

    if-ge v6, v7, :cond_3

    const/16 v7, 0x78

    if-lt v5, v7, :cond_4

    :cond_3
    invoke-static {v5, v6}, Lcom/google/android/location/g/f;->a(II)I

    move-result v6

    :goto_2
    if-eqz v2, :cond_6

    int-to-double v6, v6

    const-wide v13, 0x4052c00000000000L

    cmpg-double v6, v6, v13

    if-gez v6, :cond_5

    const-wide/high16 v6, 0x3fe0000000000000L

    :goto_3
    move-object/from16 v0, p5

    invoke-virtual {v0, v9, v8, v6, v7}, Lcom/google/android/location/g/g;->a(IID)V

    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_1

    :cond_4
    sget-object v7, Lcom/google/android/location/g/f;->a:[[I

    aget-object v7, v7, v5

    aget v6, v7, v6

    goto :goto_2

    :cond_5
    const-wide v6, 0x3fa999999999999aL

    goto :goto_3

    :cond_6
    if-ge v6, v3, :cond_7

    iget-object v6, v12, Lcom/google/android/location/g/b;->d:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    float-to-double v6, v6

    goto :goto_3

    :cond_7
    add-int v7, v3, v4

    if-lt v6, v7, :cond_8

    iget-object v6, v12, Lcom/google/android/location/g/b;->d:[F

    add-int/lit8 v7, v4, -0x1

    aget v6, v6, v7

    float-to-double v6, v6

    goto :goto_3

    :cond_8
    iget-object v7, v12, Lcom/google/android/location/g/b;->d:[F

    sub-int/2addr v6, v3

    aget v6, v7, v6

    float-to-double v6, v6

    goto :goto_3

    :cond_9
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    goto :goto_0

    :cond_a
    return-void
.end method

.method private a(Lcom/google/android/location/e/C;Lcom/google/android/location/e/C;)Z
    .locals 2

    invoke-static {p1, p2}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v0

    const/16 v1, 0xc8

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/C;Lcom/google/android/location/g/f$a;)Z
    .locals 2

    invoke-virtual {p2}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/C;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/C;Lcom/google/android/location/e/C;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/w$a;Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w$a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;>;)Z"
        }
    .end annotation

    const-wide/16 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/C;

    iget v4, v0, Lcom/google/android/location/e/C;->i:I

    const v5, 0x9c40

    if-ge v4, v5, :cond_2

    invoke-static {p1, v0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)D

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    :goto_1
    move-wide v1, v0

    goto :goto_0

    :cond_0
    const-wide v3, 0x408f400000000000L

    mul-double v0, v3, v1

    const-wide v2, 0x3ff3333333333333L

    div-double/2addr v0, v2

    double-to-int v0, v0

    iget v1, p1, Lcom/google/android/location/e/w$a;->c:I

    if-le v0, v1, :cond_1

    iput v0, p1, Lcom/google/android/location/e/w$a;->c:I

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-wide v0, v1

    goto :goto_1
.end method

.method private a(Lcom/google/android/location/e/w;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    invoke-static {p1, v0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide v4, 0x4052c00000000000L

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/google/android/location/e/C;)D
    .locals 4

    iget v0, p0, Lcom/google/android/location/e/C;->i:I

    int-to-double v0, v0

    const-wide v2, 0x408f400000000000L

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4034000000000000L

    div-double v0, v2, v0

    return-wide v0
.end method

.method private b(Ljava/util/Map;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/g/f;->c(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/location/g/f;->d(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method private c(Ljava/util/Map;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/location/g/f;->d:Lcom/google/android/location/g/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/location/g/n;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/google/android/location/g/n$a;->c()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private d(Ljava/util/Map;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    new-instance v2, Lcom/google/android/location/g/f$a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/android/location/g/f$a;-><init>(Lcom/google/android/location/e/u;)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/g/f$a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/C;

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/C;Lcom/google/android/location/g/f$a;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-direct {p0, v5}, Lcom/google/android/location/g/f;->a(Ljava/util/List;)Lcom/google/android/location/g/f$a;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5}, Ljava/util/List;->clear()V

    invoke-interface {v3}, Ljava/util/List;->clear()V

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/location/g/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/g/f$b;-><init>(Lcom/google/android/location/g/f;Lcom/google/android/location/g/f$1;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v4

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    return-object v2
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/g/f;->b(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/g/f;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/google/android/location/g/f;->a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/g;

    invoke-virtual {v1}, Lcom/google/android/location/g/g;->a()V

    invoke-direct {p0, v0}, Lcom/google/android/location/g/f;->a(Ljava/util/Map;)Lcom/google/android/location/e/w;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/g;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/location/g/f;->a(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/location/e/w;Lcom/google/android/location/g/g;)Lcom/google/android/location/e/u;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/g/f;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/w;

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "No location found by lre localizer"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/g/f;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const/16 v1, 0x50

    const-wide v6, 0x3fd3333333333333L

    cmpg-double v2, v4, v6

    if-gtz v2, :cond_3

    const/16 v1, 0x4e

    :cond_3
    new-instance v2, Lcom/google/android/location/g/n$a;

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    move-object v0, v2

    goto :goto_0
.end method
