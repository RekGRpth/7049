.class Lcom/google/android/location/g/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/g$a;
    }
.end annotation


# static fields
.field private static final d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/g/g$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/g/g$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:I

.field private final c:[[D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/location/g/g$1;

    invoke-direct {v0}, Lcom/google/android/location/g/g$1;-><init>()V

    sput-object v0, Lcom/google/android/location/g/g;->d:Ljava/util/Comparator;

    new-instance v0, Lcom/google/android/location/g/g$2;

    invoke-direct {v0}, Lcom/google/android/location/g/g$2;-><init>()V

    sput-object v0, Lcom/google/android/location/g/g;->e:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/g/g;->b:I

    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/g/g;->a:I

    iget v0, p0, Lcom/google/android/location/g/g;->a:I

    iget v1, p0, Lcom/google/android/location/g/g;->a:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, Lcom/google/android/location/g/g;->c:[[D

    invoke-virtual {p0}, Lcom/google/android/location/g/g;->a()V

    return-void
.end method

.method private a(II)Z
    .locals 1

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    if-gt p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    neg-int v0, v0

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    if-gt p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    neg-int v0, v0

    if-lt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 8

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    move v0, v1

    :goto_0
    iget v4, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v0, v4, :cond_1

    move v4, v1

    :goto_1
    iget v5, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v4, v5, :cond_0

    iget-object v5, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v5, v5, v0

    aget-wide v5, v5, v4

    add-double/2addr v5, v2

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v5

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_2
    iget v4, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v0, v4, :cond_3

    move v4, v1

    :goto_3
    iget v5, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v4, v5, :cond_2

    iget-object v5, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v5, v5, v0

    aget-wide v6, v5, v4

    div-double/2addr v6, v2

    aput-wide v6, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method


# virtual methods
.method public a(DLcom/google/android/location/e/w;D)Lcom/google/android/location/e/w$a;
    .locals 11

    invoke-direct {p0}, Lcom/google/android/location/g/g;->b()V

    new-instance v3, Ljava/util/PriorityQueue;

    const/16 v0, 0x64

    sget-object v1, Lcom/google/android/location/g/g;->e:Ljava/util/Comparator;

    invoke-direct {v3, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v1, v0, :cond_3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget v0, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v2, v0, :cond_2

    new-instance v4, Lcom/google/android/location/g/g$a;

    iget-object v0, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v0, v0, v1

    aget-wide v5, v0, v2

    invoke-direct {v4, v1, v2, v5, v6}, Lcom/google/android/location/g/g$a;-><init>(IID)V

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    const/16 v5, 0x64

    if-ge v0, v5, :cond_1

    invoke-virtual {v3, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/g$a;

    invoke-static {v0}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v5

    invoke-static {v4}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v7

    cmpg-double v0, v5, v7

    if-gez v0, :cond_0

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const/16 v0, 0x64

    new-array v0, v0, [Lcom/google/android/location/g/g$a;

    invoke-virtual {v3, v0}, Ljava/util/PriorityQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/g/g$a;

    sget-object v1, Lcom/google/android/location/g/g;->d:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    :goto_3
    array-length v4, v0

    if-ge v1, v4, :cond_4

    cmpg-double v4, v2, p1

    if-gez v4, :cond_4

    aget-object v4, v0, v1

    invoke-static {v4}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v4

    add-double/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    array-length v2, v0

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    const/4 v2, 0x2

    if-lt v1, v2, :cond_6

    add-int/lit8 v2, v1, -0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v2

    add-int/lit8 v4, v1, -0x2

    aget-object v4, v0, v4

    invoke-static {v4}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_6

    add-int/lit8 v2, v1, -0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v3

    add-int/lit8 v2, v1, -0x1

    :goto_4
    if-ltz v2, :cond_5

    aget-object v5, v0, v2

    invoke-static {v5}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v5

    if-eqz v5, :cond_7

    :cond_5
    if-ltz v2, :cond_6

    move v1, v2

    :cond_6
    new-array v3, v1, [D

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v1, :cond_8

    aget-object v5, v0, v2

    invoke-virtual {v5}, Lcom/google/android/location/g/g$a;->b()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, p4

    iget v8, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v8, v8

    mul-double/2addr v8, p4

    sub-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual {v5}, Lcom/google/android/location/g/g$a;->a()I

    move-result v7

    int-to-double v7, v7

    mul-double/2addr v7, p4

    iget v9, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v9, v9

    mul-double/2addr v9, p4

    sub-double/2addr v7, v9

    double-to-int v7, v7

    invoke-static {v6, v7, p3}, Lcom/google/android/location/g/c;->a(IILcom/google/android/location/e/w;)Lcom/google/android/location/e/h;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v5}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v5

    aput-wide v5, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    :cond_8
    new-instance v2, Lcom/google/android/location/g/m;

    invoke-direct {v2}, Lcom/google/android/location/g/m;-><init>()V

    invoke-virtual {v2, v4, v3}, Lcom/google/android/location/g/m;->a(Ljava/util/List;[D)Lcom/google/android/location/e/h;

    move-result-object v5

    if-nez v5, :cond_9

    const/4 v0, 0x0

    :goto_6
    return-object v0

    :cond_9
    invoke-static {v5, p3}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v6, 0x400e000000000000L

    div-double/2addr v2, v6

    iget v4, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v6, v4

    add-double/2addr v2, v6

    double-to-int v6, v2

    invoke-static {v5, p3}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v7, 0x400e000000000000L

    div-double/2addr v2, v7

    iget v4, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v7, v4

    add-double/2addr v2, v7

    double-to-int v7, v2

    const/high16 v3, -0x80000000

    const/4 v2, 0x0

    move v4, v2

    :goto_7
    if-ge v4, v1, :cond_c

    aget-object v2, v0, v4

    invoke-static {v2}, Lcom/google/android/location/g/g$a;->b(Lcom/google/android/location/g/g$a;)I

    move-result v8

    sub-int/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    invoke-static {v2}, Lcom/google/android/location/g/g$a;->c(Lcom/google/android/location/g/g$a;)I

    move-result v2

    sub-int/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/16 v9, 0x78

    if-ge v8, v9, :cond_a

    const/16 v9, 0x78

    if-lt v2, v9, :cond_b

    :cond_a
    invoke-static {v2, v8}, Lcom/google/android/location/g/f;->a(II)I

    move-result v2

    :goto_8
    if-le v2, v3, :cond_d

    :goto_9
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_7

    :cond_b
    sget-object v9, Lcom/google/android/location/g/f;->a:[[I

    aget-object v2, v9, v2

    aget v2, v2, v8

    goto :goto_8

    :cond_c
    const/16 v0, 0x14

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v0, Lcom/google/android/location/e/w$a;

    invoke-direct {v0}, Lcom/google/android/location/e/w$a;-><init>()V

    invoke-virtual {v0, v5}, Lcom/google/android/location/e/w$a;->a(Lcom/google/android/location/e/h;)Lcom/google/android/location/e/w$a;

    mul-int/lit16 v1, v1, 0x3e8

    iput v1, v0, Lcom/google/android/location/e/w$a;->c:I

    goto :goto_6

    :cond_d
    move v2, v3

    goto :goto_9
.end method

.method a()V
    .locals 6

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v0, v2, :cond_1

    move v2, v1

    :goto_1
    iget v3, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v3, v3, v0

    const-wide/high16 v4, 0x3ff0000000000000L

    aput-wide v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(IID)V
    .locals 6

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/g/g;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/location/g/g;->b:I

    add-int/2addr v1, p2

    iget-object v2, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v0, v2, v0

    aget-wide v2, v0, v1

    const-wide/high16 v4, 0x4059000000000000L

    mul-double/2addr v4, p3

    mul-double/2addr v2, v4

    aput-wide v2, v0, v1

    goto :goto_0
.end method
