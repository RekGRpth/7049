.class public Lcom/google/android/location/g/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/l$1;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/os/i;

.field private final b:Lcom/google/android/location/b/e;

.field private final c:Lcom/google/android/location/g/k;

.field private d:Z

.field private e:Lcom/google/android/location/e/s;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/e;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/g/k;

    invoke-direct {v0}, Lcom/google/android/location/g/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/g/l;->c:Lcom/google/android/location/g/k;

    iput-boolean v1, p0, Lcom/google/android/location/g/l;->d:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;

    iput-boolean v1, p0, Lcom/google/android/location/g/l;->f:Z

    iput-object p1, p0, Lcom/google/android/location/g/l;->a:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    return-void
.end method

.method private static a(Lcom/google/android/location/e/s$a;)I
    .locals 3

    sget-object v0, Lcom/google/android/location/g/l$1;->a:[I

    invoke-virtual {p0}, Lcom/google/android/location/e/s$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Programming error: unsupported request type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x3

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Lcom/google/android/location/e/s;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->w:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/g/l;->a(Lcom/google/android/location/e/s$a;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/e/s;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xa

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/android/location/e/l;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    iget-object v2, p0, Lcom/google/android/location/g/l;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/location/b/e;->a(Ljava/lang/String;J)Lcom/google/android/location/e/l;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/g/l;->f:Z

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/location/e/s;->a(Ljava/lang/String;)Lcom/google/android/location/e/s;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/g/l;->a(Lcom/google/android/location/e/s;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/util/Set;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/g/l;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/location/b/e;->a(JJ)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/g/l;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/g/l;->c:Lcom/google/android/location/g/k;

    invoke-virtual {v0}, Lcom/google/android/location/g/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/g/l;->d:Z

    iget-object v0, p0, Lcom/google/android/location/g/l;->c:Lcom/google/android/location/g/k;

    invoke-virtual {v0}, Lcom/google/android/location/g/k;->a()Lcom/google/android/location/e/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;

    iget-object v0, p0, Lcom/google/android/location/g/l;->a:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;

    invoke-static {v1}, Lcom/google/android/location/g/l;->b(Lcom/google/android/location/e/s;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/b/e;->a(J)V

    return-void
.end method

.method declared-synchronized a(Lcom/google/android/location/e/s;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/s;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/g/l;->c:Lcom/google/android/location/g/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/k;->a(Lcom/google/android/location/e/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/g/l;->f:Z

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/b/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/location/e/j;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/e;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    iget-object v2, p0, Lcom/google/android/location/g/l;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/location/b/e;->b(Ljava/lang/String;J)Lcom/google/android/location/e/j;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/g/l;->f:Z

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/location/e/s;->b(Ljava/lang/String;)Lcom/google/android/location/e/s;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/g/l;->a(Lcom/google/android/location/e/s;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/b/e;->b()V

    return-void
.end method

.method public declared-synchronized b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/g/l;->b:Lcom/google/android/location/b/e;

    iget-object v1, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/location/b/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JLcom/google/android/location/e/s;)Z

    move-result v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/g/l;->d:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/g/l;->e:Lcom/google/android/location/e/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
