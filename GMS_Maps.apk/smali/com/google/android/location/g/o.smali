.class Lcom/google/android/location/g/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/o$1;,
        Lcom/google/android/location/g/o$b;,
        Lcom/google/android/location/g/o$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/location/g/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/o;->a:Ljava/util/logging/Logger;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/o;->b:Ljava/util/Set;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(IILcom/google/android/location/e/w;Lcom/google/android/location/e/w;ILcom/google/android/location/e/u;)D
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/location/e/w;",
            "Lcom/google/android/location/e/w;",
            "I",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;)D"
        }
    .end annotation

    mul-int v9, p5, p5

    mul-int v10, p1, p1

    mul-int v11, p2, p2

    invoke-static {p3}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v1

    invoke-static {p3}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v3

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/g/c;->b(DDDD)D

    move-result-wide v1

    double-to-int v1, v1

    mul-int v2, v1, v1

    add-int v3, v9, v10

    sub-int/2addr v3, v11

    int-to-double v3, v3

    mul-int/lit8 v5, p1, 0x2

    mul-int v5, v5, p5

    int-to-double v5, v5

    div-double/2addr v3, v5

    add-int/2addr v2, v10

    int-to-double v5, v2

    mul-int/lit8 v2, p1, 0x2

    mul-int/2addr v1, v2

    int-to-double v1, v1

    mul-double/2addr v1, v3

    sub-double v1, v5, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    return-wide v1
.end method

.method private a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    sub-double/2addr v8, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v8, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    add-double/2addr v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    add-double/2addr v12, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    add-double/2addr v14, v10

    mul-double/2addr v12, v14

    mul-double v14, v8, v8

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    add-double/2addr v1, v10

    invoke-static {v8, v9, v1, v2}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    add-double/2addr v1, v6

    new-instance v3, Lcom/google/android/location/e/u;

    invoke-static {v4, v5}, Lcom/google/android/location/g/c;->c(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v1, v2}, Lcom/google/android/location/g/c;->d(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3
.end method

.method private a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/w;
    .locals 12

    invoke-static {p1, p2}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v6

    iget v0, p1, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v7

    iget v0, p2, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v8

    sub-int v0, v7, v8

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v6, v0, :cond_1

    if-ge v7, v8, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/g/o;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v4

    add-int v0, v6, v8

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget v1, p1, Lcom/google/android/location/e/w;->a:I

    iget v9, p2, Lcom/google/android/location/e/w;->a:I

    iget v2, p1, Lcom/google/android/location/e/w;->b:I

    iget v10, p2, Lcom/google/android/location/e/w;->b:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v11

    const-wide v0, 0x400921fb54442d18L

    add-double/2addr v4, v0

    add-int v0, v6, v7

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object v0, p0

    move v1, v9

    move v2, v10

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v9

    invoke-direct {p0, v11, v9}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;

    move-result-object v10

    add-int v0, v7, v8

    if-le v6, v0, :cond_2

    iget-object v0, v11, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, v11, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, v9, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, v9, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->b(DDDD)D

    move-result-wide v0

    move-wide v1, v0

    :goto_1
    new-instance p1, Lcom/google/android/location/e/w;

    iget-object v0, v10, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->e(D)I

    move-result v3

    iget-object v0, v10, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/location/g/c;->e(D)I

    move-result v0

    double-to-int v1, v1

    invoke-static {v1}, Lcom/google/android/location/g/c;->c(I)I

    move-result v1

    invoke-direct {p1, v3, v0, v1}, Lcom/google/android/location/e/w;-><init>(III)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    move v1, v7

    move v2, v8

    move-object v3, p1

    move-object v4, p2

    move v5, v6

    move-object v6, v10

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/g/o;->a(IILcom/google/android/location/e/w;Lcom/google/android/location/e/w;ILcom/google/android/location/e/u;)D

    move-result-wide v0

    move-wide v1, v0

    goto :goto_1
.end method

.method private a()Lcom/google/android/location/g/n$a;
    .locals 4

    new-instance v0, Lcom/google/android/location/g/n$a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/g/o;->b:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    return-object v0
.end method

.method private a(Ljava/util/Set;Lcom/google/android/location/g/o$b;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;>;",
            "Lcom/google/android/location/g/o$b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/g/o$b;

    invoke-direct {p0, v1, p2}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/g/o$b;Lcom/google/android/location/g/o$b;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/w;Ljava/util/Map;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/w;

    invoke-static {v1, p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v1

    const/16 v4, 0xfa

    if-le v1, v4, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/g/o$b;->a(I)V

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    add-int/lit8 v1, v2, 0x1

    move v3, v1

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/g/o$b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/g/o$b;Lcom/google/android/location/g/o$b;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->a()V

    invoke-virtual {v1}, Lcom/google/android/location/g/o$b;->a()V

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    return-void
.end method

.method private a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;I)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->b()I

    move-result v0

    if-ne v0, p2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/location/g/o$b;Lcom/google/android/location/g/o$b;)Z
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v2

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v0

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    invoke-static {v1}, Lcom/google/android/location/g/c;->b(I)I

    move-result v1

    add-int/2addr v0, v1

    if-gt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .locals 12

    const-wide v8, 0x416312d000000000L

    iget v0, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v0

    iget v2, p2, Lcom/google/android/location/e/w;->a:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v2

    iget v4, p2, Lcom/google/android/location/e/w;->b:I

    int-to-double v4, v4

    div-double/2addr v4, v8

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    int-to-double v6, v6

    div-double/2addr v6, v8

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private b(Ljava/util/List;)Lcom/google/android/location/e/u;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const v1, 0x7fffffff

    const/high16 v0, -0x80000000

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->b()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->b()I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/location/e/u;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private c(Ljava/util/List;)Lcom/google/android/location/e/w;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    const v4, 0x249f0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-direct {p0, v2, v0}, Lcom/google/android/location/g/o;->a(Ljava/util/Set;Lcom/google/android/location/g/o$b;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/location/g/o;->e(Ljava/util/List;)Lcom/google/android/location/e/w;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    :goto_2
    iget v1, v0, Lcom/google/android/location/e/w;->c:I

    if-ge v1, v4, :cond_3

    new-instance v1, Lcom/google/android/location/e/w$a;

    invoke-direct {v1, v0}, Lcom/google/android/location/e/w$a;-><init>(Lcom/google/android/location/e/w;)V

    iput v4, v1, Lcom/google/android/location/e/w$a;->c:I

    invoke-virtual {v1}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v0

    :cond_3
    return-object v0

    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/location/g/o;->d(Ljava/util/List;)Lcom/google/android/location/e/w;

    move-result-object v0

    goto :goto_2
.end method

.method private d(Ljava/util/List;)Lcom/google/android/location/e/w;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/w;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v7, v0

    move-object v8, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    add-int/lit8 v1, v7, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/google/android/location/e/w;

    invoke-direct {p0, v0, v6}, Lcom/google/android/location/g/o;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v4

    iget v1, v0, Lcom/google/android/location/e/w;->a:I

    iget v2, v0, Lcom/google/android/location/e/w;->b:I

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v9

    iget v1, v6, Lcom/google/android/location/e/w;->a:I

    iget v2, v6, Lcom/google/android/location/e/w;->b:I

    iget v0, v6, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v3

    const-wide v10, 0x400921fb54442d18L

    add-double/2addr v4, v10

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v0

    invoke-direct {p0, v9, v0}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;

    move-result-object v0

    if-nez v8, :cond_0

    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move-object v8, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/google/android/location/e/w$a;

    invoke-direct {v1}, Lcom/google/android/location/e/w$a;-><init>()V

    iget-object v0, v8, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/location/g/c;->e(D)I

    move-result v2

    iget-object v0, v8, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->e(D)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/e/w$a;->a(II)Lcom/google/android/location/e/w$a;

    move-result-object v2

    const v0, 0x7fffffff

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-static {v2, v0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)I

    move-result v0

    if-ge v0, v1, :cond_3

    :goto_3
    move v1, v0

    goto :goto_2

    :cond_2
    invoke-static {v1}, Lcom/google/android/location/g/c;->c(I)I

    move-result v0

    iput v0, v2, Lcom/google/android/location/e/w$a;->c:I

    invoke-virtual {v2}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_3
.end method

.method private e(Ljava/util/List;)Lcom/google/android/location/e/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v1

    const/4 v0, 0x1

    move-object v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/w;

    move-result-object v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method a(IIID)Lcom/google/android/location/e/u;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIID)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    int-to-double v0, p3

    const-wide v2, 0x4158554c00000000L

    div-double/2addr v0, v2

    invoke-static {p1}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v2

    invoke-static {p2}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v2, v10

    sub-double/2addr v0, v2

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    add-double/2addr v0, v4

    const-wide v2, 0x400921fb54442d18L

    add-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L

    rem-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L

    sub-double/2addr v0, v2

    new-instance v2, Lcom/google/android/location/e/u;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method public a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/g/o;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget-object v2, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v5, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    if-eq v2, v5, :cond_3

    iget-object v0, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v2, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    if-ne v0, v2, :cond_2

    :cond_3
    move v2, v3

    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/C;

    if-nez v2, :cond_5

    iget-object v7, v1, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v8, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    if-eq v7, v8, :cond_5

    iget-object v1, v1, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v7, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    if-ne v1, v7, :cond_4

    :cond_5
    new-instance v7, Lcom/google/android/location/g/o$b;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-direct {v7, v1, v0}, Lcom/google/android/location/g/o$b;-><init>(Ljava/lang/Long;Lcom/google/android/location/e/w;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    new-instance v0, Lcom/google/android/location/g/o$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/g/o$a;-><init>(Lcom/google/android/location/g/o;Lcom/google/android/location/g/o$1;)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_7
    invoke-direct {p0, v5}, Lcom/google/android/location/g/o;->a(Ljava/util/List;)V

    invoke-direct {p0, v5}, Lcom/google/android/location/g/o;->b(Ljava/util/List;)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    if-eq v1, v2, :cond_9

    move v1, v4

    :goto_3
    if-eqz v1, :cond_8

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v5, v0}, Lcom/google/android/location/g/o;->a(Ljava/util/List;I)V

    :cond_8
    if-nez v1, :cond_7

    invoke-direct {p0, v5}, Lcom/google/android/location/g/o;->c(Ljava/util/List;)Lcom/google/android/location/e/w;

    move-result-object v1

    iget v0, v1, Lcom/google/android/location/e/w;->c:I

    const v2, 0x16e360

    if-le v0, v2, :cond_a

    sget-object v0, Lcom/google/android/location/g/o;->a:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring computed location since accuracy too high: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mm."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/g/o;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v1, v3

    goto :goto_3

    :cond_a
    const/16 v2, 0x50

    invoke-direct {p0, v1, p1}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/w;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_b

    sget-object v0, Lcom/google/android/location/g/o;->a:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not returning location for the following outliers: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_b
    new-instance v0, Lcom/google/android/location/g/n$a;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    goto/16 :goto_0

    :cond_c
    move v2, v4

    goto/16 :goto_1
.end method
