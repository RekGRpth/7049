.class public Lcom/google/android/location/g/p;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/p$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/d/d;

.field private final b:Lcom/google/android/location/g/n;

.field private final c:Lcom/google/android/location/g/n;

.field private final d:Lcom/google/android/location/g/i;

.field private final e:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/location/g/q;

.field private final g:Lcom/google/android/location/g/r;

.field private final h:Lcom/google/android/location/os/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/g/o;Lcom/google/android/location/g/f;Lcom/google/android/location/b/i;Lcom/google/android/location/g/i;Lcom/google/android/location/g/q;Lcom/google/android/location/g/r;Lcom/google/android/location/os/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/g/o;",
            "Lcom/google/android/location/g/f;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/g/i;",
            "Lcom/google/android/location/g/q;",
            "Lcom/google/android/location/g/r;",
            "Lcom/google/android/location/os/c;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/location/d/d;->a()Lcom/google/android/location/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    iput-object p1, p0, Lcom/google/android/location/g/p;->b:Lcom/google/android/location/g/n;

    iput-object p2, p0, Lcom/google/android/location/g/p;->c:Lcom/google/android/location/g/n;

    iput-object p4, p0, Lcom/google/android/location/g/p;->d:Lcom/google/android/location/g/i;

    iput-object p3, p0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    iput-object p5, p0, Lcom/google/android/location/g/p;->f:Lcom/google/android/location/g/q;

    iput-object p6, p0, Lcom/google/android/location/g/p;->g:Lcom/google/android/location/g/r;

    iput-object p7, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    add-int/lit8 v0, v1, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/util/Map;)Lcom/google/android/location/e/C$a;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Lcom/google/android/location/e/C$a;"
        }
    .end annotation

    const/4 v1, -0x1

    sget-object v0, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget-object v0, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    invoke-virtual {v0}, Lcom/google/android/location/e/C$a;->ordinal()I

    move-result v4

    if-le v4, v2, :cond_2

    invoke-static {}, Lcom/google/android/location/e/C$a;->a()Lcom/google/android/location/e/C$a;

    move-result-object v1

    if-ne v0, v1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/e/C$a;->ordinal()I

    move-result v1

    :goto_2
    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-nez v0, :cond_2

    :cond_0
    move-object p1, p2

    :cond_1
    :goto_0
    return-object p1

    :cond_2
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    iget-object v1, p2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    if-le v0, v1, :cond_1

    move-object p1, p2

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/E;)Lcom/google/android/location/e/E;
    .locals 5

    const/4 v1, 0x0

    if-nez p1, :cond_1

    const/4 p1, 0x0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const/4 v2, 0x1

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_5

    iget-object v3, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/location/d/d;->a(Lcom/google/android/location/e/E$a;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_3
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    invoke-virtual {v3, v0}, Lcom/google/android/location/d/d;->a(Lcom/google/android/location/e/E$a;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/google/android/location/e/E;

    iget-wide v3, p1, Lcom/google/android/location/e/E;->a:J

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/location/e/E;-><init>(JLjava/util/ArrayList;)V

    move-object p1, v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method private a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/g/p;->a(Ljava/util/Map;)Lcom/google/android/location/e/C$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/google/android/location/e/C$a;->b:Lcom/google/android/location/e/C$a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/g/p;->b:Lcom/google/android/location/g/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/g/n;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/g/p;->c:Lcom/google/android/location/g/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/g/n;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/location/g/l;Lcom/google/android/location/b/i;Lcom/google/android/location/os/i;)Lcom/google/android/location/g/p;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/g/l;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/os/i;",
            ")",
            "Lcom/google/android/location/g/p;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/location/g/o;

    invoke-direct {v1}, Lcom/google/android/location/g/o;-><init>()V

    invoke-interface {p2}, Lcom/google/android/location/os/i;->o()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/g/h;->a(Ljava/io/InputStream;)Lcom/google/android/location/g/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/g/h;->a()Ljava/util/List;

    move-result-object v0

    new-instance v2, Lcom/google/android/location/g/f;

    invoke-direct {v2, v0}, Lcom/google/android/location/g/f;-><init>(Ljava/util/List;)V

    new-instance v4, Lcom/google/android/location/g/i;

    invoke-direct {v4, p0}, Lcom/google/android/location/g/i;-><init>(Lcom/google/android/location/g/j;)V

    invoke-static {p2}, Lcom/google/android/location/g/q;->a(Lcom/google/android/location/os/i;)Lcom/google/android/location/g/q;

    move-result-object v5

    new-instance v6, Lcom/google/android/location/g/r;

    invoke-direct {v6, p2}, Lcom/google/android/location/g/r;-><init>(Lcom/google/android/location/os/c;)V

    new-instance v0, Lcom/google/android/location/g/p;

    move-object v3, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/g/p;-><init>(Lcom/google/android/location/g/o;Lcom/google/android/location/g/f;Lcom/google/android/location/b/i;Lcom/google/android/location/g/i;Lcom/google/android/location/g/q;Lcom/google/android/location/g/r;Lcom/google/android/location/os/c;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;III)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " hasLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int v1, p2, p3

    sub-int v1, p4, v1

    const-string v2, " noLocation="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " cacheMiss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    return-void
.end method

.method static a(Ljava/util/Collection;D)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/location/e/C;",
            ">;D)Z"
        }
    .end annotation

    const/4 v9, 0x0

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    move v0, v9

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/location/e/C;

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/location/e/C;

    if-eq v8, v6, :cond_2

    iget v0, v8, Lcom/google/android/location/e/w;->a:I

    int-to-double v0, v0

    iget v2, v8, Lcom/google/android/location/e/w;->b:I

    int-to-double v2, v2

    iget v4, v6, Lcom/google/android/location/e/w;->a:I

    int-to-double v4, v4

    iget v6, v6, Lcom/google/android/location/e/w;->b:I

    int-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/e/i;->a(DDDD)D

    move-result-wide v0

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    move v0, v9

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static a(Ljava/util/Set;)Z
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v7, 0x0

    const-wide/16 v0, 0x0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v4

    move-wide v11, v0

    move-wide v1, v11

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide v9, 0xfcffff000000L

    and-long/2addr v5, v9

    if-eqz v3, :cond_0

    move-wide v0, v5

    move v2, v7

    :goto_1
    move v3, v2

    move-wide v11, v0

    move-wide v1, v11

    goto :goto_0

    :cond_0
    cmp-long v0, v5, v1

    if-eqz v0, :cond_1

    move v0, v4

    :goto_2
    if-eqz v0, :cond_3

    :goto_3
    return v4

    :cond_1
    move v0, v7

    goto :goto_2

    :cond_2
    move v4, v7

    goto :goto_3

    :cond_3
    move-wide v11, v1

    move-wide v0, v11

    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/util/List;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/E;",
            ">;",
            "Lcom/google/android/location/e/w;",
            ")",
            "Lcom/google/android/location/e/D;"
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/g/p;->a(Ljava/util/List;Lcom/google/android/location/d/e;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/location/g/p;->a(Ljava/util/Set;Lcom/google/android/location/e/w;)Lcom/google/android/location/g/p$a;

    move-result-object v0

    const/high16 v1, -0x80000000

    iget-object v6, v0, Lcom/google/android/location/g/p$a;->b:Ljava/util/Map;

    iget-object v10, v0, Lcom/google/android/location/g/p$a;->a:Lcom/google/android/location/e/o$a;

    iget-object v0, v0, Lcom/google/android/location/g/p$a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-interface {v9, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E;

    invoke-direct {p0, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/E;)Lcom/google/android/location/e/E;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/location/g/p;->f:Lcom/google/android/location/g/q;

    invoke-virtual {v0, v9}, Lcom/google/android/location/g/q;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/g/p;->g:Lcom/google/android/location/g/r;

    invoke-virtual {v2, v0}, Lcom/google/android/location/g/r;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/g/p;->d:Lcom/google/android/location/g/i;

    invoke-virtual {v2, v7, v0}, Lcom/google/android/location/g/i;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/location/e/D;

    invoke-virtual {v8}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    iget-object v1, v0, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    const/16 v2, 0x7530

    if-ge v1, v2, :cond_2

    :goto_1
    return-object v0

    :cond_1
    move-object v0, v7

    :cond_2
    invoke-virtual {v8}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/location/e/w;->f:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    iget v1, v1, Lcom/google/android/location/e/w;->g:I

    move-object v8, v0

    move v0, v1

    move-object v1, v2

    :goto_2
    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v10, v2, :cond_3

    new-instance v0, Lcom/google/android/location/e/D;

    iget-object v1, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v7

    move-object v2, v10

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-direct {p0, v6, v9}, Lcom/google/android/location/g/p;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v3

    if-nez v3, :cond_5

    :cond_4
    new-instance v0, Lcom/google/android/location/e/D;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v1, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/g/c;->c(Lcom/google/android/location/e/w;)Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v0, Lcom/google/android/location/e/D;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v1, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto :goto_1

    :cond_6
    new-instance v3, Lcom/google/android/location/e/w$a;

    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/location/e/w$a;-><init>(Lcom/google/android/location/e/w;)V

    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->b()I

    move-result v2

    iput v2, v3, Lcom/google/android/location/e/w$a;->d:I

    iput-object v1, v3, Lcom/google/android/location/e/w$a;->f:Ljava/lang/String;

    iput v0, v3, Lcom/google/android/location/e/w$a;->g:I

    new-instance v0, Lcom/google/android/location/e/D;

    invoke-virtual {v3}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move-object v8, v7

    move v0, v1

    move-object v1, v7

    goto :goto_2
.end method

.method a(Ljava/util/Set;Lcom/google/android/location/e/w;)Lcom/google/android/location/g/p$a;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/location/e/w;",
            ")",
            "Lcom/google/android/location/g/p$a;"
        }
    .end annotation

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v2}, Lcom/google/android/location/os/c;->b()J

    move-result-wide v14

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v10, v1

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v1, v9, v14, v15}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;

    move-result-object v17

    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/google/android/location/e/C;

    invoke-virtual {v3}, Lcom/google/android/location/e/C;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v3, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v2, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    if-eq v1, v2, :cond_2

    const/4 v11, 0x1

    if-eqz p2, :cond_d

    move-object/from16 v0, p2

    iget v1, v0, Lcom/google/android/location/e/w;->c:I

    int-to-double v1, v1

    const-wide v4, 0x408f400000000000L

    div-double v18, v1, v4

    iget v1, v3, Lcom/google/android/location/e/C;->a:I

    int-to-double v1, v1

    iget v3, v3, Lcom/google/android/location/e/C;->b:I

    int-to-double v3, v3

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/location/e/w;->a:I

    int-to-double v5, v5

    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/location/e/w;->b:I

    int-to-double v7, v7

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/e/i;->a(DDDD)D

    move-result-wide v1

    cmpl-double v1, v1, v18

    if-lez v1, :cond_d

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v12, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_2
    move v1, v10

    :goto_3
    move v10, v1

    goto :goto_0

    :cond_1
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v13, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/location/e/C;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Lcom/google/android/location/e/C;->c()V

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v10, 0x1

    goto :goto_3

    :cond_4
    if-nez p2, :cond_5

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    const-wide v2, 0x40b3880000000000L

    invoke-static {v1, v2, v3}, Lcom/google/android/location/g/p;->a(Ljava/util/Collection;D)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    :goto_4
    return-object v1

    :cond_5
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_8

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_8

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/g/p;->a(Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_6

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    const-wide v2, 0x40b3880000000000L

    invoke-static {v1, v2, v3}, Lcom/google/android/location/g/p;->a(Ljava/util/Collection;D)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_4

    :cond_6
    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    :cond_7
    :goto_5
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v8

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v1, v3, v14, v15}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    const/4 v2, 0x0

    invoke-virtual {v4}, Lcom/google/android/location/b/a;->c()I

    move-result v4

    invoke-static {}, Lcom/google/android/location/e/C;->b()Lcom/google/android/location/e/C;

    move-result-object v5

    move-wide v6, v14

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/b/i;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    goto :goto_6

    :cond_8
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    goto :goto_5

    :cond_9
    if-lez v8, :cond_b

    const/4 v1, 0x5

    invoke-static {v1, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lt v8, v1, :cond_a

    const-string v1, "Good cache hits. Computing WiFi location locally"

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v12, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_4

    :cond_a
    const-string v1, "Not enough positive cache hits compared to misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_4

    :cond_b
    if-lez v10, :cond_c

    const-string v1, "Too many cache  misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_4

    :cond_c
    const-string v1, "Too many no-location APs. Will not compute a location nor go to the server."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_4

    :cond_d
    move v1, v11

    goto/16 :goto_1
.end method

.method a(Ljava/util/List;Lcom/google/android/location/d/e;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/E;",
            ">;",
            "Lcom/google/android/location/d/e;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/e/E;->a()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v6

    invoke-interface {p2, v6}, Lcom/google/android/location/d/e;->a(Lcom/google/android/location/e/E$a;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    iget-object v1, v6, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, v6, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-interface {v3, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget v6, v6, Lcom/google/android/location/e/E$a;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/location/g/p;->a(Ljava/util/List;)I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    return-object v2
.end method
