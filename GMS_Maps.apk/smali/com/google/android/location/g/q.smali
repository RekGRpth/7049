.class Lcom/google/android/location/g/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:D

.field private final c:D


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0.0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "1.1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "1.5"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "1.6"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "2.0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "2.0"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "2.1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "2.2"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "2.3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "2.3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3.0"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "3.1"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "3.2"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "4.0"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "4.0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "4.1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3ff0000000000000L

    iput-wide v0, p0, Lcom/google/android/location/g/q;->b:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/g/q;->c:D

    return-void
.end method

.method constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/location/g/q;->b:D

    iput-wide p3, p0, Lcom/google/android/location/g/q;->c:D

    return-void
.end method

.method public static a(Lcom/google/android/location/os/i;)Lcom/google/android/location/g/q;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-interface {p0}, Lcom/google/android/location/os/i;->q()Lcom/google/android/location/os/i$a;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/location/os/i$a;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v1, Lcom/google/android/location/os/i$a;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-ge v0, v7, :cond_1

    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    aget-object v0, v0, v5

    iget v3, v1, Lcom/google/android/location/os/i$a;->d:I

    sget-object v4, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_2

    sget-object v0, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    iget v1, v1, Lcom/google/android/location/os/i$a;->d:I

    aget-object v0, v0, v1

    :cond_2
    const-string v1, "%s/%s/%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aget-object v4, v2, v5

    aput-object v4, v3, v5

    aget-object v4, v2, v6

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s/%s/"

    new-array v3, v7, [Ljava/lang/Object;

    aget-object v4, v2, v5

    aput-object v4, v3, v5

    aget-object v2, v2, v6

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "models.txt"

    invoke-interface {p0, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    goto :goto_0

    :cond_3
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {v3, v0, v1}, Lcom/google/android/location/g/q;->a(Ljava/io/BufferedReader;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/g/q;

    move-result-object v0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/io/BufferedReader;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/g/q;
    .locals 17

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    const-wide/high16 v12, 0x3ff0000000000000L

    const-wide/16 v8, 0x0

    const-wide v4, 0x7fefffffffffffffL

    const/4 v0, 0x0

    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1}, Lcom/google/android/location/g/q;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    const/4 v2, 0x1

    :try_start_1
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const/4 v4, 0x2

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpg-double v1, v2, v6

    if-ltz v1, :cond_2

    const-wide/high16 v6, -0x3fa7000000000000L

    cmpg-double v1, v4, v6

    if-ltz v1, :cond_2

    const-wide/high16 v6, 0x4059000000000000L

    cmpl-double v1, v4, v6

    if-lez v1, :cond_3

    :cond_2
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1}, Lcom/google/android/location/g/q;-><init>()V

    move-object v0, v1

    goto :goto_1

    :cond_3
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/location/g/q;-><init>(DD)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1}, Lcom/google/android/location/g/q;-><init>()V

    move-object v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v6, 0x3

    if-lt v3, v6, :cond_0

    const/4 v3, 0x1

    :try_start_3
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-wide v2

    const-wide/16 v15, 0x0

    cmpg-double v15, v10, v15

    if-ltz v15, :cond_0

    const-wide/high16 v15, -0x3fa7000000000000L

    cmpg-double v15, v6, v15

    if-ltz v15, :cond_0

    const-wide/high16 v15, 0x4059000000000000L

    cmpl-double v15, v6, v15

    if-gtz v15, :cond_0

    cmpg-double v15, v2, v4

    if-gez v15, :cond_7

    move-object v0, v1

    move-wide v4, v6

    move-wide v6, v10

    :goto_2
    move-wide v8, v4

    move-wide v12, v6

    move-wide v4, v2

    goto/16 :goto_0

    :catch_1
    move-exception v1

    :cond_5
    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0, v12, v13, v8, v9}, Lcom/google/android/location/g/q;-><init>(DD)V

    goto/16 :goto_1

    :cond_6
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    goto/16 :goto_1

    :catch_2
    move-exception v2

    goto/16 :goto_0

    :cond_7
    move-wide v2, v4

    move-wide v6, v12

    move-wide v4, v8

    goto :goto_2
.end method


# virtual methods
.method public a(I)I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/g/q;->b:D

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/location/g/q;->c:D

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public a(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/g/q;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method
