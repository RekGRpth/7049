.class Lcom/google/android/location/g/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/os/c;

.field private b:J

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/location/os/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/g/r;->a:Lcom/google/android/location/os/c;

    return-void
.end method


# virtual methods
.method a(Ljava/util/Map;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/g/r;->a:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/location/g/r;->b:J

    sub-long v3, v1, v3

    const-wide/16 v5, 0x1f40

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/g/r;->c:Ljava/util/Map;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/g/r;->c:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iput-wide v1, p0, Lcom/google/android/location/g/r;->b:J

    iput-object p1, p0, Lcom/google/android/location/g/r;->c:Ljava/util/Map;

    goto :goto_0
.end method
