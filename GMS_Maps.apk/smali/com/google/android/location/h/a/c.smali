.class public Lcom/google/android/location/h/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/h/a/c$b;,
        Lcom/google/android/location/h/a/c$a;
    }
.end annotation


# instance fields
.field protected a:Z

.field private b:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;

.field private d:Las/c;

.field private e:Lar/d;

.field private f:Lcom/google/googlenav/common/io/g;

.field private g:Ljava/lang/String;

.field private h:[Ljava/lang/Thread;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Las/c;Lar/d;Lcom/google/googlenav/common/io/g;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/location/h/a/c;->d:Las/c;

    iput-object p2, p0, Lcom/google/android/location/h/a/c;->e:Lar/d;

    iput-object p3, p0, Lcom/google/android/location/h/a/c;->f:Lcom/google/googlenav/common/io/g;

    iput-object p4, p0, Lcom/google/android/location/h/a/c;->g:Ljava/lang/String;

    new-array v0, p5, [Ljava/lang/Thread;

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/a/c;)Las/c;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/a/c;->b()Las/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/h/a/c;Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/a/c;->a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/a/c;->f:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/common/io/g;->a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/location/h/a/c$a;)Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/h/a/c;Lcom/google/android/location/h/a/c$a;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c$a;)Z

    move-result v0

    return v0
.end method

.method private b()Las/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/a/c;->d:Las/c;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/a/c;->f:Lcom/google/googlenav/common/io/g;

    return-object v0
.end method

.method private c()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/location/h/a/c;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    monitor-exit v1

    :goto_1
    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/location/h/a/c;->a:Z

    if-nez v2, :cond_1

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/a/c$a;

    iget-object v2, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/Vector;->removeElementAt(I)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v0}, Lcom/google/android/location/h/a/c$a;->run()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lcom/google/android/location/h/a/b;
    .locals 1

    new-instance v0, Lcom/google/android/location/h/a/c$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/h/a/c$a;-><init>(Lcom/google/android/location/h/a/c;Ljava/lang/String;I)V

    return-object v0
.end method

.method public a()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/h/a/c;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/a/c;->a:Z

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/android/location/h/a/c;->e:Lar/d;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/location/h/a/c;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, p0}, Lar/d;->a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/h/a/c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void
.end method
