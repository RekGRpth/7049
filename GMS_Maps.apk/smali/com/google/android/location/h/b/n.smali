.class public abstract Lcom/google/android/location/h/b/n;
.super Lcom/google/android/location/h/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# instance fields
.field protected a:Lcom/google/android/location/h/f;

.field private b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/h/b/a;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/android/location/h/b/n;->a(I)V

    iput p2, p0, Lcom/google/android/location/h/b/n;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/h/f;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/h/b/a;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/h/b/n;->a:Lcom/google/android/location/h/f;

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/location/h/b/n;->a(I)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/h/b/n;->b:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public abstract b_()I
.end method

.method public abstract c_()Ljava/io/InputStream;
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/h/b/n;->b:I

    return v0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/n;->a:Lcom/google/android/location/h/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/b/n;->a:Lcom/google/android/location/h/f;

    invoke-virtual {v0}, Lcom/google/android/location/h/f;->b()V

    :cond_0
    return-void
.end method
