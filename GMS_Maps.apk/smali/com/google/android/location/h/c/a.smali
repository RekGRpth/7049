.class public Lcom/google/android/location/h/c/a;
.super Lcom/google/android/location/h/a;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/location/h/c/a;


# instance fields
.field private b:Z

.field private c:J

.field private d:Z


# direct methods
.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/location/h/a;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/location/h/c/a;->b:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "RequestNewInstallId"

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/h/c/a;->d:Z

    iget-boolean v0, p0, Lcom/google/android/location/h/c/a;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/h/c/a;->b()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/c/a;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/location/h/c/a;->c:J

    return-wide p1
.end method

.method public static declared-synchronized a()Lcom/google/android/location/h/c/a;
    .locals 2

    const-class v1, Lcom/google/android/location/h/c/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/h/c/a;->e:Lcom/google/android/location/h/c/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/h/c/a;

    invoke-direct {v0}, Lcom/google/android/location/h/c/a;-><init>()V

    sput-object v0, Lcom/google/android/location/h/c/a;->e:Lcom/google/android/location/h/c/a;

    :cond_0
    sget-object v0, Lcom/google/android/location/h/c/a;->e:Lcom/google/android/location/h/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/h/c/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/h/c/a;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/c/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/h/c/a;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/h/c/a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/location/h/c/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/h/c/a;->b:Z

    return p1
.end method

.method private d()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "InstallId"

    iget-wide v2, p0, Lcom/google/android/location/h/c/a;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "RequestNewInstallId"

    iget-boolean v2, p0, Lcom/google/android/location/h/c/a;->d:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method


# virtual methods
.method public declared-synchronized addObserver(Lcom/google/googlenav/common/util/Observer;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/Long;

    iget-wide v1, p0, Lcom/google/android/location/h/c/a;->c:J

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    invoke-interface {p1, p0, v0}, Lcom/google/googlenav/common/util/Observer;->update(Lcom/google/googlenav/common/util/Observable;Ljava/lang/Object;)V

    invoke-super {p0, p1}, Lcom/google/android/location/h/a;->addObserver(Lcom/google/googlenav/common/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/h/c/a;->b:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/location/h/h;->g()Lcom/google/android/location/h/h;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/h/c/a;->b:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/h/c/a;->d:Z

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/location/h/c/a;->c:J

    invoke-direct {p0}, Lcom/google/android/location/h/c/a;->d()V

    new-instance v1, Lcom/google/android/location/h/b/j;

    const-string v2, "g:c"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/h/b/j;-><init>(Ljava/lang/String;I)V

    new-instance v2, Lcom/google/android/location/h/c/a$1;

    invoke-direct {v2, p0}, Lcom/google/android/location/h/c/a$1;-><init>(Lcom/google/android/location/h/c/a;)V

    invoke-virtual {v1, v2}, Lcom/google/android/location/h/b/m;->a(Lcom/google/android/location/h/b/m$a;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()J
    .locals 6

    const-wide/16 v4, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/h/c/a;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/h/c/a;->b()V

    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "InstallId"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    :cond_1
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/location/h/c/a;->b()V

    :cond_2
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
