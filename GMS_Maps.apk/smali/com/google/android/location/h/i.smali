.class public Lcom/google/android/location/h/i;
.super Lcom/google/android/location/h/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method private j()V
    .locals 2

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/i;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/location/h/i$1;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/h/i$1;-><init>(Lcom/google/android/location/h/i;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/i;->addObserver(Lcom/google/googlenav/common/util/Observer;)V

    invoke-virtual {p0}, Lcom/google/android/location/h/i;->submitRequest()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/location/h/i;->h()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/location/h/i;->i()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_1
    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0, p1}, Lcom/google/android/location/h/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/io/DataInputStream;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0}, Lcom/google/android/location/h/b;->b()Ljava/io/DataInputStream;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0}, Lcom/google/android/location/h/b;->c()I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0}, Lcom/google/android/location/h/b;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0}, Lcom/google/android/location/h/b;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public getHeaderField(I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0, p1}, Lcom/google/android/location/h/b;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0, p1}, Lcom/google/android/location/h/b;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    invoke-super {p0}, Lcom/google/android/location/h/b;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
