.class public Lcom/google/android/location/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/i$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)F
    .locals 7

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000

    const/4 v6, 0x2

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    :cond_0
    return v1

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_0
    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {p1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/4 v0, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v1

    move v1, v0

    :goto_1
    if-ge v3, v5, :cond_0

    if-nez v3, :cond_3

    add-int/lit8 v0, v5, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v6, v0

    sub-float v0, v2, v0

    :goto_2
    cmpl-float v6, v0, v1

    if-lez v6, :cond_4

    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-int/lit8 v0, v3, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v6, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method a(Ljava/util/List;Z)F
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;Z)F"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    move v1, v0

    :goto_0
    if-ge v3, v4, :cond_1

    if-eqz p2, :cond_0

    const/4 v0, 0x3

    if-lt v3, v0, :cond_3

    :cond_0
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/android/location/i;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)F

    move-result v0

    add-float/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v2

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    const/high16 v0, 0x43b40000

    :goto_2
    return v0

    :cond_2
    int-to-float v0, v1

    div-float v0, v2, v0

    goto :goto_2

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public a(Ljava/util/List;)Lcom/google/android/location/i$a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Lcom/google/android/location/i$a;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/location/i$a;

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->b(Ljava/util/List;)F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/google/android/location/i;->a(Ljava/util/List;Z)F

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->c(Ljava/util/List;)F

    move-result v3

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->d(Ljava/util/List;)F

    move-result v4

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->e(Ljava/util/List;)F

    move-result v5

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->f(Ljava/util/List;)F

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/i$a;-><init>(FFFFFF)V

    return-object v0
.end method

.method b(Ljava/util/List;)F
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    const/4 v5, 0x3

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_1

    if-lt v2, v5, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    add-int/2addr v0, v1

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_1
    int-to-float v0, v1

    add-int/lit8 v1, v3, -0x3

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method c(Ljava/util/List;)F
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    const/4 v8, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x5

    if-ge v2, v0, :cond_0

    const/high16 v0, 0x43b40000

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v2, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v3, 0x2

    div-long v3, v0, v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v6, v0

    cmp-long v0, v6, v3

    if-gtz v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v5, v8}, Lcom/google/android/location/i;->a(Ljava/util/List;Z)F

    move-result v0

    goto :goto_0
.end method

.method d(Ljava/util/List;)F
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    const/4 v7, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x5

    if-ge v1, v0, :cond_0

    const/high16 v0, 0x43b40000

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v2, v0

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v5, v0

    cmp-long v0, v5, v2

    if-lez v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, Lcom/google/android/location/i;->a(Ljava/util/List;Z)F

    move-result v0

    goto :goto_0
.end method

.method e(Ljava/util/List;)F
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    const/4 v13, 0x3

    const/4 v2, 0x0

    const/high16 v12, 0x41900000

    const/4 v11, 0x2

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-gt v8, v13, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    move v7, v2

    move v5, v4

    move v6, v4

    :goto_1
    if-ge v7, v8, :cond_4

    if-lt v7, v13, :cond_5

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    move v3, v4

    :goto_2
    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v9

    if-ge v1, v9, :cond_3

    invoke-virtual {v0, v11, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-virtual {v9, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v9

    cmpl-float v10, v9, v12

    if-lez v10, :cond_2

    sub-float/2addr v9, v12

    add-float/2addr v3, v9

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    cmpl-float v0, v3, v4

    if-lez v0, :cond_5

    add-float v1, v6, v3

    const/high16 v0, 0x3f800000

    add-float/2addr v0, v5

    :goto_3
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v0

    move v6, v1

    goto :goto_1

    :cond_4
    cmpl-float v0, v5, v4

    if-eqz v0, :cond_0

    div-float v4, v6, v5

    goto :goto_0

    :cond_5
    move v0, v5

    move v1, v6

    goto :goto_3
.end method

.method f(Ljava/util/List;)F
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    if-gt v9, v13, :cond_0

    :goto_0
    return v4

    :cond_0
    move v8, v2

    move v6, v2

    move v7, v4

    :goto_1
    if-ge v8, v9, :cond_3

    if-lt v8, v13, :cond_4

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    move v3, v4

    :goto_2
    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    if-ge v1, v5, :cond_2

    invoke-virtual {v0, v12, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    invoke-virtual {v10, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v5

    invoke-virtual {v10, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    const/high16 v11, 0x41900000

    cmpl-float v10, v10, v11

    if-lez v10, :cond_1

    cmpl-float v10, v5, v3

    if-lez v10, :cond_1

    move v3, v5

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    add-float v1, v7, v3

    add-int/lit8 v0, v6, 0x1

    :goto_3
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move v6, v0

    move v7, v1

    goto :goto_1

    :cond_3
    int-to-float v0, v6

    div-float v4, v7, v0

    goto :goto_0

    :cond_4
    move v0, v6

    move v1, v7

    goto :goto_3
.end method
