.class public Lcom/google/android/location/i/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/i/b;

.field final b:Lcom/google/android/location/i/c;

.field final c:Lcom/google/android/location/i/c;

.field final d:Lcom/google/android/location/i/c;

.field final e:Lcom/google/android/location/i/c;

.field final f:Lcom/google/android/location/os/i;

.field volatile g:Z


# direct methods
.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/i/a;->g:Z

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-object p1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/location/i/b;->a(J)V

    invoke-virtual {p2, v0, v1}, Lcom/google/android/location/i/b;->b(J)V

    invoke-virtual {p2}, Lcom/google/android/location/i/b;->a()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {p2}, Lcom/google/android/location/i/b;->b()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-virtual {p2}, Lcom/google/android/location/i/b;->c()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    invoke-virtual {p2}, Lcom/google/android/location/i/b;->d()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V
    .locals 7

    new-instance v0, Lcom/google/android/location/i/b;

    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/i/b;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;JJ)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/i/a;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/b;)V

    return-void
.end method

.method private a(Lcom/google/android/location/i/c;JZ)Lcom/google/android/location/i/c$a;
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-object v0, p1

    move-wide v1, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->b(JJZ)Lcom/google/android/location/i/c$a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/location/i/b;->b(J)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v0}, Lcom/google/android/location/i/b;->d()Lcom/google/android/location/i/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/c;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized a(J)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/i/a;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/google/android/location/i/c;->a(JJ)Z

    iget-object v2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/i/b;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/location/i/c$a;J)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2, p3}, Lcom/google/android/location/i/c$a;->a(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/b;->b(J)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->m()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->n()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->o()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->p()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    :cond_3
    return-void
.end method

.method public a(ZZI)V
    .locals 0

    iput-boolean p2, p0, Lcom/google/android/location/i/a;->g:Z

    return-void
.end method

.method public declared-synchronized a(JZ)Z
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/i/a;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-wide v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->a(JJZ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(JZ)Lcom/google/android/location/i/c$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c;JZ)Lcom/google/android/location/i/c$a;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {v0}, Lcom/google/android/location/i/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-virtual {v0}, Lcom/google/android/location/i/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    invoke-virtual {v0}, Lcom/google/android/location/i/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    if-nez v3, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz v1, :cond_2

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    if-eqz v2, :cond_3

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    if-eqz v3, :cond_0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public b(J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v2}, Lcom/google/android/location/i/b;->c()Lcom/google/android/location/i/c;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/google/android/location/i/c;->a(JJ)Z

    iget-object v2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/i/b;->b(J)V

    return-void
.end method

.method public c(JZ)Lcom/google/android/location/i/c$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c;JZ)Lcom/google/android/location/i/c$a;

    move-result-object v0

    return-object v0
.end method

.method public d(JZ)Z
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v0}, Lcom/google/android/location/i/b;->c()Lcom/google/android/location/i/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-wide v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->a(JJZ)Z

    move-result v0

    return v0
.end method

.method public e(JZ)Z
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v0}, Lcom/google/android/location/i/b;->d()Lcom/google/android/location/i/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-wide v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->a(JJZ)Z

    move-result v0

    return v0
.end method
