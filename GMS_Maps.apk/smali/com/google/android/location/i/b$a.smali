.class final Lcom/google/android/location/i/b$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/i/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/i/b;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/i/b;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/google/android/location/i/b$a;->b:J

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x0

    new-instance v2, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/os/i;->e()Ljava/io/File;

    move-result-object v1

    const-string v3, "cp_state"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/android/location/os/i;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    iget-wide v3, p0, Lcom/google/android/location/i/b$a;->b:J

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;JLjava/io/ByteArrayOutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v0, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v1

    :goto_1
    iget-object v1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v1, v0}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v0, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    goto :goto_0

    :catch_2
    move-exception v1

    move-object v1, v0

    :goto_3
    iget-object v0, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v0, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_4
    iget-object v2, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v2, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method
