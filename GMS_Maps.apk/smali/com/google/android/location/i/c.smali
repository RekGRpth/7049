.class public Lcom/google/android/location/i/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/i/c$1;,
        Lcom/google/android/location/i/c$a;,
        Lcom/google/android/location/i/c$b;
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:J

.field final c:J

.field d:Lcom/google/android/location/os/h$a;

.field e:J

.field f:J

.field g:Lcom/google/android/location/i/c$b;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/i/c$b;

    invoke-direct {v0}, Lcom/google/android/location/i/c$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/i/c;->g:Lcom/google/android/location/i/c$b;

    iput-object p1, p0, Lcom/google/android/location/i/c;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/location/i/c;->c:J

    iput-object p4, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iput-wide p5, p0, Lcom/google/android/location/i/c;->b:J

    invoke-virtual {p0, p7, p8}, Lcom/google/android/location/i/c;->a(J)V

    return-void
.end method

.method private static a(JJJJ)J
    .locals 2

    add-long v0, p0, p2

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/i/c;J)J
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/c;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(JJ)J
    .locals 5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/i/c;->b:J

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/location/i/c;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    :goto_0
    return-wide p1

    :cond_0
    iget-wide v1, p0, Lcom/google/android/location/i/c;->c:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/location/i/c;->b:J

    add-long/2addr v3, p3

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    :cond_1
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->b:J

    sub-long p1, v0, v2

    goto :goto_0
.end method

.method private b()Z
    .locals 6

    const-wide/32 v4, 0x5265c00

    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/i/c;->c:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(J)V
    .locals 4

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    invoke-direct {p0}, Lcom/google/android/location/i/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/i/c;->b:J

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-wide v1, p0, Lcom/google/android/location/i/c;->c:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/i/c;->f:J

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/i/c;->f:J

    goto :goto_0
.end method

.method private declared-synchronized f(J)J
    .locals 6

    const-wide/16 v0, 0x0

    monitor-enter p0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->a:J

    iget-wide v2, p0, Lcom/google/android/location/i/c;->e:J

    sub-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->e:J

    add-long/2addr v2, v0

    iget-object v4, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v4, v4, Lcom/google/android/location/os/h$a;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/i/c;->g:Lcom/google/android/location/i/c$b;

    invoke-virtual {v0}, Lcom/google/android/location/i/c$b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/c;->e(J)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->d(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 13

    monitor-enter p0

    if-nez p7, :cond_0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v1, 0x2

    :try_start_1
    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v9

    const/4 v1, 0x1

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v11

    iget-wide v5, p0, Lcom/google/android/location/i/c;->b:J

    move-wide/from16 v1, p3

    move-wide/from16 v3, p5

    move-wide v7, p1

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/i/c;->a(JJJJ)J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    add-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-ltz v1, :cond_1

    const-wide/16 v1, -0x1

    cmp-long v1, v9, v1

    if-nez v1, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/c;->e(J)V

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->d(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    :try_start_2
    iget-wide v5, p0, Lcom/google/android/location/i/c;->b:J

    move-wide/from16 v1, p3

    move-wide v3, v9

    move-wide v7, p1

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/i/c;->a(JJJJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    invoke-direct {p0}, Lcom/google/android/location/i/c;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    invoke-direct {p0, v1, v2, p1, p2}, Lcom/google/android/location/i/c;->b(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    :cond_3
    iget-object v1, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v1, v1, Lcom/google/android/location/os/h$a;->a:J

    invoke-static {v1, v2, v11, v12}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method declared-synchronized a(Lcom/google/android/location/os/h$a;J)V
    .locals 4

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0, p2, p3}, Lcom/google/android/location/i/c;->d(J)Z

    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    iget-wide v2, p1, Lcom/google/android/location/os/h$a;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p1, Lcom/google/android/location/os/h$a;->a:J

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    :cond_1
    iput-object p1, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iget-wide v1, p0, Lcom/google/android/location/i/c;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    iget-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JJ)Z
    .locals 5

    const-wide/16 v0, 0x0

    monitor-enter p0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p0, p3, p4}, Lcom/google/android/location/i/c;->d(J)Z

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->c(J)Z

    move-result v0

    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/google/android/location/i/c;->e:J

    sub-long/2addr v3, p1

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JJZ)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p3, p4}, Lcom/google/android/location/i/c;->d(J)Z

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->c(J)Z

    move-result v1

    if-eqz p5, :cond_0

    iget-object v2, p0, Lcom/google/android/location/i/c;->g:Lcom/google/android/location/i/c$b;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/location/i/c$b;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(J)J
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->d(J)Z

    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(JJZ)Lcom/google/android/location/i/c$a;
    .locals 4

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    monitor-enter p0

    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_0
    invoke-virtual {p0, p3, p4}, Lcom/google/android/location/i/c;->d(J)Z

    invoke-virtual/range {p0 .. p5}, Lcom/google/android/location/i/c;->a(JJZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->e:J

    sub-long/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    new-instance v0, Lcom/google/android/location/i/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/location/i/c$a;-><init>(Lcom/google/android/location/i/c;JLcom/google/android/location/i/c$1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c(J)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized d(J)Z
    .locals 10

    const-wide/16 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/location/i/c;->f:J

    iget-wide v4, p0, Lcom/google/android/location/i/c;->f:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->b:J

    iput-wide p1, p0, Lcom/google/android/location/i/c;->f:J

    :cond_0
    :goto_0
    iget-wide v4, p0, Lcom/google/android/location/i/c;->e:J

    iget-wide v6, p0, Lcom/google/android/location/i/c;->e:J

    add-long/2addr v0, v6

    iget-object v6, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v6, v6, Lcom/google/android/location/os/h$a;->a:J

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/location/i/c;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    :cond_2
    monitor-exit p0

    return v0

    :cond_3
    :try_start_1
    iget-wide v4, p0, Lcom/google/android/location/i/c;->f:J

    sub-long v4, p1, v4

    iget-object v6, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v6, v6, Lcom/google/android/location/os/h$a;->c:J

    div-long/2addr v4, v6

    cmp-long v6, v4, v0

    if-ltz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->b:J

    mul-long/2addr v0, v4

    iget-wide v6, p0, Lcom/google/android/location/i/c;->f:J

    iget-object v8, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v8, v8, Lcom/google/android/location/os/h$a;->c:J

    mul-long/2addr v4, v8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/location/i/c;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 9

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const-string v1, "%s - current tokens: %d, last refill: %s, params: %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/i/c;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/location/i/c;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lcom/google/android/location/i/c;->b:J

    iget-wide v7, p0, Lcom/google/android/location/i/c;->f:J

    add-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
