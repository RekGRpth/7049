.class Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;
.super Lcom/google/android/location/internal/INetworkLocationInternal$Stub;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

.field private final b:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

.field private final c:Landroid/content/pm/PackageManager;


# direct methods
.method constructor <init>(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-direct {p0}, Lcom/google/android/location/internal/INetworkLocationInternal$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {p1}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Landroid/app/Service;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Service;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->c:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Landroid/app/Service;

    move-result-object v0

    const-string v3, "activity"

    invoke-virtual {v0, v3}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v2, v4, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private b(Landroid/location/Location;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x3e80

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    if-eqz p1, :cond_0

    const-string v2, "RMI for "

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-virtual {v2, v1}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->dump(Ljava/io/PrintWriter;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access is restricted to packages signed with the same certificate."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILcom/google/android/location/internal/ILocationListener;I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b()V

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v1}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;

    move-result-object v1

    invoke-virtual {v1, p2, v0, p1, p3}, Lcom/google/android/location/internal/server/c;->a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;II)V

    return-void
.end method

.method public a(Lcom/google/android/location/internal/ILocationListener;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/c;->a(Lcom/google/android/location/internal/ILocationListener;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b()V

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v1}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/internal/server/c;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public a(Landroid/location/Location;)[B
    .locals 4

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/google/android/location/e/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lcom/google/android/location/e/t;

    invoke-virtual {v2, v0}, Lcom/google/android/location/internal/server/c;->a(Lcom/google/android/location/e/t;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->a:Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;

    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;->b(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B

    move-result-object v0

    return-object v0
.end method
