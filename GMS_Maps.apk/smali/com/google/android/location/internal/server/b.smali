.class Lcom/google/android/location/internal/server/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/b$b;,
        Lcom/google/android/location/internal/server/b$a;,
        Lcom/google/android/location/internal/server/b$c;
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/location/internal/server/b$c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/server/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/server/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(I)V
    .locals 4

    const v3, 0x7fffffff

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    iput v3, p0, Lcom/google/android/location/internal/server/b;->e:I

    iput v3, p0, Lcom/google/android/location/internal/server/b;->f:I

    iput p1, p0, Lcom/google/android/location/internal/server/b;->a:I

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/location/Location;Z)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$b;

    iget-boolean v4, v0, Lcom/google/android/location/internal/server/b$b;->d:Z

    if-ne p3, v4, :cond_2

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/b$b;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    :goto_1
    move v2, v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private d()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.location.internal.EXTRA_RELEASE_VERSION"

    iget v2, p0, Lcom/google/android/location/internal/server/b;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private e()V
    .locals 3

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$c;

    iget v2, p0, Lcom/google/android/location/internal/server/b;->e:I

    iget v0, v0, Lcom/google/android/location/internal/server/b$c;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$b;

    iget v2, p0, Lcom/google/android/location/internal/server/b;->e:I

    iget v0, v0, Lcom/google/android/location/internal/server/b$b;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    goto :goto_1

    :cond_1
    return-void
.end method

.method private f()V
    .locals 3

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$a;

    iget v2, p0, Lcom/google/android/location/internal/server/b;->f:I

    iget v0, v0, Lcom/google/android/location/internal/server/b$a;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    return v0
.end method

.method a(Landroid/content/Context;Landroid/location/Location;Landroid/location/Location;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/location/Location;",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$c;

    :try_start_0
    iget-object v6, v0, Lcom/google/android/location/internal/server/b$c;->a:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v6, p2}, Lcom/google/android/location/internal/ILocationListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v0, v0, Lcom/google/android/location/internal/server/b$c;->b:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    move v0, v3

    goto :goto_1

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/location/Location;Z)Z

    move-result v0

    or-int/2addr v0, v1

    invoke-direct {p0, p1, p3, v3}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/location/Location;Z)Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    :cond_1
    return-object v4
.end method

.method a(Landroid/content/Context;Landroid/app/PendingIntent;I)V
    .locals 4

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NLP ActivityPendingIntent client in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/internal/server/b$a;

    invoke-direct {v1, p2, p3, v0}, Lcom/google/android/location/internal/server/b$a;-><init>(Landroid/app/PendingIntent;ILandroid/os/PowerManager$WakeLock;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->f()V

    return-void
.end method

.method a(Landroid/content/Context;Landroid/app/PendingIntent;ILandroid/location/Location;Z)V
    .locals 4

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NLP PendingIntent client in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/internal/server/b$b;

    invoke-direct {v1, p2, p3, v0, p5}, Lcom/google/android/location/internal/server/b$b;-><init>(Landroid/app/PendingIntent;ILandroid/os/PowerManager$WakeLock;Z)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "location"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/internal/server/b$b;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    return-void
.end method

.method a(Landroid/content/Context;Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "activity"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$a;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/b$a;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    :goto_1
    move v2, v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->f()V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method a(Landroid/content/Context;Z)V
    .locals 5

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$c;

    if-eqz p2, :cond_0

    :try_start_0
    iget-object v0, v0, Lcom/google/android/location/internal/server/b$c;->a:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v0}, Lcom/google/android/location/internal/ILocationListener;->onProviderEnabled()V

    :goto_1
    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/google/android/location/internal/server/b$c;->a:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v0}, Lcom/google/android/location/internal/ILocationListener;->onProviderDisabled()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v0, v2

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "providerEnabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    move-object v1, v0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$a;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/b$a;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    move v0, v2

    :goto_4
    move v3, v0

    move-object v0, v1

    goto :goto_3

    :cond_3
    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    :cond_4
    return-void

    :cond_5
    move v0, v3

    goto :goto_4
.end method

.method a(Lcom/google/android/location/internal/ILocationListener;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/location/internal/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    return-void
.end method

.method a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/location/internal/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/internal/server/b$c;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/location/internal/server/b$c;-><init>(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    return-void
.end method

.method b()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    return v0
.end method

.method c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
