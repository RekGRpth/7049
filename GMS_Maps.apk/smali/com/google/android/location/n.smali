.class public Lcom/google/android/location/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/n$1;,
        Lcom/google/android/location/n$a;,
        Lcom/google/android/location/n$b;,
        Lcom/google/android/location/n$c;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/google/android/location/k/b;

.field private final c:J

.field private final d:I

.field private final e:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/k/b;)V
    .locals 8

    const/4 v2, 0x3

    const-wide/32 v3, 0x927c0

    const-wide/32 v5, 0xdbba0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/n;-><init>(Lcom/google/android/location/k/b;IJJZ)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/k/b;IJJZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iput p2, p0, Lcom/google/android/location/n;->d:I

    iput-wide p3, p0, Lcom/google/android/location/n;->c:J

    iput-wide p5, p0, Lcom/google/android/location/n;->e:J

    iput-boolean p7, p0, Lcom/google/android/location/n;->a:Z

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Map;)Ljava/util/Calendar;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;Lcom/google/android/location/k/b;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/k/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/location/k/b;->a(Lcom/google/android/location/k/b;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/n$b;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$b;

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_1
    return-void
.end method

.method private b(Ljava/util/SortedMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Ljava/util/TreeMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->b:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-ne v2, v4, :cond_0

    :cond_1
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_0

    :cond_2
    sget-object v2, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    invoke-interface {v0, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->d:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v2, v4, :cond_0

    :cond_4
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_0

    :cond_5
    sget-object v2, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    invoke-interface {v0, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_6
    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c(Ljava/util/SortedMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/u;

    iget-object v1, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/u;

    iget-object v1, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/n$b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$b;

    iget-wide v2, p0, Lcom/google/android/location/n;->c:J

    const-wide/16 v4, 0x4

    div-long v4, v2, v4

    iget-object v0, v0, Lcom/google/android/location/n$b;->a:Lcom/google/android/location/k/b;

    iget-wide v2, v0, Lcom/google/android/location/k/b;->a:J

    iget-object v7, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v7, v7, Lcom/google/android/location/k/b;->a:J

    cmp-long v2, v2, v7

    if-nez v2, :cond_1

    iget-wide v2, v0, Lcom/google/android/location/k/b;->a:J

    :goto_1
    iget-wide v7, v0, Lcom/google/android/location/k/b;->b:J

    iget-object v9, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v9, v9, Lcom/google/android/location/k/b;->b:J

    cmp-long v7, v7, v9

    if-nez v7, :cond_2

    iget-wide v4, v0, Lcom/google/android/location/k/b;->b:J

    :goto_2
    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/location/k/b;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/n;->a(Ljava/util/List;Lcom/google/android/location/k/b;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-wide v2, v0, Lcom/google/android/location/k/b;->a:J

    add-long/2addr v2, v4

    goto :goto_1

    :cond_2
    iget-wide v7, v0, Lcom/google/android/location/k/b;->b:J

    sub-long v4, v7, v4

    goto :goto_2

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/location/n;->c(Ljava/util/List;)V

    return-object v1
.end method

.method a(Ljava/util/SortedMap;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/n$b;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v8

    invoke-interface {p1}, Ljava/util/SortedMap;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v8}, Lcom/google/android/location/n;->b(Ljava/util/List;)V

    :cond_0
    move-object v0, v8

    :goto_0
    return-object v0

    :cond_1
    const-wide/16 v1, -0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v3, v0

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/location/n$a;

    const-wide/16 v10, -0x1

    cmp-long v0, v1, v10

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-eq v7, v0, :cond_2

    sget-object v0, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v7, v0, :cond_3

    :cond_2
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$a;

    move-object v5, v0

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v3, v0

    goto :goto_1

    :cond_4
    if-eq v7, v5, :cond_3

    invoke-static {v3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v10, v1

    iget-wide v12, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v10, v12

    iget-wide v12, p0, Lcom/google/android/location/n;->e:J

    cmp-long v0, v10, v12

    if-ltz v0, :cond_5

    new-instance v0, Lcom/google/android/location/n$b;

    iget-object v4, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v10, v4, Lcom/google/android/location/k/b;->b:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v12, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v3, v12

    invoke-static {v10, v11, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/n$b;-><init>(JJLcom/google/android/location/n$a;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    sget-object v0, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-eq v7, v0, :cond_6

    sget-object v0, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v7, v0, :cond_7

    :cond_6
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$a;

    move-object v5, v0

    goto :goto_2

    :cond_7
    const-wide/16 v1, -0x1

    const/4 v5, 0x0

    goto :goto_2

    :cond_8
    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-eqz v0, :cond_9

    invoke-interface {p1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Long;

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v6, v1

    iget-wide v9, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v6, v9

    iget-wide v9, p0, Lcom/google/android/location/n;->e:J

    cmp-long v0, v6, v9

    if-ltz v0, :cond_9

    new-instance v0, Lcom/google/android/location/n$b;

    iget-object v4, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v6, v4, Lcom/google/android/location/k/b;->b:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v9, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v3, v9

    invoke-static {v6, v7, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/n$b;-><init>(JJLcom/google/android/location/n$a;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_a

    invoke-direct {p0, v8}, Lcom/google/android/location/n;->b(Ljava/util/List;)V

    :cond_a
    move-object v0, v8

    goto/16 :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/location/n;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {p0, v2, v1, p1}, Lcom/google/android/location/n;->a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/location/n;->a(Ljava/util/TreeMap;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/location/n;->a(Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/location/n;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/TreeMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v1, v0, :cond_8

    const/4 v0, 0x2

    invoke-virtual {p3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v0, v4, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3, p1}, Lcom/google/android/location/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Map;)Ljava/util/Calendar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x7

    if-eq v4, v5, :cond_2

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    invoke-virtual {v4, v0}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_4
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v6, v0, Lcom/google/android/location/k/b;->a:J

    sub-long v6, v4, v6

    iget-wide v8, p0, Lcom/google/android/location/n;->c:J

    rem-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$c;

    invoke-direct {p0, v3}, Lcom/google/android/location/n;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v6

    if-nez v0, :cond_6

    new-instance v7, Lcom/google/android/location/n$c;

    if-eqz v6, :cond_5

    const/4 v0, 0x1

    :goto_2
    const/4 v6, 0x1

    const/4 v8, 0x0

    invoke-direct {v7, p0, v0, v6, v8}, Lcom/google/android/location/n$c;-><init>(Lcom/google/android/location/n;IILcom/google/android/location/n$1;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    if-eqz v6, :cond_7

    invoke-virtual {v0}, Lcom/google/android/location/n$c;->a()V

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/location/n$c;->b()V

    goto :goto_3

    :cond_8
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    invoke-virtual {v2}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$c;

    invoke-virtual {v0}, Lcom/google/android/location/n$c;->c()Lcom/google/android/location/e/u;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/google/android/location/n;->c(Ljava/util/SortedMap;)V

    :cond_a
    return-object v1
.end method

.method a(Ljava/util/TreeMap;)Ljava/util/TreeMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/u;

    iget-object v1, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v6, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    sget-object v0, Lcom/google/android/location/n$a;->f:Lcom/google/android/location/n$a;

    iget v1, p0, Lcom/google/android/location/n;->d:I

    int-to-long v10, v1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_3

    long-to-double v0, v8

    div-double v0, v6, v0

    double-to-float v0, v0

    const v1, 0x3f4ccccd

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    sget-object v0, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    :cond_0
    :goto_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const v1, 0x3e4ccccc

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    sget-object v0, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/location/n$a;->e:Lcom/google/android/location/n$a;

    goto :goto_1

    :cond_3
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_0

    long-to-double v8, v8

    cmpl-double v1, v6, v8

    if-nez v1, :cond_4

    sget-object v0, Lcom/google/android/location/n$a;->b:Lcom/google/android/location/n$a;

    goto :goto_1

    :cond_4
    const-wide/16 v8, 0x0

    cmpl-double v1, v6, v8

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/location/n$a;->d:Lcom/google/android/location/n$a;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    :goto_2
    iget-object v3, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v3, v3, Lcom/google/android/location/k/b;->b:J

    cmp-long v3, v0, v3

    if-gez v3, :cond_7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/n$a;->f:Lcom/google/android/location/n$a;

    invoke-virtual {v2, v3, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget-wide v3, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v0, v3

    goto :goto_2

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_8

    invoke-direct {p0, v2}, Lcom/google/android/location/n;->b(Ljava/util/SortedMap;)V

    :cond_8
    invoke-direct {p0, v2}, Lcom/google/android/location/n;->b(Ljava/util/TreeMap;)V

    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_9

    invoke-direct {p0, v2}, Lcom/google/android/location/n;->b(Ljava/util/SortedMap;)V

    :cond_9
    return-object v2
.end method

.method b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method
