.class public Lcom/google/android/location/o;
.super Lcom/google/android/location/a;
.source "SourceFile"


# instance fields
.field final g:[Lcom/google/android/location/a;

.field h:Lcom/google/android/location/a;

.field final i:Lcom/google/android/location/b/g;

.field final j:Lcom/google/android/location/e/d;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/y;Lcom/google/android/location/u;Lcom/google/android/location/i/a;Lcom/google/android/location/e/d;Lcom/google/android/location/a/b;Lcom/google/android/location/g/l;)V
    .locals 13

    const-string v2, "NetworkCollector"

    new-instance v6, Lcom/google/android/location/a$b;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Lcom/google/android/location/a$b;-><init>(Lcom/google/android/location/b/g;)V

    sget-object v7, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    new-instance v1, Lcom/google/android/location/f;

    iget-object v5, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    invoke-static {p1}, Lcom/google/android/location/f;->a(Lcom/google/android/location/os/i;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/f;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/u;Lcom/google/android/location/g;)V

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/location/o;->j:Lcom/google/android/location/e/d;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v12

    new-instance v2, Lcom/google/android/location/c;

    iget-object v3, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object/from16 v0, p5

    invoke-direct {v2, p1, p2, v0, v3}, Lcom/google/android/location/c;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/location/l;

    iget-object v6, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/location/l;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/g;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/location/e;

    iget-object v6, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/e;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/location/w;

    iget-object v6, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p4

    move-object v8, v1

    move-object/from16 v9, p7

    move-object/from16 v10, p9

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/w;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/location/s;

    iget-object v5, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/s;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/location/a;

    invoke-interface {v12, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/location/a;

    iput-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    return-void
.end method

.method private a(Lcom/google/android/location/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    if-eq v0, p1, :cond_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->a()V

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget-object v5, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    if-eq v4, v5, :cond_2

    invoke-virtual {v4}, Lcom/google/android/location/a;->a()V

    invoke-virtual {v4}, Lcom/google/android/location/a;->b()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-direct {p0, v4}, Lcom/google/android/location/o;->a(Lcom/google/android/location/a;)V

    iput-object v4, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    iget-object v2, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    aget-object v2, v2, v1

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/location/o;->a(Lcom/google/android/location/a;)V

    iget-object v0, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public a(IIZ)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/location/a;->a(IIZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/o;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/google/android/location/e/E;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g;->b(Lcom/google/android/location/e/E;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/o;->j:Lcom/google/android/location/e/d;

    invoke-virtual {v0}, Lcom/google/android/location/e/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/e;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/h;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public a(Z)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public b(Lcom/google/android/location/e/E;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g;->a(Lcom/google/android/location/e/E;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->b(Lcom/google/android/location/e/E;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public b(Z)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->b(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->b()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/google/android/location/a;->c()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method

.method public c(Z)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->c(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    return-void
.end method
