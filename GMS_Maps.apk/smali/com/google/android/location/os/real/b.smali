.class Lcom/google/android/location/os/real/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/l;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/google/android/location/c/l;

.field private final c:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/location/c/l;Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/location/os/real/b;->b:Lcom/google/android/location/c/l;

    const-string v0, "power"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "AsyncCollectorListener"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/b;)Lcom/google/android/location/c/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->b:Lcom/google/android/location/c/l;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/os/real/b;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$10;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$10;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$13;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/os/real/b$13;-><init>(Lcom/google/android/location/os/real/b;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$14;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/os/real/b$14;-><init>(Lcom/google/android/location/os/real/b;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/google/android/location/c/H;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$15;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$15;-><init>(Lcom/google/android/location/os/real/b;Lcom/google/android/location/c/H;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$17;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$17;-><init>(Lcom/google/android/location/os/real/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$16;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$16;-><init>(Lcom/google/android/location/os/real/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/os/real/b$1;-><init>(Lcom/google/android/location/os/real/b;Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/os/real/b$8;-><init>(Lcom/google/android/location/os/real/b;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(ZZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$11;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/os/real/b$11;-><init>(Lcom/google/android/location/os/real/b;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a_(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$18;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$18;-><init>(Lcom/google/android/location/os/real/b;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$4;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$4;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$3;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$3;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$6;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$6;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$5;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$5;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$2;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$2;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$7;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$7;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$9;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$9;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$12;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$12;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
