.class final Lcom/google/android/location/os/real/c$a;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/c;

.field private final b:Landroid/net/wifi/WifiManager;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/c;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$a;->b:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c$a;-><init>(Lcom/google/android/location/os/real/c;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const/16 v4, 0x16

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0, v4, v2, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0, v4, v1, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x9

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_9
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_a
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_b
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_c
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0

    :cond_d
    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/os/real/c$a;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1, v2}, Lcom/google/android/location/os/real/i;->a(JLjava/util/List;)Lcom/google/android/location/os/real/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v2, 0x12

    invoke-static {v1, v2, v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_e
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    const-string v0, "wifi_state"

    const/4 v3, 0x4

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v4, 0x13

    if-eqz v0, :cond_10

    :goto_2
    invoke-static {v3, v4, v1, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    goto/16 :goto_0

    :cond_f
    if-ne v0, v1, :cond_0

    move v0, v2

    goto :goto_1

    :cond_10
    move v1, v2

    goto :goto_2

    :cond_11
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x14

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_12
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v3, 0x18

    invoke-static {p1}, Lcom/google/android/location/os/real/c;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_13

    :goto_3
    invoke-static {v0, v3, v1, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    goto/16 :goto_0

    :cond_13
    move v1, v2

    goto :goto_3

    :cond_14
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x1b

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_0
.end method
