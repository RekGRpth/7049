.class final Lcom/google/android/location/os/real/c$c;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/c;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:Landroid/net/wifi/WifiManager;

.field private e:I

.field private f:Lcom/google/android/location/e/e;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/c;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->c:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->d:Landroid/net/wifi/WifiManager;

    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c$c;-><init>(Lcom/google/android/location/os/real/c;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v3, Lcom/google/android/location/os/d;->b:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v3}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v3

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Lcom/google/android/location/os/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->d(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$e;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->e(Lcom/google/android/location/os/real/c;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->f(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->g(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;

    move-result-object v1

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->l:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->m:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v0, p1, Landroid/os/Message;->arg2:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v4, v0}, Lcom/google/android/location/os/e;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    if-eqz v4, :cond_4

    :goto_4
    invoke-interface {v0, v3, v1}, Lcom/google/android/location/os/a;->a(IZ)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4

    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_5

    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    :cond_5
    const/4 v0, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    iget v4, p0, Lcom/google/android/location/os/real/c$c;->e:I

    invoke-static {v0, v3, v4, v1, v2}, Lcom/google/android/location/os/real/f;->a(Landroid/telephony/TelephonyManager;Landroid/telephony/CellLocation;IJ)Lcom/google/android/location/e/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    :goto_5
    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3, v4, v1, v2}, Lcom/google/android/location/os/real/j;->a(Landroid/telephony/TelephonyManager;J)Lcom/google/android/location/e/e;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1, v0}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v2}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/e;)V

    iget-object v2, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v2}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/e;)V

    :cond_6
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/e;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/e;)V

    goto/16 :goto_0

    :cond_7
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    goto :goto_5

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/telephony/SignalStrength;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    instance-of v1, v1, Lcom/google/android/location/e/b;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    :goto_6
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/os/real/c$c;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->b(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget v3, p0, Lcom/google/android/location/os/real/c$c;->e:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/e/e;->a(JI)Lcom/google/android/location/e/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/e;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    goto :goto_6

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_10
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_11
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->e(Lcom/google/android/location/os/real/c;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    new-instance v2, Lcom/google/android/location/os/real/g;

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c$b;->a()I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/g;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/g;)V

    goto/16 :goto_0

    :pswitch_12
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->e(Lcom/google/android/location/os/real/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    new-instance v2, Lcom/google/android/location/os/real/g;

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c$b;->a()I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/g;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/g;)V

    goto/16 :goto_0

    :pswitch_13
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/os/real/i;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/E;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/E;)V

    goto/16 :goto_0

    :pswitch_14
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_9

    :goto_7
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->d(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->b(Z)V

    goto/16 :goto_0

    :cond_9
    move v1, v2

    goto :goto_7

    :pswitch_15
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "scale"

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "level"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "plugged"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_a

    :goto_8
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v1}, Lcom/google/android/location/os/e;->a(IIZ)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v3, v4, v1}, Lcom/google/android/location/os/a;->a(IIZ)V

    goto/16 :goto_0

    :cond_a
    move v1, v2

    goto :goto_8

    :pswitch_16
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_b

    :goto_9
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->c(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->c(Z)V

    goto/16 :goto_0

    :cond_b
    move v1, v2

    goto :goto_9

    :pswitch_17
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_c

    :goto_a
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->d(Z)V

    goto/16 :goto_0

    :cond_c
    move v1, v2

    goto :goto_a

    :pswitch_18
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_d

    :goto_b
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->e(Z)V

    goto/16 :goto_0

    :cond_d
    move v1, v2

    goto :goto_b

    :pswitch_19
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->k:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->j:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->c:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/google/android/location/os/real/c$c;->d:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v3}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V

    goto/16 :goto_0

    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->p:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/os/h;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/h;)V

    goto/16 :goto_0

    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->O:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/t;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/t;)V

    goto/16 :goto_0

    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/a/n$b;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/a/n$b;)V

    goto/16 :goto_0

    :pswitch_1f
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/clientlib/NlpActivity;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_20
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/a;->a(II)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_21
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v2

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lcom/google/android/location/os/a;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_22
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v2

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/os/i$b;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    invoke-interface {v2, v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_12
        :pswitch_16
        :pswitch_2
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1b
        :pswitch_1a
        :pswitch_1c
        :pswitch_1d
        :pswitch_1f
        :pswitch_20
        :pswitch_1e
        :pswitch_21
        :pswitch_22
    .end packed-switch
.end method
