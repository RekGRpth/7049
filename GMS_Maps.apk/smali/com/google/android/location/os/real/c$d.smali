.class final Lcom/google/android/location/os/real/c$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/c;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/google/android/location/os/real/c;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/os/real/c$d;->a:Lcom/google/android/location/os/real/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/location/os/real/c$d;->b:I

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 5

    const-string v0, "gps"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/j;->a(Landroid/location/Location;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/os/real/c$d;->a:Lcom/google/android/location/os/real/c;

    iget v3, p0, Lcom/google/android/location/os/real/c$d;->b:I

    new-instance v4, Lcom/google/android/location/e/u;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v4, p1, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v2, v3, v4}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
