.class final Lcom/google/android/location/os/real/c$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/location/os/real/c;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/c;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/os/real/c$f;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c$f;-><init>(Lcom/google/android/location/os/real/c;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x1

    const/4 v0, -0x4

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->j(Lcom/google/android/location/os/real/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    new-instance v2, Lcom/google/android/location/os/real/c$c;

    iget-object v3, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/location/os/real/c$c;-><init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V

    invoke-static {v0, v2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;Landroid/os/Handler;)Landroid/os/Handler;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v1, v1, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->j(Lcom/google/android/location/os/real/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v2}, Lcom/google/android/location/os/real/c;->k(Lcom/google/android/location/os/real/c;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    new-instance v3, Lcom/google/android/location/os/real/c$a;

    iget-object v4, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/location/os/real/c$a;-><init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V

    invoke-static {v2, v3}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$a;)Lcom/google/android/location/os/real/c$a;

    iget-object v2, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v3}, Lcom/google/android/location/os/real/c;->l(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->d(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$e;

    move-result-object v1

    const/16 v2, 0x550

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "passive"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v5}, Lcom/google/android/location/os/real/c;->g(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;

    move-result-object v5

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_1
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/location/os/real/c$f;->a:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->a:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/a;->a()V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    move v0, v7

    :goto_0
    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->d(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->c(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->c(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->a(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->d(Z)V

    iget-object v2, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v1

    const-string v3, "wifi"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v3}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$f;->b:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->m(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/h;)V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
