.class Lcom/google/android/location/os/real/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/c$1;,
        Lcom/google/android/location/os/real/c$f;,
        Lcom/google/android/location/os/real/c$a;,
        Lcom/google/android/location/os/real/c$b;,
        Lcom/google/android/location/os/real/c$e;,
        Lcom/google/android/location/os/real/c$d;,
        Lcom/google/android/location/os/real/c$c;
    }
.end annotation


# instance fields
.field private A:Z

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field private final l:Landroid/content/Context;

.field private final m:Lcom/google/android/location/os/e;

.field private final n:Lcom/google/android/location/os/real/c$d;

.field private final o:Lcom/google/android/location/os/real/c$d;

.field private final p:Lcom/google/android/location/os/real/c$e;

.field private final q:Lcom/google/android/location/os/real/c$b;

.field private final r:Lcom/google/android/location/os/real/c$f;

.field private final s:Ljava/lang/Thread;

.field private final t:Landroid/os/PowerManager$WakeLock;

.field private final u:Lcom/google/android/location/os/h;

.field private final v:Ljava/lang/Object;

.field private w:Z

.field private x:Landroid/os/Handler;

.field private y:Lcom/google/android/location/os/real/c$a;

.field private z:Lcom/google/android/location/os/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/os/e;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/android/location/os/real/c;->w:Z

    iput-boolean v1, p0, Lcom/google/android/location/os/real/c;->A:Z

    iput-object p1, p0, Lcom/google/android/location/os/real/c;->l:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/os/real/c;->u:Lcom/google/android/location/os/h;

    iput-object p3, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nlp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_LOCATOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_ACTIVE_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_BURST_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_PASSIVE_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_CACHE_UPDATER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_CALIBRATION_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_S_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_SENSOR_UPLOADER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_ACTIVITY_DETECTION"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_IN_OUT_DOOR_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ALARM_WAKEUP_BURST_COLLECTION_TRIGGER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    new-instance v0, Lcom/google/android/location/os/real/c$d;

    const/16 v1, 0x11

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/c$d;-><init>(Lcom/google/android/location/os/real/c;I)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    new-instance v0, Lcom/google/android/location/os/real/c$d;

    const/16 v1, 0x15

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/c$d;-><init>(Lcom/google/android/location/os/real/c;I)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    new-instance v0, Lcom/google/android/location/os/real/c$e;

    invoke-direct {v0, p0, v3}, Lcom/google/android/location/os/real/c$e;-><init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->p:Lcom/google/android/location/os/real/c$e;

    new-instance v0, Lcom/google/android/location/os/real/c$f;

    invoke-direct {v0, p0, v3}, Lcom/google/android/location/os/real/c$f;-><init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    const-string v2, "NetworkLocationCallbackRunner"

    invoke-direct {v0, v3, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->s:Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/location/os/real/c$b;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    invoke-direct {v1, v0}, Lcom/google/android/location/os/real/c$b;-><init>(Landroid/location/LocationManager;)V

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->q:Lcom/google/android/location/os/real/c$b;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "NetworkLocationCallbackRunner"

    invoke-virtual {v0, v4, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->l:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$a;)Lcom/google/android/location/os/real/c$a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    invoke-static {v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(III)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    invoke-static {v0, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ILjava/lang/Object;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    invoke-static {v0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, -0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    invoke-virtual {v0, v4, v4, v1}, Lcom/google/android/location/os/e;->a(ZZI)V

    invoke-interface {p3, v4, v4, v1}, Lcom/google/android/location/os/a;->a(ZZI)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/location/os/e;->a(ZZI)V

    invoke-interface {p3, v4, v5, v1}, Lcom/google/android/location/os/a;->a(ZZI)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v5, :cond_1

    invoke-virtual {p2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    if-nez v2, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v5, v4, v0}, Lcom/google/android/location/os/e;->a(ZZI)V

    if-nez v2, :cond_5

    :goto_2
    invoke-interface {p3, v5, v4, v1}, Lcom/google/android/location/os/a;->a(ZZI)V

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/os/real/c;->a(III)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/os/real/c;->a(Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/location/os/real/c;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->z:Lcom/google/android/location/os/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->p:Lcom/google/android/location/os/real/c$e;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/os/real/c;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->A:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->q:Lcom/google/android/location/os/real/c$b;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/os/real/c;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/os/real/c;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->u:Lcom/google/android/location/os/h;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->s:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_1

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const/16 v0, 0x20

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(III)V

    return-void
.end method

.method public a(IZ)V
    .locals 2

    const/4 v1, 0x3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/location/os/real/c;->a(III)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .locals 1

    const/16 v0, 0x21

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const/16 v0, 0x1f

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/location/d/a;Ljava/lang/String;Z)V
    .locals 8

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->A:Z

    if-ne v0, p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p3, p0, Lcom/google/android/location/os/real/c;->A:Z

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    if-eqz p3, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    invoke-virtual {p1, p2, v0, v1}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    const-string v2, "gps"

    iget-object v6, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    invoke-virtual {p1, p2, v0, v1}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    const-string v2, "passive"

    iget-object v6, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .locals 1

    const/16 v0, 0x1e

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/location/os/a;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/location/os/real/c;->z:Lcom/google/android/location/os/a;

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->s:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/c$f;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 1

    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const/16 v0, 0x23

    invoke-static {p1, p2}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public a(Z)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v3, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/location/os/real/c;->w:Z

    iget-object v3, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/os/real/c;->l:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    const/4 v4, 0x1

    if-eqz p1, :cond_2

    :goto_1
    const/4 v1, 0x0

    invoke-static {v3, v4, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a(ZLjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const/16 v0, 0x22

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x4

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/location/os/real/c;->a(III)V

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/16 v0, 0x1a

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public b(Z)V
    .locals 3

    const/16 v1, 0x19

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, -0x1

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/location/os/real/c;->a(III)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    return-object v0
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/16 v0, 0x1c

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/16 v0, 0x17

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method
