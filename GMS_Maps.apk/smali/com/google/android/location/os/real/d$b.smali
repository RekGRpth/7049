.class final enum Lcom/google/android/location/os/real/d$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/os/real/d$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/os/real/d$b;

.field public static final enum b:Lcom/google/android/location/os/real/d$b;

.field public static final enum c:Lcom/google/android/location/os/real/d$b;

.field public static final enum d:Lcom/google/android/location/os/real/d$b;

.field private static final synthetic e:[Lcom/google/android/location/os/real/d$b;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/location/os/real/d$b;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/os/real/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/d$b;->a:Lcom/google/android/location/os/real/d$b;

    new-instance v0, Lcom/google/android/location/os/real/d$b;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/os/real/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    new-instance v0, Lcom/google/android/location/os/real/d$b;

    const-string v1, "MODEL_QUERY"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/os/real/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/d$b;->c:Lcom/google/android/location/os/real/d$b;

    new-instance v0, Lcom/google/android/location/os/real/d$b;

    const-string v1, "DEVICE_LOCATION_QUERY"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/os/real/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/d$b;->d:Lcom/google/android/location/os/real/d$b;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->a:Lcom/google/android/location/os/real/d$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/os/real/d$b;->c:Lcom/google/android/location/os/real/d$b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/os/real/d$b;->d:Lcom/google/android/location/os/real/d$b;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/location/os/real/d$b;->e:[Lcom/google/android/location/os/real/d$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/os/real/d$b;
    .locals 1

    const-class v0, Lcom/google/android/location/os/real/d$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/real/d$b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/os/real/d$b;
    .locals 1

    sget-object v0, Lcom/google/android/location/os/real/d$b;->e:[Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v0}, [Lcom/google/android/location/os/real/d$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/os/real/d$b;

    return-object v0
.end method
