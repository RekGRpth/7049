.class public Lcom/google/android/location/os/real/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static final a(Landroid/telephony/TelephonyManager;)I
    .locals 5

    const/4 v0, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v3, 0x1

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v4

    if-eq v4, v3, :cond_0

    if-ne v4, v1, :cond_2

    :cond_0
    move v0, v3

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eq v4, v2, :cond_3

    const/16 v3, 0x8

    if-eq v4, v3, :cond_3

    const/16 v3, 0x9

    if-eq v4, v3, :cond_3

    const/16 v3, 0xa

    if-eq v4, v3, :cond_3

    const/16 v3, 0xf

    if-ne v4, v3, :cond_4

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    if-eq v4, v0, :cond_5

    const/4 v2, 0x5

    if-eq v4, v2, :cond_5

    const/4 v2, 0x6

    if-eq v4, v2, :cond_5

    const/16 v2, 0xc

    if-eq v4, v2, :cond_5

    const/4 v2, 0x7

    if-ne v4, v2, :cond_6

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    const/16 v1, 0xd

    if-eq v4, v1, :cond_1

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/telephony/TelephonyManager;Landroid/telephony/CellLocation;IJ)Lcom/google/android/location/e/e;
    .locals 20

    const/4 v10, -0x1

    invoke-static/range {p0 .. p0}, Lcom/google/android/location/os/real/f;->a(Landroid/telephony/TelephonyManager;)I

    move-result v2

    const/4 v9, -0x1

    const/4 v6, -0x1

    const/4 v4, -0x1

    const/4 v3, -0x1

    const v16, 0x7fffffff

    const v17, 0x7fffffff

    const/4 v7, -0x1

    const/4 v15, -0x1

    const/4 v11, 0x0

    move-object/from16 v0, p1

    instance-of v5, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v5, :cond_3

    check-cast p1, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v6

    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/location/os/real/j;->a(Landroid/telephony/gsm/GsmCellLocation;)I

    move-result v10

    const/4 v5, 0x4

    if-ne v2, v5, :cond_0

    const/high16 v2, 0x10000

    if-lt v6, v2, :cond_2

    const v2, 0xfffffff

    if-gt v6, v2, :cond_2

    const/4 v2, 0x3

    :cond_0
    :goto_0
    move v5, v2

    move v2, v3

    :goto_1
    const/4 v3, 0x2

    if-eq v5, v3, :cond_9

    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    const-string v8, ""

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    const/4 v8, 0x0

    const/4 v12, 0x3

    :try_start_0
    invoke-virtual {v3, v8, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v12, 0x3

    invoke-virtual {v3, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    const-string v8, ""

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    const/4 v8, 0x0

    const/4 v12, 0x3

    :try_start_1
    invoke-virtual {v4, v8, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v12, 0x3

    invoke-virtual {v4, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v15

    move v8, v2

    move v7, v3

    :goto_3
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v2

    sget-object v12, Lcom/google/android/location/e/e;->a:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/NeighboringCellInfo;

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getNetworkType()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    :pswitch_0
    goto :goto_4

    :pswitch_1
    const/4 v12, 0x1

    if-ne v5, v12, :cond_1

    new-instance v12, Lcom/google/android/location/e/e$a;

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v13

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v18

    const/16 v19, -0x1

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v12, v13, v0, v1, v2}, Lcom/google/android/location/e/e$a;-><init>(IIII)V

    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_2
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p1

    instance-of v5, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v5, :cond_a

    check-cast p1, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    move-result v16

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    move-result v17

    const/4 v5, 0x2

    goto/16 :goto_1

    :catch_0
    move-exception v3

    move v3, v4

    goto/16 :goto_2

    :catch_1
    move-exception v4

    move v14, v7

    move v8, v2

    move v7, v3

    goto :goto_3

    :pswitch_2
    const/4 v12, 0x3

    if-ne v5, v12, :cond_1

    new-instance v12, Lcom/google/android/location/e/e$a;

    const/4 v13, -0x1

    const/16 v18, -0x1

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v19

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v12, v13, v0, v1, v2}, Lcom/google/android/location/e/e$a;-><init>(IIII)V

    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v12

    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_7

    const-string v2, ""

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :goto_5
    new-instance v2, Lcom/google/android/location/e/b;

    move-wide/from16 v3, p3

    move/from16 v11, p2

    invoke-direct/range {v2 .. v17}, Lcom/google/android/location/e/b;-><init>(JIIIIIIILjava/util/List;Ljava/lang/String;IIII)V

    const-string v3, "RealCellState"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/location/k/a/a;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_6
    return-object v2

    :cond_7
    move-object v13, v11

    goto :goto_5

    :cond_8
    move v14, v7

    move v8, v2

    move v7, v3

    goto/16 :goto_3

    :cond_9
    move v14, v7

    move v8, v2

    move v7, v4

    goto/16 :goto_3

    :cond_a
    move v5, v2

    move v2, v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
