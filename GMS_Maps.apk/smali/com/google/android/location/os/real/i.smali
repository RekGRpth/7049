.class Lcom/google/android/location/os/real/i;
.super Lcom/google/android/location/e/E;
.source "SourceFile"


# direct methods
.method private constructor <init>(JLjava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/e/E$a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/e/E;-><init>(JLjava/util/ArrayList;)V

    return-void
.end method

.method public static a(JLjava/util/List;)Lcom/google/android/location/os/real/i;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)",
            "Lcom/google/android/location/os/real/i;"
        }
    .end annotation

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v8

    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/net/wifi/ScanResult;

    iget-object v0, v5, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v5, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v1, "[IBSS]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    iget-object v0, v5, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/e/F;->a(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    invoke-virtual {v8, v5}, Lcom/google/android/location/os/real/j;->a(Landroid/net/wifi/ScanResult;)J

    move-result-wide v3

    const-wide/16 v11, 0x0

    cmp-long v0, v3, v11

    if-nez v0, :cond_2

    move v6, v7

    :goto_2
    new-instance v0, Lcom/google/android/location/e/E$a;

    iget v3, v5, Landroid/net/wifi/ScanResult;->level:I

    iget-object v4, v5, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v5, v5, Landroid/net/wifi/ScanResult;->frequency:I

    int-to-short v5, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/E$a;-><init>(JILjava/lang/String;SI)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_1

    :cond_2
    sub-long v3, p0, v3

    long-to-int v6, v3

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/android/location/os/real/i;

    invoke-direct {v0, p0, p1, v9}, Lcom/google/android/location/os/real/i;-><init>(JLjava/util/ArrayList;)V

    return-object v0
.end method
