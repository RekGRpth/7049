.class public Lcom/google/android/location/s;
.super Lcom/google/android/location/a;
.source "SourceFile"


# instance fields
.field A:J

.field B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field C:J

.field D:Lcom/google/android/location/i/a;

.field E:Z

.field private final F:Ljava/util/Random;

.field g:Z

.field h:Z

.field i:Z

.field j:Lcom/google/android/location/e/e;

.field k:Lcom/google/android/location/e/E;

.field l:Z

.field m:Lcom/google/android/location/e/E;

.field n:Lcom/google/android/location/os/g;

.field o:Z

.field p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field q:J

.field r:Z

.field s:Z

.field t:Z

.field u:Z

.field v:Z

.field w:J

.field x:J

.field y:Lcom/google/android/location/i/c$a;

.field z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;)V
    .locals 7

    const-string v1, "PassiveCollector"

    sget-object v6, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/s;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->k:Lcom/google/android/location/e/E;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->l:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->o:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->q:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->r:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->s:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->t:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->u:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->v:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->w:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->x:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->y:Lcom/google/android/location/i/c$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->z:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->A:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->C:J

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/s;->F:Ljava/util/Random;

    iget-object v0, p2, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {p0, v0}, Lcom/google/android/location/s;->a(Lcom/google/android/location/os/h;)V

    iput-object p5, p0, Lcom/google/android/location/s;->D:Lcom/google/android/location/i/a;

    return-void
.end method

.method private b(I)Z
    .locals 13

    const/4 v12, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v8

    iget-object v0, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, v8, v2

    const-wide/32 v10, 0xafc80

    cmp-long v0, v2, v10

    if-gtz v0, :cond_3

    move v0, v7

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    if-eqz v2, :cond_10

    if-eq p1, v12, :cond_0

    const/16 v2, 0x10

    if-eq p1, v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/location/s;->l:Z

    if-eqz v2, :cond_10

    if-eqz v0, :cond_10

    iget-object v2, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-object v3, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    invoke-static {v2, v3}, Lcom/google/android/location/s;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_0
    move v4, v7

    :goto_1
    iget-object v2, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    if-eqz v2, :cond_f

    if-nez v4, :cond_1

    if-eqz v0, :cond_f

    iget-object v2, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    iget-object v3, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    iget-object v6, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    invoke-virtual {v2, v3, v6}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_1
    move v3, v7

    :goto_2
    if-eqz v0, :cond_4

    move v2, v7

    :goto_3
    add-int v6, v1, v2

    if-eqz v4, :cond_5

    move v2, v7

    :goto_4
    add-int/2addr v6, v2

    if-eqz v3, :cond_6

    move v2, v7

    :goto_5
    add-int/2addr v2, v6

    if-ge v2, v12, :cond_7

    :cond_2
    :goto_6
    return v1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move v2, v1

    goto :goto_4

    :cond_6
    move v2, v1

    goto :goto_5

    :cond_7
    if-nez v3, :cond_8

    if-eq p1, v7, :cond_2

    :cond_8
    if-eqz v4, :cond_a

    if-nez v0, :cond_9

    move v1, v7

    :cond_9
    iput-boolean v1, p0, Lcom/google/android/location/s;->l:Z

    iget-object v1, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iput-object v1, p0, Lcom/google/android/location/s;->k:Lcom/google/android/location/e/E;

    :cond_a
    if-eqz v0, :cond_c

    iget-object v2, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    :goto_7
    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    :goto_8
    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    :goto_9
    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v0

    iget-boolean v5, p0, Lcom/google/android/location/s;->h:Z

    move v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/s;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V

    iget-object v1, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_b

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v1, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide v8, p0, Lcom/google/android/location/s;->C:J

    :cond_b
    iget-object v1, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v1, v7

    goto :goto_6

    :cond_c
    move-object v2, v5

    goto :goto_7

    :cond_d
    move-object v3, v5

    goto :goto_8

    :cond_e
    move-object v4, v5

    goto :goto_9

    :cond_f
    move v3, v1

    goto :goto_2

    :cond_10
    move v4, v1

    goto :goto_1
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    sget-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/s;->f:Lcom/google/android/location/a$c;

    iget-object v0, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/android/location/s;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/s;->d:Lcom/google/android/location/y;

    iget-object v1, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/s;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/y;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/s;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->C:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/s;->r:Z

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/s;->q:J

    iget-wide v0, p0, Lcom/google/android/location/s;->q:J

    const-wide/16 v2, 0x3a98

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/s;->k(J)V

    return-void
.end method

.method private final f()Z
    .locals 6

    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->m()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/location/s;->D:Lcom/google/android/location/i/a;

    invoke-virtual {v2, v4, v5, v1}, Lcom/google/android/location/i/a;->b(JZ)Lcom/google/android/location/i/c$a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/s;->y:Lcom/google/android/location/i/c$a;

    iget-object v2, p0, Lcom/google/android/location/s;->y:Lcom/google/android/location/i/c$a;

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Lcom/google/android/location/os/i;->b(I)V

    iput-boolean v1, p0, Lcom/google/android/location/s;->r:Z

    iget-object v2, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    iget-object v3, p0, Lcom/google/android/location/s;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/s;->w:J

    iget-wide v1, p0, Lcom/google/android/location/s;->w:J

    add-long/2addr v1, v4

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/s;->k(J)V

    goto :goto_0
.end method

.method private final g()V
    .locals 8

    invoke-direct {p0}, Lcom/google/android/location/s;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/s;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, 0x0

    const-wide/32 v2, 0x1d4c0

    iget-object v4, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/location/s;->w:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/s;->D:Lcom/google/android/location/i/a;

    iget-object v3, p0, Lcom/google/android/location/s;->y:Lcom/google/android/location/i/c$a;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c$a;J)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->y:Lcom/google/android/location/i/c$a;

    invoke-direct {p0}, Lcom/google/android/location/s;->i()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->w:J

    :cond_0
    return-void
.end method

.method private final h()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/s;->w:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lcom/google/android/location/s;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v2}, Lcom/google/android/location/os/i;->a(I)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->A:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->r:Z

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v2}, Lcom/google/android/location/os/i;->c(I)V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/s;->s:Z

    iput-boolean v0, p0, Lcom/google/android/location/s;->t:Z

    iput-boolean v0, p0, Lcom/google/android/location/s;->u:Z

    iput-boolean v0, p0, Lcom/google/android/location/s;->v:Z

    iput-boolean v0, p0, Lcom/google/android/location/s;->z:Z

    return-void
.end method

.method private j(J)Z
    .locals 4

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/s;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/s;->C:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/s;->s:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/s;->z:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/s;->u:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/s;->v:Z

    if-eqz v0, :cond_3

    const/16 v0, 0xb

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/location/s;->t:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private k(J)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/s;->A:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/s;->A:J

    iget-object v0, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/location/s;->A:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_0
    return-void
.end method

.method private l()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/s;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/s;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/s;->A:J

    :cond_0
    return-void
.end method

.method public a(IIZ)V
    .locals 4

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    if-nez p3, :cond_0

    float-to-double v0, v0

    const-wide v2, 0x3fc999999999999aL

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/location/s;->g:Z

    :cond_1
    iput-boolean p3, p0, Lcom/google/android/location/s;->h:Z

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    iput-object p1, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    invoke-direct {p0}, Lcom/google/android/location/s;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/s;->t:Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .locals 5

    const/4 v4, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    invoke-direct {p0}, Lcom/google/android/location/s;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/s;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-object v1, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    invoke-static {v0, v1}, Lcom/google/android/location/s;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/google/android/location/s;->u:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v0}, Lcom/google/android/location/a$b;->b()Lcom/google/android/location/os/g;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/s;->n:Lcom/google/android/location/os/g;

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4069000000000000L

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iput-boolean v4, p0, Lcom/google/android/location/s;->v:Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/s;->E:Z

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/s;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E;)Z
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/location/e/E;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p2, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {p1, v2}, Lcom/google/android/location/b/g;->a(Ljava/lang/Long;)F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/s;->F:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    cmpg-float v2, v3, v2

    if-gez v2, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/google/android/location/e/E;)V
    .locals 7

    const-wide/32 v4, 0x927c0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-object v0, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v0}, Lcom/google/android/location/a$b;->a()Lcom/google/android/location/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/s;->a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/s;->z:Z

    invoke-direct {p0}, Lcom/google/android/location/s;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/s;->k:Lcom/google/android/location/e/E;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-wide v0, v0, Lcom/google/android/location/e/E;->a:J

    iget-object v2, p0, Lcom/google/android/location/s;->k:Lcom/google/android/location/e/E;

    iget-wide v2, v2, Lcom/google/android/location/e/E;->a:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    :cond_2
    iget-boolean v6, p0, Lcom/google/android/location/s;->s:Z

    iget-object v0, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    iget-object v1, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-object v2, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/E;JJ)Z

    move-result v0

    or-int/2addr v0, v6

    iput-boolean v0, p0, Lcom/google/android/location/s;->s:Z

    iget-boolean v0, p0, Lcom/google/android/location/s;->s:Z

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/s;->i:Z

    return-void
.end method

.method protected b(J)Z
    .locals 9

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/location/s;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/s;->j()V

    move v2, v1

    :goto_0
    return v2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/s;->j(J)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/s;->e()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-wide v3, v0, Lcom/google/android/location/e/E;->a:J

    iget-object v0, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v0}, Lcom/google/android/location/a$b;->e()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-gtz v0, :cond_6

    :cond_2
    move v0, v2

    :goto_1
    iget-object v3, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/s;->j:Lcom/google/android/location/e/e;

    invoke-virtual {v3}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/location/s;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v5}, Lcom/google/android/location/a$b;->d()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_7

    :cond_3
    move v3, v2

    :goto_2
    iget-boolean v4, p0, Lcom/google/android/location/s;->s:Z

    if-eqz v4, :cond_4

    if-eqz v0, :cond_4

    iput-boolean v1, p0, Lcom/google/android/location/s;->s:Z

    :cond_4
    iget-boolean v4, p0, Lcom/google/android/location/s;->t:Z

    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    iput-boolean v1, p0, Lcom/google/android/location/s;->t:Z

    :cond_5
    iget-object v4, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/location/s;->m:Lcom/google/android/location/e/E;

    iget-wide v4, v4, Lcom/google/android/location/e/E;->a:J

    sub-long v4, p1, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0xafc80

    cmp-long v4, v4, v6

    if-gtz v4, :cond_8

    move v4, v2

    :goto_3
    iget-wide v5, p0, Lcom/google/android/location/s;->x:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_9

    move v5, v1

    :goto_4
    iget-boolean v6, p0, Lcom/google/android/location/s;->s:Z

    if-eqz v6, :cond_b

    iget-boolean v6, p0, Lcom/google/android/location/s;->z:Z

    if-eqz v6, :cond_b

    iget-boolean v6, p0, Lcom/google/android/location/s;->u:Z

    if-nez v6, :cond_b

    iget-boolean v6, p0, Lcom/google/android/location/s;->E:Z

    if-eqz v6, :cond_b

    iget-boolean v6, p0, Lcom/google/android/location/s;->i:Z

    if-nez v6, :cond_b

    if-eqz v4, :cond_b

    if-nez v5, :cond_b

    invoke-direct {p0}, Lcom/google/android/location/s;->f()Z

    move-result v4

    if-eqz v4, :cond_b

    sget-object v0, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/s;->f:Lcom/google/android/location/a$c;

    iput-boolean v2, p0, Lcom/google/android/location/s;->l:Z

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v3, v1

    goto :goto_2

    :cond_8
    move v4, v1

    goto :goto_3

    :cond_9
    iget-wide v5, p0, Lcom/google/android/location/s;->x:J

    sub-long v5, p1, v5

    const-wide/32 v7, 0x493e0

    cmp-long v5, v5, v7

    if-gez v5, :cond_a

    move v5, v2

    goto :goto_4

    :cond_a
    move v5, v1

    goto :goto_4

    :cond_b
    iget-boolean v4, p0, Lcom/google/android/location/s;->z:Z

    if-nez v4, :cond_d

    if-eqz v0, :cond_d

    if-eqz v3, :cond_d

    iget-boolean v0, p0, Lcom/google/android/location/s;->u:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/location/s;->v:Z

    if-eqz v0, :cond_d

    :cond_c
    iput-boolean v1, p0, Lcom/google/android/location/s;->u:Z

    iput-boolean v1, p0, Lcom/google/android/location/s;->v:Z

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/location/s;->s:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/location/s;->t:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/location/s;->u:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/location/s;->v:Z

    if-eqz v0, :cond_10

    :cond_e
    invoke-direct {p0}, Lcom/google/android/location/s;->k()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_10

    invoke-direct {p0, v0}, Lcom/google/android/location/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lcom/google/android/location/s;->j()V

    :cond_f
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/s;->j(J)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-direct {p0}, Lcom/google/android/location/s;->e()V

    :goto_5
    iput-boolean v1, p0, Lcom/google/android/location/s;->z:Z

    goto/16 :goto_0

    :cond_10
    move v2, v1

    goto :goto_5
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/s;->o:Z

    return-void
.end method

.method protected c(J)Z
    .locals 6

    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/location/s;->w:J

    sub-long v2, p1, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    iput-wide p1, p0, Lcom/google/android/location/s;->x:J

    iget-boolean v3, p0, Lcom/google/android/location/s;->E:Z

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/s;->l()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/location/s;->i:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/location/s;->u:Z

    if-nez v3, :cond_0

    if-eqz v2, :cond_3

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/s;->g()V

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/s;->f:Lcom/google/android/location/a$c;

    move v0, v1

    :goto_2
    return v0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    const-wide/16 p1, -0x1

    goto :goto_1

    :cond_3
    iget-wide v1, p0, Lcom/google/android/location/s;->w:J

    add-long/2addr v1, v4

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/s;->k(J)V

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/location/s;->g()V

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/s;->f:Lcom/google/android/location/a$c;

    move v0, v1

    goto :goto_2
.end method

.method protected d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/s;->g:Z

    return v0
.end method

.method protected f(J)Z
    .locals 5

    const-wide/16 v3, 0x3a98

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/s;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/location/s;->i()V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/s;->q:J

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/s;->f:Lcom/google/android/location/a$c;

    :goto_0
    return v0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/location/s;->q:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/s;->i()V

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lcom/google/android/location/s;->q:J

    add-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/s;->k(J)V

    goto :goto_0
.end method
