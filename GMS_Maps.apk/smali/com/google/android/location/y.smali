.class public Lcom/google/android/location/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/b/f;

.field private final d:Lcom/google/android/location/b/g;

.field private final e:Lcom/google/android/location/g;

.field private final f:Lcom/google/android/location/i/a;

.field private final g:Lcom/google/android/location/e/n;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/i/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/y;->a:J

    iput-object p1, p0, Lcom/google/android/location/y;->b:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/y;->c:Lcom/google/android/location/b/f;

    iput-object p3, p0, Lcom/google/android/location/y;->d:Lcom/google/android/location/b/g;

    iput-object p4, p0, Lcom/google/android/location/y;->e:Lcom/google/android/location/g;

    iput-object p5, p0, Lcom/google/android/location/y;->g:Lcom/google/android/location/e/n;

    iput-object p6, p0, Lcom/google/android/location/y;->f:Lcom/google/android/location/i/a;

    return-void
.end method

.method private a(JJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const-wide/16 v4, 0x3e8

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    div-long v2, p3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/location/y;->a:J

    sub-long v2, p1, v2

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/y;->c:Lcom/google/android/location/b/f;

    iget-object v2, v2, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v2}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/y;->c:Lcom/google/android/location/b/f;

    iget-object v2, v2, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v2}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/location/y;->g:Lcom/google/android/location/e/n;

    invoke-virtual {v2}, Lcom/google/android/location/e/n;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sub-long v1, p3, p1

    iget-object v3, p0, Lcom/google/android/location/y;->e:Lcom/google/android/location/g;

    iget-wide v4, p0, Lcom/google/android/location/y;->a:J

    add-long/2addr v1, v4

    invoke-virtual {v3, v1, v2}, Lcom/google/android/location/g;->c(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/y;->f:Lcom/google/android/location/i/a;

    invoke-virtual {v1}, Lcom/google/android/location/i/a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_1

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/y;->d:Lcom/google/android/location/b/g;

    if-eqz v1, :cond_2

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/y;->d:Lcom/google/android/location/b/g;

    invoke-virtual {v2}, Lcom/google/android/location/b/g;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/location/y;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->y()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/location/y;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->z()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/y;->c:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/y;->c:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/y;->g:Lcom/google/android/location/e/n;

    invoke-virtual {v0}, Lcom/google/android/location/e/n;->b()V

    iput-wide p1, p0, Lcom/google/android/location/y;->a:J

    return-void
.end method

.method public a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 6

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/y;->a:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/y;->a(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/y;->a:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x6ddd00

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v2, 0x5

    invoke-interface {p1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v3

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/google/android/location/y;->a(JJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-wide v0, p0, Lcom/google/android/location/y;->a:J

    goto :goto_0
.end method
