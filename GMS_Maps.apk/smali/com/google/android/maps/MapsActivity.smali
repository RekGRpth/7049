.class public Lcom/google/android/maps/MapsActivity;
.super Lcom/google/googlenav/android/BaseMapsActivity;
.source "SourceFile"

# interfaces
.implements LaT/m;
.implements Lcom/google/googlenav/android/R;
.implements Lcom/google/googlenav/android/Y;
.implements Lcom/google/googlenav/android/ag;


# static fields
.field private static final ACTION_ENTER_CAR_MODE:Ljava/lang/String; = "android.app.action.ENTER_CAR_MODE"

.field private static final ENTER_CAR_MODE_FILTER:Landroid/content/IntentFilter;

.field private static final ENTRY_POINT_RESET_TIMEOUT_MS:J = 0xdbba0L

.field public static final INPUT_FOCUS_STATE_DIALOG:I = 0x2

.field public static final INPUT_FOCUS_STATE_INITIAL:I = -0x1

.field public static final INPUT_FOCUS_STATE_MAP:I = 0x1

.field public static final INPUT_FOCUS_STATE_WAIT:I = 0x0

.field private static final MAX_MS_BEFORE_ON_CREATE:I = 0x1388

.field private static final MINIMUM_HEAP_SIZE:J = 0x600000L

.field private static final NO_TIME:I = -0x1

.field private static coldStartProfile:Lcom/google/googlenav/common/util/o;

.field public static volatile featureTestUiHandler:Landroid/os/Handler;

.field private static inputFocusStateForTesting:I

.field private static final startupClock:Lcom/google/googlenav/common/a;

.field private static stopwatchStatsLifecycleOnPause:Lbm/i;

.field private static stopwatchStatsLifecycleOnPauseVm:Lbm/i;

.field private static stopwatchStatsMenuOpen:Lbm/i;

.field private static final stopwatchStatsStartupAfterBack:Lbm/i;

.field private static final stopwatchStatsStartupAfterBackVm:Lbm/i;

.field private static final stopwatchStatsStartupAfterBriefPause:Lbm/i;

.field private static final stopwatchStatsStartupAfterBriefPauseVm:Lbm/i;

.field private static final stopwatchStatsStartupCold:Lbm/i;

.field private static final stopwatchStatsStartupColdVm:Lbm/i;

.field private static final stopwatchStatsStartupHot:Lbm/i;

.field private static final stopwatchStatsStartupHotVm:Lbm/i;

.field private static final stopwatchStatsStartupRemoteStrings:Lbm/i;

.field private static final stopwatchStatsStartupRemoteStringsVm:Lbm/i;

.field private static final stopwatchStatsStartupScreenOn:Lbm/i;

.field private static final stopwatchStatsStartupScreenOnVm:Lbm/i;


# instance fields
.field private arePowerConsumersRunning:Z

.field private buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

.field private connReceiver:Landroid/content/BroadcastReceiver;

.field private final dockReceiver:Landroid/content/BroadcastReceiver;

.field private entryPointType:Lcom/google/android/maps/A;

.field private gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

.field private hasDataConnection:Z

.field private intentProcessor:Lcom/google/googlenav/android/M;

.field private final latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

.field private mapViewMenuController:Lcom/google/googlenav/ui/as;

.field private orientationProvider:LaV/h;

.field private final screenReceiver:Landroid/content/BroadcastReceiver;

.field private shouldSetupEntryPointOnResume:Z

.field private startupStartTime:J

.field private startupType:Lcom/google/android/maps/B;

.field private stopPowerConsumersTask:Las/d;

.field private tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

.field private voiceRecognizer:Lcom/google/googlenav/android/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x16

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.app.action.ENTER_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->ENTER_CAR_MODE_FILTER:Landroid/content/IntentFilter;

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup remote strings"

    const-string v3, "guid"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStrings:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup remote strings vm"

    const-string v3, "guid_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStringsVm:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup cold"

    const-string v3, "guif"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupCold:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup cold vm"

    const-string v3, "guif_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupColdVm:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after back"

    const-string v3, "guir"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBack:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after back vm"

    const-string v3, "guir_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBackVm:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup hot"

    const-string v3, "guis"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHot:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup hot vm"

    const-string v3, "guis_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHotVm:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup screen on"

    const-string v3, "guip"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOn:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup screen on vm"

    const-string v3, "guip_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOnVm:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after brief pause"

    const-string v3, "guib"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPause:Lbm/i;

    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after brief pause vm"

    const-string v3, "guib_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPauseVm:Lbm/i;

    const/4 v0, -0x1

    sput v0, Lcom/google/android/maps/MapsActivity;->inputFocusStateForTesting:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/android/BaseMapsActivity;-><init>()V

    new-instance v0, Lcom/google/android/maps/a;

    invoke-direct {v0, p0}, Lcom/google/android/maps/a;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->dockReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/maps/p;

    invoke-direct {v0, p0}, Lcom/google/android/maps/p;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->screenReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    new-instance v0, Lcom/google/android/maps/t;

    invoke-direct {v0, p0}, Lcom/google/android/maps/t;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->connReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    invoke-direct {v0}, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    sget-object v0, Lcom/google/android/maps/B;->a:Lcom/google/android/maps/B;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/maps/MapsActivity;Lcom/google/android/maps/B;)Lcom/google/android/maps/B;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/maps/MapsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->stopPowerConsumers()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/maps/MapsActivity;)LaV/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/maps/MapsActivity;)Lcom/google/googlenav/android/M;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/maps/MapsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/maps/MapsActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/maps/MapsActivity;)Las/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/maps/MapsActivity;Las/d;)Las/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/maps/MapsActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/maps/MapsActivity;)Lcom/google/googlenav/ui/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/maps/MapsActivity;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/MapsActivity;->onCreateInternal(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/maps/MapsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->onResumeInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/maps/MapsActivity;)Lcom/google/googlenav/ui/s;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/maps/MapsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->onNewFeaturesContentHintClick()V

    return-void
.end method

.method private createTabletDialog()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/view/android/bC;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/bC;-><init>(Lcom/google/googlenav/android/BaseMapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->show()V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    return-void
.end method

.method private determineEntryPointType()Lcom/google/android/maps/A;
    .locals 4

    sget-object v0, Lcom/google/android/maps/A;->a:Lcom/google/android/maps/A;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v2, Lcom/google/googlenav/android/M;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/maps/A;->b:Lcom/google/android/maps/A;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/googlenav/android/M;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/maps/A;->c:Lcom/google/android/maps/A;

    goto :goto_0
.end method

.method private getController()Lcom/google/googlenav/ui/s;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    return-object v0
.end method

.method private getGmmApplication()Lcom/google/googlenav/android/AndroidGmmApplication;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    return-object v0
.end method

.method public static getInputFocusForTesting()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    sget v0, Lcom/google/android/maps/MapsActivity;->inputFocusStateForTesting:I

    return v0
.end method

.method private getMapController()LaN/u;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    return-object v0
.end method

.method public static getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    return-object v0
.end method

.method private initTransitNavigation()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/rideabout/view/a;

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iget-object v3, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/rideabout/view/a;-><init>(Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V

    invoke-static {v1}, Lcom/google/googlenav/ui/view/r;->a(Lcom/google/googlenav/ui/view/r;)V

    new-instance v1, Lcom/google/android/maps/rideabout/app/q;

    invoke-direct {v1, p0, v0}, Lcom/google/android/maps/rideabout/app/q;-><init>(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-static {v1}, LaS/a;->a(LaS/a;)V

    return-void
.end method

.method private isIntentAvailable(Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onCreateInternal(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "MapsActivity.onCreateInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/googlenav/android/J;->a(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->c()V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setDefaultKeyMode(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->initMapUi()V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->determineEntryPointType()Lcom/google/android/maps/A;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/s;->a(Lcom/google/android/maps/A;)V

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    new-instance v3, Lcom/google/googlenav/android/D;

    invoke-direct {v3}, Lcom/google/googlenav/android/D;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/android/A;->a(Lcom/google/googlenav/android/C;)V

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->d()V

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    iget-object v3, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/android/A;->a(Lcom/google/googlenav/android/B;)V

    new-instance v0, Lcom/google/googlenav/android/ad;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/ad;-><init>(Lcom/google/googlenav/android/ag;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->voiceRecognizer:Lcom/google/googlenav/android/ad;

    new-instance v0, Lcom/google/googlenav/android/M;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getMapController()LaN/u;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4}, Lcom/google/googlenav/android/M;-><init>(Lcom/google/googlenav/android/R;Lcom/google/googlenav/ui/s;LaN/u;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    if-nez p1, :cond_0

    new-instance v0, Lcom/google/android/maps/w;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/maps/w;-><init>(Lcom/google/android/maps/MapsActivity;ZZ)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v3, v0, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v3, Lcom/google/android/maps/x;

    invoke-direct {v3, p0}, Lcom/google/android/maps/x;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-static {v0, v3}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0x32e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_1
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/y;

    invoke-direct {v1, p0}, Lcom/google/android/maps/y;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    const-string v0, "MapsActivity.onCreateInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_2
    sget-object v3, Lcom/google/googlenav/z;->d:Lcom/google/googlenav/z;

    invoke-static {v3, v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private onNewFeaturesContentHintClick()V
    .locals 5

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->m()V

    :cond_0
    :goto_0
    const/4 v0, 0x6

    const-string v1, "ch"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "c"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->openOptionsMenu()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->b()Landroid/view/View;

    move-result-object v0

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LaA/h;->a(Landroid/content/Context;Landroid/view/View;LaA/g;)V

    goto :goto_0
.end method

.method private onResumeInternal()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v0, "MapsActivity.onResumeInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aD()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aE()Z

    invoke-static {}, LaM/j;->a()LaM/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/j;->a(Landroid/content/Context;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->j()Lcom/google/googlenav/ui/android/L;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/L;->a(Landroid/view/View;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->h(Z)V

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->requestFocus()Z

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->c()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->resumeTransitNavigationView()V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->setUpForEntryPoint()V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->u()V

    const-string v1, "settings_preference"

    invoke-virtual {p0, v1, v5}, Lcom/google/android/maps/MapsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    const-string v3, "lastRunVersionCode"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-ge v3, v4, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    const/16 v4, 0x61a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v4, Lcom/google/android/maps/b;

    invoke-direct {v4, p0, v2, v0, v1}, Lcom/google/android/maps/b;-><init>(Lcom/google/android/maps/MapsActivity;Landroid/content/pm/PackageInfo;Lcom/google/googlenav/ui/s;Landroid/content/SharedPreferences;)V

    invoke-static {v3, v4}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->queueStarSync()V

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/e;

    sget-object v2, Lcom/google/googlenav/y;->a:Lcom/google/googlenav/y;

    invoke-direct {v1, p0, v5, v6, v2}, Lcom/google/android/maps/e;-><init>(Lcom/google/android/maps/MapsActivity;ZZLcom/google/googlenav/y;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    iput-boolean v6, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    :cond_6
    :goto_1
    const-string v0, "MapsActivity.onResumeInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private pauseTransitNavigationView()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->d()V

    return-void
.end method

.method private queueStarSync()V
    .locals 4

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/q;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/maps/q;-><init>(Lcom/google/android/maps/MapsActivity;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    return-void
.end method

.method private resumeTransitNavigationView()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->c()V

    return-void
.end method

.method public static setInputFocusForTesting(I)V
    .locals 0

    return-void
.end method

.method private setLastMenuForTest(Landroid/view/Menu;)V
    .locals 0

    return-void
.end method

.method private setMinimumHeapSize(J)V
    .locals 6

    :try_start_0
    const-string v0, "dalvik.system.VMRuntime"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getRuntime"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const-string v2, "setMinimumHeapSize"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "setMinimumHeapSize reflection failed"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setUpForEntryPoint()V
    .locals 12

    const/16 v11, 0x12

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->determineEntryPointType()Lcom/google/android/maps/A;

    move-result-object v4

    sget-object v0, Lcom/google/android/maps/A;->b:Lcom/google/android/maps/A;

    if-ne v4, v0, :cond_2

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-boolean v5, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    if-eqz v5, :cond_0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/googlenav/ui/s;->M()J

    move-result-wide v7

    const-wide/32 v9, 0xdbba0

    add-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-gez v5, :cond_3

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v1, v11}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v6

    const-string v7, "android.intent.action.MAIN"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const/high16 v5, 0x100000

    and-int/2addr v5, v6

    if-eqz v5, :cond_1

    :cond_4
    iput-object v4, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    iget-object v4, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    sget-object v5, Lcom/google/android/maps/A;->a:Lcom/google/android/maps/A;

    if-ne v4, v5, :cond_5

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->k()I

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->resetForInvocation()V

    sget-object v3, Lcom/google/android/maps/s;->a:[I

    iget-object v4, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    invoke-virtual {v4}, Lcom/google/android/maps/A;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const-string v3, "source"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, ""

    if-eqz v3, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "s="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    const/16 v3, 0x57

    const-string v4, "o"

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/ak;->a(Z)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v11}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_7
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    goto/16 :goto_1

    :pswitch_1
    const/16 v0, 0x3d

    const-string v1, "li"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->startFriendsListView(Lbf/am;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setupMapViewMenuController()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/as;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/as;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    new-instance v0, Lbf/av;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbf/av;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/android/maps/MapsActivity;)V

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/as;->a(Lcom/google/googlenav/ui/au;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/as;)V

    return-void
.end method

.method private stopPowerConsumers()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->Z()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    invoke-virtual {v0}, LaV/h;->h()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->e()V

    :cond_2
    return-void
.end method


# virtual methods
.method public activateAreaSelector(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)Lcom/google/googlenav/ui/android/r;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/android/r;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/r;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)V

    return-object v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/googlenav/android/BaseMapsActivity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public getActivity()Lcom/google/android/maps/MapsActivity;
    .locals 0

    return-object p0
.end method

.method public getBaseAndroidView()Lcom/google/googlenav/ui/android/BaseAndroidView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    return-object v0
.end method

.method public getBottomBar()Landroid/view/View;
    .locals 2

    const v0, 0x7f1002d3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewStub;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f10003d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getButtonContainer()Lcom/google/googlenav/ui/android/ButtonContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    return-object v0
.end method

.method public getCurrentViewportDetails()Landroid/os/Bundle;
    .locals 5

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getMapController()LaN/u;

    move-result-object v1

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v2

    const-string v3, "centerLatitude"

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "centerLongitude"

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "latitudeSpan"

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "longitudeSpan"

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "zoomLevel"

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public getMapViewMenuController()Lcom/google/googlenav/ui/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    return-object v0
.end method

.method public getNfcUrl()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/C;->x()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbf/i;->bc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/friend/android/e;->a()Lcom/google/googlenav/friend/android/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/googlenav/friend/android/e;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "contacts_accessor_formatted_address"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v0
.end method

.method public getState()Lcom/google/googlenav/android/i;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getGmmApplication()Lcom/google/googlenav/android/AndroidGmmApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    return-object v0
.end method

.method public getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    return-object v0
.end method

.method public getUiThreadHandler()Lcom/google/googlenav/android/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->k()Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    return-object v0
.end method

.method public hasDataConnection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    return v0
.end method

.method public initClickableView(Lcom/google/googlenav/ui/s;)V
    .locals 4

    const-string v0, "MapsActivity.initClickableView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bubble not found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const v0, 0x7f1002d1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100050

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/googlenav/ui/view/e;

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iget-object v3, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/googlenav/ui/view/e;-><init>(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V

    invoke-static {v1}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/view/e;)V

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-static {v0, v1}, Lbf/h;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/s;)V

    const-string v0, "MapsActivity.initClickableView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void
.end method

.method public initMapUi()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MapsActivity.initMapUi"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p0}, Lcom/google/googlenav/ui/view/android/ad;->a(Lcom/google/googlenav/android/BaseMapsActivity;)V

    invoke-static {p0}, Lcom/google/googlenav/ui/view/android/S;->a(Lcom/google/googlenav/android/BaseMapsActivity;)V

    invoke-static {p0}, Lcom/google/googlenav/mylocationnotifier/a;->a(Lcom/google/android/maps/MapsActivity;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    const v0, 0x7f0400ee

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setContentView(I)V

    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f1002cd

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    const v0, 0x7f100052

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ButtonContainer;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(Landroid/app/Application;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f1002cf

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BaseAndroidView;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/android/BaseAndroidView;->a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->a(Lcom/google/googlenav/ui/android/D;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapsActivity;->initClickableView(Lcom/google/googlenav/ui/s;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->initTransitNavigation()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->createTabletDialog()V

    :cond_1
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->c()Lcom/google/googlenav/ui/android/FloorPickerView;

    move-result-object v2

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setIndoorState(Ln/s;)V

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    invoke-interface {v0, v2}, LaH/m;->a(LaH/A;)V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-static {v2, v1, v0}, Lcom/google/googlenav/ui/android/N;->a(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)Lcom/google/googlenav/ui/android/N;

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->b()V

    const-string v0, "MapsActivity.initMapUi"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    goto/16 :goto_0

    :cond_4
    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    const v0, 0x7f0400ef

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setContentView(I)V

    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f1002ce

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    goto/16 :goto_1
.end method

.method public lockScreenOrientation()V
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setRequestedOrientation(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x7

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setRequestedOrientation(I)V

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/android/S;->a(IILandroid/content/Intent;)V

    invoke-super {p0, p1, p2, p3}, Lcom/google/googlenav/android/BaseMapsActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/am;->a(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->onConfigurationChangedInternal()V

    return-void
.end method

.method public onConfigurationChangedInternal()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->d()V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->f()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    :cond_1
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->e()V

    :cond_2
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->l()V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->b()V

    :cond_3
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/16 v4, 0x16

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    :cond_0
    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    const-wide/32 v0, 0x600000

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/MapsActivity;->setMinimumHeapSize(J)V

    invoke-static {}, Lcom/google/googlenav/android/c;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/maps/B;->d:Lcom/google/android/maps/B;

    :goto_0
    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->determineEntryPointType()Lcom/google/android/maps/A;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    iput-boolean v3, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    new-instance v0, LaU/d;

    invoke-direct {v0}, LaU/d;-><init>()V

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/o;->a(Lcom/google/android/maps/driveabout/vector/p;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/c;->a(Landroid/app/Application;)Lcom/google/googlenav/android/c;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->requestFeature(I)Z

    :cond_1
    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_1
    const-string v1, "UA-25817243-1"

    invoke-static {v1, p0, v0}, Laj/a;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v3}, Laj/a;->a(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/googlenav/android/i;->a(Lcom/google/android/maps/MapsActivity;Z)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->setupMapViewMenuController()V

    new-instance v0, Lcom/google/android/maps/u;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/u;-><init>(Lcom/google/android/maps/MapsActivity;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/google/googlenav/android/i;->a(Ljava/util/Locale;Lcom/google/googlenav/android/x;ZZ)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->al()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->l()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/S;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->a(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbl/j;->a(Landroid/content/Context;)V

    new-instance v0, Lbm/i;

    const-string v1, "menu open"

    const-string v2, "mo"

    invoke-direct {v0, v1, v2, v4}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsMenuOpen:Lbm/i;

    new-instance v0, Lbm/i;

    const-string v1, "maps onPause"

    const-string v2, "ap"

    invoke-direct {v0, v1, v2, v4}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    new-instance v0, Lbm/i;

    const-string v1, "maps onPause vm"

    const-string v2, "ap_vm"

    invoke-direct {v0, v1, v2, v4}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->m()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/maps/MapsActivity;->onCreateInternal(Landroid/os/Bundle;)V

    :cond_4
    const-string v0, "MapsActivity.onCreate"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_5
    sget-object v0, Lcom/google/android/maps/B;->c:Lcom/google/android/maps/B;

    goto/16 :goto_0

    :cond_6
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/as;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gk;->w()Lcom/google/googlenav/actionbar/b;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/googlenav/actionbar/b;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onDestroy()V

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Laj/a;->a()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->M()V

    :cond_1
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, LaS/a;->y()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->a()V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->f()V

    :cond_4
    invoke-static {p0}, Lcom/google/googlenav/android/J;->b(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->b(Lcom/google/android/maps/MapsActivity;)V

    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/android/BaseMapsActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->g()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/k;->a()V

    return-void
.end method

.method public onMainMenuItemsChanged(Lbf/i;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbf/i;->aE()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->invalidateOptionsMenu()V

    :cond_1
    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/android/BaseMapsActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsMenuOpen:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->b()V

    invoke-direct {p0, p2}, Lcom/google/android/maps/MapsActivity;->setLastMenuForTest(Landroid/view/Menu;)V

    return v0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/googlenav/android/i;->a(Lcom/google/android/maps/MapsActivity;Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/maps/MapsActivity;->setIntent(Landroid/content/Intent;)V

    iput-boolean v4, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/high16 v0, 0x100000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onNewIntent(Landroid/content/Intent;)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/h;

    invoke-direct {v1, p0, v4, v3}, Lcom/google/android/maps/h;-><init>(Lcom/google/android/maps/MapsActivity;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    goto :goto_0
.end method

.method public onOfflineDataUpdate(LaT/l;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p1}, LaT/l;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaT/l;->b()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, LaT/l;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaT/l;->c()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/r;

    invoke-direct {v1, p0}, Lcom/google/android/maps/r;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/as;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/MapsActivity;->setLastMenuForTest(Landroid/view/Menu;)V

    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/android/BaseMapsActivity;->onPanelClosed(ILandroid/view/Menu;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->o(Z)V

    return-void
.end method

.method public onPause()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaT/a;->k()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p0}, LaT/a;->b(LaT/m;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/u;->c()V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/aa;->b()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    :goto_0
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    sget-object v1, Lcom/google/android/maps/B;->g:Lcom/google/android/maps/B;

    iput-object v1, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    iput-boolean v3, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/ak;->b(Z)V

    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onPause()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->m()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->n()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_2
    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    :goto_1
    return-void

    :cond_3
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->k(Z)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/android/background/MapWorkerService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.googlenav.android.background.ON_PAUSE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "is_finishing"

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->isFinishing()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapsActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->T()V

    :cond_6
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->b()V

    :cond_7
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->pauseTransitNavigationView()V

    new-instance v1, Lcom/google/android/maps/f;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/f;-><init>(Lcom/google/android/maps/MapsActivity;Las/c;)V

    iput-object v1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v2, v3}, Las/d;->a(J)V

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    invoke-virtual {v1}, Las/d;->g()V

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    goto :goto_1

    :cond_8
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsMenuOpen:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/as;->a(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 2

    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    sget-object v0, Lcom/google/android/maps/B;->e:Lcom/google/android/maps/B;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onRestart()V

    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x1

    const-string v0, "MapsActivity.onResume"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/maps/A;->a:Lcom/google/android/maps/A;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    :cond_1
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onResume()V

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, LaT/a;->k()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p0}, LaT/a;->a(LaT/m;)V

    :cond_2
    invoke-static {}, Lcom/google/googlenav/u;->b()V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/aa;->a()V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->e()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;Lcom/google/googlenav/android/Y;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/maps/z;

    invoke-direct {v0, p0}, Lcom/google/android/maps/z;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2, v0, v4, v4}, Lcom/google/googlenav/android/i;->a(Ljava/util/Locale;Lcom/google/googlenav/android/x;ZZ)V

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->d()V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->m()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->onResumeInternal()V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/ak;->b(Z)V

    const-string v0, "MapsActivity.onResume"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    const-string v0, "MapsActivity.onStart"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->screenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->dockReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->ENTER_CAR_MODE_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->connReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->b()Lcom/google/googlenav/prefetch/android/g;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    invoke-interface {v1, v0}, LaH/m;->a(LaH/A;)V

    :cond_1
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    new-instance v1, Lcom/google/android/maps/v;

    invoke-direct {v1, p0}, Lcom/google/android/maps/v;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    const-string v0, "MapsActivity.onStart"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onStop()V

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Laj/a;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->screenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->dockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->connReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->b()Lcom/google/googlenav/prefetch/android/g;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    invoke-interface {v1, v0}, LaH/m;->b(LaH/A;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->stopPowerConsumers()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    :cond_3
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 3

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onWindowFocusChanged(Z)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v2

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->o(Z)V

    if-eqz p1, :cond_0

    invoke-static {v1}, Lcom/google/android/maps/MapsActivity;->setInputFocusForTesting(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processIntentAndStartSession()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    invoke-virtual {v1}, Lcom/google/googlenav/android/M;->a()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, -0x2

    if-eq v1, v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/K;->a(I)V

    return v1
.end method

.method public refreshFriends()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x158

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/X;->a(IILjava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public refreshFriendsSettings()V
    .locals 3

    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/k;

    invoke-direct {v2, p0}, Lcom/google/android/maps/k;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    return-void
.end method

.method public resetForInvocation()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->at()V

    return-void
.end method

.method public resolveType(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    return-void
.end method

.method public screenDrawn()V
    .locals 6

    const-wide/16 v4, -0x1

    iget-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/maps/s;->b:[I

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    invoke-virtual {v2}, Lcom/google/android/maps/B;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/maps/B;->a:Lcom/google/android/maps/B;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    iput-wide v4, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    return-void

    :pswitch_0
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStringsVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupColdVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_2
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBackVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_3
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHotVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_4
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOnVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_5
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPauseVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/maps/s;->b:[I

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    invoke-virtual {v2}, Lcom/google/android/maps/B;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_6
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStrings:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_7
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupCold:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_8
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBack:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_9
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHot:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_a
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOn:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_b
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPause:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public searchFor(Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method public showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->d(Ljava/lang/String;)Lcom/google/googlenav/ag;

    move-result-object v0

    return-object v0
.end method

.method public showFriend(Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lbf/X;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showStarDetails(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->k(Ljava/lang/String;)V

    return-void
.end method

.method public showStarOnMap(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->j(Ljava/lang/String;)V

    return-void
.end method

.method public startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;I)V

    return-void
.end method

.method public startBusinessDetailsLayer(Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    return-void
.end method

.method public startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aC;->a(Lcom/google/googlenav/ai;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;ILjava/lang/String;)V

    return-void
.end method

.method public startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V
    .locals 8

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    return-void
.end method

.method public startFriendsLayer(Lbf/am;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    return-void
.end method

.method public startFriendsLayerHistorySummary()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/ac;->a(Lcom/google/googlenav/ui/s;Z)V

    return-void
.end method

.method public startFriendsListView(Lbf/am;)V
    .locals 2

    const/16 v0, 0x13c

    invoke-virtual {p1, v0}, Lbf/am;->d(I)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    return-void
.end method

.method public startFriendsLocation(Lbf/am;Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x14b

    invoke-virtual {p1, v0, p2}, Lbf/am;->a(ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    return-void
.end method

.method public startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V
    .locals 8

    const/16 v7, 0xc8

    const/4 v6, 0x1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/dg;->e()I

    move-result v1

    and-int/lit8 v1, v1, -0x3

    new-instance v2, Lcom/google/android/maps/i;

    invoke-direct {v2, p0}, Lcom/google/android/maps/i;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/C;->a()V

    :cond_0
    new-instance v4, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v4}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    const/16 v4, 0x50f

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    const/16 v4, 0x61d

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/j;

    invoke-direct {v1, p0, v2, p2}, Lcom/google/android/maps/j;-><init>(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/friend/aQ;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method public startFriendsProfile(Lbf/am;Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x14a

    invoke-virtual {p1, v0, p2}, Lbf/am;->a(ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    return-void
.end method

.method public startLatitudeSettingsActivity()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/o;

    invoke-direct {v2, p0}, Lcom/google/android/maps/o;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    return-void
.end method

.method public startLocationHistoryOptIn(Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "start_activity_on_complete"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/maps/m;

    invoke-direct {v3, p0, v0}, Lcom/google/android/maps/m;-><init>(Lcom/google/android/maps/MapsActivity;Ljava/lang/Class;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(ZLcom/google/googlenav/ui/wizard/db;)V

    return-void
.end method

.method public startManageAutoCheckinPlaces()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/el;)V

    return-void
.end method

.method public startMyPlacesList(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    return-void
.end method

.method public startNextMatchingActivity(Landroid/content/Intent;)Z
    .locals 7

    const/4 v2, 0x0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    if-nez v1, :cond_0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_0
    iget v5, v1, Landroid/content/pm/ResolveInfo;->priority:I

    iget v0, v0, Landroid/content/pm/ResolveInfo;->priority:I

    if-ne v5, v0, :cond_2

    :goto_2
    if-eqz v2, :cond_1

    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/MapsActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v2, v1

    goto :goto_2
.end method

.method public startOffersList()V
    .locals 3

    sget-object v0, LaA/b;->k:LaA/c;

    sget v1, LaA/c;->a:I

    invoke-virtual {v0, v1}, LaA/c;->a(I)V

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "i"

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/offers/a;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 3

    invoke-static {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getCurrentViewportDetails()Landroid/os/Bundle;

    move-result-object v0

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    const-string v1, "searchIncludeInHistory"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->o()Lbf/i;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/n;->a(Lcom/google/googlenav/ui/wizard/C;Lbf/i;)I

    move-result v1

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbb/o;->d(I)V

    invoke-super {p0, p1, p2, v0, p4}, Lcom/google/googlenav/android/BaseMapsActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    goto :goto_0
.end method

.method public startSettingsActivity()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/google/googlenav/settings/SettingsPreferenceActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/n;

    invoke-direct {v2, p0}, Lcom/google/android/maps/n;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    return-void
.end method

.method public startTransitEntry()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ay()V

    return-void
.end method

.method public startTransitNavigationLayer()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->H()V

    return-void
.end method

.method public startTransitStationPage(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->f(Ljava/lang/String;)V

    return-void
.end method

.method public startVoiceRecognition(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {p0, v0}, Lcom/google/android/maps/MapsActivity;->isIntentAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->voiceRecognizer:Lcom/google/googlenav/android/ad;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    :cond_0
    return-void
.end method

.method public unlockScreenOrientation()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setRequestedOrientation(I)V

    return-void
.end method
