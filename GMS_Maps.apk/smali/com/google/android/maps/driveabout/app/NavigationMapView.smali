.class public Lcom/google/android/maps/driveabout/app/NavigationMapView;
.super Lcom/google/android/maps/driveabout/vector/VectorMapView;
.source "SourceFile"


# static fields
.field private static A:I


# instance fields
.field private volatile b:LaH/h;

.field private volatile c:Landroid/location/Location;

.field private d:[LO/z;

.field private e:LO/z;

.field private f:Lcom/google/android/maps/driveabout/app/de;

.field private g:[Lcom/google/android/maps/driveabout/app/de;

.field private final h:Lcom/google/android/maps/driveabout/vector/aA;

.field private final i:Lcom/google/android/maps/driveabout/app/bT;

.field private final j:Lcom/google/android/maps/driveabout/vector/A;

.field private k:Lcom/google/android/maps/driveabout/vector/j;

.field private final l:Lcom/google/android/maps/driveabout/vector/A;

.field private final m:Lcom/google/android/maps/driveabout/vector/M;

.field private n:Lcom/google/android/maps/driveabout/app/dN;

.field private o:Lcom/google/android/maps/driveabout/vector/aZ;

.field private p:Lcom/google/android/maps/driveabout/vector/aZ;

.field private final q:Lcom/google/android/maps/driveabout/vector/bd;

.field private r:Lcom/google/android/maps/driveabout/app/bC;

.field private s:Lcom/google/android/maps/driveabout/app/bK;

.field private final t:Landroid/graphics/Bitmap;

.field private u:Landroid/graphics/Bitmap;

.field private v:Landroid/graphics/Bitmap;

.field private w:LF/H;

.field private x:Lcom/google/android/maps/driveabout/app/cv;

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->A:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    iput v6, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    const v0, 0x7f02012e

    invoke-static {p2, v0}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->t:Landroid/graphics/Bitmap;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/j;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/m;->a:Lcom/google/android/maps/driveabout/vector/m;

    invoke-direct {v0, p2, v1}, Lcom/google/android/maps/driveabout/vector/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c(Z)Lcom/google/android/maps/driveabout/vector/aA;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    new-array v1, v6, [Lcom/google/android/maps/driveabout/vector/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v3, 0x7f02016e

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v3, 0x7f02016b

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a([Lcom/google/android/maps/driveabout/vector/aC;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    const v1, 0x7f0b000a

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x7f0c0000

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const v3, 0x7f0c0001

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(FII)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->t:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bT;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bT;-><init>(Lcom/google/android/maps/driveabout/vector/aA;Lcom/google/googlenav/common/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/M;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->m:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/M;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    const v1, -0x7f000001

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/M;->b(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    const/high16 v2, 0x60000000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/q;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, LA/c;->g:LA/c;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;LA/c;)Lcom/google/android/maps/driveabout/vector/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setTrafficMode(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/A;->b(I)V

    invoke-virtual {p0, v6}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setBaseDistancePenaltyFactorForLabelOverlay(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setFocusable(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setEnabled(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowTiltGesture(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowRotateGesture(Z)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setVisibility(I)V

    sget-object v0, LG/a;->s:LG/a;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDefaultLabelTheme(LG/a;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setLabelTheme(LG/a;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m_()V

    return-void
.end method

.method private C()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    if-ne v2, v0, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    if-eqz v2, :cond_1

    :goto_1
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/de;->b(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(LO/N;LO/N;Lo/X;)Lo/af;
    .locals 12

    invoke-virtual {p1}, LO/N;->x()LO/P;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, LO/N;->z()I

    move-result v1

    invoke-virtual {p2}, LO/N;->z()I

    move-result v2

    new-instance v3, Lo/am;

    invoke-direct {v3, p3, v1, v2}, Lo/am;-><init>(Lo/X;II)V

    invoke-virtual {v3}, Lo/am;->e()Lo/X;

    move-result-object v11

    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v1, 0x2

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, LO/P;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x3

    :cond_2
    new-instance v9, Lo/H;

    new-instance v0, Lo/I;

    const/4 v3, 0x3

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lo/b;->b:Lo/b;

    invoke-direct {v9, v0, v1}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    const/4 v0, 0x0

    new-array v10, v0, [I

    new-instance v0, Lo/af;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v3, v2, [Lo/H;

    const/4 v2, 0x0

    aput-object v9, v3, v2

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v4

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v11

    invoke-direct/range {v0 .. v10}, Lo/af;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/f;
    .locals 5

    const/16 v4, 0x20

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040039

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f1000fa

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f1000fb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    new-instance v1, Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    return-object v1

    :cond_1
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static setDefaultTrafficMode(I)V
    .locals 0

    sput p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->A:I

    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    invoke-virtual {v0}, LO/z;->d()I

    move-result v0

    goto :goto_0
.end method

.method public a(LO/z;)V
    .locals 6

    invoke-virtual {p1}, LO/z;->p()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3dcccccd

    mul-float/2addr v1, v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LO/z;->k()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_1

    invoke-virtual {p1, v0}, LO/z;->a(I)LO/N;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1, v4}, LO/z;->a(I)LO/N;

    move-result-object v4

    invoke-virtual {v4}, LO/N;->e()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v5, v1

    if-lez v5, :cond_0

    invoke-virtual {p1}, LO/z;->n()Lo/X;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(LO/N;LO/N;Lo/X;)Lo/af;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setImportantLabelFeatures(Ljava/util/List;)V

    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->d:[LO/z;

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Z)V

    if-eqz p2, :cond_0

    array-length v0, p2

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/de;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->d:[LO/z;

    const/4 v0, 0x0

    :goto_1
    array-length v1, p2

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    new-instance v2, Lcom/google/android/maps/driveabout/app/de;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget-object v4, p2, v0

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/de;-><init>(Landroid/content/res/Resources;LO/z;)V

    aput-object v2, v1, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    :cond_2
    aget-object v1, p2, v0

    if-ne v1, p1, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    aget-object v1, p2, v0

    if-ne p1, v1, :cond_4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->C()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    new-instance v0, Lcom/google/android/maps/driveabout/app/cu;

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v1

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->t:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d0006

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cu;-><init>(Lo/u;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bd;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/f;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    return-void
.end method

.method public a(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->d:[LO/z;

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->b(LF/H;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    :cond_2
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bd;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b:LaH/h;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setMyLocation(LaH/h;Z)V

    return-void
.end method

.method public c()LA/c;
    .locals 1

    sget-object v0, LA/c;->b:LA/c;

    return-object v0
.end method

.method public m_()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/vector/j;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/m;->a:Lcom/google/android/maps/driveabout/vector/m;

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 8

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x:Lcom/google/android/maps/driveabout/app/cv;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x:Lcom/google/android/maps/driveabout/app/cv;

    move v1, p1

    move v2, p2

    move v5, v4

    move v6, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/cv;->a(IIFIIII)V

    :cond_0
    return-void
.end method

.method public setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, LA/c;->j:LA/c;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;ZZLandroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    goto :goto_0
.end method

.method public setCompassMargins(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    return-void
.end method

.method public setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/j;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    return-void
.end method

.method public setController(Lcom/google/android/maps/driveabout/vector/bk;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setController(Lcom/google/android/maps/driveabout/vector/bk;)V

    instance-of v0, p1, Lcom/google/android/maps/driveabout/app/cq;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/maps/driveabout/app/cq;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/bT;)V

    :cond_0
    return-void
.end method

.method public setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_1
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    if-nez v0, :cond_2

    sget-object v0, LA/c;->d:LA/c;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    goto :goto_0
.end method

.method public setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    new-instance v0, Lcom/google/android/maps/driveabout/app/bK;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    sget-object v2, LA/c;->h:LA/c;

    invoke-direct {v0, p1, p0, v1, v2}, Lcom/google/android/maps/driveabout/app/bK;-><init>(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/NavigationMapView;Lcom/google/android/maps/driveabout/vector/A;LA/c;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Lcom/google/android/maps/driveabout/app/bF;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    :cond_1
    return-void
.end method

.method public setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    return-void
.end method

.method public setMyLocation(LaH/h;Z)V
    .locals 12

    const/4 v3, 0x0

    const/4 v8, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/bT;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b:LaH/h;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bT;->a(LaH/h;)V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b:LaH/h;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    const/high16 v1, 0x44fa0000

    invoke-static {v0, v1}, Lo/T;->a(FF)Lo/T;

    move-result-object v0

    invoke-virtual {v9, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/A;->e()V

    const/high16 v0, 0x7f0b0000

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    const v0, 0x7f0b0001

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->u:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200fe

    invoke-static {v0, v2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->u:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->v:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200f6

    invoke-static {v0, v2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->v:Landroid/graphics/Bitmap;

    :cond_2
    iget-object v11, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, LF/H;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->u:Landroid/graphics/Bitmap;

    const-string v6, ""

    const-string v7, ""

    invoke-direct/range {v0 .. v8}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    const v0, 0x7f0b0002

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    const v0, 0x7f0b0003

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iget-object v10, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, LF/H;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->v:Landroid/graphics/Bitmap;

    const-string v6, ""

    const-string v7, ""

    move-object v1, v9

    invoke-direct/range {v0 .. v8}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setModelChanged()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q_()V

    return-void
.end method

.method public setOnSizeChangedListener(Lcom/google/android/maps/driveabout/app/cv;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x:Lcom/google/android/maps/driveabout/app/cv;

    return-void
.end method

.method public setRawLocation(Landroid/location/Location;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    return-void
.end method

.method public setTrafficMode(I)V
    .locals 2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    sget p1, Lcom/google/android/maps/driveabout/app/NavigationMapView;->A:I

    :cond_0
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bC;->a(I)V

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :goto_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    const/high16 v1, 0x41f00000

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bd;->a(F)V

    :goto_1
    iput p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->C()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    const/high16 v1, 0x41700000

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bd;->a(F)V

    goto :goto_1
.end method

.method public setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V
    .locals 2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/dN;

    invoke-direct {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/dN;-><init>(LO/z;LO/N;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/app/dN;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/dN;->a(LO/z;LO/N;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1
.end method

.method public setViewMode(I)V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-ne v0, v5, :cond_9

    if-eq p1, v5, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowScroll(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowZoomGestures(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    :goto_1
    iget v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-ne v3, v6, :cond_2

    if-eq p1, v6, :cond_2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v0, v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/E;->m:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    :cond_2
    iget v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-eq v3, v5, :cond_4

    if-ne p1, v5, :cond_4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v0, v1

    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowScroll(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowZoomGestures(Z)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dN;

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_4
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-eq v2, v6, :cond_5

    if-ne p1, v6, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    move v0, v1

    :cond_5
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_6
    iput p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    return-void

    :cond_7
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->e:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v1, v0

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_0

    :cond_9
    move v0, v2

    goto :goto_1
.end method
