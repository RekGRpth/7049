.class public Lcom/google/android/maps/driveabout/app/NavigationView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/cQ;


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Landroid/view/View;

.field private C:Landroid/widget/LinearLayout;

.field private D:Landroid/view/animation/Animation;

.field private E:Landroid/view/animation/Animation;

.field private F:Landroid/widget/TextView;

.field private G:Lcom/google/android/maps/driveabout/app/cO;

.field private H:Landroid/widget/TextView;

.field private I:Lcom/google/android/maps/driveabout/app/cO;

.field private J:Landroid/widget/RelativeLayout;

.field private K:Landroid/view/View;

.field private L:Landroid/view/View;

.field private M:Landroid/view/View;

.field private N:Landroid/view/View;

.field private O:Landroid/view/View;

.field private P:Landroid/view/View;

.field private Q:Landroid/widget/ImageView;

.field private R:Landroid/view/View;

.field private S:Landroid/view/View;

.field private T:Landroid/view/View;

.field private U:Landroid/widget/Button;

.field private V:Landroid/widget/Button;

.field private W:Ljava/lang/Runnable;

.field private Z:J

.field private a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

.field private aa:Landroid/location/Location;

.field private ab:Z

.field private ac:I

.field private final ad:Lcom/google/android/maps/driveabout/app/dz;

.field private ae:Landroid/view/View$OnClickListener;

.field private af:Landroid/view/View$OnClickListener;

.field private final ag:I

.field private ah:Landroid/content/DialogInterface$OnClickListener;

.field private ai:Landroid/view/View$OnTouchListener;

.field private final b:Lcom/google/android/maps/driveabout/app/aI;

.field private c:Landroid/widget/ListView;

.field private d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

.field private e:Lcom/google/android/maps/driveabout/app/an;

.field private f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

.field private g:Lcom/google/android/maps/driveabout/app/cO;

.field private h:Lcom/google/android/maps/driveabout/app/TopBarView;

.field private i:Landroid/view/ViewGroup;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View$OnClickListener;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/view/ViewGroup;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/ImageView;

.field private z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ab:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dz;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aI;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/aI;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const-string v0, "/"

    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private K()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setDrawingCacheEnabled(Z)V

    return-void
.end method

.method private L()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->g()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n()LC/a;

    move-result-object v2

    invoke-virtual {v2}, LC/a;->h()Lo/T;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lo/T;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private M()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->g()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->e()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private N()V
    .locals 3

    new-instance v0, Lcom/google/android/maps/driveabout/app/cK;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cK;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    const-wide/16 v1, 0x1388

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private O()V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    const v0, 0x7f100103

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1000ec

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v1, 0x7f1000ee

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v2, 0x7f1000ef

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v2, 0x7f1000f0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v2, 0x7f1000ed

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private P()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v2, 0x7f0b0014

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCompassMargins(II)V

    :cond_1
    return-void

    :cond_2
    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v2, 0x7f0b0016

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ae:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Navigation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to start activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->af:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->c(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->s:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eF;->c(Landroid/view/View;I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    return-void
.end method

.method private c(I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/NavigationView;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Z:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->aa:Landroid/location/Location;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public B()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method

.method public C()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public D()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    return-void
.end method

.method public E()V
    .locals 3

    const/4 v0, 0x4

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCopyrightPadding(II)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    const v2, 0x7f050008

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->J:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->d(Landroid/view/View;I)V

    return-void
.end method

.method public F()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCopyrightPadding(II)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    const v2, 0x7f05000d

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->J:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->d(Landroid/view/View;I)V

    return-void
.end method

.method public G()V
    .locals 4

    const/16 v1, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->O()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->c(Landroid/view/View;I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->s:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->c(Landroid/view/View;I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    invoke-static {v1, v3}, Lcom/google/android/maps/driveabout/app/eF;->c(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public H()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(Z)V

    return-void
.end method

.method public I()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p()V

    return-void
.end method

.method public J()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h()V

    return-void
.end method

.method public a()Lcom/google/android/maps/driveabout/app/NavigationMapView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/an;->a(I)V

    return-void
.end method

.method public a(II)V
    .locals 2

    const v0, 0x7f0d0048

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->a(IIILandroid/content/Intent;)V

    return-void
.end method

.method public a(IIILandroid/content/Intent;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/maps/driveabout/app/cG;

    invoke-direct {v5, p0, p4}, Lcom/google/android/maps/driveabout/app/cG;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V

    new-instance v6, Lcom/google/android/maps/driveabout/app/cH;

    invoke-direct {v6, p0, p4}, Lcom/google/android/maps/driveabout/app/cH;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/an;->a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/app/cI;

    invoke-direct {v0, p0, p3}, Lcom/google/android/maps/driveabout/app/cI;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/maps/driveabout/app/an;->a(IILandroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public a(IIZ)V
    .locals 2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02013e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f0202b9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f020103

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/an;->a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method public a(LO/U;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p0}, Lcom/google/android/maps/driveabout/app/an;->a(LO/U;Lcom/google/android/maps/driveabout/app/cQ;)V

    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(LO/z;[LO/z;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    return-void
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/an;->c(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cO;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p4, 0x0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/an;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->F:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->G:Lcom/google/android/maps/driveabout/app/cO;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(I)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/cN;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/app/cN;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    return-void
.end method

.method public a(Lo/o;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/bp;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cQ;Lo/o;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/an;->a(Z)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    return-void
.end method

.method public a(ZZ)V
    .locals 4

    const v3, 0x7f020117

    const v2, 0x7f020113

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const v1, 0x7f10011a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d007a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d007b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(ZZZ)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/maps/driveabout/app/TopBarView;->setButtonVisibility(ZZZ)V

    if-eqz p3, :cond_0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->L()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->M()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Z:J

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->N()V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public a([LO/U;ZZLcom/google/android/maps/driveabout/app/cR;)V
    .locals 2

    if-eqz p3, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/cF;

    invoke-direct {v0, p0, p4}, Lcom/google/android/maps/driveabout/app/cF;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Lcom/google/android/maps/driveabout/app/cR;)V

    move-object p4, v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p4}, Lcom/google/android/maps/driveabout/app/an;->a([LO/U;ILcom/google/android/maps/driveabout/app/cR;)V

    return-void
.end method

.method public a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/an;->a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V

    return-void
.end method

.method public a(FF)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    :goto_0
    const/4 v3, 0x2

    new-array v3, v3, [I

    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v4, v3, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    aget v3, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v3

    int-to-float v4, v4

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_3

    int-to-float v4, v5

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_3

    int-to-float v3, v3

    cmpl-float v3, p2, v3

    if-ltz v3, :cond_3

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_3

    move v0, v1

    :goto_1
    move v2, v0

    :cond_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public a(LO/z;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/aI;->a(LO/z;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/OutputStream;)Z
    .locals 11

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getHeight()I

    move-result v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/app/NavigationView;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l()Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_2

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v2, v1

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->getHeight()I

    move-result v1

    int-to-float v5, v1

    const/16 v1, 0x8

    new-array v1, v1, [F

    const/4 v3, 0x0

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x1

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x2

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x3

    aput v4, v1, v3

    const/4 v3, 0x4

    aput v2, v1, v3

    const/4 v3, 0x5

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x6

    aput v2, v1, v3

    const/4 v3, 0x7

    aput v4, v1, v3

    const/16 v3, 0x8

    new-array v3, v3, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v3, v9

    const/4 v9, 0x1

    add-float v10, v4, v5

    aput v10, v3, v9

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v3, v9

    const/4 v9, 0x3

    aput v5, v3, v9

    const/4 v9, 0x4

    aput v2, v3, v9

    const/4 v9, 0x5

    add-float/2addr v4, v5

    aput v4, v3, v9

    const/4 v4, 0x6

    aput v2, v3, v4

    const/4 v2, 0x7

    aput v5, v3, v2

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->setPolyToPoly([FI[FII)Z

    new-instance v1, Landroid/graphics/ColorMatrix;

    const/16 v2, 0x14

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrix;-><init>([F)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    invoke-virtual {v7, v8, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v6, v0, v1, p1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    const/4 v0, 0x1

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
    .end array-data
.end method

.method public b(LO/z;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(LO/z;)V

    return-void
.end method

.method public b(LO/z;[LO/z;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(LO/z;[LO/z;)V

    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->setRoute(LO/z;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(I)V

    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    return-void
.end method

.method public b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    const v1, 0x7f0d007c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->I:Lcom/google/android/maps/driveabout/app/cO;

    const/16 v1, 0x9c4

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    const v1, 0x7f0d007d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cJ;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/cJ;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public f(Z)V
    .locals 4

    const v1, 0x7f050011

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, v2, v2, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setButtonVisibility(ZZZ)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_1
    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public g(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Z)V

    return-void
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public h(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Z)V

    return-void
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->b()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->d()V

    return-void
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ah:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->r()V

    return-void
.end method

.method public n()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->G:Lcom/google/android/maps/driveabout/app/cO;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(Z)V

    return-void
.end method

.method public o()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->I:Lcom/google/android/maps/driveabout/app/cO;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(Z)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 6

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f100104

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->s:Landroid/view/ViewGroup;

    const v0, 0x7f100107

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f10010f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/TopBarView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    const v0, 0x7f100109

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f10010a

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v4, v0, v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setConnectedViews(Landroid/widget/TextView;Landroid/widget/TextView;)V

    const v0, 0x7f100105

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->t:Landroid/view/View;

    const v0, 0x7f100108

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    const v0, 0x7f10010c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    const v0, 0x7f100100

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v0, 0x7f100146

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    const v0, 0x7f10010b

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cO;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    const/4 v4, 0x1

    invoke-direct {v0, p0, v1, v4}, Lcom/google/android/maps/driveabout/app/cO;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    const v0, 0x7f100147

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    const v0, 0x7f100148

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v0, 0x7f100149

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    const v0, 0x7f10014a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const v0, 0x7f10014b

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    const v0, 0x7f10010d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->F:Landroid/widget/TextView;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cO;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->F:Landroid/widget/TextView;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/maps/driveabout/app/cO;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->G:Lcom/google/android/maps/driveabout/app/cO;

    const v0, 0x7f10010e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cO;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/maps/driveabout/app/cO;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->I:Lcom/google/android/maps/driveabout/app/cO;

    const v0, 0x7f100106

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/NavigationImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    const v0, 0x7f1000ce

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->J:Landroid/widget/RelativeLayout;

    const v0, 0x7f1000cf

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    const v0, 0x7f1000d3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    const v0, 0x7f1000d4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    const v0, 0x7f1000d5

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    const v0, 0x7f1000d6

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    const v0, 0x7f1000d7

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    const v0, 0x7f1000d2

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    const v0, 0x7f1000d1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    const v0, 0x7f1000d0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    const v0, 0x7f10012d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->T:Landroid/view/View;

    const v0, 0x7f10012f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    const v0, 0x7f10012e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100110

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const v0, 0x7f100111

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    const v0, 0x7f10011f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100120

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f10011d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10011c

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100112

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f05000d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050008

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->K()V

    return-void

    :cond_1
    move v0, v3

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "User interaction"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ai:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ai:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 5

    const/4 v1, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ab:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b002f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v0, 0x7f100101

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v2, v4, v2

    sub-int v0, v2, v0

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setMaxWidth(I)V

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ab:Z

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public p()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method

.method public q()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public r()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public s()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public setAlternateRouteButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setAlternateRouteButtonTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public setBackToMyLocationButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V

    return-void
.end method

.method public setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    return-void
.end method

.method public setCloseActivityListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->af:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V

    return-void
.end method

.method public setCurrentDirectionsListStep(LO/N;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/aI;->a(LO/N;)V

    return-void
.end method

.method public setCurrentRoadName(Landroid/location/Location;)V
    .locals 6

    const/4 v5, 0x0

    if-eqz p1, :cond_2

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->aa:Landroid/location/Location;

    move-object v0, p1

    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->l()Lo/af;

    move-result-object v1

    invoke-virtual {v0}, LaH/h;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lo/af;->c()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {v1, v5}, Lo/af;->d(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v2

    invoke-virtual {v1, v5}, Lo/af;->b(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/maps/driveabout/app/cL;

    invoke-direct {v4, p0, p1}, Lcom/google/android/maps/driveabout/app/cL;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/location/Location;)V

    invoke-virtual {v2, v3, v4}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v2

    invoke-virtual {v2}, Lv/a;->c()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Lv/a;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v5}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/dj;

    const/high16 v3, 0x3f800000

    invoke-direct {v1, v0, v3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/16 v3, 0x21

    invoke-virtual {v2, v1, v5, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dz;

    invoke-virtual {v1, v5}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_0
.end method

.method public setDestinationButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->o:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setDestinationInfoButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->g()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setDialogManager(Lcom/google/android/maps/driveabout/app/an;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    return-void
.end method

.method public setDoubleTapZoomsAboutCenter(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDoubleTapZoomsAboutCenter(Z)V

    return-void
.end method

.method public setExitButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->h()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ae:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setFoundDestinationButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->f()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    return-void
.end method

.method public setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V

    return-void
.end method

.method public setLayersButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->n:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setListItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public setListViewButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setLoadingDialogListeners(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method public setMapController(Lcom/google/android/maps/driveabout/vector/bk;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setController(Lcom/google/android/maps/driveabout/vector/bk;)V

    return-void
.end method

.method public setMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V

    return-void
.end method

.method public setMapView(Lcom/google/android/maps/driveabout/app/NavigationMapView;Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    return-void
.end method

.method public setMapViewKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public setMapViewVisibility(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setVisibility(I)V

    return-void
.end method

.method public setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    return-void
.end method

.method public setMuteButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setMyLocation(Landroid/location/Location;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    check-cast p1, LaH/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setMyLocation(LaH/h;Z)V

    return-void
.end method

.method public setNavigationImageStep(LO/N;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationImageView;->setStep(LO/N;)V

    return-void
.end method

.method public setNavigationImageViewClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ai:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method public setRawLocation(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setRawLocation(Landroid/location/Location;)V

    return-void
.end method

.method public setRecordingSample(IF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/an;->a(IF)V

    return-void
.end method

.method public setRerouteToDestinationButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->e()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setRouteAroundTrafficCancelButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setRouteAroundTrafficConfirmButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setRouteOptionsButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSpeechInstallButtonListener(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ah:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public setStatusBarContent(Ljava/lang/CharSequence;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    const-string v0, "__route_status"

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_0
.end method

.method public setStreetViewButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setStreetViewLaunchButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTimeRemaining(IZIZ)V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dz;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    mul-int/lit16 v3, p1, 0x3e8

    int-to-long v3, v3

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(J)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dz;

    invoke-virtual {v0, p1, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(IZ)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    :cond_1
    if-nez p2, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    if-nez p3, :cond_2

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d003f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    :goto_2
    new-instance v1, Landroid/text/style/RelativeSizeSpan;

    const v2, 0x3f19999a

    invoke-direct {v1, v2}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0040

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public setTopBarClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/maps/driveabout/app/dg;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->d()Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->setListener(Lcom/google/android/maps/driveabout/app/dg;)V

    return-void
.end method

.method public setTopOverlayText(Ljava/lang/CharSequence;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTrafficButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTrafficButtonState(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    if-eqz p1, :cond_0

    const v0, 0x7f020155

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void

    :cond_0
    const v0, 0x7f020153

    goto :goto_0
.end method

.method public setTrafficMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setTrafficMode(I)V

    return-void
.end method

.method public setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V

    return-void
.end method

.method public setViewMode(I)V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->c(I)Landroid/view/View;

    move-result-object v1

    if-eq v1, v0, :cond_2

    if-ne p1, v4, :cond_4

    if-eqz v0, :cond_1

    const v2, 0x7f050008

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    const v2, 0x7f050007

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setViewMode(I)V

    :cond_3
    iput p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    if-ne v2, v4, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    const v2, 0x7f05000e

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    if-eqz v1, :cond_2

    const v0, 0x7f05000d

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    goto :goto_1

    :cond_5
    if-ne p1, v5, :cond_7

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eq v0, v2, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    const v0, 0x7f050010

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    goto :goto_1

    :cond_7
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    if-ne v2, v5, :cond_8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_2

    const v2, 0x7f050011

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    goto :goto_1

    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    if-eqz v0, :cond_2

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public setVoiceSearchButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->m:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setZoomButtonClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public t()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->T:Landroid/view/View;

    const v1, 0x7f05000d

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method

.method public u()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->T:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method

.method public w()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    return-void
.end method

.method public x()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->e(Landroid/view/View;I)V

    return-void
.end method

.method public y()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->e(Landroid/view/View;I)V

    return-void
.end method

.method public z()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    return-void
.end method
