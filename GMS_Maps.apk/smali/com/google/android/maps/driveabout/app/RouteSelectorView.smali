.class public Lcom/google/android/maps/driveabout/app/RouteSelectorView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/android/maps/driveabout/app/dh;

.field private b:Lcom/google/android/maps/driveabout/app/dh;

.field private c:Lcom/google/android/maps/driveabout/app/dz;

.field private d:LO/z;

.field private e:[LO/z;

.field private f:I

.field private g:Lcom/google/android/maps/driveabout/app/dg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/dh;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/dh;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/RouteSelectorView;)Lcom/google/android/maps/driveabout/app/dg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->g:Lcom/google/android/maps/driveabout/app/dg;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->c:Lcom/google/android/maps/driveabout/app/dz;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04004c

    invoke-virtual {v0, v1, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dh;

    const v2, 0x7f100135

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2, v4, v5}, Lcom/google/android/maps/driveabout/app/dh;-><init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;ZLcom/google/android/maps/driveabout/app/df;)V

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dh;

    const v2, 0x7f100136

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2, v4, v5}, Lcom/google/android/maps/driveabout/app/dh;-><init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;ZLcom/google/android/maps/driveabout/app/df;)V

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/maps/driveabout/app/dh;

    const v3, 0x7f100137

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/dh;-><init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;ZLcom/google/android/maps/driveabout/app/df;)V

    aput-object v2, v0, v1

    new-instance v0, Lcom/google/android/maps/driveabout/app/dh;

    const v1, 0x7f100134

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1, v6, v5}, Lcom/google/android/maps/driveabout/app/dh;-><init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;ZLcom/google/android/maps/driveabout/app/df;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/dh;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/RouteSelectorView;)Lcom/google/android/maps/driveabout/app/dz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->c:Lcom/google/android/maps/driveabout/app/dz;

    return-object v0
.end method


# virtual methods
.method public a(III)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->f:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->f:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->d:LO/z;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p1, p2}, LO/z;->a(Landroid/content/Context;II)LO/E;

    move-result-object v1

    invoke-virtual {v1}, LO/E;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dh;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->d:LO/z;

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/maps/driveabout/app/dh;->a(II)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    invoke-virtual {v0, p3, v3}, Lcom/google/android/maps/driveabout/app/dh;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    return-void
.end method

.method public setListener(Lcom/google/android/maps/driveabout/app/dg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->g:Lcom/google/android/maps/driveabout/app/dg;

    return-void
.end method

.method public setRoutes(LO/z;[LO/z;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v3, 0x1

    const/16 v5, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->d:LO/z;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->e:[LO/z;

    invoke-static {v0, p2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->d:LO/z;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->e:[LO/z;

    array-length v0, p2

    if-le v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    aget-object v0, v0, v3

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    move v0, v1

    :goto_1
    array-length v2, p2

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    aget-object v2, v2, v0

    aget-object v4, p2, v0

    invoke-virtual {v2, v4}, Lcom/google/android/maps/driveabout/app/dh;->a(LO/z;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    aget-object v4, v2, v0

    aget-object v2, p2, v0

    if-ne p1, v2, :cond_1

    move v2, v3

    :goto_2
    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/app/dh;->a(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_3
    if-ge v0, v6, :cond_4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a:[Lcom/google/android/maps/driveabout/app/dh;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/dh;->a(LO/z;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b:Lcom/google/android/maps/driveabout/app/dh;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dh;->a(I)V

    goto :goto_0
.end method
