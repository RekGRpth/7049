.class public abstract Lcom/google/android/maps/driveabout/app/T;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/google/android/maps/driveabout/app/bR;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/os/Handler;

.field protected c:I

.field protected d:LaH/h;

.field private e:Ljava/util/ArrayList;

.field private f:Ljava/util/ArrayList;

.field private g:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/T;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->d:LaH/h;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->b:Landroid/os/Handler;

    return-void
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IFF)Landroid/view/View;
    .locals 9

    const v3, 0x7f1000e3

    const/high16 v8, -0x40800000

    const/16 v7, 0x8

    const/4 v6, 0x0

    instance-of v0, p1, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f1000e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p3, :cond_3

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    const v0, 0x7f100125

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100126

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/app/DirectionView;

    cmpl-float v2, p5, v8

    if-eqz v2, :cond_5

    new-instance v2, Lcom/google/android/maps/driveabout/app/aK;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;)V

    invoke-static {p5}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v6, v4, v5}, Lcom/google/android/maps/driveabout/app/aK;->a(IIZI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    cmpl-float v0, p6, v8

    if-eqz v0, :cond_4

    invoke-virtual {v1, p6}, Lcom/google/android/maps/driveabout/app/DirectionView;->setOrientation(F)V

    invoke-virtual {v1, v6}, Lcom/google/android/maps/driveabout/app/DirectionView;->setVisibility(I)V

    :goto_2
    const v0, 0x7f1000e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz p4, :cond_6

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    return-object p1

    :cond_2
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    invoke-virtual {v1, v7}, Lcom/google/android/maps/driveabout/app/DirectionView;->setVisibility(I)V

    goto :goto_2

    :cond_5
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v7}, Lcom/google/android/maps/driveabout/app/DirectionView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;LaH/h;Z)Lcom/google/android/maps/driveabout/app/T;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/ad;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/app/ad;-><init>(Landroid/content/Context;LaH/h;Z)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;)Lcom/google/android/maps/driveabout/app/T;
    .locals 6

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/aa;

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aa;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZZ)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZ)Lcom/google/android/maps/driveabout/app/T;
    .locals 6

    if-nez p3, :cond_0

    const/4 v3, 0x1

    :goto_0
    new-instance v0, Lcom/google/android/maps/driveabout/app/aa;

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aa;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZZ)V

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)Lcom/google/android/maps/driveabout/app/T;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/W;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/W;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method private a(Lcom/google/android/maps/driveabout/app/Y;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/Y;->g()LO/U;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a()V
    .locals 0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/T;->b()V

    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-gez v0, :cond_0

    :goto_1
    return v1

    :cond_0
    if-eqz v0, :cond_1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method private static b()V
    .locals 1

    const/16 v0, 0xa

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 6

    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    monitor-enter p0

    :try_start_0
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/T;->a(Lcom/google/android/maps/driveabout/app/Y;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/android/maps/driveabout/app/T;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method

.method public a(LaH/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->d:LaH/h;

    return-void
.end method

.method protected declared-synchronized a(Ljava/util/ArrayList;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/T;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/T;->c:I

    return-void
.end method

.method protected b(Ljava/util/ArrayList;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/T;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->a(Ljava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/maps/driveabout/app/U;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/U;-><init>(Lcom/google/android/maps/driveabout/app/T;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method c(Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/T;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/V;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/V;-><init>(Lcom/google/android/maps/driveabout/app/T;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/Y;

    if-nez v0, :cond_0

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040042

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v3, v1

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->e()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->i()F

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->j()F

    move-result v6

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IFF)Landroid/view/View;

    move-result-object v3

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->g()LO/U;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->h()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    const v4, 0x7f0d00d8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->g()LO/U;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/maps/driveabout/app/dz;->b(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->i()F

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->j()F

    move-result v6

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IFF)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040044

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    move-object v3, v1

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->f()I

    move-result v0

    invoke-virtual {v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_0

    :cond_2
    move-object v0, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public isEnabled(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/Y;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
