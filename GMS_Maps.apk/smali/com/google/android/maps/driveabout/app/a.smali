.class public Lcom/google/android/maps/driveabout/app/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/maps/driveabout/app/NavigationService;

.field private final c:Lcom/google/android/maps/driveabout/app/j;

.field private d:LK/ac;

.field private e:LK/T;

.field private f:LK/T;

.field private final g:LK/T;

.field private final h:LK/c;

.field private final i:LK/c;

.field private j:Lcom/google/android/maps/driveabout/app/g;

.field private k:Lcom/google/android/maps/driveabout/app/g;

.field private l:Lcom/google/android/maps/driveabout/app/g;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private final q:Lcom/google/android/maps/driveabout/app/aK;

.field private final r:Ljava/util/HashMap;

.field private s:I

.field private t:LK/V;

.field private u:J

.field private v:Landroid/os/Handler;

.field private w:Z

.field private final x:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/a;->o:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    iput v1, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/a;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->v:Landroid/os/Handler;

    new-instance v0, LK/ac;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    new-instance v1, Lcom/google/android/maps/driveabout/app/e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/e;-><init>(Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/b;)V

    invoke-virtual {v0, v1}, LK/ac;->a(LK/ae;)V

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {v0, v1}, LK/F;->a(Law/p;Landroid/content/Context;)LK/F;

    move-result-object v0

    new-instance v1, LK/u;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, LK/u;-><init>(Landroid/content/Context;LK/C;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->g:LK/T;

    new-instance v0, LK/J;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/J;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->h:LK/c;

    new-instance v0, LK/ah;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/ah;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->i:LK/c;

    new-instance v0, Lcom/google/android/maps/driveabout/app/j;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aK;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->c(I)V

    return-void
.end method

.method private a(LO/j;Z)LK/V;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->b()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2

    if-eqz p2, :cond_1

    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v1, 0x7f0d000f

    const/4 v0, 0x6

    :goto_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    invoke-direct {v2, v0}, LK/s;-><init>(I)V

    invoke-static {p1, v1, v2}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :pswitch_0
    const v1, 0x7f0d000d

    const/16 v0, 0x9

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0d000e

    const/16 v0, 0xa

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const v1, 0x7f0d0012

    const/4 v0, 0x5

    goto :goto_0

    :pswitch_2
    const v1, 0x7f0d0010

    const/4 v0, 0x7

    goto :goto_0

    :pswitch_3
    const v1, 0x7f0d0011

    const/16 v0, 0x8

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p1}, LO/j;->j()LK/m;

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    invoke-static {p1, v1, v2}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/a;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->v:Landroid/os/Handler;

    return-object v0
.end method

.method private a(LK/V;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/a;->t:LK/V;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/a;->u:J

    return-void
.end method

.method private declared-synchronized a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    :try_start_0
    invoke-interface {p2}, Lcom/google/android/maps/driveabout/app/d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/maps/driveabout/app/g;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/maps/driveabout/app/g;-><init>(Lcom/google/android/maps/driveabout/app/a;LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/g;->a(Lcom/google/android/maps/driveabout/app/g;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/maps/driveabout/app/d;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->j:Lcom/google/android/maps/driveabout/app/g;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/g;->e(Lcom/google/android/maps/driveabout/app/g;)Lcom/google/android/maps/driveabout/app/d;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/g;->e(Lcom/google/android/maps/driveabout/app/g;)Lcom/google/android/maps/driveabout/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/d;->a()V

    :cond_4
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    :goto_2
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/g;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private a(LO/j;)V
    .locals 0

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/app/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/g;->a()V

    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    return-object v0
.end method

.method private b(LO/j;I)V
    .locals 4

    new-instance v1, Lcom/google/android/maps/driveabout/app/c;

    invoke-direct {v1, p1}, Lcom/google/android/maps/driveabout/app/c;-><init>(LO/j;)V

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/c;->a()LO/j;

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/c;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/c;->a()LO/j;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, v2, p2}, Lcom/google/android/maps/driveabout/app/a;->c(LO/j;I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/a;)LK/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->i:LK/c;

    return-object v0
.end method

.method private c(I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {p1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v3

    const-string v0, "VoiceGuidanceEnabled"

    invoke-virtual {v3, v0, v1}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    const-string v0, "AlertMode"

    invoke-virtual {v3, v0, v1}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    :cond_0
    const-string v0, "VolumeMode"

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    if-ne v0, v1, :cond_2

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    :cond_2
    const-string v0, "AlertsDisabled"

    invoke-virtual {v3, v0, v2}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_3

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    :cond_3
    const-string v0, "AlertsMuted"

    invoke-virtual {v3, v0, v2}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_4

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method private c(LO/j;I)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v1, v0, v6}, LK/T;->a(LK/V;LK/U;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, LO/j;->a()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->e()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, LO/N;->f()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, LO/j;->d()I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {v0}, LO/N;->a()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-virtual {p1}, LO/j;->c()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    double-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/maps/driveabout/app/aK;->a(II)LK/V;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v1, v0, v6}, LK/T;->a(LK/V;LK/U;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/a;)LK/T;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    return-object v0
.end method

.method private d(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {p1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v1

    const-string v2, "VoiceGuidanceEnabled"

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    const-string v0, "Volume"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    const-string v0, "AlertMode"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    const-string v0, "VolumeMode"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    const-string v0, "AlertsDisabled"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    const-string v0, "AlertsMuted"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/a;)LK/T;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->f:LK/T;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/a;)LK/T;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->g:LK/T;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/app/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->o:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/maps/driveabout/app/a;)LK/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->h:LK/c;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/maps/driveabout/app/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/a;->o()V

    return-void
.end method

.method private declared-synchronized o()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->j:Lcom/google/android/maps/driveabout/app/g;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a(LO/j;I)LK/V;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, LO/j;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, LO/j;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, LO/j;->j()LK/m;

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    invoke-static {p1, v1, v2}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->j()LO/N;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LO/N;->e()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/aK;->a(LO/j;II)LK/V;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LO/j;->a()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;Z)LK/V;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(I)V
    .locals 4

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    invoke-virtual {v0}, LK/ac;->a()LK/ab;

    move-result-object v0

    new-instance v1, LK/d;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->v:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, v0}, LK/d;-><init>(Landroid/content/Context;Landroid/os/Handler;LK/ab;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2, v0}, LK/E;->a(Law/p;Landroid/content/Context;LK/ab;)LK/E;

    move-result-object v1

    new-instance v2, LK/u;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, LK/u;-><init>(Landroid/content/Context;LK/C;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->f:LK/T;

    const-string v1, "com.google.android.apps.networktts"

    invoke-interface {v0}, LK/ab;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->n()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/maps/driveabout/app/b;-><init>(Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(LO/j;II)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e4ccccd

    mul-float/2addr v1, v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3ecccccd

    mul-float/2addr v2, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v3, p2, v3

    int-to-float v4, v3

    neg-float v1, v1

    cmpl-float v1, v4, v1

    if-ltz v1, :cond_0

    int-to-float v1, v3

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/a;->c(LO/j;II)LK/V;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/d;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/app/d;->a()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v2, 0x7f0d0016

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, LK/s;-><init>(I)V

    invoke-static {v0, v1, v2}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/a;->o:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->m:Z

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/a;->c(I)V

    return-void
.end method

.method public b(LO/j;II)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/a;->c(LO/j;II)LK/V;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LK/T;->a(LK/V;LK/U;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->w:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->b(LO/j;I)V

    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x3

    invoke-static {v0, p1, v1}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/a;->m:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    return v0
.end method

.method public c(LO/j;II)LK/V;
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    if-ne v1, v6, :cond_2

    invoke-virtual {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, LO/j;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, LO/j;->g()LO/j;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, LO/j;->g()LO/j;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;Z)LK/V;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, LK/V;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LO/j;->i()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v4, 0x7f0d00a7

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-virtual {v2}, LK/V;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v1}, LK/V;->b()LK/m;

    move-result-object v1

    new-instance v3, LK/s;

    invoke-direct {v3, v7}, LK/s;-><init>(I)V

    invoke-virtual {v2}, LK/V;->b()LK/m;

    move-result-object v2

    invoke-static {v3, v2}, LK/m;->a(LK/m;LK/m;)LK/m;

    move-result-object v2

    invoke-static {v2}, LK/m;->a(LK/m;)LK/m;

    move-result-object v2

    invoke-static {v1, v2}, LK/m;->a(LK/m;LK/m;)LK/m;

    move-result-object v1

    invoke-static {p1, v0, v1}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    if-nez v1, :cond_3

    if-ltz p2, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/maps/driveabout/app/aK;->a(II)LK/V;

    move-result-object v1

    invoke-virtual {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, LK/W;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v3, v1, v2}, LK/W;-><init>(Landroid/content/Context;LK/V;LK/V;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    if-ne v1, v5, :cond_1

    invoke-virtual {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public c(Z)V
    .locals 4

    const/4 v3, 0x3

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/g;->c()V

    :cond_1
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    iget v0, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->d(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/g;->b()V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getVibrateSetting(I)I

    move-result v0

    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/app/a;->m:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public e()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v0}, LK/T;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public g()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v2, 0x7f0d0013

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, LK/s;-><init>(I)V

    invoke-static {v0, v1, v2}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_0
.end method

.method public h()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v2, 0x7f0d0014

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, LK/s;-><init>(I)V

    invoke-static {v0, v1, v2}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_0
.end method

.method public i()V
    .locals 3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v1, 0x7f0d0015

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LK/s;

    invoke-direct {v1, v2}, LK/s;-><init>(I)V

    invoke-static {v2, v0, v1}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_0
.end method

.method public j()LK/V;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->t:LK/V;

    return-object v0
.end method

.method public k()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/maps/driveabout/app/a;->u:J

    return-wide v0
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v0}, LK/T;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->h:LK/c;

    invoke-interface {v0}, LK/c;->b()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->i:LK/c;

    invoke-interface {v0}, LK/c;->b()V

    return-void
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    invoke-virtual {v0}, LK/ac;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method n()V
    .locals 2

    new-instance v0, LK/N;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-direct {v0, v1}, LK/N;-><init>(LK/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->w:Z

    return-void
.end method
