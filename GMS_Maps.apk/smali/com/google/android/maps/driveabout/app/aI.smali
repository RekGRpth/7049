.class public Lcom/google/android/maps/driveabout/app/aI;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field private a:LO/z;

.field private b:I

.field private final c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(LO/N;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    invoke-virtual {p1}, LO/N;->i()I

    move-result v1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    invoke-virtual {p1}, LO/N;->i()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, LO/N;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aI;->a()V

    :cond_0
    return-void
.end method

.method public a(LO/z;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    if-eq p1, v1, :cond_0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aI;->a()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    invoke-virtual {v0}, LO/z;->k()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    invoke-virtual {v1, v0}, LO/z;->a(I)LO/N;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    add-int/2addr v0, p1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/google/android/maps/driveabout/app/DirectionsListItem;

    if-nez v0, :cond_1

    :cond_0
    new-instance p2, Lcom/google/android/maps/driveabout/app/DirectionsListItem;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/google/android/maps/driveabout/app/DirectionsListItem;-><init>(Landroid/content/Context;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/aI;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/maps/driveabout/app/DirectionsListItem;->setStep(LO/N;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    invoke-virtual {v0}, LO/z;->q()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/maps/driveabout/app/DirectionsListItem;->setDistanceUnits(I)V

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p2, v0}, Lcom/google/android/maps/driveabout/app/DirectionsListItem;->setHighlighted(Z)V

    return-object p2

    :cond_1
    check-cast p2, Lcom/google/android/maps/driveabout/app/DirectionsListItem;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->a:LO/z;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aI;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aI;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
