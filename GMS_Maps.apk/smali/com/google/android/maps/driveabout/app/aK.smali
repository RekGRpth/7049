.class public Lcom/google/android/maps/driveabout/app/aK;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:I

.field private m:Landroid/content/Context;

.field private final n:Lcom/google/android/maps/driveabout/app/aN;

.field private final o:Lcom/google/android/maps/driveabout/app/aN;

.field private final p:Lcom/google/android/maps/driveabout/app/aN;

.field private final q:Lcom/google/android/maps/driveabout/app/aN;

.field private final r:Lcom/google/android/maps/driveabout/app/aN;

.field private final s:Lcom/google/android/maps/driveabout/app/aN;

.field private final t:Lcom/google/android/maps/driveabout/app/aN;

.field private final u:Lcom/google/android/maps/driveabout/app/aN;

.field private final v:Lcom/google/android/maps/driveabout/app/aN;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f0d0027

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d0026

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->n:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->o:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->p:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->q:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->r:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->s:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->t:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->u:Lcom/google/android/maps/driveabout/app/aN;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aN;-><init>(Lcom/google/android/maps/driveabout/app/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->v:Lcom/google/android/maps/driveabout/app/aN;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const-string v0, "imperial"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aK;->l:I

    :goto_0
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/aK;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d001c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d001e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d0020

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d0022

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d0024

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d001d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d001f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d0021

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d0023

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    const v1, 0x7f0d0025

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->j:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aK;->a()V

    return-void

    :cond_0
    const-string v0, "imperial_yards"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aK;->l:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aK;->l:I

    goto/16 :goto_0
.end method

.method private static a(JI)I
    .locals 5

    const/16 v4, 0x8

    shl-long v0, p0, v4

    shl-int/lit8 v2, p2, 0x7

    int-to-long v2, v2

    add-long/2addr v0, v2

    int-to-long v2, p2

    div-long/2addr v0, v2

    shr-long/2addr v0, v4

    long-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/aK;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->m:Landroid/content/Context;

    return-object v0
.end method

.method private a(IIZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    int-to-long v0, p1

    const-wide/32 v2, 0x8028

    mul-long v4, v0, v2

    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    const-wide/32 v0, 0x4c9960

    cmp-long v0, v4, v0

    if-gez v0, :cond_2

    const-wide/16 v0, 0x3

    div-long v1, v4, v0

    long-to-int v0, v1

    div-int/lit16 v0, v0, 0x2710

    mul-int/lit16 v3, v0, 0x2710

    int-to-long v3, v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x1388

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    if-eqz p3, :cond_1

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->b(II)I

    move-result v0

    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {p4, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_5

    const-wide/32 v0, 0x4c9960

    cmp-long v0, v4, v0

    if-gez v0, :cond_5

    long-to-int v0, v4

    div-int/lit16 v0, v0, 0x2710

    mul-int/lit16 v1, v0, 0x2710

    int-to-long v1, v1

    sub-long v1, v4, v1

    const-wide/16 v3, 0x1388

    cmp-long v1, v1, v3

    if-ltz v1, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    if-eqz p3, :cond_4

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->b(II)I

    move-result v0

    :cond_4
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {p4, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-wide/32 v0, 0x2fd6180

    cmp-long v0, v4, v0

    if-gez v0, :cond_6

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aK;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x509100

    invoke-static {v4, v5, v3}, Lcom/google/android/maps/driveabout/app/aK;->a(JI)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p5, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-wide/32 v0, 0x325aa00

    cmp-long v0, v4, v0

    if-gtz v0, :cond_7

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aK;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p5, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const-wide/32 v0, 0x325aa00

    div-long v2, v4, v0

    const-wide/32 v0, 0x325aa00

    rem-long v0, v4, v0

    const-wide/16 v4, 0xa

    cmp-long v4, v2, v4

    if-ltz v4, :cond_9

    const-wide/32 v4, 0x192d500

    cmp-long v4, v0, v4

    if-ltz v4, :cond_9

    const-wide/16 v0, 0x1

    add-long/2addr v2, v0

    const-wide/16 v0, 0x0

    :cond_8
    :goto_1
    const-wide/16 v4, 0xa

    cmp-long v4, v2, v4

    if-ltz v4, :cond_a

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p5, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    const v4, 0x509100

    invoke-static {v0, v1, v4}, Lcom/google/android/maps/driveabout/app/aK;->a(JI)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v4, 0xa

    cmp-long v4, v0, v4

    if-nez v4, :cond_8

    const-wide/16 v0, 0x1

    add-long/2addr v2, v0

    const-wide/16 v0, 0x0

    goto :goto_1

    :cond_a
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aK;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {p5, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/16 v0, 0xa

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/16 v1, 0x3cf

    if-ge p1, v1, :cond_2

    if-eqz p2, :cond_0

    const/16 v1, 0x12c

    if-ge p1, v1, :cond_1

    :goto_0
    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/aK;->b(II)I

    move-result p1

    :cond_0
    new-array v0, v6, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    const/16 v0, 0x32

    goto :goto_0

    :cond_2
    const/16 v1, 0x3e8

    if-gt p1, v1, :cond_3

    new-array v0, v6, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aK;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p4, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v4, v3, -0x3

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-lt v3, v0, :cond_5

    const/16 v4, 0x1f4

    if-lt v1, v4, :cond_5

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v2

    :cond_4
    :goto_2
    if-lt v3, v0, :cond_6

    new-array v0, v6, [Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p4, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    int-to-long v4, v1

    const/16 v1, 0x64

    invoke-static {v4, v5, v1}, Lcom/google/android/maps/driveabout/app/aK;->a(JI)I

    move-result v1

    if-ne v1, v0, :cond_4

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v2

    goto :goto_2

    :cond_6
    new-array v0, v6, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/aK;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p4, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private a(Lcom/google/android/maps/driveabout/app/aN;ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aN;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/aM;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/aM;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/maps/driveabout/app/aM;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot format distance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private a()V
    .locals 13

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/google/android/maps/driveabout/app/aN;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->n:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->o:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->p:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x3

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    const/4 v0, 0x3

    new-array v3, v0, [I

    fill-array-data v3, :array_1

    const/4 v0, 0x3

    new-array v4, v0, [I

    fill-array-data v4, :array_2

    const/4 v0, 0x0

    :goto_0
    array-length v5, v1

    if-ge v0, v5, :cond_0

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x5f

    aget v8, v2, v0

    const-string v9, "50"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x91

    aget v8, v2, v0

    const-string v9, "100"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0xbe

    aget v8, v2, v0

    const-string v9, "150"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x118

    aget v8, v2, v0

    const-string v9, "200"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x172

    aget v8, v2, v0

    const-string v9, "300"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x1cc

    aget v8, v2, v0

    const-string v9, "400"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x226

    aget v8, v2, v0

    const-string v9, "500"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x2ee

    aget v8, v2, v0

    const-string v9, "600"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x3b6

    aget v8, v2, v0

    const-string v9, "800"

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aO;

    const/16 v7, 0x514

    aget v8, v3, v0

    const/high16 v9, 0x447a0000

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aO;-><init>(Lcom/google/android/maps/driveabout/app/aK;IIF)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aL;

    const/16 v7, 0x73a

    aget v8, v4, v0

    const/4 v9, 0x0

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v5, v1, v0

    new-instance v6, Lcom/google/android/maps/driveabout/app/aO;

    const v7, 0x7fffffff

    aget v8, v3, v0

    const/high16 v9, 0x447a0000

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/android/maps/driveabout/app/aO;-><init>(Lcom/google/android/maps/driveabout/app/aK;IIF)V

    invoke-virtual {v5, v6}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_0
    const/4 v0, 0x3

    new-array v1, v0, [Lcom/google/android/maps/driveabout/app/aN;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->q:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->r:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->s:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x3

    new-array v2, v0, [I

    fill-array-data v2, :array_3

    const/4 v0, 0x3

    new-array v3, v0, [I

    fill-array-data v3, :array_4

    const/4 v0, 0x3

    new-array v4, v0, [I

    fill-array-data v4, :array_5

    const/4 v0, 0x3

    new-array v5, v0, [I

    fill-array-data v5, :array_6

    const/4 v0, 0x3

    new-array v6, v0, [I

    fill-array-data v6, :array_7

    const/4 v0, 0x3

    new-array v7, v0, [I

    fill-array-data v7, :array_8

    const/4 v0, 0x0

    :goto_1
    array-length v8, v1

    if-ge v0, v8, :cond_1

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x41e7a5e4

    aget v11, v2, v0

    const-string v12, "50"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x4230c8b4

    aget v11, v2, v0

    const-string v12, "100"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x4267a5e4

    aget v11, v2, v0

    const-string v12, "150"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x42aab021

    aget v11, v2, v0

    const-string v12, "200"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x42e18d50

    aget v11, v2, v0

    const-string v12, "300"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x430c3540

    aget v11, v2, v0

    const-string v12, "400"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x4327a3d7

    aget v11, v2, v0

    const-string v12, "500"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x43618d50

    aget v11, v2, v0

    const-string v12, "600"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x4390c7ae

    aget v11, v2, v0

    const-string v12, "800"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x43c61eb8

    aget v11, v2, v0

    const-string v12, "1000"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x44350d1b

    aget v11, v4, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x448cd14e

    aget v11, v5, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x44bf1c0f

    aget v11, v6, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aO;

    const v10, 0x4502c25b

    aget v11, v3, v0

    const v12, 0x44c92b02

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aO;-><init>(Lcom/google/android/maps/driveabout/app/aK;FIF)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x45350d1b

    aget v11, v7, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aO;

    const v10, 0x7fffffff

    aget v11, v3, v0

    const v12, 0x44c92b02

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aO;-><init>(Lcom/google/android/maps/driveabout/app/aK;IIF)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_1
    const/4 v0, 0x3

    new-array v1, v0, [Lcom/google/android/maps/driveabout/app/aN;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->t:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->u:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aK;->v:Lcom/google/android/maps/driveabout/app/aN;

    aput-object v2, v1, v0

    const/4 v0, 0x3

    new-array v2, v0, [I

    fill-array-data v2, :array_9

    const/4 v0, 0x0

    :goto_2
    array-length v8, v1

    if-ge v0, v8, :cond_2

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x42adbc6b

    aget v11, v2, v0

    const-string v12, "50"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x43049688

    aget v11, v2, v0

    const-string v12, "100"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x432dbc6b

    aget v11, v2, v0

    const-string v12, "150"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x43800419

    aget v11, v2, v0

    const-string v12, "200"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x43a929fc

    aget v11, v2, v0

    const-string v12, "300"

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x44350d1b

    aget v11, v4, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x448cd14e

    aget v11, v5, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x44bf1c0f

    aget v11, v6, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aO;

    const v10, 0x4502c25b

    aget v11, v3, v0

    const v12, 0x44c92b02

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aO;-><init>(Lcom/google/android/maps/driveabout/app/aK;FIF)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aL;

    const v10, 0x45350d1b

    aget v11, v7, v0

    const/4 v12, 0x0

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aL;-><init>(Lcom/google/android/maps/driveabout/app/aK;FILjava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    aget-object v8, v1, v0

    new-instance v9, Lcom/google/android/maps/driveabout/app/aO;

    const v10, 0x7fffffff

    aget v11, v3, v0

    const v12, 0x44c92b02

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/google/android/maps/driveabout/app/aO;-><init>(Lcom/google/android/maps/driveabout/app/aK;IIF)V

    invoke-virtual {v8, v9}, Lcom/google/android/maps/driveabout/app/aN;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :cond_2
    return-void

    :array_0
    .array-data 4
        0x7f0d00a9
        0x7f0d00ab
        0x7f0d00ad
    .end array-data

    :array_1
    .array-data 4
        0x7f0e0003
        0x7f0e0004
        0x7f0e0005
    .end array-data

    :array_2
    .array-data 4
        0x7f0d00aa
        0x7f0d00ac
        0x7f0d00ae
    .end array-data

    :array_3
    .array-data 4
        0x7f0d00b0
        0x7f0d00b6
        0x7f0d00bc
    .end array-data

    :array_4
    .array-data 4
        0x7f0e0006
        0x7f0e0007
        0x7f0e0008
    .end array-data

    :array_5
    .array-data 4
        0x7f0d00b2
        0x7f0d00b8
        0x7f0d00be
    .end array-data

    :array_6
    .array-data 4
        0x7f0d00b3
        0x7f0d00b9
        0x7f0d00bf
    .end array-data

    :array_7
    .array-data 4
        0x7f0d00b4
        0x7f0d00ba
        0x7f0d00c0
    .end array-data

    :array_8
    .array-data 4
        0x7f0d00b5
        0x7f0d00bb
        0x7f0d00c1
    .end array-data

    :array_9
    .array-data 4
        0x7f0d00b1
        0x7f0d00b7
        0x7f0d00bd
    .end array-data
.end method

.method private static b(II)I
    .locals 1

    shr-int/lit8 v0, p1, 0x1

    add-int/2addr v0, p0

    div-int/2addr v0, p1

    mul-int/2addr v0, p1

    return v0
.end method

.method private b(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v2, 0x2

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aK;->a(IIZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v2, 0x3

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aK;->a(IIZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(I)I
    .locals 0

    if-nez p1, :cond_0

    iget p1, p0, Lcom/google/android/maps/driveabout/app/aK;->l:I

    :cond_0
    return p1
.end method

.method public a(II)LK/V;
    .locals 4

    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/aK;->a(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->n:Lcom/google/android/maps/driveabout/app/aN;

    :goto_0
    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-direct {p0, v0, p1, v3}, Lcom/google/android/maps/driveabout/app/aK;->a(Lcom/google/android/maps/driveabout/app/aN;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, LK/q;

    invoke-direct {v3, p1, v1}, LK/q;-><init>(II)V

    invoke-static {v2, v0, v3}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->q:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->t:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(LO/j;II)LK/V;
    .locals 3

    invoke-virtual {p0, p3}, Lcom/google/android/maps/driveabout/app/aK;->a(I)I

    move-result v2

    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->r()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    packed-switch v2, :pswitch_data_0

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->o:Lcom/google/android/maps/driveabout/app/aN;

    :goto_1
    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/maps/driveabout/app/aK;->a(Lcom/google/android/maps/driveabout/app/aN;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LK/s;

    const/16 v2, 0xb

    invoke-direct {v1, v2}, LK/s;-><init>(I)V

    invoke-static {p1, v0, v1}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    invoke-virtual {v0}, LO/P;->c()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :pswitch_0
    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->r:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->s:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_1

    :pswitch_1
    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->u:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->v:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->p:Lcom/google/android/maps/driveabout/app/aN;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(IIZI)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x1

    if-gez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/aK;->a(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    if-ne p4, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aK;->d:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->a(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aK;->i:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->a(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    if-ne p4, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aK;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->b(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aK;->f:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->b(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    if-ne p4, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aK;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->c(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aK;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aK;->f:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/aK;->c(IZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-string v0, ""

    goto :goto_0
.end method
