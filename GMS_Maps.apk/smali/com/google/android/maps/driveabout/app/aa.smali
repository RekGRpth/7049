.class Lcom/google/android/maps/driveabout/app/aa;
.super Lcom/google/android/maps/driveabout/app/T;
.source "SourceFile"


# instance fields
.field e:Ljava/lang/Runnable;

.field private final f:Lcom/google/android/maps/driveabout/app/af;

.field private final g:Ljava/util/ArrayList;

.field private final h:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZZ)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/T;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/maps/driveabout/app/ab;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ab;-><init>(Lcom/google/android/maps/driveabout/app/aa;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->e:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/aa;->f:Lcom/google/android/maps/driveabout/app/af;

    if-eqz p3, :cond_2

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00ce

    const v2, 0x7f020144

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00cd

    const v2, 0x7f020146

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00cf

    const v2, 0x7f020142

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00d0

    const v2, 0x7f020145

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->a()Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aa;->a(Ljava/util/ArrayList;)V

    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/app/aa;->h:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aa;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_2
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f04002f

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/Y;->d(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private b()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/ac;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ac;-><init>(Lcom/google/android/maps/driveabout/app/aa;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ac;->start()V

    return-void
.end method


# virtual methods
.method public a(LaH/h;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aa;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aa;->e:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    if-eqz v2, :cond_5

    if-eqz p1, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    invoke-virtual {v2, p1}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x407f400000000000L

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    if-eqz v3, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    if-eqz v0, :cond_3

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aa;->b()V

    :cond_4
    return-void

    :cond_5
    move v2, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Lo/u;LO/U;Ljava/util/ArrayList;)V
    .locals 5

    const v4, 0x7f0d00d5

    const v2, 0x7f0d00d4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aa;->h:Z

    if-eqz v0, :cond_3

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/Y;->b(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    const/4 v0, 0x4

    invoke-static {p2, v0}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/U;

    const/4 v3, 0x5

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->f:Lcom/google/android/maps/driveabout/app/af;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2}, Lcom/google/android/maps/driveabout/app/af;->a(Lo/u;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    if-lez v2, :cond_5

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aa;->h:Z

    if-eqz v2, :cond_6

    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/Y;->b(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/U;

    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/aa;->b(Ljava/util/ArrayList;)V

    return-void
.end method

.method public a_(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/maps/driveabout/app/aa;->c:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aa;->b()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
