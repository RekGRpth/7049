.class Lcom/google/android/maps/driveabout/app/ad;
.super Lcom/google/android/maps/driveabout/app/T;
.source "SourceFile"


# instance fields
.field private e:Z


# direct methods
.method constructor <init>(Landroid/content/Context;LaH/h;Z)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/T;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0d00d0

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->a()Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/ad;->a(Ljava/util/ArrayList;)V

    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/ad;->a(LaH/h;)V

    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/StarredItemProvider;->b:Landroid/net/Uri;

    sget-object v2, Lcom/google/googlenav/provider/StarredItemProvider;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/ad;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/ad;->b()V

    return-void
.end method

.method private b()V
    .locals 11

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ad;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/StarredItemProvider;->b:Landroid/net/Uri;

    sget-object v2, Lcom/google/googlenav/provider/StarredItemProvider;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d00d0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v5, :cond_8

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v8, v0, [Lcom/google/android/maps/driveabout/app/Y;

    monitor-enter p0

    move v0, v6

    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x4

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v1, 0x5

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-nez v2, :cond_2

    if-eqz v4, :cond_6

    :cond_2
    new-instance v1, Lo/u;

    invoke-direct {v1, v2, v4}, Lo/u;-><init>(II)V

    move-object v4, v1

    :goto_1
    const/4 v1, 0x2

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x6

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v2, :cond_3

    if-eqz v1, :cond_1

    :cond_3
    if-nez v1, :cond_4

    move-object v1, v2

    :cond_4
    new-instance v9, LO/U;

    const/4 v10, 0x0

    invoke-direct {v9, v1, v4, v2, v10}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    invoke-static {v9, v1}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v1, :cond_5

    if-eqz v4, :cond_5

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v1, v4}, LaH/h;->b(Lo/u;)F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/Y;->a(F)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v1, v4}, LaH/h;->a(Lo/u;)F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/Y;->b(F)V

    :cond_5
    add-int/lit8 v1, v0, 0x1

    aput-object v2, v8, v0

    move v0, v1

    goto :goto_0

    :cond_6
    move-object v4, v3

    goto :goto_1

    :cond_7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->k()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v8, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_9

    const v0, 0x7f0d00dc

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/Y;->c(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/app/ad;->b(Ljava/util/ArrayList;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_a
    move v0, v6

    goto :goto_2
.end method


# virtual methods
.method public a(LaH/h;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v2, :cond_4

    if-eqz p1, :cond_4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v2, p1}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v2

    float-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4

    move v3, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v2, :cond_5

    if-eqz p1, :cond_5

    invoke-virtual {p1}, LaH/h;->e()J

    move-result-wide v4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v2}, LaH/h;->e()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x40c3880000000000L

    cmpl-double v2, v4, v6

    if-lez v2, :cond_5

    move v2, v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v4, :cond_0

    if-nez v2, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->a(LaH/h;)V

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/maps/driveabout/app/ae;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ae;-><init>(Lcom/google/android/maps/driveabout/app/ad;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ae;->start()V

    :cond_3
    return-void

    :cond_4
    move v3, v0

    goto :goto_0

    :cond_5
    move v2, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
