.class Lcom/google/android/maps/driveabout/app/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field final a:I

.field final b:Lo/u;

.field final c:LO/U;

.field final d:J

.field final e:I

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "time"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dest_lat"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "dest_lng"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "dest_address"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "dest_title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "dest_token"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "source_lat"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "source_lng"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "day_of_week"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "hour_of_day"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/app/ai;->g:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(ILo/u;LO/U;JII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/maps/driveabout/app/ai;->a:I

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/ai;->b:Lo/u;

    iput-wide p4, p0, Lcom/google/android/maps/driveabout/app/ai;->d:J

    iput p6, p0, Lcom/google/android/maps/driveabout/app/ai;->e:I

    iput p7, p0, Lcom/google/android/maps/driveabout/app/ai;->f:I

    return-void
.end method

.method static a(ILandroid/database/Cursor;)Lcom/google/android/maps/driveabout/app/ai;
    .locals 13

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v3, 0x7

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    new-instance v3, LO/U;

    new-instance v12, LO/V;

    invoke-direct {v12, v2}, LO/V;-><init>(Ljava/lang/String;)V

    new-instance v2, Lo/u;

    invoke-direct {v2, v0, v1}, Lo/u;-><init>(II)V

    invoke-direct {v3, v12, v2, v8, v9}, LO/U;-><init>(LO/V;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lo/u;

    invoke-direct {v2, v10, v11}, Lo/u;-><init>(II)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ai;

    move v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/ai;-><init>(ILo/u;LO/U;JII)V

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/ai;->g:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "time"

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/ai;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "dest_lat"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    invoke-virtual {v2}, Lo/u;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "dest_lng"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    invoke-virtual {v2}, Lo/u;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "dest_address"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-virtual {v2}, LO/U;->d()LO/V;

    move-result-object v2

    invoke-virtual {v2}, LO/V;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "dest_title"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-virtual {v2}, LO/U;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "dest_token"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-virtual {v2}, LO/U;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "source_lat"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->b:Lo/u;

    invoke-virtual {v2}, Lo/u;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "source_lng"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ai;->b:Lo/u;

    invoke-virtual {v2}, Lo/u;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "day_of_week"

    iget v2, p0, Lcom/google/android/maps/driveabout/app/ai;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "hour_of_day"

    iget v2, p0, Lcom/google/android/maps/driveabout/app/ai;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method
