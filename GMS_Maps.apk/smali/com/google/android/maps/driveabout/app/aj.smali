.class Lcom/google/android/maps/driveabout/app/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lo/u;

.field final b:LO/V;

.field final c:Ljava/lang/String;


# direct methods
.method constructor <init>(LO/U;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, LO/U;->c()Lo/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aj;->a:Lo/u;

    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/app/aj;)Z
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aj;->a:Lo/u;

    invoke-static {v1}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/maps/driveabout/app/aj;->a:Lo/u;

    invoke-static {v2}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1}, Lo/T;->e()D

    move-result-wide v4

    div-double v1, v2, v4

    const-wide/high16 v3, 0x4069000000000000L

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/maps/driveabout/app/aj;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aj;->a:Lo/u;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/aj;->a:Lo/u;

    invoke-virtual {v2, v3}, Lo/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    invoke-virtual {v2, v3}, LO/V;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aj;->a:Lo/u;

    invoke-virtual {v0}, Lo/u;->hashCode()I

    move-result v0

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aj;->b:LO/V;

    invoke-virtual {v0}, LO/V;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
