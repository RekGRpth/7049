.class public Lcom/google/android/maps/driveabout/app/bC;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[Landroid/graphics/Bitmap;

.field private final b:Landroid/content/Context;

.field private final c:Law/p;

.field private d:Lcom/google/android/maps/driveabout/app/bF;

.field private final e:Lcom/google/android/maps/driveabout/app/bI;

.field private final f:Lcom/google/android/maps/driveabout/app/bI;

.field private final g:Lcom/google/android/maps/driveabout/app/bI;

.field private final h:Lcom/google/android/maps/driveabout/app/bI;

.field private final i:Ljava/util/LinkedList;

.field private j:I

.field private k:Lo/aR;


# direct methods
.method public constructor <init>(Landroid/content/Context;Law/p;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bC;->j:I

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/bC;->c:Law/p;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bC;->h()Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->e:Lcom/google/android/maps/driveabout/app/bI;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->o()Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    new-instance v0, Lcom/google/android/maps/driveabout/app/bI;

    const-string v1, "__RECENTS"

    const-string v2, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/bI;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/bD;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    new-instance v0, Lcom/google/android/maps/driveabout/app/bI;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/bI;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/bD;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->e:Lcom/google/android/maps/driveabout/app/bI;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const v3, 0x7f020131

    invoke-static {v0, v3}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f02012c

    invoke-static {v0, v3}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f020134

    invoke-static {v0, v3}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->a:[Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->n()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->p()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bC;->a()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "layerDisplayName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "isActive"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "isSearch"

    aput-object v2, v0, v1

    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    const-string v0, "DATA_PROTO_SAVED_LAYER_STATE"

    invoke-static {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/bC;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/MatrixCursor;)V

    const-string v0, "DATA_PROTO_SAVED_RECENT_LAYERS"

    invoke-static {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/bC;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/MatrixCursor;)V

    return-object v1
.end method

.method private static a(Landroid/content/Context;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x2

    :try_start_0
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v2, 0x3

    :try_start_2
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v3, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x4

    if-eq v3, v4, :cond_3

    :cond_2
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :try_start_4
    aget-byte v1, v2, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    const/4 v3, 0x1

    aget-byte v3, v2, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v1, v3

    const/4 v3, 0x2

    aget-byte v3, v2, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v1, v3

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DATA_LAYER_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {p0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    const/16 v3, 0x1000

    invoke-direct {v1, v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ag;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_4

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_4

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_0

    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_0

    :try_start_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    if-eqz v1, :cond_5

    :try_start_9
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :cond_5
    :goto_3
    throw v0

    :catch_5
    move-exception v1

    goto/16 :goto_0

    :catch_6
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bC;)Lo/aR;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->k:Lo/aR;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Landroid/database/MatrixCursor;)V
    .locals 11

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {p0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    const/16 v5, 0x1000

    invoke-direct {v1, v0, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    move v2, v4

    :goto_0
    if-ge v2, v6, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Landroid/content/Context;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    const/4 v8, 0x5

    invoke-static {v0, v8}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sub-int v10, v6, v2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v7, v8, v9

    const/4 v7, 0x2

    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v7

    const/4 v0, 0x3

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v8, v0

    invoke-virtual {p2, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_2
    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_3
    const-string v2, "LayerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error creating cursor from GMM Layer File: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_5
    throw v0

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/bC;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bI;->d(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/maps/driveabout/app/bE;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->c(Lcom/google/android/maps/driveabout/app/bE;)V

    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 13

    const/4 v1, 0x0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/app/bI;->c(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    invoke-static {v3, v0}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "SQLiteException reading from recent layers"

    invoke-static {v2, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bC;->j()Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v9

    if-nez v9, :cond_3

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v9, :cond_1

    :try_start_7
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2

    :cond_3
    :try_start_8
    const-string v0, "layerDisplayName"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-string v0, "isActive"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    if-ltz v10, :cond_4

    if-gez v11, :cond_5

    :cond_4
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    if-eqz v9, :cond_1

    :try_start_9
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2

    :cond_5
    :try_start_a
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/bI;->d(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v12, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    new-instance v0, Lcom/google/android/maps/driveabout/app/bE;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const v6, 0x7f020129

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v3, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bE;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZIZI)V

    invoke-static {v12, v0}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    :cond_6
    if-eqz p1, :cond_7

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/bC;->a(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bI;->j()I

    move-result v0

    const/16 v1, 0x14

    if-lt v0, v1, :cond_5

    :cond_8
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    if-eqz v9, :cond_1

    :try_start_b
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_3

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_9

    :try_start_c
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_5

    :cond_9
    :goto_3
    throw v0

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v9

    goto/16 :goto_1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/bC;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->q()V

    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v4, 0x7

    const/4 v0, 0x0

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/bC;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->q()V

    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/bC;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    return-object v0
.end method

.method private declared-synchronized c(Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/bC;->d(Lcom/google/android/maps/driveabout/app/bE;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bC;->j:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/bI;->c(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->d(Lcom/google/android/maps/driveabout/app/bE;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->e()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->c:Law/p;

    new-instance v1, Lcom/google/android/maps/driveabout/app/bJ;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/bJ;-><init>(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/bE;)V

    invoke-interface {v0, v1}, Law/p;->c(Law/g;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Landroid/graphics/Bitmap;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/android/maps/driveabout/app/bE;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/android/maps/driveabout/app/bE;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()Landroid/graphics/Bitmap;
    .locals 5

    const/4 v1, 0x0

    monitor-enter p0

    move v2, v1

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->a:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/bE;->c(I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/bC;->a:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v2

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->a:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    monitor-exit p0

    return-object v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized l()Z
    .locals 5

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/app/bI;->c(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/android/maps/driveabout/app/bE;Z)Z

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private m()V
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/android/maps/driveabout/app/bE;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    const-string v2, "ActiveDefaultLayers"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private n()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bI;->j()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bI;->d(I)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->c(Lcom/google/android/maps/driveabout/app/bE;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private o()Lcom/google/android/maps/driveabout/app/bI;
    .locals 10

    const/4 v4, 0x0

    new-instance v9, Lcom/google/android/maps/driveabout/app/bI;

    const-string v0, "__SYSTEM_LAYERS"

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {v9, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bI;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/bD;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    const v1, 0x7f0d003d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Lcom/google/android/maps/driveabout/app/bE;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    const-string v2, "TrafficIncident"

    const v6, 0x7f020128

    move v5, v4

    move v7, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bE;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZIZI)V

    invoke-static {v9, v0}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    return-object v9
.end method

.method private p()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    const-string v1, "ActiveDefaultLayers"

    const-string v2, ""

    invoke-static {v0, v1, v2}, LR/s;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-direct {p0, v3}, Lcom/google/android/maps/driveabout/app/bC;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private q()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->d:Lcom/google/android/maps/driveabout/app/bF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->d:Lcom/google/android/maps/driveabout/app/bF;

    invoke-interface {v0, p0}, Lcom/google/android/maps/driveabout/app/bF;->a(Lcom/google/android/maps/driveabout/app/bC;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/bD;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/bD;-><init>(Lcom/google/android/maps/driveabout/app/bC;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bD;->start()V

    return-void
.end method

.method public a(I)V
    .locals 3

    const/high16 v2, 0x41000000

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->g:Lcom/google/android/maps/driveabout/app/bI;

    const-string v1, "TrafficIncident"

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bI;->c(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->b(Lcom/google/android/maps/driveabout/app/bE;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    const/high16 v1, 0x41f00000

    invoke-virtual {v0, v2, v1}, Lcom/google/android/maps/driveabout/app/bE;->a(FF)V

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Lcom/google/android/maps/driveabout/app/bE;)V

    goto :goto_0

    :cond_1
    const/high16 v1, 0x41700000

    invoke-virtual {v0, v2, v1}, Lcom/google/android/maps/driveabout/app/bE;->a(FF)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/bC;->c(Lcom/google/android/maps/driveabout/app/bE;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->m()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->q()V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bF;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bC;->d:Lcom/google/android/maps/driveabout/app/bF;

    return-void
.end method

.method public a(Lo/aR;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bC;->k:Lo/aR;

    return-void
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 5

    const/4 v1, 0x1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->h:Lcom/google/android/maps/driveabout/app/bI;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/bI;->c(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/maps/driveabout/app/bE;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/android/maps/driveabout/app/bE;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bE;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/android/maps/driveabout/app/bE;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Lcom/google/android/maps/driveabout/app/bI;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->e:Lcom/google/android/maps/driveabout/app/bI;

    return-object v0
.end method

.method public b(Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/bC;->d(Lcom/google/android/maps/driveabout/app/bE;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->m()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->q()V

    return-void
.end method

.method public c()Lcom/google/android/maps/driveabout/app/bI;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->f:Lcom/google/android/maps/driveabout/app/bI;

    return-object v0
.end method

.method public declared-synchronized d()[Lcom/google/android/maps/driveabout/app/bE;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/bE;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/app/bE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->q()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->m()V

    :cond_0
    return-void
.end method

.method public declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->l()Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->q()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bC;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method h()Lcom/google/android/maps/driveabout/app/bI;
    .locals 15

    new-instance v10, Lcom/google/android/maps/driveabout/app/bI;

    const-string v0, "__DEFAULT"

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {v10, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bI;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/bD;)V

    const/4 v0, 0x4

    new-array v11, v0, [Lcom/google/android/maps/driveabout/app/bG;

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/bG;

    const v1, 0x7f0d008a

    const v2, 0x7f0d008a

    const v3, 0x7f020124

    const v4, 0x7f020130

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bG;-><init>(IIII[I)V

    aput-object v0, v11, v6

    const/4 v6, 0x1

    new-instance v0, Lcom/google/android/maps/driveabout/app/bG;

    const v1, 0x7f0d008c

    const v2, 0x7f0d008c

    const v3, 0x7f020123

    const v4, 0x7f02012f

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bG;-><init>(IIII[I)V

    aput-object v0, v11, v6

    const/4 v6, 0x2

    new-instance v0, Lcom/google/android/maps/driveabout/app/bG;

    const v1, 0x7f0d008f

    const v2, 0x7f0d0091

    const v3, 0x7f02011f

    const v4, 0x7f02012a

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bG;-><init>(IIII[I)V

    aput-object v0, v11, v6

    const/4 v6, 0x3

    new-instance v0, Lcom/google/android/maps/driveabout/app/bG;

    const v1, 0x7f0d008d

    const v2, 0x7f0d008d

    const v3, 0x7f020126

    const v4, 0x7f020132

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bG;-><init>(IIII[I)V

    aput-object v0, v11, v6

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ja"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v6, 0x2

    new-instance v0, Lcom/google/android/maps/driveabout/app/bG;

    const v1, 0x7f0d0090

    const v2, 0x7f0d0090

    const v3, 0x7f020122

    const v4, 0x7f02012d

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bG;-><init>(IIII[I)V

    aput-object v0, v11, v6

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/bG;

    const v1, 0x7f0d008b

    const v2, 0x7f0d008b

    const v3, 0x7f020120

    const v4, 0x7f02012b

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bG;-><init>(IIII[I)V

    aput-object v0, v11, v6

    :cond_1
    array-length v12, v11

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v12, :cond_3

    aget-object v13, v11, v9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    iget v1, v13, Lcom/google/android/maps/driveabout/app/bG;->a:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    iget v1, v13, Lcom/google/android/maps/driveabout/app/bG;->b:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v7, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/bE;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    iget v6, v13, Lcom/google/android/maps/driveabout/app/bG;->c:I

    iget v8, v13, Lcom/google/android/maps/driveabout/app/bG;->d:I

    move-object v3, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bE;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZIZI)V

    if-eq v2, v14, :cond_2

    invoke-virtual {v0, v14}, Lcom/google/android/maps/driveabout/app/bE;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v1, v13, Lcom/google/android/maps/driveabout/app/bG;->e:[I

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bE;->a([I)Lcom/google/android/maps/driveabout/app/bE;

    invoke-static {v10, v0}, Lcom/google/android/maps/driveabout/app/bI;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;)V

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_3
    return-object v10
.end method

.method protected i()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    const-string v1, "DA_LayerInfo"

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dB;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, v1, v2}, LJ/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/bC;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bC;->c:Law/p;

    new-instance v2, Lcom/google/android/maps/driveabout/app/bH;

    invoke-direct {v2, p0, v0}, Lcom/google/android/maps/driveabout/app/bH;-><init>(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v1, v2}, Law/p;->c(Law/g;)V

    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method j()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bC;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
