.class public Lcom/google/android/maps/driveabout/app/bK;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/bF;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/bC;

.field private final b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

.field private final c:Lcom/google/android/maps/driveabout/vector/A;

.field private d:Lk/b;

.field private final e:Lu/d;

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/Bitmap;

.field private i:Ljava/util/HashSet;

.field private final j:Ljava/util/List;

.field private volatile k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/NavigationMapView;Lcom/google/android/maps/driveabout/vector/A;LA/c;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bK;->a:Lcom/google/android/maps/driveabout/app/bC;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/bK;->c:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->i:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    new-instance v0, Lu/d;

    invoke-direct {v0, p4, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    new-instance v1, LA/a;

    invoke-direct {v1}, LA/a;-><init>()V

    invoke-virtual {v0, v1}, Lu/d;->a(LA/b;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    new-instance v1, Lcom/google/android/maps/driveabout/app/bN;

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/bN;-><init>(Lcom/google/android/maps/driveabout/app/bK;Lcom/google/android/maps/driveabout/app/bL;)V

    invoke-virtual {v0, v1}, Lu/d;->a(Lu/i;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bK;->f:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bK;->g:I

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020133

    invoke-static {v0, v1}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->h:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method a(LC/a;)V
    .locals 9

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->a:Lcom/google/android/maps/driveabout/app/bC;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->d()[Lcom/google/android/maps/driveabout/app/bE;

    move-result-object v4

    move v1, v2

    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_1

    aget-object v5, v4, v1

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/bE;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    invoke-virtual {v5, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    new-instance v7, Lr/s;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/bE;->i()Lo/J;

    move-result-object v8

    invoke-direct {v7, v0, v8}, Lr/s;-><init>(Lo/aq;Lo/J;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/bK;->i:Ljava/util/HashSet;

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move v1, v2

    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/s;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v0, v4}, Lcom/google/android/maps/driveabout/app/bK;->a(Lr/s;[Lcom/google/android/maps/driveabout/app/bE;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q()LF/H;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v1, v0, Lcom/google/android/maps/driveabout/app/bM;

    if-eqz v1, :cond_5

    check-cast v0, Lcom/google/android/maps/driveabout/app/bM;

    move v1, v2

    :goto_4
    array-length v3, v4

    if-ge v1, v3, :cond_8

    aget-object v3, v4, v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bM;->t()Lcom/google/android/maps/driveabout/app/bE;

    move-result-object v7

    if-ne v3, v7, :cond_7

    const/4 v0, 0x1

    :goto_5
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p()V

    :cond_5
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->c:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v1, Lcom/google/android/maps/driveabout/app/bO;

    invoke-direct {v1, v5}, Lcom/google/android/maps/driveabout/app/bO;-><init>(Ljava/util/HashSet;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/J;Z)V

    :cond_6
    iput-object v6, p0, Lcom/google/android/maps/driveabout/app/bK;->i:Ljava/util/HashSet;

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    return-void

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_5
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/bK;->a(LC/a;)V

    :cond_0
    return-void
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->d:Lk/b;

    if-nez v0, :cond_0

    new-instance v0, Lk/b;

    new-instance v1, LA/a;

    invoke-direct {v1}, LA/a;-><init>()V

    invoke-direct {v0, v1}, Lk/b;-><init>(LA/b;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->d:Lk/b;

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bC;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->postInvalidate()V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->b(Z)V

    return-void
.end method

.method a(Lr/s;[Lcom/google/android/maps/driveabout/app/bE;)Z
    .locals 13

    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->b(Lo/aq;)LF/T;

    move-result-object v0

    if-eqz v0, :cond_0

    move v1, v9

    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_6

    aget-object v2, p2, v1

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bE;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lr/s;->l()Lo/J;

    move-result-object v3

    invoke-virtual {v3}, Lo/J;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    aget-object v6, p2, v1

    :goto_1
    if-nez v6, :cond_2

    move v9, v10

    :cond_0
    :goto_2
    return v9

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    check-cast v0, LF/Y;

    new-instance v11, Lcom/google/android/maps/driveabout/vector/aF;

    invoke-direct {v11}, Lcom/google/android/maps/driveabout/vector/aF;-><init>()V

    invoke-virtual {v0, v11}, LF/Y;->a(Lcom/google/android/maps/driveabout/vector/aF;)Z

    :cond_3
    :goto_3
    invoke-virtual {v11}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v11}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v0

    invoke-virtual {v0}, Ly/c;->a()Lo/n;

    move-result-object v5

    instance-of v0, v5, Lo/U;

    if-eqz v0, :cond_3

    check-cast v5, Lo/U;

    :try_start_0
    invoke-virtual {v5}, Lo/U;->p()Lo/H;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v0

    invoke-virtual {v0}, Lo/I;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_4
    invoke-virtual {v6, v0}, Lcom/google/android/maps/driveabout/app/bE;->c(I)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/bE;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bK;->h:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/bK;->f:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bK;->g:I

    :goto_5
    iget-object v12, p0, Lcom/google/android/maps/driveabout/app/bK;->c:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, Lcom/google/android/maps/driveabout/app/bM;

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bM;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILo/U;Lcom/google/android/maps/driveabout/app/bE;Lr/s;Lcom/google/android/maps/driveabout/app/bL;)V

    invoke-virtual {v12, v0}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    goto :goto_3

    :catch_0
    move-exception v0

    move v0, v9

    goto :goto_4

    :cond_4
    move v9, v10

    goto :goto_2

    :cond_5
    move-object v2, v8

    goto :goto_5

    :cond_6
    move-object v6, v8

    goto :goto_1
.end method

.method public b(LC/a;LD/a;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->a:Lcom/google/android/maps/driveabout/app/bC;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->e()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->d:Lk/b;

    invoke-virtual {v0, p1}, Lk/b;->a(LC/a;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-static {v1, v0}, Lk/b;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    goto :goto_0
.end method

.method public c(LD/a;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v1, v0}, Lu/d;->a(Z)V

    return-void
.end method

.method public i_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0}, Lu/d;->g()V

    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
