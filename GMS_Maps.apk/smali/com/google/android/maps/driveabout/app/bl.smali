.class public Lcom/google/android/maps/driveabout/app/bl;
.super Lcom/google/android/maps/driveabout/app/cU;
.source "SourceFile"


# instance fields
.field protected d:F


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/maps/driveabout/app/cU;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    const/high16 v0, 0x41680000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    return-void
.end method


# virtual methods
.method protected a()F
    .locals 1

    const/high16 v0, 0x41700000

    return v0
.end method

.method protected a(LaH/h;)F
    .locals 3

    const/high16 v0, 0x41380000

    invoke-virtual {p1}, LaH/h;->f()Z

    move-result v1

    if-nez v1, :cond_0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, LaH/h;->l()Lo/af;

    move-result-object v1

    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v1, :cond_2

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lo/af;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lo/af;->f()I

    move-result v1

    const/16 v2, 0x80

    if-lt v1, v2, :cond_4

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_0

    :cond_4
    const/16 v0, 0x50

    if-lt v1, v0, :cond_5

    const/high16 v0, 0x41480000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_0

    :cond_5
    const/high16 v0, 0x41680000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_0
.end method
