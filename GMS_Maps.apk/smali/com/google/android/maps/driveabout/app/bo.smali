.class public Lcom/google/android/maps/driveabout/app/bo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[LO/U;

.field private b:LO/U;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:[LO/b;

.field private f:Landroid/content/Intent;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LO/U;Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p1}, LO/U;->c()Lo/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bn;->a(Lo/u;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LO/V;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2, p5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    invoke-virtual {p1}, LO/U;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2, p6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    return-void
.end method


# virtual methods
.method a()Landroid/net/Uri;
    .locals 7

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "google.navigation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    aget-object v1, v1, v0

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "altvia"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(Lo/u;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    aget-object v1, v1, v0

    const-string v3, "q"

    const-string v4, "ll"

    const-string v5, "title"

    const-string v6, "token"

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->b:LO/U;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->b:LO/U;

    const-string v3, "s"

    const-string v4, "sll"

    const-string v5, "stitle"

    const-string v6, "stoken"

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bo;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const-string v0, "mode"

    const-string v1, "w"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    const-string v0, "entry"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->e:[LO/b;

    if-eqz v0, :cond_6

    const-string v0, "opt"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->e:[LO/b;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a([LO/b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->f:Landroid/content/Intent;

    if-eqz v0, :cond_7

    const-string v0, "r"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->f:Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bo;->g:Z

    if-eqz v0, :cond_8

    const-string v0, "goff"

    const-string v1, "true"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_8
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :cond_9
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bo;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    const-string v0, "mode"

    const-string v1, "b"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1
.end method

.method a(I)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/bo;->c:I

    return-object p0
.end method

.method a(LO/U;)Lcom/google/android/maps/driveabout/app/bo;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [LO/U;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    return-object p0
.end method

.method a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->f:Landroid/content/Intent;

    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    return-object p0
.end method

.method a(Z)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/bo;->g:Z

    return-object p0
.end method

.method a([LO/U;)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    return-object p0
.end method

.method a([LO/b;)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->e:[LO/b;

    return-object p0
.end method

.method b(LO/U;)Lcom/google/android/maps/driveabout/app/bo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->b:LO/U;

    return-object p0
.end method
