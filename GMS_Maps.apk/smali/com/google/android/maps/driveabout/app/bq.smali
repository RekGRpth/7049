.class final Lcom/google/android/maps/driveabout/app/bq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/layer/q;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/cQ;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/cQ;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bq;->a:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/bq;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bq;->a:Lcom/google/android/maps/driveabout/app/cQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bq;->a:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->j()V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/W;Z)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bq;->a:Lcom/google/android/maps/driveabout/app/cQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bq;->a:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->j()V

    :cond_0
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0073

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2}, Lcom/google/googlenav/W;->aR()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bq;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
