.class public Lcom/google/android/maps/driveabout/app/bt;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/by;

.field private final b:Lcom/google/android/maps/driveabout/app/bx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bt;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v3, 0x7f0201a7

    invoke-virtual {v0, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/bt;->requestWindowFeature(I)Z

    const v0, 0x7f04003a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bt;->setContentView(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f100033

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1000fc

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/bu;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/bu;-><init>(Lcom/google/android/maps/driveabout/app/bt;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-static/range {p1 .. p7}, Lcom/google/android/maps/driveabout/app/bt;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZ)[Lcom/google/android/maps/driveabout/app/bE;

    move-result-object v3

    const v0, 0x7f1000fd

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v4, Lcom/google/android/maps/driveabout/app/bx;

    const/4 v5, 0x0

    invoke-direct {v4, p0, p1, v3, v5}, Lcom/google/android/maps/driveabout/app/bx;-><init>(Lcom/google/android/maps/driveabout/app/bt;Landroid/content/Context;[Lcom/google/android/maps/driveabout/app/bE;Lcom/google/android/maps/driveabout/app/bu;)V

    iput-object v4, p0, Lcom/google/android/maps/driveabout/app/bt;->b:Lcom/google/android/maps/driveabout/app/bx;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/bt;->b:Lcom/google/android/maps/driveabout/app/bx;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/bt;->b:Lcom/google/android/maps/driveabout/app/bx;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object p8, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    const v0, 0x7f1000ff

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bt;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v0, Lcom/google/android/maps/driveabout/app/bv;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/bv;-><init>(Lcom/google/android/maps/driveabout/app/bt;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    array-length v5, v3

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v3, v0

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v6

    if-eqz v6, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;I)Lcom/google/android/maps/driveabout/app/bE;
    .locals 4

    if-nez p1, :cond_0

    const v1, 0x7f0d003d

    const v0, 0x7f020128

    :goto_0
    new-instance v2, Lcom/google/android/maps/driveabout/app/bz;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v0, v3}, Lcom/google/android/maps/driveabout/app/bz;-><init>(Landroid/content/Context;IILcom/google/android/maps/driveabout/app/bu;)V

    return-object v2

    :cond_0
    const v1, 0x7f0d0040

    const v0, 0x7f02011e

    goto :goto_0
.end method

.method private a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->b(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->c(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/by;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bt;->dismiss()V

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    if-eqz v2, :cond_0

    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/bA;

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->b(Z)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bt;->dismiss()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/bB;

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->a(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/bz;

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v2

    if-nez v2, :cond_5

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->c(Z)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/bw;

    if-eqz v2, :cond_8

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v2

    if-nez v2, :cond_7

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/by;->d(Z)V

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bt;->a:Lcom/google/android/maps/driveabout/app/by;

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/by;->a(Lcom/google/android/maps/driveabout/app/bE;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bt;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bt;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bt;Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/bt;->a(Lcom/google/android/maps/driveabout/app/bE;)V

    return-void
.end method

.method private static a(Ljava/util/ArrayList;Lcom/google/android/maps/driveabout/app/bI;I)V
    .locals 3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bI;->j()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bI;->d(I)Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v1

    instance-of v0, v1, Lcom/google/android/maps/driveabout/app/bE;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/bE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast v1, Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZ)[Lcom/google/android/maps/driveabout/app/bE;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, p3}, Lcom/google/android/maps/driveabout/app/bt;->a(Landroid/content/Context;I)Lcom/google/android/maps/driveabout/app/bE;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/google/android/maps/driveabout/app/bE;->a(Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/maps/driveabout/app/bA;

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/bA;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bu;)V

    invoke-virtual {v1, p4}, Lcom/google/android/maps/driveabout/app/bA;->a(Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x3

    if-ne p3, v1, :cond_0

    new-instance v1, Lcom/google/android/maps/driveabout/app/bw;

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/bw;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bu;)V

    invoke-virtual {v1, p6}, Lcom/google/android/maps/driveabout/app/bw;->a(Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {v0, p1, p3}, Lcom/google/android/maps/driveabout/app/bt;->a(Ljava/util/ArrayList;Lcom/google/android/maps/driveabout/app/bI;I)V

    invoke-static {v0, p2, p3}, Lcom/google/android/maps/driveabout/app/bt;->a(Ljava/util/ArrayList;Lcom/google/android/maps/driveabout/app/bI;I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/maps/driveabout/app/bE;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/app/bE;

    return-object v0
.end method
