.class public Lcom/google/android/maps/driveabout/app/cS;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:LO/N;

.field protected b:Z

.field protected c:Z

.field protected d:Z

.field protected e:Lcom/google/android/maps/driveabout/vector/q;

.field protected f:I

.field protected g:Z

.field protected final h:Lcom/google/android/maps/driveabout/app/cq;

.field protected i:Landroid/content/Context;

.field private j:Z

.field private final k:Lcom/google/android/maps/driveabout/app/bC;

.field private final l:Lcom/google/android/maps/driveabout/app/cT;

.field private final m:Lcom/google/android/maps/driveabout/app/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/cT;Lcom/google/android/maps/driveabout/vector/bB;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->e:Lcom/google/android/maps/driveabout/vector/q;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cS;->i:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/cS;->m:Lcom/google/android/maps/driveabout/app/a;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->h:Lcom/google/android/maps/driveabout/app/cq;

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->h:Lcom/google/android/maps/driveabout/app/cq;

    invoke-virtual {v0, p5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/vector/bB;)V

    :cond_0
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/cS;->k:Lcom/google/android/maps/driveabout/app/bC;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/cS;->l:Lcom/google/android/maps/driveabout/app/cT;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->i:Landroid/content/Context;

    const-string v1, "HeadingUpPreferred"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/app/cq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->h:Lcom/google/android/maps/driveabout/app/cq;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/cS;->f:I

    return-void
.end method

.method public a(LO/N;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cS;->a:LO/N;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cS;->e:Lcom/google/android/maps/driveabout/vector/q;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->b:Z

    return-void
.end method

.method public b()LO/N;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->a:LO/N;

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->j:Z

    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/cS;->f:I

    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->g:Z

    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cS;->f:I

    return v0
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->c:Z

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    return v0
.end method

.method public f()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->i:Landroid/content/Context;

    const-string v1, "HeadingUpPreferred"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    invoke-static {v0, v1, v2}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->b:Z

    return v0
.end method

.method public h()Lcom/google/android/maps/driveabout/vector/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->e:Lcom/google/android/maps/driveabout/vector/q;

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->j:Z

    return v0
.end method

.method public j()Lcom/google/android/maps/driveabout/app/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->m:Lcom/google/android/maps/driveabout/app/a;

    return-object v0
.end method

.method public k()Lcom/google/android/maps/driveabout/app/bC;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->k:Lcom/google/android/maps/driveabout/app/bC;

    return-object v0
.end method

.method public l()Lcom/google/android/maps/driveabout/app/cT;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->l:Lcom/google/android/maps/driveabout/app/cT;

    return-object v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->g:Z

    return v0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->c:Z

    return v0
.end method
