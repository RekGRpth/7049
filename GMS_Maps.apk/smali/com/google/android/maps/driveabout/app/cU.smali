.class public Lcom/google/android/maps/driveabout/app/cU;
.super Lcom/google/android/maps/driveabout/app/k;
.source "SourceFile"


# static fields
.field private static final d:F

.field static final e:Lcom/google/android/maps/driveabout/app/cV;

.field private static final l:F

.field private static final m:F


# instance fields
.field protected f:I

.field protected g:Lcom/google/android/maps/driveabout/app/cV;

.field protected h:Lcom/google/android/maps/driveabout/app/cV;

.field protected i:Lcom/google/android/maps/driveabout/app/cV;

.field protected j:Lcom/google/android/maps/driveabout/app/cV;

.field protected k:Lcom/google/android/maps/driveabout/app/cV;

.field private n:F

.field private final o:Lq/e;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-wide/16 v4, 0x0

    const-wide v0, 0x408f400000000000L

    invoke-static {v4, v5}, Lo/T;->a(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/app/cU;->d:F

    const-wide v0, 0x40a7700000000000L

    invoke-static {v4, v5}, Lo/T;->a(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/app/cU;->l:F

    new-instance v0, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v1, 0x418e0000

    const/high16 v2, 0x42200000

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    sput-object v0, Lcom/google/android/maps/driveabout/app/cU;->e:Lcom/google/android/maps/driveabout/app/cV;

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/app/cU;->m:F

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/k;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/cU;->n:F

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/cU;->h:Lcom/google/android/maps/driveabout/app/cV;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/cU;->j:Lcom/google/android/maps/driveabout/app/cV;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/cU;->i:Lcom/google/android/maps/driveabout/app/cV;

    sget-object v0, Lcom/google/android/maps/driveabout/app/cU;->e:Lcom/google/android/maps/driveabout/app/cV;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->k:Lcom/google/android/maps/driveabout/app/cV;

    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/cU;->o:Lq/e;

    return-void
.end method

.method private a(F)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->h:Lcom/google/android/maps/driveabout/app/cV;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/cV;->a:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->h:Lcom/google/android/maps/driveabout/app/cV;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/cV;->b:F

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/cV;->a:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/cV;->b:F

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->j:Lcom/google/android/maps/driveabout/app/cV;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/cV;->b:F

    goto :goto_0
.end method

.method private a(Lo/T;Lo/t;IILo/T;)F
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v0, p1}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    aget v0, v2, v3

    if-gez v0, :cond_1

    invoke-virtual {p2}, Lo/t;->d()Lo/T;

    move-result-object v0

    invoke-virtual {p2}, Lo/t;->g()Lo/T;

    move-result-object v3

    invoke-static {v0, v3, p5, p1}, Lo/V;->a(Lo/T;Lo/T;Lo/T;Lo/T;)D

    move-result-wide v3

    double-to-float v0, v3

    div-float v0, v1, v0

    :goto_0
    aget v3, v2, v5

    if-gez v3, :cond_2

    invoke-virtual {p2}, Lo/t;->f()Lo/T;

    move-result-object v2

    invoke-virtual {p2}, Lo/t;->g()Lo/T;

    move-result-object v3

    invoke-static {v2, v3, p5, p1}, Lo/V;->a(Lo/T;Lo/T;Lo/T;Lo/T;)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :cond_0
    :goto_1
    return v0

    :cond_1
    aget v0, v2, v3

    if-le v0, p3, :cond_3

    invoke-virtual {p2}, Lo/t;->e()Lo/T;

    move-result-object v0

    invoke-virtual {p2}, Lo/t;->f()Lo/T;

    move-result-object v3

    invoke-static {v0, v3, p5, p1}, Lo/V;->a(Lo/T;Lo/T;Lo/T;Lo/T;)D

    move-result-wide v3

    double-to-float v0, v3

    div-float v0, v1, v0

    goto :goto_0

    :cond_2
    aget v2, v2, v5

    if-le v2, p4, :cond_0

    invoke-virtual {p2}, Lo/t;->e()Lo/T;

    move-result-object v2

    invoke-virtual {p2}, Lo/t;->d()Lo/T;

    move-result-object v3

    invoke-static {v2, v3, p5, p1}, Lo/V;->a(Lo/T;Lo/T;Lo/T;Lo/T;)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private a(ILo/T;FF)LC/b;
    .locals 6

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/cU;->c(I)Lcom/google/android/maps/driveabout/app/cV;

    move-result-object v1

    new-instance v0, LC/b;

    iget v2, v1, Lcom/google/android/maps/driveabout/app/cV;->a:F

    iget v3, v1, Lcom/google/android/maps/driveabout/app/cV;->b:F

    move-object v1, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method private a(LC/b;FLo/T;IIF)Z
    .locals 7

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    if-ne v0, v2, :cond_0

    const/high16 v0, 0x41700000

    :goto_0
    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    const/high16 v0, 0x41a00000

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v0

    invoke-virtual {p3, v0}, Lo/T;->d(Lo/T;)F

    move-result v0

    sget v3, Lcom/google/android/maps/driveabout/app/cU;->l:F

    sget v4, Lcom/google/android/maps/driveabout/app/cU;->l:F

    mul-float/2addr v3, v4

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, LC/b;->e()F

    move-result v3

    invoke-virtual {p1}, LC/b;->f()F

    move-result v4

    invoke-direct {p0, v5, v0, v3, v4}, Lcom/google/android/maps/driveabout/app/cU;->a(ILo/T;FF)LC/b;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5, p6}, Lcom/google/android/maps/driveabout/app/cU;->a(LC/b;IIF)V

    const v0, 0x3e4ccccc

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    if-ne v3, v5, :cond_4

    const/4 v0, 0x0

    :cond_4
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v3, p3}, LC/a;->b(Lo/T;)[I

    move-result-object v3

    aget v4, v3, v1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->k()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    aget v4, v3, v1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->k()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f800000

    sub-float/2addr v6, v0

    mul-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    aget v4, v3, v2

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->l()I

    move-result v5

    if-ge v4, v5, :cond_5

    aget v3, v3, v2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v4}, LC/a;->l()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    cmpl-float v0, v3, v0

    if-lez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method private a(LC/b;Lo/T;IIF)Z
    .locals 7

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v0

    invoke-virtual {p2, v0}, Lo/T;->d(Lo/T;)F

    move-result v0

    sget v3, Lcom/google/android/maps/driveabout/app/cU;->d:F

    sget v4, Lcom/google/android/maps/driveabout/app/cU;->d:F

    mul-float/2addr v3, v4

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, LC/b;->e()F

    move-result v3

    invoke-virtual {p1}, LC/b;->f()F

    move-result v4

    invoke-direct {p0, v5, v0, v3, v4}, Lcom/google/android/maps/driveabout/app/cU;->a(ILo/T;FF)LC/b;

    move-result-object v0

    invoke-virtual {p0, v0, p3, p4, p5}, Lcom/google/android/maps/driveabout/app/cU;->a(LC/b;IIF)V

    const v0, 0x3e4ccccc

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    if-ne v3, v5, :cond_2

    const/4 v0, 0x0

    :cond_2
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v3, p2}, LC/a;->b(Lo/T;)[I

    move-result-object v3

    aget v4, v3, v2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->k()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    aget v4, v3, v2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->k()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f800000

    sub-float/2addr v6, v0

    mul-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    aget v4, v3, v1

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->l()I

    move-result v5

    if-ge v4, v5, :cond_3

    aget v3, v3, v1

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v4}, LC/a;->l()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    cmpl-float v0, v3, v0

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private c(I)Lcom/google/android/maps/driveabout/app/cV;
    .locals 1

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->k:Lcom/google/android/maps/driveabout/app/cV;

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->j:Lcom/google/android/maps/driveabout/app/cV;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->h:Lcom/google/android/maps/driveabout/app/cV;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->i:Lcom/google/android/maps/driveabout/app/cV;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected a()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/cV;->a:F

    return v0
.end method

.method protected a(LaH/h;)F
    .locals 1

    const/high16 v0, 0x41680000

    return v0
.end method

.method a(LaH/h;FI)F
    .locals 14

    invoke-virtual {p1}, LaH/h;->l()Lo/af;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lo/af;->f()I

    move-result v1

    const/16 v3, 0x50

    if-ge v1, v3, :cond_1

    :cond_0
    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v1

    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, LaH/h;->m()Lo/T;

    move-result-object v5

    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v6

    const-wide/high16 v3, 0x4000000000000000L

    const/high16 v1, 0x41f00000

    sub-float v1, v1, p2

    float-to-double v7, v1

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    move/from16 v0, p3

    int-to-double v7, v0

    mul-double/2addr v3, v7

    const-wide/high16 v7, 0x4070000000000000L

    div-double/2addr v3, v7

    double-to-float v1, v3

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->e()I

    move-result v7

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cU;->o:Lq/e;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cU;->o:Lq/e;

    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v4

    invoke-virtual {v3, v2, v4, v1, v7}, Lq/e;->a(Lo/af;FFI)Ljava/util/List;

    move-result-object v1

    :goto_1
    new-instance v8, Lo/T;

    invoke-direct {v8}, Lo/T;-><init>()V

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/af;

    invoke-virtual {v1}, Lo/af;->b()Lo/X;

    move-result-object v10

    const/4 v1, 0x0

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v13

    :goto_3
    invoke-virtual {v10}, Lo/X;->b()I

    move-result v4

    if-ge v3, v4, :cond_6

    invoke-virtual {v10, v3, v8}, Lo/X;->a(ILo/T;)V

    invoke-virtual {v8, v5}, Lo/T;->d(Lo/T;)F

    move-result v4

    mul-int v11, v7, v7

    int-to-float v11, v11

    cmpg-float v4, v4, v11

    if-gez v4, :cond_3

    move v4, v2

    move v2, v1

    :goto_4
    invoke-virtual {v10}, Lo/X;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, v3

    move v3, v1

    move v1, v2

    move v2, v4

    goto :goto_3

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v5, v8}, Lo/T;->a(Lo/T;Lo/T;)D

    move-result-wide v11

    double-to-float v4, v11

    invoke-static {v6, v4}, Lo/V;->b(FF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const/high16 v12, 0x42b40000

    cmpl-float v11, v11, v12

    if-lez v11, :cond_4

    move v4, v2

    move v2, v1

    goto :goto_4

    :cond_4
    const/high16 v11, -0x3d900000

    const/high16 v12, 0x42700000

    invoke-static {v12, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v11, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    cmpg-float v11, v4, v1

    if-gez v11, :cond_5

    move v13, v4

    move v4, v2

    move v2, v13

    goto :goto_4

    :cond_5
    cmpl-float v11, v4, v2

    if-lez v11, :cond_8

    move v2, v1

    goto :goto_4

    :cond_6
    move v3, v2

    move v2, v1

    goto :goto_2

    :cond_7
    add-float v1, v3, v2

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    add-float/2addr v1, v6

    goto/16 :goto_0

    :cond_8
    move v4, v2

    move v2, v1

    goto :goto_4
.end method

.method a(LC/b;Lo/T;FLo/T;IIF)I
    .locals 8

    const/4 v6, 0x3

    const/4 v7, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cU;->a(LC/b;Lo/T;IIF)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x42c80000

    cmpl-float v0, p3, v0

    if-lez v0, :cond_3

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    if-ne v0, v6, :cond_2

    move v0, v7

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cU;->a(LC/b;FLo/T;IIF)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v7

    goto :goto_0
.end method

.method public a(LC/b;F)LC/b;
    .locals 6

    invoke-virtual {p1}, LC/b;->e()F

    move-result v0

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    invoke-virtual {p1}, LC/b;->d()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    iget v1, v1, Lcom/google/android/maps/driveabout/app/cV;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {p1}, LC/b;->a()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cU;->g:Lcom/google/android/maps/driveabout/app/cV;

    iget v3, v3, Lcom/google/android/maps/driveabout/app/cV;->b:F

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    move v4, p2

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    move-object p1, v0

    :cond_1
    return-object p1
.end method

.method public a(LC/b;LO/N;Z)LC/b;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v0

    if-eqz p3, :cond_0

    const/4 v1, 0x4

    invoke-virtual {p2}, LO/N;->g()F

    move-result v2

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/cU;->a(ILo/T;FF)LC/b;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x5

    invoke-virtual {p2}, LO/N;->g()F

    move-result v2

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/cU;->a(ILo/T;FF)LC/b;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LC/b;LaH/h;IIF)LC/b;
    .locals 6

    invoke-virtual {p2}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lo/T;->a(DD)Lo/T;

    move-result-object v1

    const/high16 v0, 0x3f800000

    const/high16 v2, 0x40000000

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cU;->b:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cU;->c:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    int-to-float v3, p4

    div-float/2addr v2, v3

    sub-float v5, v0, v2

    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/cU;->a(LaH/h;)F

    move-result v2

    new-instance v0, LC/b;

    const/high16 v3, 0x42200000

    invoke-virtual {p0, p2, v2, p4}, Lcom/google/android/maps/driveabout/app/cU;->a(LaH/h;FI)F

    move-result v4

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method public a(LC/b;LaH/h;Lo/S;LO/N;FIIF)LC/b;
    .locals 15

    const/high16 v1, 0x3f800000

    const/high16 v2, 0x40000000

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cU;->b:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cU;->c:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    move/from16 v0, p7

    int-to-float v3, v0

    div-float/2addr v2, v3

    sub-float v6, v1, v2

    if-eqz p4, :cond_0

    invoke-virtual/range {p4 .. p4}, LO/N;->b()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual/range {p4 .. p4}, LO/N;->j()LO/N;

    move-result-object p4

    :cond_0
    invoke-virtual/range {p2 .. p2}, LaH/h;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p2 .. p2}, LaH/h;->getSpeed()F

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/app/cU;->n:F

    :cond_1
    const/4 v1, 0x0

    cmpl-float v1, p5, v1

    if-ltz v1, :cond_2

    new-instance v1, LC/b;

    invoke-virtual/range {p3 .. p3}, Lo/S;->a()Lo/T;

    move-result-object v2

    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/cU;->a(F)F

    move-result v4

    invoke-virtual/range {p3 .. p3}, Lo/S;->b()F

    move-result v5

    move/from16 v3, p5

    invoke-direct/range {v1 .. v6}, LC/b;-><init>(Lo/T;FFFF)V

    :goto_0
    return-object v1

    :cond_2
    if-nez p4, :cond_3

    const/4 v11, 0x0

    :goto_1
    invoke-virtual/range {p3 .. p3}, Lo/S;->a()Lo/T;

    move-result-object v9

    iget v10, p0, Lcom/google/android/maps/driveabout/app/cU;->n:F

    move-object v7, p0

    move-object/from16 v8, p1

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/maps/driveabout/app/cU;->a(LC/b;Lo/T;FLo/T;IIF)I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/app/cU;->f:I

    invoke-virtual/range {p3 .. p3}, Lo/S;->a()Lo/T;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lo/S;->b()F

    move-result v3

    invoke-direct {p0, v1, v2, v3, v6}, Lcom/google/android/maps/driveabout/app/cU;->a(ILo/T;FF)LC/b;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-virtual/range {p4 .. p4}, LO/N;->a()Lo/T;

    move-result-object v11

    goto :goto_1
.end method

.method public a(LC/b;LaH/h;Lo/am;IIF)LC/b;
    .locals 14

    if-nez p3, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual/range {p2 .. p2}, LaH/h;->getLatitude()D

    move-result-wide v3

    invoke-virtual/range {p2 .. p2}, LaH/h;->getLongitude()D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lo/T;->a(DD)Lo/T;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lo/am;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lo/am;->a(I)Lo/T;

    move-result-object v9

    new-instance v3, LC/b;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cU;->a()F

    move-result v5

    const/high16 v6, 0x42200000

    invoke-static {v4, v9}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v7

    const/high16 v8, 0x3f800000

    const/high16 v10, 0x40000000

    iget v11, p0, Lcom/google/android/maps/driveabout/app/cU;->b:I

    iget v12, p0, Lcom/google/android/maps/driveabout/app/cU;->c:I

    add-int/2addr v11, v12

    int-to-float v11, v11

    mul-float/2addr v10, v11

    move/from16 v0, p5

    int-to-float v11, v0

    div-float/2addr v10, v11

    sub-float/2addr v8, v10

    invoke-direct/range {v3 .. v8}, LC/b;-><init>(Lo/T;FFFF)V

    move/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cU;->a(LC/b;IIF)V

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/cU;->a:LC/a;

    invoke-virtual {v5}, LC/a;->B()Lo/aQ;

    move-result-object v5

    invoke-virtual {v5}, Lo/aQ;->c()Lo/ae;

    move-result-object v7

    check-cast v7, Lo/t;

    move-object v5, p0

    move-object v6, v9

    move/from16 v8, p4

    move/from16 v9, p5

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/google/android/maps/driveabout/app/cU;->a(Lo/T;Lo/t;IILo/T;)F

    move-result v8

    new-instance v6, Lo/T;

    invoke-direct {v6}, Lo/T;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lo/am;->a()I

    move-result v5

    div-int/lit8 v5, v5, 0xa

    if-nez v5, :cond_2

    const/4 v5, 0x1

    move v11, v5

    :goto_1
    const/4 v5, 0x0

    move v12, v5

    move v13, v8

    :goto_2
    invoke-virtual/range {p3 .. p3}, Lo/am;->a()I

    move-result v5

    if-ge v12, v5, :cond_1

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v6}, Lo/am;->a(ILo/T;)V

    move-object v5, p0

    move/from16 v8, p4

    move/from16 v9, p5

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/google/android/maps/driveabout/app/cU;->a(Lo/T;Lo/t;IILo/T;)F

    move-result v5

    invoke-static {v13, v5}, Ljava/lang/Math;->max(FF)F

    move-result v8

    add-int v5, v12, v11

    move v12, v5

    move v13, v8

    goto :goto_2

    :cond_1
    invoke-virtual {v3}, LC/b;->a()F

    move-result v4

    float-to-double v5, v13

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    double-to-float v5, v5

    sget v6, Lcom/google/android/maps/driveabout/app/cU;->m:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    const v5, 0x3ecccccd

    sub-float v5, v4, v5

    new-instance p1, LC/b;

    invoke-virtual {v3}, LC/b;->c()Lo/T;

    move-result-object v4

    invoke-virtual {v3}, LC/b;->d()F

    move-result v6

    invoke-virtual {v3}, LC/b;->e()F

    move-result v7

    invoke-virtual {v3}, LC/b;->f()F

    move-result v8

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, LC/b;-><init>(Lo/T;FFFF)V

    goto/16 :goto_0

    :cond_2
    move v11, v5

    goto :goto_1
.end method
