.class public Lcom/google/android/maps/driveabout/app/cW;
.super Lcom/google/android/maps/driveabout/app/k;
.source "SourceFile"


# static fields
.field private static final e:F


# instance fields
.field private final d:[F

.field private final f:Z

.field private g:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/app/cW;->e:F

    return-void
.end method

.method public constructor <init>(FFFZ)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/k;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    const/4 v1, 0x2

    aput p3, v0, v1

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    iput p1, p0, Lcom/google/android/maps/driveabout/app/cW;->g:F

    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    return-void
.end method

.method private static a(LaH/h;)F
    .locals 4

    const/high16 v1, 0x41680000

    const/high16 v0, 0x41380000

    invoke-virtual {p0}, LaH/h;->f()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, LaH/h;->l()Lo/af;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, LaH/h;->l()Lo/af;

    move-result-object v2

    invoke-virtual {v2}, Lo/af;->f()I

    move-result v2

    const/16 v3, 0x30

    if-gt v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v1, 0x50

    if-gt v2, v1, :cond_0

    const/high16 v0, 0x41480000

    goto :goto_0
.end method

.method private a(Lo/T;Lo/T;IF)F
    .locals 4

    invoke-virtual {p1, p2}, Lo/T;->c(Lo/T;)F

    move-result v0

    const/high16 v1, 0x43800000

    mul-float/2addr v0, v1

    mul-float/2addr v0, p4

    int-to-float v1, p3

    const/high16 v2, 0x3f000000

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    const/high16 v1, 0x41f00000

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v0, v2

    sget v2, Lcom/google/android/maps/driveabout/app/cW;->e:F

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    const v1, 0x3e4ccccd

    sub-float v1, v0, v1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    aget v2, v2, v0

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    aget v0, v1, v0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(LC/b;F)LC/b;
    .locals 6

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    if-eqz v0, :cond_2

    move v4, p2

    :goto_0
    invoke-virtual {p1}, LC/b;->e()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    invoke-virtual {p1}, LC/b;->d()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {p1}, LC/b;->a()F

    move-result v2

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    move-object p1, v0

    :cond_1
    return-object p1

    :cond_2
    move v4, v3

    goto :goto_0
.end method

.method public a(LC/b;LO/N;Z)LC/b;
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LO/N;->g()F

    move-result v4

    :goto_0
    new-instance v0, LC/b;

    const/high16 v2, 0x41820000

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0

    :cond_0
    move v4, v3

    goto :goto_0
.end method

.method public a(LC/b;LaH/h;IIF)LC/b;
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p2}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, LaH/h;->getLongitude()D

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v1

    new-instance v0, LC/b;

    invoke-static {p2}, Lcom/google/android/maps/driveabout/app/cW;->a(LaH/h;)F

    move-result v2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method public a(LC/b;LaH/h;Lo/S;LO/N;FIIF)LC/b;
    .locals 6

    const/4 v3, 0x0

    cmpl-float v0, p5, v3

    if-ltz v0, :cond_0

    move v2, p5

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lo/S;->b()F

    move-result v4

    :goto_1
    new-instance v0, LC/b;

    invoke-virtual {p3}, Lo/S;->a()Lo/T;

    move-result-object v1

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0

    :cond_0
    if-eqz p4, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cW;->b:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p7, v0

    invoke-static {p6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p3}, Lo/S;->a()Lo/T;

    move-result-object v1

    invoke-virtual {p4}, LO/N;->a()Lo/T;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0, p8}, Lcom/google/android/maps/driveabout/app/cW;->a(Lo/T;Lo/T;IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/maps/driveabout/app/cW;->g:F

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/google/android/maps/driveabout/app/cW;->g:F

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1
.end method

.method public a(LC/b;LaH/h;Lo/am;IIF)LC/b;
    .locals 9

    const/high16 v8, 0x43800000

    const/4 v3, 0x0

    if-nez p3, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p3}, Lo/am;->f()Lo/ad;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Lo/T;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lo/ad;->e()Lo/T;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    invoke-virtual {p2}, LaH/h;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p2}, LaH/h;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lo/T;->a(DD)Lo/T;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lo/ad;->a([Lo/T;)Lo/ad;

    move-result-object v1

    invoke-virtual {v1}, Lo/ad;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v8

    mul-float/2addr v0, p6

    int-to-float v2, p4

    div-float/2addr v0, v2

    invoke-virtual {v1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v8

    mul-float/2addr v2, p6

    int-to-float v4, p5

    div-float/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const v2, 0x3f4ccccd

    div-float/2addr v0, v2

    const/high16 v2, 0x41f00000

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v0, v4

    sget v4, Lcom/google/android/maps/driveabout/app/cW;->e:F

    mul-float/2addr v0, v4

    sub-float v0, v2, v0

    const v2, 0x3e4ccccd

    sub-float/2addr v0, v2

    const/high16 v2, 0x41000000

    const/high16 v4, 0x41700000

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    new-instance v0, LC/b;

    invoke-virtual {v1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    move-object p1, v0

    goto :goto_0
.end method
