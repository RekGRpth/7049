.class public Lcom/google/android/maps/driveabout/app/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/b;


# static fields
.field private static a:Z

.field private static b:Lcom/google/android/maps/driveabout/app/ci;

.field private static final c:Lcom/google/googlenav/j;

.field private static d:Lcom/google/googlenav/ui/android/L;

.field private static volatile e:Z


# instance fields
.field private final f:Landroid/app/Application;

.field private g:Lcom/google/android/maps/driveabout/app/NavigationActivity;

.field private h:Lcom/google/android/maps/driveabout/app/NavigationService;

.field private i:LaH/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/j;

    invoke-direct {v0}, Lcom/google/googlenav/j;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    return-void
.end method

.method private constructor <init>(Landroid/app/Application;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->f:Landroid/app/Application;

    return-void
.end method

.method public static a()Lcom/google/android/maps/driveabout/app/ci;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    return-object v0
.end method

.method public static a(Landroid/app/Application;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/maps/driveabout/app/ci;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->e(Landroid/content/Context;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->b()Lcom/google/googlenav/android/b;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/maps/driveabout/app/ci;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ci;-><init>(Landroid/app/Application;)V

    sput-object v1, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    :cond_1
    sget-object v1, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/AndroidGmmApplication;->b(Lcom/google/googlenav/android/b;)V

    :cond_2
    return-void
.end method

.method private static a(Landroid/content/Context;II)V
    .locals 5

    const/16 v4, 0x138e

    const/16 v3, 0x1068

    const/16 v0, 0xfa0

    if-ge p1, v0, :cond_0

    if-lt p2, v0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "google_maps_navigation"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p0}, LJ/a;->a(Ljava/io/File;Landroid/content/Context;)Z

    :cond_0
    if-ge p1, v3, :cond_1

    if-lt p2, v3, :cond_1

    const/4 v0, 0x6

    :goto_0
    if-ltz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "._speech_nav_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".wav"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    if-ge p1, v4, :cond_2

    if-lt p2, v4, :cond_2

    invoke-static {p0}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    const-string v1, "cache_ImageTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    const-string v1, "cache_LayerTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    const-string v1, "cache_RoadGraphTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    const-string v1, "cache_VectorTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    const-string v1, "cache_Resource"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;I)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-gtz p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v2, 0x64

    if-lt p1, v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    rem-int/lit8 v2, v2, 0x64

    if-ge v2, p1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_providers_allowed"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/maps/driveabout/app/ci;->e:Z

    return v0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static e(Landroid/content/Context;)V
    .locals 6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    new-instance v1, LR/d;

    invoke-direct {v1}, LR/d;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->f(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    const-string v3, "DriveAbout"

    const v4, 0x7f070003

    new-instance v5, Lcom/google/googlenav/android/I;

    invoke-direct {v5, p0}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->j()V

    invoke-static {p0, v0}, LO/c;->a(Landroid/content/Context;Law/p;)V

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/google/googlenav/ui/android/L;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/L;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/maps/driveabout/app/ci;->d:Lcom/google/googlenav/ui/android/L;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    invoke-virtual {v0}, Las/c;->d()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/maps/driveabout/app/ci;->a:Z

    return-void
.end method

.method private static f(Landroid/content/Context;)V
    .locals 2

    const-string v0, "LastRunVersion"

    const/16 v1, 0xce4

    invoke-static {p0, v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    const-string v1, "6140304"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;II)V

    const-string v0, "LastRunVersion"

    invoke-static {p0, v0, v1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private static j()V
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sget-object v1, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/j;->a([I)V

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method


# virtual methods
.method public a(LaH/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->i:LaH/h;

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/Config;->a(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ci;->f:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LO/c;->a(Landroid/content/Context;)V

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Ljava/util/Locale;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->g:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->h:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "DriveAbout"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "RmiOverride"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "RmiOverride"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ci;->i:LaH/h;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ci;->i:LaH/h;

    invoke-virtual {v1}, LaH/h;->r()LaN/B;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, LR/m;->A()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;LR/m;)Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "DriveAbout"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "OfflineRoutingOverride"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "OfflineRoutingOverride"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_0

    invoke-static {p1}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LR/m;->B()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public d()V
    .locals 0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->c()V

    return-void
.end method

.method public e()V
    .locals 0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->d()V

    return-void
.end method

.method public f()Lcom/google/android/maps/driveabout/app/NavigationActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ci;->g:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    return-object v0
.end method

.method public g()Lcom/google/android/maps/driveabout/app/NavigationService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ci;->h:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-object v0
.end method

.method public h()Lcom/google/googlenav/j;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    return-object v0
.end method

.method public synthetic i()Landroid/app/Activity;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    return-object v0
.end method
