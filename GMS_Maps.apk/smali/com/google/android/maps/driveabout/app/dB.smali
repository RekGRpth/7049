.class public Lcom/google/android/maps/driveabout/app/dB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/ar;


# static fields
.field private static final a:Lo/ar;

.field private static final b:Lo/ar;


# instance fields
.field private volatile c:I

.field private volatile d:Z

.field private e:LO/z;

.field private f:Lo/X;

.field private g:I

.field private h:I

.field private i:I

.field private j:Lo/ar;

.field private final k:LR/h;

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/maps/driveabout/app/dB;->a:Lo/ar;

    new-instance v0, Lcom/google/android/maps/driveabout/app/dC;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dC;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/dB;->b:Lo/ar;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xc8

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Z

    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->a:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    new-instance v0, LR/h;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->k:LR/h;

    return-void
.end method

.method static a(ILo/T;)I
    .locals 4

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v0

    int-to-double v2, p0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method private b()V
    .locals 5

    const/16 v4, 0xe

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->a:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    invoke-virtual {v1}, LO/z;->k()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->a()Lo/T;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(Lo/T;)I

    move-result v1

    invoke-static {v0, v4, v1}, Lcom/google/android/maps/driveabout/app/bP;->a(Lo/T;II)Lcom/google/android/maps/driveabout/app/bP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->l:I

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->f:Lo/X;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->f:Lo/X;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    invoke-virtual {v1, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/driveabout/app/bP;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(Lo/T;)I

    move-result v3

    invoke-direct {v2, v0, v1, v4, v3}, Lcom/google/android/maps/driveabout/app/bP;-><init>(Lo/T;Lo/T;II)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->a:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    goto :goto_0
.end method


# virtual methods
.method a(Lo/T;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->c:I

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dB;->a(ILo/T;)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->f:Lo/X;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:I

    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->k:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    return-void
.end method

.method public a(LO/N;I)V
    .locals 2

    invoke-virtual {p1}, LO/N;->i()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    if-gt v1, v0, :cond_1

    sget-object v1, Lcom/google/android/maps/driveabout/app/dB;->b:Lo/ar;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    iput p2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    :cond_0
    :goto_0
    iput p2, p0, Lcom/google/android/maps/driveabout/app/dB;->i:I

    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    if-gt v0, p2, :cond_2

    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    iput p2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    sget-object v1, Lcom/google/android/maps/driveabout/app/dB;->a:Lo/ar;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:I

    if-ge v0, p2, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    goto :goto_0
.end method

.method public a(LO/z;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->f:Lo/X;

    iput v1, p0, Lcom/google/android/maps/driveabout/app/dB;->g:I

    iput v1, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    iput v1, p0, Lcom/google/android/maps/driveabout/app/dB;->i:I

    sget-object v0, Lcom/google/android/maps/driveabout/app/dB;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    return-void
.end method

.method a(I)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dB;->f:Lo/X;

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v3

    if-lt v2, v3, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/google/android/maps/driveabout/app/dB;->i:I

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dB;->f:Lo/X;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->t()I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    invoke-static {v3, v2}, Lcom/google/android/maps/driveabout/app/dB;->a(ILo/T;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, LO/z;->b(I)D

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dB;->e:LO/z;

    iget v6, p0, Lcom/google/android/maps/driveabout/app/dB;->i:I

    invoke-virtual {v5, v6}, LO/z;->b(I)D

    move-result-wide v5

    sub-double/2addr v3, v5

    int-to-double v5, v2

    cmpg-double v2, v3, v5

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method b(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->c:I

    return-void
.end method

.method public c()Lo/aq;
    .locals 3

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    sget-object v1, Lcom/google/android/maps/driveabout/app/dB;->a:Lo/ar;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lo/ar;

    invoke-interface {v0}, Lo/ar;->c()Lo/aq;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->b()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->k:LR/h;

    invoke-virtual {v1, v0}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->k:LR/h;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
