.class public Lcom/google/android/maps/driveabout/app/dO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

.field private c:Lcom/google/android/maps/driveabout/app/cS;

.field private d:Lcom/google/android/maps/driveabout/app/NavigationView;

.field private e:Lcom/google/android/maps/driveabout/app/cQ;

.field private f:Lcom/google/android/maps/driveabout/app/cQ;

.field private g:Lcom/google/android/maps/driveabout/app/aQ;

.field private h:LQ/p;

.field private i:Ll/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eD;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/eD;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->a:Landroid/os/Handler;

    return-void
.end method

.method private B()V
    .locals 3

    new-instance v0, Lcom/google/android/maps/driveabout/app/dP;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dP;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/ea;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ea;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-interface {v2, v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private final C()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->j()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dO;)LQ/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/dO;)Lcom/google/android/maps/driveabout/app/NavigationActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    return-object v0
.end method

.method private b(Lcom/google/android/maps/driveabout/app/NavigationView;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/ev;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ev;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setOnSizeChangedListener(Lcom/google/android/maps/driveabout/app/cv;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ew;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ew;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ex;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ex;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setNavigationImageViewClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ey;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ey;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLoadingDialogListeners(Landroid/content/DialogInterface$OnCancelListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ez;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ez;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setListItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eA;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eA;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/dQ;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dQ;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/dR;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/dR;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/dS;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/dS;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/NavigationView;->setTopBarClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/maps/driveabout/app/dg;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dT;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dT;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMuteButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dU;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dU;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/dV;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dV;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setZoomButtonClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dW;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dW;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setAlternateRouteButtonTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dX;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dX;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteOptionsButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dY;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dY;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setStreetViewButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dZ;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dZ;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setStreetViewLaunchButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eb;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eb;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ec;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ec;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ed;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ed;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ee;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ee;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ef;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ef;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setBackToMyLocationButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eg;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eg;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setListViewButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eC;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/eC;-><init>(Lcom/google/android/maps/driveabout/app/dO;Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMapViewKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eh;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eh;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setTrafficButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ei;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ei;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRerouteToDestinationButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ej;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ej;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setFoundDestinationButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ek;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ek;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setDestinationInfoButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/em;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/em;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/en;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/en;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteAroundTrafficConfirmButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eo;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eo;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteAroundTrafficCancelButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/ep;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ep;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setExitButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eq;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eq;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setCloseActivityListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/er;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/er;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setDestinationButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/es;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/es;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLayersButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/et;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/et;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setVoiceSearchButtonListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eu;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eu;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setSpeechInstallButtonListener(Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/dO;)Lcom/google/android/maps/driveabout/app/NavigationView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/dO;)Lcom/google/android/maps/driveabout/app/cS;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/dO;)Lcom/google/android/maps/driveabout/app/aQ;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->al()V

    return-void
.end method

.method public a()LQ/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    return-object v0
.end method

.method public a(LO/N;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LQ/s;->b(LO/N;Z)V

    return-void
.end method

.method public a(LO/U;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LQ/s;->a(LO/U;Z)V

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;Ll/f;LQ/p;)V
    .locals 3

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/dO;->i:Ll/f;

    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    new-instance v0, Lcom/google/android/maps/driveabout/app/dx;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dx;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->f:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->f:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    sget-object v1, LQ/x;->b:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method public a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eE;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LQ/s;->a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eE;)V

    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a()Lcom/google/android/maps/driveabout/app/NavigationView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setRequestedOrientation(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    sget-object v1, LQ/x;->e:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->b(LQ/x;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->P()V

    return-void

    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dO;->b(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->p()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setMapController(Lcom/google/android/maps/driveabout/vector/bk;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-virtual {v0, v1}, LQ/p;->a(Lcom/google/android/maps/driveabout/app/cQ;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->b()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->q()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->J()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bE;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bC;->b(Lcom/google/android/maps/driveabout/app/bE;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bC;->a(Lcom/google/android/maps/driveabout/app/bE;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bS;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->b(Lcom/google/android/maps/driveabout/app/bS;)V

    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(Lo/aR;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/eB;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/eB;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {v0, p1, v1}, LQ/s;->a(Lo/aR;Lcom/google/android/maps/driveabout/app/by;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->c(Z)V

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->F()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->O()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->e()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->f:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setMapController(Lcom/google/android/maps/driveabout/vector/bk;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->e:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-virtual {v0, v1}, LQ/p;->a(Lcom/google/android/maps/driveabout/app/cQ;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dO;->c(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dO;->d(Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->p()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->o()V

    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-void
.end method

.method public c(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->e(Z)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDefaultTrafficMode(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setTrafficMode(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->V()V

    return-void
.end method

.method public e(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->d(Z)V

    return-void
.end method

.method public f()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->aj()V

    return-void
.end method

.method public f(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->f(Z)V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LO/z;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LO/z;->a(I)LO/N;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, LO/N;->b()I

    move-result v2

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v1}, LO/z;->m()LO/U;

    move-result-object v1

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/maps/driveabout/app/dw;->a(Landroid/content/Context;LO/N;Lo/u;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "UIEventHandler"

    const-string v1, "Unable to show street view: no step"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dw;->a(Landroid/content/Context;LO/N;)V

    goto :goto_0
.end method

.method public g(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->M()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->N()V

    goto :goto_0
.end method

.method public h()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->W()V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->X()V

    return-void
.end method

.method public j()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->Q()V

    return-void
.end method

.method public k()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->R()V

    return-void
.end method

.method public l()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->S()V

    return-void
.end method

.method public m()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->T()V

    return-void
.end method

.method public n()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->U()V

    return-void
.end method

.method public o()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Lo/aR;)V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->f()V

    return-void
.end method

.method public q()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->M()V

    return-void
.end method

.method public r()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dO;->i:Ll/f;

    invoke-virtual {v0, v1}, LQ/s;->a(Ll/f;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->v()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->u()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->v()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->P()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->B()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    goto :goto_0
.end method

.method public t()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->u()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dO;->v()V

    goto :goto_0
.end method

.method public u()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->c()V

    :goto_1
    const-string v0, "UIEventHandler"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    new-instance v1, Lcom/google/android/maps/driveabout/app/el;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/el;-><init>(Lcom/google/android/maps/driveabout/app/dO;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->h()V

    goto :goto_1
.end method

.method public v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "UIEventHandler"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->g()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->i()V

    goto :goto_0
.end method

.method public w()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->i()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->f()V

    goto :goto_0
.end method

.method public x()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->E()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->B()V

    :cond_0
    return-void
.end method

.method public y()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->O()V

    return-void
.end method

.method public z()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dO;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dO;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->N()V

    return-void
.end method
