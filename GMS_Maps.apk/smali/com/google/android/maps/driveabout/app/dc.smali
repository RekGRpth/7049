.class public Lcom/google/android/maps/driveabout/app/dc;
.super LR/v;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/maps/driveabout/app/dc;


# instance fields
.field private c:Law/h;

.field private d:LO/t;

.field private e:Lcom/google/android/maps/driveabout/app/dD;

.field private f:Lcom/google/android/maps/driveabout/app/dD;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/dc;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dc;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/dc;->a:Lcom/google/android/maps/driveabout/app/dc;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, LR/v;-><init>()V

    return-void
.end method

.method private a(Lr/t;Lcom/google/android/maps/driveabout/app/dB;LO/t;)Lcom/google/android/maps/driveabout/app/dD;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/dD;

    invoke-direct {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/dD;-><init>(Lr/t;Lcom/google/android/maps/driveabout/app/dB;)V

    invoke-virtual {p3, v0}, LO/t;->a(LO/q;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/maps/driveabout/app/dc;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/dc;->a:Lcom/google/android/maps/driveabout/app/dc;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/dc;->c(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/maps/driveabout/app/dc;->a:Lcom/google/android/maps/driveabout/app/dc;

    return-object v0
.end method

.method private static a(Lcom/google/android/maps/driveabout/app/dD;Lcom/google/android/maps/driveabout/app/dB;Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/app/dd;

    const-string v1, "OfflineReroutingHelper"

    invoke-direct {v0, v1, p2, p1, p0}, Lcom/google/android/maps/driveabout/app/dd;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/maps/driveabout/app/dB;Lcom/google/android/maps/driveabout/app/dD;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dd;->start()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->e:Lcom/google/android/maps/driveabout/app/dD;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dD;->o_()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dD;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dD;->o_()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    invoke-virtual {v0}, LO/t;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->c:Law/h;

    invoke-virtual {v0}, Law/h;->u()V

    return-void
.end method

.method protected b(Landroid/content/Context;)V
    .locals 3

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->c:Law/h;

    invoke-static {p1}, LO/t;->a(Landroid/content/Context;)LO/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    sget-object v0, LA/c;->b:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/I;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dB;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/app/dB;-><init>()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dc;->a(Lr/t;Lcom/google/android/maps/driveabout/app/dB;LO/t;)Lcom/google/android/maps/driveabout/app/dD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->e:Lcom/google/android/maps/driveabout/app/dD;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->e:Lcom/google/android/maps/driveabout/app/dD;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dD;->n_()V

    sget-object v0, LA/c;->i:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/w;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dB;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/app/dB;-><init>()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dc;->a(Lr/t;Lcom/google/android/maps/driveabout/app/dB;LO/t;)Lcom/google/android/maps/driveabout/app/dD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dD;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dD;

    invoke-static {v0, v1, p1}, Lcom/google/android/maps/driveabout/app/dc;->a(Lcom/google/android/maps/driveabout/app/dD;Lcom/google/android/maps/driveabout/app/dB;Landroid/content/Context;)V

    return-void
.end method
