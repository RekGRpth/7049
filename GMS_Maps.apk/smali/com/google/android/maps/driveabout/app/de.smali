.class public Lcom/google/android/maps/driveabout/app/de;
.super Lcom/google/android/maps/driveabout/vector/J;
.source "SourceFile"


# instance fields
.field private volatile b:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LO/z;)V
    .locals 6

    invoke-virtual {p2}, LO/z;->n()Lo/X;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/maps/driveabout/app/de;->a(LO/z;)[I

    move-result-object v4

    const v0, 0x7f0b0017

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/J;-><init>(Lo/X;II[IF)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/de;->b:Z

    return-void
.end method

.method private static a(LO/z;)[I
    .locals 7

    const/4 v2, 0x1

    invoke-virtual {p0}, LO/z;->d()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    new-array v4, v0, [I

    invoke-virtual {p0}, LO/z;->u()Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    array-length v1, v4

    if-ge v0, v1, :cond_1

    aput v2, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v4

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/F;

    invoke-virtual {v0}, LO/F;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v1, v2

    :goto_2
    invoke-virtual {v0}, LO/F;->b()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    :goto_3
    invoke-virtual {v0}, LO/F;->c()I

    move-result v6

    if-gt v3, v6, :cond_3

    aput v1, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :pswitch_0
    const/4 v1, 0x2

    goto :goto_2

    :pswitch_1
    const/4 v1, 0x3

    goto :goto_2

    :pswitch_2
    const/4 v1, 0x4

    goto :goto_2

    :pswitch_3
    const/4 v1, 0x5

    goto :goto_2

    :cond_4
    move-object v0, v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 2

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v0, v1, :cond_1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_2

    :cond_1
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/de;->b(I)V

    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/J;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/de;->b(I)V

    goto :goto_1
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/de;->b:Z

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 2

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/de;->b:Z

    if-nez v1, :cond_0

    const/high16 v1, 0x41700000

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/de;->c(Z)V

    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/J;->b(LC/a;LD/a;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
