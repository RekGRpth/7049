.class public Lcom/google/android/maps/driveabout/app/dz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static b:Lcom/google/android/maps/driveabout/app/dz;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/maps/driveabout/app/aK;

.field private final e:C

.field private final f:C

.field private final g:Ljava/util/HashSet;

.field private final h:Ljava/util/HashSet;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:LO/P;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "%1$s"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/google/android/maps/driveabout/app/dz;->a:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aK;

    invoke-direct {v0, p1}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->d:Lcom/google/android/maps/driveabout/app/aK;

    const v0, 0x7f0d00c2

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->b(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->h:Ljava/util/HashSet;

    const v0, 0x7f0d00c3

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->b(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->g:Ljava/util/HashSet;

    const v0, 0x7f0d0026

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Lcom/google/android/maps/driveabout/app/dz;->e:C

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Lcom/google/android/maps/driveabout/app/dz;->f:C

    return-void
.end method

.method private a(Ljava/lang/String;I)I
    .locals 3

    const/16 v0, 0x20

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    if-lez v0, :cond_0

    if-le v0, p2, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->h:Ljava/util/HashSet;

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IIIF)Landroid/text/Spannable;
    .locals 9

    const v8, 0x7f09007a

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v7, 0x21

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x2

    :goto_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dz;->d:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v3, p1, p2, v1, v0}, Lcom/google/android/maps/driveabout/app/aK;->a(IIZI)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move v0, v2

    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_0

    iget-char v6, p0, Lcom/google/android/maps/driveabout/app/dz;->e:C

    if-ne v5, v6, :cond_9

    :cond_0
    if-lez v0, :cond_2

    and-int/lit8 v5, p3, 0x1

    if-eqz v5, :cond_1

    new-instance v5, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v5}, Landroid/text/style/SuperscriptSpan;-><init>()V

    invoke-virtual {v4, v5, v2, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const v6, 0x3f19999a

    invoke-direct {v5, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v4, v5, v2, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    and-int/lit8 v5, p3, 0x2

    if-eqz v5, :cond_2

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4, v5, v2, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    :goto_2
    if-le v2, v0, :cond_6

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_3

    iget-char v6, p0, Lcom/google/android/maps/driveabout/app/dz;->f:C

    if-ne v5, v6, :cond_a

    :cond_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_6

    and-int/lit8 v5, p3, 0x1

    if-eqz v5, :cond_4

    new-instance v5, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v5}, Landroid/text/style/SuperscriptSpan;-><init>()V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    const/high16 v5, 0x3f800000

    cmpl-float v5, p4, v5

    if-eqz v5, :cond_5

    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v5, p4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_5
    and-int/lit8 v5, p3, 0x2

    if-eqz v5, :cond_6

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v4, v5, v2, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    if-ge v0, v2, :cond_7

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4, v3, v0, v2, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_7
    return-object v4

    :cond_8
    move v0, v1

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_a
    add-int/lit8 v2, v2, -0x1

    goto :goto_2
.end method

.method private static a(LO/P;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;
    .locals 6

    const/16 v5, 0x21

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/dj;

    const v2, 0x3f99999a

    invoke-direct {v1, p1, v2}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p0}, LO/P;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0}, LO/P;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    new-instance v3, Landroid/text/style/RelativeSizeSpan;

    const/high16 v4, 0x3f400000

    invoke-direct {v3, v4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0, v3, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v3, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Landroid/text/Spannable;
    .locals 5

    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;IIZ)Landroid/text/Spannable;
    .locals 2

    invoke-virtual {p0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/high16 v1, 0x3fc00000

    invoke-static {p0, v0, p2, v1, p3}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .locals 3

    const v0, 0x7f020152

    const/high16 v1, 0x3f800000

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;
    .locals 6

    const/16 v5, 0x21

    const/4 v4, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090081

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v1, "  "

    invoke-virtual {v0, v4, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/driveabout/app/dj;

    invoke-direct {v2, v1, p3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 3

    const/4 v0, 0x0

    const/high16 v1, 0x3fc00000

    const/4 v2, 0x1

    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/text/Spannable;
    .locals 11

    const/16 v8, 0x21

    const/4 v7, 0x1

    const v6, 0x3f19999a

    const-string v0, "%1$s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    sget v0, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int v1, v3, v0

    const-string v0, "%2$s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    sget v0, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v0, v2

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-ge v3, v2, :cond_3

    move v9, v2

    move v2, v1

    move v1, v9

    :goto_0
    if-eqz p4, :cond_2

    if-lez v3, :cond_0

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5, v3, v6}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_0
    if-ge v2, v1, :cond_1

    invoke-direct {p0, v4, v2, v1, v6}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {p0, v4, v0, v5, v6}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_2
    invoke-virtual {v4, v1, v0, p3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v4, v0, v1, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v4, v3, v2, p2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {v4, v0, v3, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v4

    :cond_3
    move-object v9, p2

    move-object p2, p3

    move-object p3, v9

    move v10, v1

    move v1, v3

    move v3, v2

    move v2, v0

    move v0, v10

    goto :goto_0
.end method

.method public static a()Lcom/google/android/maps/driveabout/app/dz;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lcom/google/android/maps/driveabout/app/dz;

    return-object v0
.end method

.method public static a(Landroid/content/Context;LO/U;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LO/V;->a()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0d00a0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LO/U;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, LO/V;->a()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, LO/V;->a()I

    move-result v4

    if-ge v0, v4, :cond_4

    if-lez v0, :cond_3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v2, v0}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v3, v4, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 8

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, LO/P;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LO/P;->e()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_4
    if-eqz v6, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_5
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_6
    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    move-object p0, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lcom/google/android/maps/driveabout/app/dz;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/dz;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dz;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lcom/google/android/maps/driveabout/app/dz;

    :cond_0
    return-void
.end method

.method private a(Landroid/text/Spannable;IIF)V
    .locals 4

    const/16 v3, 0x21

    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v0, p4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-interface {p1, v0, p2, p3, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {p1, v0, p2, p3, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v2, LO/P;

    invoke-virtual {p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    aget-object v0, v2, v1

    check-cast v0, LO/P;

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;LO/N;LO/P;I)V
    .locals 3

    if-eqz p3, :cond_0

    invoke-virtual {p3}, LO/P;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-static {v0, p2}, LO/m;->a(Landroid/content/Context;LO/N;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "  "

    invoke-virtual {p1, p4, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dj;

    const/high16 v2, 0x3f800000

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    add-int/lit8 v0, p4, 0x1

    const/16 v2, 0x21

    invoke-virtual {p1, v1, p4, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;Lv/c;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v2, LO/P;

    invoke-virtual {p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_2

    aget-object v0, v3, v1

    check-cast v0, LO/P;

    const/4 v2, 0x0

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p2}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lv/a;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lv/a;->c()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    invoke-virtual {v4}, Lv/a;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/P;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {p1, v4, v5, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_1
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {p1, v2, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/lang/String;I)I

    move-result v1

    if-gez v1, :cond_0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;I)I
    .locals 3

    const/16 v0, 0x20

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v0

    if-lez v0, :cond_0

    add-int/lit8 v1, p2, -0x1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->g:Ljava/util/HashSet;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Landroid/text/Spannable;
    .locals 6

    const/16 v5, 0x21

    const v0, 0x7f0d00ec

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableString;

    const v2, 0x7f0d00da

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v3, "%2$s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    sget v3, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v3, v0

    invoke-virtual {v2, v0, v3, v1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "%1$s"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    sget v3, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v3, v1

    const-string v4, " "

    invoke-virtual {v2, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Lcom/google/android/maps/driveabout/app/dj;

    const/high16 v4, 0x3f800000

    invoke-direct {v3, v0, v4}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v2, v3, v1, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v2
.end method

.method public static b(Landroid/content/Context;LO/U;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d000a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;LO/U;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Ljava/util/HashSet;
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private b(LO/P;)Z
    .locals 1

    invoke-virtual {p1}, LO/P;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LO/P;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)I
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/dz;->b(Ljava/lang/String;I)I

    move-result v1

    if-gez v1, :cond_0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method a(LO/P;)Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:LO/P;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->i:Landroid/graphics/drawable/Drawable;

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040038

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, LO/P;->g()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const v1, 0x7f020104

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    :goto_1
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->setDensity(I)V

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    monitor-enter p0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->i:Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dz;->j:LO/P;

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    invoke-virtual {p1}, LO/P;->g()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const v1, 0x7f020105

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_2
    const v1, 0x7f020106

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public a(II)Landroid/text/Spannable;
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v1, 0x7f0d000c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000

    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/android/maps/driveabout/app/dz;->a(IIIF)Landroid/text/Spannable;

    move-result-object v2

    const-string v3, "%1$s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    sget v3, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v3, v0

    invoke-virtual {v1, v0, v3, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    return-object v1
.end method

.method public a(III)Landroid/text/Spannable;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Landroid/text/SpannableString;

    const-string v2, " "

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/ScaleXSpan;

    const/high16 v3, 0x40800000

    invoke-direct {v2, v3}, Landroid/text/style/ScaleXSpan;-><init>(F)V

    const/4 v3, 0x1

    const/16 v4, 0x21

    invoke-interface {v1, v2, v5, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p0, p1, p3, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(IIZ)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0, p2, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(IZ)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method public a(IIZ)Landroid/text/Spannable;
    .locals 2

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const v1, 0x3f19999a

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(IIIF)Landroid/text/Spannable;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a(ILO/N;I)Landroid/text/Spannable;
    .locals 4

    if-gtz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/maps/driveabout/app/dz;->b(LO/N;Lv/c;)Landroid/text/Spannable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v2, 0x7f0d002e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "%1$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const-string v2, "%1$s"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    const/4 v3, 0x1

    invoke-virtual {p0, p1, p3, v3}, Lcom/google/android/maps/driveabout/app/dz;->a(IIZ)Landroid/text/Spannable;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "%2$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const-string v2, "%2$s"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p2}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/SpannableStringBuilder;)V

    goto :goto_0
.end method

.method public a(IZ)Landroid/text/Spannable;
    .locals 8

    const/4 v2, 0x0

    const v7, 0x3f19999a

    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/maps/driveabout/app/dA;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/dA;-><init>(Lcom/google/android/maps/driveabout/app/dz;I)V

    iget v3, v0, Lcom/google/android/maps/driveabout/app/dA;->a:I

    iget v4, v0, Lcom/google/android/maps/driveabout/app/dA;->b:I

    iget v5, v0, Lcom/google/android/maps/driveabout/app/dA;->c:I

    if-lez v3, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v2, 0x7f0d0028

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/text/Spannable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-gtz v4, :cond_1

    if-eqz p2, :cond_5

    :cond_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    if-eqz p2, :cond_3

    const v0, 0x7f0d002d

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_2

    const/16 v6, 0xa

    if-ge v5, v6, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x30

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    if-nez p2, :cond_4

    :goto_2
    invoke-direct {p0, v3, v4, v0, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v0, 0x7f0d002a

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v3, 0x7f0d002c

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "%1$s"

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    sget v0, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int v6, v4, v0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-lez v4, :cond_6

    invoke-direct {p0, v0, v2, v4, v7}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_6
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v6, v2, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v0, v6, v2, v7}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_7
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v6, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v4

    const/16 v2, 0x21

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public a(LO/N;IILv/c;)Landroid/text/Spannable;
    .locals 17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0020

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    div-float/2addr v3, v4

    const v4, 0x3f19999a

    mul-float v5, v3, v4

    const/4 v3, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(IIIF)Landroid/text/Spannable;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, LO/N;->x()LO/P;

    move-result-object v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_0
    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, LO/N;->y()LO/P;

    move-result-object v7

    if-eqz v7, :cond_5

    if-eqz v3, :cond_0

    invoke-virtual/range {p1 .. p1}, LO/N;->x()LO/P;

    move-result-object v7

    invoke-virtual {v7}, LO/P;->d()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_5

    :cond_0
    invoke-virtual/range {p1 .. p1}, LO/N;->y()LO/P;

    move-result-object v3

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v7, 0x7f0d001b

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "%1$s"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const-string v8, "%2$s"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v11

    sget v12, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v12, v10

    sget v13, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v13, v11

    if-lez v10, :cond_1

    new-instance v14, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v14, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/4 v15, 0x0

    const/16 v16, 0x21

    move/from16 v0, v16

    invoke-virtual {v9, v14, v15, v10, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v14, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09007a

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    invoke-direct {v14, v15}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v15, 0x0

    const/16 v16, 0x21

    move/from16 v0, v16

    invoke-virtual {v9, v14, v15, v10, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    if-le v11, v12, :cond_2

    new-instance v10, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v10, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/16 v14, 0x21

    invoke-virtual {v9, v10, v12, v11, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v10, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f09007a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-direct {v10, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v14, 0x21

    invoke-virtual {v9, v10, v12, v11, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v13, v10, :cond_3

    new-instance v10, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v10, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v11, 0x21

    invoke-virtual {v9, v10, v13, v5, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09007a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-direct {v5, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v10, 0x21

    invoke-virtual {v9, v5, v13, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_3
    if-nez v3, :cond_6

    const-string v4, ""

    :goto_2
    if-le v8, v7, :cond_7

    sget v5, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v5, v8

    invoke-virtual {v9, v8, v5, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1, v3, v8}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/SpannableStringBuilder;LO/N;LO/P;I)V

    sget v3, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v3, v7

    invoke-virtual {v9, v7, v3, v6}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_3
    return-object v9

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_5
    if-eqz v3, :cond_8

    invoke-virtual/range {p1 .. p1}, LO/N;->x()LO/P;

    move-result-object v3

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/P;Lv/c;)Landroid/text/Spannable;

    move-result-object v4

    goto :goto_2

    :cond_7
    sget v5, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v5, v7

    invoke-virtual {v9, v7, v5, v6}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    sget v5, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v5, v8

    invoke-virtual {v9, v8, v5, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1, v3, v8}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/SpannableStringBuilder;LO/N;LO/P;I)V

    goto :goto_3

    :cond_8
    move-object v3, v4

    goto/16 :goto_1
.end method

.method public a(LO/N;Lv/c;)Landroid/text/Spannable;
    .locals 4

    invoke-virtual {p1}, LO/N;->x()LO/P;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/P;Lv/c;)Landroid/text/Spannable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v2, 0x7f0d0019

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%1$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v1, v2

    invoke-virtual {v3, v2, v1, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    return-object v3
.end method

.method a(LO/P;Lv/c;)Landroid/text/Spannable;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, LO/P;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LO/P;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lv/a;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lv/a;->c()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lv/a;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/P;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p1}, LO/P;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/P;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LO/P;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_1
.end method

.method public a(LO/Q;)Landroid/text/Spannable;
    .locals 5

    invoke-virtual {p1}, LO/Q;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LO/Q;->a()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/16 v2, 0x9

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Landroid/text/Spannable;
    .locals 5

    const/4 v2, 0x0

    const v4, 0x3f19999a

    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dz;->b(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dz;->c(Ljava/lang/String;)I

    move-result v0

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    move v1, v2

    :cond_0
    if-lez v1, :cond_1

    invoke-direct {p0, v3, v2, v1, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v3, v0, v2, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_2
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-interface {v3, v2, v1, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    return-object v3
.end method

.method public a(J)Ljava/lang/CharSequence;
    .locals 11

    const v10, 0xef01

    const v9, 0xef00

    const/16 v8, 0x21

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    instance-of v1, v0, Ljava/text/SimpleDateFormat;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v5, -0x1

    move v1, v2

    move v3, v2

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_9

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x27

    if-ne v6, v7, :cond_0

    if-nez v3, :cond_2

    move v3, v4

    :cond_0
    :goto_2
    if-nez v3, :cond_3

    const/16 v7, 0x61

    if-ne v6, v7, :cond_3

    move v3, v1

    :goto_3
    if-ltz v3, :cond_5

    move v1, v3

    :goto_4
    if-lez v1, :cond_4

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-eqz v5, :cond_4

    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_1
    const-string v0, "HH:mm"

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "a"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_5
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v3, :cond_8

    if-le v0, v3, :cond_8

    const v5, 0x3f19999a

    invoke-direct {p0, v1, v3, v0, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    if-lez v3, :cond_6

    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v5, v2, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v2, v0, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_7
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v1, v3, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    :goto_5
    return-object v1

    :cond_8
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_5

    :cond_9
    move v3, v5

    goto/16 :goto_3
.end method

.method public a(Ljava/util/Collection;IILandroid/text/TextPaint;ILv/c;)Ljava/lang/CharSequence;
    .locals 14

    const v1, 0x7f0d0007

    move/from16 v0, p5

    if-eq v0, v1, :cond_0

    const v1, 0x7f0d0008

    move/from16 v0, p5

    if-ne v0, v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v2, "%1$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    sget v3, Lcom/google/android/maps/driveabout/app/dz;->a:I

    add-int/2addr v3, v2

    if-lez v2, :cond_1

    const/4 v4, 0x0

    const v5, 0x3f19999a

    invoke-direct {p0, v9, v4, v2, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const v4, 0x3f19999a

    invoke-direct {p0, v9, v3, v1, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/Spannable;IIF)V

    :cond_2
    const-string v1, ""

    invoke-virtual {v9, v2, v3, v1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v3, 0x7f0d0009

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v6, " "

    const v1, 0x7f0d0009

    move/from16 v0, p5

    if-ne v0, v1, :cond_d

    const/4 v1, 0x0

    invoke-virtual {v9, v1, v7}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v9, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v2

    :goto_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v5, v4

    move v4, v3

    move v3, v1

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    move/from16 v0, p2

    if-ge v5, v0, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LO/P;

    move-object/from16 v0, p6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/P;Lv/c;)Landroid/text/Spannable;

    move-result-object v8

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v11

    move/from16 v0, p2

    if-gt v11, v0, :cond_4

    invoke-virtual {v9, v3, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v2

    add-int/2addr v2, v3

    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "\n"

    invoke-virtual {v9, v2, v3}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v2, v2, 0x1

    move v3, v4

    move v4, v5

    :goto_3
    move v5, v4

    move v4, v3

    move v3, v2

    move-object v2, v1

    goto :goto_2

    :cond_3
    const-string v1, "%1$s"

    goto/16 :goto_0

    :cond_4
    if-nez v2, :cond_5

    invoke-virtual {v9, v3, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v2

    add-int/2addr v2, v3

    move v3, v4

    move v4, v5

    goto :goto_3

    :cond_5
    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/dz;->b(LO/P;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual {v2}, LO/P;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/app/dz;->b(LO/P;)Z

    move-result v2

    if-nez v2, :cond_b

    move-object v2, v7

    :goto_4
    invoke-virtual {v9, v3, v2}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v11

    add-int/2addr v11, v3

    invoke-virtual {v9, v11, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v8

    add-int/2addr v8, v11

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    move-object/from16 v0, p4

    invoke-static {v9, v4, v11, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F

    move-result v11

    move/from16 v0, p3

    int-to-float v12, v0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_8

    add-int/lit8 v4, p2, -0x1

    if-ge v5, v4, :cond_7

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/2addr v4, v3

    const-string v11, "\n"

    invoke-virtual {v9, v3, v4, v11}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v8, v2

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v4, v3, 0x1

    move v3, v4

    move v4, v5

    goto :goto_3

    :cond_7
    invoke-virtual {v9, v3, v8}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-object v1, v9

    :goto_5
    return-object v1

    :cond_8
    if-eq v2, v6, :cond_9

    new-instance v11, Landroid/text/style/ForegroundColorSpan;

    iget-object v12, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f09007a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-direct {v11, v12}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v2, v3

    const/16 v12, 0x21

    invoke-virtual {v9, v11, v3, v2, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_9
    move v2, v8

    move v3, v4

    move v4, v5

    goto/16 :goto_3

    :cond_a
    move-object v1, v9

    goto :goto_5

    :cond_b
    move-object v2, v6

    goto :goto_4

    :cond_c
    move v3, v4

    move v4, v5

    goto/16 :goto_3

    :cond_d
    move v1, v2

    goto/16 :goto_1
.end method

.method public a(I)Ljava/lang/String;
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v3, Lcom/google/android/maps/driveabout/app/dA;

    invoke-direct {v3, p0, p1}, Lcom/google/android/maps/driveabout/app/dA;-><init>(Lcom/google/android/maps/driveabout/app/dz;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    iget v2, v3, Lcom/google/android/maps/driveabout/app/dA;->a:I

    new-array v4, v9, [Ljava/lang/Object;

    iget v5, v3, Lcom/google/android/maps/driveabout/app/dA;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0001

    iget v4, v3, Lcom/google/android/maps/driveabout/app/dA;->b:I

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, v3, Lcom/google/android/maps/driveabout/app/dA;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0e0002

    iget v5, v3, Lcom/google/android/maps/driveabout/app/dA;->c:I

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, v3, Lcom/google/android/maps/driveabout/app/dA;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget v4, v3, Lcom/google/android/maps/driveabout/app/dA;->a:I

    if-lez v4, :cond_1

    iget v2, v3, Lcom/google/android/maps/driveabout/app/dA;->b:I

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v3, 0x7f0d0029

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, v3, Lcom/google/android/maps/driveabout/app/dA;->b:I

    if-lez v0, :cond_3

    iget v0, v3, Lcom/google/android/maps/driveabout/app/dA;->c:I

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    const v3, 0x7f0d002b

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v1, v4, v8

    aput-object v2, v4, v9

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method public a(LO/U;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(LO/N;Lv/c;)Landroid/text/Spannable;
    .locals 2

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/text/SpannableStringBuilder;Lv/c;)V

    return-object v0
.end method
