.class Lcom/google/android/maps/driveabout/app/eD;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcom/google/android/maps/driveabout/app/NavigationService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/maps/driveabout/app/eD;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eD;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 2

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/eD;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/google/android/maps/driveabout/app/eD;->a:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eD;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/eD;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-ne v0, v1, :cond_2

    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    :cond_2
    return-void
.end method
