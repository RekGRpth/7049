.class Lcom/google/android/maps/driveabout/app/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/U;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/a;

.field private final b:LK/V;

.field private c:LK/a;

.field private final d:Lcom/google/android/maps/driveabout/app/d;

.field private e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/a;LK/V;Lcom/google/android/maps/driveabout/app/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/g;->d:Lcom/google/android/maps/driveabout/app/d;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/g;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/g;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/g;->d()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/g;)LK/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    return-object v0
.end method

.method private d()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/g;->a(Z)Z

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/g;->f()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/g;)Lcom/google/android/maps/driveabout/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->d:Lcom/google/android/maps/driveabout/app/d;

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/maps/driveabout/app/h;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/h;-><init>(Lcom/google/android/maps/driveabout/app/g;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/a;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->j(Lcom/google/android/maps/driveabout/app/a;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    invoke-virtual {v0}, LK/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    new-instance v1, Lcom/google/android/maps/driveabout/app/i;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/i;-><init>(Lcom/google/android/maps/driveabout/app/g;)V

    invoke-virtual {v0, v1}, LK/a;->a(LK/b;)V

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/g;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/g;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/g;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LK/V;LK/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/a;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/g;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/g;)Z
    .locals 5

    const/4 v0, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-virtual {v1}, LK/V;->d()LO/j;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-virtual {v2}, LK/V;->d()LO/j;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LO/j;->a()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    invoke-virtual {v1}, LO/j;->a()I

    move-result v3

    invoke-virtual {v2}, LO/j;->a()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, LO/j;->h()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LO/j;->h()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->a()Lo/T;

    move-result-object v1

    invoke-virtual {v2}, LO/j;->e()LO/N;

    move-result-object v2

    invoke-virtual {v2}, LO/N;->a()Lo/T;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Z)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->e:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v3, LK/L;

    invoke-direct {v3}, LK/L;-><init>()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/a;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/a;->c(Lcom/google/android/maps/driveabout/app/a;)LK/c;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-interface {v2, v4}, LK/c;->a(LK/V;)LK/a;

    move-result-object v2

    invoke-virtual {v3, v2}, LK/L;->a(LK/a;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    const-string v4, "a"

    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/a;->d(Lcom/google/android/maps/driveabout/app/a;)LK/T;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/a;->e(Lcom/google/android/maps/driveabout/app/a;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/a;->d(Lcom/google/android/maps/driveabout/app/a;)LK/T;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-interface {v2, v4}, LK/T;->a(LK/V;)LK/a;

    move-result-object v2

    if-nez v2, :cond_3

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->d(Lcom/google/android/maps/driveabout/app/a;)LK/T;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-interface {v0, v2, p0}, LK/T;->a(LK/V;LK/U;)V

    move v0, v1

    goto :goto_0

    :cond_3
    move-object v1, v2

    :goto_1
    if-nez v1, :cond_4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/a;->f(Lcom/google/android/maps/driveabout/app/a;)LK/T;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v1, "f"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/a;->f(Lcom/google/android/maps/driveabout/app/a;)LK/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-interface {v1, v2}, LK/T;->a(LK/V;)LK/a;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    const-string v1, "y"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/a;->g(Lcom/google/android/maps/driveabout/app/a;)LK/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-interface {v1, v2}, LK/T;->a(LK/V;)LK/a;

    move-result-object v1

    :cond_5
    if-eqz v1, :cond_6

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/a;->h(Lcom/google/android/maps/driveabout/app/a;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    const-string v2, "c"

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/a;->i(Lcom/google/android/maps/driveabout/app/a;)LK/c;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/g;->b:LK/V;

    invoke-interface {v2, v4}, LK/c;->a(LK/V;)LK/a;

    move-result-object v2

    invoke-virtual {v3, v2}, LK/L;->a(LK/a;)V

    :cond_7
    invoke-virtual {v3, v1}, LK/L;->a(LK/a;)V

    :cond_8
    invoke-virtual {v3}, LK/L;->a()LK/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    goto/16 :goto_0

    :cond_9
    move-object v1, v2

    goto :goto_1
.end method

.method b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    invoke-virtual {v0}, LK/a;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->b()V

    :cond_1
    return-void
.end method

.method c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    invoke-virtual {v0}, LK/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->a:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/g;->c:LK/a;

    invoke-virtual {v0}, LK/a;->b()V

    :cond_1
    return-void
.end method
