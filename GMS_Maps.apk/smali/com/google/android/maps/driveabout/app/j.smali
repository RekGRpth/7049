.class public Lcom/google/android/maps/driveabout/app/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/media/AudioManager;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/j;->b:Z

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/j;->a:Landroid/media/AudioManager;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/j;->a:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    const-string v0, "a"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/j;->a:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/j;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/j;->b:Z

    return-void
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/j;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/j;->b:Z

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/j;->b:Z

    return v0
.end method

.method protected d()Z
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/j;->a:Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/j;->a:Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
