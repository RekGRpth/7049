.class public Lcom/google/android/maps/driveabout/power/PowerManagerPreference;
.super Landroid/preference/CheckBoxPreference;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final onClick()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/power/PowerManagerPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UserPreference"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->b(Ljava/lang/String;)V

    const-string v0, "M"

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/power/PowerManagerPreference;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "UserPreference"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    const-string v0, "M"

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/power/PowerManagerPreference;->setChecked(Z)V

    goto :goto_0
.end method
