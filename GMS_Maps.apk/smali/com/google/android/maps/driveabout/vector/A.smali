.class public Lcom/google/android/maps/driveabout/vector/A;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/LinkedList;

.field private e:Z

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;

.field private h:Lo/aQ;

.field private i:Ljava/util/List;

.field private final j:Lcom/google/android/maps/driveabout/vector/E;

.field private k:I

.field private l:Z

.field private m:LF/H;

.field private n:Lcom/google/android/maps/driveabout/vector/C;

.field private o:Lcom/google/android/maps/driveabout/vector/B;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;Lcom/google/android/maps/driveabout/vector/x;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/A;->k:I

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/A;->j:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/A;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->b(LC/a;)V

    return-void
.end method

.method private a(Ljava/lang/RuntimeException;Ljava/util/Iterator;I)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "#:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/A;->k:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/maps/driveabout/vector/A;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " T:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " E:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " C:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " numM:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "GLMarkerOverlay"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(LC/a;)V
    .locals 5

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->f()Lo/D;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lo/D;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ln/s;->e(Lo/r;)Ln/k;

    move-result-object v3

    if-eqz v3, :cond_0

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, LF/H;->e()Lo/T;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v4, v3}, Lo/T;->b(I)V

    invoke-virtual {v0, v4}, LF/H;->a(Lo/T;)V

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->c(LC/a;)V

    return-void
.end method

.method private c(FFLC/a;)V
    .locals 2

    const/high16 v0, 0x428c0000

    sub-float v0, p2, v0

    invoke-virtual {p3, p1, v0}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v1, v0}, LF/H;->a(Lo/T;)V

    return-void
.end method

.method private c(LC/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0, p1}, LF/H;->b(LC/a;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method private d(LF/H;)V
    .locals 2

    invoke-virtual {p1}, LF/H;->r()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->n()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, LF/H;->s()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->o()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private e(LF/H;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->o:Lcom/google/android/maps/driveabout/vector/B;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->o:Lcom/google/android/maps/driveabout/vector/B;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/B;->a(LF/H;)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->e:Z

    return-void
.end method

.method private declared-synchronized j()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->r()I

    invoke-virtual {v0}, LF/H;->s()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;)LF/H;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(LC/a;)V
    .locals 8

    const v7, -0x41b33333

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->h:Lo/aQ;

    invoke-virtual {v1, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    :goto_1
    invoke-virtual {v1}, Lo/aQ;->a()Lo/aR;

    move-result-object v2

    invoke-virtual {v1}, Lo/aQ;->d()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-virtual {v0, v3, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    invoke-virtual {v1}, Lo/aQ;->e()Lo/T;

    move-result-object v3

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v3

    invoke-virtual {v1}, Lo/aQ;->g()Lo/T;

    move-result-object v4

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v4

    invoke-virtual {v1}, Lo/aQ;->f()Lo/T;

    move-result-object v5

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v6

    invoke-virtual {v5, v6, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lo/aQ;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/aQ;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->n()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, LF/H;->e()Lo/T;

    move-result-object v5

    invoke-virtual {v2, v5}, Lo/aR;->a(Lo/T;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1, v5}, Lo/aQ;->a(Lo/T;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    invoke-virtual {v3, v5}, Lo/aQ;->a(Lo/T;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, p1}, LF/H;->a(LC/a;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_3
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v0, v4, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/lang/RuntimeException;Ljava/util/Iterator;I)V

    :cond_4
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->h:Lo/aQ;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->e:Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_1
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 6

    const/4 v1, 0x1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/vector/A;->a(LC/a;)V

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/A;->b(LC/a;)V

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/A;->c(LC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    monitor-exit p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->p()V

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v0, p1, LD/a;->g:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    new-instance v3, Lcom/google/android/maps/driveabout/vector/aT;

    invoke-direct {v3, p3}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/r;)V

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->o()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0, p1, p2, v3}, LF/H;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->q()Z

    move-result v5

    if-eqz v5, :cond_4

    move-object v2, v0

    :cond_4
    invoke-virtual {v0, p1, p2, v3}, LF/H;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_2

    :cond_5
    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    if-eqz v2, :cond_6

    invoke-virtual {v2, p1, p2, v3}, LF/H;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_7
    const/4 v0, 0x2

    goto :goto_3
.end method

.method public declared-synchronized a(LF/H;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p0}, LF/H;->a(Lcom/google/android/maps/driveabout/vector/A;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LF/J;Z)V
    .locals 5

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-interface {p1, v0}, LF/J;->a(LF/H;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, LF/H;->q()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez p2, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LF/H;->d(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    :try_start_3
    invoke-virtual {v0}, LF/H;->q()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LF/H;->d(Z)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    :cond_2
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, LF/H;->p()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/A;->d(LF/H;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V

    goto :goto_0

    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method

.method public declared-synchronized a(Ljava/util/List;FFLo/T;LC/a;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p5}, Lcom/google/android/maps/driveabout/vector/A;->a(LC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/c;->a(FFLo/T;LC/a;)I

    move-result v2

    if-ge v2, p6, :cond_0

    new-instance v3, Lcom/google/android/maps/driveabout/vector/t;

    invoke-direct {v3, v0, p0, v2}, Lcom/google/android/maps/driveabout/vector/t;-><init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public b(LF/H;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, LF/H;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LF/H;->d(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->d(LF/H;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V

    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public b(FFLC/a;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/C;->b(LF/H;)V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LC/a;LD/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->j()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public declared-synchronized c(LF/H;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->d(LF/H;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(FFLo/T;LC/a;)Z
    .locals 4

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1, p2, p3, p4}, LF/H;->b(FFLo/T;LC/a;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/vector/C;->a(LF/H;)V

    :cond_1
    monitor-exit p0

    move v0, v1

    :goto_0
    return v0

    :cond_2
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d(FFLo/T;LC/a;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLC/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v1}, LF/H;->l()LF/I;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v1}, LF/H;->l()LF/I;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v2}, LF/H;->e()Lo/T;

    move-result-object v2

    invoke-interface {v1, v2}, LF/I;->a(Lo/T;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-interface {v1, v2}, Lcom/google/android/maps/driveabout/vector/C;->c(LF/H;)V

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public e()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->e()Lcom/google/android/maps/driveabout/vector/c;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, LF/H;

    if-eqz v2, :cond_0

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->m()Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->j()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method

.method public h()Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method public j_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->j:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
