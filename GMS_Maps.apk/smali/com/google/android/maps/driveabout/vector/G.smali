.class public Lcom/google/android/maps/driveabout/vector/G;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final a:Lo/T;

.field private static final b:Lo/T;


# instance fields
.field private final c:Ljava/util/List;

.field private final d:Lo/ad;

.field private final e:Ljava/util/List;

.field private final f:Ljava/lang/Object;

.field private g:Lm/v;

.field private h:Lo/ad;

.field private i:F

.field private j:F

.field private k:B

.field private l:Z

.field private m:Z

.field private n:LE/o;

.field private o:LE/d;

.field private p:Lo/ad;

.field private q:I

.field private final r:Ljava/lang/Object;

.field private s:I

.field private t:I

.field private final u:Ljava/util/List;

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lo/T;

    const/high16 v1, -0x40000000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/G;->a:Lo/T;

    new-instance v0, Lo/T;

    const/high16 v1, 0x40000000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/G;->b:Lo/T;

    return-void
.end method

.method public constructor <init>(Lo/X;Ljava/util/List;III)V
    .locals 8

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/G;->v:Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-virtual {p1}, Lo/X;->f()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-virtual {p1, v2}, Lo/X;->c(I)Lo/X;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-virtual {v0, v2}, Lo/X;->c(I)Lo/X;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->a()Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/G;->t:I

    iput p4, p0, Lcom/google/android/maps/driveabout/vector/G;->s:I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/z;

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/G;->t:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/G;->s:I

    const/4 v7, 0x0

    invoke-direct {v4, v0, v5, v6, v7}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/G;->q:I

    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p5, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x78

    const-string v2, "t"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method private static a(Lo/ad;Lo/ad;)B
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1, p0}, Lo/ad;->a(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    int-to-byte v0, v0

    :cond_0
    sget-object v1, Lcom/google/android/maps/driveabout/vector/G;->b:Lo/T;

    new-instance v2, Lo/ad;

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3, v1}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v4

    invoke-virtual {v4, v1}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    invoke-virtual {v2, p0}, Lo/ad;->a(Lo/ae;)Z

    move-result v2

    if-eqz v2, :cond_1

    or-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    :cond_1
    new-instance v2, Lo/ad;

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v4

    invoke-virtual {v4, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    invoke-virtual {v2, p0}, Lo/ad;->a(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    :cond_2
    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/G;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/G;Lm/v;)Lm/v;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    return-object p1
.end method

.method private a(LC/a;Z)V
    .locals 1

    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->a(LC/a;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->b(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->f(LC/a;)V

    :cond_1
    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->c(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->g(LC/a;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/G;->h()V

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LD/a;LC/a;)V
    .locals 3

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->p:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->p:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->b(LD/a;LC/a;Lo/T;F)V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/G;->q:I

    invoke-static {v0, v2}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->o:LE/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(LC/a;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    :goto_1
    if-eq v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/G;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/G;->m:Z

    return p1
.end method

.method private static b(I)I
    .locals 2

    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    return-object v0
.end method

.method private b(LD/a;LC/a;)V
    .locals 12

    invoke-static {p2}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v6

    and-int/lit8 v0, v6, 0x1

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    move v3, v0

    :goto_0
    if-eqz v3, :cond_6

    const/4 v0, 0x1

    :goto_1
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    and-int/lit8 v1, v6, 0x4

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {v5}, Lm/v;->e()I

    move-result v7

    new-instance v8, LE/o;

    mul-int v9, v7, v0

    invoke-direct {v8, v9}, LE/o;-><init>(I)V

    new-instance v9, LE/d;

    invoke-virtual {v5}, Lm/v;->d()I

    move-result v10

    mul-int/lit8 v10, v10, 0x3

    mul-int/2addr v0, v10

    invoke-direct {v9, v0}, LE/d;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {v4}, Lo/ad;->d()Lo/T;

    move-result-object v10

    invoke-virtual {v4}, Lo/ad;->g()I

    move-result v11

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    invoke-static {v5, v9, v0}, Lm/o;->a(Lm/v;LE/e;I)V

    invoke-static {v5, v8, v10, v11}, Lm/o;->a(Lm/v;LE/q;Lo/T;I)V

    const/4 v0, 0x1

    :cond_2
    if-eqz v2, :cond_3

    mul-int v2, v7, v0

    invoke-static {v5, v9, v2}, Lm/o;->a(Lm/v;LE/e;I)V

    sget-object v2, Lcom/google/android/maps/driveabout/vector/G;->a:Lo/T;

    invoke-virtual {v10, v2}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v2

    invoke-static {v5, v8, v2, v11}, Lm/o;->a(Lm/v;LE/q;Lo/T;I)V

    add-int/lit8 v0, v0, 0x1

    :cond_3
    if-eqz v1, :cond_4

    mul-int/2addr v0, v7

    invoke-static {v5, v9, v0}, Lm/o;->a(Lm/v;LE/e;I)V

    sget-object v0, Lcom/google/android/maps/driveabout/vector/G;->b:Lo/T;

    invoke-virtual {v10, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v5, v8, v0, v11}, Lm/o;->a(Lm/v;LE/q;Lo/T;I)V

    :cond_4
    iput-object v8, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    iput-object v9, p0, Lcom/google/android/maps/driveabout/vector/G;->o:LE/d;

    new-instance v0, Lo/ad;

    invoke-virtual {v4}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {v4}, Lo/ad;->e()Lo/T;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->p:Lo/ad;

    invoke-virtual {p2}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/G;->j:F

    iput-byte v6, p0, Lcom/google/android/maps/driveabout/vector/G;->k:B

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v1, 0x0

    move v2, v1

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method private b(LC/a;)Z
    .locals 3

    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    return-object v0
.end method

.method private c(LC/a;)Z
    .locals 3

    const/high16 v2, 0x40000000

    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    mul-float/2addr v1, v2

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LC/a;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v6, 0x3fa00000

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/G;->m:Z

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/G;->m:Z

    monitor-exit v2

    :goto_0
    return v1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, LC/a;->o()F

    move-result v2

    iget-byte v3, p0, Lcom/google/android/maps/driveabout/vector/G;->k:B

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/G;->j:F

    mul-float/2addr v3, v6

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/G;->j:F

    div-float/2addr v3, v6

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static e(LC/a;)Lo/ad;
    .locals 5

    const/high16 v4, 0x20000000

    invoke-virtual {p0}, LC/a;->B()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->b()Lo/ad;

    move-result-object v2

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v0

    invoke-virtual {v2}, Lo/ad;->h()I

    move-result v1

    const v3, 0x71c71c7

    if-gt v0, v3, :cond_0

    if-le v1, v3, :cond_1

    :cond_0
    new-instance v1, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    sub-int/2addr v0, v4

    const/high16 v3, -0x20000000

    invoke-direct {v1, v0, v3}, Lo/T;-><init>(II)V

    new-instance v0, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    add-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    const v3, 0x1fffffff

    invoke-direct {v0, v2, v3}, Lo/T;-><init>(II)V

    :goto_0
    new-instance v2, Lo/ad;

    invoke-direct {v2, v1, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v2

    :cond_1
    new-instance v3, Lo/T;

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v3, v0, v1}, Lo/T;-><init>(II)V

    invoke-virtual {v2}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/G;->q:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f(LC/a;)V
    .locals 4

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {v2, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private g(LC/a;)V
    .locals 6

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->b(I)I

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    int-to-float v5, v3

    invoke-virtual {v0, v5}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    return-void

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private h()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/maps/driveabout/vector/I;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/vector/I;-><init>(Lcom/google/android/maps/driveabout/vector/G;Lcom/google/android/maps/driveabout/vector/H;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/G;->v:Z

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->o:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    :cond_0
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 3

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/G;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    if-nez v1, :cond_0

    invoke-direct {p0, p2, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(LC/a;Z)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/G;->d(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/G;->b(LD/a;LC/a;)V

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/G;->a(LD/a;LC/a;)V

    :cond_2
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_3
    invoke-virtual {p1}, LD/a;->B()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {p1}, LD/a;->A()V

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    invoke-virtual {p1}, LD/a;->B()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p1}, LD/a;->A()V

    :cond_5
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(LC/a;Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/z;->b(LC/a;LD/a;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->a(LD/a;)V

    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
