.class public Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;
.super Landroid/view/SurfaceView;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final a:Lcom/google/android/maps/driveabout/vector/Z;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Lcom/google/android/maps/driveabout/vector/Y;

.field private d:Lcom/google/android/maps/driveabout/vector/ac;

.field private e:Z

.field private f:Lcom/google/android/maps/driveabout/vector/U;

.field private g:Lcom/google/android/maps/driveabout/vector/V;

.field private h:Lcom/google/android/maps/driveabout/vector/W;

.field private i:Lcom/google/android/maps/driveabout/vector/aa;

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/Z;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/Z;-><init>(Lcom/google/android/maps/driveabout/vector/P;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->a:Lcom/google/android/maps/driveabout/vector/Z;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->k:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)Lcom/google/android/maps/driveabout/vector/U;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f:Lcom/google/android/maps/driveabout/vector/U;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)Lcom/google/android/maps/driveabout/vector/V;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->g:Lcom/google/android/maps/driveabout/vector/V;

    return-object v0
.end method

.method private c()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)Lcom/google/android/maps/driveabout/vector/W;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->h:Lcom/google/android/maps/driveabout/vector/W;

    return-object v0
.end method

.method static synthetic e()Lcom/google/android/maps/driveabout/vector/Z;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->a:Lcom/google/android/maps/driveabout/vector/Z;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)Lcom/google/android/maps/driveabout/vector/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->i:Lcom/google/android/maps/driveabout/vector/aa;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->j:I

    return v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->l:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;)Lcom/google/android/maps/driveabout/vector/ac;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->d:Lcom/google/android/maps/driveabout/vector/ac;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->f()V

    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public n_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->g()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->d:Lcom/google/android/maps/driveabout/vector/ac;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->b()I

    move-result v0

    :goto_0
    new-instance v2, Lcom/google/android/maps/driveabout/vector/Y;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lcom/google/android/maps/driveabout/vector/Y;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/Y;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->e:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->h()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->e:Z

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    return-void
.end method

.method public q_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->c()V

    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->j:I

    return-void
.end method

.method public setEGLConfigChooser(IIIIII)V
    .locals 8

    new-instance v0, Lcom/google/android/maps/driveabout/vector/R;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/R;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;IIIIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V

    return-void
.end method

.method public setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f:Lcom/google/android/maps/driveabout/vector/U;

    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ad;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/vector/ad;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V

    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f()V

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->k:I

    return-void
.end method

.method public setEGLContextFactory(Lcom/google/android/maps/driveabout/vector/V;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->g:Lcom/google/android/maps/driveabout/vector/V;

    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lcom/google/android/maps/driveabout/vector/W;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->h:Lcom/google/android/maps/driveabout/vector/W;

    return-void
.end method

.method public setGLWrapper(Lcom/google/android/maps/driveabout/vector/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->i:Lcom/google/android/maps/driveabout/vector/aa;

    return-void
.end method

.method public setKeepEglContextOnDetach(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->m:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->h()V

    :cond_0
    return-void
.end method

.method public setPreserveEGLContextOnPause(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->l:Z

    return-void
.end method

.method public setRenderMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/Y;->a(I)V

    return-void
.end method

.method public setRenderer(Lcom/google/android/maps/driveabout/vector/ac;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f:Lcom/google/android/maps/driveabout/vector/U;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ad;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/vector/ad;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->f:Lcom/google/android/maps/driveabout/vector/U;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->g:Lcom/google/android/maps/driveabout/vector/V;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/vector/S;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;Lcom/google/android/maps/driveabout/vector/P;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->g:Lcom/google/android/maps/driveabout/vector/V;

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->h:Lcom/google/android/maps/driveabout/vector/W;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/T;

    invoke-direct {v0, v2}, Lcom/google/android/maps/driveabout/vector/T;-><init>(Lcom/google/android/maps/driveabout/vector/P;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->h:Lcom/google/android/maps/driveabout/vector/W;

    :cond_2
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->d:Lcom/google/android/maps/driveabout/vector/ac;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/Y;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/Y;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->start()V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/Y;->a(II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->d()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->c:Lcom/google/android/maps/driveabout/vector/Y;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/Y;->e()V

    return-void
.end method
