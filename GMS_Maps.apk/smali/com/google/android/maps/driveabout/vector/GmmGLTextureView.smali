.class public Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;
.super Landroid/view/TextureView;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field private static final a:Lcom/google/android/maps/driveabout/vector/an;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Lcom/google/android/maps/driveabout/vector/am;

.field private d:Lcom/google/android/maps/driveabout/vector/aK;

.field private e:Z

.field private f:Lcom/google/android/maps/driveabout/vector/U;

.field private g:Lcom/google/android/maps/driveabout/vector/aj;

.field private h:Lcom/google/android/maps/driveabout/vector/ak;

.field private i:Lcom/google/android/maps/driveabout/vector/aa;

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/an;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/an;-><init>(Lcom/google/android/maps/driveabout/vector/ae;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->a:Lcom/google/android/maps/driveabout/vector/an;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->b()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->k:I

    return v0
.end method

.method static synthetic a()Lcom/google/android/maps/driveabout/vector/an;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->a:Lcom/google/android/maps/driveabout/vector/an;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)Lcom/google/android/maps/driveabout/vector/U;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->f:Lcom/google/android/maps/driveabout/vector/U;

    return-object v0
.end method

.method private b()V
    .locals 0

    invoke-virtual {p0, p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)Lcom/google/android/maps/driveabout/vector/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->g:Lcom/google/android/maps/driveabout/vector/aj;

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)Lcom/google/android/maps/driveabout/vector/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->h:Lcom/google/android/maps/driveabout/vector/ak;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)Lcom/google/android/maps/driveabout/vector/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->i:Lcom/google/android/maps/driveabout/vector/aa;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->j:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->l:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;)Lcom/google/android/maps/driveabout/vector/aK;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    return-object v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->b()I

    move-result v0

    :goto_0
    new-instance v2, Lcom/google/android/maps/driveabout/vector/am;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lcom/google/android/maps/driveabout/vector/am;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/am;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->e:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->e()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->e:Z

    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->c()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/maps/driveabout/vector/am;->a(II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->d()V

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/maps/driveabout/vector/am;->a(II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->j:I

    return-void
.end method

.method public setEGLConfigChooser(IIIIII)V
    .locals 8

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ag;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/ag;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;IIIIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V

    return-void
.end method

.method public setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->f:Lcom/google/android/maps/driveabout/vector/U;

    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ap;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/vector/ap;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V

    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c()V

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->k:I

    return-void
.end method

.method public setEGLContextFactory(Lcom/google/android/maps/driveabout/vector/aj;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->g:Lcom/google/android/maps/driveabout/vector/aj;

    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lcom/google/android/maps/driveabout/vector/ak;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->h:Lcom/google/android/maps/driveabout/vector/ak;

    return-void
.end method

.method public setGLWrapper(Lcom/google/android/maps/driveabout/vector/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->i:Lcom/google/android/maps/driveabout/vector/aa;

    return-void
.end method

.method public setKeepEglContextOnDetach(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->m:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->e()V

    :cond_0
    return-void
.end method

.method public setPreserveEGLContextOnPause(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->l:Z

    return-void
.end method

.method public setRenderMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/am;->a(I)V

    return-void
.end method

.method public setRenderer(Lcom/google/android/maps/driveabout/vector/aK;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->f:Lcom/google/android/maps/driveabout/vector/U;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ap;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/vector/ap;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->f:Lcom/google/android/maps/driveabout/vector/U;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->g:Lcom/google/android/maps/driveabout/vector/aj;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ah;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/vector/ah;-><init>(Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;Lcom/google/android/maps/driveabout/vector/ae;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->g:Lcom/google/android/maps/driveabout/vector/aj;

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->h:Lcom/google/android/maps/driveabout/vector/ak;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ai;

    invoke-direct {v0, v2}, Lcom/google/android/maps/driveabout/vector/ai;-><init>(Lcom/google/android/maps/driveabout/vector/ae;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->h:Lcom/google/android/maps/driveabout/vector/ak;

    :cond_2
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/am;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/am;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/GmmGLTextureView;->c:Lcom/google/android/maps/driveabout/vector/am;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/am;->start()V

    return-void
.end method
