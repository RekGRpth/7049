.class public Lcom/google/android/maps/driveabout/vector/M;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/maps/driveabout/vector/E;

.field private b:[I


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    sget v0, Lcom/google/android/maps/driveabout/vector/q;->g:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/M;->b:[I

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/M;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/M;->b:[I

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/q;->a()I

    move-result v1

    aget v0, v0, v1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    if-gtz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/high16 v2, -0x40800000

    invoke-interface {v1, v3, v3, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-static {v1, v0}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v0, p1, LD/a;->h:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/M;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/M;->b:[I

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/q;->a()I

    move-result v1

    aput p2, v0, v1

    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/M;->b:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/M;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
