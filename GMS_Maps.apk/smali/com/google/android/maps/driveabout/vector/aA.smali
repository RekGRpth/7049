.class public Lcom/google/android/maps/driveabout/vector/aA;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;
.implements Lcom/google/android/maps/driveabout/vector/c;


# static fields
.field private static G:LE/a;

.field private static final f:F


# instance fields
.field private A:F

.field private B:Lcom/google/android/maps/driveabout/vector/E;

.field private final C:I

.field private D:F

.field private E:Z

.field private F:Lcom/google/android/maps/driveabout/vector/aB;

.field protected volatile d:Lcom/google/android/maps/driveabout/vector/aU;

.field public e:Ljava/util/List;

.field private g:F

.field private h:F

.field private i:F

.field private final j:Landroid/content/res/Resources;

.field private final k:Z

.field private l:I

.field private final m:Ljava/util/Map;

.field private n:Lh/l;

.field private o:Lcom/google/android/maps/driveabout/vector/a;

.field private final p:Lo/S;

.field private final q:Lo/S;

.field private final r:Lo/S;

.field private s:Z

.field private t:F

.field private u:F

.field private v:I

.field private w:Z

.field private volatile x:Lo/D;

.field private volatile y:Z

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x4

    const/high16 v0, 0x40000000

    sput v0, Lcom/google/android/maps/driveabout/vector/aA;->f:F

    new-instance v0, LE/a;

    invoke-direct {v0, v2}, LE/a;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aA;->G:LE/a;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aA;->G:LE/a;

    const v1, 0x73217bce

    invoke-virtual {v0, v1, v2}, LE/a;->b(II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/x;Z)V
    .locals 6

    const/high16 v5, 0x3f000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->j:Landroid/content/res/Resources;

    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/vector/aA;->k:Z

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    const v2, 0x7f02016e

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    const v2, 0x7f02016b

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a([Lcom/google/android/maps/driveabout/vector/aC;)V

    const/high16 v0, 0x42800000

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    iput v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->z:F

    iput v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->A:F

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(F)V

    const/16 v0, 0x4000

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->d(I)V

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->t:Lcom/google/android/maps/driveabout/vector/E;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->B:Lcom/google/android/maps/driveabout/vector/E;

    const/high16 v0, 0x41800000

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    const/high16 v0, 0x41400000

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    const/high16 v0, 0x3f400000

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->j:Landroid/content/res/Resources;

    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->C:I

    new-instance v0, Lh/m;

    invoke-direct {v0}, Lh/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    return-void
.end method

.method private declared-synchronized a(LD/a;I)LD/b;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    if-nez v0, :cond_0

    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->j:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, LD/b;->a(Landroid/content/res/Resources;I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ZZZ)Lcom/google/android/maps/driveabout/vector/aC;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aC;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aC;->a(ZZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(LC/a;)F
    .locals 4

    const/high16 v0, 0x3f800000

    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0
.end method

.method private declared-synchronized b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 11

    const/4 v7, 0x0

    const/high16 v10, 0x3f800000

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v0}, Lo/S;->c()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "MyLocation"

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/a;-><init>(Lo/T;IIILo/r;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    sget v1, Lcom/google/android/maps/driveabout/vector/aA;->f:F

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(F)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v1}, Lo/S;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v2}, Lo/S;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/T;I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->w()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget v2, v0, Lcom/google/android/maps/driveabout/vector/aC;->f:I

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->b(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->g:I

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/a;->c(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/a;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_1
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v1}, Lo/S;->a()Lo/T;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, LC/a;->a(Lo/T;Z)F

    move-result v1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    :goto_0
    invoke-virtual {p2, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    mul-float/2addr v0, v1

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v2}, Lo/S;->a()Lo/T;

    move-result-object v2

    invoke-static {p1, p2, v2, v0}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    invoke-virtual {p1}, LD/a;->p()V

    iget-object v0, p1, LD/a;->h:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p1, LD/a;->d:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->w()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->E:Z

    if-nez v0, :cond_7

    iget v0, v2, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    if-eq v0, v3, :cond_7

    const/4 v0, 0x1

    :goto_1
    iget-boolean v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    :cond_2
    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->d:I

    if-eqz v3, :cond_3

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->d:I

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v3

    invoke-virtual {v3, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->h()F

    move-result v3

    const/high16 v4, 0x41200000

    mul-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v4}, Lo/S;->h()F

    move-result v4

    const/high16 v5, 0x40400000

    mul-float/2addr v4, v5

    sub-float v4, v10, v4

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v5}, Lo/S;->h()F

    move-result v5

    const/high16 v6, 0x40800000

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v6}, Lo/S;->h()F

    move-result v6

    const/high16 v7, -0x3f800000

    mul-float/2addr v6, v7

    invoke-interface {v1, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    const/4 v4, 0x0

    invoke-interface {v1, v5, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-interface {v1, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/4 v4, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-interface {v1, v4, v7, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    div-float v4, v10, v3

    div-float v7, v10, v3

    div-float v3, v10, v3

    invoke-interface {v1, v4, v7, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    neg-float v3, v5

    neg-float v4, v6

    const/4 v5, 0x0

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    :cond_3
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->h()F

    move-result v3

    cmpl-float v3, v3, v9

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->h()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v4}, Lo/S;->a()Lo/T;

    move-result-object v4

    invoke-virtual {v4}, Lo/T;->e()D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_4
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->e()Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->e:I

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v3, 0x40000000

    const/high16 v4, 0x40000000

    const/high16 v5, 0x40000000

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->e:I

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v3

    invoke-virtual {v3, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    const/high16 v3, 0x3f000000

    const/high16 v4, 0x3f000000

    const/high16 v5, 0x3f000000

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->b()F

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :goto_2
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v0}, Lo/S;->j()F

    move-result v0

    cmpl-float v0, v0, v10

    if-nez v0, :cond_9

    iget v0, v2, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    :goto_3
    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_6
    :try_start_1
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    goto/16 :goto_0

    :cond_7
    move v0, v7

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p2}, LC/a;->p()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p2}, LC/a;->q()F

    move-result v3

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    :try_start_2
    iget v0, v2, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private o()V
    .locals 0

    return-void
.end method

.method private declared-synchronized q()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    invoke-virtual {v0}, LD/b;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private r()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->F:Lcom/google/android/maps/driveabout/vector/aB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->F:Lcom/google/android/maps/driveabout/vector/aB;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    int-to-float v3, v3

    const/high16 v4, 0x47800000

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aB;->a(FFF)V

    :cond_0
    return-void
.end method

.method private s()F
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    div-float/2addr v0, v1

    :goto_0
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    mul-float/2addr v1, v0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    :goto_1
    mul-float/2addr v1, v0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->A:F

    :goto_2
    mul-float/2addr v0, v1

    return v0

    :cond_0
    const/high16 v0, 0x3e800000

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->z:F

    goto :goto_2
.end method

.method private t()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->w()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    return v0
.end method

.method private declared-synchronized u()Lo/S;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/S;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->a(Lo/S;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private v()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->d:Lcom/google/android/maps/driveabout/vector/aU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->d:Lcom/google/android/maps/driveabout/vector/aU;

    invoke-interface {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/aU;->a(ZZ)V

    :cond_0
    return-void
.end method

.method private w()Lcom/google/android/maps/driveabout/vector/aC;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->u()Lo/S;

    move-result-object v0

    invoke-virtual {v0}, Lo/S;->e()Z

    move-result v1

    invoke-virtual {v0}, Lo/S;->g()Z

    move-result v2

    invoke-virtual {v0}, Lo/S;->i()Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/aA;->a(ZZZ)Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(FFLo/T;LC/a;)I
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;

    move-result-object v0

    invoke-virtual {p4, v0}, LC/a;->b(Lo/T;)[I

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    aget v1, v0, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    mul-float/2addr v1, v2

    aget v2, v0, v3

    int-to-float v2, v2

    sub-float v2, p2, v2

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(F)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->r()V

    return-void
.end method

.method public a(FFF)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    return-void
.end method

.method public a(FII)V
    .locals 3

    const/high16 v2, 0x42c80000

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v1, p1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    int-to-float v1, p2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->z:F

    int-to-float v1, p3

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->A:F

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(F)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->r()V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    invoke-virtual {p1}, LD/a;->d()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lh/l;->a(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    :goto_1
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->a(Lo/S;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/S;)V

    invoke-virtual {p1}, LD/a;->d()J

    move-result-wide v0

    const-wide/16 v2, 0xc8

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, LD/a;->a(J)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-nez v0, :cond_4

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/S;)V

    goto :goto_2

    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    if-eqz v2, :cond_5

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    invoke-virtual {v2}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/aH;->a(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v2}, Lo/S;->a()Lo/T;

    move-result-object v2

    invoke-interface {v0, p1, p2, p3, v2}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V

    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aA;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    if-eqz v0, :cond_6

    invoke-interface {v0, p1, p3}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_6
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 0

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aA;->d:Lcom/google/android/maps/driveabout/vector/aU;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->B:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method public a(Ljava/util/List;FFLo/T;LC/a;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aA;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/aA;->a(FFLo/T;LC/a;)I

    move-result v0

    if-ge v0, p6, :cond_0

    new-instance v1, Lcom/google/android/maps/driveabout/vector/t;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/maps/driveabout/vector/t;-><init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lo/S;)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    invoke-virtual {p1}, Lo/S;->l()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->f()Lo/D;

    move-result-object v0

    invoke-virtual {p1}, Lo/S;->f()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->g()Z

    move-result v0

    invoke-virtual {p1}, Lo/S;->g()Z

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0, p1}, Lo/S;->a(Lo/S;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->b(Lo/S;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->o()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->v()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    new-instance v0, Lh/m;

    invoke-direct {v0}, Lh/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public varargs declared-synchronized a([Lcom/google/android/maps/driveabout/vector/aC;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->a([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->e:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LC/a;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aA;->e()Lo/T;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->s()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    aget v4, v2, v1

    sub-int/2addr v4, v3

    aget v5, v2, v1

    add-int/2addr v5, v3

    aget v6, v2, v0

    sub-int/2addr v6, v3

    aget v2, v2, v0

    add-int/2addr v2, v3

    invoke-virtual {p1}, LC/a;->k()I

    move-result v3

    if-ge v4, v3, :cond_1

    if-ltz v5, :cond_1

    invoke-virtual {p1}, LC/a;->l()I

    move-result v3

    if-ge v6, v3, :cond_1

    if-ltz v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a_(LC/a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    return v0
.end method

.method public declared-synchronized b(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LC/a;LD/a;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aA;->b(LC/a;)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    invoke-interface {v0, p1}, Lh/l;->a(LC/a;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/util/List;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aA;->f()Lo/D;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    if-eqz v3, :cond_0

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    invoke-virtual {v3}, Lo/D;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v0, v3}, Ln/s;->e(Lo/r;)Ln/k;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/d;->b(Ljava/util/List;)Z

    move-result v0

    :goto_0
    return v0

    :cond_2
    sget-object v3, Lcom/google/android/maps/driveabout/vector/aI;->i:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v4, v1, [Lcom/google/android/maps/driveabout/vector/aJ;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public c()LC/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)V
    .locals 1

    const v0, 0x3f2b851f

    packed-switch p1, :pswitch_data_0

    const/high16 v0, 0x3f800000

    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(F)V

    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public c(LD/a;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->q()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->w:Z

    return-void
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->r()V

    return-void
.end method

.method public declared-synchronized e()Lo/T;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->a(Lo/S;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Lo/D;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->u()Lo/S;

    move-result-object v0

    invoke-virtual {v0}, Lo/S;->f()Lo/D;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->w:Z

    return-void
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h_()I
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->C:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->k:Z

    return v0
.end method

.method public declared-synchronized n()Lo/S;
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Lo/S;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->u()Lo/S;

    move-result-object v1

    invoke-direct {v0, v1}, Lo/S;-><init>(Lo/S;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->B:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .locals 0

    return-object p0
.end method
