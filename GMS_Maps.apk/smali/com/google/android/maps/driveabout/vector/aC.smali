.class public Lcom/google/android/maps/driveabout/vector/aC;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;

.field private final j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aC;->h:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aC;->i:Ljava/lang/Boolean;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aC;->j:Ljava/lang/Boolean;

    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    iput p5, p0, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    iput p6, p0, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    iput p7, p0, Lcom/google/android/maps/driveabout/vector/aC;->d:I

    iput p8, p0, Lcom/google/android/maps/driveabout/vector/aC;->e:I

    iput p9, p0, Lcom/google/android/maps/driveabout/vector/aC;->f:I

    iput p10, p0, Lcom/google/android/maps/driveabout/vector/aC;->g:I

    return-void
.end method

.method public static a()Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aD;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aD;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(ZZZ)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p2, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p3, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
