.class public Lcom/google/android/maps/driveabout/vector/aD;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Z

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x73217bce

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->i:I

    const v0, 0x338cc6ef

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->j:I

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->h:Z

    return-object p0
.end method

.method public a(I)Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    invoke-virtual {p0, p1, p1}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->d:Ljava/lang/Integer;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->e:Ljava/lang/Integer;

    return-object p0
.end method

.method public a(Z)Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->a:Ljava/lang/Boolean;

    return-object p0
.end method

.method public b()Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->h:Z

    return-object p0
.end method

.method public b(II)Lcom/google/android/maps/driveabout/vector/aD;
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aD;->i:I

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/aD;->j:I

    return-object p0
.end method

.method public b(Z)Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->b:Ljava/lang/Boolean;

    return-object p0
.end method

.method public c()Lcom/google/android/maps/driveabout/vector/aC;
    .locals 11

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Texture ID must be specified."

    invoke-static {v0, v1}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aC;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aD;->a:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aD;->b:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aD;->c:Ljava/lang/Boolean;

    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aD;->h:Z

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aD;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aD;->e:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/aD;->f:Ljava/lang/Integer;

    if-nez v7, :cond_1

    move v7, v8

    :goto_1
    iget-object v9, p0, Lcom/google/android/maps/driveabout/vector/aD;->g:Ljava/lang/Integer;

    if-nez v9, :cond_2

    :goto_2
    iget v9, p0, Lcom/google/android/maps/driveabout/vector/aD;->i:I

    iget v10, p0, Lcom/google/android/maps/driveabout/vector/aD;->j:I

    invoke-direct/range {v0 .. v10}, Lcom/google/android/maps/driveabout/vector/aC;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIIII)V

    return-object v0

    :cond_0
    move v0, v8

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/aD;->f:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aD;->g:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_2
.end method

.method public c(Z)Lcom/google/android/maps/driveabout/vector/aD;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->c:Ljava/lang/Boolean;

    return-object p0
.end method
