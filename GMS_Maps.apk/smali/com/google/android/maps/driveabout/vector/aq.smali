.class public Lcom/google/android/maps/driveabout/vector/aq;
.super Lcom/google/android/maps/driveabout/vector/aZ;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;
.implements Ln/f;
.implements Ln/r;
.implements Ln/u;


# instance fields
.field private volatile d:Z

.field private volatile e:Z

.field private final f:Lr/n;

.field private final g:Ln/s;

.field private final h:Ln/n;

.field private i:Lk/a;

.field private final j:Lcom/google/android/maps/driveabout/vector/as;

.field private final k:Ljava/util/Set;

.field private volatile l:Ljava/util/Set;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Ljava/util/List;

.field private final p:Lcom/google/android/maps/driveabout/vector/aJ;


# direct methods
.method protected constructor <init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;ILandroid/content/Context;Ln/s;)V
    .locals 20

    sget-object v5, LA/c;->n:LA/c;

    new-instance v7, Lcom/google/android/maps/driveabout/vector/au;

    move-object/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, p8

    move-object/from16 v3, p9

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/au;-><init>(Lg/c;ILandroid/content/Context;Ln/s;)V

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p7

    invoke-direct/range {v4 .. v19}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->o:Ljava/util/List;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/ar;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/maps/driveabout/vector/ar;-><init>(Lcom/google/android/maps/driveabout/vector/aq;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->p:Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->f:Lr/n;

    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    new-instance v4, Ln/n;

    invoke-direct {v4}, Ln/n;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/as;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/E;->d:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v6, 0x12c

    invoke-direct {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/as;-><init>(Lcom/google/android/maps/driveabout/vector/E;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    const v5, -0x7fafafb0

    invoke-virtual {v4, v5}, Lcom/google/android/maps/driveabout/vector/as;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aq;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lo/r;)Ly/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->c:Ly/b;

    invoke-static {v0, p1}, Ly/a;->a(Ly/b;Ljava/lang/Object;)Ly/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/y;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln/s;->a(Lo/r;)Lo/D;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Ln/s;->c(Lo/r;)Lo/D;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v2, v1, v0}, Ln/s;->b(Lo/D;Lo/D;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v2}, Lk/a;->c()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v2, v1, v0}, Ln/s;->a(Lo/D;Lo/D;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v2, v0, v5, v5, v5}, Ln/s;->a(Lo/r;ZZZ)Ln/k;

    move-result-object v2

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v3, v4, v5, v5, v0}, Ln/s;->a(Lo/r;ZZZ)Ln/k;

    move-result-object v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    invoke-virtual {v0}, Ln/k;->e()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ln/k;->b()F

    move-result v0

    invoke-virtual {v3}, Ln/k;->b()F

    move-result v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Ln/k;->a(I)V

    const/16 v0, 0x18

    invoke-virtual {v3, v0}, Ln/k;->a(I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lk/a;->a(Lo/r;Lo/D;)V

    goto/16 :goto_0

    :cond_3
    const/16 v0, 0x14

    :try_start_2
    invoke-virtual {v2, v0}, Ln/k;->a(I)V

    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ln/k;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private c(LC/a;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, LC/a;->s()F

    move-result v0

    const/high16 v1, 0x41880000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v1

    sget-object v3, Lo/av;->c:Lo/av;

    invoke-virtual {v1, v3}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v1

    check-cast v1, Lo/E;

    invoke-virtual {v1}, Lo/E;->b()Lo/r;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly/b;

    if-nez v1, :cond_1

    invoke-direct {p0, v3}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/r;)Ly/b;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v0

    invoke-virtual {v3, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v0, Ln/n;->a:Ln/l;

    if-eq v3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/k;

    if-nez v0, :cond_2

    new-instance v0, Lo/k;

    const/4 v4, 0x1

    new-array v4, v4, [Lo/j;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-direct {v0, v4}, Lo/k;-><init>([Lo/j;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lo/k;->a(Lo/j;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private d(LC/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LC/a;->s()F

    move-result v0

    const v1, 0x416e6666

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->c(LC/a;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v1, v0}, Ln/s;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->a(LC/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-virtual {v2, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ln/s;->a(Ljava/util/Set;)V

    goto :goto_0
.end method

.method private e(LC/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1}, LC/a;->s()F

    move-result v1

    const/high16 v2, 0x41880000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->e(LC/a;)Lo/r;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v1, v0}, Ln/s;->d(Lo/r;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0}, Ln/s;->c()Lo/y;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lo/z;->f()I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/as;->b(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v1, v0}, Ln/s;->b(Lo/y;)Lo/z;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lo/aQ;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;)I
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0}, Ln/s;->g()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    if-eqz v0, :cond_0

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v1

    sget-object v4, Lo/av;->c:Lo/av;

    invoke-virtual {v1, v4}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v1

    check-cast v1, Lo/E;

    invoke-virtual {v1}, Lo/E;->b()Lo/r;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p1, :cond_1

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v4

    invoke-virtual {v4}, Lo/aq;->i()Lo/ad;

    move-result-object v4

    invoke-virtual {p1, v4}, Lo/aQ;->b(Lo/ae;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/r;)Ly/b;

    move-result-object v1

    invoke-interface {v0, v1}, LF/T;->a(Ly/b;)V

    invoke-interface {v0, p2}, LF/T;->a(Lcom/google/android/maps/driveabout/vector/aF;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lo/T;)Lo/D;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0}, Lk/a;->d()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-virtual {v3, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3, p1}, Ln/l;->a(Lo/T;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v1, Lo/av;->c:Lo/av;

    invoke-virtual {v0, v1}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v0

    check-cast v0, Lo/E;

    invoke-virtual {v0}, Lo/E;->c()Lo/D;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 2

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->d:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/as;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/as;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, LC/a;->s()F

    move-result v0

    const v1, 0x416e6666

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->b:Z

    goto :goto_0
.end method

.method protected a(Lg/a;)V
    .locals 1

    move-object v0, p1

    check-cast v0, Lk/a;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lg/a;)V

    return-void
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0}, Ln/s;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/r;)Ly/b;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public a(Ln/s;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aq;->t()V

    invoke-virtual {p1}, Ln/s;->c()Lo/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lo/y;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lo/A;->a:Lcom/google/common/base/x;

    invoke-static {v0, v1}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    goto :goto_0
.end method

.method public a(Ln/s;Lo/y;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aq;->t()V

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/y;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    return-void
.end method

.method public a(Lo/aq;Ln/l;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    return-void
.end method

.method a(Ljava/util/List;)Z
    .locals 10

    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    if-eqz v0, :cond_8

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/as;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->d:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v5

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v1}, Ln/s;->i()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    invoke-virtual {v0}, Ln/k;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->h:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v7, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    :goto_1
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ln/k;->b()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_4

    invoke-virtual {v0}, Ln/k;->d()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ln/k;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-virtual {v0}, Ln/k;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->f:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aq;->p:Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-static {v8}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->c:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aq;->p:Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-static {v8}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Ln/k;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_1

    invoke-virtual {v0}, Ln/k;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ln/k;->g()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->g:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v7, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->b:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v1, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    new-instance v6, Lcom/google/android/maps/driveabout/vector/at;

    invoke-direct {v6, v4}, Lcom/google/android/maps/driveabout/vector/at;-><init>(Ljava/util/Set;)V

    aput-object v6, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v1, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/at;

    invoke-direct {v4, v5}, Lcom/google/android/maps/driveabout/vector/at;-><init>(Ljava/util/Set;)V

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    move v0, v2

    :goto_2
    return v0

    :cond_8
    move v0, v3

    goto :goto_2
.end method

.method public a_(LC/a;)I
    .locals 5

    const/4 v1, 0x2

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/as;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    monitor-exit v2

    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    invoke-virtual {v0}, Ln/k;->f()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v1}, Lk/a;->c()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v1}, Ln/s;->b()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    :cond_4
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected b(LC/a;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->b(LC/a;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ln/s;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->e(LC/a;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->d(LC/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, LC/a;->s()F

    move-result v1

    const v2, 0x416e6666

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;LD/a;)Z

    move-result v0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->c(LC/a;)V

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    goto :goto_0
.end method

.method public b(Ljava/util/List;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public c()LC/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c(LD/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->c(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0, v1}, Ln/s;->d(Lo/r;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0, v1}, Ln/s;->a(Ljava/util/Set;)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0, p0}, Ln/s;->a(Ln/u;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-virtual {v0, p0}, Ln/n;->a(Ln/r;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/s;

    invoke-virtual {v0}, Ln/s;->j()Ln/e;

    move-result-object v0

    invoke-interface {v0, p0}, Ln/e;->a(Ln/f;)V

    return-void
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0}, Lk/a;->b()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->a:Lcom/google/android/maps/driveabout/vector/aU;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aU;->a(ZZ)V

    goto :goto_0
.end method

.method public i()Ln/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .locals 0

    return-object p0
.end method
