.class Lcom/google/android/maps/driveabout/vector/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/aJ;


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/at;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/aJ;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/at;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V
    .locals 5

    const/16 v4, 0x80

    const/16 v3, 0x1e01

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, LD/a;->w()V

    invoke-interface {v0, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    const/16 v1, 0x207

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const v1, -0x9f9fa0

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    :cond_0
    return-void
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 2

    invoke-interface {p2}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, LD/a;->x()V

    :cond_0
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/at;->a(Lcom/google/android/maps/driveabout/vector/aJ;)I

    move-result v0

    return v0
.end method
