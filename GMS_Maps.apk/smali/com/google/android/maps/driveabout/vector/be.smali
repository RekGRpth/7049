.class public Lcom/google/android/maps/driveabout/vector/be;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(LD/a;LC/a;Lo/T;F)V
    .locals 2

    iget-object v0, p0, LD/a;->l:[F

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F[F)V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, LD/a;->l:[F

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    return-void
.end method

.method private static a(LD/a;LC/a;Lo/T;FZ[F)V
    .locals 4

    if-nez p0, :cond_1

    new-instance v1, Lo/T;

    invoke-direct {v1}, Lo/T;-><init>()V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    :goto_0
    invoke-virtual {p1, v1}, LC/a;->a(Lo/T;)V

    invoke-static {p2, v1, v0}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    if-eqz p4, :cond_0

    invoke-virtual {v0, v0}, Lo/T;->i(Lo/T;)V

    :cond_0
    invoke-virtual {p1}, LC/a;->w()F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    aput v3, p5, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    aput v3, p5, v2

    const/4 v2, 0x2

    invoke-virtual {v0}, Lo/T;->h()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    aput v0, p5, v2

    const/4 v0, 0x3

    mul-float/2addr v1, p3

    aput v1, p5, v0

    return-void

    :cond_1
    iget-object v1, p0, LD/a;->m:Lo/T;

    iget-object v0, p0, LD/a;->n:Lo/T;

    goto :goto_0
.end method

.method public static a(LD/a;LC/a;Lo/T;F[F)V
    .locals 6

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;FZ[F)V

    return-void
.end method

.method public static a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    neg-float v0, v0

    invoke-interface {p0, v0, v2, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    const/high16 v1, 0x42b40000

    sub-float/2addr v0, v1

    invoke-interface {p0, v0, v3, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    return-void
.end method

.method public static a(Ljavax/microedition/khronos/opengles/GL10;[F)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    aget v0, p1, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-interface {p0, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    aget v0, p1, v3

    aget v1, p1, v3

    aget v2, p1, v3

    invoke-interface {p0, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    return-void
.end method

.method public static b(LD/a;LC/a;Lo/T;F)V
    .locals 2

    iget-object v0, p0, LD/a;->l:[F

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/be;->b(LD/a;LC/a;Lo/T;F[F)V

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, LD/a;->l:[F

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    return-void
.end method

.method public static b(LD/a;LC/a;Lo/T;F[F)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;FZ[F)V

    return-void
.end method
