.class Lcom/google/android/maps/driveabout/vector/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bi;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/bi;-><init>(Lcom/google/android/maps/driveabout/vector/bh;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bh;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bj;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/bj;-><init>(Lcom/google/android/maps/driveabout/vector/bh;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bh;->b:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/bg;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/bh;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Law/g;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bh;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public b(Law/g;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bh;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public k()V
    .locals 0

    return-void
.end method
