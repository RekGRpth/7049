.class public Lcom/google/android/maps/driveabout/vector/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;


# static fields
.field private static final b:F

.field private static final c:LC/b;

.field private static d:F


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private e:Lcom/google/android/maps/driveabout/vector/bm;

.field private final f:Z

.field private final g:Lw/b;

.field private volatile h:LC/b;

.field private volatile i:LC/b;

.field private volatile j:F

.field private volatile k:LC/c;

.field private volatile l:Z

.field private m:Z

.field private n:Lg/a;

.field private o:Lcom/google/android/maps/driveabout/vector/aU;

.field private p:Lcom/google/android/maps/driveabout/vector/bB;

.field private q:Lcom/google/android/maps/driveabout/vector/bq;

.field private r:Lcom/google/android/maps/driveabout/vector/aE;

.field private s:Z

.field private t:I

.field private u:F

.field private final v:Ln/s;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/vector/bk;->b:F

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    const/high16 v0, 0x41a80000

    sput v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->u:F

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->a:Landroid/content/res/Resources;

    sget-object v0, LC/a;->d:LC/b;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    sget-object v0, LC/a;->d:LC/b;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    :goto_0
    new-instance v2, Lw/b;

    int-to-float v0, v0

    invoke-direct {v2, v0}, Lw/b;-><init>(F)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->f:Z

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->v:Ln/s;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private a(F)F
    .locals 8

    const-wide/high16 v0, 0x4000000000000000L

    const-wide v2, 0x40031eb851eb851fL

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget v6, Lcom/google/android/maps/driveabout/vector/bk;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404d5ae147ae147bL

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(LC/a;Lw/b;Lo/T;F)LC/b;
    .locals 6

    invoke-virtual {p0}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v1

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v0

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v2

    sub-int/2addr v0, v2

    float-to-double v2, p3

    const-wide v4, 0x400921fb54442d18L

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L

    div-double/2addr v2, v4

    double-to-float v2, v2

    neg-float v3, v2

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    neg-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    int-to-float v4, v1

    mul-float/2addr v4, v2

    int-to-float v5, v0

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    int-to-float v1, v1

    mul-float/2addr v1, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    new-instance v1, Lo/T;

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Lo/T;-><init>(II)V

    invoke-virtual {p0}, LC/a;->p()F

    move-result v0

    add-float/2addr v0, p3

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f(F)F

    move-result v4

    new-instance v0, LC/b;

    invoke-virtual {p0}, LC/a;->r()F

    move-result v2

    invoke-virtual {p0}, LC/a;->q()F

    move-result v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {p1, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(LC/b;LC/a;FF)LC/b;
    .locals 6

    invoke-virtual {p1}, LC/a;->y()F

    move-result v0

    mul-float/2addr v0, p2

    neg-float v1, p3

    invoke-virtual {p1}, LC/a;->y()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1}, LC/a;->q()F

    move-result v2

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, LC/a;->u()Lo/T;

    move-result-object v2

    invoke-virtual {p1}, LC/a;->v()Lo/T;

    move-result-object v3

    new-instance v4, Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    invoke-direct {v4, v5, v2}, Lo/T;-><init>(II)V

    new-instance v5, Lo/T;

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v2

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-direct {v5, v2, v3}, Lo/T;-><init>(II)V

    invoke-static {v4, v0, v4}, Lo/T;->b(Lo/T;FLo/T;)V

    invoke-static {v5, v1, v5}, Lo/T;->b(Lo/T;FLo/T;)V

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-virtual {p0}, LC/b;->a()F

    move-result v2

    invoke-virtual {v0}, Lo/T;->h()I

    move-result v3

    invoke-virtual {v0, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    invoke-static {v1, v5, v1}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    invoke-virtual {v1, v3}, Lo/T;->b(I)V

    new-instance v0, LC/b;

    invoke-virtual {p0}, LC/b;->d()F

    move-result v3

    invoke-virtual {p0}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method public static a(LC/b;LC/a;Lw/b;FFF)LC/b;
    .locals 8

    const/high16 v1, 0x40000000

    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v6, p4, v0

    invoke-virtual {p1}, LC/a;->l()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v7, p5, v0

    invoke-static {p0, p1, v6, v7}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v0

    invoke-virtual {p2, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v4

    new-instance v0, LC/b;

    invoke-virtual {v4}, LC/b;->c()Lo/T;

    move-result-object v1

    sget v2, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    invoke-virtual {v4}, LC/b;->a()F

    move-result v3

    add-float/2addr v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {p1, v0}, LC/a;->a(LC/b;)V

    neg-float v1, v6

    neg-float v2, v7

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v0

    return-object v0
.end method

.method private a(LC/c;)V
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    instance-of v0, p1, Lw/d;

    if-eqz v0, :cond_0

    check-cast p1, Lw/d;

    invoke-interface {p1}, Lw/d;->a()V

    :cond_0
    return-void
.end method

.method private a(LC/c;LC/b;)V
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    instance-of v0, p1, Lw/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->l:Z

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->m:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0, p2}, Lw/b;->a(LC/b;)LC/b;

    move-result-object p2

    :cond_1
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bk;->i:LC/b;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->o:Lcom/google/android/maps/driveabout/vector/aU;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->o:Lcom/google/android/maps/driveabout/vector/aU;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aU;->a(ZZ)V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static b(F)V
    .locals 0

    sput p0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return-void
.end method

.method public static d()F
    .locals 1

    sget v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return v0
.end method

.method static synthetic e(F)F
    .locals 1

    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/bk;->f(F)F

    move-result v0

    return v0
.end method

.method private static f(F)F
    .locals 7

    const-wide v5, 0x4076800000000000L

    move v0, p0

    :goto_0
    float-to-double v1, v0

    cmpl-double v1, v1, v5

    if-ltz v1, :cond_0

    float-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_0

    :cond_0
    :goto_1
    float-to-double v1, v0

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    float-to-double v0, v0

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_1

    :cond_1
    return v0
.end method

.method static synthetic j()F
    .locals 1

    sget v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return v0
.end method


# virtual methods
.method public declared-synchronized a(FFF)F
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bp;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bp;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bp;-><init>(LC/b;Lw/b;)V

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(FFFI)F
    .locals 7

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-virtual {v1}, LC/b;->a()F

    move-result v0

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bo;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bo;-><init>(LC/b;Lw/b;FFFI)V

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    invoke-virtual {v1}, LC/b;->a()F

    move-result v0

    add-float/2addr v0, p1

    sget v1, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public a(FI)F
    .locals 7

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v5}, LC/b;->a()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v5}, LC/b;->d()F

    move-result v3

    invoke-virtual {v5}, LC/b;->e()F

    move-result v4

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {v6, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    return v0
.end method

.method public a(Lo/T;)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0}, Lw/b;->a()Lw/c;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0, p1}, Lw/c;->a(Lo/T;)F

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized a(FF)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bp;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bp;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bp;-><init>(LC/b;Lw/b;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LC/c;I)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(LC/c;II)V
    .locals 11

    const/4 v10, -0x1

    const/high16 v9, 0x442f0000

    const/high16 v8, 0x41000000

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    if-eqz v1, :cond_1

    move p3, v0

    move p2, v0

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-interface {p1}, LC/c;->b()LC/b;

    move-result-object v2

    invoke-virtual {v2, v1}, LC/b;->a(LC/b;)LC/b;

    move-result-object v6

    invoke-virtual {v6}, LC/b;->a()F

    move-result v2

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v6}, LC/b;->a()F

    move-result v3

    invoke-virtual {v1}, LC/b;->a()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v2}, LR/m;->y()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, LR/m;->z()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_3

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, LC/b;->a()F

    move-result v2

    invoke-virtual {v1}, LC/b;->a()F

    move-result v4

    add-float/2addr v2, v4

    const/high16 v4, 0x3f000000

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/16 v4, 0x1e

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/high16 v4, 0x40000000

    shr-int v2, v4, v2

    invoke-virtual {v6}, LC/b;->c()Lo/T;

    move-result-object v4

    invoke-virtual {v1}, LC/b;->c()Lo/T;

    move-result-object v5

    invoke-virtual {v4, v5}, Lo/T;->c(Lo/T;)F

    move-result v5

    int-to-float v4, v2

    div-float v7, v5, v4

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    int-to-float v4, v4

    cmpg-float v4, v7, v4

    if-gtz v4, :cond_4

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_6

    if-nez p2, :cond_5

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;)V

    goto :goto_0

    :cond_4
    move v4, v0

    goto :goto_1

    :cond_5
    if-ne p2, v10, :cond_9

    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v8

    const/high16 v2, 0x3f400000

    mul-float/2addr v0, v2

    const/high16 v2, 0x3fc00000

    add-float/2addr v0, v2

    mul-float/2addr v0, v9

    float-to-int v3, v0

    :goto_2
    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bl;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bl;-><init>(LC/b;LC/c;IZF)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    goto/16 :goto_0

    :cond_6
    if-nez p3, :cond_7

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;)V

    goto/16 :goto_0

    :cond_7
    if-ne p3, v10, :cond_8

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    int-to-float v0, v0

    sub-float v0, v7, v0

    const/high16 v3, 0x4e800000

    int-to-float v2, v2

    div-float v2, v3, v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    const v2, 0x40833333

    mul-float/2addr v0, v2

    const v2, 0x3fb33333

    add-float/2addr v0, v2

    mul-float/2addr v0, v9

    float-to-int v0, v0

    const/16 v2, 0x9c4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->u:F

    mul-float/2addr v0, v2

    float-to-int v3, v0

    :goto_3
    invoke-direct {p0, v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(F)F

    move-result v5

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bl;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bl;-><init>(LC/b;LC/c;IZF)V

    invoke-direct {p0, v0, v6}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    goto/16 :goto_0

    :cond_8
    move v3, p3

    goto :goto_3

    :cond_9
    move v3, p2

    goto :goto_2
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/aE;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->r:Lcom/google/android/maps/driveabout/vector/aE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->o:Lcom/google/android/maps/driveabout/vector/aU;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bB;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->p:Lcom/google/android/maps/driveabout/vector/bB;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bm;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->e:Lcom/google/android/maps/driveabout/vector/bm;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bq;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->q:Lcom/google/android/maps/driveabout/vector/bq;

    return-void
.end method

.method public a(Lo/D;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->v:Ln/s;

    invoke-virtual {v0, p1}, Ln/s;->a(Lo/D;)V

    return-void
.end method

.method public a(Lo/T;I)V
    .locals 6

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    new-instance v0, LC/b;

    invoke-virtual {v1}, LC/b;->a()F

    move-result v2

    invoke-virtual {v1}, LC/b;->d()F

    move-result v3

    invoke-virtual {v1}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    return-void
.end method

.method public a(Lo/ad;IIFI)V
    .locals 6

    const/high16 v4, 0x43800000

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    mul-float/2addr v0, p4

    int-to-float v2, p2

    div-float/2addr v0, v2

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    mul-float/2addr v2, p4

    int-to-float v4, p3

    div-float/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v2, 0x41f00000

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v0, v4

    sget v4, Lcom/google/android/maps/driveabout/vector/bk;->b:F

    mul-float/2addr v0, v4

    sub-float/2addr v2, v0

    new-instance v0, LC/b;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {p0, v0, p5}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    goto :goto_0
.end method

.method public a(Lw/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0, p1}, Lw/b;->a(Lw/c;)V

    return-void
.end method

.method protected a(ZZZII)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->e:Lcom/google/android/maps/driveabout/vector/bm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->e:Lcom/google/android/maps/driveabout/vector/bm;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bm;->a(ZZZII)V

    :cond_0
    return-void
.end method

.method public a_(LC/a;)I
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v1, v1, Lw/d;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lw/d;

    invoke-interface {v0}, Lw/d;->c()I

    move-result v1

    invoke-interface {v0, p1}, Lw/d;->a(LC/a;)LC/c;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v3}, LC/c;->b()LC/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    invoke-interface {v0}, Lw/d;->c()I

    move-result v0

    or-int/2addr v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-virtual {p1, v1}, LC/a;->a(LC/b;)V

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->m:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->r:Lcom/google/android/maps/driveabout/vector/aE;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->n:Lg/a;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/maps/driveabout/vector/bs;

    new-instance v2, LA/a;

    invoke-direct {v2}, LA/a;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/vector/bs;-><init>(LA/b;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->n:Lg/a;

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->n:Lg/a;

    invoke-interface {v1, p1}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->r:Lcom/google/android/maps/driveabout/vector/aE;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-interface {v2, v3, v1}, Lcom/google/android/maps/driveabout/vector/aE;->a(LC/b;Ljava/util/List;)V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->m:Z

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->p:Lcom/google/android/maps/driveabout/vector/bB;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->p:Lcom/google/android/maps/driveabout/vector/bB;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/bB;->a(LC/a;)V

    :cond_3
    return v0

    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v2}, LC/c;->b()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->l:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized b(FFF)F
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bp;

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bp;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bp;-><init>(LC/b;Lw/b;)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FF)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bn;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bn;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bn;-><init>(LC/b;)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bn;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/bn;->a(FF)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    float-to-int v4, p1

    float-to-int v5, p2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(ZZZII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FI)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/br;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/br;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/maps/driveabout/vector/br;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/br;-><init>(LC/b;Lw/b;)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/br;->a(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-virtual {v5}, LC/b;->d()F

    move-result v0

    add-float v3, v0, p1

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v5}, LC/b;->a()F

    move-result v2

    invoke-virtual {v5}, LC/b;->e()F

    move-result v4

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {v6, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public c()LC/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->i:LC/b;

    return-object v0
.end method

.method public c(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0, p1}, Lw/b;->a(F)V

    return-void
.end method

.method public d(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->u:F

    return-void
.end method

.method public e()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0}, Lw/b;->a()Lw/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/high16 v0, 0x40000000

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lw/c;->a()F

    move-result v0

    goto :goto_0
.end method

.method public f()LC/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    return-object v0
.end method

.method public g()F
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F

    return v0
.end method

.method public declared-synchronized h()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->q:Lcom/google/android/maps/driveabout/vector/bq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->q:Lcom/google/android/maps/driveabout/vector/bq;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bq;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v2

    new-instance v0, LC/b;

    invoke-virtual {v2}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v2}, LC/b;->a()F

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    const/16 v1, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->l:Z

    return v0
.end method
