.class Lcom/google/android/maps/driveabout/vector/bo;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:I

.field private final f:J

.field private final g:Lw/b;

.field private h:Lw/d;


# direct methods
.method protected constructor <init>(LC/b;Lw/b;FFFI)V
    .locals 2

    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bo;->g:Lw/b;

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/bo;->b:F

    iput p4, p0, Lcom/google/android/maps/driveabout/vector/bo;->c:F

    iput p5, p0, Lcom/google/android/maps/driveabout/vector/bo;->d:F

    iput p6, p0, Lcom/google/android/maps/driveabout/vector/bo;->e:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->f:J

    return-void
.end method


# virtual methods
.method public a(LC/a;)LC/c;
    .locals 7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->a:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bo;->g:Lw/b;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bo;->b:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bo;->c:F

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/bo;->d:F

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;Lw/b;FFF)LC/b;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/google/android/maps/driveabout/vector/bo;->f:J

    sub-long/2addr v0, v3

    long-to-int v3, v0

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bl;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bo;->a:LC/b;

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/bo;->e:I

    sub-int v3, v6, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bl;-><init>(LC/b;LC/c;IZF)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    invoke-interface {v0, p1}, Lw/d;->a(LC/a;)LC/c;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    invoke-interface {v0}, Lw/d;->c()I

    move-result v0

    goto :goto_0
.end method
