.class Lcom/google/android/maps/driveabout/vector/bp;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private volatile h:Z

.field private final i:Lw/b;

.field private final j:[F


# direct methods
.method public constructor <init>(LC/b;Lw/b;)V
    .locals 1

    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bp;->i:Lw/b;

    return-void
.end method


# virtual methods
.method public a(LC/a;)LC/c;
    .locals 11

    monitor-enter p0

    :try_start_0
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bp;->f:F

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/bp;->g:F

    iget v7, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    :goto_0
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    mul-float/2addr v1, v2

    const v2, -0x42333333

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v6, v0

    :goto_1
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    sub-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->h:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    monitor-exit p0

    move-object p0, v0

    :cond_0
    :goto_2
    return-object p0

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    mul-float/2addr v1, v2

    const v2, 0x3dcccccd

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v6, v0

    goto :goto_1

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_8

    :cond_4
    const/4 v0, 0x1

    move v2, v0

    :goto_3
    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    :goto_4
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-static {v2, p1, v7, v8}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    if-nez v1, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {p1, v2}, LC/a;->a(LC/b;)V

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {p1, v4, v5}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->i:Lw/b;

    invoke-static {p1, v2, v1, v6}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/a;Lw/b;Lo/T;F)LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {p1, v1}, LC/a;->a(LC/b;)V

    :cond_7
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->i:Lw/b;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;Lw/b;FFF)LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_8
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_4

    :cond_a
    const/4 v0, 0x0

    goto :goto_5
.end method

.method declared-synchronized a(FFFFFF)[F
    .locals 4

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    add-float/2addr v0, p5

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    add-float/2addr v0, p6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    cmpl-float v0, p1, v1

    if-nez v0, :cond_0

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_1

    :cond_0
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/bp;->f:F

    iput p4, p0, Lcom/google/android/maps/driveabout/vector/bp;->g:F

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->h:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {v2}, LC/b;->a()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    add-float/2addr v2, v3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bk;->j()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, 0x40000000

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {v2}, LC/b;->e()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    add-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/maps/driveabout/vector/bk;->e(F)F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
