.class public Lcom/google/android/maps/driveabout/vector/bt;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SourceFile"

# interfaces
.implements LaD/m;


# static fields
.field private static final c:F

.field private static final d:F

.field private static final e:D


# instance fields
.field private final a:F

.field private final b:F

.field private final f:Lcom/google/android/maps/driveabout/vector/bv;

.field private g:Lcom/google/android/maps/driveabout/vector/bw;

.field private h:Lcom/google/android/maps/driveabout/vector/bw;

.field private i:Landroid/view/MotionEvent;

.field private j:F

.field private k:F

.field private l:Lcom/google/android/maps/driveabout/vector/bu;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-boolean v0, Lcom/google/googlenav/android/E;->g:Z

    if-eqz v0, :cond_0

    const v0, 0x3f7f3b64

    :goto_0
    sput v0, Lcom/google/android/maps/driveabout/vector/bt;->c:F

    const/high16 v0, 0x3f800000

    sget v1, Lcom/google/android/maps/driveabout/vector/bt;->c:F

    div-float/2addr v0, v1

    sput v0, Lcom/google/android/maps/driveabout/vector/bt;->d:F

    const-wide/high16 v0, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/vector/bt;->e:D

    return-void

    :cond_0
    const v0, 0x3f7fbe77

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/bv;)V
    .locals 2

    const/16 v1, 0x14

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    sget-object v0, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bw;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/bw;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->b:F

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->a:F

    return-void
.end method

.method private static b(LaD/q;)Z
    .locals 2

    invoke-virtual {p0}, LaD/q;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaD/q;->c()F

    move-result v0

    sget v1, Lcom/google/android/maps/driveabout/vector/bt;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p0}, LaD/q;->c()F

    move-result v0

    sget v1, Lcom/google/android/maps/driveabout/vector/bt;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bw;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bw;-><init>(Lcom/google/android/maps/driveabout/vector/bw;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->h:Lcom/google/android/maps/driveabout/vector/bw;

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x63

    invoke-static {v0, p1}, Lbm/m;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->a:Z

    return-void
.end method

.method public a(LaD/o;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, LaD/o;->a(FF)V

    invoke-virtual {p1}, LaD/o;->c()F

    move-result v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    invoke-virtual {p1}, LaD/o;->a()F

    move-result v1

    invoke-virtual {p1}, LaD/o;->b()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/bk;->b(FFF)F

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaD/q;)Z
    .locals 5

    const/high16 v4, 0x40000000

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaD/q;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    const/high16 v1, -0x40800000

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(FI)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-virtual {p1}, LaD/q;->c()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/google/android/maps/driveabout/vector/bt;->e:D

    div-double/2addr v0, v2

    double-to-float v0, v0

    invoke-virtual {p1}, LaD/q;->a()F

    move-result v1

    invoke-virtual {p1}, LaD/q;->b()F

    move-result v2

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/bt;->b(LaD/q;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    :cond_2
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(FFF)F

    move-result v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    goto :goto_0
.end method

.method public a(LaD/u;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v1, v1, Lcom/google/android/maps/driveabout/vector/bw;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v1

    invoke-virtual {p1}, LaD/u;->a()F

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/bk;->b(FI)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->h:Lcom/google/android/maps/driveabout/vector/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->h:Lcom/google/android/maps/driveabout/vector/bw;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->b:Z

    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->c:Z

    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->d:Z

    return-void
.end method

.method public e(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->e:Z

    return-void
.end method

.method public f(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->f:Z

    return-void
.end method

.method public g(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->g:Z

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    sget-object v0, Lcom/google/android/maps/driveabout/vector/bu;->b:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->d(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/16 v5, 0x14a

    const/4 v0, 0x0

    const/high16 v4, 0x40000000

    const/high16 v6, 0x3f000000

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v1, v1, Lcom/google/android/maps/driveabout/vector/bw;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bu;->b:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v1, v0, v4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    :goto_0
    const/high16 v2, 0x3f800000

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v4, v4, Lcom/google/android/maps/driveabout/vector/bw;->c:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v4

    invoke-virtual {v4, v2, v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(FI)F

    move-result v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v4, v2, v1, v0}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    move v0, v3

    :cond_0
    :goto_2
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v4

    invoke-virtual {v4, v2, v1, v0, v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(FFFI)F

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    :cond_4
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    sub-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    sub-float/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/bu;->b:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v4, v5, :cond_5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/bt;->b:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->b:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->a:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_7

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bu;->c:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    const-string v2, "d"

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/vector/bt;->a(Ljava/lang/String;)V

    :cond_5
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/bu;->c:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v2, v4, :cond_8

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x40c00000

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(FI)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-interface {v1, v0, v2, v4}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    :cond_6
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    move v0, v3

    goto/16 :goto_2

    :cond_7
    move v0, v3

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bu;->d:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    invoke-static {v0, v1, v2, v4}, LaD/j;->a(FFFF)F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v0, v1, v4, v5}, LaD/j;->a(FFFF)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v5}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v5

    sub-float v2, v4, v2

    const/high16 v4, 0x43340000

    mul-float/2addr v2, v4

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L

    div-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v5, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->b(FFF)F

    goto :goto_3
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->a(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/vector/bt;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/bk;->b(FF)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->e(FF)V

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/bv;->a(Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/bk;->a(FF)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/bv;->f(FF)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->c(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->b(FF)Z

    move-result v0

    return v0
.end method
