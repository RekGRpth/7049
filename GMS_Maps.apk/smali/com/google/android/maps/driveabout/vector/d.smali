.class public abstract Lcom/google/android/maps/driveabout/vector/d;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/maps/driveabout/vector/x;

.field protected b:Z

.field protected c:I

.field private d:Lcom/google/android/maps/driveabout/vector/e;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/x;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/d;->a:Lcom/google/android/maps/driveabout/vector/x;

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/d;->g()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/d;->d:Lcom/google/android/maps/driveabout/vector/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/d;->d:Lcom/google/android/maps/driveabout/vector/e;

    invoke-interface {v0, p0, p1}, Lcom/google/android/maps/driveabout/vector/e;->a(Lcom/google/android/maps/driveabout/vector/d;Lcom/google/android/maps/driveabout/vector/c;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/d;->d:Lcom/google/android/maps/driveabout/vector/e;

    return-void
.end method

.method public abstract a(Ljava/util/List;FFLo/T;LC/a;I)V
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/d;->c:I

    return-void
.end method

.method public l()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/d;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/d;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public l_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/d;->b:Z

    return v0
.end method
