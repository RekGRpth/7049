.class public Lcom/google/android/maps/driveabout/vector/i;
.super Lcom/google/android/maps/driveabout/vector/aZ;
.source "SourceFile"


# static fields
.field private static final d:Lo/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, LaN/B;

    const v1, 0x243d580

    const v2, 0x81b3200

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    invoke-static {v0}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v0

    new-instance v1, LaN/B;

    const v2, 0x1f78a40

    const v3, 0x88601c0

    invoke-direct {v1, v2, v3}, LaN/B;-><init>(II)V

    invoke-static {v1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    invoke-static {v0, v1}, Lo/ad;->a(Lo/T;Lo/T;)Lo/ad;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/i;->d:Lo/ad;

    return-void
.end method

.method constructor <init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V
    .locals 0

    invoke-direct/range {p0 .. p15}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-void
.end method

.method static a(Ln/s;FLo/T;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ln/s;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ln/s;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/maps/driveabout/vector/i;->d:Lo/ad;

    invoke-virtual {v2, p2}, Lo/ad;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/high16 v2, 0x41900000

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 3

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    invoke-virtual {p2}, LC/a;->s()F

    move-result v1

    invoke-virtual {p2}, LC/a;->h()Lo/T;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/i;->a(Ln/s;FLo/T;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/i;->b:Z

    goto :goto_0
.end method

.method protected v_()Lcom/google/android/maps/driveabout/vector/aI;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->c:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method
