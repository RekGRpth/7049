.class Lcom/google/android/maps/driveabout/vector/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/b;


# instance fields
.field private final a:Lz/K;

.field private final b:Lz/i;

.field private final c:Lz/h;

.field private d:Z

.field private e:Z

.field private final f:I

.field private g:Lz/c;

.field private final h:Lh/h;

.field private i:F

.field private volatile j:Z


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lz/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/l;->g:Lz/c;

    sget-object v0, Lz/k;->a:Lz/P;

    invoke-interface {p1, p0, v0}, Lz/c;->a(Lz/b;Lz/O;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized b(Lz/c;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->b:Lz/i;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/l;->a:Lz/K;

    invoke-virtual {v0, v1}, Lz/i;->a(Lz/K;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->d:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    invoke-virtual {v0}, Lh/h;->hasEnded()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->j:Z

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->j:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lh/h;->a(J)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->c:Lz/h;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/l;->f:I

    invoke-interface {v0, v1}, Lz/h;->a(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    invoke-virtual {v0}, Lh/h;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->g:Lz/c;

    sget-object v1, Lz/k;->a:Lz/P;

    invoke-interface {v0, p0, v1}, Lz/c;->a(Lz/b;Lz/O;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->c:Lz/h;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    invoke-interface {v0, v1, v2, v3, v4}, Lz/h;->a(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
