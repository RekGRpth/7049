.class public Lcom/google/android/maps/driveabout/vector/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/c;

.field private final b:Lcom/google/android/maps/driveabout/vector/d;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/t;->a:Lcom/google/android/maps/driveabout/vector/c;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    return v0
.end method

.method public a(FFLo/T;LC/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/t;->a:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/c;->a(FFLo/T;LC/a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/d;->l()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p4}, LC/a;->m()F

    move-result v0

    const/high16 v1, 0x40a00000

    mul-float/2addr v0, v1

    mul-float/2addr v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/d;->l()I

    move-result v2

    float-to-int v0, v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/t;->d:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/t;->d:Z

    return v0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/t;->a:Lcom/google/android/maps/driveabout/vector/c;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/d;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    return-void
.end method
