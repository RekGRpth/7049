.class public Lcom/google/android/maps/driveabout/vector/x;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/E;

.field private final b:[F

.field private c:Lcom/google/android/maps/driveabout/vector/c;

.field private d:Lo/T;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/maps/driveabout/vector/f;

.field private g:Landroid/graphics/Bitmap;

.field private h:LD/b;

.field private i:Lcom/google/android/maps/driveabout/vector/y;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:F

.field private final o:I

.field private final p:Lh/e;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->A:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {p0, v0, p1}, Lcom/google/android/maps/driveabout/vector/x;-><init>(Lcom/google/android/maps/driveabout/vector/E;Landroid/content/res/Resources;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/E;Landroid/content/res/Resources;)V
    .locals 8

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    new-instance v0, Lh/e;

    const-wide/16 v1, 0xbb8

    const-wide/16 v3, 0x2710

    sget-object v5, Lh/g;->c:Lh/g;

    const/high16 v6, 0x10000

    const v7, 0x8000

    invoke-direct/range {v0 .. v7}, Lh/e;-><init>(JJLh/g;II)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/x;->a:Lcom/google/android/maps/driveabout/vector/E;

    if-nez p2, :cond_0

    const v0, 0xffff00

    :goto_0
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->o:I

    return-void

    :cond_0
    const v0, 0x7f090093

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V
    .locals 3

    invoke-virtual {p3, p2}, LE/o;->d(LD/a;)V

    invoke-virtual {p4, p2}, LE/i;->d(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    invoke-virtual {v0, p1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void
.end method

.method private c(FFLC/a;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v2

    invoke-virtual {p3, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    aget v4, v2, v1

    int-to-float v4, v4

    aget v5, v2, v0

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/maps/driveabout/vector/f;->a(FF)V

    aget v3, v2, v1

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    add-int/2addr v4, v3

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_3

    int-to-float v3, v4

    cmpl-float v3, p1, v3

    if-lez v3, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    sub-int v3, v2, v3

    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-ltz v3, :cond_5

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private h()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->b()V

    :cond_2
    return-void
.end method

.method private i()Landroid/graphics/Bitmap;
    .locals 6

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/x;->j:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->k:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, LD/b;->c(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    const/16 v2, 0x20

    invoke-static {v1, v2}, LD/b;->c(II)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    sub-int v5, v1, v5

    int-to-float v4, v4

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/maps/driveabout/vector/f;->d(FF)V

    return-object v2
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 8

    const/high16 v7, 0x10000

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    :cond_0
    monitor-exit p0

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/c;->f()Lo/D;

    move-result-object v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v2

    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v2, v1}, Ln/s;->e(Lo/r;)Ln/k;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    invoke-virtual {v2, v0}, Lo/T;->b(Lo/T;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    invoke-virtual {v1, p2, v0}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Lo/T;->b(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    :cond_3
    invoke-virtual {p2}, LC/a;->B()Lo/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lo/aS;->a()Lo/aR;

    move-result-object v2

    if-nez v0, :cond_4

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    invoke-virtual {v2, v0}, Lo/aR;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-nez v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v1, p2}, Lcom/google/android/maps/driveabout/vector/c;->a(LC/a;)Z

    move-result v1

    if-nez v1, :cond_6

    monitor-exit p0

    goto :goto_0

    :cond_6
    invoke-virtual {p2}, LC/a;->j()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    invoke-virtual {p2, v0, v1}, LC/a;->a(Lo/T;[F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    :cond_7
    if-nez v0, :cond_8

    const-string v0, "UI"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Null point for ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "camera="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    monitor-exit p0

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {p1}, LD/a;->p()V

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    if-nez v2, :cond_9

    new-instance v2, LD/b;

    invoke-direct {v2, p1}, LD/b;-><init>(LD/a;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, LD/b;->b(Landroid/graphics/Bitmap;)V

    :cond_9
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/x;->n:F

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    invoke-static {v1, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    invoke-virtual {p2}, LC/a;->t()Lo/T;

    move-result-object v2

    invoke-virtual {p2}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v0

    invoke-virtual {p2}, LC/a;->j()Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v0, 0x0

    :goto_1
    neg-float v4, v2

    const/high16 v5, 0x3f000000

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v5}, Lcom/google/android/maps/driveabout/vector/c;->h()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v6}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v0, v6

    invoke-interface {v1, v4, v5, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v0, 0x3f800000

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/x;->o:I

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v0, p1, LD/a;->g:LE/o;

    iget-object v2, p1, LD/a;->d:LE/i;

    invoke-direct {p0, v1, p1, v0, v2}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V

    :cond_a
    :goto_2
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->f()V

    goto/16 :goto_0

    :cond_b
    :try_start_2
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    sub-float v4, v5, v4

    mul-float/2addr v0, v4

    goto :goto_1

    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->a()Lcom/google/android/maps/driveabout/vector/g;

    move-result-object v0

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aF()Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    invoke-virtual {v2, p1}, Lh/e;->a(LD/a;)I

    move-result v2

    if-ge v2, v7, :cond_d

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    invoke-interface {v1, v3, v4, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :cond_d
    iget-object v2, p1, LD/a;->g:LE/o;

    iget-object v3, p1, LD/a;->d:LE/i;

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V

    if-eqz v0, :cond_a

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/x;->o:I

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/g;->a:LE/o;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/g;->b:LE/i;

    invoke-direct {p0, v1, p1, v2, v0}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    invoke-virtual {v0}, Lh/e;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/f;->e()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->g_()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->d()V

    :cond_3
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->g_()V

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/f;->e()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/y;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(FFLC/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    invoke-virtual {v0}, Lh/e;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/f;->c(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(FFLo/T;LC/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/f;->b(FF)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->g()V

    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->g()V

    :cond_1
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(FFLo/T;LC/a;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/y;->b(Lcom/google/android/maps/driveabout/vector/c;)V

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_1
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized b(LC/a;LD/a;)Z
    .locals 2

    monitor-enter p0

    const/high16 v0, 0x3f800000

    :try_start_0
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    invoke-virtual {p1, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->n:F

    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->j:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, LC/a;->l()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->k:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->j:I

    invoke-virtual {p1}, LC/a;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->k:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(LD/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(FFLo/T;LC/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->k_()V

    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized e()Lcom/google/android/maps/driveabout/vector/c;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->d()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected g()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/D;->g()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/y;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized k_()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
