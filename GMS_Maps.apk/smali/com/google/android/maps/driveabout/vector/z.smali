.class public Lcom/google/android/maps/driveabout/vector/z;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final a:Lo/T;

.field private static final b:Lo/T;


# instance fields
.field private final c:Lo/X;

.field private d:Lo/X;

.field private e:Lo/X;

.field private final f:Ljava/util/List;

.field private final g:Lo/r;

.field private h:Lo/ad;

.field private i:F

.field private j:F

.field private final k:LE/o;

.field private final l:LE/d;

.field private final m:LE/i;

.field private n:F

.field private o:I

.field private p:Z

.field private final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lo/T;

    const/high16 v1, 0x40000000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    new-instance v0, Lo/T;

    const/high16 v1, -0x40000000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    return-void
.end method

.method public constructor <init>(Lo/X;FILo/r;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/z;->c:Lo/X;

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/z;->n:F

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/z;->g:Lo/r;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lx/h;->a(Ljava/util/List;)I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/z;->q:I

    new-instance v1, LE/o;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->q:I

    invoke-direct {v1, v2}, LE/o;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    new-instance v1, LE/i;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->q:I

    invoke-direct {v1, v2}, LE/i;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    new-instance v1, LE/d;

    invoke-static {v0}, Lx/h;->b(Ljava/util/List;)I

    move-result v0

    invoke-direct {v1, v0}, LE/d;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    return-void
.end method

.method static a(Lo/X;)Ljava/util/List;
    .locals 13

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lo/X;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lo/Z;

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    invoke-direct {v2, v0}, Lo/Z;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/Z;->a(Lo/T;)Z

    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v5

    if-ge v0, v5, :cond_3

    invoke-virtual {p0, v0, v4}, Lo/X;->a(ILo/T;)V

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    if-ge v5, v6, :cond_2

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v7

    sub-int/2addr v6, v7

    const/high16 v7, 0x40000000

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_1

    const/high16 v5, 0x20000000

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v7

    sub-int/2addr v5, v7

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v7

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    int-to-double v9, v7

    int-to-double v11, v5

    mul-double/2addr v9, v11

    int-to-double v5, v6

    div-double v5, v9, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    add-int/2addr v5, v8

    new-instance v6, Lo/T;

    const v7, 0x1fffffff

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lo/Z;->c()V

    new-instance v6, Lo/T;

    const/high16 v7, -0x20000000

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    :cond_1
    :goto_2
    invoke-virtual {v2, v4}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v3, v4}, Lo/T;->b(Lo/T;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    if-le v5, v6, :cond_1

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v7

    sub-int/2addr v6, v7

    const/high16 v7, 0x40000000

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_1

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v5

    const/high16 v7, 0x20000000

    add-int/2addr v5, v7

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v7

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    int-to-double v9, v7

    int-to-double v11, v5

    mul-double/2addr v9, v11

    int-to-double v5, v6

    div-double v5, v9, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    add-int/2addr v5, v8

    new-instance v6, Lo/T;

    const/high16 v7, -0x20000000

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lo/Z;->c()V

    new-instance v6, Lo/T;

    const v7, 0x1fffffff

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_4

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(F)Lo/X;
    .locals 1

    const/high16 v0, 0x41200000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->c:Lo/X;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/z;->h()V

    const/high16 v0, 0x40c00000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->e:Lo/X;

    goto :goto_0
.end method

.method private a(LD/a;LC/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0, p1}, LE/o;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    invoke-virtual {v0, p1}, LE/d;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(Lo/X;LC/a;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/z;->j:F

    return-void
.end method

.method private a(LD/a;Lcom/google/android/maps/driveabout/vector/r;LC/a;)V
    .locals 6

    const/4 v3, 0x0

    const/high16 v5, 0x10000

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/z;->e()Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    invoke-interface {v2, p1, p3, p2, v0}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v3}, Lo/ad;->g()I

    move-result v3

    int-to-float v3, v3

    invoke-static {p1, p3, v0, v3}, Lcom/google/android/maps/driveabout/vector/be;->b(LD/a;LC/a;Lo/T;F)V

    invoke-virtual {p1}, LD/a;->p()V

    const/4 v0, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v0, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v1, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/z;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/16 v0, 0x18

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v5, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    if-eqz v2, :cond_1

    invoke-interface {v2, p1, p2}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_1
    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    const v4, 0xff00

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    shr-int/lit8 v0, v0, 0x10

    and-int/2addr v0, v4

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    shr-int/lit8 v1, v1, 0x8

    and-int/2addr v1, v4

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    and-int/2addr v2, v4

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v3, v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lo/X;LC/a;)V
    .locals 11

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v5

    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, LC/a;->y()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/z;->n:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x40e00000

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000

    mul-float v2, v0, v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v0

    const/4 v6, 0x0

    const/high16 v7, 0x10000

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    iget-object v9, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    iget-object v10, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v10}, Lx/h;->a(Lo/X;FFLo/T;IIILE/q;LE/e;LE/k;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(LC/a;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3fa00000

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/z;->p:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/z;->p:Z

    monitor-exit p0

    :goto_0
    return v1

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, LC/a;->o()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/z;->j:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/z;->j:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static b(I)I
    .locals 2

    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method private b(LC/a;)Z
    .locals 4

    const/high16 v3, 0x40000000

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->i:F

    mul-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->i:F

    div-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->c()Lo/ae;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/ad;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LC/a;)V
    .locals 9

    const/high16 v5, 0x20000000

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/z;->a(F)Lo/X;

    move-result-object v4

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->b()Lo/ad;

    move-result-object v2

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v0

    invoke-virtual {v2}, Lo/ad;->h()I

    move-result v1

    const v3, 0x71c71c7

    if-gt v0, v3, :cond_0

    if-le v1, v3, :cond_1

    :cond_0
    new-instance v1, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    sub-int/2addr v0, v5

    const/high16 v3, -0x40000000

    invoke-direct {v1, v0, v3}, Lo/T;-><init>(II)V

    new-instance v0, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    add-int/2addr v2, v5

    add-int/lit8 v2, v2, -0x1

    const v3, 0x3fffffff

    invoke-direct {v0, v2, v3}, Lo/T;-><init>(II)V

    move-object v2, v0

    move-object v3, v1

    :goto_0
    new-instance v0, Lo/ad;

    invoke-direct {v0, v3, v2}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/z;->b(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v4, v0}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/z;->a(Lo/X;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    new-instance v1, Lo/h;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-direct {v1, v6}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v1, v0, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v5}, Ljava/util/List;->clear()V

    new-instance v1, Lo/ad;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    invoke-virtual {v3, v6}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v6

    sget-object v7, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    invoke-virtual {v2, v7}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    new-instance v6, Lo/h;

    invoke-direct {v6, v1}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v6, v0, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/X;

    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    sget-object v8, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    invoke-virtual {v1, v8}, Lo/X;->b(Lo/T;)Lo/X;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    new-instance v3, Lo/T;

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v3, v0, v1}, Lo/T;-><init>(II)V

    invoke-virtual {v2}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    goto/16 :goto_0

    :cond_2
    invoke-interface {v5}, Ljava/util/List;->clear()V

    new-instance v1, Lo/ad;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    invoke-virtual {v3, v6}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v6

    sget-object v7, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    invoke-virtual {v2, v7}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    new-instance v6, Lo/h;

    invoke-direct {v6, v1}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v6, v0, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    sget-object v7, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    invoke-virtual {v0, v7}, Lo/X;->b(Lo/T;)Lo/X;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-interface {v5}, Ljava/util/List;->clear()V

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/z;->i:F

    return-void
.end method

.method private e()Lcom/google/android/maps/driveabout/vector/aJ;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->g:Lo/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->g:Lo/r;

    invoke-virtual {v0, v1}, Ln/s;->e(Lo/r;)Ln/k;

    move-result-object v0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->c:Lo/X;

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/z;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/z;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->e:Lo/X;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/z;->c(LC/a;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-direct {p0, p1, p3, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;LC/a;)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_2
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/z;->b(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/z;->c(LC/a;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/z;->p:Z

    monitor-exit p0

    :cond_0
    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;)V

    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
