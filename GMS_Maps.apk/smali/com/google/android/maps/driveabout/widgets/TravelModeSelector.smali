.class public Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/maps/driveabout/widgets/f;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/google/android/maps/driveabout/app/cX;

.field private d:[Lcom/google/android/maps/driveabout/app/P;

.field private e:Lcom/google/android/maps/driveabout/widgets/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/widgets/b;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/widgets/b;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a:Lcom/google/android/maps/driveabout/widgets/f;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/P;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->d:[Lcom/google/android/maps/driveabout/app/P;

    sget-object v0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a:Lcom/google/android/maps/driveabout/widgets/f;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->e:Lcom/google/android/maps/driveabout/widgets/f;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/maps/driveabout/widgets/c;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/widgets/c;-><init>(Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/P;)Landroid/view/View;
    .locals 3

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040056

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;Landroid/view/View;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/maps/driveabout/app/P;Landroid/view/View;)V
    .locals 2

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f100033

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/P;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x7f100034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/P;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->b()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;Lcom/google/android/maps/driveabout/app/P;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->b(Lcom/google/android/maps/driveabout/app/P;)V

    return-void
.end method

.method private b()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/cX;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->b:Landroid/content/Context;

    const v2, 0x7f10015d

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/google/android/maps/driveabout/widgets/e;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->d:[Lcom/google/android/maps/driveabout/app/P;

    invoke-direct {v3, v4, v5}, Lcom/google/android/maps/driveabout/widgets/e;-><init>(Landroid/content/Context;[Lcom/google/android/maps/driveabout/app/P;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cX;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/ListAdapter;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    new-instance v1, Lcom/google/android/maps/driveabout/widgets/d;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/widgets/d;-><init>(Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cX;->a(Lcom/google/android/maps/driveabout/app/cZ;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cX;->show()V

    return-void
.end method

.method private b(Lcom/google/android/maps/driveabout/app/P;)V
    .locals 1

    invoke-static {p1, p0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->e:Lcom/google/android/maps/driveabout/widgets/f;

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/widgets/f;->a(Lcom/google/android/maps/driveabout/app/P;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cX;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->c:Lcom/google/android/maps/driveabout/app/cX;

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/P;)V
    .locals 0

    invoke-static {p1, p0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;Landroid/view/View;)V

    return-void
.end method

.method public setTravelModeChangedListener(Lcom/google/android/maps/driveabout/widgets/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->e:Lcom/google/android/maps/driveabout/widgets/f;

    return-void
.end method

.method public setVisibleTravelModes([Lcom/google/android/maps/driveabout/app/P;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->d:[Lcom/google/android/maps/driveabout/app/P;

    return-void
.end method
