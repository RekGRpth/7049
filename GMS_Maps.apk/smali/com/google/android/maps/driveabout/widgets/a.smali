.class public Lcom/google/android/maps/driveabout/widgets/a;
.super Landroid/support/v4/view/x;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/bR;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/maps/driveabout/app/M;

.field private c:[Lcom/google/android/maps/driveabout/app/T;

.field private d:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;Lcom/google/android/maps/driveabout/app/M;)V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/support/v4/view/x;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/widgets/a;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/widgets/a;->b:Lcom/google/android/maps/driveabout/app/M;

    new-array v0, v5, [Lcom/google/android/maps/driveabout/app/T;

    const/4 v1, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;LaH/h;Z)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p1, p2, v1, v3}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZ)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {p1, v2}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;Z)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/google/android/maps/driveabout/widgets/a;->c:[Lcom/google/android/maps/driveabout/app/T;

    new-array v0, v5, [Ljava/lang/String;

    const v1, 0x7f0d00d1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f0d00d6

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f0d00cf

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/google/android/maps/driveabout/widgets/a;->d:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(I)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/widgets/a;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    new-instance v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/widgets/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/widgets/a;->c:[Lcom/google/android/maps/driveabout/app/T;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/widgets/a;->b:Lcom/google/android/maps/driveabout/app/M;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    check-cast p1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public a(LaH/h;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/widgets/a;->c:[Lcom/google/android/maps/driveabout/app/T;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lcom/google/android/maps/driveabout/app/bR;->a(LaH/h;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(I)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/widgets/a;->c:[Lcom/google/android/maps/driveabout/app/T;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lcom/google/android/maps/driveabout/app/bR;->a_(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/view/ViewGroup;)V
    .locals 0

    return-void
.end method
