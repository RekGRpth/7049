.class public Lcom/google/android/maps/rideabout/app/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lbi/d;

.field private b:LU/z;

.field private c:Lcom/google/android/maps/rideabout/app/c;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lbi/d;Lcom/google/android/maps/rideabout/app/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/a;->d:Z

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/a;->a:Lbi/d;

    iput-object p2, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    return-void
.end method

.method private b(ZLjava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    invoke-static {p2}, Lcom/google/googlenav/ui/view/android/rideabout/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private b(LU/z;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/a;->c(LU/z;)Lcom/google/android/maps/rideabout/app/c;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->b:LU/A;

    iget-object v2, p1, LU/z;->b:LU/A;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    if-nez v0, :cond_0

    iget-object v0, p1, LU/z;->c:Lbi/a;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    if-eqz v0, :cond_1

    iget-object v0, p1, LU/z;->c:Lbi/a;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    if-eqz v0, :cond_3

    iget-object v0, p1, LU/z;->c:Lbi/a;

    if-eqz v0, :cond_3

    iget-object v0, p1, LU/z;->c:Lbi/a;

    iget-object v0, v0, Lbi/a;->a:Lbi/t;

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v2, v2, LU/z;->c:Lbi/a;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/a;->e:Z

    iput-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LU/z;)Lcom/google/android/maps/rideabout/app/c;
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p1, LU/z;->c:Lbi/a;

    sget-object v1, Lcom/google/android/maps/rideabout/app/b;->b:[I

    iget-object v2, p1, LU/z;->b:LU/A;

    invoke-virtual {v2}, LU/A;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->h:Lcom/google/android/maps/rideabout/app/c;

    :goto_1
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->i:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->h:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->a:Lbi/d;

    iget-object v2, v0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/rideabout/app/b;->a:[I

    invoke-virtual {v1}, Lbi/h;->b()Lbi/q;

    move-result-object v3

    invoke-virtual {v3}, Lbi/q;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->a:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :pswitch_4
    iget v2, v0, Lbi/a;->b:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_0

    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->b:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :cond_0
    :pswitch_5
    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/a;->a:Lbi/d;

    iget-object v3, v0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v2, v3}, Lbi/d;->g(Lbi/t;)Z

    move-result v2

    invoke-virtual {v1}, Lbi/h;->y()F

    move-result v1

    const/high16 v3, 0x3f800000

    iget v4, v0, Lbi/a;->b:F

    sub-float/2addr v3, v4

    mul-float/2addr v1, v3

    if-eqz v2, :cond_1

    iget v0, v0, Lbi/a;->b:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    const/high16 v0, 0x447a0000

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_1

    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->d:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->c:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :pswitch_6
    iget v2, v0, Lbi/a;->b:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_4

    instance-of v2, v1, Lbi/o;

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->a:Lbi/d;

    iget-object v0, v0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v0}, Lbi/d;->c(Lbi/t;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->f:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->a:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :cond_3
    instance-of v0, v1, Lbi/m;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->a:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v4, p0, Lcom/google/android/maps/rideabout/app/a;->d:Z

    :cond_5
    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->e:Lcom/google/android/maps/rideabout/app/c;

    goto :goto_1

    :pswitch_7
    iput-boolean v4, p0, Lcom/google/android/maps/rideabout/app/a;->d:Z

    sget-object v0, Lcom/google/android/maps/rideabout/app/c;->g:Lcom/google/android/maps/rideabout/app/c;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private c(ZLjava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->m()Ljava/lang/String;

    move-result-object v1

    const-string v0, ""

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->n()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method private m()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->p()I

    move-result v1

    const-string v0, ""

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x489

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->o()F

    move-result v1

    float-to-int v1, v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private o()F
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    iget v0, v0, Lbi/a;->b:F

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v1

    invoke-virtual {v1}, Lbi/h;->z()F

    move-result v2

    invoke-virtual {v1}, Lbi/h;->r()F

    move-result v1

    mul-float/2addr v0, v1

    sub-float v0, v2, v0

    return v0
.end method

.method private p()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    iget v0, v0, Lbi/a;->b:F

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v1

    invoke-virtual {v1}, Lbi/h;->A()I

    move-result v2

    invoke-virtual {v1}, Lbi/h;->q()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sub-int v0, v2, v0

    return v0
.end method

.method private static q()J
    .locals 2

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->t:I

    int-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public a(Z)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/rideabout/app/b;->c:[I

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v2}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const/16 v1, 0x4eb

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {v0}, Lbi/h;->w()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    check-cast v0, Lbi/s;

    const/16 v1, 0x48e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v0}, Lbi/s;->G()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lbi/s;->L()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lbi/h;->a()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x4ce

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x4a4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x4a2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    check-cast v0, Lbi/o;

    const/16 v1, 0x4d9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v0}, Lbi/o;->G()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lbi/o;->L()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/rideabout/app/a;->b(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_6
    const/16 v0, 0x48a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_7
    const/16 v0, 0x4ea

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    const/16 v0, 0x4c5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method

.method public a(ZLjava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v3

    sget-object v4, Lcom/google/android/maps/rideabout/app/b;->c:[I

    iget-object v5, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v5}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move-object v0, v2

    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/rideabout/app/a;->c(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_2
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Lbi/h;->a(Ljava/util/Date;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_2

    invoke-static {}, Lcom/google/android/maps/rideabout/app/a;->q()J

    move-result-wide v5

    cmp-long v5, v3, v5

    if-gez v5, :cond_2

    long-to-int v2, v3

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/bd;->a(IZ)Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_1

    invoke-static {v2}, Lcom/google/googlenav/ui/view/android/rideabout/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_1
    const/16 v3, 0x499

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v3, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(LU/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/a;->b(LU/z;)V

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/a;->e:Z

    return v0
.end method

.method public b()Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/maps/rideabout/app/b;->c:[I

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v2}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    iget-boolean v1, p0, Lcom/google/android/maps/rideabout/app/a;->d:Z

    if-eqz v1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public c()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    sget-object v2, Lcom/google/android/maps/rideabout/app/c;->c:Lcom/google/android/maps/rideabout/app/c;

    if-ne v1, v2, :cond_0

    const/16 v1, 0x4b6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    check-cast v0, Lbi/s;

    invoke-virtual {v0}, Lbi/s;->G()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/maps/rideabout/app/a;->a(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    sget-object v1, Lcom/google/android/maps/rideabout/app/c;->c:Lcom/google/android/maps/rideabout/app/c;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->a()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x4b7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "/"

    invoke-direct {p0, v4, v3}, Lcom/google/android/maps/rideabout/app/a;->c(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->p()I

    move-result v1

    const/16 v2, 0x4b0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "/"

    invoke-virtual {p0, v4, v0}, Lcom/google/android/maps/rideabout/app/a;->a(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/rideabout/app/b;->c:[I

    iget-object v3, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v3}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Lbi/h;->a(Ljava/util/Date;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    invoke-static {}, Lcom/google/android/maps/rideabout/app/a;->q()J

    move-result-wide v3

    cmp-long v3, v1, v3

    if-gez v3, :cond_0

    long-to-int v0, v1

    invoke-static {v0, v5}, Lcom/google/googlenav/ui/bd;->a(IZ)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x499

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public f()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/maps/rideabout/app/b;->c:[I

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v1}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/a;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public g()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    iget v0, v0, Lbi/a;->b:F

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v1

    invoke-virtual {v1}, Lbi/h;->B()I

    move-result v2

    invoke-virtual {v1}, Lbi/h;->q()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sub-int v0, v2, v0

    return v0
.end method

.method public h()Z
    .locals 2

    sget-object v0, Lcom/google/android/maps/rideabout/app/b;->c:[I

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    invoke-virtual {v1}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method

.method public i()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v1, v1, LU/z;->b:LU/A;

    sget-object v2, LU/A;->c:LU/A;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v1, v1, LU/z;->a:LU/R;

    invoke-virtual {v1}, LU/R;->a()LU/S;

    move-result-object v1

    sget-object v2, LU/S;->a:LU/S;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public j()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/a;->k()Lbi/h;

    move-result-object v0

    instance-of v1, v0, Lbi/p;

    if-nez v1, :cond_0

    instance-of v0, v0, Lbi/k;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Lbi/h;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v0, v0, LU/z;->c:Lbi/a;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->a:Lbi/d;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/a;->b:LU/z;

    iget-object v1, v1, LU/z;->c:Lbi/a;

    iget-object v1, v1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    goto :goto_0
.end method

.method public l()Lcom/google/android/maps/rideabout/app/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/a;->c:Lcom/google/android/maps/rideabout/app/c;

    return-object v0
.end method
