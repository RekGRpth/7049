.class Lcom/google/android/maps/rideabout/app/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/rideabout/app/o;


# static fields
.field private static final a:Lcom/google/android/maps/rideabout/app/p;


# instance fields
.field private volatile b:Lcom/google/android/maps/rideabout/app/x;

.field private volatile c:Lcom/google/android/maps/rideabout/app/y;

.field private volatile d:LaS/c;

.field private e:Landroid/speech/tts/TextToSpeech;

.field private f:Landroid/content/Context;

.field private g:LaS/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/rideabout/app/p;

    invoke-direct {v0}, Lcom/google/android/maps/rideabout/app/p;-><init>()V

    sput-object v0, Lcom/google/android/maps/rideabout/app/p;->a:Lcom/google/android/maps/rideabout/app/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/p;->a()V

    return-void
.end method

.method static g()Lcom/google/android/maps/rideabout/app/p;
    .locals 1

    sget-object v0, Lcom/google/android/maps/rideabout/app/p;->a:Lcom/google/android/maps/rideabout/app/p;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    sget-object v0, LaS/c;->a:LaS/c;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.speech.tts.engine.INSTALL_TTS_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public a(Landroid/content/Context;LaS/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/p;->f:Landroid/content/Context;

    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v0, p1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    iput-object p2, p0, Lcom/google/android/maps/rideabout/app/p;->g:LaS/b;

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/p;->a()V

    return-void
.end method

.method public a(Lcom/google/android/maps/rideabout/app/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->b:Lcom/google/android/maps/rideabout/app/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->b:Lcom/google/android/maps/rideabout/app/x;

    invoke-interface {p1, v0}, Lcom/google/android/maps/rideabout/app/y;->a(Lcom/google/android/maps/rideabout/app/x;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/p;->c:Lcom/google/android/maps/rideabout/app/y;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    const-string v1, "     "

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    goto :goto_0
.end method

.method public b()LaS/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    return-object v0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    sget-object v1, LaS/c;->c:LaS/c;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    sget-object v1, LaS/c;->c:LaS/c;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->u()Ljava/util/EnumSet;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0, v1}, LaS/a;->a(Ljava/util/EnumSet;)V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->u()Ljava/util/EnumSet;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v1}, LaS/a;->a(Ljava/util/EnumSet;)V

    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    :cond_0
    return-void
.end method

.method public onInit(I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    sget-object v0, LaS/c;->e:LaS/c;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/p;->d()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, LaS/c;->f:LaS/c;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->g:LaS/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->g:LaS/b;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    invoke-interface {v0, v1}, LaS/b;->a(LaS/c;)V

    iput-object v4, p0, Lcom/google/android/maps/rideabout/app/p;->g:LaS/b;

    goto :goto_0

    :pswitch_0
    sget-object v0, LaS/c;->b:LaS/c;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    goto :goto_1

    :pswitch_1
    sget-object v0, LaS/c;->d:LaS/c;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    goto :goto_1

    :pswitch_2
    sget-object v0, LaS/c;->c:LaS/c;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->d:LaS/c;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->c:Lcom/google/android/maps/rideabout/app/y;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/p;->c:Lcom/google/android/maps/rideabout/app/y;

    new-instance v1, Lcom/google/android/maps/rideabout/app/w;

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/p;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/maps/rideabout/app/p;->e:Landroid/speech/tts/TextToSpeech;

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/rideabout/app/w;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;)V

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/app/y;->a(Lcom/google/android/maps/rideabout/app/x;)V

    iput-object v4, p0, Lcom/google/android/maps/rideabout/app/p;->c:Lcom/google/android/maps/rideabout/app/y;

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
