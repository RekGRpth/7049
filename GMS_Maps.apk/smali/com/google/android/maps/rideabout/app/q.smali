.class public Lcom/google/android/maps/rideabout/app/q;
.super LaS/a;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/CharSequence;


# instance fields
.field private b:Lcom/google/android/maps/rideabout/app/NavigationService;

.field private c:Z

.field private d:Lbi/d;

.field private e:Z

.field private final f:Lcom/google/android/maps/MapsActivity;

.field private g:Lax/w;

.field private h:Lcom/google/googlenav/ui/e;

.field private final i:Lcom/google/googlenav/ui/wizard/jv;

.field private j:Lcom/google/android/maps/rideabout/app/o;

.field private k:Landroid/media/AudioManager;

.field private final l:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "Navigation service not ready"

    sput-object v0, Lcom/google/android/maps/rideabout/app/q;->a:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0}, LaS/a;-><init>()V

    new-instance v0, Lcom/google/android/maps/rideabout/app/r;

    invoke-direct {v0, p0}, Lcom/google/android/maps/rideabout/app/r;-><init>(Lcom/google/android/maps/rideabout/app/q;)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->l:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    iput-object p2, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method private A()V
    .locals 8

    const-string v0, "s"

    invoke-direct {p0, v0}, Lcom/google/android/maps/rideabout/app/q;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x4aa

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x49b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x486

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x49d

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x49c

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-instance v7, Lcom/google/android/maps/rideabout/app/t;

    invoke-direct {v7, p0}, Lcom/google/android/maps/rideabout/app/t;-><init>(Lcom/google/android/maps/rideabout/app/q;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method private B()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-static {v0}, Lcom/google/android/maps/rideabout/app/q;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->q()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->n()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/app/q;->a(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->H()V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->c()V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->e()V

    goto :goto_0
.end method

.method private C()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->L()Lcom/google/googlenav/ui/wizard/ik;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/app/o;->d()V

    return-void
.end method

.method private D()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->E()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->F()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_providers_allowed"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private F()V
    .locals 8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->J()Lcom/google/googlenav/ui/wizard/ib;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ib;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->J()Lcom/google/googlenav/ui/wizard/ib;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ib;->a()V

    :cond_0
    const-string v0, "s"

    invoke-direct {p0, v0}, Lcom/google/android/maps/rideabout/app/q;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x4a5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x4e0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-instance v7, Lcom/google/android/maps/rideabout/app/v;

    invoke-direct {v7, p0}, Lcom/google/android/maps/rideabout/app/v;-><init>(Lcom/google/android/maps/rideabout/app/q;)V

    move-object v5, v4

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method private G()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private H()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/j;->a(Landroid/content/Context;Law/h;LX/e;)V

    return-void
.end method

.method private I()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->h:Lcom/google/googlenav/ui/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/q;Landroid/media/AudioManager;)Landroid/media/AudioManager;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/q;->k:Landroid/media/AudioManager;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/q;)Lcom/google/android/maps/rideabout/app/NavigationService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/q;Lcom/google/android/maps/rideabout/app/NavigationService;)Lcom/google/android/maps/rideabout/app/NavigationService;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/q;Lcom/google/android/maps/rideabout/app/o;)Lcom/google/android/maps/rideabout/app/o;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v1, 0xc3b

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/q;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x61

    const-string v1, "d"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/q;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/rideabout/app/q;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/maps/rideabout/app/q;)Lax/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->g:Lax/w;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/rideabout/app/q;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/q;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x61

    const-string v1, "td"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const v1, 0x7fffffff

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningServiceInfo;

    const-class v2, Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PERM"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/maps/rideabout/app/q;)Lbi/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/maps/rideabout/app/q;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/q;->c(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x61

    const-string v1, "gd"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/rideabout/app/q;)Lcom/google/android/maps/MapsActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/rideabout/app/q;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/maps/rideabout/app/q;)Lcom/google/android/maps/rideabout/app/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/maps/rideabout/app/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->B()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/maps/rideabout/app/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->C()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/maps/rideabout/app/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->G()V

    return-void
.end method


# virtual methods
.method public a(LaS/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-interface {v0, v1, p1}, Lcom/google/android/maps/rideabout/app/o;->a(Landroid/content/Context;LaS/b;)V

    return-void
.end method

.method public a(Lax/w;Lcom/google/googlenav/ui/e;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/q;->g:Lax/w;

    new-instance v0, Lbi/e;

    invoke-direct {v0, p1}, Lbi/e;-><init>(Lax/w;)V

    invoke-virtual {v0}, Lbi/e;->a()Lbi/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    iput-object p2, p0, Lcom/google/android/maps/rideabout/app/q;->h:Lcom/google/googlenav/ui/e;

    return-void
.end method

.method public a(Ljava/util/EnumSet;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Landroid/content/Context;Ljava/util/EnumSet;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    const-string v1, "ServiceTerminatedSafely"

    invoke-static {v0, v1, p1}, LX/m;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/q;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/q;->k:Landroid/media/AudioManager;

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->I()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    iget-object v3, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    iget-object v4, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v3, v4}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Landroid/content/Context;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v2

    if-eqz v2, :cond_1

    packed-switch p1, :pswitch_data_0

    :cond_1
    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->k:Landroid/media/AudioManager;

    invoke-virtual {v1, v5, v0, v6}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->k:Landroid/media/AudioManager;

    const/4 v2, -0x1

    invoke-virtual {v1, v5, v2, v6}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Lcom/google/android/maps/rideabout/app/NavigationService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    return-object v0
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    invoke-interface {v0, p1}, Lcom/google/android/maps/rideabout/app/o;->a(Z)V

    return-void
.end method

.method public c()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    const-class v2, Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/MapsActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public d()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->c()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    const-class v2, Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/MapsActivity;->stopService(Landroid/content/Intent;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/q;->e:Z

    iput-object v3, p0, Lcom/google/android/maps/rideabout/app/q;->g:Lax/w;

    iput-object v3, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    iput-object v3, p0, Lcom/google/android/maps/rideabout/app/q;->h:Lcom/google/googlenav/ui/e;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/app/o;->a()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/q;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    const-class v2, Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/q;->l:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/MapsActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iput-boolean v3, p0, Lcom/google/android/maps/rideabout/app/q;->c:Z

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/q;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->l:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/q;->c:Z

    :cond_0
    return-void
.end method

.method g()V
    .locals 3

    sget-object v0, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->u()Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    new-instance v2, Lcom/google/android/maps/rideabout/app/s;

    invoke-direct {v2, p0}, Lcom/google/android/maps/rideabout/app/s;-><init>(Lcom/google/android/maps/rideabout/app/q;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/rideabout/app/o;->a(Landroid/content/Context;LaS/b;)V

    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/q;->e:Z

    return v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-static {v0}, Lcom/google/android/maps/rideabout/app/q;->a(Landroid/content/Context;)V

    return-void
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-static {v0}, Lcom/google/android/maps/rideabout/app/q;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public k()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->l()V

    return-void
.end method

.method public l()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->w()LaS/c;

    move-result-object v0

    sget-object v1, LaS/c;->a:LaS/c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    const-string v1, "ShowTransitNavDisclaimer"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->A()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/q;->B()V

    goto :goto_0
.end method

.method public m()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->w()LaS/c;

    move-result-object v0

    sget-object v1, LaS/c;->b:LaS/c;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "s"

    invoke-direct {p0, v0}, Lcom/google/android/maps/rideabout/app/q;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->i:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v1, Lcom/google/android/maps/rideabout/app/u;

    invoke-direct {v1, p0}, Lcom/google/android/maps/rideabout/app/u;-><init>(Lcom/google/android/maps/rideabout/app/q;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_0
.end method

.method public n()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->h:Lcom/google/googlenav/ui/e;

    const/16 v1, 0xb54

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    return-void
.end method

.method public o()Lax/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->g:Lax/w;

    return-object v0
.end method

.method public p()Lbi/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    return-object v0
.end method

.method public q()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    const-string v1, "ServiceTerminatedSafely"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public r()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    sget-object v1, Lcom/google/android/maps/rideabout/app/q;->a:Ljava/lang/CharSequence;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/q;->b()Lcom/google/android/maps/rideabout/app/NavigationService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Lbi/d;)V

    goto :goto_0
.end method

.method public s()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method public t()Ljava/util/EnumSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->e()Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/util/EnumSet;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Landroid/content/Context;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->f:Lcom/google/android/maps/MapsActivity;

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/app/o;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public w()LaS/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    if-nez v0, :cond_0

    sget-object v0, LaS/c;->a:LaS/c;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/app/o;->b()LaS/c;

    move-result-object v0

    goto :goto_0
.end method

.method public x()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->a()Lcom/google/android/maps/rideabout/view/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/view/c;->i()LU/z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    invoke-static {v1, v0}, LX/r;->a(Lbi/d;LU/z;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/q;->d:Lbi/d;

    invoke-virtual {v1}, Lbi/d;->c()F

    move-result v1

    invoke-static {v1}, LX/r;->a(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x61

    const-string v3, "el"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "m="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "d="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public y()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->b:Lcom/google/android/maps/rideabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/q;->j:Lcom/google/android/maps/rideabout/app/o;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/app/o;->f()V

    :cond_0
    return-void
.end method
