.class public Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;
.super Lcom/google/android/maps/rideabout/view/StepDescriptionView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/rideabout/view/i;


# instance fields
.field private h:Lcom/google/googlenav/ui/view/android/rideabout/q;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private q:Lcom/google/android/maps/rideabout/view/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;)Lcom/google/android/maps/rideabout/view/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->q:Lcom/google/android/maps/rideabout/view/c;

    return-object v0
.end method

.method private a(ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->setBackgroundType(I)V

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->c()V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->c(Z)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->c(Z)V

    goto :goto_1
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->setClickable(Z)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/q;

    const v1, 0x7f0f00e7

    const v2, 0x7f0f00e8

    invoke-direct {v0, p1, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/q;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->h:Lcom/google/googlenav/ui/view/android/rideabout/q;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0203d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->i:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0203d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->j:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0203d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->k:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0203d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->l:Landroid/graphics/drawable/Drawable;

    const v0, 0x7f100478

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->m:Landroid/widget/TextView;

    const v0, 0x7f100479

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->n:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->c:Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/rideabout/view/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->d:Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/rideabout/view/SqueezedLabelView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->h()V

    return-void
.end method

.method private g()V
    .locals 2

    const v0, 0x7f10047a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/maps/rideabout/view/n;

    invoke-direct {v1, p0}, Lcom/google/android/maps/rideabout/view/n;-><init>(Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10047b

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/maps/rideabout/view/o;

    invoke-direct {v1, p0}, Lcom/google/android/maps/rideabout/view/o;-><init>(Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private h()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->f()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x4a0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x1080038

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v2, Lcom/google/android/maps/driveabout/app/dj;

    const v3, 0x3fa66666

    invoke-direct {v2, v0, v3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    const/16 v0, 0x21

    invoke-virtual {v1, v2, v4, v5, v0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    const v0, 0x7f10047a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->o:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->o:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10047b

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->p:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->p:Landroid/widget/Button;

    const/16 v1, 0x48c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->b(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/rideabout/app/a;Z)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/google/android/maps/rideabout/app/a;->a(Z)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/maps/rideabout/view/p;->a:[I

    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/app/a;->l()Lcom/google/android/maps/rideabout/app/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/rideabout/app/c;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-nez v2, :cond_0

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/app/a;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_2
    invoke-direct {p0, v1, v2, v0, p2}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)V

    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->i:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->k:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->h:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/4 v4, 0x0

    new-array v4, v4, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/ui/view/android/rideabout/q;->b(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/maps/rideabout/view/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->q:Lcom/google/android/maps/rideabout/view/c;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/16 v2, 0x4ea

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->b:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->g()V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/16 v2, 0x4c5

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->q:Lcom/google/android/maps/rideabout/view/c;

    return-void
.end method

.method protected e()I
    .locals 1

    const v0, 0x7f0401c1

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "x\n%s"

    return-object v0
.end method
