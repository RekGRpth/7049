.class public Lcom/google/android/maps/rideabout/view/a;
.super Lcom/google/googlenav/ui/view/r;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/android/BaseAndroidView;

.field private final b:Lcom/google/googlenav/ui/android/ButtonContainer;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/r;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/rideabout/view/a;->a:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iput-object p2, p0, Lcom/google/android/maps/rideabout/view/a;->b:Lcom/google/googlenav/ui/android/ButtonContainer;

    return-void
.end method

.method private m()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/a;->b:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f100057

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ViewSwitcher;->setMeasureAllChildren(Z)V

    :cond_0
    return-void
.end method

.method private n()Landroid/widget/ViewSwitcher;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/a;->b:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f100439

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    return-object v0
.end method

.method private o()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/a;->b:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f100054

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private p()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/a;->a:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->m()V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/view/a;->b()V

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Landroid/widget/ViewSwitcher;Landroid/content/res/Configuration;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->o()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/a;->b:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f100075

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/a;->a:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/rideabout/view/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/maps/rideabout/view/b;-><init>(Lcom/google/android/maps/rideabout/view/a;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public d()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/a;->a:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public e()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->o()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public f()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->o()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public g()Lcom/google/android/maps/rideabout/view/h;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/rideabout/view/TransitNavigationFooterView;

    return-object v0
.end method

.method public h()Lcom/google/android/maps/rideabout/view/i;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;

    return-object v0
.end method

.method public i()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->n()Landroid/widget/ViewSwitcher;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->o()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()I
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->p()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->p()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/a;->p()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Landroid/content/res/Configuration;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
