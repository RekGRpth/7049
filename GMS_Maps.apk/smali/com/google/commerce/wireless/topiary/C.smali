.class public Lcom/google/commerce/wireless/topiary/C;
.super Landroid/support/v4/app/f;
.source "SourceFile"


# instance fields
.field protected N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

.field protected O:Lcom/google/commerce/wireless/topiary/D;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/f;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, -0x1

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/C;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    :try_start_0
    invoke-static {v2}, Lcom/google/commerce/wireless/topiary/b;->a(Landroid/app/Activity;)Landroid/accounts/Account;
    :try_end_0
    .catch Lcom/google/commerce/wireless/topiary/c; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v3

    iput-object v3, p0, Lcom/google/commerce/wireless/topiary/C;->O:Lcom/google/commerce/wireless/topiary/D;

    new-instance v3, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    iget-object v4, p0, Lcom/google/commerce/wireless/topiary/C;->O:Lcom/google/commerce/wireless/topiary/D;

    invoke-direct {v3, v2, v0, v4, v1}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;Lcom/google/commerce/wireless/topiary/z;)V

    iput-object v3, p0, Lcom/google/commerce/wireless/topiary/C;->N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/C;->N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/C;->b(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/C;->N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/C;->N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/C;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/commerce/wireless/topiary/T;->a(Landroid/content/Intent;)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/commerce/wireless/topiary/C;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/C;->N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a()V

    iput-object v1, p0, Lcom/google/commerce/wireless/topiary/C;->N:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    iput-object v1, p0, Lcom/google/commerce/wireless/topiary/C;->O:Lcom/google/commerce/wireless/topiary/D;

    invoke-super {p0}, Landroid/support/v4/app/f;->i()V

    return-void
.end method
