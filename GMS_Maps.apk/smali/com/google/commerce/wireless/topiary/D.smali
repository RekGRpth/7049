.class public Lcom/google/commerce/wireless/topiary/D;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/commerce/wireless/topiary/D;

.field private static b:Lcom/google/commerce/wireless/topiary/a;


# instance fields
.field private c:Ljava/util/Map;

.field private d:Ljava/util/Map;

.field private e:Ljava/util/Map;

.field private f:Landroid/content/Context;

.field private g:Lcom/google/commerce/wireless/topiary/B;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->c:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->e:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/D;->f:Landroid/content/Context;

    new-instance v0, Lcom/google/commerce/wireless/topiary/B;

    invoke-direct {v0, p1}, Lcom/google/commerce/wireless/topiary/B;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->g:Lcom/google/commerce/wireless/topiary/B;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/D;->a:Lcom/google/commerce/wireless/topiary/D;

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/commerce/wireless/topiary/m;->a(Landroid/content/Context;)V

    new-instance v1, Lcom/google/commerce/wireless/topiary/D;

    invoke-direct {v1, v0}, Lcom/google/commerce/wireless/topiary/D;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/commerce/wireless/topiary/D;->a:Lcom/google/commerce/wireless/topiary/D;

    invoke-static {v0}, Lcom/google/commerce/wireless/topiary/d;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/d;

    move-result-object v0

    sput-object v0, Lcom/google/commerce/wireless/topiary/D;->b:Lcom/google/commerce/wireless/topiary/a;

    :cond_0
    sget-object v0, Lcom/google/commerce/wireless/topiary/D;->a:Lcom/google/commerce/wireless/topiary/D;

    return-object v0
.end method

.method private b(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<no account>"

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/HybridWebView;
    .locals 3

    const-string v0, "HybridWebViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating WebView for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/D;->b(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-direct {v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;-><init>(Landroid/content/Context;)V

    if-eqz p2, :cond_0

    invoke-virtual {v0, p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/D;Landroid/accounts/Account;)V

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;
    .locals 3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/T;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/f;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->c:Ljava/util/Map;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/commerce/wireless/topiary/f;

    invoke-direct {v0, p1, p0}, Lcom/google/commerce/wireless/topiary/f;-><init>(Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;)V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/D;->c:Ljava/util/Map;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->destroy()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/E;)V
    .locals 4

    invoke-direct {p0, p3}, Lcom/google/commerce/wireless/topiary/D;->b(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "HybridWebViewManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preloading url "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    if-nez v0, :cond_0

    const-string v0, "HybridWebViewManager"

    const-string v2, "Creating preloading webview "

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->f:Landroid/content/Context;

    invoke-virtual {p0, v0, p3}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/HybridWebView;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setPreloading(Z)V

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/D;->d:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    invoke-interface {p4, v0}, Lcom/google/commerce/wireless/topiary/E;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    return-void
.end method

.method public b()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/D;->a()V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/f;->b()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/D;->b()V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->g:Lcom/google/commerce/wireless/topiary/B;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/B;->a()V

    return-void
.end method

.method d()Lcom/google/commerce/wireless/topiary/a;
    .locals 1

    sget-object v0, Lcom/google/commerce/wireless/topiary/D;->b:Lcom/google/commerce/wireless/topiary/a;

    return-object v0
.end method

.method e()Lcom/google/commerce/wireless/topiary/B;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/D;->g:Lcom/google/commerce/wireless/topiary/B;

    return-object v0
.end method
