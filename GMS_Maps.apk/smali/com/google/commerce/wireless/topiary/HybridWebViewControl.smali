.class public Lcom/google/commerce/wireless/topiary/HybridWebViewControl;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/commerce/wireless/topiary/v;


# static fields
.field private static h:Lcom/google/commerce/wireless/topiary/F;


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/widget/ProgressBar;

.field private d:Landroid/os/Handler;

.field private final e:Landroid/accounts/Account;

.field private f:Lcom/google/commerce/wireless/topiary/D;

.field private g:Lcom/google/commerce/wireless/topiary/z;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/commerce/wireless/topiary/F;

    const-string v1, "Could not load due to internal error. Details: %1$s"

    const-string v2, "Could not log in. Please check you have a working account set up."

    const-string v3, "Could not load. Please check your network connection and try again"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/commerce/wireless/topiary/F;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->h:Lcom/google/commerce/wireless/topiary/F;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;Lcom/google/commerce/wireless/topiary/z;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, -0x2

    const/4 v4, -0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->d:Landroid/os/Handler;

    invoke-virtual {p0, p4}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->setClient(Lcom/google/commerce/wireless/topiary/z;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x101007a

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->addView(Landroid/view/View;)V

    const-string v0, "Before everything"

    invoke-static {v6, v0}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    iput-object p3, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->f:Lcom/google/commerce/wireless/topiary/D;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->e:Landroid/accounts/Account;

    new-instance v0, Lcom/google/commerce/wireless/topiary/A;

    invoke-direct {v0}, Lcom/google/commerce/wireless/topiary/A;-><init>()V

    invoke-direct {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->e()Lcom/google/commerce/wireless/topiary/HybridWebView;

    return-void
.end method

.method private a(Z)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c(I)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->canGoBack()Z

    move-result v3

    if-eqz v3, :cond_3

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v4, 0x7

    if-le v3, v4, :cond_3

    if-nez p1, :cond_2

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->goBack()V

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v1, :cond_5

    if-nez p1, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Landroid/webkit/WebView;)V

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method private e()Lcom/google/commerce/wireless/topiary/HybridWebView;
    .locals 3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->f:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->e:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/HybridWebView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v0, p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setHybridWebViewUiClient(Lcom/google/commerce/wireless/topiary/v;)V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v1, v0}, Lcom/google/commerce/wireless/topiary/z;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-object v0
.end method

.method public static setResources(Lcom/google/commerce/wireless/topiary/F;)V
    .locals 0

    sput-object p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->h:Lcom/google/commerce/wireless/topiary/F;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/commerce/wireless/topiary/HybridWebView;)Landroid/webkit/WebView;
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->e()Lcom/google/commerce/wireless/topiary/HybridWebView;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->removeAllViews()V

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->d:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->f:Lcom/google/commerce/wireless/topiary/D;

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->destroyDrawingCache()V

    return-void
.end method

.method public a(Landroid/webkit/WebView;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/webkit/WebView;->destroy()V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p0, v1}, Lcom/google/commerce/wireless/topiary/z;->a(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;I)V

    goto :goto_0
.end method

.method public a(Lcom/google/commerce/wireless/topiary/HybridWebView;ILjava/lang/String;I)V
    .locals 7

    iget-object v6, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->d:Landroid/os/Handler;

    new-instance v0, Lcom/google/commerce/wireless/topiary/y;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/commerce/wireless/topiary/y;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Lcom/google/commerce/wireless/topiary/HybridWebView;ILjava/lang/String;I)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V
    .locals 7

    iget-object v6, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->d:Landroid/os/Handler;

    new-instance v0, Lcom/google/commerce/wireless/topiary/x;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/commerce/wireless/topiary/x;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/HybridWebView;)V
    .locals 3

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->e()Lcom/google/commerce/wireless/topiary/HybridWebView;

    move-result-object v0

    move-object p3, v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    if-eq v0, p3, :cond_1

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    move-object p3, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p3, p1, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/commerce/wireless/topiary/z;->c(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected b(Lcom/google/commerce/wireless/topiary/HybridWebView;ILjava/lang/String;I)V
    .locals 6

    const/4 v4, 0x0

    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->h:Lcom/google/commerce/wireless/topiary/F;

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/F;->a:Ljava/lang/String;

    packed-switch p2, :pswitch_data_0

    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v4

    const/4 v2, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p0, v4}, Lcom/google/commerce/wireless/topiary/z;->b(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Z)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    move-object v1, p0

    move v2, p2

    move v4, p4

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/commerce/wireless/topiary/z;->a(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;ILjava/lang/String;ILjava/lang/String;)V

    :cond_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->h:Lcom/google/commerce/wireless/topiary/F;

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/F;->c:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->h:Lcom/google/commerce/wireless/topiary/F;

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/F;->b:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->h:Lcom/google/commerce/wireless/topiary/F;

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/F;->a:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/commerce/wireless/topiary/z;->a(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    return-void
.end method

.method protected b(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V
    .locals 3

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d()Z

    move-result v1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v2, p0, v0}, Lcom/google/commerce/wireless/topiary/z;->a(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Z)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p0, v1}, Lcom/google/commerce/wireless/topiary/z;->b(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Z)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/commerce/wireless/topiary/z;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/commerce/wireless/topiary/z;->b(Lcom/google/commerce/wireless/topiary/HybridWebViewControl;Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Z)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Z)Z

    return-void
.end method

.method public setClient(Lcom/google/commerce/wireless/topiary/z;)V
    .locals 0

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->g:Lcom/google/commerce/wireless/topiary/z;

    return-void
.end method

.method public setWaitUiVisibility(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setWebViewVisibility(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
