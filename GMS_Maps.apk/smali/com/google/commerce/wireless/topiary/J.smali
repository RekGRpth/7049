.class Lcom/google/commerce/wireless/topiary/J;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/commerce/wireless/topiary/Y;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/J;->b:Ljava/lang/String;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/J;->a:Landroid/net/Uri;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/J;->a:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/net/URL;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    return-object v0
.end method

.method private a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->g()Lcom/google/commerce/wireless/topiary/L;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/L;->b()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->b()I

    move-result v1

    sub-int v0, v1, v0

    if-lez v0, :cond_0

    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/commerce/wireless/topiary/N;->i:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;)V

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->g()Lcom/google/commerce/wireless/topiary/L;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/L;->c()V

    goto :goto_0
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/net/HttpURLConnection;)V
    .locals 2

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    new-instance v1, Lcom/google/commerce/wireless/topiary/n;

    invoke-direct {v1, v0}, Lcom/google/commerce/wireless/topiary/n;-><init>(I)V

    throw v1

    :cond_0
    return-void
.end method

.method private a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;Ljava/io/InputStream;)[B
    .locals 2

    new-instance v0, Lcom/google/commerce/wireless/topiary/l;

    invoke-direct {v0, p3}, Lcom/google/commerce/wireless/topiary/l;-><init>(Ljava/io/InputStream;)V

    :goto_0
    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/l;->a()[B

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/J;->a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;)V

    goto :goto_0
.end method

.method private b(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;[B)Ljava/io/InputStream;
    .locals 6

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->g()Lcom/google/commerce/wireless/topiary/L;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/L;->a()V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const-string v0, "POST"

    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v5

    if-nez v4, :cond_0

    if-nez v5, :cond_0

    invoke-virtual {v2, v3}, Lcom/google/commerce/wireless/topiary/L;->a(Ljava/util/Map;)V

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/J;->a(Ljava/net/HttpURLConnection;)V

    invoke-direct {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/J;->a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;)V

    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/commerce/wireless/topiary/L;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v1}, Lcom/google/commerce/wireless/topiary/J;->a(Ljava/io/Closeable;)V

    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, v1}, Lcom/google/commerce/wireless/topiary/J;->a(Ljava/io/Closeable;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/commerce/wireless/topiary/K;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 4

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/J;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v2

    const-string v0, "User-Agent"

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/J;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Accept"

    const-string v1, "application/x-protobuffer"

    invoke-virtual {v2, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;[B)[B
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/J;->b(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;[B)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/J;->a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/google/commerce/wireless/topiary/J;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-direct {p0, v1}, Lcom/google/commerce/wireless/topiary/J;->a(Ljava/io/Closeable;)V

    throw v0
.end method
