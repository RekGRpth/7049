.class public Lcom/google/commerce/wireless/topiary/K;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/commerce/wireless/topiary/L;

.field private final b:Ljava/util/Map;

.field private c:Landroid/accounts/Account;

.field private d:Ljava/util/Map;

.field private e:Lcom/google/commerce/wireless/topiary/N;

.field private f:Ljava/lang/Object;

.field private g:I

.field private h:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/commerce/wireless/topiary/L;

    invoke-direct {v0}, Lcom/google/commerce/wireless/topiary/L;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->a:Lcom/google/commerce/wireless/topiary/L;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->b:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->b:Ljava/util/Map;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->d:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->f:Ljava/lang/Object;

    const/16 v0, 0x7530

    iput v0, p0, Lcom/google/commerce/wireless/topiary/K;->g:I

    return-void
.end method


# virtual methods
.method public a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->c:Landroid/accounts/Account;

    return-object v0
.end method

.method public a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/K;
    .locals 0

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/K;->c:Landroid/accounts/Account;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->a:Lcom/google/commerce/wireless/topiary/L;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/L;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/commerce/wireless/topiary/M;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/M;I)V

    :cond_0
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/K;->f:Ljava/lang/Object;

    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/M;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->b:Ljava/util/Map;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/N;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;Ljava/lang/Exception;)V

    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/N;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/K;->e:Lcom/google/commerce/wireless/topiary/N;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/K;->h:Ljava/lang/Exception;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/commerce/wireless/topiary/K;->g:I

    return v0
.end method

.method public c()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->d:Ljava/util/Map;

    return-object v0
.end method

.method public d()Lcom/google/commerce/wireless/topiary/N;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->e:Lcom/google/commerce/wireless/topiary/N;

    return-object v0
.end method

.method public e()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->h:Ljava/lang/Exception;

    return-object v0
.end method

.method f()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->f:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->f:Ljava/lang/Object;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/K;->f:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/K;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method g()Lcom/google/commerce/wireless/topiary/L;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/K;->a:Lcom/google/commerce/wireless/topiary/L;

    return-object v0
.end method
