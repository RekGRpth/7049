.class public Lcom/google/commerce/wireless/topiary/U;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/U;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/U;->b:Ljava/lang/String;

    iput p3, p0, Lcom/google/commerce/wireless/topiary/U;->c:I

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/commerce/wireless/topiary/U;
    .locals 4

    new-instance v0, Lcom/google/commerce/wireless/topiary/U;

    const-string v1, "url"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "allowedAgeInSeconds"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/commerce/wireless/topiary/U;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
