.class public Lcom/google/commerce/wireless/topiary/W;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/commerce/wireless/topiary/T;

.field private b:Lcom/google/commerce/wireless/topiary/X;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>(Lcom/google/commerce/wireless/topiary/T;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/commerce/wireless/topiary/W;->c:J

    iput-wide v0, p0, Lcom/google/commerce/wireless/topiary/W;->d:J

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/W;->a:Lcom/google/commerce/wireless/topiary/T;

    sget-object v0, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/W;->b:Lcom/google/commerce/wireless/topiary/X;

    return-void
.end method

.method public static a(J)Z
    .locals 2

    const-wide/16 v0, 0x12c

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/commerce/wireless/topiary/X;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/W;->b:Lcom/google/commerce/wireless/topiary/X;

    return-object v0
.end method

.method public a(Lcom/google/commerce/wireless/topiary/X;)V
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/W;->b:Lcom/google/commerce/wireless/topiary/X;

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-ne p1, v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/commerce/wireless/topiary/W;->c:J

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/W;->b:Lcom/google/commerce/wireless/topiary/X;

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    if-ne p1, v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/commerce/wireless/topiary/W;->d:J

    :cond_1
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/W;->b:Lcom/google/commerce/wireless/topiary/X;

    return-void
.end method

.method public b()J
    .locals 4

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/commerce/wireless/topiary/W;->c:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/commerce/wireless/topiary/W;->c:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public c()J
    .locals 4

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/commerce/wireless/topiary/W;->d:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/commerce/wireless/topiary/W;->d:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method
