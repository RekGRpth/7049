.class Lcom/google/commerce/wireless/topiary/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/commerce/wireless/topiary/M;


# instance fields
.field private final a:Lcom/google/commerce/wireless/topiary/a;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/commerce/wireless/topiary/b;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/i;->a:Lcom/google/commerce/wireless/topiary/a;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/i;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/commerce/wireless/topiary/i;->c:Ljava/lang/String;

    return-void
.end method

.method private c(Lcom/google/commerce/wireless/topiary/K;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->c()Ljava/util/Map;

    move-result-object v0

    const-string v1, "Authorization"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/i;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/i;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/commerce/wireless/topiary/K;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->a()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/i;->a:Lcom/google/commerce/wireless/topiary/a;

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/i;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lcom/google/commerce/wireless/topiary/a;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "AuthorizationRpcProcessor"

    const-string v1, "Couldn\'t get authentication token"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/commerce/wireless/topiary/N;->f:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/i;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/commerce/wireless/topiary/N;->f:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;)V

    goto :goto_0
.end method

.method public b(Lcom/google/commerce/wireless/topiary/K;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->d()Lcom/google/commerce/wireless/topiary/N;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/N;->f:Lcom/google/commerce/wireless/topiary/N;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->a()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/i;->c(Lcom/google/commerce/wireless/topiary/K;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/i;->a:Lcom/google/commerce/wireless/topiary/a;

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/commerce/wireless/topiary/a;->b(Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/M;)V

    :cond_0
    return-void
.end method
