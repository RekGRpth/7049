.class public Lcom/google/commerce/wireless/topiary/o;
.super Landroid/webkit/WebChromeClient;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/commerce/wireless/topiary/HybridWebView;


# direct methods
.method public constructor <init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V
    .locals 0

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    return-void
.end method


# virtual methods
.method protected a(Landroid/webkit/WebView;I)V
    .locals 0

    return-void
.end method

.method public final onCloseWindow(Landroid/webkit/WebView;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p1}, Lcom/google/commerce/wireless/topiary/v;->a(Landroid/webkit/WebView;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/webkit/WebChromeClient;->onCloseWindow(Landroid/webkit/WebView;)V

    return-void
.end method

.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebChromeClient;->onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v2, v2, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v2, :cond_0

    const-string v2, "Create window"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    const-string v2, "HybridWebChromeClient"

    const-string v3, "Creating a new window"

    invoke-static {v2, v3}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v2, v2, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-interface {v2, v3}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;)Landroid/webkit/WebView;

    move-result-object v2

    const-string v3, "Window created"

    invoke-static {v0, v3}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setVisibility(I)V

    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/webkit/WebView$WebViewTransport;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView$WebViewTransport;->setWebView(Landroid/webkit/WebView;)V

    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    move v0, v1

    goto :goto_0
.end method

.method public onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLandroid/webkit/WebStorage$QuotaUpdater;)V
    .locals 2

    const-wide/16 v0, 0x2

    mul-long/2addr v0, p5

    invoke-interface {p9, v0, v1}, Landroid/webkit/WebStorage$QuotaUpdater;->updateQuota(J)V

    return-void
.end method

.method public final onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    const/16 v0, 0x64

    if-lt p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/o;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->g()V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/o;->a(Landroid/webkit/WebView;I)V

    return-void
.end method

.method public onReachedMaxAppCacheSize(JJLandroid/webkit/WebStorage$QuotaUpdater;)V
    .locals 2

    const-wide/16 v0, 0x2

    mul-long/2addr v0, p1

    invoke-interface {p5, v0, v1}, Landroid/webkit/WebStorage$QuotaUpdater;->updateQuota(J)V

    return-void
.end method
