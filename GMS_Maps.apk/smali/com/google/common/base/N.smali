.class abstract enum Lcom/google/common/base/N;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/K;


# static fields
.field public static final enum a:Lcom/google/common/base/N;

.field public static final enum b:Lcom/google/common/base/N;

.field public static final enum c:Lcom/google/common/base/N;

.field public static final enum d:Lcom/google/common/base/N;

.field private static final synthetic e:[Lcom/google/common/base/N;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/common/base/O;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1, v2}, Lcom/google/common/base/O;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/base/N;->a:Lcom/google/common/base/N;

    new-instance v0, Lcom/google/common/base/P;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1, v3}, Lcom/google/common/base/P;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/base/N;->b:Lcom/google/common/base/N;

    new-instance v0, Lcom/google/common/base/Q;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1, v4}, Lcom/google/common/base/Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/base/N;->c:Lcom/google/common/base/N;

    new-instance v0, Lcom/google/common/base/R;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1, v5}, Lcom/google/common/base/R;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/base/N;->d:Lcom/google/common/base/N;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/common/base/N;

    sget-object v1, Lcom/google/common/base/N;->a:Lcom/google/common/base/N;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/common/base/N;->b:Lcom/google/common/base/N;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/base/N;->c:Lcom/google/common/base/N;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/common/base/N;->d:Lcom/google/common/base/N;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/common/base/N;->e:[Lcom/google/common/base/N;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/base/L;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/common/base/N;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/base/N;
    .locals 1

    const-class v0, Lcom/google/common/base/N;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/N;

    return-object v0
.end method

.method public static values()[Lcom/google/common/base/N;
    .locals 1

    sget-object v0, Lcom/google/common/base/N;->e:[Lcom/google/common/base/N;

    invoke-virtual {v0}, [Lcom/google/common/base/N;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/base/N;

    return-object v0
.end method


# virtual methods
.method a()Lcom/google/common/base/K;
    .locals 0

    return-object p0
.end method
