.class public final Lcom/google/common/base/S;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/common/base/e;

.field private final b:Z

.field private final c:Lcom/google/common/base/X;

.field private final d:I


# direct methods
.method private constructor <init>(Lcom/google/common/base/X;)V
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/common/base/e;->n:Lcom/google/common/base/e;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/common/base/S;-><init>(Lcom/google/common/base/X;ZLcom/google/common/base/e;I)V

    return-void
.end method

.method private constructor <init>(Lcom/google/common/base/X;ZLcom/google/common/base/e;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/common/base/S;->c:Lcom/google/common/base/X;

    iput-boolean p2, p0, Lcom/google/common/base/S;->b:Z

    iput-object p3, p0, Lcom/google/common/base/S;->a:Lcom/google/common/base/e;

    iput p4, p0, Lcom/google/common/base/S;->d:I

    return-void
.end method

.method public static a(C)Lcom/google/common/base/S;
    .locals 1

    invoke-static {p0}, Lcom/google/common/base/e;->a(C)Lcom/google/common/base/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/S;->a(Lcom/google/common/base/e;)Lcom/google/common/base/S;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/common/base/e;)Lcom/google/common/base/S;
    .locals 2

    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/common/base/S;

    new-instance v1, Lcom/google/common/base/T;

    invoke-direct {v1, p0}, Lcom/google/common/base/T;-><init>(Lcom/google/common/base/e;)V

    invoke-direct {v0, v1}, Lcom/google/common/base/S;-><init>(Lcom/google/common/base/X;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/base/S;)Lcom/google/common/base/e;
    .locals 1

    iget-object v0, p0, Lcom/google/common/base/S;->a:Lcom/google/common/base/e;

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/base/S;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/common/base/S;->b(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/base/S;->c:Lcom/google/common/base/X;

    invoke-interface {v0, p0, p1}, Lcom/google/common/base/X;->b(Lcom/google/common/base/S;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/common/base/S;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/common/base/S;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/google/common/base/S;)I
    .locals 1

    iget v0, p0, Lcom/google/common/base/S;->d:I

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/common/base/V;

    invoke-direct {v0, p0, p1}, Lcom/google/common/base/V;-><init>(Lcom/google/common/base/S;Ljava/lang/CharSequence;)V

    return-object v0
.end method
