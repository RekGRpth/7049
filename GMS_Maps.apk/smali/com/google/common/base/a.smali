.class abstract Lcom/google/common/base/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:Lcom/google/common/base/c;

.field private b:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/common/base/c;->b:Lcom/google/common/base/c;

    iput-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    return-void
.end method

.method private c()Z
    .locals 2

    sget-object v0, Lcom/google/common/base/c;->d:Lcom/google/common/base/c;

    iput-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    invoke-virtual {p0}, Lcom/google/common/base/a;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/base/a;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    sget-object v1, Lcom/google/common/base/c;->c:Lcom/google/common/base/c;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/common/base/c;->a:Lcom/google/common/base/c;

    iput-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method protected final b()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/common/base/c;->c:Lcom/google/common/base/c;

    iput-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    sget-object v3, Lcom/google/common/base/c;->d:Lcom/google/common/base/c;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    sget-object v0, Lcom/google/common/base/b;->a:[I

    iget-object v3, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    invoke-virtual {v3}, Lcom/google/common/base/c;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0}, Lcom/google/common/base/a;->c()Z

    move-result v2

    :goto_1
    :pswitch_0
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_1
    move v2, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/base/a;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/common/base/c;->b:Lcom/google/common/base/c;

    iput-object v0, p0, Lcom/google/common/base/a;->a:Lcom/google/common/base/c;

    iget-object v0, p0, Lcom/google/common/base/a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
