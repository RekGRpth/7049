.class Lcom/google/common/collect/I;
.super Lcom/google/common/collect/G;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# instance fields
.field final synthetic g:Lcom/google/common/collect/x;


# direct methods
.method constructor <init>(Lcom/google/common/collect/x;Ljava/lang/Object;Ljava/util/List;Lcom/google/common/collect/G;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/I;->g:Lcom/google/common/collect/x;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/common/collect/G;-><init>(Lcom/google/common/collect/x;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/common/collect/G;)V

    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    invoke-virtual {p0}, Lcom/google/common/collect/I;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/common/collect/I;->g:Lcom/google/common/collect/x;

    invoke-static {v1}, Lcom/google/common/collect/x;->c(Lcom/google/common/collect/x;)I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/I;->d()V

    :cond_0
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/common/collect/I;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/I;->e()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    iget-object v3, p0, Lcom/google/common/collect/I;->g:Lcom/google/common/collect/x;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, Lcom/google/common/collect/x;->a(Lcom/google/common/collect/x;I)I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/I;->d()V

    goto :goto_0
.end method

.method g()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->e()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    new-instance v0, Lcom/google/common/collect/J;

    invoke-direct {v0, p0}, Lcom/google/common/collect/J;-><init>(Lcom/google/common/collect/I;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    new-instance v0, Lcom/google/common/collect/J;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/J;-><init>(Lcom/google/common/collect/I;I)V

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/I;->g:Lcom/google/common/collect/x;

    invoke-static {v1}, Lcom/google/common/collect/x;->b(Lcom/google/common/collect/x;)I

    invoke-virtual {p0}, Lcom/google/common/collect/I;->b()V

    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 4

    invoke-virtual {p0}, Lcom/google/common/collect/I;->a()V

    iget-object v0, p0, Lcom/google/common/collect/I;->g:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/I;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/common/collect/I;->g()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/common/collect/I;->f()Lcom/google/common/collect/G;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0, v1, v2, p0}, Lcom/google/common/collect/x;->a(Lcom/google/common/collect/x;Ljava/lang/Object;Ljava/util/List;Lcom/google/common/collect/G;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/common/collect/I;->f()Lcom/google/common/collect/G;

    move-result-object p0

    goto :goto_0
.end method
