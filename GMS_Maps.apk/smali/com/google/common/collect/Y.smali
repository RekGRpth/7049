.class final Lcom/google/common/collect/Y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:I


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/common/collect/Y;-><init>(I)V

    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/common/collect/Y;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/Y;->a:I

    return v0
.end method

.method public a(I)I
    .locals 2

    iget v0, p0, Lcom/google/common/collect/Y;->a:I

    add-int v1, v0, p1

    iput v1, p0, Lcom/google/common/collect/Y;->a:I

    return v0
.end method

.method public b(I)I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/Y;->a:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/common/collect/Y;->a:I

    return v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lcom/google/common/collect/Y;->a:I

    return-void
.end method

.method public d(I)I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/Y;->a:I

    iput p1, p0, Lcom/google/common/collect/Y;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/common/collect/Y;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/common/collect/Y;

    iget v0, p1, Lcom/google/common/collect/Y;->a:I

    iget v1, p0, Lcom/google/common/collect/Y;->a:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/Y;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/common/collect/Y;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
