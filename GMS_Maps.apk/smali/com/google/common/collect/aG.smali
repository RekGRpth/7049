.class public Lcom/google/common/collect/aG;
.super Lcom/google/common/collect/az;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/dz;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final transient a:Lcom/google/common/collect/aR;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ax;ILjava/util/Comparator;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/az;-><init>(Lcom/google/common/collect/ax;I)V

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/common/collect/aG;->a:Lcom/google/common/collect/aR;

    return-void

    :cond_0
    invoke-static {p3}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/common/collect/aG;->b(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;
    .locals 6

    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lcom/google/common/collect/cV;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    invoke-static {}, Lcom/google/common/collect/aG;->e()Lcom/google/common/collect/aG;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v0, p0, Lcom/google/common/collect/aG;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/google/common/collect/aG;

    invoke-virtual {v0}, Lcom/google/common/collect/aG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-static {}, Lcom/google/common/collect/ax;->i()Lcom/google/common/collect/ay;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {p0}, Lcom/google/common/collect/cV;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez p1, :cond_3

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v2, v4, v0}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_3
    move v1, v0

    goto :goto_1

    :cond_3
    invoke-static {p1, v0}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/google/common/collect/aR;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v0, Lcom/google/common/collect/aG;

    invoke-virtual {v2}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v2

    invoke-direct {v0, v2, v1, p1}, Lcom/google/common/collect/aG;-><init>(Lcom/google/common/collect/ax;ILjava/util/Comparator;)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public static e()Lcom/google/common/collect/aG;
    .locals 1

    sget-object v0, Lcom/google/common/collect/ad;->a:Lcom/google/common/collect/ad;

    return-object v0
.end method

.method public static f()Lcom/google/common/collect/aH;
    .locals 1

    new-instance v0, Lcom/google/common/collect/aH;

    invoke-direct {v0}, Lcom/google/common/collect/aH;-><init>()V

    return-object v0
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Lcom/google/common/collect/ar;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aG;->c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aG;->c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aG;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/ImmutableSet;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/aG;->a:Lcom/google/common/collect/aR;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/aG;->a:Lcom/google/common/collect/aR;

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    goto :goto_0
.end method
