.class public final Lcom/google/common/collect/aH;
.super Lcom/google/common/collect/aA;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/common/collect/aA;-><init>()V

    new-instance v0, Lcom/google/common/collect/aI;

    invoke-direct {v0}, Lcom/google/common/collect/aI;-><init>()V

    iput-object v0, p0, Lcom/google/common/collect/aH;->a:Lcom/google/common/collect/cV;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/aG;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/aH;->a:Lcom/google/common/collect/cV;

    iget-object v1, p0, Lcom/google/common/collect/aH;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Lcom/google/common/collect/aG;->a(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/google/common/collect/aH;
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/aH;->a:Lcom/google/common/collect/cV;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/common/collect/cV;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aH;
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/aH;->a:Lcom/google/common/collect/cV;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/cV;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-object p0
.end method

.method public varargs a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/common/collect/aH;
    .locals 1

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/google/common/collect/aH;

    move-result-object v0

    return-object v0
.end method
