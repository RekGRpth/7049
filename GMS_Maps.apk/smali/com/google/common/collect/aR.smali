.class public abstract Lcom/google/common/collect/aR;
.super Lcom/google/common/collect/aS;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/dH;
.implements Ljava/util/SortedSet;


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final d:Lcom/google/common/collect/aR;


# instance fields
.field final transient a:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/common/collect/dh;->b()Lcom/google/common/collect/dh;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/aR;->b:Ljava/util/Comparator;

    new-instance v0, Lcom/google/common/collect/ae;

    sget-object v1, Lcom/google/common/collect/aR;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/google/common/collect/ae;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/common/collect/aR;->d:Lcom/google/common/collect/aR;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/aS;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    return-void
.end method

.method static a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    invoke-interface {p0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;
    .locals 1

    sget-object v0, Lcom/google/common/collect/aR;->b:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/aR;->h()Lcom/google/common/collect/aR;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/common/collect/ae;

    invoke-direct {v0, p0}, Lcom/google/common/collect/ae;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/google/common/collect/aR;
    .locals 2

    invoke-static {p0, p1}, Lcom/google/common/collect/dI;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/common/collect/aR;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/common/collect/aR;

    invoke-virtual {v0}, Lcom/google/common/collect/aR;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/common/collect/dI;->b(Ljava/util/Comparator;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/common/collect/dw;

    invoke-direct {v0, v1, p0}, Lcom/google/common/collect/dw;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/google/common/collect/aR;
    .locals 1

    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method private static h()Lcom/google/common/collect/aR;
    .locals 1

    sget-object v0, Lcom/google/common/collect/aR;->d:Lcom/google/common/collect/aR;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)I
.end method

.method abstract a(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
.end method

.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;
.end method

.method b(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, p2}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method abstract b(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
.end method

.method b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Lcom/google/common/collect/dY;
.end method

.method public c(Ljava/lang/Object;)Lcom/google/common/collect/aR;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aR;->c(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aR;
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/common/collect/aR;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method c(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Lcom/google/common/collect/aR;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aR;->d(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method d(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/common/collect/aR;->b(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aR;->c(Ljava/lang/Object;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aR;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aR;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aR;->d(Ljava/lang/Object;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method
