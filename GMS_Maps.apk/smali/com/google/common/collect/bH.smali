.class abstract enum Lcom/google/common/collect/bH;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/common/collect/bH;

.field public static final enum b:Lcom/google/common/collect/bH;

.field public static final enum c:Lcom/google/common/collect/bH;

.field public static final enum d:Lcom/google/common/collect/bH;

.field public static final enum e:Lcom/google/common/collect/bH;

.field private static final synthetic f:[Lcom/google/common/collect/bH;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/common/collect/bI;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bI;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bH;->a:Lcom/google/common/collect/bH;

    new-instance v0, Lcom/google/common/collect/bJ;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lcom/google/common/collect/bJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bH;->b:Lcom/google/common/collect/bH;

    new-instance v0, Lcom/google/common/collect/bK;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Lcom/google/common/collect/bK;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bH;->c:Lcom/google/common/collect/bH;

    new-instance v0, Lcom/google/common/collect/bL;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lcom/google/common/collect/bL;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bH;->d:Lcom/google/common/collect/bH;

    new-instance v0, Lcom/google/common/collect/bM;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lcom/google/common/collect/bM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bH;->e:Lcom/google/common/collect/bH;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/common/collect/bH;

    sget-object v1, Lcom/google/common/collect/bH;->a:Lcom/google/common/collect/bH;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/common/collect/bH;->b:Lcom/google/common/collect/bH;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/collect/bH;->c:Lcom/google/common/collect/bH;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/common/collect/bH;->d:Lcom/google/common/collect/bH;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/common/collect/bH;->e:Lcom/google/common/collect/bH;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/common/collect/bH;->f:[Lcom/google/common/collect/bH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/collect/bF;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/bH;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/bH;
    .locals 1

    const-class v0, Lcom/google/common/collect/bH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bH;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/bH;
    .locals 1

    sget-object v0, Lcom/google/common/collect/bH;->f:[Lcom/google/common/collect/bH;

    invoke-virtual {v0}, [Lcom/google/common/collect/bH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/bH;

    return-object v0
.end method
