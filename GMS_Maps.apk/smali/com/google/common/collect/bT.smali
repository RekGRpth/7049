.class abstract enum Lcom/google/common/collect/bT;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/common/collect/bT;

.field public static final enum b:Lcom/google/common/collect/bT;

.field public static final enum c:Lcom/google/common/collect/bT;

.field public static final enum d:Lcom/google/common/collect/bT;

.field public static final enum e:Lcom/google/common/collect/bT;

.field public static final enum f:Lcom/google/common/collect/bT;

.field public static final enum g:Lcom/google/common/collect/bT;

.field public static final enum h:Lcom/google/common/collect/bT;

.field public static final enum i:Lcom/google/common/collect/bT;

.field public static final enum j:Lcom/google/common/collect/bT;

.field public static final enum k:Lcom/google/common/collect/bT;

.field public static final enum l:Lcom/google/common/collect/bT;

.field static final m:[[Lcom/google/common/collect/bT;

.field private static final synthetic n:[Lcom/google/common/collect/bT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/common/collect/bU;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lcom/google/common/collect/bU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->a:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/bY;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Lcom/google/common/collect/bY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->b:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/bZ;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Lcom/google/common/collect/bZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->c:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/ca;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Lcom/google/common/collect/ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->d:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/cb;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v7}, Lcom/google/common/collect/cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->e:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/cc;

    const-string v1, "SOFT_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->f:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/cd;

    const-string v1, "SOFT_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->g:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/ce;

    const-string v1, "SOFT_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/ce;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->h:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/cf;

    const-string v1, "WEAK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->i:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/bV;

    const-string v1, "WEAK_EXPIRABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bV;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->j:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/bW;

    const-string v1, "WEAK_EVICTABLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bW;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->k:Lcom/google/common/collect/bT;

    new-instance v0, Lcom/google/common/collect/bX;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->l:Lcom/google/common/collect/bT;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/common/collect/bT;

    sget-object v1, Lcom/google/common/collect/bT;->a:Lcom/google/common/collect/bT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/collect/bT;->b:Lcom/google/common/collect/bT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/common/collect/bT;->c:Lcom/google/common/collect/bT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/common/collect/bT;->d:Lcom/google/common/collect/bT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/common/collect/bT;->e:Lcom/google/common/collect/bT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/common/collect/bT;->f:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/common/collect/bT;->g:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/common/collect/bT;->h:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/common/collect/bT;->i:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/common/collect/bT;->j:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/common/collect/bT;->k:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/common/collect/bT;->l:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/common/collect/bT;->n:[Lcom/google/common/collect/bT;

    new-array v0, v6, [[Lcom/google/common/collect/bT;

    new-array v1, v7, [Lcom/google/common/collect/bT;

    sget-object v2, Lcom/google/common/collect/bT;->a:Lcom/google/common/collect/bT;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/common/collect/bT;->b:Lcom/google/common/collect/bT;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/common/collect/bT;->c:Lcom/google/common/collect/bT;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/common/collect/bT;->d:Lcom/google/common/collect/bT;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v7, [Lcom/google/common/collect/bT;

    sget-object v2, Lcom/google/common/collect/bT;->e:Lcom/google/common/collect/bT;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/common/collect/bT;->f:Lcom/google/common/collect/bT;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/common/collect/bT;->g:Lcom/google/common/collect/bT;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/common/collect/bT;->h:Lcom/google/common/collect/bT;

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Lcom/google/common/collect/bT;

    sget-object v2, Lcom/google/common/collect/bT;->i:Lcom/google/common/collect/bT;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/common/collect/bT;->j:Lcom/google/common/collect/bT;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/common/collect/bT;->k:Lcom/google/common/collect/bT;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/common/collect/bT;->l:Lcom/google/common/collect/bT;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/common/collect/bT;->m:[[Lcom/google/common/collect/bT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/collect/bQ;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/bT;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/google/common/collect/cz;ZZ)Lcom/google/common/collect/bT;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    sget-object v1, Lcom/google/common/collect/bT;->m:[[Lcom/google/common/collect/bT;

    invoke-virtual {p0}, Lcom/google/common/collect/cz;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/bT;
    .locals 1

    const-class v0, Lcom/google/common/collect/bT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bT;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/bT;
    .locals 1

    sget-object v0, Lcom/google/common/collect/bT;->n:[Lcom/google/common/collect/bT;

    invoke-virtual {v0}, [Lcom/google/common/collect/bT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/bT;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/common/collect/ct;Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)Lcom/google/common/collect/cs;
    .locals 2

    invoke-interface {p2}, Lcom/google/common/collect/cs;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/common/collect/cs;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/google/common/collect/bT;->a(Lcom/google/common/collect/ct;Ljava/lang/Object;ILcom/google/common/collect/cs;)Lcom/google/common/collect/cs;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/google/common/collect/ct;Ljava/lang/Object;ILcom/google/common/collect/cs;)Lcom/google/common/collect/cs;
.end method

.method a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/common/collect/cs;->e()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lcom/google/common/collect/cs;->a(J)V

    invoke-interface {p1}, Lcom/google/common/collect/cs;->g()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/collect/bP;->a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    invoke-interface {p1}, Lcom/google/common/collect/cs;->f()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/collect/bP;->a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    invoke-static {p1}, Lcom/google/common/collect/bP;->d(Lcom/google/common/collect/cs;)V

    return-void
.end method

.method b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V
    .locals 1

    invoke-interface {p1}, Lcom/google/common/collect/cs;->i()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/collect/bP;->b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    invoke-interface {p1}, Lcom/google/common/collect/cs;->h()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/collect/bP;->b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    invoke-static {p1}, Lcom/google/common/collect/bP;->e(Lcom/google/common/collect/cs;)V

    return-void
.end method
