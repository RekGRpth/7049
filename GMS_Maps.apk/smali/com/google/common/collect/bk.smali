.class Lcom/google/common/collect/bk;
.super Ljava/util/AbstractSequentialList;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lcom/google/common/collect/bj;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/bk;->b:Lcom/google/common/collect/bj;

    iput-object p2, p0, Lcom/google/common/collect/bk;->a:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/util/AbstractSequentialList;-><init>()V

    return-void
.end method


# virtual methods
.method public listIterator(I)Ljava/util/ListIterator;
    .locals 3

    new-instance v0, Lcom/google/common/collect/bv;

    iget-object v1, p0, Lcom/google/common/collect/bk;->b:Lcom/google/common/collect/bj;

    iget-object v2, p0, Lcom/google/common/collect/bk;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/common/collect/bv;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;I)V

    return-object v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/bk;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/collect/aZ;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/bk;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/collect/aZ;->b(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bk;->b:Lcom/google/common/collect/bj;

    invoke-static {v0}, Lcom/google/common/collect/bj;->d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/bk;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/common/collect/cW;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
