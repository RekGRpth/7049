.class Lcom/google/common/collect/bp;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/common/collect/bj;


# direct methods
.method private constructor <init>(Lcom/google/common/collect/bj;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/bp;->a:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/bj;Lcom/google/common/collect/bk;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/bp;-><init>(Lcom/google/common/collect/bj;)V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Lcom/google/common/collect/bs;

    iget-object v1, p0, Lcom/google/common/collect/bp;->a:Lcom/google/common/collect/bj;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bs;-><init>(Lcom/google/common/collect/bj;Lcom/google/common/collect/bk;)V

    new-instance v1, Lcom/google/common/collect/bq;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/bq;-><init>(Lcom/google/common/collect/bp;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bp;->a:Lcom/google/common/collect/bj;

    invoke-static {v0}, Lcom/google/common/collect/bj;->d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
