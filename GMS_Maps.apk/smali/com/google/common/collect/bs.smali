.class Lcom/google/common/collect/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Set;

.field b:Lcom/google/common/collect/bt;

.field c:Lcom/google/common/collect/bt;

.field final synthetic d:Lcom/google/common/collect/bj;


# direct methods
.method private constructor <init>(Lcom/google/common/collect/bj;)V
    .locals 1

    iput-object p1, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    invoke-virtual {v0}, Lcom/google/common/collect/bj;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/dA;->a(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bs;->a:Ljava/util/Set;

    iget-object v0, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    invoke-static {v0}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/bj;Lcom/google/common/collect/bk;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/bs;-><init>(Lcom/google/common/collect/bj;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bs;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v1, v1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/bs;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iget-object v1, v1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    iget-object v1, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v1, v1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
