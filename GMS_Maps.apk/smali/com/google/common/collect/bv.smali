.class Lcom/google/common/collect/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final a:Ljava/lang/Object;

.field b:I

.field c:Lcom/google/common/collect/bt;

.field d:Lcom/google/common/collect/bt;

.field e:Lcom/google/common/collect/bt;

.field final synthetic f:Lcom/google/common/collect/bj;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V
    .locals 1

    iput-object p1, p0, Lcom/google/common/collect/bv;->f:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/common/collect/bv;->a:Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/collect/bj;->c(Lcom/google/common/collect/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    return-void
.end method

.method public constructor <init>(Lcom/google/common/collect/bj;Ljava/lang/Object;I)V
    .locals 2

    iput-object p1, p0, Lcom/google/common/collect/bv;->f:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/bj;->d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/common/collect/cW;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-static {p3, v1}, Lcom/google/common/base/J;->b(II)I

    div-int/lit8 v0, v1, 0x2

    if-lt p3, v0, :cond_0

    invoke-static {p1}, Lcom/google/common/collect/bj;->e(Lcom/google/common/collect/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iput v1, p0, Lcom/google/common/collect/bv;->b:I

    :goto_0
    add-int/lit8 v0, p3, 0x1

    if-ge p3, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/common/collect/bv;->previous()Ljava/lang/Object;

    move p3, v0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/bj;->c(Lcom/google/common/collect/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    :goto_1
    add-int/lit8 v0, p3, -0x1

    if-lez p3, :cond_1

    invoke-virtual {p0}, Lcom/google/common/collect/bv;->next()Ljava/lang/Object;

    move p3, v0

    goto :goto_1

    :cond_1
    iput-object p2, p0, Lcom/google/common/collect/bv;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/bv;->f:Lcom/google/common/collect/bj;

    iget-object v1, p0, Lcom/google/common/collect/bv;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    invoke-static {v0, v1, p1, v2}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bv;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/common/collect/bv;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    return-void
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrevious()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bv;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/common/collect/bv;->b:I

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public nextIndex()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/bv;->b:I

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bv;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/bv;->b:I

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/bv;->b:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iget-object v1, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->e:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bv;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/bv;->b:I

    :goto_1
    iget-object v0, p0, Lcom/google/common/collect/bv;->f:Lcom/google/common/collect/bj;

    iget-object v1, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    invoke-static {v0, v1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;Lcom/google/common/collect/bt;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bv;->c:Lcom/google/common/collect/bt;

    goto :goto_1
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/bv;->d:Lcom/google/common/collect/bt;

    iput-object p1, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
