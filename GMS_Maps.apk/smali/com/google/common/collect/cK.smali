.class final Lcom/google/common/collect/cK;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/common/collect/bP;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bP;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/cK;->a:Lcom/google/common/collect/bP;

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cK;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Lcom/google/common/collect/bP;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cK;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/bP;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cK;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Lcom/google/common/collect/bP;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lcom/google/common/collect/cI;

    iget-object v1, p0, Lcom/google/common/collect/cK;->a:Lcom/google/common/collect/bP;

    invoke-direct {v0, v1}, Lcom/google/common/collect/cI;-><init>(Lcom/google/common/collect/bP;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cK;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Lcom/google/common/collect/bP;->size()I

    move-result v0

    return v0
.end method
