.class abstract Lcom/google/common/collect/co;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field b:I

.field c:I

.field d:Lcom/google/common/collect/ct;

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field f:Lcom/google/common/collect/cs;

.field g:Lcom/google/common/collect/cQ;

.field h:Lcom/google/common/collect/cQ;

.field final synthetic i:Lcom/google/common/collect/bP;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bP;)V
    .locals 1

    iput-object p1, p0, Lcom/google/common/collect/co;->i:Lcom/google/common/collect/bP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/co;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/common/collect/co;->c:I

    invoke-virtual {p0}, Lcom/google/common/collect/co;->b()V

    return-void
.end method


# virtual methods
.method a(Lcom/google/common/collect/cs;)Z
    .locals 4

    :try_start_0
    invoke-interface {p1}, Lcom/google/common/collect/cs;->d()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/co;->i:Lcom/google/common/collect/bP;

    invoke-virtual {v1, p1}, Lcom/google/common/collect/bP;->b(Lcom/google/common/collect/cs;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/google/common/collect/cQ;

    iget-object v3, p0, Lcom/google/common/collect/co;->i:Lcom/google/common/collect/bP;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/common/collect/cQ;-><init>(Lcom/google/common/collect/bP;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/common/collect/co;->g:Lcom/google/common/collect/cQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/common/collect/co;->d:Lcom/google/common/collect/ct;

    invoke-virtual {v1}, Lcom/google/common/collect/ct;->n()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/common/collect/co;->d:Lcom/google/common/collect/ct;

    invoke-virtual {v1}, Lcom/google/common/collect/ct;->n()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/common/collect/co;->d:Lcom/google/common/collect/ct;

    invoke-virtual {v1}, Lcom/google/common/collect/ct;->n()V

    throw v0
.end method

.method final b()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/co;->g:Lcom/google/common/collect/cQ;

    invoke-virtual {p0}, Lcom/google/common/collect/co;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/common/collect/co;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget v0, p0, Lcom/google/common/collect/co;->b:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/common/collect/co;->i:Lcom/google/common/collect/bP;

    iget-object v0, v0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    iget v1, p0, Lcom/google/common/collect/co;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/common/collect/co;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/common/collect/co;->d:Lcom/google/common/collect/ct;

    iget-object v0, p0, Lcom/google/common/collect/co;->d:Lcom/google/common/collect/ct;

    iget v0, v0, Lcom/google/common/collect/ct;->b:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/common/collect/co;->d:Lcom/google/common/collect/ct;

    iget-object v0, v0, Lcom/google/common/collect/ct;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lcom/google/common/collect/co;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, Lcom/google/common/collect/co;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/co;->c:I

    invoke-virtual {p0}, Lcom/google/common/collect/co;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    invoke-interface {v0}, Lcom/google/common/collect/cs;->b()Lcom/google/common/collect/cs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    :goto_0
    iget-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/co;->a(Lcom/google/common/collect/cs;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    invoke-interface {v0}, Lcom/google/common/collect/cs;->b()Lcom/google/common/collect/cs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method d()Z
    .locals 3

    :cond_0
    iget v0, p0, Lcom/google/common/collect/co;->c:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/common/collect/co;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lcom/google/common/collect/co;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/common/collect/co;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cs;

    iput-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/common/collect/co;->f:Lcom/google/common/collect/cs;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/co;->a(Lcom/google/common/collect/cs;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/common/collect/co;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Lcom/google/common/collect/cQ;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/co;->g:Lcom/google/common/collect/cQ;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/co;->g:Lcom/google/common/collect/cQ;

    iput-object v0, p0, Lcom/google/common/collect/co;->h:Lcom/google/common/collect/cQ;

    invoke-virtual {p0}, Lcom/google/common/collect/co;->b()V

    iget-object v0, p0, Lcom/google/common/collect/co;->h:Lcom/google/common/collect/cQ;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/co;->g:Lcom/google/common/collect/cQ;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/co;->h:Lcom/google/common/collect/cQ;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/co;->i:Lcom/google/common/collect/bP;

    iget-object v1, p0, Lcom/google/common/collect/co;->h:Lcom/google/common/collect/cQ;

    invoke-virtual {v1}, Lcom/google/common/collect/cQ;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/bP;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/co;->h:Lcom/google/common/collect/cQ;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
