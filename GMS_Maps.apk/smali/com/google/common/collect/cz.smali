.class abstract enum Lcom/google/common/collect/cz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/common/collect/cz;

.field public static final enum b:Lcom/google/common/collect/cz;

.field public static final enum c:Lcom/google/common/collect/cz;

.field private static final synthetic d:[Lcom/google/common/collect/cz;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/common/collect/cA;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cA;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/cz;->a:Lcom/google/common/collect/cz;

    new-instance v0, Lcom/google/common/collect/cB;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Lcom/google/common/collect/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/cz;->b:Lcom/google/common/collect/cz;

    new-instance v0, Lcom/google/common/collect/cC;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lcom/google/common/collect/cC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/cz;->c:Lcom/google/common/collect/cz;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/common/collect/cz;

    sget-object v1, Lcom/google/common/collect/cz;->a:Lcom/google/common/collect/cz;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/common/collect/cz;->b:Lcom/google/common/collect/cz;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/collect/cz;->c:Lcom/google/common/collect/cz;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/common/collect/cz;->d:[Lcom/google/common/collect/cz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/collect/bQ;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/cz;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/cz;
    .locals 1

    const-class v0, Lcom/google/common/collect/cz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cz;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/cz;
    .locals 1

    sget-object v0, Lcom/google/common/collect/cz;->d:[Lcom/google/common/collect/cz;

    invoke-virtual {v0}, [Lcom/google/common/collect/cz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/cz;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/google/common/base/t;
.end method

.method abstract a(Lcom/google/common/collect/ct;Lcom/google/common/collect/cs;Ljava/lang/Object;)Lcom/google/common/collect/cJ;
.end method
