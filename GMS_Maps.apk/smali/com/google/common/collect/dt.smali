.class Lcom/google/common/collect/dt;
.super Lcom/google/common/collect/ar;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/common/collect/dn;


# direct methods
.method constructor <init>(Lcom/google/common/collect/dn;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/ar;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/dt;->a:Lcom/google/common/collect/dn;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()Lcom/google/common/collect/dY;
    .locals 2

    new-instance v0, Lcom/google/common/collect/du;

    iget-object v1, p0, Lcom/google/common/collect/dt;->a:Lcom/google/common/collect/dn;

    invoke-static {v1}, Lcom/google/common/collect/dn;->a(Lcom/google/common/collect/dn;)[Lcom/google/common/collect/dq;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/du;-><init>(Lcom/google/common/collect/dt;I)V

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dt;->a:Lcom/google/common/collect/dn;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/dn;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/dt;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dt;->a:Lcom/google/common/collect/dn;

    invoke-static {v0}, Lcom/google/common/collect/dn;->a(Lcom/google/common/collect/dn;)[Lcom/google/common/collect/dq;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
