.class abstract Lcom/google/common/collect/q;
.super Lcom/google/common/collect/M;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1f3c5464cd7009c6L


# instance fields
.field private transient a:Ljava/util/Map;

.field private transient b:J


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/common/collect/M;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-super {p0}, Lcom/google/common/collect/M;->size()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    return-void
.end method

.method static synthetic a(Lcom/google/common/collect/q;Ljava/lang/Object;Ljava/util/Map;)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/q;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/Map;)I
    .locals 5

    const/4 v1, 0x0

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/common/collect/Y;->d(I)I

    move-result v0

    iget-wide v1, p0, Lcom/google/common/collect/q;->b:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/common/collect/q;->b:J

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/common/collect/q;J)J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/common/collect/q;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/common/collect/q;)J
    .locals 4

    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/google/common/collect/q;->b:J

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    move v1, v0

    :goto_1
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Ljava/lang/Object;I)I
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/common/collect/q;->a(Ljava/lang/Object;)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    if-lez p2, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    new-instance v1, Lcom/google/common/collect/Y;

    invoke-direct {v1, p2}, Lcom/google/common/collect/Y;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    int-to-long v3, p2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I

    move-result v4

    int-to-long v5, v4

    int-to-long v7, p2

    add-long/2addr v5, v7

    const-wide/32 v7, 0x7fffffff

    cmp-long v3, v5, v7

    if-gtz v3, :cond_3

    move v3, v1

    :goto_3
    const-string v7, "too many occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v3, v7, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, p2}, Lcom/google/common/collect/Y;->a(I)I

    move v2, v4

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_3
.end method

.method public a()Ljava/util/Set;
    .locals 1

    invoke-super {p0}, Lcom/google/common/collect/M;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;I)I
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/q;->a(Ljava/lang/Object;)I

    move-result v2

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-lez p2, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I

    move-result v1

    if-le v1, p2, :cond_3

    :goto_2
    neg-int v2, p2

    invoke-virtual {v0, v2}, Lcom/google/common/collect/Y;->b(I)I

    iget-wide v2, p0, Lcom/google/common/collect/q;->b:J

    int-to-long v4, p2

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/common/collect/q;->b:J

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move p2, v1

    goto :goto_2
.end method

.method b()Ljava/util/Iterator;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Lcom/google/common/collect/r;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/r;-><init>(Lcom/google/common/collect/q;Ljava/util/Iterator;)V

    return-object v1
.end method

.method c()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/common/collect/Y;->c(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    return-void
.end method

.method d()Ljava/util/Set;
    .locals 2

    new-instance v0, Lcom/google/common/collect/t;

    iget-object v1, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/t;-><init>(Lcom/google/common/collect/q;Ljava/util/Map;)V

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    new-instance v0, Lcom/google/common/collect/v;

    invoke-direct {v0, p0}, Lcom/google/common/collect/v;-><init>(Lcom/google/common/collect/q;)V

    return-object v0
.end method

.method public size()I
    .locals 2

    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    invoke-static {v0, v1}, Lac/a;->a(J)I

    move-result v0

    return v0
.end method
