.class Lcom/google/common/collect/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Ljava/util/Map$Entry;

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/google/common/collect/q;


# direct methods
.method constructor <init>(Lcom/google/common/collect/q;Ljava/util/Iterator;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/r;->c:Lcom/google/common/collect/q;

    iput-object p2, p0, Lcom/google/common/collect/r;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/cX;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/r;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/google/common/collect/r;->a:Ljava/util/Map$Entry;

    new-instance v1, Lcom/google/common/collect/s;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/s;-><init>(Lcom/google/common/collect/r;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/r;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/r;->a()Lcom/google/common/collect/cX;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/common/collect/r;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "no calls to next() since the last call to remove()"

    invoke-static {v0, v2}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/common/collect/r;->c:Lcom/google/common/collect/q;

    iget-object v0, p0, Lcom/google/common/collect/r;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/Y;->d(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v0, v1}, Lcom/google/common/collect/q;->a(Lcom/google/common/collect/q;J)J

    iget-object v0, p0, Lcom/google/common/collect/r;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/r;->a:Ljava/util/Map$Entry;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
