.class public Lcom/google/googlenav/Q;
.super Law/a;
.source "SourceFile"


# static fields
.field static a:Z

.field private static final b:Ljava/lang/Object;

.field private static c:I

.field private static d:J

.field private static e:Lcom/google/googlenav/S;

.field private static final f:Ljava/util/Hashtable;


# instance fields
.field private final g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/Q;->a:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/Q;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/google/googlenav/Q;->f:Ljava/util/Hashtable;

    return-void
.end method

.method constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 11

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/Q;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/googlenav/Q;->e:Lcom/google/googlenav/S;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/googlenav/Q;->e:Lcom/google/googlenav/S;

    invoke-interface {v2, p0}, Lcom/google/googlenav/S;->a(Lcom/google/googlenav/Q;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/Q;->i()J

    move-result-wide v4

    sget-object v6, Lcom/google/googlenav/Q;->b:Ljava/lang/Object;

    monitor-enter v6

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/Q;->h:Z

    :goto_0
    sget v0, Lcom/google/googlenav/Q;->c:I

    if-nez v0, :cond_1

    sput-wide v4, Lcom/google/googlenav/Q;->d:J

    :cond_1
    sget v0, Lcom/google/googlenav/Q;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/googlenav/Q;->c:I

    monitor-exit v6

    return-void

    :cond_2
    sget v2, Lcom/google/googlenav/Q;->c:I

    const/16 v3, 0xa

    if-lt v2, v3, :cond_5

    move v3, v1

    :goto_1
    sget v2, Lcom/google/googlenav/Q;->c:I

    if-lez v2, :cond_6

    sget-wide v7, Lcom/google/googlenav/Q;->d:J

    sub-long v7, v4, v7

    const-wide/16 v9, 0x7530

    cmp-long v2, v7, v9

    if-ltz v2, :cond_6

    move v2, v1

    :goto_2
    if-nez v3, :cond_3

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    iput-boolean v0, p0, Lcom/google/googlenav/Q;->h:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    move v3, v0

    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2
.end method

.method public static a(I)Z
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/Q;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/Q;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic b(I)Z
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/Q;->c(I)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0xa -> :sswitch_0
        0xe -> :sswitch_0
        0x1d -> :sswitch_0
    .end sparse-switch
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 4

    invoke-static {p0}, Lcom/google/googlenav/Q;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/googlenav/Q;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/google/googlenav/Q;->f:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sget-object v3, Lcom/google/googlenav/Q;->f:Ljava/util/Hashtable;

    invoke-virtual {v3, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, v2, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/Q;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/Q;->f:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x6

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/Q;->i:Z

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/googlenav/Q;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/google/googlenav/Q;->c:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/googlenav/Q;->c:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/Q;->i:Z

    iget-object v0, p0, Lcom/google/googlenav/Q;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dV;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    return v0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x4a

    return v0
.end method

.method protected i()J
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public s_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/Q;->h:Z

    return v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
