.class public Lcom/google/googlenav/R;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/util/List;

.field private m:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/R;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/R;->b:Ljava/lang/Integer;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->m:Ljava/lang/Integer;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->l:Ljava/util/List;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/R;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/R;->f:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x1

    sget-boolean v1, Lcom/google/googlenav/Q;->a:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/R;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    move v1, v2

    :goto_1
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/wireless/googlenav/proto/j2me/dV;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v4, p0, Lcom/google/googlenav/R;->a:Ljava/lang/String;

    if-eqz v4, :cond_2

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/googlenav/R;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v4, p0, Lcom/google/googlenav/R;->b:Ljava/lang/Integer;

    if-eqz v4, :cond_3

    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/googlenav/R;->b:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v4, p0, Lcom/google/googlenav/R;->c:Ljava/lang/String;

    if-eqz v4, :cond_4

    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/googlenav/R;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    iget-object v4, p0, Lcom/google/googlenav/R;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v3}, Lcom/google/googlenav/Q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-static {v4}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Date;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v4, p0, Lcom/google/googlenav/R;->e:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/googlenav/R;->e:Ljava/lang/String;

    invoke-virtual {v3, v7, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    iget-object v4, p0, Lcom/google/googlenav/R;->f:Ljava/lang/Boolean;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/googlenav/R;->f:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_6
    iget-object v4, p0, Lcom/google/googlenav/R;->g:Ljava/lang/String;

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/googlenav/R;->h:Ljava/lang/String;

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/googlenav/R;->i:Ljava/lang/String;

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/googlenav/R;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/googlenav/R;->m:Ljava/lang/Integer;

    if-eqz v4, :cond_d

    :cond_7
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/dV;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v5, 0x7

    invoke-virtual {v3, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v5, p0, Lcom/google/googlenav/R;->g:Ljava/lang/String;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/googlenav/R;->g:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_8
    iget-object v5, p0, Lcom/google/googlenav/R;->h:Ljava/lang/String;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/googlenav/R;->h:Ljava/lang/String;

    invoke-virtual {v4, v7, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_9
    iget-object v5, p0, Lcom/google/googlenav/R;->i:Ljava/lang/String;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/google/googlenav/R;->i:Ljava/lang/String;

    invoke-virtual {v4, v8, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_a
    iget-object v5, p0, Lcom/google/googlenav/R;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/google/googlenav/R;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4, v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_b
    iget-object v2, p0, Lcom/google/googlenav/R;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    const/4 v2, 0x5

    iget-object v5, p0, Lcom/google/googlenav/R;->k:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v4, v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_c
    iget-object v2, p0, Lcom/google/googlenav/R;->m:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    const/4 v2, 0x6

    iget-object v5, p0, Lcom/google/googlenav/R;->m:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_d
    iget-object v2, p0, Lcom/google/googlenav/R;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/Q;->b(I)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/googlenav/R;->l:Ljava/util/List;

    if-eqz v2, :cond_e

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/R;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/googlenav/R;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :sswitch_0
    move v1, v0

    goto/16 :goto_1

    :cond_e
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/Q;

    invoke-direct {v2, v3, v1}, Lcom/google/googlenav/Q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    iget-object v0, p0, Lcom/google/googlenav/R;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v6, :cond_0

    invoke-static {v3}, Lcom/google/googlenav/Q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method public b(I)Lcom/google/googlenav/R;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/R;->d:Ljava/lang/Integer;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->c:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/R;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/R;->k:Ljava/lang/Boolean;

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->e:Ljava/lang/String;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->g:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->h:Ljava/lang/String;

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/google/googlenav/R;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/R;->i:Ljava/lang/String;

    return-object p0
.end method
