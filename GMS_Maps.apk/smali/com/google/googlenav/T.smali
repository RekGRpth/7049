.class public Lcom/google/googlenav/T;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/F;


# static fields
.field private static final v:Ljava/util/Comparator;


# instance fields
.field private a:LaN/o;

.field private final b:LaN/p;

.field private final c:LaN/u;

.field private final d:Lcom/google/googlenav/layer/m;

.field private e:Ljava/lang/String;

.field private f:B

.field private g:B

.field private h:Ljava/util/Vector;

.field private i:I

.field private final j:Ljava/util/Hashtable;

.field private final k:Ljava/util/Hashtable;

.field private final l:Ljava/util/Hashtable;

.field private m:Lcom/google/googlenav/W;

.field private n:LaN/P;

.field private o:LaN/n;

.field private p:LaN/H;

.field private q:[LaN/P;

.field private r:Lcom/google/googlenav/V;

.field private s:I

.field private t:[Lcom/google/googlenav/W;

.field private u:Lbf/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/U;

    invoke-direct {v0}, Lcom/google/googlenav/U;-><init>()V

    sput-object v0, Lcom/google/googlenav/T;->v:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/googlenav/T;->i:I

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    iput-object p2, p0, Lcom/google/googlenav/T;->a:LaN/o;

    iput-object p3, p0, Lcom/google/googlenav/T;->b:LaN/p;

    iput-object p4, p0, Lcom/google/googlenav/T;->c:LaN/u;

    iput-object p1, p0, Lcom/google/googlenav/T;->d:Lcom/google/googlenav/layer/m;

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/T;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->f()B

    move-result v0

    iput-byte v0, p0, Lcom/google/googlenav/T;->f:B

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    iput v1, p0, Lcom/google/googlenav/T;->i:I

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/T;->k:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/T;->l:Ljava/util/Hashtable;

    invoke-virtual {p4}, LaN/u;->f()LaN/H;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/T;->p:LaN/H;

    invoke-virtual {p3}, LaN/p;->g()[LaN/P;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/T;->q:[LaN/P;

    return-void
.end method

.method private a(Ljava/util/Vector;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/T;->l:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/T;->l:Ljava/util/Hashtable;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Lcom/google/googlenav/W;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v0

    invoke-interface {v0}, LaN/g;->b()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v1

    invoke-interface {v1}, LaN/g;->b()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()B
    .locals 2

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/googlenav/T;->d:Lcom/google/googlenav/layer/m;

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->b:LaN/p;

    invoke-virtual {v0}, LaN/p;->k()B

    move-result v0

    :cond_0
    return v0
.end method

.method private j()Ljava/util/Vector;
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/T;->b:LaN/p;

    invoke-virtual {v0}, LaN/p;->g()[LaN/P;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/T;->i()B

    move-result v2

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v4, p0, Lcom/google/googlenav/T;->a:LaN/o;

    aget-object v5, v1, v0

    invoke-static {v2, v5}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v5

    invoke-interface {v4, v5}, LaN/o;->a(LaN/P;)LaN/n;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, LaN/n;->e()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v3, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private k()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/T;->r:Lcom/google/googlenav/V;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->p:LaN/H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->q:[LaN/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->p:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/T;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->p:LaN/H;

    invoke-virtual {v0}, LaN/H;->b()LaN/Y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/T;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/T;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->r:Lcom/google/googlenav/V;

    invoke-direct {p0}, Lcom/google/googlenav/T;->m()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/V;->a(Z)V

    iput-object v2, p0, Lcom/google/googlenav/T;->r:Lcom/google/googlenav/V;

    iput-object v2, p0, Lcom/google/googlenav/T;->p:LaN/H;

    iput-object v2, p0, Lcom/google/googlenav/T;->q:[LaN/P;

    :cond_0
    return-void
.end method

.method private l()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/T;->q:[LaN/P;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/T;->i()B

    move-result v2

    iget-object v1, p0, Lcom/google/googlenav/T;->q:[LaN/P;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_4

    iget-object v3, p0, Lcom/google/googlenav/T;->q:[LaN/P;

    aget-object v3, v3, v1

    invoke-virtual {v3}, LaN/P;->j()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/googlenav/T;->b:LaN/p;

    invoke-virtual {v4, v3}, LaN/p;->a(LaN/P;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/T;->a:LaN/o;

    invoke-static {v2, v3}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v3

    invoke-interface {v4, v3}, LaN/o;->a(LaN/P;)LaN/n;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, LaN/n;->e()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private m()Z
    .locals 10

    invoke-direct {p0}, Lcom/google/googlenav/T;->j()Ljava/util/Vector;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    iget-object v5, p0, Lcom/google/googlenav/T;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v6, v3

    int-to-long v8, v1

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v0, v5, v6, v7}, LaN/n;->a(Ljava/lang/String;J)Ljava/util/Hashtable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    iget-object v6, p0, Lcom/google/googlenav/T;->c:LaN/u;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v6, v0}, LaN/u;->d(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private n()V
    .locals 3

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/T;->s:I

    iget-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v2}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput v0, p0, Lcom/google/googlenav/T;->s:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(LaN/B;)Ljava/util/Enumeration;
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/T;->i()B

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/T;->c:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-static {v1, p1, v2}, LaN/P;->a(BLaN/B;LaN/Y;)LaN/P;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/T;->n:LaN/P;

    invoke-virtual {v1, v2}, LaN/P;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v1, p0, Lcom/google/googlenav/T;->n:LaN/P;

    iput-object v0, p0, Lcom/google/googlenav/T;->o:LaN/n;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/T;->o:LaN/n;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/T;->o:LaN/n;

    invoke-virtual {v1}, LaN/n;->e()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/T;->a:LaN/o;

    iget-object v2, p0, Lcom/google/googlenav/T;->n:LaN/P;

    invoke-interface {v1, v2}, LaN/o;->a(LaN/P;)LaN/n;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/T;->o:LaN/n;

    iget-object v1, p0, Lcom/google/googlenav/T;->o:LaN/n;

    if-nez v1, :cond_3

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/googlenav/T;->o:LaN/n;

    iget-object v4, p0, Lcom/google/googlenav/T;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v1, v2}, LaN/n;->a(Ljava/lang/String;J)Ljava/util/Hashtable;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 11

    invoke-direct {p0}, Lcom/google/googlenav/T;->k()V

    iget-object v0, p0, Lcom/google/googlenav/T;->d:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->d:Lcom/google/googlenav/layer/m;

    iget-object v1, p0, Lcom/google/googlenav/T;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/m;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/T;->i:I

    iget-object v0, p0, Lcom/google/googlenav/T;->l:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/T;->j()Ljava/util/Vector;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v5

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_6

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    iget-object v1, p0, Lcom/google/googlenav/T;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v7, v5

    int-to-long v9, v2

    sub-long/2addr v7, v9

    const-wide/16 v9, 0x1

    sub-long/2addr v7, v9

    invoke-virtual {v0, v1, v7, v8}, LaN/n;->a(Ljava/lang/String;J)Ljava/util/Hashtable;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v7

    :cond_3
    :goto_2
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    iget-object v1, p0, Lcom/google/googlenav/T;->k:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/W;

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/google/googlenav/W;->l()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/googlenav/W;->a(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->m()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/googlenav/W;->b(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->n()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/googlenav/W;->c(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->c(LaN/g;)V

    :goto_3
    iget-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-byte v0, p0, Lcom/google/googlenav/T;->f:B

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(B)V

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    iget-object v8, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v9, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/google/googlenav/T;->b(Lcom/google/googlenav/W;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v1}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/W;->c(LaN/g;)V

    goto :goto_2

    :cond_5
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto/16 :goto_1

    :cond_6
    invoke-direct {p0, v4}, Lcom/google/googlenav/T;->a(Ljava/util/Vector;)V

    iput-object v4, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/T;->i:I

    iget-object v0, p0, Lcom/google/googlenav/T;->u:Lbf/k;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/T;->u:Lbf/k;

    invoke-interface {v1, v0}, Lbf/k;->c(Lbf/i;)V

    goto/16 :goto_0

    :cond_7
    move-object v1, v0

    goto :goto_3
.end method

.method public a(B)V
    .locals 0

    iput-byte p1, p0, Lcom/google/googlenav/T;->g:B

    return-void
.end method

.method public a(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/T;->f()I

    move-result v0

    if-lt p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/googlenav/T;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/googlenav/W;

    iput-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/T;->n()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/V;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/T;->r:Lcom/google/googlenav/V;

    return-void
.end method

.method public a(Lcom/google/googlenav/W;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/T;->k:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/T;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/googlenav/T;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/T;->i:I

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/W;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/W;->l()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->a(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->m()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->b(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->n()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->c(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->o()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->d(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->p()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->e(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->g(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->a(Ljava/util/List;)V

    invoke-virtual {p2}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/W;->b(LaN/g;)V

    invoke-virtual {v0}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/W;->c(LaN/g;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p2, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/T;->k:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/T;->j:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, p2, v0}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public b(I)Lcom/google/googlenav/E;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/E;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v0

    invoke-interface {v0}, LaN/g;->b()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-direct {p0, v0}, Lcom/google/googlenav/T;->b(Lcom/google/googlenav/W;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->D()LaN/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->c(LaN/g;)V

    goto :goto_0
.end method

.method public b(LaN/B;)V
    .locals 4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/T;->s:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/W;

    iput-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2, p1}, LaN/B;->a(LaN/B;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/W;->b(J)V

    iget-object v2, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    sget-object v1, Lcom/google/googlenav/T;->v:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-direct {p0}, Lcom/google/googlenav/T;->n()V

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public c(I)I
    .locals 0

    return p1
.end method

.method public d()B
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/T;->g:B

    return v0
.end method

.method public e()Lcom/google/googlenav/E;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/T;->k:Ljava/util/Hashtable;

    iget-object v1, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/T;->m:Lcom/google/googlenav/W;

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/T;->i:I

    return v0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/T;->k:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/T;->l:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/T;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/T;->i:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    return-void
.end method

.method public h()[Lcom/google/googlenav/W;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/T;->t:[Lcom/google/googlenav/W;

    return-object v0
.end method
