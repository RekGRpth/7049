.class Lcom/google/googlenav/Z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/cd;


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:[Lcom/google/googlenav/bZ;

.field private final c:[Lcom/google/googlenav/bZ;

.field private final d:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/bZ;[Lcom/google/googlenav/bZ;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/Z;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p2, p0, Lcom/google/googlenav/Z;->b:[Lcom/google/googlenav/bZ;

    iput-object p3, p0, Lcom/google/googlenav/Z;->c:[Lcom/google/googlenav/bZ;

    iput-object p4, p0, Lcom/google/googlenav/Z;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/googlenav/Z;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/Z;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0
.end method

.method public b(I)Lcom/google/googlenav/ab;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/Z;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    return-object v0
.end method
