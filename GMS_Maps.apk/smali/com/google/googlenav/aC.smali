.class public Lcom/google/googlenav/aC;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;


# static fields
.field private static final a:Lcom/google/googlenav/aF;


# instance fields
.field private b:Ljava/util/concurrent/ConcurrentMap;

.field private c:Lcom/google/googlenav/aE;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/googlenav/aF;

    invoke-direct {v0, v1, v1}, Lcom/google/googlenav/aF;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cx;)V

    sput-object v0, Lcom/google/googlenav/aC;->a:Lcom/google/googlenav/aF;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, LY/e;->a()LY/e;

    move-result-object v0

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, LY/e;->a(J)LY/e;

    move-result-object v0

    invoke-virtual {v0}, LY/e;->o()LY/d;

    move-result-object v0

    invoke-interface {v0}, LY/d;->a()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/aD;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/aC;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/googlenav/aC;
    .locals 1

    sget-object v0, Lcom/google/googlenav/aG;->a:Lcom/google/googlenav/aC;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/googlenav/aF;
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aF;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 5

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/aF;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bl()Lcom/google/googlenav/cx;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/aF;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cx;)V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/aC;->c:Lcom/google/googlenav/aE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aC;->c:Lcom/google/googlenav/aE;

    invoke-interface {v0}, Lcom/google/googlenav/aE;->a()V

    :cond_0
    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/aC;->a:Lcom/google/googlenav/aF;

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/aC;->a:Lcom/google/googlenav/aF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/google/googlenav/ai;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aF;

    iget-object v0, v0, Lcom/google/googlenav/aF;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ai;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/aC;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aF;

    iget-object v3, v0, Lcom/google/googlenav/aF;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/google/googlenav/aF;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/util/l;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
