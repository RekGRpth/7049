.class public Lcom/google/googlenav/aZ;
.super Law/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/F;


# static fields
.field private static final F:Lbm/i;

.field public static a:Z

.field private static final aa:Lcom/google/common/collect/ImmutableList;

.field private static c:Z

.field private static final d:Ljava/util/regex/Pattern;

.field private static e:I


# instance fields
.field private volatile A:Z

.field private B:I

.field private C:Ljava/lang/String;

.field private D:I

.field private E:B

.field private G:I

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Z

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Lcom/google/googlenav/layer/m;

.field private P:Lcom/google/googlenav/layer/a;

.field private Q:Z

.field private R:[I

.field private final S:Ljava/util/HashSet;

.field private T:Z

.field private U:[Lcom/google/googlenav/ai;

.field private V:I

.field private W:Lcom/google/googlenav/bb;

.field private X:J

.field private Y:[Ljava/lang/String;

.field private Z:I

.field private ab:[Lcom/google/googlenav/bc;

.field private ac:[Lcom/google/googlenav/be;

.field private ad:[LaW/R;

.field private ae:Z

.field private af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private ag:I

.field private ah:I

.field private b:J

.field private f:[Lcom/google/googlenav/ai;

.field private g:[I

.field private h:Lcom/google/googlenav/bf;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z

.field private p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:LaN/H;

.field private v:LaN/B;

.field private w:I

.field private x:I

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/aZ;->a:Z

    sput-boolean v0, Lcom/google/googlenav/aZ;->c:Z

    const-string v0, "(.*) cid:\\d+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/aZ;->d:Ljava/util/regex/Pattern;

    const/4 v0, -0x1

    sput v0, Lcom/google/googlenav/aZ;->e:I

    new-instance v0, Lbm/i;

    const-string v1, "search"

    const-string v2, "s"

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    new-instance v0, Lcom/google/common/collect/aw;

    invoke-direct {v0}, Lcom/google/common/collect/aw;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    invoke-virtual {v0}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/aZ;->aa:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Law/a;-><init>()V

    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->o:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->q:I

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->A:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->D:I

    iput-byte v2, p0, Lcom/google/googlenav/aZ;->E:B

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->L:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->Q:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->T:Z

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    iput v3, p0, Lcom/google/googlenav/aZ;->Z:I

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->ae:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->ag:I

    iput v3, p0, Lcom/google/googlenav/aZ;->ah:I

    iput v2, p0, Lcom/google/googlenav/aZ;->G:I

    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Law/a;-><init>()V

    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->o:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->q:I

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    iput-boolean v4, p0, Lcom/google/googlenav/aZ;->s:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->A:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->D:I

    iput-byte v2, p0, Lcom/google/googlenav/aZ;->E:B

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->L:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->Q:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->T:Z

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    iput v3, p0, Lcom/google/googlenav/aZ;->Z:I

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->ae:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->ag:I

    iput v3, p0, Lcom/google/googlenav/aZ;->ah:I

    sget-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    iput-object p2, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iput p3, p0, Lcom/google/googlenav/aZ;->G:I

    iget-object v0, p1, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iget v0, p1, Lcom/google/googlenav/aZ;->j:I

    iput v0, p0, Lcom/google/googlenav/aZ;->j:I

    iget-object v0, p1, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/aZ;->u:LaN/H;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    iget v0, p1, Lcom/google/googlenav/aZ;->x:I

    iput v0, p0, Lcom/google/googlenav/aZ;->x:I

    iget v0, p1, Lcom/google/googlenav/aZ;->w:I

    iput v0, p0, Lcom/google/googlenav/aZ;->w:I

    iget v0, p1, Lcom/google/googlenav/aZ;->y:I

    iput v0, p0, Lcom/google/googlenav/aZ;->y:I

    iget-object v0, p1, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->T:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->T:Z

    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->r:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->r:Z

    iget-object v0, p1, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->o:Z

    iput-boolean v4, p0, Lcom/google/googlenav/aZ;->Q:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/bf;LaN/u;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Law/a;-><init>()V

    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->o:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->q:I

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->A:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->D:I

    iput-byte v2, p0, Lcom/google/googlenav/aZ;->E:B

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->L:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->Q:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->T:Z

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    iput v3, p0, Lcom/google/googlenav/aZ;->Z:I

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->ae:Z

    iput v3, p0, Lcom/google/googlenav/aZ;->ag:I

    iput v3, p0, Lcom/google/googlenav/aZ;->ah:I

    sget-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    iput-object p1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v0, p1, Lcom/google/googlenav/bf;->r:I

    iput v0, p0, Lcom/google/googlenav/aZ;->G:I

    iget-object v0, p1, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iget v0, p1, Lcom/google/googlenav/bf;->b:I

    iput v0, p0, Lcom/google/googlenav/aZ;->j:I

    iget-object v0, p1, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/bf;->f:LaN/H;

    if-nez v0, :cond_2

    invoke-virtual {p2}, LaN/u;->f()LaN/H;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    if-eqz p2, :cond_5

    iget v0, p1, Lcom/google/googlenav/bf;->d:I

    if-ne v0, v3, :cond_3

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {p2, v0, v1}, LaN/u;->a(ILaN/H;)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/googlenav/aZ;->w:I

    iget v0, p1, Lcom/google/googlenav/bf;->e:I

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {p2, v0}, LaN/u;->b(LaN/H;)I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/google/googlenav/aZ;->x:I

    :goto_3
    iget v0, p1, Lcom/google/googlenav/bf;->c:I

    iput v0, p0, Lcom/google/googlenav/aZ;->y:I

    iget-object v0, p1, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/bf;->i:Lcom/google/googlenav/bb;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_1

    :cond_0
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->T:Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p1, Lcom/google/googlenav/bf;->f:LaN/H;

    goto :goto_0

    :cond_3
    iget v0, p1, Lcom/google/googlenav/bf;->d:I

    goto :goto_1

    :cond_4
    iget v0, p1, Lcom/google/googlenav/bf;->e:I

    goto :goto_2

    :cond_5
    iput v2, p0, Lcom/google/googlenav/aZ;->w:I

    iput v2, p0, Lcom/google/googlenav/aZ;->x:I

    goto :goto_3
.end method

.method public static a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public static a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;
    .locals 2

    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0}, Lcom/google/googlenav/aZ;-><init>()V

    iput-object p1, v0, Lcom/google/googlenav/aZ;->u:LaN/H;

    iput p2, v0, Lcom/google/googlenav/aZ;->w:I

    iput p3, v0, Lcom/google/googlenav/aZ;->x:I

    array-length v1, p0

    iput v1, v0, Lcom/google/googlenav/aZ;->q:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/googlenav/aZ;->s:Z

    iput-object p0, v0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    invoke-direct {v0}, Lcom/google/googlenav/aZ;->ba()V

    if-eqz p4, :cond_0

    iput-object p4, v0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, p4, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iget v1, p4, Lcom/google/googlenav/bf;->b:I

    iput v1, v0, Lcom/google/googlenav/aZ;->j:I

    iget-object v1, p4, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    iget v1, p4, Lcom/google/googlenav/bf;->c:I

    iput v1, v0, Lcom/google/googlenav/aZ;->y:I

    iget-object v1, p4, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v1, ""

    iput-object v1, v0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/googlenav/aZ;->j:I

    goto :goto_0
.end method

.method public static a([Lcom/google/googlenav/ai;Lcom/google/googlenav/bf;LaN/u;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;
    .locals 1

    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    iput-object p0, v0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    iput-object p3, v0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    invoke-direct {v0}, Lcom/google/googlenav/aZ;->ba()V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, " "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, " loc:"

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v1, 0x3d

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bc;

    if-nez v1, :cond_2

    const/16 v1, 0x2c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    const-string v1, "ft"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v1, 0x3a

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p0, :cond_5

    const-string p0, ""

    :cond_2
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/aZ;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :cond_4
    if-eqz p1, :cond_0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-static {p0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method private aT()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aU()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aV()V
    .locals 7

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/16 v3, 0x10

    invoke-static {}, Lcom/google/googlenav/android/F;->g()Landroid/util/Pair;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_2

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_1

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "s=l"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nt="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "st="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "e="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/google/googlenav/aZ;->B:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v3}, Lbm/i;->g()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stat"

    invoke-static {v6, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method private aW()[I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    new-array v3, v1, [I

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, -0x1

    aput v2, v3, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v1, 0x1

    aput v1, v3, v0

    move v1, v2

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method private aX()V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private aY()V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v5, v0

    new-array v0, v5, [I

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_3

    move v2, v3

    move v0, v4

    :goto_1
    if-gt v2, v4, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    iget-object v6, p0, Lcom/google/googlenav/aZ;->g:[I

    aget v6, v6, v2

    aget-object v1, v1, v6

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    iget-object v6, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v6

    if-eq v2, v4, :cond_0

    if-eqz v1, :cond_1

    if-eqz v6, :cond_1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v6}, LaN/B;->c()I

    move-result v6

    if-ge v1, v6, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->g:[I

    aget v1, v1, v2

    iget-object v6, p0, Lcom/google/googlenav/aZ;->g:[I

    aput v0, v6, v2

    move v0, v1

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private aZ()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ah()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aZ;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ag;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/aZ;->w:I

    iget v4, p0, Lcom/google/googlenav/aZ;->x:I

    iget-object v5, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v5}, LaN/H;->b()LaN/Y;

    move-result-object v5

    invoke-virtual {v5}, LaN/Y;->a()I

    move-result v5

    invoke-static {v2, v3, v4, v5}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x3

    iget v2, p0, Lcom/google/googlenav/aZ;->j:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-object v1
.end method

.method public static b(Ljava/io/DataInput;)Lcom/google/googlenav/aZ;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->m(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-direct {v1}, Lcom/google/googlenav/aZ;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    const/4 v2, 0x3

    invoke-static {v0, v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    iput v2, v1, Lcom/google/googlenav/aZ;->j:I

    iput-boolean v3, v1, Lcom/google/googlenav/aZ;->o:Z

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    :cond_0
    const/4 v2, 0x5

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iput-object v2, v1, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    :cond_1
    iput-boolean v3, v1, Lcom/google/googlenav/aZ;->A:Z

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    new-instance v3, LaN/H;

    invoke-static {v2}, LaN/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    const/4 v4, 0x0

    invoke-direct {v3, v2, v0, v4}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    iput-object v3, v1, Lcom/google/googlenav/aZ;->u:LaN/H;

    return-object v1
.end method

.method public static b(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v1, 0x3d

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/be;

    if-nez v1, :cond_2

    const/16 v1, 0x2c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    const-string v1, "ot"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v1, 0x3a

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Lcom/google/googlenav/be;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private ba()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/aZ;->Z:I

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aY()V

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aW()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->R:[I

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aX()V

    :cond_0
    return-void
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/bK;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v3, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v3, v3, Lcom/google/googlenav/bf;->t:Z

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :cond_0
    iget-object v3, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v3, v3, Lcom/google/googlenav/bf;->C:I

    if-eqz v3, :cond_2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbO/G;->n:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v4, v4, Lcom/google/googlenav/bf;->C:I

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    if-nez v1, :cond_1

    const/16 v0, 0x25

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 6

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x3

    const/4 v4, 0x1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    :cond_0
    const/16 v0, 0xe

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    const/16 v0, 0xf

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->v:LaN/B;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->w:I

    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->x:I

    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/google/googlenav/aZ;->r:Z

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    :cond_1
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    :cond_2
    iget v0, p0, Lcom/google/googlenav/aZ;->G:I

    const/16 v1, 0xb

    invoke-static {p1, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/aZ;->G:I

    iget v1, p0, Lcom/google/googlenav/aZ;->G:I

    if-ge v1, v0, :cond_3

    const-string v1, "offset"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/googlenav/aZ;->G:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/16 v0, 0xd

    invoke-static {p1, v0, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->q:I

    const/16 v0, 0x18

    invoke-static {p1, v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sget v0, Lcom/google/googlenav/aZ;->e:I

    if-ne v0, v5, :cond_4

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/googlenav/aZ;->G:I

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/aZ;->q:I

    if-ge v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    sput v0, Lcom/google/googlenav/aZ;->e:I

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const-string v1, "("

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ")"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 7

    const/16 v0, 0x8

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_3

    const/16 v3, 0x8

    :try_start_0
    invoke-virtual {p1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0x9f

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x9f

    iget-object v5, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-static {v3}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/av;->a:Lcom/google/googlenav/av;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/av;)V

    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v4, v4, Lcom/google/googlenav/bf;->A:Z

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->e(Z)V

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ai()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->h(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->i(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aT()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->j(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->k(Ljava/lang/String;)V

    :cond_2
    iget-object v4, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aput-object v3, v4, v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    :cond_3
    sget-object v0, Laz/a;->a:Laz/a;

    invoke-virtual {v0}, Laz/a;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    if-le v2, v6, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, v6

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v3, v3, v1

    aput-object v3, v2, v6

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aput-object v0, v2, v1

    :cond_4
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    return-void
.end method

.method private f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/16 v4, 0x10

    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->n:Z

    :cond_0
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->l:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v0, v1, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/16 v4, 0x11

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private h(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    aget v0, v0, p1

    return v0
.end method

.method private h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 12

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/bc;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    const/16 v1, 0x12

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    long-to-int v3, v3

    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6, v5}, Ljava/util/Vector;-><init>(I)V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_0

    const/4 v7, 0x3

    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    new-instance v8, Lcom/google/googlenav/bd;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v9

    const/4 v10, 0x2

    invoke-static {v7, v10}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    invoke-static {v7, v11}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v9, v10, v7}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    new-instance v2, Lcom/google/googlenav/bc;

    invoke-direct {v2, v3, v4, v6}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    const/16 v7, 0x15

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v0, v1, [Lcom/google/googlenav/be;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->ac:[Lcom/google/googlenav/be;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    long-to-int v3, v3

    const/4 v4, 0x2

    invoke-static {v2, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/googlenav/aZ;->ac:[Lcom/google/googlenav/be;

    new-instance v6, Lcom/google/googlenav/be;

    invoke-direct {v6, v3, v4, v2}, Lcom/google/googlenav/be;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 6

    const/16 v5, 0x14

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v0, v1, [LaW/R;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->ad:[LaW/R;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/aZ;->ad:[LaW/R;

    const/4 v4, 0x0

    invoke-static {v2, v4}, LaW/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaW/S;)LaW/R;

    move-result-object v2

    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private k(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/layer/m;

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    :goto_0
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/layer/a;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/layer/m;

    invoke-direct {v4, v1}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {v2, v3, v4, v0}, Lcom/google/googlenav/layer/a;-><init>(Ljava/lang/String;Lcom/google/googlenav/layer/m;I)V

    iput-object v2, p0, Lcom/google/googlenav/aZ;->P:Lcom/google/googlenav/layer/a;

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    goto :goto_0
.end method

.method private l(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static m(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "Missing SEARCH_RESPONSE_WITH_PAYLOAD"

    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    const-string v2, " proto:\n"

    invoke-virtual {v1, v2}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toTextFormat(Ljava/io/Writer;)V

    const-string v2, "Search.assertCorrectProtoBuf"

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v0, "Null SEARCH_RESPONSE_WITH_PAYLOAD"

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v0, "Missing RESPONSE"

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "null RESPONSE"

    goto :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    goto :goto_0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4f3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x4f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public D()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->y:Z

    goto :goto_0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->l:Ljava/lang/String;

    return-object v0
.end method

.method public F()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    return-object v0
.end method

.method public H()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public I()[Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public J()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->V:I

    return v0
.end method

.method public K()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->n:Z

    return v0
.end method

.method public L()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    return-object v0
.end method

.method public M()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    return-object v0
.end method

.method public N()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->o:Z

    return v0
.end method

.method public O()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->v:LaN/B;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->v:LaN/B;

    goto :goto_0
.end method

.method public P()LaN/M;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->l:LaN/M;

    return-object v0
.end method

.method public Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    return-object v0
.end method

.method public S()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->w:I

    return v0
.end method

.method public T()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->x:I

    return v0
.end method

.method public U()LaN/H;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    return-object v0
.end method

.method public V()LaN/H;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->E:LaN/H;

    return-object v0
.end method

.method public W()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->y:I

    return v0
.end method

.method public X()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    return-object v0
.end method

.method public Y()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/googlenav/aZ;->Z:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/google/googlenav/aZ;->Z:I

    :goto_0
    return v0

    :cond_0
    iput v0, p0, Lcom/google/googlenav/aZ;->Z:I

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->M()Lcom/google/googlenav/aq;

    move-result-object v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/googlenav/aZ;->Z:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/googlenav/aZ;->Z:I

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/google/googlenav/aZ;->Z:I

    goto :goto_0
.end method

.method public Z()V
    .locals 1

    invoke-super {p0}, Law/a;->Z()V

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    invoke-interface {v0, p0}, Lcom/google/googlenav/bb;->b(Lcom/google/googlenav/aZ;)V

    :cond_0
    return-void
.end method

.method public a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;
    .locals 2

    new-instance v0, Lcom/google/googlenav/aZ;

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;
    .locals 1

    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)V

    return-object v0
.end method

.method public a(B)V
    .locals 0

    iput-byte p1, p0, Lcom/google/googlenav/aZ;->E:B

    return-void
.end method

.method public a(I)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->ag:I

    iget v0, p0, Lcom/google/googlenav/aZ;->D:I

    if-eq p1, v0, :cond_0

    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x5

    const-string v1, "1"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/googlenav/aZ;->D:I

    if-gez p1, :cond_3

    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    if-ne v0, v3, :cond_0

    const/16 v0, 0x12

    const-string v1, "1"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    if-nez v0, :cond_1

    iput-byte v3, p0, Lcom/google/googlenav/aZ;->E:B

    goto :goto_1
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/googlenav/aZ;->X:J

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    add-int/2addr v0, v2

    new-array v2, v0, [Lcom/google/googlenav/ai;

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v3, v3, v0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Lcom/google/googlenav/ai;->a(B)V

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ae()V

    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v4, v4, Lcom/google/googlenav/bf;->A:Z

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->e(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    add-int/2addr v4, v0

    aput-object v3, v2, v4

    invoke-virtual {p0, v3}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/ai;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iput-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    iget v0, p1, Lcom/google/googlenav/aZ;->q:I

    iput v0, p0, Lcom/google/googlenav/aZ;->q:I

    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->s:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 11

    const/16 v10, 0x10

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    invoke-virtual {v5, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x3e

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->J:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/googlenav/aZ;->G:I

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->al()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/aZ;->w:I

    iget v4, p0, Lcom/google/googlenav/aZ;->x:I

    iget-object v6, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v6}, LaN/H;->b()LaN/Y;

    move-result-object v6

    invoke-virtual {v6}, LaN/Y;->a()I

    move-result v6

    invoke-static {v0, v1, v4, v6}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v5, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    sget-boolean v0, Lcom/google/googlenav/aZ;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x12

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x2e

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x33

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x34

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x11

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x24

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x3d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v2

    :goto_0
    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x6

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x13

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x14

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x18

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xe

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/D;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->I()I

    move-result v4

    add-int/lit8 v4, v4, -0x14

    invoke-virtual {v0, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/bi;->a(Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-virtual {v0, v10, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    long-to-int v1, v6

    div-int/lit16 v1, v1, 0x400

    div-int/lit8 v1, v1, 0x3

    const/16 v4, 0xc8

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xc

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    invoke-virtual {v5, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1a

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x12

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x16

    const/16 v1, 0x100

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x19

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x20

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x21

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aN()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x32

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v6

    invoke-virtual {v6}, LaN/M;->j()[LaN/B;

    move-result-object v6

    invoke-static {v6}, LaN/M;->a([LaN/B;)[B

    move-result-object v6

    invoke-virtual {v1, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x4

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1b

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_4
    const/16 v0, 0x17

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aB()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aB()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1c

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_5
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lcom/google/googlenav/aZ;->T:Z

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->av()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x1f

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eW;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aw()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x23

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_6
    const/16 v0, 0x2f

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->z:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x31

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_8

    const/16 v0, 0x37

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_8
    invoke-direct {p0, v5}, Lcom/google/googlenav/aZ;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/16 v0, 0x26

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_a

    move v0, v3

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    const/16 v1, 0x27

    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v4, v4, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v4, v4, v0

    invoke-virtual {v5, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    move v0, v3

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->b()I

    move-result v0

    invoke-static {v0, v5, v2}, Lcom/google/googlenav/ah;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    const/16 v0, 0x2b

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x35

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x36

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-boolean v0, Lcom/google/googlenav/aZ;->c:Z

    if-eqz v0, :cond_e

    sget-object v0, Lcom/google/googlenav/aZ;->aa:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_b

    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/fC;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v4, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x2c

    invoke-virtual {v5, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_2

    :cond_c
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_e

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bc;

    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fC;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->a()I

    move-result v1

    invoke-virtual {v7, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_d

    move v4, v3

    :goto_4
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_d

    new-instance v8, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fC;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/bd;

    invoke-virtual {v1}, Lcom/google/googlenav/bd;->a()I

    move-result v1

    invoke-virtual {v8, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/bd;

    invoke-virtual {v1}, Lcom/google/googlenav/bd;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v7, v9, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    :cond_d
    const/16 v0, 0x2d

    invoke-virtual {v5, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_3

    :cond_e
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/be;

    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/fC;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0}, Lcom/google/googlenav/be;->a()I

    move-result v0

    invoke-virtual {v4, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x39

    invoke-virtual {v5, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_5

    :cond_f
    const/16 v0, 0x2a

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->D()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->af()Z

    move-result v1

    if-nez v1, :cond_10

    move v3, v2

    :cond_10
    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->D:LaW/R;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->D:LaW/R;

    invoke-virtual {v0}, LaW/R;->f()Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0x3a

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->D:LaW/R;

    invoke-virtual {v1}, LaW/R;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_11
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->G:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x3c

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_12
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v5, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/aZ;->o:Z

    return-void
.end method

.method public a([Lcom/google/googlenav/ai;)V
    .locals 5

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_2
    array-length v2, p1

    if-ge v1, v2, :cond_3

    aget-object v4, p1, v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aN()Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v2

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/ai;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 7

    const/16 v6, 0x1b1

    const/16 v5, 0x1a

    const/16 v4, 0x17

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/googlenav/aZ;->a(J)V

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    const/16 v0, 0x19

    const/4 v3, -0x1

    invoke-static {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->ah:I

    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iput-boolean v1, p0, Lcom/google/googlenav/aZ;->A:Z

    sget-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aV()V

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->w()Z

    move-result v3

    invoke-interface {v0, p0, v3}, Lcom/google/googlenav/bb;->a(Lcom/google/googlenav/aZ;Z)V

    :cond_1
    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/googlenav/K;->d(Z)V

    :goto_2
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-gez v0, :cond_5

    int-to-long v2, v0

    :goto_3
    iput-wide v2, p0, Lcom/google/googlenav/aZ;->b:J

    :goto_4
    return v1

    :pswitch_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    :pswitch_2
    invoke-direct {p0, v2}, Lcom/google/googlenav/aZ;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->k(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v0

    if-nez v0, :cond_2

    iput v1, p0, Lcom/google/googlenav/aZ;->B:I

    const/16 v0, 0x5f1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_0

    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    iput v1, p0, Lcom/google/googlenav/aZ;->B:I

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v2}, Lcom/google/googlenav/aZ;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_4
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/K;->d(Z)V

    goto :goto_2

    :cond_5
    int-to-long v2, v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    goto :goto_3

    :cond_6
    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/googlenav/aZ;->b:J

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public aA()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->T:Z

    return v0
.end method

.method public aB()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->p:LaN/B;

    return-object v0
.end method

.method public aC()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->F:Z

    return v0
.end method

.method public aD()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bb()I

    move-result v2

    if-lez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public aE()[Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public aF()V
    .locals 4

    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-le v0, v3, :cond_1

    new-array v1, v3, [Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    :cond_1
    return-void
.end method

.method public aG()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    return-object v0
.end method

.method public aH()[Lcom/google/googlenav/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    return-object v0
.end method

.method public aI()[Lcom/google/googlenav/be;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->ac:[Lcom/google/googlenav/be;

    return-object v0
.end method

.method public aJ()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    return-object v0
.end method

.method public aK()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    return-object v0
.end method

.method public aL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->v:Ljava/lang/String;

    return-object v0
.end method

.method public aM()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public aN()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->ae:Z

    return v0
.end method

.method public aO()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->I:Z

    return v0
.end method

.method public aP()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    return-wide v0
.end method

.method public aQ()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aa()Lcom/google/googlenav/layer/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->P:Lcom/google/googlenav/layer/a;

    return-object v0
.end method

.method public ab()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ac()Lcom/google/googlenav/layer/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method public ad()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public ae()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bC()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public af()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->Q:Z

    if-eqz v0, :cond_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->Q:Z

    return v0
.end method

.method public ag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    goto :goto_0
.end method

.method public ah()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 12

    const/4 v11, 0x3

    const/4 v6, -0x1

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    invoke-virtual {v5, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    if-eq v0, v6, :cond_1

    const/16 v0, 0x19

    iget v3, p0, Lcom/google/googlenav/aZ;->ah:I

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-virtual {v5, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0xe

    iget-object v3, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0xf

    iget-object v3, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    const/4 v0, 0x5

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    iget v3, p0, Lcom/google/googlenav/aZ;->w:I

    iget v4, p0, Lcom/google/googlenav/aZ;->x:I

    invoke-static {v0, v3, v4, v6}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x7

    invoke-virtual {v5, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xb

    iget v3, p0, Lcom/google/googlenav/aZ;->G:I

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v5}, Lcom/google/googlenav/aZ;->l(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    invoke-virtual {v3, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v0, 0x7

    iget-object v4, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    const/16 v0, 0x8

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :cond_6
    if-eqz v0, :cond_7

    const/16 v0, 0x9

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    if-eqz v0, :cond_8

    const/16 v0, 0xc

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    if-eqz v0, :cond_9

    move v0, v2

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_9

    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v5, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    if-eqz v0, :cond_b

    move v3, v2

    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    array-length v0, v0

    if-ge v3, v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    aget-object v6, v0, v3

    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v6}, Lcom/google/googlenav/bc;->a()I

    move-result v0

    invoke-virtual {v7, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v6}, Lcom/google/googlenav/bc;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v6}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_a

    move v4, v2

    :goto_3
    invoke-virtual {v6}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_a

    new-instance v8, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v6}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bd;

    invoke-virtual {v0}, Lcom/google/googlenav/bd;->a()I

    move-result v9

    invoke-virtual {v8, v1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/bd;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v7, v11, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_a
    const/16 v0, 0x12

    invoke-virtual {v5, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_b
    return-object v5

    :cond_c
    move v0, v2

    goto/16 :goto_0
.end method

.method public ai()[Lcom/google/googlenav/ui/aH;
    .locals 5

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_2
    invoke-interface {v1}, LaN/g;->a()I

    move-result v0

    const/4 v4, 0x2

    if-eq v0, v4, :cond_3

    const/4 v4, 0x3

    if-ne v0, v4, :cond_4

    :cond_3
    move-object v0, v1

    check-cast v0, Lcom/google/googlenav/ui/aH;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aH;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    check-cast v1, Lcom/google/googlenav/ui/aH;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/ui/aH;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ui/aH;

    goto :goto_1
.end method

.method public aj()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->G:I

    return v0
.end method

.method public ak()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->q:I

    return v0
.end method

.method public al()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/googlenav/aZ;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget v0, Lcom/google/googlenav/aZ;->e:I

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public am()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->al()I

    move-result v2

    if-ge v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/googlenav/aZ;->s:Z

    if-eqz v1, :cond_0

    const v0, 0x7fffffff

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ak()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ak()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aj()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->al()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public an()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    return-object v0
.end method

.method public ao()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->r:Z

    return v0
.end method

.method public ap()I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public aq()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public ar()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public as()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public at()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public au()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public av()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->q:Z

    return v0
.end method

.method public aw()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v0, v0, Lcom/google/googlenav/bf;->s:I

    return v0
.end method

.method public ax()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aT()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aU()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ay()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    return-wide v0
.end method

.method public az()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->L:Z

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x2e

    return v0
.end method

.method public b(I)Lcom/google/googlenav/E;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    invoke-virtual {v2}, Lcom/google/googlenav/layer/m;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-object v0
.end method

.method public b(Ljava/io/DataOutput;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aZ()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/aZ;->L:Z

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->D:I

    return v0
.end method

.method public c(I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->h(I)I

    move-result v0

    return v0
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/aZ;->ae:Z

    return-void
.end method

.method public c_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()B
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    return v0
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/aZ;->B:I

    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    invoke-interface {v0, p0}, Lcom/google/googlenav/bb;->a(Lcom/google/googlenav/aZ;)V

    :cond_0
    return-void
.end method

.method public e()Lcom/google/googlenav/E;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/aZ;->V:I

    return-void
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    return v0
.end method

.method public f(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->R:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/aZ;->R:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public g(I)Lcom/google/googlenav/bc;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->a()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    aget-object v0, v1, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->ag:I

    return v0
.end method

.method public k()Lcom/google/googlenav/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->h:Lcom/google/googlenav/ba;

    return-object v0
.end method

.method public l()Lcom/google/googlenav/bb;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    return-object v0
.end method

.method public m()Lcom/google/googlenav/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    return-object v0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    return-object v0
.end method

.method public p()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    return v0
.end method

.method public q()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/aZ;->B:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    return v0
.end method

.method public v()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aZ;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public z()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    return v0
.end method
