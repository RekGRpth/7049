.class public Lcom/google/googlenav/aa;
.super Law/a;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/googlenav/Y;

.field private b:Z

.field private c:Z

.field private d:[I

.field private final e:Lcom/google/googlenav/ui/bh;

.field private final f:Lax/y;

.field private final g:I

.field private final h:I

.field private i:Ljava/util/List;

.field private final j:Z

.field private volatile k:Z

.field private l:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lax/y;)V
    .locals 2

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    new-instance v0, Lcom/google/googlenav/Y;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/Y;-><init>(Lax/y;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iput-object p1, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    const/16 v0, 0x4e20

    iput v0, p0, Lcom/google/googlenav/aa;->g:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/aa;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    return-void
.end method

.method public constructor <init>(Lax/y;Ljava/lang/String;Lcom/google/googlenav/ui/bh;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    new-instance v0, Lcom/google/googlenav/Y;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/Y;-><init>(Lax/y;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iput-object p1, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    const/16 v0, 0x4e20

    iput v0, p0, Lcom/google/googlenav/aa;->g:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/aa;->h:I

    iput-object p3, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/Y;Lax/y;IILcom/google/googlenav/ui/bh;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iput-object p5, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    iput-object p2, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    iput p3, p0, Lcom/google/googlenav/aa;->g:I

    iput p4, p0, Lcom/google/googlenav/aa;->h:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->c:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    return-void
.end method

.method private a([ILjava/util/List;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v3

    array-length v4, p1

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_1

    aget v5, p1, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-interface {v4, v3}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;
    .locals 9

    const/16 v8, 0x9

    const/16 v7, 0x8

    const/4 v6, 0x6

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/util/HashMap;
    .locals 4

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .locals 8

    const/16 v7, 0x8

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ab;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public a(Lax/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v0, p1}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Lax/y;)Lax/y;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 7

    const/16 v6, 0x3e8

    const/4 v5, 0x5

    const/4 v4, 0x3

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iget-object v2, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    invoke-virtual {v2}, Lax/y;->f()LaN/B;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v0}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;)Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iget-object v2, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;)Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->f()LaN/B;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V

    invoke-virtual {v1, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    const/16 v0, 0x9

    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/aa;->d:[I

    invoke-static {v0}, Lcom/google/googlenav/common/util/a;->a([I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/aa;->d:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    const/16 v5, 0x8

    invoke-virtual {v1, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget v0, p0, Lcom/google/googlenav/aa;->h:I

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v0, p0, Lcom/google/googlenav/aa;->g:I

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_1
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    invoke-static {v2}, Lcom/google/googlenav/ui/m;->a(Lcom/google/googlenav/ui/m;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a([I)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/aa;->d:[I

    return-void
.end method

.method public a(J)Z
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->k:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/aa;->z_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lcom/google/googlenav/aa;->k:Z

    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 8

    const/4 v5, 0x0

    const/16 v4, 0x9

    const/4 v3, 0x3

    const/4 v7, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Z)Z

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;I)I

    :cond_0
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    new-instance v2, Lcom/google/googlenav/layer/m;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/googlenav/aa;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    iget-object v0, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aa;->a(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/aa;->d:[I

    iget-object v3, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/aa;->a([ILjava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lbf/bU;->bM()V

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v2}, Lcom/google/googlenav/Y;->b(Lcom/google/googlenav/Y;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v2}, Lcom/google/googlenav/Y;->b(Lcom/google/googlenav/Y;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    const/4 v3, 0x7

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v1}, Lcom/google/googlenav/aa;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/bh;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;)V

    :cond_2
    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-eqz v2, :cond_4

    new-instance v3, Lcom/google/googlenav/Z;

    invoke-direct {v3, v1, v5, v5, v0}, Lcom/google/googlenav/Z;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/bZ;[Lcom/google/googlenav/bZ;Ljava/util/Map;)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    invoke-virtual {v1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/bZ;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/hF;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v4, v6, v3}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/cd;)V

    invoke-virtual {v5, v7}, Lcom/google/googlenav/bZ;->a(Z)V

    invoke-virtual {v5, v7}, Lcom/google/googlenav/bZ;->b(Z)V

    iget-object v4, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    new-instance v6, Lcom/google/googlenav/cp;

    invoke-direct {v6, v5}, Lcom/google/googlenav/cp;-><init>(Lcom/google/googlenav/bZ;)V

    invoke-virtual {v4, v6}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/cp;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    sget-object v2, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/aa;->k:Z

    iget-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return v7

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized a_()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x7e

    return v0
.end method

.method public declared-synchronized d_()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/aa;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aa;->c:Z

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    return v0
.end method

.method public l()Lcom/google/googlenav/Y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    return-object v0
.end method
