.class public Lcom/google/googlenav/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/ah;->a:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/googlenav/ah;->b:[I

    return-void

    :array_0
    .array-data 4
        0xa
        0xf
        0xe
        0x15
        0x1b
        0x1c
        0x23
        0x32
        0x24
        0x33
        0x27
        0x4e
        0x4f
        0x4b
        0x4a
        0x56
    .end array-data

    :array_1
    .array-data 4
        0x11
        0x31
        0x1f
        0x20
        0x4c
        0x2d
        0x50
    .end array-data
.end method

.method public static a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ev;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBool(IZ)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public static a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 7

    const/4 v6, 0x4

    const/16 v5, 0x29

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/ah;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const/16 v1, 0x1d

    invoke-virtual {p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x28

    invoke-virtual {p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_1
    sget-object v1, Lcom/google/googlenav/ah;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/google/googlenav/ah;->a:[I

    aget v1, v1, v0

    invoke-virtual {p1, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/16 v0, 0x38

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :sswitch_1
    const/16 v1, 0x28

    invoke-virtual {p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v0

    :goto_2
    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    aget v3, v3, v1

    invoke-virtual {p1, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    :goto_3
    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    aget v1, v1, v0

    invoke-virtual {p1, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    const/16 v0, 0x2a

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p2, :cond_3

    move v1, v0

    :goto_4
    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    array-length v3, v3

    if-ge v1, v3, :cond_3

    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    aget v3, v3, v1

    invoke-virtual {p1, v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    :goto_5
    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_4

    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    aget v1, v1, v0

    invoke-virtual {p1, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_4
    invoke-virtual {p1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :sswitch_3
    if-eqz p2, :cond_5

    move v1, v0

    :goto_6
    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    array-length v3, v3

    if-ge v1, v3, :cond_5

    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    aget v3, v3, v1

    invoke-virtual {p1, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_5
    :goto_7
    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_6

    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    aget v1, v1, v0

    invoke-virtual {p1, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_0

    :sswitch_4
    if-eqz p2, :cond_7

    move v1, v0

    :goto_8
    sget-object v3, Lcom/google/googlenav/ah;->a:[I

    array-length v3, v3

    if-ge v1, v3, :cond_7

    const/16 v3, 0x9

    sget-object v4, Lcom/google/googlenav/ah;->a:[I

    aget v4, v4, v1

    invoke-virtual {p1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_7
    :goto_9
    sget-object v1, Lcom/google/googlenav/ah;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_8

    const/16 v1, 0x9

    sget-object v3, Lcom/google/googlenav/ah;->b:[I

    aget v3, v3, v0

    invoke-virtual {p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_1
        0x2e -> :sswitch_0
        0x50 -> :sswitch_2
        0x5d -> :sswitch_4
        0x77 -> :sswitch_3
    .end sparse-switch
.end method
