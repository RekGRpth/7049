.class public Lcom/google/googlenav/android/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/v;
.implements LaV/i;
.implements Lcom/google/android/maps/driveabout/vector/bq;


# static fields
.field public static final a:Lcom/google/googlenav/android/A;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Lcom/google/googlenav/android/C;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/android/A;

    invoke-direct {v0}, Lcom/google/googlenav/android/A;-><init>()V

    sput-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0}, Lcom/google/googlenav/android/C;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    return-void
.end method

.method public a(FF)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->c:Z

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/android/A;->a(FZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->c:Z

    :cond_0
    return-void
.end method

.method a(FZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    iget-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-interface {v0, p1, v1, p2}, Lcom/google/googlenav/android/C;->a(FZZ)V

    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/android/B;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/C;->a(Lcom/google/googlenav/android/B;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/android/C;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    return-void
.end method

.method public a(ZZZII)V
    .locals 1

    iput-boolean p3, p0, Lcom/google/googlenav/android/A;->d:Z

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->j()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->f:Z

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, LaV/h;->a(LaV/i;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0, p0}, Lcom/google/googlenav/android/C;->a(LaN/v;)V

    return-void
.end method

.method e()Z
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/android/A;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    iput-boolean v1, p0, Lcom/google/googlenav/android/A;->c:Z

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->g()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/android/A;->a(FZ)V

    goto :goto_0
.end method

.method g()V
    .locals 3

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x62

    const-string v1, "a"

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v2

    invoke-virtual {v2}, LaV/h;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    iget-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/C;->a(Z)V

    :cond_0
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_2

    sget-object v0, LaV/j;->c:LaV/j;

    :goto_0
    invoke-virtual {v1, v0}, LaV/h;->a(LaV/j;)V

    :cond_1
    return-void

    :cond_2
    sget-object v0, LaV/j;->b:LaV/j;

    goto :goto_0
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->d:Z

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    return-void
.end method

.method j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0}, Lcom/google/googlenav/android/C;->b()Z

    move-result v0

    return v0
.end method

.method public k()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    iget-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/C;->b(Z)I

    move-result v0

    return v0
.end method
