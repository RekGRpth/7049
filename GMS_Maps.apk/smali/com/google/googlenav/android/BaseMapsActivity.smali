.class public abstract Lcom/google/googlenav/android/BaseMapsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"


# instance fields
.field private dialogToBeDismissed:Landroid/app/Dialog;

.field private shownDialogs:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->shownDialogs:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->dialogToBeDismissed:Landroid/app/Dialog;

    return-void
.end method

.method private static actuallyDismiss(Landroid/app/Dialog;)V
    .locals 2

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    instance-of v0, p0, Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->t()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Dialog hide"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static actuallyShow(Landroid/app/Dialog;)V
    .locals 1

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p0, Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->s()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private static isFullScreen(Landroid/app/Dialog;)Z
    .locals 1

    instance-of v0, p0, Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->P_()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public dismissDialog(Landroid/app/Dialog;)V
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/googlenav/ui/view/android/S;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->u()Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/android/BaseMapsActivity;->shownDialogs:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/android/BaseMapsActivity;->shownDialogs:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->dialogToBeDismissed:Landroid/app/Dialog;

    invoke-static {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->actuallyDismiss(Landroid/app/Dialog;)V

    invoke-static {p1}, Lcom/google/googlenav/android/BaseMapsActivity;->actuallyDismiss(Landroid/app/Dialog;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->dialogToBeDismissed:Landroid/app/Dialog;

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->shownDialogs:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    return-void

    :cond_1
    invoke-static {p1}, Lcom/google/googlenav/android/BaseMapsActivity;->isFullScreen(Landroid/app/Dialog;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/google/googlenav/android/BaseMapsActivity;->actuallyDismiss(Landroid/app/Dialog;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->dialogToBeDismissed:Landroid/app/Dialog;

    invoke-static {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->actuallyDismiss(Landroid/app/Dialog;)V

    iput-object p1, p0, Lcom/google/googlenav/android/BaseMapsActivity;->dialogToBeDismissed:Landroid/app/Dialog;

    goto :goto_0
.end method

.method public abstract getState()Lcom/google/googlenav/android/i;
.end method

.method public getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getUiThreadHandler()Lcom/google/googlenav/android/aa;
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public declared-synchronized showDialog(Landroid/app/Dialog;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/googlenav/android/BaseMapsActivity;->actuallyShow(Landroid/app/Dialog;)V

    iget-object v0, p0, Lcom/google/googlenav/android/BaseMapsActivity;->shownDialogs:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
