.class public Lcom/google/googlenav/android/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/C;


# instance fields
.field private a:Lcom/google/googlenav/android/B;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c()Lcom/google/android/maps/driveabout/vector/bk;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->g()LaN/p;

    move-result-object v0

    instance-of v1, v0, LaO/a;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LaO/a;

    invoke-virtual {v0}, LaO/a;->w()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->h()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(FZZ)V
    .locals 7

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/android/D;->c()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v5

    invoke-virtual {v5}, LC/b;->d()F

    move-result v0

    if-nez p2, :cond_1

    :goto_1
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->b(Landroid/location/Location;)I

    move-result v0

    int-to-float v4, v0

    :goto_2
    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v5}, LC/b;->a()F

    move-result v2

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    if-eqz p3, :cond_2

    const/16 v1, 0x190

    :goto_3
    invoke-virtual {v6, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    goto :goto_0

    :cond_1
    cmpl-float v1, v0, v3

    if-nez v1, :cond_4

    const/high16 v3, 0x42480000

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    move v4, p1

    goto :goto_2

    :cond_4
    move v3, v0

    goto :goto_1
.end method

.method public a(LaN/v;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/u;->a(LaN/v;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/android/B;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/D;->a:Lcom/google/googlenav/android/B;

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/android/D;->b(Z)I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/android/D;->b:I

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput v0, p0, Lcom/google/googlenav/android/D;->b:I

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/android/D;->b:I

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/android/D;->a:Lcom/google/googlenav/android/B;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/D;->a:Lcom/google/googlenav/android/B;

    invoke-interface {v0}, Lcom/google/googlenav/android/B;->a()V

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->g()LaN/p;

    move-result-object v0

    instance-of v2, v0, LaO/a;

    if-eqz v2, :cond_0

    check-cast v0, LaO/a;

    invoke-virtual {v0}, LaO/a;->w()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->i()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public b(Z)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/android/D;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/android/D;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    return v0
.end method
