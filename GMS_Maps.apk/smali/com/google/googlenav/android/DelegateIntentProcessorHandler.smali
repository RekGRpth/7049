.class public abstract Lcom/google/googlenav/android/DelegateIntentProcessorHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/R;


# instance fields
.field protected delegate:Lcom/google/googlenav/android/R;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/R;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    return-void
.end method


# virtual methods
.method public getActivity()Lcom/google/android/maps/MapsActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    return-object v0
.end method

.method public getDelegate()Lcom/google/googlenav/android/R;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUiThreadHandler()Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method public refreshFriends()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->refreshFriends()V

    return-void
.end method

.method public refreshFriendsSettings()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->refreshFriendsSettings()V

    return-void
.end method

.method public resetForInvocation()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    return-void
.end method

.method public resolveType(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->resolveType(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    return-void
.end method

.method public showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;

    move-result-object v0

    return-object v0
.end method

.method public showStarDetails(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->showStarDetails(Ljava/lang/String;)V

    return-void
.end method

.method public showStarOnMap(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->showStarOnMap(Ljava/lang/String;)V

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method public startBusinessDetailsLayer(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    return-void
.end method

.method public startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V

    return-void
.end method

.method public startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/R;->startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/R;->startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V

    return-void
.end method

.method public startFriendsLayer(Lbf/am;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startFriendsLayer(Lbf/am;)V

    return-void
.end method

.method public startFriendsLayerHistorySummary()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startFriendsLayerHistorySummary()V

    return-void
.end method

.method public startFriendsListView(Lbf/am;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    return-void
.end method

.method public startFriendsLocation(Lbf/am;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startFriendsLocation(Lbf/am;Ljava/lang/String;)V

    return-void
.end method

.method public startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V

    return-void
.end method

.method public startFriendsProfile(Lbf/am;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startFriendsProfile(Lbf/am;Ljava/lang/String;)V

    return-void
.end method

.method public startLatitudeSettingsActivity()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startLatitudeSettingsActivity()V

    return-void
.end method

.method public startLocationHistoryOptIn(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startLocationHistoryOptIn(Landroid/content/Intent;)V

    return-void
.end method

.method public startManageAutoCheckinPlaces()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startManageAutoCheckinPlaces()V

    return-void
.end method

.method public startMyPlacesList(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startMyPlacesList(Ljava/lang/String;)V

    return-void
.end method

.method public startNextMatchingActivity(Landroid/content/Intent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startNextMatchingActivity(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public startOffersList()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startOffersList()V

    return-void
.end method

.method public startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/googlenav/android/R;->startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    return-void
.end method

.method public startSettingsActivity()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startSettingsActivity()V

    return-void
.end method

.method public startTransitEntry()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitEntry()V

    return-void
.end method

.method public startTransitNavigationLayer()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitNavigationLayer()V

    return-void
.end method

.method public startTransitStationPage(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startTransitStationPage(Ljava/lang/String;)V

    return-void
.end method
