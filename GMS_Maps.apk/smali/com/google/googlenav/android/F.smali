.class public Lcom/google/googlenav/android/F;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/IntentFilter;

.field private static final b:Landroid/content/IntentFilter;

.field private static final c:Landroid/content/BroadcastReceiver;

.field private static d:Z

.field private static volatile f:Landroid/net/NetworkInfo;

.field private static g:Lcom/google/googlenav/android/H;


# instance fields
.field private final e:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/googlenav/android/F;->a:Landroid/content/IntentFilter;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/googlenav/android/F;->b:Landroid/content/IntentFilter;

    new-instance v0, Lcom/google/googlenav/android/G;

    invoke-direct {v0}, Lcom/google/googlenav/android/G;-><init>()V

    sput-object v0, Lcom/google/googlenav/android/F;->c:Landroid/content/BroadcastReceiver;

    sput-object v2, Lcom/google/googlenav/android/F;->f:Landroid/net/NetworkInfo;

    sput-object v2, Lcom/google/googlenav/android/F;->g:Lcom/google/googlenav/android/H;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/android/F;->e:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized a(Landroid/app/Application;)V
    .locals 3

    const-class v1, Lcom/google/googlenav/android/F;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/googlenav/android/F;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/google/googlenav/android/F;->c(Landroid/content/Context;)V

    sget-object v0, Lcom/google/googlenav/android/F;->c:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/googlenav/android/F;->b:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v2}, Landroid/app/Application;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/android/F;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/android/F;->f:Landroid/net/NetworkInfo;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/F;->f:Landroid/net/NetworkInfo;

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)I
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/googlenav/android/F;->f(Landroid/content/Context;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ReceiverCallNotAllowedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_1
    return v0

    :cond_0
    const-string v1, "level"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    const-string v2, "scale"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    div-int v0, v1, v0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)I
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/google/googlenav/android/F;->f(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "status"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v1, 0x1

    const-string v4, "plugged"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_4

    const/16 v0, 0x41

    :goto_1
    const/4 v1, 0x2

    if-ne v3, v1, :cond_2

    or-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne v3, v1, :cond_3

    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v1, 0x5

    if-ne v3, v1, :cond_0

    or-int/lit8 v0, v0, 0x10

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static e()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/googlenav/android/F;->g:Lcom/google/googlenav/android/H;

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/googlenav/android/F;->g:Lcom/google/googlenav/android/H;

    iget-boolean v0, v0, Lcom/google/googlenav/android/H;->b:Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/googlenav/android/F;->f:Landroid/net/NetworkInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method

.method private static f(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/googlenav/android/F;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static f()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/android/F;->g:Lcom/google/googlenav/android/H;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/android/F;->g:Lcom/google/googlenav/android/H;

    iget-boolean v0, v0, Lcom/google/googlenav/android/H;->a:Z

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/android/F;->f:Landroid/net/NetworkInfo;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method

.method public static g()Landroid/util/Pair;
    .locals 2

    sget-object v0, Lcom/google/googlenav/android/F;->f:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/F;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/googlenav/android/F;->e(Landroid/content/Context;)I

    move-result v0

    and-int/lit8 v0, v0, 0x40

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/F;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/googlenav/android/F;->d(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/F;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/googlenav/android/F;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/F;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/googlenav/android/F;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
