.class public Lcom/google/googlenav/android/S;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile g:Lcom/google/googlenav/android/S;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Map;

.field private c:I

.field private d:I

.field private e:Landroid/content/Intent;

.field private final f:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/android/S;->b:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/S;->f:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/googlenav/android/S;->a:Landroid/content/Context;

    return-void
.end method

.method public static a()Lcom/google/googlenav/android/S;
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/S;->g:Lcom/google/googlenav/android/S;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/S;->g:Lcom/google/googlenav/android/S;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/android/S;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/S;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/googlenav/android/S;->g:Lcom/google/googlenav/android/S;

    :cond_0
    return-void
.end method

.method private c()Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/S;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/S;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/android/S;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/android/S;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/T;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/T;->a(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/android/S;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lcom/google/googlenav/android/S;->c:I

    iput p2, p0, Lcom/google/googlenav/android/S;->d:I

    iput-object p3, p0, Lcom/google/googlenav/android/S;->e:Landroid/content/Intent;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(ILcom/google/googlenav/android/T;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/S;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/android/S;->c()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V
    .locals 2

    invoke-interface {p2}, Lcom/google/googlenav/android/T;->a()I

    move-result v0

    invoke-direct {p0}, Lcom/google/googlenav/android/S;->c()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0, v0, p2}, Lcom/google/googlenav/android/S;->a(ILcom/google/googlenav/android/T;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/android/T;)V
    .locals 4

    iget-object v1, p0, Lcom/google/googlenav/android/S;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/googlenav/android/S;->c:I

    invoke-interface {p1}, Lcom/google/googlenav/android/T;->a()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/google/googlenav/android/S;->c:I

    iget v2, p0, Lcom/google/googlenav/android/S;->d:I

    iget-object v3, p0, Lcom/google/googlenav/android/S;->e:Landroid/content/Intent;

    invoke-interface {p1, v0, v2, v3}, Lcom/google/googlenav/android/T;->a(IILandroid/content/Intent;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/android/S;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/android/S;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/S;->e:Landroid/content/Intent;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Class;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/android/S;->c()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/googlenav/android/S;->c()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public b()I
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/android/S;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/googlenav/android/S;->c:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
