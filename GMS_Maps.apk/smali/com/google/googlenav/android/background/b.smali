.class public Lcom/google/googlenav/android/background/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Landroid/content/Intent;

.field private c:Ljava/lang/String;

.field private d:Landroid/os/Messenger;

.field private e:[D


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/background/b;->a:Ljava/util/Map;

    sget-object v0, Lcom/google/googlenav/android/background/b;->a:Ljava/util/Map;

    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "m"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/android/background/b;->b:Landroid/content/Intent;

    return-void
.end method

.method private a(I)V
    .locals 4

    sget-object v0, Lcom/google/googlenav/android/background/b;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/googlenav/android/background/b;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    const/16 v1, 0x60

    const-string v2, "i"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/android/background/b;->d:Landroid/os/Messenger;

    const/4 v1, 0x0

    invoke-static {v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private b()Z
    .locals 8

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v7, 0x3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/android/background/b;->b:Landroid/content/Intent;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/android/background/b;->b:Landroid/content/Intent;

    const-string v3, "messenger"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    iput-object v0, p0, Lcom/google/googlenav/android/background/b;->d:Landroid/os/Messenger;

    iget-object v0, p0, Lcom/google/googlenav/android/background/b;->d:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/background/b;->b:Landroid/content/Intent;

    const-string v3, "sender"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-nez v0, :cond_2

    invoke-direct {p0, v2}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/android/background/b;->c:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/android/background/b;->a:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/googlenav/android/background/b;->c:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, v4}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/android/background/b;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "locations"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-direct {p0, v7}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto :goto_0

    :cond_4
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v0, v3

    if-lt v0, v4, :cond_5

    array-length v0, v3

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    :cond_5
    invoke-direct {p0, v7}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto :goto_0

    :cond_6
    array-length v0, v3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/googlenav/android/background/b;->e:[D

    move v0, v1

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_7

    :try_start_0
    iget-object v4, p0, Lcom/google/googlenav/android/background/b;->e:[D

    aget-object v5, v3, v0

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    aput-wide v5, v4, v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-direct {p0, v7}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto :goto_0

    :cond_7
    move v0, v1

    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_a

    iget-object v4, p0, Lcom/google/googlenav/android/background/b;->e:[D

    aget-wide v4, v4, v0

    invoke-static {v4, v5}, LaN/B;->b(D)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-direct {p0, v7}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/google/googlenav/android/background/b;->e:[D

    add-int/lit8 v5, v0, 0x1

    aget-wide v4, v4, v5

    invoke-static {v4, v5}, LaN/B;->a(D)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-direct {p0, v7}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_a
    move v1, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 8

    const/4 v1, 0x0

    const-wide v6, 0x412e848000000000L

    invoke-direct {p0}, Lcom/google/googlenav/android/background/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/prefetch/android/h;->d()Lcom/google/googlenav/prefetch/android/h;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a()V

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/android/background/b;->e:[D

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/googlenav/android/background/b;->e:[D

    aget-wide v3, v3, v0

    mul-double/2addr v3, v6

    double-to-int v3, v3

    iget-object v4, p0, Lcom/google/googlenav/android/background/b;->e:[D

    add-int/lit8 v5, v0, 0x1

    aget-wide v4, v4, v5

    mul-double/2addr v4, v6

    double-to-int v4, v4

    new-instance v5, LaN/B;

    invoke-direct {v5, v3, v4}, LaN/B;-><init>(II)V

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/googlenav/android/background/b;->c:Ljava/lang/String;

    invoke-interface {v2, v5, v3, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->b()V

    invoke-direct {p0, v1}, Lcom/google/googlenav/android/background/b;->a(I)V

    goto :goto_0
.end method
