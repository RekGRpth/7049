.class public Lcom/google/googlenav/android/w;
.super Lcom/google/googlenav/ui/aC;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/android/i;

.field private final b:Las/c;

.field private c:Lcom/google/googlenav/ui/X;

.field private d:Lcom/google/googlenav/ui/aD;

.field private e:Lcom/google/googlenav/ui/android/p;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/i;Las/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-direct {p0}, Lcom/google/googlenav/ui/aC;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/android/w;->b:Las/c;

    invoke-virtual {p2}, Las/c;->d()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/aA;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->h(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/d;

    move-result-object v0

    return-object v0
.end method

.method public a(LaN/p;)Lcom/google/googlenav/ui/bF;
    .locals 2

    new-instance v1, Lcom/google/googlenav/ui/android/q;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/android/w;->b(LaN/p;)Lcom/google/googlenav/ui/p;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/p;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/android/q;-><init>(LaN/p;Lcom/google/googlenav/ui/android/p;)V

    return-object v1
.end method

.method public b()Lcom/google/googlenav/ui/aD;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/w;->d:Lcom/google/googlenav/ui/aD;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/c;

    iget-object v1, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v1}, Lcom/google/googlenav/android/i;->e(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/AndroidGmmApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/android/w;->d:Lcom/google/googlenav/ui/aD;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/w;->d:Lcom/google/googlenav/ui/aD;

    return-object v0
.end method

.method public b(LaN/p;)Lcom/google/googlenav/ui/p;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/android/w;->e:Lcom/google/googlenav/ui/android/p;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/p;

    iget-object v1, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v1}, Lcom/google/googlenav/android/i;->l(Lcom/google/googlenav/android/i;)LaN/p;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v2}, Lcom/google/googlenav/android/i;->i(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/android/L;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/android/p;-><init>(LaN/p;Lcom/google/googlenav/ui/android/L;)V

    iput-object v0, p0, Lcom/google/googlenav/android/w;->e:Lcom/google/googlenav/ui/android/p;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/w;->e:Lcom/google/googlenav/ui/android/p;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/ui/X;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->c:Lcom/google/googlenav/ui/X;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/d;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/d;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/w;->c:Lcom/google/googlenav/ui/X;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/w;->c:Lcom/google/googlenav/ui/X;

    return-object v0
.end method

.method public d()Lcom/google/googlenav/ui/android/L;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->i(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/android/L;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->j(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/googlenav/layer/r;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->k(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/layer/r;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->b(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    return-object v0
.end method

.method public i()Las/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/w;->b:Las/c;

    return-object v0
.end method
