.class public Lcom/google/googlenav/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;I[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ap;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/googlenav/ap;->b:I

    iput-object p3, p0, Lcom/google/googlenav/ap;->c:[B

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ap;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ap;->b:I

    return v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ap;
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ap;

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ap;-><init>(Ljava/lang/String;I[B)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ap;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ap;-><init>(Ljava/lang/String;I[B)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ap;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ap;->b:I

    return v0
.end method

.method public c()[B
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ap;->c:[B

    return-object v0
.end method
