.class Lcom/google/googlenav/appwidget/traffic/l;
.super Law/a;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/googlenav/appwidget/traffic/m;

.field private final c:Lax/s;

.field private final d:Lcom/google/googlenav/appwidget/traffic/i;

.field private final e:Lcom/google/googlenav/appwidget/traffic/d;

.field private final f:Lcom/google/googlenav/common/a;

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/appwidget/traffic/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/appwidget/traffic/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/appwidget/traffic/m;Lcom/google/googlenav/appwidget/traffic/d;Lax/j;Lcom/google/googlenav/appwidget/traffic/i;Lcom/google/googlenav/common/a;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/appwidget/traffic/l;->b:Lcom/google/googlenav/appwidget/traffic/m;

    new-instance v0, Lax/s;

    invoke-direct {v0, p3}, Lax/s;-><init>(Lax/k;)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    iput-object p4, p0, Lcom/google/googlenav/appwidget/traffic/l;->d:Lcom/google/googlenav/appwidget/traffic/i;

    iput-object p2, p0, Lcom/google/googlenav/appwidget/traffic/l;->e:Lcom/google/googlenav/appwidget/traffic/d;

    iput-object p5, p0, Lcom/google/googlenav/appwidget/traffic/l;->f:Lcom/google/googlenav/common/a;

    iget v0, p2, Lcom/google/googlenav/appwidget/traffic/d;->a:I

    iput v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 4

    packed-switch p1, :pswitch_data_0

    const/16 v0, 0x5f2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const/16 v0, 0x6c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/appwidget/traffic/l;->e:Lcom/google/googlenav/appwidget/traffic/d;

    iget-object v3, v3, Lcom/google/googlenav/appwidget/traffic/d;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x314

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x318

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(II)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id"

    invoke-static {v1, p1, v0}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    iget-object v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->d:Lcom/google/googlenav/appwidget/traffic/i;

    invoke-virtual {v1}, Lcom/google/googlenav/appwidget/traffic/i;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const-string v2, "c"

    invoke-static {v2, v1, v0}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    const-string v1, "rc"

    invoke-static {v1, p2, v0}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x48

    const-string v2, "wr"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->Z()V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0, p1}, Lax/s;->a(Ljava/io/DataOutput;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 12

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0, p1}, Lax/s;->a(Ljava/io/DataInput;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->y()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->d:Lcom/google/googlenav/appwidget/traffic/i;

    iget v2, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/traffic/l;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/appwidget/traffic/i;->a(ILjava/lang/String;)V

    iget v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/appwidget/traffic/l;->a(II)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->b:Lcom/google/googlenav/appwidget/traffic/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->b:Lcom/google/googlenav/appwidget/traffic/m;

    iget v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    invoke-interface {v0, v1}, Lcom/google/googlenav/appwidget/traffic/m;->a(I)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->f:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->aB()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v1}, Lax/s;->aC()LaN/B;

    move-result-object v9

    invoke-virtual {v0, v9}, LaN/B;->b(LaN/B;)J

    move-result-wide v0

    long-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->M()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v1}, Lax/s;->aU()I

    move-result v1

    int-to-double v2, v1

    const-wide/high16 v10, 0x4059000000000000L

    mul-double/2addr v2, v10

    add-int v4, v0, v1

    int-to-double v10, v4

    div-double/2addr v2, v10

    const-wide/high16 v10, 0x3fe0000000000000L

    add-double/2addr v2, v10

    double-to-int v4, v2

    add-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long v10, v0, v2

    add-long v2, v5, v10

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->d:Lcom/google/googlenav/appwidget/traffic/i;

    iget v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    invoke-virtual/range {v0 .. v8}, Lcom/google/googlenav/appwidget/traffic/i;->a(IJIJJ)V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->e:Lcom/google/googlenav/appwidget/traffic/d;

    iget-wide v0, v0, Lcom/google/googlenav/appwidget/traffic/d;->l:J

    const-wide/32 v7, 0x3a980

    add-long/2addr v0, v7

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    add-long v0, v5, v10

    const-wide/32 v2, 0x1d4c0

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/googlenav/appwidget/traffic/l;->d:Lcom/google/googlenav/appwidget/traffic/i;

    iget v3, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/googlenav/appwidget/traffic/i;->b(IJ)V

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->e:Lcom/google/googlenav/appwidget/traffic/d;

    iget-object v0, v0, Lcom/google/googlenav/appwidget/traffic/d;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->e:Lcom/google/googlenav/appwidget/traffic/d;

    iget-object v0, v0, Lcom/google/googlenav/appwidget/traffic/d;->e:Ljava/lang/Integer;

    if-nez v0, :cond_1

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->d:Lcom/google/googlenav/appwidget/traffic/i;

    iget v1, p0, Lcom/google/googlenav/appwidget/traffic/l;->g:I

    invoke-virtual {v9}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v9}, LaN/B;->e()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/appwidget/traffic/i;->a(III)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->a_()Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->b()I

    move-result v0

    return v0
.end method

.method public b_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->b_()Z

    move-result v0

    return v0
.end method

.method public d_()V
    .locals 0

    return-void
.end method

.method public s()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->s()Z

    move-result v0

    return v0
.end method

.method public s_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->s_()Z

    move-result v0

    return v0
.end method

.method public t_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->t_()Z

    move-result v0

    return v0
.end method

.method public z_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/traffic/l;->c:Lax/s;

    invoke-virtual {v0}, Lax/s;->z_()Z

    move-result v0

    return v0
.end method
