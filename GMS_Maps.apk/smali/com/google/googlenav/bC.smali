.class Lcom/google/googlenav/bC;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/bB;

.field private b:Z

.field private c:Ljava/lang/Thread;

.field private d:I

.field private e:I

.field private final f:LaR/u;


# direct methods
.method constructor <init>(LaR/u;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/bC;->f:LaR/u;

    iput-boolean v1, p0, Lcom/google/googlenav/bC;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    iput v1, p0, Lcom/google/googlenav/bC;->d:I

    iput v1, p0, Lcom/google/googlenav/bC;->e:I

    new-instance v0, Lcom/google/googlenav/bB;

    invoke-direct {v0}, Lcom/google/googlenav/bB;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/bC;->a:Lcom/google/googlenav/bB;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bC;)I
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/bC;->b()I

    move-result v0

    return v0
.end method

.method private declared-synchronized a()Lcom/google/googlenav/bB;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/bC;->a:Lcom/google/googlenav/bB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)V
    .locals 1

    :goto_0
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget v0, p0, Lcom/google/googlenav/bC;->e:I

    if-lt v0, p1, :cond_1

    monitor-exit p0

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method private declared-synchronized a(ILcom/google/googlenav/bB;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lcom/google/googlenav/bC;->a:Lcom/google/googlenav/bB;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    iget v0, p0, Lcom/google/googlenav/bC;->e:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/bC;->e:I

    iget-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/bC;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/bC;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/bC;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bC;ILcom/google/googlenav/bB;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/bC;->a(ILcom/google/googlenav/bB;)V

    return-void
.end method

.method private declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    iget v0, p0, Lcom/google/googlenav/bC;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/bC;->d:I

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/bC;->c()V

    :cond_1
    iget v0, p0, Lcom/google/googlenav/bC;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/googlenav/bC;)Lcom/google/googlenav/bB;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/bC;->a()Lcom/google/googlenav/bB;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/bC;)LaR/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bC;->f:LaR/u;

    return-object v0
.end method

.method private declared-synchronized c()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    iget v0, p0, Lcom/google/googlenav/bC;->d:I

    new-instance v1, Lcom/google/googlenav/bD;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/bD;-><init>(Lcom/google/googlenav/bC;I)V

    iput-object v1, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
