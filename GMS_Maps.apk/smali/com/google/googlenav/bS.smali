.class public Lcom/google/googlenav/bS;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/bN;

.field private final b:Ljava/util/List;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bN;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/bS;->a:Lcom/google/googlenav/bN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    iput-object p2, p0, Lcom/google/googlenav/bS;->c:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bS;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bS;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a([[II)Ljava/util/List;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/bS;->c()I

    move-result v1

    aget-object v0, p1, v1

    aget v2, v0, p2

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_2

    if-eq v0, v1, :cond_0

    aget-object v4, p1, v0

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v4, p1, v0

    aget v4, v4, p2

    if-eq v4, v2, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method static synthetic b(Lcom/google/googlenav/bS;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bT;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a([Lcom/google/googlenav/ui/view/android/aT;)[[Lcom/google/googlenav/bO;
    .locals 9

    const/16 v4, 0xf

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->h()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_1

    invoke-interface {v1, v5, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    :cond_1
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/bS;->a:Lcom/google/googlenav/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->j()[I

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    array-length v3, p1

    filled-new-array {v0, v3}, [I

    move-result-object v0

    const-class v3, Lcom/google/googlenav/bO;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Lcom/google/googlenav/bO;

    iget-object v0, p0, Lcom/google/googlenav/bS;->a:Lcom/google/googlenav/bN;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bN;Ljava/util/List;[I[Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;I)I

    move-result v1

    move v0, v5

    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_2

    aget-object v3, v4, v1

    new-instance v7, Lcom/google/googlenav/bO;

    const-string v8, ":"

    invoke-direct {v7, v8, v5}, Lcom/google/googlenav/bO;-><init>(Ljava/lang/String;Z)V

    aput-object v7, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/bS;->a:Lcom/google/googlenav/bN;

    move-object v1, v6

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bN;Ljava/util/List;[I[Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;I)I

    return-object v4
.end method

.method public b()[Lcom/google/googlenav/ui/view/android/aT;
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [[I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->j()[I

    move-result-object v0

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/bS;->c()I

    move-result v0

    aget-object v1, v3, v0

    array-length v0, v1

    new-array v4, v0, [Lcom/google/googlenav/ui/view/android/aT;

    :goto_1
    array-length v0, v1

    if-ge v2, v0, :cond_3

    invoke-direct {p0, v3, v2}, Lcom/google/googlenav/bS;->a([[II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_2
    new-instance v5, Lcom/google/googlenav/ui/view/android/aT;

    iget-object v0, p0, Lcom/google/googlenav/bS;->a:Lcom/google/googlenav/bN;

    invoke-static {v0}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bN;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    aget v6, v1, v2

    invoke-virtual {v0, v6}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cp;

    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/googlenav/ui/view/android/aT;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v7, 0x0

    aput-object v7, v3, v0

    iget-object v7, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bT;

    new-instance v7, Lcom/google/googlenav/bQ;

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->f()I

    move-result v0

    invoke-direct {v7, v8, v0}, Lcom/google/googlenav/bQ;-><init>(Ljava/lang/String;I)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v2, -0x1

    aget-object v0, v4, v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/view/android/aT;->a(Ljava/util/List;)V

    goto :goto_2

    :cond_3
    return-object v4
.end method

.method public c()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bS;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/googlenav/bS;->a:Lcom/google/googlenav/bN;

    invoke-virtual {v1}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
