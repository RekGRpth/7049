.class public Lcom/google/googlenav/bV;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/bN;

.field private final b:Ljava/lang/String;

.field private final c:[I

.field private final d:I

.field private final e:Lcom/google/googlenav/co;

.field private f:I

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bN;Ljava/lang/String;Lcom/google/googlenav/co;[III)V
    .locals 2

    const/4 v0, -0x1

    iput-object p1, p0, Lcom/google/googlenav/bV;->a:Lcom/google/googlenav/bN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/googlenav/bV;->f:I

    iput-object p2, p0, Lcom/google/googlenav/bV;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/bV;->e:Lcom/google/googlenav/co;

    iput-object p4, p0, Lcom/google/googlenav/bV;->c:[I

    iput p6, p0, Lcom/google/googlenav/bV;->d:I

    iput p5, p0, Lcom/google/googlenav/bV;->g:I

    if-eq p5, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v1, p4

    if-ge v0, v1, :cond_0

    aget v1, p4, v0

    if-ne v1, p5, :cond_1

    iput v0, p0, Lcom/google/googlenav/bV;->f:I

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/bV;)[I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bV;->c:[I

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bV;->c:[I

    array-length v0, v0

    return v0
.end method

.method public a(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bV;->c:[I

    aget v0, v0, p1

    return v0
.end method

.method public b(I)Lcom/google/googlenav/bZ;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bV;->a:Lcom/google/googlenav/bN;

    invoke-static {v0}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bN;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/bV;->c:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cp;

    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bV;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/bV;->d:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/bV;->f:I

    return v0
.end method
