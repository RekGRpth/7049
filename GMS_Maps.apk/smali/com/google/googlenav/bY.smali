.class Lcom/google/googlenav/bY;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:[Lcom/google/googlenav/bX;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .locals 6

    const/4 v5, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v0, v1, [Lcom/google/googlenav/bX;

    iput-object v0, p0, Lcom/google/googlenav/bY;->a:[Lcom/google/googlenav/bX;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/bY;->a:[Lcom/google/googlenav/bX;

    new-instance v3, Lcom/google/googlenav/bX;

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bX;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput p2, p0, Lcom/google/googlenav/bY;->b:I

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/bY;)[Lcom/google/googlenav/bX;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bY;->a:[Lcom/google/googlenav/bX;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/bY;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/bY;->b:I

    return v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/bY;)I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/bY;->a:[Lcom/google/googlenav/bX;

    aget-object v0, v0, v2

    iget-object v1, p1, Lcom/google/googlenav/bY;->a:[Lcom/google/googlenav/bX;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bX;->a(Lcom/google/googlenav/bX;)I

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/googlenav/bY;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/bY;->a(Lcom/google/googlenav/bY;)I

    move-result v0

    return v0
.end method
