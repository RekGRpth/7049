.class public Lcom/google/googlenav/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public A:Z

.field public final B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field public final C:I

.field public final D:LaW/R;

.field public final E:LaN/H;

.field public final F:Z

.field public final G:Z

.field public final H:Z

.field public final I:Z

.field public final J:Ljava/lang/String;

.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:LaN/H;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/google/googlenav/ba;

.field public final i:Lcom/google/googlenav/bb;

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:LaN/M;

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:LaN/B;

.field public final q:Z

.field public final r:I

.field public final s:I

.field public final t:Z

.field public final u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field public final v:Ljava/lang/String;

.field public final w:Ljava/util/Map;

.field public final x:Ljava/util/Set;

.field public final y:Z

.field public final z:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;IIIILaN/H;Ljava/lang/String;Lcom/google/googlenav/ba;Lcom/google/googlenav/bb;ILjava/lang/String;LaN/M;ZZZLaN/B;ZIIZ[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/util/Map;ZZZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;ILaW/R;LaN/H;ZZZZLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/googlenav/bf;->b:I

    iput p3, p0, Lcom/google/googlenav/bf;->c:I

    iput p4, p0, Lcom/google/googlenav/bf;->d:I

    iput p5, p0, Lcom/google/googlenav/bf;->e:I

    iput-object p6, p0, Lcom/google/googlenav/bf;->f:LaN/H;

    iput-object p7, p0, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/googlenav/bf;->h:Lcom/google/googlenav/ba;

    iput-object p9, p0, Lcom/google/googlenav/bf;->i:Lcom/google/googlenav/bb;

    iput p10, p0, Lcom/google/googlenav/bf;->j:I

    iput-object p11, p0, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/googlenav/bf;->l:LaN/M;

    iput-boolean p13, p0, Lcom/google/googlenav/bf;->m:Z

    iput-boolean p14, p0, Lcom/google/googlenav/bf;->n:Z

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->o:Z

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/googlenav/bf;->p:LaN/B;

    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->q:Z

    move/from16 v0, p18

    iput v0, p0, Lcom/google/googlenav/bf;->r:I

    move/from16 v0, p19

    iput v0, p0, Lcom/google/googlenav/bf;->s:I

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->t:Z

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/googlenav/bf;->v:Ljava/lang/String;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    move/from16 v0, p24

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->z:Z

    move/from16 v0, p25

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->y:Z

    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->A:Z

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    move/from16 v0, p29

    iput v0, p0, Lcom/google/googlenav/bf;->C:I

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/googlenav/bf;->D:LaW/R;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/googlenav/bf;->E:LaN/H;

    move/from16 v0, p32

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->F:Z

    move/from16 v0, p33

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->G:Z

    move/from16 v0, p34

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->H:Z

    move/from16 v0, p35

    iput-boolean v0, p0, Lcom/google/googlenav/bf;->I:Z

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/googlenav/bf;->J:Ljava/lang/String;

    return-void
.end method
