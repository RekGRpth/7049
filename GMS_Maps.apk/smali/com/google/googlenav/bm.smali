.class public Lcom/google/googlenav/bm;
.super Lcom/google/googlenav/friend/bi;
.source "SourceFile"


# static fields
.field static final synthetic a:Z

.field private static e:Lcom/google/googlenav/bm;

.field private static final i:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final f:Ljava/util/concurrent/Future;

.field private volatile g:Ljava/util/concurrent/Future;

.field private volatile h:Lcom/google/googlenav/bu;

.field private j:Lcom/google/googlenav/br;

.field private final k:Lcom/google/googlenav/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/bm;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/bm;->a:Z

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/bm;->i:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;Lcom/google/googlenav/friend/d;)V
    .locals 2

    invoke-direct {p0, p1, p2, p4}, Lcom/google/googlenav/friend/bi;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/d;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bm;->j:Lcom/google/googlenav/br;

    iput-object p3, p0, Lcom/google/googlenav/bm;->k:Lcom/google/googlenav/J;

    sget-object v0, Lcom/google/googlenav/bm;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/googlenav/bn;

    invoke-direct {v1, p0}, Lcom/google/googlenav/bn;-><init>(Lcom/google/googlenav/bm;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bm;->f:Ljava/util/concurrent/Future;

    return-void
.end method

.method public static declared-synchronized a()Lcom/google/googlenav/bm;
    .locals 2

    const-class v0, Lcom/google/googlenav/bm;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/bm;->e:Lcom/google/googlenav/bm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/bm;
    .locals 4

    const-class v1, Lcom/google/googlenav/bm;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/googlenav/bm;

    new-instance v2, Lcom/google/googlenav/friend/e;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/google/googlenav/friend/e;-><init>(I)V

    invoke-direct {v0, p0, p1, p2, v2}, Lcom/google/googlenav/bm;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;Lcom/google/googlenav/friend/d;)V

    sput-object v0, Lcom/google/googlenav/bm;->e:Lcom/google/googlenav/bm;

    sget-object v0, Lcom/google/googlenav/bm;->e:Lcom/google/googlenav/bm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/bm;)Lcom/google/googlenav/bu;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/bm;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/bm;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bu;->a(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/bm;->k()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/bm;)Lcom/google/googlenav/br;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/bm;->o()Lcom/google/googlenav/br;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized b()Z
    .locals 2

    const-class v1, Lcom/google/googlenav/bm;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/bm;->e:Lcom/google/googlenav/bm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic c(Lcom/google/googlenav/bm;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/bm;->s()V

    return-void
.end method

.method public static e()Lcom/google/googlenav/bu;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-direct {v0}, Lcom/google/googlenav/bm;->l()Lcom/google/googlenav/bu;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static j()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private k()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/bm;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/googlenav/bp;

    invoke-direct {v1, p0}, Lcom/google/googlenav/bp;-><init>(Lcom/google/googlenav/bm;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bm;->g:Ljava/util/concurrent/Future;

    :cond_0
    return-void
.end method

.method private l()Lcom/google/googlenav/bu;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/bm;->f:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bu;

    iput-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Lcom/google/googlenav/br;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bm;->j:Lcom/google/googlenav/br;

    return-object v0
.end method


# virtual methods
.method public B_()V
    .locals 7

    const/16 v3, 0x420

    sget-boolean v0, Lcom/google/googlenav/bm;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bm;->j:Lcom/google/googlenav/br;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/bm;->r()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, ""

    sget-object v1, Lcom/google/googlenav/bq;->a:[I

    iget-object v2, p0, Lcom/google/googlenav/bm;->j:Lcom/google/googlenav/br;

    invoke-virtual {v2}, Lcom/google/googlenav/br;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/bm;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x417

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/google/googlenav/bo;

    invoke-direct {v2, p0}, Lcom/google/googlenav/bo;-><init>(Lcom/google/googlenav/bm;)V

    iget-object v0, p0, Lcom/google/googlenav/bm;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    invoke-virtual {p0}, Lcom/google/googlenav/bm;->C_()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized M_()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/bm;->a_(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bu;

    invoke-super {p0}, Lcom/google/googlenav/friend/bi;->M_()V

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aC;->b()V

    invoke-static {}, LaM/j;->a()LaM/j;

    move-result-object v0

    invoke-virtual {v0}, LaM/j;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bm;->k:Lcom/google/googlenav/J;

    check-cast v0, Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)Lcom/google/googlenav/friend/br;
    .locals 6

    new-instance v0, Lcom/google/googlenav/bs;

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/bs;-><init>(Lcom/google/googlenav/bm;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;Lcom/google/googlenav/bu;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/br;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bm;->j:Lcom/google/googlenav/br;

    invoke-virtual {p0}, Lcom/google/googlenav/bm;->B_()V

    return-void
.end method

.method public a_(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bu;
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/bu;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    invoke-direct {p0}, Lcom/google/googlenav/bm;->k()V

    iget-object v0, p0, Lcom/google/googlenav/bm;->h:Lcom/google/googlenav/bu;

    return-object v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/googlenav/friend/bi;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/bm;->a_(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bu;

    return-void
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/bm;->a_(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bu;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, v0, Lcom/google/googlenav/bu;->j:Z

    goto :goto_0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    const-string v0, "SPICY_TERMS_ACCEPTED_2"

    return-object v0
.end method

.method protected g()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public h()Ljava/util/List;
    .locals 3

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
