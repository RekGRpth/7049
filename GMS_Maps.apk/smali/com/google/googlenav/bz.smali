.class public Lcom/google/googlenav/bz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/C;
.implements LaR/ab;
.implements Lcom/google/googlenav/F;


# instance fields
.field private a:I

.field private b:B

.field private c:Lcom/google/googlenav/by;

.field private volatile d:Lcom/google/googlenav/bB;

.field private final e:LaR/u;

.field private final f:LaR/aa;

.field private final g:Lbf/k;

.field private final h:Lcom/google/googlenav/bC;


# direct methods
.method public constructor <init>(LaR/u;LaR/aa;Lbf/k;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/bC;

    invoke-direct {v0, p1}, Lcom/google/googlenav/bC;-><init>(LaR/u;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/googlenav/bz;-><init>(LaR/u;LaR/aa;Lbf/k;Lcom/google/googlenav/bC;)V

    return-void
.end method

.method constructor <init>(LaR/u;LaR/aa;Lbf/k;Lcom/google/googlenav/bC;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/bz;->a:I

    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    new-instance v0, Lcom/google/googlenav/bB;

    invoke-direct {v0}, Lcom/google/googlenav/bB;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iput-object p1, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    iput-object p2, p0, Lcom/google/googlenav/bz;->f:LaR/aa;

    iput-object p3, p0, Lcom/google/googlenav/bz;->g:Lbf/k;

    iput-object p4, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-interface {p1, p0}, LaR/u;->a(LaR/C;)V

    invoke-interface {p2, p0}, LaR/aa;->a(LaR/ab;)V

    invoke-static {p4}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    return-void
.end method

.method static synthetic a(LaR/D;)Lcom/google/googlenav/by;
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/bz;->b(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaR/D;LaR/H;)Lcom/google/googlenav/by;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/bz;->b(LaR/D;LaR/H;)Lcom/google/googlenav/by;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/bz;)Lcom/google/googlenav/by;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/bz;Lcom/google/googlenav/by;)Lcom/google/googlenav/by;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/bz;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/bz;)LaR/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    return-object v0
.end method

.method private static b(LaR/D;)Lcom/google/googlenav/by;
    .locals 1

    new-instance v0, Lcom/google/googlenav/by;

    invoke-direct {v0, p0}, Lcom/google/googlenav/by;-><init>(LaR/D;)V

    return-object v0
.end method

.method private static b(LaR/D;LaR/H;)Lcom/google/googlenav/by;
    .locals 3

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v0, Lcom/google/googlenav/by;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/by;-><init>(LaR/D;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    invoke-static {p0}, Lcom/google/googlenav/bz;->b(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z
    .locals 2

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    invoke-virtual {v0}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/bz;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, -0x2

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/googlenav/bz;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public F_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    return-void
.end method

.method public a(Ljava/lang/String;)I
    .locals 2

    const/4 v1, -0x1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v0, v0, Lcom/google/googlenav/bB;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    invoke-interface {v0, p0}, LaR/u;->b(LaR/C;)V

    iget-object v0, p0, Lcom/google/googlenav/bz;->f:LaR/aa;

    invoke-interface {v0, p0}, LaR/aa;->b(LaR/ab;)V

    return-void
.end method

.method public declared-synchronized a(B)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-byte p1, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/googlenav/bz;->a:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/googlenav/bz;->d(I)Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    invoke-static {v0, v1}, Lcom/google/googlenav/bz;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    :cond_2
    :goto_1
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    invoke-static {v0}, Lcom/google/googlenav/bz;->b(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a(LaR/P;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/bA;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bA;-><init>(Lcom/google/googlenav/bz;Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/googlenav/bA;->g()V

    return-void
.end method

.method public a(Z)Z
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v1, v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;I)V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v1, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v1}, Lcom/google/googlenav/bC;->b(Lcom/google/googlenav/bC;)Lcom/google/googlenav/bB;

    move-result-object v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    monitor-exit p0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->b(Lcom/google/googlenav/bC;)Lcom/google/googlenav/bB;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    invoke-direct {p0}, Lcom/google/googlenav/bz;->g()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/bz;->g:Lbf/k;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/bz;->g:Lbf/k;

    invoke-interface {v1, v0}, Lbf/k;->c(Lbf/i;)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public synthetic b(I)Lcom/google/googlenav/E;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/bz;->d(I)Lcom/google/googlenav/ai;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b()Lcom/google/googlenav/by;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    return-void
.end method

.method public declared-synchronized c()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/googlenav/bz;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(I)I
    .locals 0

    return p1
.end method

.method public declared-synchronized d()B
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(I)Lcom/google/googlenav/ai;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v0, v0, Lcom/google/googlenav/bB;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic e()Lcom/google/googlenav/E;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v0, v0, Lcom/google/googlenav/bB;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    return v0
.end method
