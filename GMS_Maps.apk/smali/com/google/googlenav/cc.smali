.class public Lcom/google/googlenav/cc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:I

.field private final c:Z

.field private final d:I

.field private final e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v2, 0x3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/cc;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/cc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/cc;->d:I

    invoke-static {p1, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/cc;->b:I

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/cc;->c:Z

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/cc;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cc;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/cc;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/cc;->c()J

    move-result-wide v3

    const-wide/16 v5, 0x1518

    cmp-long v0, v3, v5

    if-gtz v0, :cond_1

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lcom/google/googlenav/cc;->c:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/googlenav/cc;->d:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    :goto_1
    and-int/2addr v0, v1

    :cond_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cc;->c:Z

    return v0
.end method

.method public c()J
    .locals 6

    invoke-virtual {p0}, Lcom/google/googlenav/cc;->f()J

    move-result-wide v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v4, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/cc;->f()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/cc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/cc;->d:I

    return v0
.end method

.method public f()J
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/cc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    iget v2, p0, Lcom/google/googlenav/cc;->b:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method
