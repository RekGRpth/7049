.class public Lcom/google/googlenav/ch;
.super Lcom/google/googlenav/cn;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ci;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ci;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/cn;-><init>(Lcom/google/googlenav/bZ;I)V

    iput-object p2, p0, Lcom/google/googlenav/ch;->a:Lcom/google/googlenav/ci;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ch;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/io/DataInput;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/google/googlenav/cn;->a(Ljava/io/DataInput;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic b()I
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/cn;->b()I

    move-result v0

    return v0
.end method

.method public declared-synchronized d_()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ch;->a:Lcom/google/googlenav/ci;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ch;->a:Lcom/google/googlenav/ci;

    invoke-interface {v0}, Lcom/google/googlenav/ci;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic i()Lcom/google/googlenav/bZ;
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/cn;->i()Lcom/google/googlenav/bZ;

    move-result-object v0

    return-object v0
.end method

.method public u_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ch;->a:Lcom/google/googlenav/ci;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ch;->a:Lcom/google/googlenav/ci;

    invoke-interface {v0}, Lcom/google/googlenav/ci;->b()V

    :cond_0
    return-void
.end method
