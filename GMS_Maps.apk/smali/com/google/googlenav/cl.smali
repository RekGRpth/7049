.class public final Lcom/google/googlenav/cl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final c:J

.field private final d:J

.field private final e:Lcom/google/googlenav/ab;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0xa

    const/16 v4, 0x9

    const/16 v1, 0x8

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/cl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p1, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/cl;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/google/googlenav/cl;->c:J

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    :cond_0
    iput-wide v2, p0, Lcom/google/googlenav/cl;->d:J

    :goto_1
    if-eqz p1, :cond_3

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-interface {p2, v0}, Lcom/google/googlenav/cd;->b(I)Lcom/google/googlenav/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/cl;->e:Lcom/google/googlenav/ab;

    :goto_2
    return-void

    :cond_1
    move-wide v0, v2

    goto :goto_0

    :cond_2
    iput-object v6, p0, Lcom/google/googlenav/cl;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide v2, p0, Lcom/google/googlenav/cl;->c:J

    iput-wide v2, p0, Lcom/google/googlenav/cl;->d:J

    goto :goto_1

    :cond_3
    iput-object v6, p0, Lcom/google/googlenav/cl;->e:Lcom/google/googlenav/ab;

    goto :goto_2
.end method

.method private a(I)I
    .locals 7

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-double v3, v0

    const-wide v5, 0x3fcb367a0f9096bcL

    mul-double/2addr v3, v5

    int-to-double v0, v1

    const-wide v5, 0x3fe6e2eb1c432ca5L

    mul-double/2addr v0, v5

    add-double/2addr v0, v3

    int-to-double v2, v2

    const-wide v4, 0x3fb27bb2fec56d5dL

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x4066500000000000L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x1000000

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/cl;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/cl;->c:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/cl;->d:J

    return-wide v0
.end method

.method public e()I
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/cl;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/cl;->f()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/googlenav/cl;->a(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    return v0
.end method

.method public f()I
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/cl;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    return v0
.end method

.method public g()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cl;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
