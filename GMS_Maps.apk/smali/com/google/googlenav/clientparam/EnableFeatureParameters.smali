.class public final Lcom/google/googlenav/clientparam/EnableFeatureParameters;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile isActivePlaceSuggestionsEnabled:Z

.field private volatile isAndroidNativeNetworkLocationProviderEnabled:Z

.field private volatile isBicyclingEnabled:Z

.field private volatile isBuzzEnabled:Z

.field private volatile isCheckinAddPlaceEnabled:Z

.field private volatile isDownloadMapAreaEnabled:Z

.field private volatile isGaiaLoginEnabled:Z

.field private volatile isGooglePlusEnabled:Z

.field private volatile isKmlSearchEnabled:Z

.field private volatile isLatitudeEnabled:Z

.field private volatile isMyMapsEnabled:Z

.field private volatile isOffersFeatureSwitcherEnabled:Z

.field private volatile isOfflineMapsEnabled:Z

.field private volatile isPanoramaPhotoUploadEnabled:Z

.field private volatile isPersonalizedSmartMapsDisabled:Z

.field private volatile isPertileEnabled:Z

.field private volatile isPhotoUploadEnabled:Z

.field private volatile isRasterMapsEnabled:Z

.field private volatile isRatingsReviewTextEnabled:Z

.field private volatile isRoadLayersEnabled:Z

.field private volatile isSesameMapDataEditsEnabled:Z

.field private volatile isStarringSyncEnabled:Z

.field private volatile isTilePrefetchingEnabled:Z

.field private volatile isUserGeneratedContentEnabled:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPersonalizedSmartMapsDisabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRoadLayersEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->mergeFlags(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public static getDefaultProto()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    return-object v0
.end method

.method private mergeFlags(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    if-eqz v0, :cond_6

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPersonalizedSmartMapsDisabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    if-eqz v0, :cond_11

    const/16 v0, 0x13

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    if-eqz v0, :cond_13

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    if-eqz v0, :cond_14

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_14

    :goto_14
    iput-boolean v1, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_7

    :cond_8
    move v0, v2

    goto/16 :goto_8

    :cond_9
    move v0, v2

    goto/16 :goto_9

    :cond_a
    move v0, v2

    goto/16 :goto_a

    :cond_b
    move v0, v2

    goto/16 :goto_b

    :cond_c
    move v0, v2

    goto/16 :goto_c

    :cond_d
    move v0, v2

    goto/16 :goto_d

    :cond_e
    move v0, v2

    goto/16 :goto_e

    :cond_f
    move v0, v2

    goto/16 :goto_f

    :cond_10
    move v0, v2

    goto :goto_10

    :cond_11
    move v0, v2

    goto :goto_11

    :cond_12
    move v0, v2

    goto :goto_12

    :cond_13
    move v0, v2

    goto :goto_13

    :cond_14
    move v1, v2

    goto :goto_14
.end method


# virtual methods
.method public isActivePlaceSuggestionsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    return v0
.end method

.method public isAndroidNativeNetworkLocationProviderEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    return v0
.end method

.method public isBicyclingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    return v0
.end method

.method public isBuzzEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    return v0
.end method

.method public isCheckinAddPlaceEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    return v0
.end method

.method public isDownloadMapAreaEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    return v0
.end method

.method public isGaiaLoginEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    return v0
.end method

.method public isGooglePlusEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    return v0
.end method

.method public isKmlSearchEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    return v0
.end method

.method public isLatitudeEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    return v0
.end method

.method public isMyMapsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    return v0
.end method

.method public isOffersFeatureSwitcherEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled:Z

    return v0
.end method

.method public isOfflineMapsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    return v0
.end method

.method public isPanoramaPhotoUploadEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    return v0
.end method

.method public isPersonalizedSmartMapsDisabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPersonalizedSmartMapsDisabled:Z

    return v0
.end method

.method public isPertileEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    return v0
.end method

.method public isPhotoUploadEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    return v0
.end method

.method public isRasterMapsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    return v0
.end method

.method public isRatingsReviewTextEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    return v0
.end method

.method public isRoadLayersEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRoadLayersEnabled:Z

    return v0
.end method

.method public isSesameMapDataEditsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    return v0
.end method

.method public isStarringSyncEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    return v0
.end method

.method public isTilePrefetchingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    return v0
.end method

.method public isUserGeneratedContentEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    return v0
.end method

.method public mergeEnabledFeatures(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->mergeFlags(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method
