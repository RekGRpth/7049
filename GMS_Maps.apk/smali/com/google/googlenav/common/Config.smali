.class public Lcom/google/googlenav/common/Config;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/d;


# static fields
.field protected static final a:Ljava/lang/Object;

.field public static b:[Ljava/lang/String;

.field private static f:Lcom/google/googlenav/common/Config;

.field private static g:Ljava/lang/String;

.field private static volatile h:Ljava/lang/Boolean;

.field private static volatile i:Ljava/lang/Boolean;

.field private static p:Ljava/lang/Thread;


# instance fields
.field protected c:Lcom/google/googlenav/common/io/g;

.field protected d:Landroid/content/Context;

.field protected e:I

.field private final j:Lcom/google/googlenav/common/io/j;

.field private final k:Lam/h;

.field private final l:Lam/b;

.field private m:F

.field private final n:F

.field private final o:F

.field private volatile q:Z

.field private r:Lcom/google/googlenav/common/e;

.field private final s:Lcom/google/googlenav/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/common/Config;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/high16 v2, 0x43200000

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/common/Config;->q:Z

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->s:Lcom/google/googlenav/common/a;

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    iput-object v1, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    iput-object v1, p0, Lcom/google/googlenav/common/Config;->j:Lcom/google/googlenav/common/io/j;

    iput-object v1, p0, Lcom/google/googlenav/common/Config;->k:Lam/h;

    iput-object v1, p0, Lcom/google/googlenav/common/Config;->l:Lam/b;

    iput-object v1, p0, Lcom/google/googlenav/common/Config;->c:Lcom/google/googlenav/common/io/g;

    const/16 v0, 0xa0

    iput v0, p0, Lcom/google/googlenav/common/Config;->e:I

    iput v2, p0, Lcom/google/googlenav/common/Config;->n:F

    iput v2, p0, Lcom/google/googlenav/common/Config;->o:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/googlenav/common/Config;->m:F

    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->G()V

    invoke-static {p0}, Lcom/google/googlenav/common/Config;->a(Lcom/google/googlenav/common/Config;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/h;

    invoke-direct {v0}, Lcom/google/googlenav/common/io/h;-><init>()V

    :goto_0
    new-instance v1, Lan/a;

    invoke-direct {v1, p1}, Lan/a;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/common/Config;-><init>(Landroid/content/Context;Lcom/google/googlenav/common/io/j;Lam/h;)V

    return-void

    :cond_0
    new-instance v0, Lap/d;

    invoke-direct {v0, p1}, Lap/d;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/googlenav/common/io/j;Lam/h;)V
    .locals 6

    const-wide/high16 v4, 0x3fd0000000000000L

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/common/Config;->q:Z

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->s:Lcom/google/googlenav/common/a;

    iput-object p1, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/googlenav/common/Config;->j:Lcom/google/googlenav/common/io/j;

    iput-object p3, p0, Lcom/google/googlenav/common/Config;->k:Lam/h;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->p:Ljava/lang/Thread;

    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->G()V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p0, Lcom/google/googlenav/common/Config;->e:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->m:F

    :goto_0
    iget v0, p0, Lcom/google/googlenav/common/Config;->e:I

    int-to-float v0, v0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    iget v2, v1, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-lez v2, :cond_2

    :cond_0
    iput v0, p0, Lcom/google/googlenav/common/Config;->n:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->o:F

    :goto_1
    new-instance v0, Lan/d;

    invoke-direct {v0}, Lan/d;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->l:Lam/b;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/Config;->a(Ljava/util/Locale;)V

    invoke-static {p0}, Lcom/google/googlenav/common/Config;->a(Lcom/google/googlenav/common/Config;)V

    new-instance v0, Lap/a;

    invoke-direct {v0, p1}, Lap/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->c:Lcom/google/googlenav/common/io/g;

    return-void

    :cond_1
    const/16 v0, 0xa0

    iput v0, p0, Lcom/google/googlenav/common/Config;->e:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/googlenav/common/Config;->m:F

    goto :goto_0

    :cond_2
    iget v0, v1, Landroid/util/DisplayMetrics;->xdpi:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->n:F

    iget v0, v1, Landroid/util/DisplayMetrics;->ydpi:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->o:F

    goto :goto_1

    :cond_3
    iput v0, p0, Lcom/google/googlenav/common/Config;->n:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->o:F

    goto :goto_1
.end method

.method public static F()Ljava/lang/String;
    .locals 7

    const/16 v6, 0x5f

    const/16 v5, 0x2d

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private G()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;)Lcom/google/googlenav/common/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->c()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "unknown"

    sput-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->b()V

    return-void
.end method

.method private H()[Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/googlenav/common/Config;->b:[Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "en ar bg ca cs da de el en_GB es es_MX et fi fr hr hu it iw ja ko lt lv nl no pl pt_BR pt_PT ro ru sk sl sr sv tl tr uk vi zh zh_CN"

    const-string v1, " "

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/Config;->b([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->b:[Ljava/lang/String;

    :cond_0
    sget-object v0, Lcom/google/googlenav/common/Config;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public static a()Lcom/google/googlenav/common/Config;
    .locals 2

    sget-object v1, Lcom/google/googlenav/common/Config;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p0, p1}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/google/googlenav/common/e;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/googlenav/common/e;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected static a(Lcom/google/googlenav/common/Config;)V
    .locals 0

    sput-object p0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/DataInput;Z)[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;Ljava/io/DataInput;Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/common/e;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.google.settings/partner"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method public static declared-synchronized e()Ljava/lang/String;
    .locals 2

    const-class v1, Lcom/google/googlenav/common/Config;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->b()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized f()Ljava/lang/String;
    .locals 2

    const-class v1, Lcom/google/googlenav/common/Config;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;
    .locals 2

    sget-object v1, Lcom/google/googlenav/common/Config;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/Config;

    invoke-direct {v0, p0}, Lcom/google/googlenav/common/Config;-><init>(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static j()Z
    .locals 1

    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->d()Z

    move-result v0

    return v0
.end method

.method public static w()Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "ja"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/common/Config;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.touchscreen.multitouch"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->i:Ljava/lang/Boolean;

    :cond_0
    sget-object v0, Lcom/google/googlenav/common/Config;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public B()Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/common/Config;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->h:Ljava/lang/Boolean;

    :cond_0
    sget-object v0, Lcom/google/googlenav/common/Config;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected C()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()Landroid/location/LocationManager;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    return-object v0
.end method

.method public E()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    return-object v0
.end method

.method public a(D)I
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/j;->a(D)I

    move-result v0

    return v0
.end method

.method public a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 3

    new-instance v0, Ljava/util/zip/InflaterInputStream;

    new-instance v1, Ljava/util/zip/Inflater;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v0, p1, v1}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JJ)Ljava/lang/String;
    .locals 6

    const-wide/32 v4, 0xea60

    move-wide v0, p1

    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->H()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/Config;->a(Ljava/util/Locale;)V

    return-void
.end method

.method public a(Ljava/util/Locale;)V
    .locals 3

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->g()Lcom/google/googlenav/common/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/e;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->g()Lcom/google/googlenav/common/e;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->H()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/common/Config;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/e;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/e;->a([Ljava/lang/String;)V

    return-void
.end method

.method protected b()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/common/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/common/b;-><init>(Lcom/google/googlenav/common/Config;)V

    invoke-static {v0}, Lcom/google/googlenav/common/io/e;->a(Lcom/google/googlenav/common/io/f;)V

    return-void
.end method

.method public b([Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->C()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    invoke-static {v3}, Lcom/google/googlenav/common/e;->g(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method public c(I)I
    .locals 4

    int-to-double v0, p1

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/j;->a(D)I

    move-result v0

    return v0
.end method

.method protected c()Ljava/lang/String;
    .locals 3

    const-string v0, "Web"

    const-string v0, "maps_client_id"

    invoke-direct {p0, v0}, Lcom/google/googlenav/common/Config;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "Web"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->c()Z

    move-result v0

    return v0
.end method

.method protected g()Lcom/google/googlenav/common/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    return-object v0
.end method

.method public i()I
    .locals 1

    const/16 v0, 0x22

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    const-string v0, "6.14.3"

    return-object v0
.end method

.method public l()Lcom/google/googlenav/common/io/g;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->c:Lcom/google/googlenav/common/io/g;

    return-object v0
.end method

.method public m()Lcom/google/googlenav/common/io/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->j:Lcom/google/googlenav/common/io/j;

    return-object v0
.end method

.method public n()Lcom/google/googlenav/common/j;
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/j;

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/j;-><init>(Lcom/google/googlenav/common/io/j;)V

    return-object v0
.end method

.method public o()Lam/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->k:Lam/h;

    return-object v0
.end method

.method public p()Lam/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->l:Lam/b;

    return-object v0
.end method

.method public q()Lcom/google/googlenav/common/d;
    .locals 0

    return-object p0
.end method

.method public r()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const-string v0, "Unknown"

    :goto_1
    return-object v0

    :pswitch_0
    const-string v0, "WiFi"

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    const-string v0, "GPRS"

    goto :goto_1

    :pswitch_3
    const-string v0, "UMTS"

    goto :goto_1

    :pswitch_4
    const-string v0, "EDGE"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public s()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/common/Config;->e:I

    return v0
.end method

.method public t()F
    .locals 1

    iget v0, p0, Lcom/google/googlenav/common/Config;->n:F

    return v0
.end method

.method public u()F
    .locals 1

    iget v0, p0, Lcom/google/googlenav/common/Config;->o:F

    return v0
.end method

.method public v()Lcom/google/googlenav/common/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/common/Config;->s:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method public x()D
    .locals 2

    iget v0, p0, Lcom/google/googlenav/common/Config;->m:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public y()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->s()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/common/Config;->p:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
