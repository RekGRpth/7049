.class public Lcom/google/googlenav/common/util/v;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V
    .locals 9

    invoke-virtual {p0}, Lcom/google/googlenav/common/f;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v4

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-lez v6, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v6

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {p0, v6, v7, v8}, Lcom/google/googlenav/common/f;->a(JI)V

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v6

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {p1, v6, v7, v8}, Lcom/google/googlenav/common/f;->a(JI)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/googlenav/common/f;->a(JI)V

    invoke-virtual {p1, v4, v5, v1}, Lcom/google/googlenav/common/f;->a(JI)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/common/f;->b()I

    move-result v1

    if-lt v0, v1, :cond_1

    goto :goto_0
.end method

.method public static b(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V
    .locals 9

    invoke-virtual {p0}, Lcom/google/googlenav/common/f;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v4

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gez v6, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v6

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {p0, v6, v7, v8}, Lcom/google/googlenav/common/f;->a(JI)V

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v6

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {p1, v6, v7, v8}, Lcom/google/googlenav/common/f;->a(JI)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/googlenav/common/f;->a(JI)V

    invoke-virtual {p1, v4, v5, v1}, Lcom/google/googlenav/common/f;->a(JI)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/common/f;->b()I

    move-result v1

    if-lt v0, v1, :cond_1

    goto :goto_0
.end method
