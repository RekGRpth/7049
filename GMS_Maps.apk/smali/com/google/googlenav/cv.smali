.class public Lcom/google/googlenav/cv;
.super Law/a;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Ljava/util/List;

.field private final c:I

.field private final d:J

.field private final e:Lcom/google/googlenav/cw;

.field private f:I


# direct methods
.method public constructor <init>(IJLcom/google/googlenav/cw;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/cv;->b:Ljava/util/List;

    iput p1, p0, Lcom/google/googlenav/cv;->c:I

    iput-wide p2, p0, Lcom/google/googlenav/cv;->d:J

    iput-object p4, p0, Lcom/google/googlenav/cv;->e:Lcom/google/googlenav/cw;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/iI;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/googlenav/cv;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/googlenav/cv;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v2, p0, Lcom/google/googlenav/cv;->a:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/cv;->f:I

    :cond_0
    return v1

    :cond_1
    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/cv;->f:I

    iget v0, p0, Lcom/google/googlenav/cv;->f:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/cv;->a:Z

    iget-boolean v0, p0, Lcom/google/googlenav/cv;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/cv;->b:Ljava/util/List;

    invoke-virtual {v3, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x73

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/cv;->e:Lcom/google/googlenav/cw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/cv;->e:Lcom/google/googlenav/cw;

    iget-boolean v1, p0, Lcom/google/googlenav/cv;->a:Z

    iget-object v2, p0, Lcom/google/googlenav/cv;->b:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/cw;->a(ZLjava/util/List;)V

    :cond_0
    return-void
.end method

.method public u_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/cv;->e:Lcom/google/googlenav/cw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/cv;->e:Lcom/google/googlenav/cw;

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/cw;->a(ZLjava/util/List;)V

    :cond_0
    return-void
.end method
