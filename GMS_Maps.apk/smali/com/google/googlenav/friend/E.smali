.class public Lcom/google/googlenav/friend/E;
.super Lcom/google/googlenav/friend/bi;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/googlenav/friend/E;


# instance fields
.field private e:Lbf/am;

.field private f:I

.field private g:Z


# direct methods
.method constructor <init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/e;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/e;-><init>(I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/friend/bi;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/d;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/friend/E;->f:I

    return-void
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/E;
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/E;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/googlenav/friend/E;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/friend/E;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    sput-object v0, Lcom/google/googlenav/friend/E;->a:Lcom/google/googlenav/friend/E;

    sget-object v0, Lcom/google/googlenav/friend/E;->a:Lcom/google/googlenav/friend/E;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized e()Lcom/google/googlenav/friend/E;
    .locals 2

    const-class v0, Lcom/google/googlenav/friend/E;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/friend/E;->a:Lcom/google/googlenav/friend/E;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized i()Z
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/E;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/friend/E;->a:Lcom/google/googlenav/friend/E;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static j()Z
    .locals 1

    const-string v0, "TERMS_ACCEPTED_SETTING"

    invoke-static {v0}, Lcom/google/googlenav/friend/E;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static k()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "TERMS_ACCEPTED_SETTING"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method

.method public static l()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "TERMS_ACCEPTED_SETTING"

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    return-void
.end method

.method public static n()Ljava/lang/String;
    .locals 1

    const-string v0, "TERMS_ACCEPTED_SETTING"

    return-object v0
.end method

.method public static o()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method


# virtual methods
.method public B_()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/E;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/E;->c:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x192

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1b0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized C_()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/friend/E;->j()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/friend/E;->b:Lcom/google/googlenav/android/aa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/E;->b:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/friend/F;

    invoke-direct {v2, p0}, Lcom/google/googlenav/friend/F;-><init>(Lcom/google/googlenav/friend/E;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-super {p0}, Lcom/google/googlenav/friend/bi;->C_()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized M_()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/google/googlenav/friend/bi;->M_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbf/am;->c(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->aa()Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    :cond_1
    new-instance v0, Lcom/google/googlenav/friend/G;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/G;-><init>(Lcom/google/googlenav/friend/E;)V

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->d(Lcom/google/googlenav/friend/bc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)Lcom/google/googlenav/friend/br;
    .locals 6

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->Z()I

    move-result v5

    iget v0, p0, Lcom/google/googlenav/friend/E;->f:I

    if-eq v0, v1, :cond_0

    iget v5, p0, Lcom/google/googlenav/friend/E;->f:I

    iput v1, p0, Lcom/google/googlenav/friend/E;->f:I

    :cond_0
    new-instance v0, Lcom/google/googlenav/friend/H;

    iget-object v1, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/H;-><init>(Lbf/am;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;I)V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/E;->f:I

    return-void
.end method

.method public a(Lbf/am;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/E;->e:Lbf/am;

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/U;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, Lcom/google/googlenav/friend/ae;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {v1}, Lcom/google/googlenav/friend/aH;->b(Lcom/google/googlenav/friend/ae;)V

    return-void
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/ae;

    new-instance v1, Lcom/google/googlenav/friend/U;

    invoke-direct {v1, p1}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/E;->g:Z

    iget-boolean v0, p0, Lcom/google/googlenav/friend/E;->g:Z

    return v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/E;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected m()V
    .locals 3

    const/16 v0, 0x3d

    const-string v1, "a"

    const-string v2, "s"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected p()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/E;->o()I

    move-result v0

    return v0
.end method
