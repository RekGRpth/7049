.class public Lcom/google/googlenav/friend/N;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private final c:Lcom/google/googlenav/friend/O;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(ZZLcom/google/googlenav/friend/O;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/N;->a:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/N;->b:Z

    iput-boolean p1, p0, Lcom/google/googlenav/friend/N;->a:Z

    iput-boolean p2, p0, Lcom/google/googlenav/friend/N;->b:Z

    iput-object p3, p0, Lcom/google/googlenav/friend/N;->c:Lcom/google/googlenav/friend/O;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->ac:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->K:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-boolean v2, p0, Lcom/google/googlenav/friend/N;->a:Z

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/googlenav/friend/N;->b:Z

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 11

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->ad:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v4, -0x1

    invoke-static {v1, v2, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/N;->d:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_2

    const/4 v0, 0x3

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-static {v6}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/aD;

    move-result-object v7

    iget-object v0, p0, Lcom/google/googlenav/friend/N;->d:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v8, :cond_1

    const/4 v0, 0x5

    invoke-virtual {v6, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v4, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v7}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_3

    const/4 v0, 0x4

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sget-object v6, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v6}, Lcom/google/common/collect/dh;->a(Ljava/util/Comparator;)Lcom/google/common/collect/dh;

    move-result-object v6

    invoke-static {v0, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-string v6, ", "

    invoke-static {v6}, Lcom/google/common/base/A;->a(Ljava/lang/String;)Lcom/google/common/base/A;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/common/base/A;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/googlenav/friend/N;->d:Ljava/util/List;

    invoke-static {v5, v0}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method public d_()V
    .locals 2

    invoke-super {p0}, Law/a;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/N;->c:Lcom/google/googlenav/friend/O;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/N;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/N;->c:Lcom/google/googlenav/friend/O;

    iget-object v1, p0, Lcom/google/googlenav/friend/N;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/O;->a(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/N;->c:Lcom/google/googlenav/friend/O;

    invoke-interface {v0}, Lcom/google/googlenav/friend/O;->a()V

    goto :goto_0
.end method

.method public u_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/N;->c:Lcom/google/googlenav/friend/O;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/N;->c:Lcom/google/googlenav/friend/O;

    invoke-interface {v0}, Lcom/google/googlenav/friend/O;->a()V

    :cond_0
    return-void
.end method
