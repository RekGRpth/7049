.class public Lcom/google/googlenav/friend/OptInIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/List;


# instance fields
.field b:Z

.field c:I

.field private d:Lak/a;

.field private e:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/OptInIntentService;->a:Ljava/util/List;

    sget-object v0, Lcom/google/googlenav/friend/OptInIntentService;->a:Ljava/util/List;

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/googlenav/friend/OptInIntentService;->a:Ljava/util/List;

    const-string v1, "com.google.googlenav.friend.intenttest"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "OptInIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->c:I

    return-void
.end method

.method private a(Landroid/os/Messenger;I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->b:Z

    if-eqz v0, :cond_0

    iput p2, p0, Lcom/google/googlenav/friend/OptInIntentService;->c:I

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Lcom/google/googlenav/friend/ad;->a(I)V

    const/4 v0, 0x0

    invoke-static {v0, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/friend/ad;->F()V

    goto :goto_0
.end method

.method private a(Landroid/accounts/Account;Landroid/os/Messenger;ILandroid/app/PendingIntent;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x5

    const/4 v0, 0x0

    if-nez p2, :cond_0

    invoke-static {v3}, Lcom/google/googlenav/friend/ad;->a(I)V

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0, p2, v3}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_1
    if-nez p4, :cond_2

    invoke-direct {p0, p2, v3}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/friend/OptInIntentService;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, p2, v1}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_3
    const/high16 v2, -0x80000000

    if-ne p3, v2, :cond_4

    invoke-direct {p0, p2, v3}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_4
    if-eq p3, v1, :cond_5

    const/4 v1, 0x4

    invoke-direct {p0, p2, v1}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x6

    invoke-static {}, Lcom/google/googlenav/friend/ad;->D()V

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string v1, "messenger"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/accounts/Account;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v1, v4}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "version"

    const/high16 v3, -0x80000000

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v2, "sender"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/app/PendingIntent;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/accounts/Account;Landroid/os/Messenger;ILandroid/app/PendingIntent;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->E()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/OptInIntentService;->b(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1, v4}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/os/Messenger;I)V

    goto :goto_0
.end method

.method protected a(Landroid/accounts/Account;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/googlenav/common/io/j;->a(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lak/a;->b()V

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v2, v2, v1}, LaM/j;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, LaM/f;->a(Ljava/lang/String;)V

    :cond_1
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->a()V

    return v3
.end method

.method a(Ljava/lang/String;)Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/friend/OptInIntentService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()Z
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Lcom/google/googlenav/friend/aP;

    invoke-direct {v0, v1, v1, v2, v5}, Lcom/google/googlenav/friend/aP;-><init>(ZIILcom/google/googlenav/friend/aQ;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/aP;->a(Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aP;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aR;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aR;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbO/G;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x7

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v3, Lcom/google/googlenav/friend/aM;

    invoke-direct {v3, v0, v5}, Lcom/google/googlenav/friend/aM;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/aN;)V

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aM;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aO;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aO;->a()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/OptInSyncBroadcastReceiver;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/googlenav/friend/ac;->e()Lcom/google/googlenav/friend/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bi;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/googlenav/friend/bi;->a(ZLjava/lang/String;Lcom/google/googlenav/friend/bc;)V

    invoke-static {v1}, Lcom/google/googlenav/friend/as;->a(Z)Z

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bi;->u()Lcom/google/googlenav/friend/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/friend/d;->b()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V

    move v0, v1

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/OptInIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "OptInIntentService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->e:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Lak/a;

    invoke-direct {v0}, Lak/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->d:Lak/a;

    iget-object v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->d:Lak/a;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/OptInIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lak/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/OptInIntentService;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/friend/OptInIntentService;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/googlenav/friend/OptInIntentService;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
