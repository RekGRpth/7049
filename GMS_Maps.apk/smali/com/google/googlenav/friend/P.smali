.class public Lcom/google/googlenav/friend/P;
.super Law/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/common/f;

.field private b:Lcom/google/googlenav/friend/R;

.field private c:Lcom/google/googlenav/friend/Q;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/f;Lcom/google/googlenav/friend/Q;)V
    .locals 1

    invoke-direct {p0}, Law/b;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/R;

    invoke-direct {v0}, Lcom/google/googlenav/friend/R;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/f;

    iput-object v0, p0, Lcom/google/googlenav/friend/P;->a:Lcom/google/googlenav/common/f;

    iput-object p2, p0, Lcom/google/googlenav/friend/P;->c:Lcom/google/googlenav/friend/Q;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->M:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/friend/P;->a:Lcom/google/googlenav/common/f;

    invoke-virtual {v2}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/googlenav/friend/P;->a:Lcom/google/googlenav/common/f;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x3

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x1

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->N:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3, v5, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    packed-switch v2, :pswitch_data_0

    iget-object v1, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    iput-boolean v0, v1, Lcom/google/googlenav/friend/R;->a:Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v3}, LaM/f;->a(I)V

    :goto_0
    return v5

    :pswitch_0
    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    :goto_1
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    iput-boolean v5, v1, Lcom/google/googlenav/friend/R;->a:Z

    iget-object v1, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    iput-object v0, v1, Lcom/google/googlenav/friend/R;->b:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x57

    return v0
.end method

.method public d_()V
    .locals 2

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/P;->c:Lcom/google/googlenav/friend/Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    iget-boolean v0, v0, Lcom/google/googlenav/friend/R;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/P;->c:Lcom/google/googlenav/friend/Q;

    iget-object v1, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    iget-object v1, v1, Lcom/google/googlenav/friend/R;->b:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/Q;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/P;->c:Lcom/google/googlenav/friend/Q;

    invoke-interface {v0}, Lcom/google/googlenav/friend/Q;->a()V

    goto :goto_0
.end method

.method protected synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/P;->n()Lcom/google/googlenav/friend/R;

    move-result-object v0

    return-object v0
.end method

.method protected n()Lcom/google/googlenav/friend/R;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/P;->b:Lcom/google/googlenav/friend/R;

    return-object v0
.end method
