.class public Lcom/google/googlenav/friend/S;
.super Law/b;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field protected b:Z

.field private c:Ljava/util/List;

.field private final d:Z

.field private e:Lcom/google/googlenav/friend/ao;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/friend/aq;Lcom/google/googlenav/android/aa;Z)V
    .locals 2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/ao;

    invoke-direct {v1, p3, p2}, Lcom/google/googlenav/friend/ao;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/aq;)V

    invoke-direct {p0, v0, v1, p4}, Lcom/google/googlenav/friend/S;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V
    .locals 0

    invoke-direct {p0}, Law/b;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/S;->c:Ljava/util/List;

    iput-object p2, p0, Lcom/google/googlenav/friend/S;->e:Lcom/google/googlenav/friend/ao;

    iput-boolean p3, p0, Lcom/google/googlenav/friend/S;->d:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eC;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/S;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eC;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/S;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/friend/S;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/friend/S;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/S;->b:Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/friend/S;->b:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x47

    return v0
.end method

.method public d_()V
    .locals 2

    invoke-super {p0}, Law/b;->d_()V

    iget-boolean v0, p0, Lcom/google/googlenav/friend/S;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/S;->e:Lcom/google/googlenav/friend/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/S;->e:Lcom/google/googlenav/friend/ao;

    iget-object v1, p0, Lcom/google/googlenav/friend/S;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/ao;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method protected synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/S;->n()Lcom/google/googlenav/friend/T;

    move-result-object v0

    return-object v0
.end method

.method protected n()Lcom/google/googlenav/friend/T;
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/T;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/S;->b:Z

    iget-object v2, p0, Lcom/google/googlenav/friend/S;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/T;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public s_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/S;->d:Z

    return v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
