.class public Lcom/google/googlenav/friend/W;
.super Lcom/google/googlenav/friend/bi;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/googlenav/friend/W;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/e;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/e;-><init>(I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/friend/bi;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/d;)V

    return-void
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/W;
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/W;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/googlenav/friend/W;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/friend/W;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    sput-object v0, Lcom/google/googlenav/friend/W;->a:Lcom/google/googlenav/friend/W;

    sget-object v0, Lcom/google/googlenav/friend/W;->a:Lcom/google/googlenav/friend/W;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static e()Lcom/google/googlenav/friend/W;
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/W;->a:Lcom/google/googlenav/friend/W;

    return-object v0
.end method

.method public static declared-synchronized i()Z
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/W;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/friend/W;->a:Lcom/google/googlenav/friend/W;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static j()Z
    .locals 1

    const-string v0, "GOOGLE_PLUS_TERMS_ACCEPTED_SETTING"

    invoke-static {v0}, Lcom/google/googlenav/friend/W;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public B_()V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/W;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3cb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/W;->c:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v2, 0x3df

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3e0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)Lcom/google/googlenav/friend/br;
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/X;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/googlenav/friend/X;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)V

    return-object v0
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 2

    const/4 v1, 0x1

    new-instance v0, Lcom/google/googlenav/friend/U;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    const-string v0, "GOOGLE_PLUS_TERMS_ACCEPTED_SETTING"

    return-object v0
.end method

.method protected g()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected m()V
    .locals 3

    const/16 v0, 0x5b

    const-string v1, "co"

    const-string v2, "ol"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
