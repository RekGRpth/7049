.class public Lcom/google/googlenav/friend/aB;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:J

.field private b:Z

.field private c:Lcom/google/googlenav/friend/aC;


# direct methods
.method public constructor <init>(JLcom/google/googlenav/friend/aC;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-wide p1, p0, Lcom/google/googlenav/friend/aB;->a:J

    iput-object p3, p0, Lcom/google/googlenav/friend/aB;->c:Lcom/google/googlenav/friend/aC;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->aj:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/t;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-wide v2, p0, Lcom/google/googlenav/friend/aB;->a:J

    invoke-virtual {v1, v4, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->ak:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/friend/aB;->b:Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/friend/aB;->b:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x6d

    return v0
.end method

.method public d_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aB;->c:Lcom/google/googlenav/friend/aC;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/friend/aB;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aB;->c:Lcom/google/googlenav/friend/aC;

    invoke-interface {v0}, Lcom/google/googlenav/friend/aC;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aB;->c:Lcom/google/googlenav/friend/aC;

    invoke-interface {v0}, Lcom/google/googlenav/friend/aC;->b()V

    goto :goto_0
.end method
