.class public Lcom/google/googlenav/friend/aD;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final a:Lcom/google/common/collect/Q;

.field public static final b:Lcom/google/common/collect/Q;


# instance fields
.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/googlenav/friend/aG;

.field private final j:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    invoke-static/range {v0 .. v5}, Lcom/google/common/collect/ap;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ap;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/friend/aD;->a:Lcom/google/common/collect/Q;

    sget-object v0, Lcom/google/googlenav/friend/aD;->a:Lcom/google/common/collect/Q;

    invoke-interface {v0}, Lcom/google/common/collect/Q;->b()Lcom/google/common/collect/Q;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/friend/aD;->b:Lcom/google/common/collect/Q;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/googlenav/friend/aD;->c:J

    iput-object p3, p0, Lcom/google/googlenav/friend/aD;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/friend/aD;->e:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/googlenav/friend/aD;->j:J

    iput-object p9, p0, Lcom/google/googlenav/friend/aD;->i:Lcom/google/googlenav/friend/aG;

    iput p7, p0, Lcom/google/googlenav/friend/aD;->f:I

    iput p8, p0, Lcom/google/googlenav/friend/aD;->g:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/friend/aD;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/google/googlenav/friend/aG;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/googlenav/friend/aD;->c:J

    iput-object p3, p0, Lcom/google/googlenav/friend/aD;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/friend/aD;->e:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/googlenav/friend/aD;->j:J

    iput-object p8, p0, Lcom/google/googlenav/friend/aD;->i:Lcom/google/googlenav/friend/aG;

    iput-object p7, p0, Lcom/google/googlenav/friend/aD;->h:Ljava/lang/String;

    const v0, 0x7f02029c

    iput v0, p0, Lcom/google/googlenav/friend/aD;->f:I

    const v0, 0x7f0200bb

    iput v0, p0, Lcom/google/googlenav/friend/aD;->g:I

    return-void
.end method

.method private a(Lcom/google/googlenav/friend/aG;)I
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/aD;
    .locals 10

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    const-wide v2, -0x35c1067b89114543L

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/aD;->l()Lcom/google/googlenav/friend/aD;

    move-result-object v8

    new-instance v0, Lcom/google/googlenav/friend/aD;

    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v7}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aD;->f()I

    move-result v7

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aD;->g()I

    move-result v8

    sget-object v9, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    const-wide v2, -0x411daa5aff29b7b0L

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/aD;->k()Lcom/google/googlenav/friend/aD;

    move-result-object v8

    new-instance v0, Lcom/google/googlenav/friend/aD;

    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v7}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aD;->f()I

    move-result v7

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aD;->g()I

    move-result v8

    sget-object v9, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/friend/aD;

    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v7}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    const v7, 0x7f02029c

    const v8, 0x7f0200bb

    sget-object v9, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/friend/aD;
    .locals 9

    new-instance v0, Lcom/google/googlenav/friend/aD;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v5, -0x1

    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    move-object v4, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/google/googlenav/friend/aG;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/friend/aD;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    if-ne v2, v3, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->a()J

    move-result-wide v2

    const-wide v4, -0x411daa5aff29b7b0L

    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    const-wide v4, -0x35c1067b89114543L

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .locals 12

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v11

    if-nez p0, :cond_0

    move-object v0, v11

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    move v10, v0

    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v10, v0, :cond_5

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v1, v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/aD;->i()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    const/4 v1, 0x2

    if-ne v1, v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/friend/aD;->j()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized custom target: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    const-wide v4, -0x35c1067b89114543L

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/friend/aD;->l()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    const-wide v4, -0x411daa5aff29b7b0L

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/friend/aD;->k()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    new-instance v0, Lcom/google/googlenav/friend/aD;

    const/4 v1, 0x3

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    const v7, 0x7f02029c

    const v8, 0x7f0200bb

    sget-object v9, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :pswitch_2
    new-instance v0, Lcom/google/googlenav/friend/aD;

    const/4 v1, 0x3

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    const v7, 0x7f02029c

    const v8, 0x7f0200bb

    sget-object v9, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_5
    move-object v0, v11

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static final i()Lcom/google/googlenav/friend/aD;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aE;->a()Lcom/google/googlenav/friend/aE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aE;->b()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    return-object v0
.end method

.method public static final j()Lcom/google/googlenav/friend/aD;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aE;->a()Lcom/google/googlenav/friend/aE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aE;->c()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    return-object v0
.end method

.method public static final k()Lcom/google/googlenav/friend/aD;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aE;->a()Lcom/google/googlenav/friend/aE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aE;->e()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    return-object v0
.end method

.method public static final l()Lcom/google/googlenav/friend/aD;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aE;->a()Lcom/google/googlenav/friend/aE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aE;->d()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/friend/aD;->c:J

    return-wide v0
.end method

.method public b(Lcom/google/googlenav/friend/aD;)I
    .locals 3

    if-eq p0, p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-ne v0, v2, :cond_2

    sget-object v2, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-ne v1, v2, :cond_4

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aG;)I

    move-result v0

    invoke-direct {p0, v1}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aG;)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, -0x1

    goto :goto_0

    :cond_5
    invoke-static {p0}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p1}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    invoke-static {p0}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lcom/google/googlenav/friend/aD;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/google/googlenav/friend/aE;->a()Lcom/google/googlenav/friend/aE;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/aE;->a(Lcom/google/googlenav/friend/aD;)I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/friend/aE;->a()Lcom/google/googlenav/friend/aE;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/friend/aE;->a(Lcom/google/googlenav/friend/aD;)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aD;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aD;->e:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/googlenav/friend/aD;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/aD;->b(Lcom/google/googlenav/friend/aD;)I

    move-result v0

    return v0
.end method

.method public d()Lcom/google/googlenav/friend/aG;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aD;->i:Lcom/google/googlenav/friend/aG;

    return-object v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/friend/aD;->j:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/googlenav/friend/aD;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/googlenav/friend/aD;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/aG;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/aD;->f:I

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/aD;->g:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aD;->h:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PostTarget:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/aD;->i:Lcom/google/googlenav/friend/aG;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/googlenav/friend/aD;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/aD;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
