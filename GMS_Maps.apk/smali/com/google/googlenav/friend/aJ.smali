.class public Lcom/google/googlenav/friend/aJ;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Ljava/util/Vector;

.field private final c:Lcom/google/googlenav/friend/aA;


# direct methods
.method private constructor <init>(ILjava/util/Vector;Lcom/google/googlenav/friend/aA;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput p1, p0, Lcom/google/googlenav/friend/aJ;->a:I

    iput-object p2, p0, Lcom/google/googlenav/friend/aJ;->b:Ljava/util/Vector;

    iput-object p3, p0, Lcom/google/googlenav/friend/aJ;->c:Lcom/google/googlenav/friend/aA;

    return-void
.end method

.method public constructor <init>(Ljava/util/Vector;Lcom/google/googlenav/friend/aA;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/friend/aJ;-><init>(ILjava/util/Vector;Lcom/google/googlenav/friend/aA;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 13

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->H:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->e(I)I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(I)I

    move-result v5

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->e(I)I

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(I)I

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->e(I)I

    move-result v6

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(I)I

    move-result v7

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->e(I)I

    move-result v8

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(I)I

    move-result v9

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/googlenav/friend/aJ;->a:I

    move v2, v1

    move v1, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aJ;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aJ;->b:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/n;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/wireless/googlenav/proto/j2me/bK;->G:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x1

    invoke-virtual {v0, v12, v10, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v12, 0x2

    invoke-virtual {v0, v12, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v12, 0x3

    invoke-virtual {v0, v12, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v12, 0x1

    invoke-virtual {v3, v12, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/wireless/googlenav/proto/j2me/bK;->G:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x1

    invoke-virtual {v0, v12, v10, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v12, 0x2

    invoke-virtual {v0, v12, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v12, 0x3

    invoke-virtual {v0, v12, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v12, 0x1

    invoke-virtual {v3, v12, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/wireless/googlenav/proto/j2me/bK;->G:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x1

    invoke-virtual {v0, v12, v10, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v10, 0x2

    invoke-virtual {v0, v10, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v10, 0x3

    invoke-virtual {v0, v10, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v10, 0x1

    invoke-virtual {v3, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_1
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 10

    const/4 v0, 0x0

    const/4 v5, -0x1

    const/4 v3, 0x3

    const/4 v2, 0x1

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->I:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-static {v6, v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v4, 0x2

    invoke-virtual {v6, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4, v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v4

    packed-switch v1, :pswitch_data_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v4}, LaM/f;->a(I)V

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    invoke-virtual {v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    move v4, v0

    move v5, v0

    :goto_2
    if-ge v5, v7, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/friend/aJ;->b:Ljava/util/Vector;

    iget v9, p0, Lcom/google/googlenav/friend/aJ;->a:I

    add-int/2addr v9, v4

    invoke-virtual {v0, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/n;

    const/4 v9, 0x4

    invoke-static {v0, v5, v6, v3, v9}, LaB/m;->a(LaB/n;ILcom/google/googlenav/common/io/protocol/ProtoBuf;II)LaB/m;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/2addr v5, v1

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/aJ;->c:Lcom/google/googlenav/friend/aA;

    invoke-interface {v0, v8}, Lcom/google/googlenav/friend/aA;->a(Ljava/util/Vector;)V

    iget v0, p0, Lcom/google/googlenav/friend/aJ;->a:I

    add-int/lit8 v0, v0, 0x5

    iget-object v1, p0, Lcom/google/googlenav/friend/aJ;->b:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/aJ;

    iget v3, p0, Lcom/google/googlenav/friend/aJ;->a:I

    add-int/lit8 v3, v3, 0x5

    iget-object v4, p0, Lcom/google/googlenav/friend/aJ;->b:Ljava/util/Vector;

    iget-object v5, p0, Lcom/google/googlenav/friend/aJ;->c:Lcom/google/googlenav/friend/aA;

    invoke-direct {v1, v3, v4, v5}, Lcom/google/googlenav/friend/aJ;-><init>(ILjava/util/Vector;Lcom/google/googlenav/friend/aA;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x30

    return v0
.end method
