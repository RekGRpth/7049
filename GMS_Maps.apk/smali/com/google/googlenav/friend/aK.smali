.class public Lcom/google/googlenav/friend/aK;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field private final a:Ljava/util/Vector;

.field private b:Lcom/google/googlenav/common/f;

.field private c:Lcom/google/googlenav/common/f;

.field private d:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/F;)V
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/aK;->d:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    invoke-interface {p1, v3}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    new-instance v5, Lcom/google/googlenav/friend/aI;

    if-nez v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-direct {v5, p0, v1, v0}, Lcom/google/googlenav/friend/aI;-><init>(Lcom/google/googlenav/friend/aK;ZLcom/google/googlenav/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {v0, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->a()V

    return-void
.end method

.method public constructor <init>(Ljava/util/Vector;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/aK;->d:Ljava/util/Hashtable;

    iput-object p1, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->a()V

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(I)Lcom/google/googlenav/friend/aI;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Long;)Lcom/google/googlenav/friend/aI;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->d:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/friend/aI;
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public a()V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->d:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    iget-object v3, p0, Lcom/google/googlenav/friend/aK;->d:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcom/google/googlenav/friend/aI;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 5

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->i()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public f()Z
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public g()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    iput-object v0, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    return-void
.end method

.method public i()Lcom/google/googlenav/common/f;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/googlenav/common/f;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/f;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/f;->a(J)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/common/f;

    invoke-direct {v2}, Lcom/google/googlenav/common/f;-><init>()V

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/f;->a(J)V

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v3}, Lcom/google/googlenav/common/f;->b()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {p0, v4}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v4

    invoke-virtual {v4, v1}, LaN/B;->b(LaN/B;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/f;->a(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    invoke-static {v0, v2}, Lcom/google/googlenav/common/util/v;->a(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->b:Lcom/google/googlenav/common/f;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/google/googlenav/common/f;
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    if-nez v1, :cond_3

    new-instance v1, Lcom/google/googlenav/common/f;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/f;-><init>(I)V

    iput-object v1, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/f;->a(J)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    invoke-virtual {v1}, Lcom/google/googlenav/common/f;->a()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Lcom/google/googlenav/common/f;

    invoke-direct {v1}, Lcom/google/googlenav/common/f;-><init>()V

    iget-object v2, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    invoke-virtual {v2}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v3

    long-to-int v3, v3

    invoke-virtual {p0, v3}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v3

    invoke-virtual {v3}, LaN/B;->c()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/common/f;->a(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/v;->b(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/friend/aK;->c:Lcom/google/googlenav/common/f;

    return-object v0
.end method
