.class public Lcom/google/googlenav/friend/aP;
.super Law/b;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private b:Z

.field private c:I

.field private d:Z

.field private e:I

.field private f:Lcom/google/googlenav/friend/aQ;

.field private g:Lcom/google/googlenav/friend/aR;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/friend/aQ;)V
    .locals 1

    invoke-direct {p0}, Law/b;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/aR;

    invoke-direct {v0}, Lcom/google/googlenav/friend/aR;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    iput p1, p0, Lcom/google/googlenav/friend/aP;->e:I

    iput-object p2, p0, Lcom/google/googlenav/friend/aP;->f:Lcom/google/googlenav/friend/aQ;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/aP;->a:Z

    return-void
.end method

.method public constructor <init>(ZIILcom/google/googlenav/friend/aQ;)V
    .locals 1

    invoke-direct {p0}, Law/b;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/aR;

    invoke-direct {v0}, Lcom/google/googlenav/friend/aR;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    iput-boolean p1, p0, Lcom/google/googlenav/friend/aP;->b:Z

    iput p2, p0, Lcom/google/googlenav/friend/aP;->c:I

    iput p3, p0, Lcom/google/googlenav/friend/aP;->e:I

    iput-object p4, p0, Lcom/google/googlenav/friend/aP;->f:Lcom/google/googlenav/friend/aQ;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/aP;->a:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v1, p0, Lcom/google/googlenav/friend/aP;->e:I

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/aP;->a:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/googlenav/friend/aP;->b:Z

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v1, p0, Lcom/google/googlenav/friend/aP;->c:I

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bK;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-boolean v0, p0, Lcom/google/googlenav/friend/aP;->d:Z

    if-eqz v0, :cond_1

    invoke-virtual {v1, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {v1, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/aP;->d:Z

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/googlenav/friend/aR;->a(Lcom/google/googlenav/friend/aR;I)I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/aR;->b(Lcom/google/googlenav/friend/aR;I)I

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    invoke-static {v1}, Lcom/google/googlenav/friend/aR;->a(Lcom/google/googlenav/friend/aR;)I

    move-result v1

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    return v3
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x33

    return v0
.end method

.method public d_()V
    .locals 2

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    invoke-static {v0}, Lcom/google/googlenav/friend/aR;->b(Lcom/google/googlenav/friend/aR;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aP;->f:Lcom/google/googlenav/friend/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aP;->f:Lcom/google/googlenav/friend/aQ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/aQ;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aP;->f:Lcom/google/googlenav/friend/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aP;->f:Lcom/google/googlenav/friend/aQ;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/aQ;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aP;->n()Lcom/google/googlenav/friend/aR;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/googlenav/friend/aR;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aP;->g:Lcom/google/googlenav/friend/aR;

    return-object v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
