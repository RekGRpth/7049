.class public Lcom/google/googlenav/friend/aS;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:I

.field private final c:Lcom/google/googlenav/friend/aT;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private f:I


# direct methods
.method public constructor <init>(ZILcom/google/googlenav/friend/aT;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-boolean p1, p0, Lcom/google/googlenav/friend/aS;->a:Z

    iput p2, p0, Lcom/google/googlenav/friend/aS;->b:I

    iput-object p3, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eC;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/googlenav/friend/aS;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/aS;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/friend/aS;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eC;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/googlenav/friend/aS;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/aS;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/aS;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/friend/aS;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/aS;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eC;->j:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v1, p0, Lcom/google/googlenav/friend/aS;->b:I

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/aS;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_0
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/google/googlenav/friend/aS;->b:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/aS;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eC;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/friend/aS;->f:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    return v2
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x53

    return v0
.end method

.method public d_()V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/friend/aS;->f:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/aT;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/aT;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aS;->c:Lcom/google/googlenav/friend/aT;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/aT;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
