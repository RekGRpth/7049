.class public final Lcom/google/googlenav/friend/aU;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;I)I
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result p1

    :cond_0
    return p1
.end method

.method public static a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/aV;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/googlenav/friend/aV;-><init>(Las/c;Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aV;->g()V

    return-void
.end method

.method public static a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/aX;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/googlenav/friend/aX;-><init>(Las/c;Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aX;->g()V

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/googlenav/friend/bc;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    new-instance v0, Lcom/google/googlenav/friend/aZ;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/googlenav/friend/aZ;-><init>(Las/c;Lcom/google/googlenav/friend/bc;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aZ;->g()V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLcom/google/googlenav/friend/ba;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/aW;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/googlenav/friend/aW;-><init>(Las/c;Ljava/lang/String;ZLcom/google/googlenav/friend/ba;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aW;->g()V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLcom/google/googlenav/friend/bc;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/aY;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/googlenav/friend/aY;-><init>(Las/c;Ljava/lang/String;ZLcom/google/googlenav/friend/bc;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aY;->g()V

    return-void
.end method

.method public static a(Ljava/lang/String;Z)Z
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    invoke-static {v0}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, p1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;I)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method

.method public static b(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, Lcom/google/googlenav/friend/aU;->b(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
