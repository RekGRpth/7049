.class public Lcom/google/googlenav/friend/af;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field private e:Lcom/google/googlenav/friend/ae;

.field private f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/ae;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/af;->e:Lcom/google/googlenav/friend/ae;

    iput-boolean v2, p0, Lcom/google/googlenav/friend/af;->a:Z

    iput-boolean v2, p0, Lcom/google/googlenav/friend/af;->b:Z

    iput-boolean v2, p0, Lcom/google/googlenav/friend/af;->c:Z

    iput-boolean v2, p0, Lcom/google/googlenav/friend/af;->d:Z

    iput-boolean v2, p0, Lcom/google/googlenav/friend/af;->g:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/ae;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/af;->e:Lcom/google/googlenav/friend/ae;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/af;->a:Z

    return-void
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/af;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/af;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/af;->b:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/af;->e:Lcom/google/googlenav/friend/ae;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->c()Z

    move-result v0

    return v0
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/af;->c:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/af;->e:Lcom/google/googlenav/friend/ae;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->b()Z

    move-result v0

    return v0
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/af;->d:Z

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/af;->e:Lcom/google/googlenav/friend/ae;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->d()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/af;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/af;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/af;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/af;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/af;->a:Z

    return v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/af;->b:Z

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/af;->c:Z

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/af;->d:Z

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/af;->g:Z

    return v0
.end method
