.class public final enum Lcom/google/googlenav/friend/az;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/googlenav/friend/az;

.field private static final synthetic c:[Lcom/google/googlenav/friend/az;


# instance fields
.field private b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/friend/az;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/friend/az;

    sget-object v1, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/googlenav/friend/az;->c:[Lcom/google/googlenav/friend/az;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 3

    const-class v1, Lcom/google/googlenav/friend/az;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(I)V
    .locals 4

    const-class v1, Lcom/google/googlenav/friend/az;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/friend/az;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    const/4 v3, 0x1

    invoke-direct {v2, v0, p0, v3}, Lcom/google/googlenav/friend/az;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(IZ)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/friend/az;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/friend/az;->b(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    if-eqz p2, :cond_2

    const-string v0, "OPT_IN_HISTORY"

    iget-object v1, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lbm/e;->c(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_2
    const-string v0, "OPT_IN_HISTORY"

    iget-object v1, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lbm/e;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;IZ)V
    .locals 2

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/friend/az;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p3, :cond_2

    const-string v0, "OPT_IN_HISTORY"

    iget-object v1, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lbm/e;->c(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_2
    const-string v0, "OPT_IN_HISTORY"

    iget-object v1, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lbm/e;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/friend/az;->b(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;I)I
    .locals 6

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/google/googlenav/friend/az;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-ne v2, p2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static final declared-synchronized b()V
    .locals 3

    const-class v1, Lcom/google/googlenav/friend/az;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    invoke-direct {v0}, Lcom/google/googlenav/friend/az;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clear()V

    const-string v2, "OPT_IN_HISTORY"

    invoke-static {v2, v0}, Lbm/e;->c(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(I)V
    .locals 3

    const-class v1, Lcom/google/googlenav/friend/az;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/friend/az;->a(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized c()V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-class v3, Lcom/google/googlenav/friend/az;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/googlenav/friend/az;->a:Lcom/google/googlenav/friend/az;

    invoke-direct {v2}, Lcom/google/googlenav/friend/az;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    const/4 v6, 0x1

    invoke-virtual {v4, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    if-nez v7, :cond_0

    const/4 v0, 0x2

    const/4 v7, 0x4

    invoke-virtual {v6, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    const-string v0, "OPT_IN_HISTORY"

    invoke-static {v0, v4}, Lbm/e;->c(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->E:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v0, "OPT_IN_HISTORY"

    iget-object v1, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lbm/e;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/az;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private static e()Ljava/lang/String;
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/googlenav/friend/az;
    .locals 1

    const-class v0, Lcom/google/googlenav/friend/az;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/az;

    return-object v0
.end method

.method public static values()[Lcom/google/googlenav/friend/az;
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/az;->c:[Lcom/google/googlenav/friend/az;

    invoke-virtual {v0}, [Lcom/google/googlenav/friend/az;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/friend/az;

    return-object v0
.end method
