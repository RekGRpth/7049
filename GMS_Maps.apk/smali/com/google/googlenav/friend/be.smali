.class public Lcom/google/googlenav/friend/be;
.super Law/b;
.source "SourceFile"

# interfaces
.implements LaM/i;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/lang/Integer;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:J

.field private final h:I

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:I

.field private final p:Ljava/lang/String;

.field private final q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final r:Lcom/google/googlenav/friend/bf;

.field private s:Z

.field private t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private v:Ljava/util/List;

.field private w:Z

.field private x:I

.field private y:Z


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/friend/bg;)V
    .locals 2

    invoke-direct {p0}, Law/b;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->w:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/friend/be;->x:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->a:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->b(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->b:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->c(Lcom/google/googlenav/friend/bg;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/be;->c:Ljava/lang/Integer;

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->d(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->d:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->e(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->e:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->f(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->f:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->g(Lcom/google/googlenav/friend/bg;)Lcom/google/googlenav/friend/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/be;->r:Lcom/google/googlenav/friend/bf;

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->h(Lcom/google/googlenav/friend/bg;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/friend/be;->g:J

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->i(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->h:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->j(Lcom/google/googlenav/friend/bg;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->i:Z

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->k(Lcom/google/googlenav/friend/bg;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->j:Z

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->l(Lcom/google/googlenav/friend/bg;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->k:Z

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->m(Lcom/google/googlenav/friend/bg;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->l:Z

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->n(Lcom/google/googlenav/friend/bg;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->m:Z

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->o(Lcom/google/googlenav/friend/bg;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->n:Z

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->p(Lcom/google/googlenav/friend/bg;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/be;->o:I

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->q(Lcom/google/googlenav/friend/bg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/be;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {p1}, Lcom/google/googlenav/friend/bg;->r(Lcom/google/googlenav/friend/bg;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/be;->p:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 7

    if-nez p0, :cond_0

    const-string v0, "null"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GoogleNavUserEventProto[eventType="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ",status="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gl;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v2, p0, Lcom/google/googlenav/friend/be;->a:I

    mul-int/lit8 v2, v2, 0xa

    iget v3, p0, Lcom/google/googlenav/friend/be;->b:I

    mul-int/lit8 v3, v3, 0xa

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v4, p0, Lcom/google/googlenav/friend/be;->c:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/friend/be;->c:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v7, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->p:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-object v4, p0, Lcom/google/googlenav/friend/be;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget v1, p0, Lcom/google/googlenav/friend/be;->d:I

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v1, p0, Lcom/google/googlenav/friend/be;->e:I

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v1, p0, Lcom/google/googlenav/friend/be;->f:I

    const v4, 0x1869f

    if-eq v1, v4, :cond_2

    iget v1, p0, Lcom/google/googlenav/friend/be;->f:I

    const/4 v4, -0x1

    if-eq v1, v4, :cond_2

    const/16 v1, 0xe

    iget v4, p0, Lcom/google/googlenav/friend/be;->f:I

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/4 v1, 0x5

    iget v4, p0, Lcom/google/googlenav/friend/be;->h:I

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    iget-boolean v4, p0, Lcom/google/googlenav/friend/be;->i:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    iget-boolean v4, p0, Lcom/google/googlenav/friend/be;->j:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    iget-boolean v4, p0, Lcom/google/googlenav/friend/be;->k:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb

    iget-boolean v4, p0, Lcom/google/googlenav/friend/be;->l:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xf

    iget-boolean v4, p0, Lcom/google/googlenav/friend/be;->m:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    iget v4, p0, Lcom/google/googlenav/friend/be;->o:I

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-static {v2, v3}, LaH/d;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/googlenav/friend/be;->j:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_3

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/googlenav/friend/be;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/be;->w:Z

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/gl;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v2, v1, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3, v1, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    packed-switch v2, :pswitch_data_0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->s:Z

    iget v2, p0, Lcom/google/googlenav/friend/be;->x:I

    if-lez v2, :cond_0

    move v0, v1

    :cond_0
    iget v2, p0, Lcom/google/googlenav/friend/be;->x:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/googlenav/friend/be;->x:I

    iget-boolean v2, p0, Lcom/google/googlenav/friend/be;->n:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    if-lez v3, :cond_1

    iput-boolean v1, p0, Lcom/google/googlenav/friend/be;->y:Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v2

    invoke-virtual {v2, p0}, LaM/f;->a(LaM/i;)V

    :cond_1
    if-nez v0, :cond_2

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v3}, LaM/f;->a(I)V

    :cond_2
    :goto_0
    return v1

    :pswitch_0
    iget-object v2, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/friend/be;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/google/googlenav/friend/be;->v:Ljava/util/List;

    :goto_1
    if-ge v0, v2, :cond_4

    iget-object v3, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0x76

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v4, p0, Lcom/google/googlenav/friend/be;->v:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iput-boolean v1, p0, Lcom/google/googlenav/friend/be;->s:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x3f

    return v0
.end method

.method public c_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d_()V
    .locals 6

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/be;->r:Lcom/google/googlenav/friend/bf;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/friend/be;->s:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/be;->r:Lcom/google/googlenav/friend/bf;

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lcom/google/googlenav/friend/be;->v:Ljava/util/List;

    iget-object v3, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v4, p0, Lcom/google/googlenav/friend/be;->g:J

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/friend/bf;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/googlenav/friend/be;->y:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/googlenav/friend/be;->x:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/be;->r:Lcom/google/googlenav/friend/bf;

    iget-wide v1, p0, Lcom/google/googlenav/friend/be;->g:J

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/friend/bf;->a(J)V

    goto :goto_0
.end method

.method public synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/be;->n()Lcom/google/googlenav/friend/bh;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/googlenav/friend/bh;
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/bh;

    invoke-direct {v0}, Lcom/google/googlenav/friend/bh;-><init>()V

    iget-boolean v1, p0, Lcom/google/googlenav/friend/be;->s:Z

    iput-boolean v1, v0, Lcom/google/googlenav/friend/bh;->a:Z

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v1, v0, Lcom/google/googlenav/friend/bh;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->v:Ljava/util/List;

    iput-object v1, v0, Lcom/google/googlenav/friend/bh;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v1, v0, Lcom/google/googlenav/friend/bh;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v1, p0, Lcom/google/googlenav/friend/be;->g:J

    iput-wide v1, v0, Lcom/google/googlenav/friend/bh;->e:J

    return-object v0
.end method

.method public o()V
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, p0}, LaM/f;->b(LaM/i;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/friend/be;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/be;->s:Z

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public p()V
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, p0}, LaM/f;->b(LaM/i;)V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/be;->u_()V

    return-void
.end method

.method public s_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/be;->w:Z

    return v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SnapToPlaceRequest[selected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->a:I

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->b:I

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",span="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->d:I

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->e:I

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",accuracyMeters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",gaiaId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/googlenav/friend/be;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",maxNumResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",shouldReverseGeoCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/friend/be;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",isCheckinMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/friend/be;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",shouldRequestHotpotStatistics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/friend/be;->k:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",shouldBePersonalized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/friend/be;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",purpose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/friend/be;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",userEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1}, Lcom/google/googlenav/friend/be;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/be;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/be;->r:Lcom/google/googlenav/friend/bf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/be;->r:Lcom/google/googlenav/friend/bf;

    iget-wide v1, p0, Lcom/google/googlenav/friend/be;->g:J

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/friend/bf;->a(J)V

    :cond_0
    return-void
.end method
