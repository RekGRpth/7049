.class public Lcom/google/googlenav/friend/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Integer;

.field private d:I

.field private e:I

.field private f:I

.field private g:J

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private r:Lcom/google/googlenav/friend/bf;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/googlenav/friend/bg;->d:I

    iput v2, p0, Lcom/google/googlenav/friend/bg;->e:I

    const v0, 0x1869f

    iput v0, p0, Lcom/google/googlenav/friend/bg;->f:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/friend/bg;->g:J

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/googlenav/friend/bg;->h:I

    iput-boolean v2, p0, Lcom/google/googlenav/friend/bg;->i:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/friend/bg;->o:I

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->a:I

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->b:I

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/friend/bg;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->d:I

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->e:I

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->f:I

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/friend/bg;)Lcom/google/googlenav/friend/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->r:Lcom/google/googlenav/friend/bf;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/friend/bg;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/friend/bg;->g:J

    return-wide v0
.end method

.method static synthetic i(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->h:I

    return v0
.end method

.method static synthetic j(Lcom/google/googlenav/friend/bg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/googlenav/friend/bg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->j:Z

    return v0
.end method

.method static synthetic l(Lcom/google/googlenav/friend/bg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->k:Z

    return v0
.end method

.method static synthetic m(Lcom/google/googlenav/friend/bg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->l:Z

    return v0
.end method

.method static synthetic n(Lcom/google/googlenav/friend/bg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->m:Z

    return v0
.end method

.method static synthetic o(Lcom/google/googlenav/friend/bg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->n:Z

    return v0
.end method

.method static synthetic p(Lcom/google/googlenav/friend/bg;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/bg;->o:I

    return v0
.end method

.method static synthetic q(Lcom/google/googlenav/friend/bg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic r(Lcom/google/googlenav/friend/bg;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/friend/be;
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/be;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/be;-><init>(Lcom/google/googlenav/friend/bg;)V

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->a:I

    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/bg;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/bg;->r:Lcom/google/googlenav/friend/bf;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/bg;->p:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->i:Z

    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->b:I

    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->j:Z

    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/friend/bg;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/bg;->c:Ljava/lang/Integer;

    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->l:Z

    return-object p0
.end method

.method public d(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->d:I

    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->m:Z

    return-object p0
.end method

.method public e(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->e:I

    return-object p0
.end method

.method public e(Z)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->n:Z

    return-object p0
.end method

.method public f(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->f:I

    return-object p0
.end method

.method public g(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->h:I

    return-object p0
.end method

.method public h(I)Lcom/google/googlenav/friend/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/bg;->o:I

    return-object p0
.end method
