.class public abstract Lcom/google/googlenav/friend/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/g;


# static fields
.field protected static d:Z


# instance fields
.field private final a:Ljava/util/Vector;

.field protected final b:Lcom/google/googlenav/android/aa;

.field protected final c:Lcom/google/googlenav/ui/wizard/jv;

.field private final e:Lcom/google/googlenav/friend/d;

.field private volatile f:Z

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/friend/bi;->d:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/bi;->a:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/bi;->f:Z

    iput-object p1, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    iput-object p2, p0, Lcom/google/googlenav/friend/bi;->c:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p3, p0, Lcom/google/googlenav/friend/bi;->e:Lcom/google/googlenav/friend/d;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/bi;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/bi;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/friend/bi;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/friend/bi;->i()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected static a(ILjava/lang/String;Lcom/google/googlenav/friend/bc;)V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    new-instance v0, Lcom/google/googlenav/friend/bp;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/googlenav/friend/bp;-><init>(Las/c;Lcom/google/googlenav/friend/bc;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bp;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/bi;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/bi;->f:Z

    return p1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/google/googlenav/friend/bi;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_1

    array-length v2, v1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    invoke-static {v1}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v1

    :goto_0
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/friend/bi;->f:Z

    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/friend/bl;

    invoke-direct {v2, p0}, Lcom/google/googlenav/friend/bl;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()Ljava/util/List;
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/friend/bi;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->a:Ljava/util/Vector;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized C_()V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/friend/bi;->f:Z

    new-instance v0, Lcom/google/googlenav/friend/bs;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->h()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/friend/bq;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/friend/bq;-><init>(Lcom/google/googlenav/friend/bi;Lcom/google/googlenav/friend/bj;)V

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/bs;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/bt;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public D_()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/googlenav/friend/bi;->a(ZLjava/lang/String;Lcom/google/googlenav/friend/bc;)V

    invoke-direct {p0}, Lcom/google/googlenav/friend/bi;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    invoke-interface {v0}, LaM/h;->D_()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    new-instance v1, Lcom/google/googlenav/friend/bo;

    invoke-direct {v1, p0}, Lcom/google/googlenav/friend/bo;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_1
    return-void
.end method

.method public declared-synchronized E_()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/googlenav/friend/bi;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    invoke-interface {v0}, LaM/h;->E_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method protected G_()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->u()Lcom/google/googlenav/friend/d;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->u()Lcom/google/googlenav/friend/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/friend/d;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    new-instance v1, Lcom/google/googlenav/friend/bk;

    invoke-direct {v1, p0}, Lcom/google/googlenav/friend/bk;-><init>(Lcom/google/googlenav/friend/bi;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public declared-synchronized M_()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/friend/bm;

    invoke-direct {v2, p0}, Lcom/google/googlenav/friend/bm;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/friend/bi;->a(ZLjava/lang/String;Lcom/google/googlenav/friend/bc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized N_()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/bi;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/friend/bi;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    invoke-interface {v0}, LaM/h;->N_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)Lcom/google/googlenav/friend/br;
.end method

.method public a(LaM/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/friend/bi;->e()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/bi;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/bi;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->s()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->c:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/friend/bi;->b:Lcom/google/googlenav/android/aa;

    invoke-virtual {p0, v0, v1, p0}, Lcom/google/googlenav/friend/bi;->a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)Lcom/google/googlenav/friend/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/br;->a()V

    goto :goto_0
.end method

.method public a(ZLjava/lang/String;Lcom/google/googlenav/friend/bc;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->p()I

    move-result v0

    :goto_0
    invoke-static {v0, p2, p3}, Lcom/google/googlenav/friend/bi;->a(ILjava/lang/String;Lcom/google/googlenav/friend/bc;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LaM/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method protected abstract c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
.end method

.method protected abstract d()Ljava/lang/String;
.end method

.method protected abstract g()I
.end method

.method protected h()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method protected m()V
    .locals 0

    return-void
.end method

.method protected p()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected r()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->c:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized s()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->m()V

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/friend/bj;

    invoke-direct {v2, p0}, Lcom/google/googlenav/friend/bj;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/friend/bi;->a(ZLjava/lang/String;Lcom/google/googlenav/friend/bc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public t()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/bi;->f:Z

    return v0
.end method

.method protected u()Lcom/google/googlenav/friend/d;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bi;->e:Lcom/google/googlenav/friend/d;

    return-object v0
.end method

.method public v()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/bi;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected w()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->u()Lcom/google/googlenav/friend/d;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/bi;->t()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
