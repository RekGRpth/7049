.class public Lcom/google/googlenav/friend/bv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/aL;


# direct methods
.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/aL;

    const-string v1, "location_history"

    const/16 v2, 0x1e

    const-wide/16 v3, 0x7530

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/friend/aL;-><init>(Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/google/googlenav/friend/bv;->a:Lcom/google/googlenav/friend/aL;

    return-void
.end method

.method private a(Ljava/util/Vector;II)Ljava/util/Vector;
    .locals 7

    const/4 v6, 0x6

    new-instance v2, Ljava/util/Vector;

    sub-int v0, p3, p2

    invoke-direct {v2, v0}, Ljava/util/Vector;-><init>(I)V

    :goto_0
    if-ge p2, p3, :cond_3

    invoke-virtual {p1, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    return-object v2
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bv;->a:Lcom/google/googlenav/friend/aL;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/friend/aL;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(I)J
    .locals 2

    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bv;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bv;->b()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/bv;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method private e(I)J
    .locals 2

    const-wide/32 v0, 0x493e0

    return-wide v0
.end method


# virtual methods
.method a()J
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/bv;->e(I)J

    move-result-wide v0

    return-wide v0
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/Vector;
    .locals 5

    const/4 v0, 0x1

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    if-ne v4, p2, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Filter location failed"

    invoke-static {v4, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/bv;->a:Lcom/google/googlenav/friend/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aL;->d()V

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method a(Ljava/util/Vector;J)Ljava/util/Vector;
    .locals 9

    const/4 v2, 0x0

    const/4 v8, 0x5

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    sub-long v4, v3, p2

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move-object v0, v2

    :goto_0
    if-ltz v3, :cond_0

    invoke-virtual {p1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v6}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gtz v6, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gtz v0, :cond_2

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/bv;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bv;->a:Lcom/google/googlenav/friend/aL;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/aL;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public a(I)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bv;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0, v0, p1}, Lcom/google/googlenav/friend/bv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/Vector;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/bv;->d(I)J

    move-result-wide v3

    invoke-virtual {p0, v0, v3, v4}, Lcom/google/googlenav/friend/bv;->a(Ljava/util/Vector;J)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/bv;->b(I)I

    move-result v0

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v4, v0, :cond_3

    :cond_2
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne p1, v1, :cond_5

    invoke-virtual {p0, v3}, Lcom/google/googlenav/friend/bv;->a(Ljava/util/Vector;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v0, v3}, Lcom/google/googlenav/friend/bv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Vector;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_2
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/bv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method a(Ljava/util/Vector;)Z
    .locals 9

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v1, v0}, Lcom/google/googlenav/friend/bv;->a(Ljava/util/Vector;II)Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-direct {p0, p1, v0, v2}, Lcom/google/googlenav/friend/bv;->a(Ljava/util/Vector;II)Ljava/util/Vector;

    move-result-object v6

    const-wide/16 v2, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v7

    if-ge v0, v7, :cond_1

    invoke-virtual {v5, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-wide/high16 v7, 0x3ff0000000000000L

    add-double/2addr v2, v7

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-direct {p0, p1, v1, v0}, Lcom/google/googlenav/friend/bv;->a(Ljava/util/Vector;II)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    int-to-double v5, v0

    div-double/2addr v2, v5

    invoke-virtual {p0, v4}, Lcom/google/googlenav/friend/bv;->c(I)D

    move-result-wide v5

    cmpg-double v0, v2, v5

    if-gez v0, :cond_2

    move v1, v4

    :cond_2
    return v1
.end method

.method b(I)I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method b()J
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/bv;->e(I)J

    move-result-wide v0

    return-wide v0
.end method

.method c(I)D
    .locals 2

    const-wide v0, 0x3fe3333333333333L

    return-wide v0
.end method

.method c()J
    .locals 2

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/bv;->e(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bv;->a:Lcom/google/googlenav/friend/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aL;->c()V

    return-void
.end method

.method e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/bv;->a:Lcom/google/googlenav/friend/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aL;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method
