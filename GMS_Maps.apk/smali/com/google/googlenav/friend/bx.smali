.class public Lcom/google/googlenav/friend/bx;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/f;

.field private b:Lcom/google/googlenav/common/f;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/bx;->a:Lcom/google/googlenav/common/f;

    iput-object p2, p0, Lcom/google/googlenav/friend/bx;->b:Lcom/google/googlenav/common/f;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 7

    const/4 v0, 0x0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->E:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/googlenav/friend/bx;->a:Lcom/google/googlenav/common/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/bx;->a:Lcom/google/googlenav/common/f;

    invoke-virtual {v1}, Lcom/google/googlenav/common/f;->b()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/googlenav/friend/bx;->a:Lcom/google/googlenav/common/f;

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/friend/bx;->b:Lcom/google/googlenav/common/f;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/friend/bx;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v1}, Lcom/google/googlenav/common/f;->b()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/googlenav/friend/bx;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->F:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    packed-switch v1, :pswitch_data_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/google/googlenav/friend/bx;->a(Z)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/bx;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x36

    return v0
.end method
