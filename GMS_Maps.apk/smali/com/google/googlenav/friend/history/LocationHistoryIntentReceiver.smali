.class public Lcom/google/googlenav/friend/history/LocationHistoryIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Lcom/google/googlenav/friend/history/v;
    .locals 2

    invoke-static {p1}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/v;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/friend/history/v;-><init>(Landroid/content/Context;Lbm/c;)V

    return-object v1
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/LocationHistoryIntentReceiver;->a(Landroid/content/Context;)Lcom/google/googlenav/friend/history/v;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.googlenav.friend.android.FriendServiceHelper.SIGNED_IN_OUT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "signedin"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "signedin"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/v;->a()V

    goto :goto_0
.end method
