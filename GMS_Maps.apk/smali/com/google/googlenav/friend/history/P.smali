.class public Lcom/google/googlenav/friend/history/P;
.super Lcom/google/googlenav/friend/history/O;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/googlenav/ui/view/dialog/aD;

.field private c:Ljava/util/List;

.field private d:Lcom/google/googlenav/ui/view/android/J;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/aD;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/O;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/history/P;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/googlenav/friend/history/P;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/J;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, p1, v2, v0, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v1, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/P;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/history/P;->c:Ljava/util/List;

    return-object p1
.end method

.method private a(Landroid/view/View;I)V
    .locals 6

    const v5, 0x7f1002b8

    const v4, 0x7f1001ee

    const v3, 0x7f100026

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1002bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "viewToShow is invalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :pswitch_1
    const v0, 0x7f1002bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/P;)Lcom/google/googlenav/ui/view/android/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method private b()Ljava/util/List;
    .locals 7

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/Z;

    new-instance v3, Lcom/google/googlenav/friend/history/S;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/friend/history/S;-><init>(Lcom/google/googlenav/friend/history/P;Lcom/google/googlenav/friend/history/Z;)V

    new-instance v4, Lcom/google/googlenav/friend/history/T;

    invoke-direct {v4, p0, v0}, Lcom/google/googlenav/friend/history/T;-><init>(Lcom/google/googlenav/friend/history/P;Lcom/google/googlenav/friend/history/Z;)V

    new-instance v5, Lcom/google/googlenav/friend/history/ac;

    iget-object v6, p0, Lcom/google/googlenav/friend/history/P;->a:Landroid/content/Context;

    invoke-direct {v5, v0, v6, v3, v4}, Lcom/google/googlenav/friend/history/ac;-><init>(Lcom/google/googlenav/friend/history/Z;Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->c(Landroid/view/View;)V

    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    const v0, 0x7f1002b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x26e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1002ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x26d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->f(Landroid/view/View;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/friend/history/P;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    return v0
.end method

.method static synthetic d(Lcom/google/googlenav/friend/history/P;)Lcom/google/googlenav/ui/view/dialog/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    const v0, 0x7f1002bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x274

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->e(Landroid/view/View;)V

    return-void
.end method

.method private e(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    const v0, 0x7f1001ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x25c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private f(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    new-instance v1, Lcom/google/googlenav/friend/history/Q;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/friend/history/Q;-><init>(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aD;->a(Lcom/google/googlenav/ui/view/dialog/aG;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->c:Ljava/util/List;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->e(Landroid/view/View;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->f(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/friend/history/P;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->d(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x28d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/google/googlenav/friend/history/P;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    const/16 v0, 0x2a

    return v0
.end method
