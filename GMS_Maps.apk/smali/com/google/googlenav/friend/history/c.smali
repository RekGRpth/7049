.class public Lcom/google/googlenav/friend/history/c;
.super Lcom/google/googlenav/friend/history/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/friend/history/o;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(ZZ)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const v0, 0x7f020407

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    const v0, 0x7f020408

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    const v0, 0x7f020405

    goto :goto_0

    :cond_2
    const v0, 0x7f020406

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    const v0, 0x7f02040b

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    const v0, 0x7f02040c

    goto :goto_0

    :cond_5
    if-eqz p2, :cond_6

    const v0, 0x7f020409

    goto :goto_0

    :cond_6
    const v0, 0x7f02040a

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    if-eqz p1, :cond_8

    if-eqz p2, :cond_8

    const v0, 0x7f02041d

    goto :goto_0

    :cond_8
    if-eqz p1, :cond_9

    const v0, 0x7f02041e

    goto :goto_0

    :cond_9
    if-eqz p2, :cond_a

    const v0, 0x7f02041b

    goto :goto_0

    :cond_a
    const v0, 0x7f02041c

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->p()Z

    move-result v0

    if-eqz v0, :cond_f

    if-eqz p1, :cond_c

    if-eqz p2, :cond_c

    const v0, 0x7f0203ff

    goto :goto_0

    :cond_c
    if-eqz p1, :cond_d

    const v0, 0x7f020400

    goto :goto_0

    :cond_d
    if-eqz p2, :cond_e

    const v0, 0x7f0203fd

    goto :goto_0

    :cond_e
    const v0, 0x7f0203fe

    goto :goto_0

    :cond_f
    if-eqz p1, :cond_10

    if-eqz p2, :cond_10

    const v0, 0x7f020419

    goto :goto_0

    :cond_10
    if-eqz p1, :cond_11

    const v0, 0x7f02041a

    goto :goto_0

    :cond_11
    if-eqz p2, :cond_12

    const v0, 0x7f020417

    goto :goto_0

    :cond_12
    const v0, 0x7f020418

    goto/16 :goto_0
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    :cond_0
    float-to-int v0, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
