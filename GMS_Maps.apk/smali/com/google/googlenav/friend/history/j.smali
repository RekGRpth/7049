.class public Lcom/google/googlenav/friend/history/j;
.super Lcom/google/googlenav/friend/history/x;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/U;

.field private d:Lcom/google/googlenav/ui/view/android/J;

.field private final e:Lcom/google/googlenav/ui/view/dialog/aD;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;Lcom/google/googlenav/friend/history/U;Lcom/google/googlenav/ui/view/dialog/aD;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/friend/history/x;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    iput-object p3, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    iput-object p4, p0, Lcom/google/googlenav/friend/history/j;->e:Lcom/google/googlenav/ui/view/dialog/aD;

    return-void
.end method

.method private static a(Lcom/google/googlenav/friend/history/U;Landroid/content/Context;)Lbe/o;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/U;->b()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/b;->h()Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x26a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/U;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/googlenav/friend/history/W;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v3, Lbe/o;

    invoke-direct {v3, v1, v2, v4, v0}, Lbe/o;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v3
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/j;)Lcom/google/googlenav/ui/view/android/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/friend/history/c;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/history/p;->a:Lcom/google/googlenav/friend/history/p;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/history/p;->b:Lcom/google/googlenav/friend/history/p;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/j;)Lcom/google/googlenav/ui/view/dialog/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->e:Lcom/google/googlenav/ui/view/dialog/aD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x0

    const/16 v1, 0x8

    const/4 v6, 0x0

    const v0, 0x7f1001ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1002bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    iget-object v3, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/google/googlenav/friend/history/j;->a(Lcom/google/googlenav/friend/history/U;Landroid/content/Context;)Lbe/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/j;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    iget-object v4, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v7, v1, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v3, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v1, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->e:Lcom/google/googlenav/ui/view/dialog/aD;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/U;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/friend/history/k;

    invoke-direct {v3, p0, v2}, Lcom/google/googlenav/friend/history/k;-><init>(Lcom/google/googlenav/friend/history/j;Lbe/o;)V

    invoke-interface {v0, v1, v3}, Lcom/google/googlenav/ui/view/dialog/aD;->a(Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/aF;)V

    return-void
.end method

.method b()Ljava/util/List;
    .locals 13

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v2, -0x1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v10

    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/U;->d()Ljava/util/List;

    move-result-object v3

    move v1, v7

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/history/j;->a(Lcom/google/googlenav/friend/history/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v12, v1

    :goto_1
    if-ne v12, v2, :cond_1

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_3
    if-ltz v1, :cond_6

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/history/j;->a(Lcom/google/googlenav/friend/history/c;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v8, v1

    :goto_4
    move v11, v12

    :goto_5
    if-gt v11, v8, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/U;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/c;

    if-ne v11, v12, :cond_3

    move v3, v9

    :goto_6
    if-ne v11, v8, :cond_4

    move v4, v9

    :goto_7
    sget-object v0, Lcom/google/googlenav/friend/history/n;->a:[I

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/p;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_8
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_5

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_3
    move v3, v7

    goto :goto_6

    :cond_4
    move v4, v7

    goto :goto_7

    :pswitch_0
    new-instance v0, Lbe/k;

    iget-object v2, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3, v4}, Lbe/k;-><init>(Lcom/google/googlenav/friend/history/c;Landroid/content/Context;ZZ)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :pswitch_1
    new-instance v5, Lcom/google/googlenav/friend/history/l;

    invoke-direct {v5, p0, v1}, Lcom/google/googlenav/friend/history/l;-><init>(Lcom/google/googlenav/friend/history/j;Lcom/google/googlenav/friend/history/c;)V

    new-instance v6, Lcom/google/googlenav/friend/history/m;

    invoke-direct {v6, p0, v1}, Lcom/google/googlenav/friend/history/m;-><init>(Lcom/google/googlenav/friend/history/j;Lcom/google/googlenav/friend/history/c;)V

    new-instance v0, Lbe/x;

    iget-object v2, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    invoke-direct/range {v0 .. v6}, Lbe/x;-><init>(Lcom/google/googlenav/friend/history/c;Landroid/content/Context;ZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :pswitch_2
    new-instance v0, Lbe/D;

    invoke-direct {v0, v1}, Lbe/D;-><init>(Lcom/google/googlenav/friend/history/c;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :pswitch_3
    new-instance v0, Lbe/r;

    const/16 v1, 0x30e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbe/r;-><init>(Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_5
    move-object v0, v10

    goto/16 :goto_2

    :cond_6
    move v8, v2

    goto :goto_4

    :cond_7
    move v12, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
