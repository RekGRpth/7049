.class public abstract Lcom/google/googlenav/friend/history/x;
.super Lcom/google/googlenav/friend/history/O;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field protected final b:Lcom/google/googlenav/friend/history/b;

.field protected c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/O;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iput-object p2, p0, Lcom/google/googlenav/friend/history/x;->c:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/friend/history/x;)I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v1, p1, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    return v0
.end method

.method public c()Lcom/google/googlenav/friend/history/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/googlenav/friend/history/x;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/x;->a(Lcom/google/googlenav/friend/history/x;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/x;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/googlenav/friend/history/x;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/googlenav/friend/history/x;

    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v1, p1, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/b;->hashCode()I

    move-result v0

    return v0
.end method
