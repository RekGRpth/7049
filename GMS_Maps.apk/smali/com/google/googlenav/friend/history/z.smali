.class public Lcom/google/googlenav/friend/history/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/v;

.field private final b:Lcom/google/googlenav/friend/history/q;

.field private final c:Lcom/google/googlenav/friend/history/W;

.field private final d:Lcom/google/googlenav/friend/reporting/s;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/v;Lcom/google/googlenav/friend/history/q;Lcom/google/googlenav/friend/history/W;Lcom/google/googlenav/friend/reporting/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/history/z;->e:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    iput-object p3, p0, Lcom/google/googlenav/friend/history/z;->b:Lcom/google/googlenav/friend/history/q;

    iput-object p4, p0, Lcom/google/googlenav/friend/history/z;->c:Lcom/google/googlenav/friend/history/W;

    iput-object p5, p0, Lcom/google/googlenav/friend/history/z;->d:Lcom/google/googlenav/friend/reporting/s;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/z;)Lcom/google/googlenav/friend/history/v;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/googlenav/friend/history/z;
    .locals 6

    invoke-static {p0}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/reporting/f;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    invoke-static {p0, v1}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v5

    new-instance v0, Lcom/google/googlenav/friend/history/z;

    new-instance v2, Lcom/google/googlenav/friend/history/v;

    invoke-static {p0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/friend/history/v;-><init>(Landroid/content/Context;Lbm/c;)V

    new-instance v3, Lcom/google/googlenav/friend/history/q;

    invoke-direct {v3}, Lcom/google/googlenav/friend/history/q;-><init>()V

    new-instance v4, Lcom/google/googlenav/friend/history/W;

    invoke-direct {v4, p0}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/z;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/v;Lcom/google/googlenav/friend/history/q;Lcom/google/googlenav/friend/history/W;Lcom/google/googlenav/friend/reporting/s;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/googlenav/friend/history/z;->b:Lcom/google/googlenav/friend/history/q;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/friend/history/q;->a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "LocationHistory.LocationHistoryManager"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "LocationHistory.LocationHistoryManager"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/z;)Lcom/google/googlenav/friend/reporting/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->d:Lcom/google/googlenav/friend/reporting/s;

    return-object v0
.end method

.method private c()Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    new-instance v0, Lcom/google/googlenav/friend/S;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/googlenav/friend/S;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/S;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/T;

    if-eqz v0, :cond_0

    iget-boolean v2, v0, Lcom/google/googlenav/friend/T;->a:Z

    if-nez v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v2, Lcom/google/googlenav/friend/U;

    iget-object v0, v0, Lcom/google/googlenav/friend/T;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v0, Lcom/google/googlenav/friend/ae;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v2, p0, Lcom/google/googlenav/friend/history/z;->d:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/reporting/s;->a(Z)V

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(Lcom/google/googlenav/friend/ae;)V

    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/friend/history/z;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/z;->c()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/history/v;->a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/z;->b(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v1, p1, v0}, Lcom/google/googlenav/friend/history/v;->a(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)Z

    goto :goto_0
.end method

.method a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/history/U;
    .locals 6

    const/4 v5, 0x5

    const/4 v0, 0x4

    invoke-static {p2, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v3, Lcom/google/googlenav/friend/history/c;

    iget-object v4, p0, Lcom/google/googlenav/friend/history/z;->e:Landroid/content/Context;

    invoke-direct {v3, v0, p1, v4}, Lcom/google/googlenav/friend/history/c;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    :cond_1
    new-instance v2, Lcom/google/googlenav/friend/history/U;

    const/4 v3, 0x3

    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3, v1, v0}, Lcom/google/googlenav/friend/history/U;-><init>(Lcom/google/googlenav/friend/history/b;Ljava/lang/String;Ljava/util/List;F)V

    return-object v2
.end method

.method a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Landroid/util/Pair;Z)Lcom/google/googlenav/friend/history/f;
    .locals 7

    new-instance v0, Lcom/google/googlenav/friend/history/d;

    iget-object v1, p3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v1, p3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/friend/history/d;-><init>(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;IIZLcom/google/googlenav/friend/history/e;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/d;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/f;

    return-object v0
.end method

.method a(Landroid/util/Pair;)Lcom/google/googlenav/friend/history/i;
    .locals 4

    new-instance v1, Lcom/google/googlenav/friend/history/g;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/googlenav/friend/history/g;-><init>(IILcom/google/googlenav/friend/history/h;)V

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/g;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/i;

    return-object v0
.end method

.method a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;
    .locals 4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p1, p2}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/b;

    iget-object v3, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/history/v;->b(Lcom/google/googlenav/friend/history/b;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0, v3}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/history/U;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Z)Ljava/util/List;
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x1

    invoke-static {p1, p2}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->c:Lcom/google/googlenav/friend/history/W;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Landroid/util/Pair;Z)Lcom/google/googlenav/friend/history/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/f;->a()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/f;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {v0, v7}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    new-instance v5, Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/googlenav/friend/history/b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v4, v5, v3}, Lcom/google/googlenav/friend/history/v;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/B;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/friend/history/B;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;)V

    invoke-virtual {v1}, Las/b;->g()V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/N;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/J;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/googlenav/friend/history/J;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Lcom/google/googlenav/friend/history/N;)V

    invoke-virtual {v1}, Las/b;->g()V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/A;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/googlenav/friend/history/A;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Lcom/google/googlenav/friend/history/b;)V

    invoke-virtual {v1}, Las/b;->g()V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/L;)V
    .locals 6

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    new-instance v0, Lcom/google/googlenav/friend/history/F;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/F;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Lcom/google/googlenav/friend/history/L;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    invoke-virtual {v0}, Las/b;->g()V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/friend/history/M;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/C;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/googlenav/friend/history/C;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Ljava/lang/String;Lcom/google/googlenav/friend/history/M;)V

    invoke-virtual {v1}, Las/b;->g()V

    return-void
.end method

.method public b()Ljava/util/List;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/friend/history/z;->c:Lcom/google/googlenav/friend/history/W;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/friend/history/z;->a(Landroid/util/Pair;)Lcom/google/googlenav/friend/history/i;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/i;->a()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/i;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    invoke-static {v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v3, Lcom/google/googlenav/friend/history/Z;

    iget-object v4, p0, Lcom/google/googlenav/friend/history/z;->e:Landroid/content/Context;

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/friend/history/Z;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
