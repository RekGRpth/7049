.class public Lcom/google/googlenav/friend/reporting/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/googlenav/friend/reporting/g;

.field private final d:Lcom/google/googlenav/friend/reporting/d;

.field private final e:Lcom/google/googlenav/friend/reporting/s;

.field private final f:Lcom/google/googlenav/friend/reporting/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;

    sput-object v0, Lcom/google/googlenav/friend/reporting/n;->a:Ljava/lang/Class;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/g;Lcom/google/googlenav/friend/reporting/d;Lcom/google/googlenav/friend/reporting/s;Lcom/google/googlenav/friend/reporting/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/googlenav/friend/reporting/n;->c:Lcom/google/googlenav/friend/reporting/g;

    iput-object p3, p0, Lcom/google/googlenav/friend/reporting/n;->d:Lcom/google/googlenav/friend/reporting/d;

    iput-object p4, p0, Lcom/google/googlenav/friend/reporting/n;->e:Lcom/google/googlenav/friend/reporting/s;

    iput-object p5, p0, Lcom/google/googlenav/friend/reporting/n;->f:Lcom/google/googlenav/friend/reporting/e;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/googlenav/friend/reporting/n;
    .locals 6

    new-instance v0, Lcom/google/googlenav/friend/reporting/f;

    invoke-static {p0}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    invoke-static {p0, v0}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v4

    new-instance v2, Lcom/google/googlenav/friend/reporting/g;

    invoke-direct {v2, p0, v0, v4}, Lcom/google/googlenav/friend/reporting/g;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;Lcom/google/googlenav/friend/reporting/s;)V

    new-instance v3, Lcom/google/googlenav/friend/reporting/d;

    invoke-static {p0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v0

    invoke-direct {v3, p0, v0, v4}, Lcom/google/googlenav/friend/reporting/d;-><init>(Landroid/content/Context;Lbm/c;Lcom/google/googlenav/friend/reporting/s;)V

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    new-instance v5, Lcom/google/googlenav/friend/reporting/e;

    invoke-direct {v5, p0}, Lcom/google/googlenav/friend/reporting/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/googlenav/friend/reporting/n;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/reporting/n;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/g;Lcom/google/googlenav/friend/reporting/d;Lcom/google/googlenav/friend/reporting/s;Lcom/google/googlenav/friend/reporting/e;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/n;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->f:Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/n;->e()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/n;->c()V

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->e:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/friend/reporting/s;->b(J)V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/n;->a()V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/n;->f()V

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->c:Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/g;->c()V
    :try_end_0
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->d:Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/d;->c()V
    :try_end_1
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->e:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/s;->f()Z

    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method c()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    new-instance v1, Lal/a;

    invoke-direct {v1}, Lal/a;-><init>()V

    invoke-static {v0, v1}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a(Landroid/content/Context;Lcom/google/googlenav/common/a;)V

    return-void
.end method

.method d()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v0

    return v0
.end method

.method e()V
    .locals 8

    const/4 v7, 0x0

    const/4 v4, 0x0

    const-wide/32 v2, 0xea60

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    sget-object v6, Lcom/google/googlenav/friend/reporting/n;->a:Ljava/lang/Class;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v5, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    const/high16 v6, 0x8000000

    invoke-static {v5, v7, v1, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/location/clientlib/b;->a(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/location/clientlib/b;->b(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0, v5}, Landroid/location/LocationManager;->removeUpdates(Landroid/app/PendingIntent;)V

    iget-object v5, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    const/high16 v6, 0x10000000

    invoke-static {v5, v7, v1, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    new-instance v1, Landroid/content/Intent;

    const-string v6, "com.google.android.location.internal.GMM_NLP"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "com.google.android.location.internal.EXTRA_DEBUG_INFO"

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v6, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v6, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v6, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    invoke-virtual {v6, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    :cond_0
    :goto_0
    const-string v1, "passive"

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V

    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v6, "network"

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "network"

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method f()V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    sget-object v3, Lcom/google/googlenav/friend/reporting/n;->a:Ljava/lang/Class;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/n;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/app/PendingIntent;)V

    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    return-void
.end method
