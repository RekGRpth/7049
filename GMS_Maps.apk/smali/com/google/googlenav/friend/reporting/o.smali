.class public Lcom/google/googlenav/friend/reporting/o;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(JJLjava/util/List;Z)Lcom/google/googlenav/friend/reporting/q;
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/friend/reporting/q;

    sget-object v1, Lcom/google/googlenav/friend/reporting/p;->b:Lcom/google/googlenav/friend/reporting/p;

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/q;-><init>(Lcom/google/googlenav/friend/reporting/p;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    sub-long v0, p1, p3

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Lcom/google/googlenav/friend/reporting/q;

    sget-object v1, Lcom/google/googlenav/friend/reporting/p;->c:Lcom/google/googlenav/friend/reporting/p;

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/q;-><init>(Lcom/google/googlenav/friend/reporting/p;)V

    goto :goto_0

    :cond_1
    if-eqz p6, :cond_2

    sub-long v0, p1, p3

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    new-instance v0, Lcom/google/googlenav/friend/reporting/q;

    sget-object v1, Lcom/google/googlenav/friend/reporting/p;->d:Lcom/google/googlenav/friend/reporting/p;

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/q;-><init>(Lcom/google/googlenav/friend/reporting/p;)V

    goto :goto_0

    :cond_2
    sub-long v0, p1, p3

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    new-instance v0, Lcom/google/googlenav/friend/reporting/q;

    sget-object v1, Lcom/google/googlenav/friend/reporting/p;->e:Lcom/google/googlenav/friend/reporting/p;

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/q;-><init>(Lcom/google/googlenav/friend/reporting/p;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/googlenav/friend/reporting/q;

    sget-object v1, Lcom/google/googlenav/friend/reporting/p;->a:Lcom/google/googlenav/friend/reporting/p;

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/q;-><init>(Lcom/google/googlenav/friend/reporting/p;)V

    goto :goto_0
.end method
