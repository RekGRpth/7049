.class public Lcom/google/googlenav/friend/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/android/aa;

.field private final b:Lcom/google/googlenav/ui/s;

.field private final c:LaN/u;

.field private final d:Lcom/google/googlenav/ui/friend/q;

.field private final e:Lbf/X;

.field private final f:Lcom/google/googlenav/friend/p;

.field private final g:Lcom/google/googlenav/ui/wizard/jv;

.field private h:Lcom/google/googlenav/friend/L;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/s;LaN/u;Lcom/google/googlenav/ui/friend/q;Lbf/X;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/t;->a:Lcom/google/googlenav/android/aa;

    iput-object p2, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    iput-object p3, p0, Lcom/google/googlenav/friend/t;->c:LaN/u;

    iput-object p4, p0, Lcom/google/googlenav/friend/t;->d:Lcom/google/googlenav/ui/friend/q;

    iput-object p5, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    iput-object p6, p0, Lcom/google/googlenav/friend/t;->f:Lcom/google/googlenav/friend/p;

    iput-object p7, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v0, Lcom/google/googlenav/friend/D;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/D;-><init>(Lcom/google/googlenav/friend/t;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/t;->h:Lcom/google/googlenav/friend/L;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/t;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->a:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/a;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lcom/google/googlenav/b;->a(Lcom/google/googlenav/a;)Lcom/google/googlenav/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "cp"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->e(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/aI;)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/friend/aI;)V
    .locals 7

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/friend/z;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/friend/z;-><init>(Lcom/google/googlenav/friend/t;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    const/16 v4, 0x16f

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/friend/aB;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5, v2}, Lcom/google/googlenav/friend/aB;-><init>(JLcom/google/googlenav/friend/aC;)V

    invoke-virtual {v1, v3}, Law/h;->c(Law/g;)V

    const/16 v0, 0x3d

    const-string v1, "pa"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/friend/aI;Z)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v1, Lcom/google/googlenav/common/f;

    invoke-direct {v1}, Lcom/google/googlenav/common/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/f;->a(J)V

    :goto_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/friend/bx;

    invoke-direct {v3, v0, v1}, Lcom/google/googlenav/friend/bx;-><init>(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V

    invoke-virtual {v2, v3}, Law/h;->c(Law/g;)V

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aI;->a(Z)V

    return-void

    :cond_0
    new-instance v1, Lcom/google/googlenav/common/f;

    invoke-direct {v1}, Lcom/google/googlenav/common/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/f;->a(J)V

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/friend/t;Lcom/google/googlenav/friend/aI;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/t;->e(Lcom/google/googlenav/friend/aI;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/h;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/wizard/ep;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/ep;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->m()Lbf/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/friend/t;)Lbf/X;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    return-object v0
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->R()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->j()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    :cond_0
    return-void
.end method

.method private b(Lcom/google/googlenav/friend/aI;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->o()B

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->a()V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0, p1}, Lbf/X;->a(Lcom/google/googlenav/friend/aI;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bK()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/X;->a(B)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/friend/ad;->b()V

    goto :goto_0
.end method

.method private b(Lcom/google/googlenav/friend/aI;Z)V
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->k()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/f;

    invoke-direct {v0}, Lcom/google/googlenav/common/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/f;->a(J)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/friend/bx;

    invoke-direct {v3, v0, v1}, Lcom/google/googlenav/friend/bx;-><init>(Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V

    invoke-virtual {v2, v3}, Law/h;->c(Law/g;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aI;->a(Z)V

    :cond_0
    if-eqz p2, :cond_1

    new-instance v7, Lcom/google/googlenav/common/f;

    invoke-direct {v7}, Lcom/google/googlenav/common/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Lcom/google/googlenav/common/f;->a(J)V

    move-object v8, v1

    :goto_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v9

    new-instance v0, Lcom/google/googlenav/friend/bw;

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/friend/bw;-><init>(Lcom/google/googlenav/common/f;Ljava/util/Vector;Ljava/util/Vector;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V

    invoke-virtual {v9, v0}, Law/h;->c(Law/g;)V

    invoke-virtual {p1, p2}, Lcom/google/googlenav/friend/aI;->b(Z)V

    return-void

    :cond_1
    new-instance v8, Lcom/google/googlenav/common/f;

    invoke-direct {v8}, Lcom/google/googlenav/common/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v8, v2, v3}, Lcom/google/googlenav/common/f;->a(J)V

    move-object v7, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/t;Lcom/google/googlenav/friend/aI;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/t;->c(Lcom/google/googlenav/friend/aI;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/friend/t;)Lcom/google/googlenav/ui/friend/q;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->d:Lcom/google/googlenav/ui/friend/q;

    return-object v0
.end method

.method private c()V
    .locals 4

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/P;

    new-instance v2, Lcom/google/googlenav/common/f;

    invoke-direct {v2}, Lcom/google/googlenav/common/f;-><init>()V

    new-instance v3, Lcom/google/googlenav/friend/w;

    invoke-direct {v3, p0}, Lcom/google/googlenav/friend/w;-><init>(Lcom/google/googlenav/friend/t;)V

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/friend/P;-><init>(Lcom/google/googlenav/common/f;Lcom/google/googlenav/friend/Q;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private c(Lcom/google/googlenav/friend/aI;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0, p1}, Lbf/X;->a(Lcom/google/googlenav/friend/aI;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bL()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/friend/t;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private d()V
    .locals 8

    invoke-static {}, Lbf/ch;->a()V

    invoke-static {}, LaL/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x19d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x19a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x19b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xfd

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/friend/x;

    invoke-direct {v7, p0}, Lcom/google/googlenav/friend/x;-><init>(Lcom/google/googlenav/friend/t;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    :cond_0
    return-void
.end method

.method private d(Lcom/google/googlenav/friend/aI;)V
    .locals 8

    const/4 v5, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/friend/ad;->f()V

    const/16 v0, 0x185

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x184

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v7, Lcom/google/googlenav/friend/A;

    invoke-direct {v7, p0, p1}, Lcom/google/googlenav/friend/A;-><init>(Lcom/google/googlenav/friend/t;Lcom/google/googlenav/friend/aI;)V

    move-object v4, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/friend/t;)Lcom/google/googlenav/friend/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->f:Lcom/google/googlenav/friend/p;

    return-object v0
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->e()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v2, Lcom/google/googlenav/friend/B;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/friend/B;-><init>(Lcom/google/googlenav/friend/t;Ljava/util/List;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/util/List;Lcom/google/googlenav/ui/wizard/cA;)V

    return-void
.end method

.method private e(Lcom/google/googlenav/friend/aI;)V
    .locals 10

    const/4 v1, 0x0

    new-instance v5, Lcom/google/googlenav/common/f;

    invoke-direct {v5}, Lcom/google/googlenav/common/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v5, v2, v3}, Lcom/google/googlenav/common/f;->a(J)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v9

    new-instance v0, Lcom/google/googlenav/friend/bw;

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/friend/bw;-><init>(Lcom/google/googlenav/common/f;Ljava/util/Vector;Ljava/util/Vector;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V

    invoke-virtual {v9, v0}, Law/h;->c(Law/g;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/t;->f(Lcom/google/googlenav/friend/aI;)V

    return-void
.end method

.method private f()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->d()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->d()V

    return-void
.end method

.method private f(Lcom/google/googlenav/friend/aI;)V
    .locals 2

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aI;->a(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->y()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->J()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbf/X;->a(B)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/friend/ad;->e()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->g:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v1, Lcom/google/googlenav/friend/C;

    invoke-direct {v1, p0}, Lcom/google/googlenav/friend/C;-><init>(Lcom/google/googlenav/friend/t;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/iG;)V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v1}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v1

    sparse-switch p1, :sswitch_data_0

    :goto_0
    move v5, v4

    :cond_0
    :goto_1
    return v5

    :sswitch_0
    instance-of v0, p3, Lcom/google/googlenav/h;

    if-eqz v0, :cond_5

    check-cast p3, Lcom/google/googlenav/h;

    move-object v2, p3

    :goto_2
    const-string v3, "cm"

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    goto :goto_1

    :sswitch_1
    new-instance v0, Lcom/google/googlenav/ui/wizard/ep;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/ep;-><init>()V

    instance-of v1, p3, Lcom/google/googlenav/h;

    if-eqz v1, :cond_4

    check-cast p3, Lcom/google/googlenav/h;

    :goto_3
    iput-object p3, v0, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    iput-boolean v5, v0, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->m()Lbf/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/friend/t;->a()V

    goto :goto_1

    :sswitch_3
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bH()V

    goto :goto_1

    :sswitch_4
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/google/googlenav/friend/t;->f()V

    goto :goto_1

    :sswitch_6
    invoke-static {}, Lcom/google/googlenav/friend/ad;->c()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbf/X;->a(B)V

    goto :goto_1

    :sswitch_7
    check-cast p3, Ljava/lang/String;

    invoke-virtual {v1, p3}, Lcom/google/googlenav/friend/aK;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/aI;)V

    goto :goto_1

    :sswitch_8
    check-cast p3, Ljava/lang/String;

    invoke-virtual {v1, p3}, Lcom/google/googlenav/friend/aK;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->h:Lcom/google/googlenav/friend/L;

    invoke-interface {v1, v0}, Lcom/google/googlenav/friend/L;->a(Lcom/google/googlenav/friend/aI;)V

    goto :goto_1

    :sswitch_9
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0, v7}, Lbf/X;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/aI;)V

    goto/16 :goto_1

    :sswitch_a
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bH()V

    goto/16 :goto_1

    :sswitch_b
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->bH()V

    goto/16 :goto_1

    :sswitch_c
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0, v5}, Lbf/X;->k(Z)V

    goto/16 :goto_1

    :sswitch_d
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0, v4}, Lbf/X;->k(Z)V

    goto/16 :goto_1

    :sswitch_e
    invoke-direct {p0}, Lcom/google/googlenav/friend/t;->e()V

    goto/16 :goto_1

    :sswitch_f
    invoke-direct {p0}, Lcom/google/googlenav/friend/t;->d()V

    goto/16 :goto_1

    :sswitch_10
    invoke-direct {p0, v0, v4}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/aI;Z)V

    goto/16 :goto_1

    :sswitch_11
    invoke-direct {p0, v0, v5}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/aI;Z)V

    goto/16 :goto_1

    :sswitch_12
    invoke-direct {p0, v0, v5}, Lcom/google/googlenav/friend/t;->a(Lcom/google/googlenav/friend/aI;Z)V

    goto/16 :goto_1

    :sswitch_13
    invoke-direct {p0, v0, v4}, Lcom/google/googlenav/friend/t;->a(Lcom/google/googlenav/friend/aI;Z)V

    goto/16 :goto_1

    :sswitch_14
    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/t;->d(Lcom/google/googlenav/friend/aI;)V

    goto/16 :goto_1

    :sswitch_15
    invoke-direct {p0}, Lcom/google/googlenav/friend/t;->c()V

    goto/16 :goto_1

    :sswitch_16
    instance-of v0, p3, Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p3, Landroid/view/MenuItem;

    invoke-static {p3}, LaP/a;->b(Landroid/view/MenuItem;)V

    new-instance v0, Lcom/google/googlenav/friend/u;

    invoke-direct {v0, p0, p3}, Lcom/google/googlenav/friend/u;-><init>(Lcom/google/googlenav/friend/t;Landroid/view/MenuItem;)V

    new-instance v7, Lcom/google/googlenav/friend/v;

    invoke-direct {v7, p0, v0}, Lcom/google/googlenav/friend/v;-><init>(Lcom/google/googlenav/friend/t;Ljava/lang/Runnable;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->f:Lcom/google/googlenav/friend/p;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    goto/16 :goto_1

    :sswitch_17
    invoke-direct {p0}, Lcom/google/googlenav/friend/t;->b()V

    goto/16 :goto_1

    :sswitch_18
    invoke-static {}, Lcom/google/googlenav/friend/ad;->g()V

    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/t;->c(Lcom/google/googlenav/friend/aI;)V

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->d:Lcom/google/googlenav/ui/friend/q;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/friend/q;->a(J)V

    goto/16 :goto_1

    :sswitch_19
    invoke-static {}, Lcom/google/googlenav/friend/ad;->h()V

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v1, v5}, Lbf/X;->a(B)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x19

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v0}, Lax/y;->a(Lcom/google/googlenav/friend/aI;)Lax/y;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/friend/t;->c:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v7, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_1

    :sswitch_1a
    invoke-static {}, Lcom/google/googlenav/friend/ad;->k()V

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1b
    invoke-static {}, Lcom/google/googlenav/friend/ad;->l()V

    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {v1, v2, v7, v7}, Lcom/google/googlenav/aA;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1c
    iget-object v1, p0, Lcom/google/googlenav/friend/t;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->d(Ljava/lang/String;)V

    move v5, v4

    goto/16 :goto_1

    :sswitch_1d
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->D()Lcom/google/googlenav/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/google/googlenav/h;

    invoke-virtual {v1}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/google/googlenav/h;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v2}, Lcom/google/googlenav/friend/t;->a(Lcom/google/googlenav/h;)V

    goto/16 :goto_1

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/googlenav/friend/t;->a(Lcom/google/googlenav/a;)V

    goto/16 :goto_1

    :sswitch_1e
    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/t;->a(Lcom/google/googlenav/friend/aI;)V

    goto/16 :goto_1

    :sswitch_1f
    iget-object v0, p0, Lcom/google/googlenav/friend/t;->e:Lbf/X;

    invoke-virtual {v0}, Lbf/X;->Z()V

    goto/16 :goto_1

    :cond_4
    move-object p3, v7

    goto/16 :goto_3

    :cond_5
    move-object v2, v7

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_9
        0x12c -> :sswitch_e
        0x12d -> :sswitch_2
        0x12e -> :sswitch_18
        0x131 -> :sswitch_b
        0x132 -> :sswitch_a
        0x134 -> :sswitch_10
        0x135 -> :sswitch_11
        0x136 -> :sswitch_12
        0x137 -> :sswitch_13
        0x13a -> :sswitch_14
        0x13b -> :sswitch_3
        0x13c -> :sswitch_6
        0x13d -> :sswitch_16
        0x142 -> :sswitch_1a
        0x143 -> :sswitch_1b
        0x144 -> :sswitch_17
        0x14a -> :sswitch_8
        0x14b -> :sswitch_7
        0x14e -> :sswitch_c
        0x14f -> :sswitch_d
        0x150 -> :sswitch_1c
        0x152 -> :sswitch_f
        0x155 -> :sswitch_5
        0x156 -> :sswitch_1e
        0x158 -> :sswitch_15
        0x159 -> :sswitch_1f
        0x25b -> :sswitch_19
        0x834 -> :sswitch_0
        0x83b -> :sswitch_1d
        0x843 -> :sswitch_1
        0xa8c -> :sswitch_4
    .end sparse-switch
.end method
