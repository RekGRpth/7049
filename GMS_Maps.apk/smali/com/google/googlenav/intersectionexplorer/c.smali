.class public Lcom/google/googlenav/intersectionexplorer/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Lo/T;

.field private final b:Ljava/util/Map;

.field private final c:Lcom/google/android/maps/driveabout/vector/z;

.field private d:Lcom/google/android/maps/driveabout/vector/z;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lo/T;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/c;->a:Lo/T;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->e:Z

    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/c;->h()Lcom/google/android/maps/driveabout/vector/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->c:Lcom/google/android/maps/driveabout/vector/z;

    return-void
.end method

.method public static a(DD)D
    .locals 4

    sub-double v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x400921fb54442d18L

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    const-wide v2, 0x401921fb54442d18L

    sub-double v0, v2, v0

    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lo/af;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ne v0, v4, :cond_2

    const-string v0, " and "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lo/af;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lo/af;->c()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x1fa

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(DD)D
    .locals 6

    const-wide v4, 0x401921fb54442d18L

    sub-double v0, p0, p2

    :goto_0
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    add-double/2addr v0, v4

    goto :goto_0

    :cond_0
    :goto_1
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_1

    sub-double/2addr v0, v4

    goto :goto_1

    :cond_1
    return-wide v0
.end method

.method public static b(Ljava/util/List;)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lcom/google/googlenav/intersectionexplorer/c;->a(DD)D

    move-result-wide v3

    const-wide v5, 0x3fd921fb54442d18L

    cmpg-double v0, v3, v5

    if-ltz v0, :cond_0

    const-wide v7, 0x400921fb54442d18L

    sub-double v5, v7, v5

    cmpl-double v0, v3, v5

    if-lez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private h()Lcom/google/android/maps/driveabout/vector/z;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    new-instance v1, Lo/Z;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lo/Z;-><init>(I)V

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x19

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x19

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x19

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x19

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x19

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v0

    add-int/lit8 v0, v0, -0x19

    invoke-virtual {v1, v2, v0}, Lo/Z;->a(II)Z

    new-instance v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v1}, Lo/Z;->d()Lo/X;

    move-result-object v1

    const/high16 v2, 0x40c00000

    const v3, -0xff0100

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    return-object v0
.end method

.method private i()Lcom/google/android/maps/driveabout/vector/z;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    new-instance v1, Lo/Z;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lo/Z;-><init>(I)V

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x1e

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x1e

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-virtual {v1, v2, v3}, Lo/Z;->a(II)Z

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x1e

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v0

    add-int/lit8 v0, v0, -0x1e

    invoke-virtual {v1, v2, v0}, Lo/Z;->a(II)Z

    new-instance v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v1}, Lo/Z;->d()Lo/X;

    move-result-object v1

    const/high16 v2, 0x40c00000

    const/high16 v3, -0x10000

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/intersectionexplorer/c;)I
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/T;->k(Lo/T;)I

    move-result v0

    return v0
.end method

.method public declared-synchronized a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->d:Lcom/google/android/maps/driveabout/vector/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->d:Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->c:Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lo/af;->c()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/googlenav/intersectionexplorer/c;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Lo/T;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->a:Lo/T;

    return-object v0
.end method

.method public declared-synchronized b(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/googlenav/intersectionexplorer/c;->e:Z

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->d:Lcom/google/android/maps/driveabout/vector/z;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/c;->i()Lcom/google/android/maps/driveabout/vector/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->d:Lcom/google/android/maps/driveabout/vector/z;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/google/googlenav/intersectionexplorer/c;)Z
    .locals 4

    const/4 v1, 0x1

    monitor-enter p0

    if-eqz p1, :cond_0

    if-ne p1, p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->f(Lcom/google/googlenav/intersectionexplorer/c;)Lo/af;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/google/googlenav/intersectionexplorer/c;->e(Lcom/google/googlenav/intersectionexplorer/c;)V

    invoke-virtual {v0, p0, v3}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V

    invoke-virtual {p0, v0, v3}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized c(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;
    .locals 12

    const-wide v10, 0x400921fb54442d18L

    const/4 v9, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lo/af;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/c;->d(Lcom/google/googlenav/intersectionexplorer/c;)D

    move-result-wide v4

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v6

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/af;

    invoke-static {v1}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lo/af;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    invoke-interface {v6, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/c;->d(Lcom/google/googlenav/intersectionexplorer/c;)D

    move-result-wide v7

    invoke-static {v7, v8, v4, v5}, Lcom/google/googlenav/intersectionexplorer/c;->b(DD)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_2

    :pswitch_1
    const/16 v0, 0x1e9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const-wide/16 v4, 0x0

    cmpl-double v4, v1, v4

    if-nez v4, :cond_b

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_3
    cmpg-double v0, v0, v10

    if-gez v0, :cond_4

    const/16 v0, 0x1f8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0x1f9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    const/16 v0, 0x1ec

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v1, 0x1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    :goto_4
    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x1e7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpg-double v0, v0, v10

    if-gez v0, :cond_6

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    const/16 v0, 0x1f5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v9, :cond_3

    invoke-static {v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v0, 0x1f3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v1, v4

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v9, :cond_3

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpg-double v0, v0, v10

    if-gez v0, :cond_9

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x1f5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    move-object v2, v1

    goto/16 :goto_4

    :cond_b
    move-wide v0, v1

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized c()Ljava/util/List;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;)I

    move-result v0

    return v0
.end method

.method public d(Lcom/google/googlenav/intersectionexplorer/c;)D
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    invoke-static {v0, v1}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized d()Z
    .locals 5

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/dA;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-virtual {v0}, Lo/af;->c()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-nez v1, :cond_1

    :goto_2
    move-object v1, v0

    goto :goto_0

    :cond_0
    const/16 v0, 0x1fa

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    monitor-exit p0

    return v0

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/c;->d(Lcom/google/googlenav/intersectionexplorer/c;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    invoke-static {v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Ljava/util/List;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public declared-synchronized e()Ljava/util/Set;
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized e(Lcom/google/googlenav/intersectionexplorer/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f(Lcom/google/googlenav/intersectionexplorer/c;)Lo/af;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Z
    .locals 9

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const-wide/high16 v0, 0x4024000000000000L

    :try_start_1
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v3

    mul-double/2addr v3, v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, p0

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v8

    invoke-virtual {v7, v8}, Lo/T;->c(Lo/T;)F

    move-result v7

    float-to-double v7, v7

    cmpg-double v7, v7, v3

    if-gez v7, :cond_5

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/googlenav/intersectionexplorer/c;->g()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->g()I

    move-result v8

    if-ge v7, v8, :cond_2

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v2

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Lcom/google/googlenav/intersectionexplorer/c;)Z

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->e(Lcom/google/googlenav/intersectionexplorer/c;)V

    const/4 v0, 0x1

    goto :goto_3

    :cond_4
    if-eq v1, p0, :cond_0

    invoke-virtual {v1, p0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Lcom/google/googlenav/intersectionexplorer/c;)Z

    invoke-virtual {v1, p0}, Lcom/google/googlenav/intersectionexplorer/c;->e(Lcom/google/googlenav/intersectionexplorer/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method declared-synchronized g()I
    .locals 3

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    invoke-virtual {v0}, Lo/af;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->c()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a(Ljava/util/List;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
