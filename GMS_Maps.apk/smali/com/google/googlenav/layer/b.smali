.class public Lcom/google/googlenav/layer/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/layer/r;


# instance fields
.field private final a:Lcom/google/googlenav/android/i;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/layer/b;->a:Lcom/google/googlenav/android/i;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/layer/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/layer/d;-><init>(Lcom/google/googlenav/layer/b;)V

    new-instance v1, Las/d;

    invoke-virtual {p0}, Lcom/google/googlenav/layer/b;->c()Las/c;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Las/d;->g()V

    return-void
.end method

.method public a(Lbf/i;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object v0, p1

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move-object v1, v2

    :goto_1
    invoke-virtual {p1}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lbf/i;->ax()Z

    move-result v3

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "layerId"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "layerDisplayName"

    invoke-virtual {v4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "isActive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "isSearch"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    new-instance v0, Lcom/google/googlenav/layer/c;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/layer/c;-><init>(Lcom/google/googlenav/layer/b;Landroid/content/ContentValues;)V

    new-instance v1, Las/d;

    invoke-virtual {p0}, Lcom/google/googlenav/layer/b;->c()Las/c;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Las/d;->g()V

    goto :goto_0

    :sswitch_1
    move-object v0, p1

    check-cast v0, Lbf/y;

    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method protected b()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/b;->a:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method protected c()Las/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/b;->a:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    return-object v0
.end method
