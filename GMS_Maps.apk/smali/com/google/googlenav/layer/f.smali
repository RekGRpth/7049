.class public Lcom/google/googlenav/layer/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:J

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/lang/Object;

.field private g:Lcom/google/googlenav/ui/m;

.field private h:Z

.field private i:Z

.field private j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private k:Lcom/google/googlenav/layer/l;


# direct methods
.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/googlenav/layer/f;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/layer/f;->c:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    new-instance v0, Lcom/google/googlenav/ui/m;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/m;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->h:Z

    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->i:Z

    invoke-direct {p0}, Lcom/google/googlenav/layer/f;->i()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/layer/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/layer/f;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/googlenav/layer/f;
    .locals 1

    sget-object v0, Lcom/google/googlenav/layer/h;->a:Lcom/google/googlenav/layer/f;

    return-object v0
.end method

.method private i()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v0, 0x23f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/layer/m;

    const-string v3, "LayerTransit"

    new-array v4, v7, [Lan/f;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v5, Lcom/google/googlenav/ui/bi;->aF:C

    invoke-interface {v0, v5}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    aput-object v0, v4, v6

    invoke-direct {v2, v3, v1, v4, v6}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;Ljava/lang/String;[Lan/f;I)V

    const-string v0, "LayerTransit"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v3, Lcom/google/googlenav/layer/i;

    const-string v0, "__LAYERS"

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "LayerTransit"

    aput-object v5, v4, v6

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/layer/i;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/google/googlenav/layer/i;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/googlenav/layer/e;

    const-string v4, "LayerTransit"

    const/4 v5, 0x2

    invoke-direct {v0, v4, v5, v1}, Lcom/google/googlenav/layer/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;Lcom/google/googlenav/layer/e;)V

    iget-object v1, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/layer/e;->a(Lcom/google/googlenav/layer/m;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    const-string v1, "__LAYERS"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 2

    const/4 v0, -0x1

    iget-boolean v1, p0, Lcom/google/googlenav/layer/f;->h:Z

    if-eqz v1, :cond_0

    if-eq p1, v0, :cond_0

    add-int/lit16 v0, p1, 0x3e8

    :cond_0
    return v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v8, 0x7

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/layer/f;->a:I

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "__LAYERS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/googlenav/layer/f;->c:J

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/layer/i;->b()[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    iget-object v5, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    aget-object v6, v4, v0

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_3

    new-instance v4, Lcom/google/googlenav/layer/i;

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/googlenav/layer/i;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v5, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-virtual {v4}, Lcom/google/googlenav/layer/i;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_6

    new-instance v5, Lcom/google/googlenav/layer/e;

    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/googlenav/layer/e;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;Lcom/google/googlenav/layer/e;)V

    iget-object v6, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    :goto_4
    if-ge v0, v2, :cond_8

    new-instance v3, Lcom/google/googlenav/layer/m;

    invoke-virtual {p1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v3}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v4, v3}, Lcom/google/googlenav/layer/e;->a(Lcom/google/googlenav/layer/m;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_8
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    :goto_5
    if-ge v0, v2, :cond_a

    invoke-virtual {p1, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v3, 0x9

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    const/16 v5, 0xa

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v5

    int-to-long v6, v3

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/googlenav/ui/m;->a([BJ)Lcom/google/googlenav/ui/o;

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/googlenav/layer/f;->h:Z

    iget-object v4, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    const/16 v5, 0xb

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/google/googlenav/layer/f;->a(I)I

    move-result v3

    int-to-long v5, v3

    invoke-virtual {v4, v1, v5, v6}, Lcom/google/googlenav/ui/m;->a([BJ)Lcom/google/googlenav/ui/o;

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_a
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/layer/l;)V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/layer/f;->i:Z

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p2, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    iput-object v1, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v1, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/googlenav/layer/l;->a()V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->h()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/googlenav/layer/f;->i:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/layer/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/layer/l;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .locals 1

    const-string v0, "TrafficIncident"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/layer/m;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/layer/f;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public d()Ljava/util/List;
    .locals 5

    const/4 v1, 0x0

    const-string v0, "__LAYERS"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/layer/f;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/layer/i;->b()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_2

    aget-object v3, v0, v1

    invoke-virtual {p0, v3}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/layer/e;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/layer/f;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/layer/f;->a:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/layer/f;->c:J

    return-wide v0
.end method

.method public h()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput v2, p0, Lcom/google/googlenav/layer/f;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/layer/f;->c:J

    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->i:Z

    iput-object v3, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    iput-object v3, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/m;->b()V

    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->h:Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
