.class public Lcom/google/googlenav/layer/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final a:Lcom/google/googlenav/layer/m;


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final g:B

.field private final h:I

.field private final i:I

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private m:I

.field private n:I

.field private o:[Lcom/google/googlenav/e;

.field private p:[[B

.field private final q:I

.field private r:J

.field private final s:J

.field private t:Lo/o;

.field private u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/layer/m;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    const/16 v7, 0x16

    const/16 v5, 0x12

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->c:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->d:Ljava/lang/String;

    invoke-static {p1, v7}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->e:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/layer/m;->h:I

    const/4 v0, 0x4

    invoke-static {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/layer/m;->i:I

    const/16 v0, 0x13

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/layer/m;->j:Z

    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/layer/m;->k:Z

    const/16 v0, 0x14

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/layer/m;->l:Z

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v2, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v2

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_2

    iput-byte v5, p0, Lcom/google/googlenav/layer/m;->g:B

    :goto_1
    const/16 v0, 0x15

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/layer/m;->q:I

    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_4

    const/16 v1, 0x3d

    invoke-static {v0, v1, v6}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/layer/m;->m:I

    const/16 v1, 0x3e

    invoke-static {v0, v1, v7}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/layer/m;->n:I

    :goto_2
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/layer/m;->r:J

    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/layer/m;->s:J

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->t:Lo/o;

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/layer/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void

    :cond_2
    new-array v0, v3, [Lcom/google/googlenav/e;

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    new-array v0, v3, [[B

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->p:[[B

    move v0, v1

    :goto_3
    if-ge v0, v3, :cond_3

    const/16 v4, 0x11

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/layer/m;->p:[[B

    aput-object v4, v5, v0

    array-length v5, v4

    invoke-interface {v2, v4, v1, v5}, Lam/h;->a([BII)Lam/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    invoke-static {v4}, Lcom/google/googlenav/e;->a(Lam/f;)Lcom/google/googlenav/e;

    move-result-object v4

    aput-object v4, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/16 v0, 0x14

    iput-byte v0, p0, Lcom/google/googlenav/layer/m;->g:B

    goto :goto_1

    :cond_4
    iput v6, p0, Lcom/google/googlenav/layer/m;->m:I

    iput v7, p0, Lcom/google/googlenav/layer/m;->n:I

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    const-string v0, ""

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;Ljava/lang/String;[Lan/f;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Lan/f;I)V
    .locals 6

    const-wide/16 v4, -0x1

    const/16 v3, 0x14

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/layer/m;->c:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/google/googlenav/layer/m;->d:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/google/googlenav/layer/m;->e:Ljava/lang/String;

    iput v0, p0, Lcom/google/googlenav/layer/m;->h:I

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/googlenav/layer/m;->i:I

    iput-boolean v0, p0, Lcom/google/googlenav/layer/m;->j:Z

    iput-boolean v0, p0, Lcom/google/googlenav/layer/m;->k:Z

    iput-boolean v0, p0, Lcom/google/googlenav/layer/m;->l:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/layer/m;->t:Lo/o;

    new-array v1, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v1, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p3, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    array-length v1, p3

    new-array v1, v1, [Lcom/google/googlenav/e;

    iput-object v1, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    aget-object v2, p3, v0

    invoke-static {v2}, Lcom/google/googlenav/e;->a(Lam/f;)Lcom/google/googlenav/e;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput p4, p0, Lcom/google/googlenav/layer/m;->q:I

    iput-byte v3, p0, Lcom/google/googlenav/layer/m;->g:B

    :goto_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/layer/m;->m:I

    const/16 v0, 0x16

    iput v0, p0, Lcom/google/googlenav/layer/m;->n:I

    iput-wide v4, p0, Lcom/google/googlenav/layer/m;->r:J

    iput-wide v4, p0, Lcom/google/googlenav/layer/m;->s:J

    return-void

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/layer/m;->q:I

    iput-byte v3, p0, Lcom/google/googlenav/layer/m;->g:B

    goto :goto_1
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    const-string v1, "msid:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x47

    iget-object v2, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x48

    iget-object v2, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x49

    iget-object v2, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x73

    iget-object v2, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x74

    iget-object v2, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/layer/m;->o:[Lcom/google/googlenav/e;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/layer/m;->t:Lo/o;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/layer/m;->m:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/layer/m;->n:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/layer/m;->r()Lcom/google/googlenav/layer/m;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public f()B
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/layer/m;->g:B

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/layer/m;->q:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/layer/m;->h:I

    return v0
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/layer/m;->r:J

    return-wide v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/layer/m;->s:J

    return-wide v0
.end method

.method public k()Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/layer/m;->i:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/layer/m;->l:Z

    return v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/layer/m;->j:Z

    return v0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/layer/m;->k:Z

    return v0
.end method

.method public o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/googlenav/layer/m;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/layer/m;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/googlenav/layer/m;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/4 v0, 0x4

    iget v3, p0, Lcom/google/googlenav/layer/m;->i:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x5

    iget v3, p0, Lcom/google/googlenav/layer/m;->h:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v3, 0x3d

    iget v4, p0, Lcom/google/googlenav/layer/m;->m:I

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v3, 0x3e

    iget v4, p0, Lcom/google/googlenav/layer/m;->n:I

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x8

    iget-wide v3, p0, Lcom/google/googlenav/layer/m;->r:J

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x9

    iget-wide v3, p0, Lcom/google/googlenav/layer/m;->s:J

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x10

    iget-boolean v3, p0, Lcom/google/googlenav/layer/m;->k:Z

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x13

    iget-boolean v3, p0, Lcom/google/googlenav/layer/m;->j:Z

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x14

    iget-boolean v3, p0, Lcom/google/googlenav/layer/m;->l:Z

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->p:[[B

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/layer/m;->p:[[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/googlenav/layer/m;->p:[[B

    aget-object v4, v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/16 v0, 0x15

    iget v3, p0, Lcom/google/googlenav/layer/m;->q:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    const/16 v0, 0x12

    iget-object v3, p0, Lcom/google/googlenav/layer/m;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v3, v3, v1

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->e:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    const-string v1, "msid:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->u:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/layer/m;->t:Lo/o;

    if-eqz v0, :cond_5

    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->t:Lo/o;

    invoke-virtual {v1}, Lo/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    return-object v2
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->v:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public q()Lo/o;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/m;->t:Lo/o;

    return-object v0
.end method

.method protected r()Lcom/google/googlenav/layer/m;
    .locals 1

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/m;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/layer/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
