.class public Lcom/google/googlenav/layer/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/layer/q;


# instance fields
.field private final a:Lbf/i;

.field private final b:Lcom/google/googlenav/layer/m;

.field private final c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lbf/i;Lcom/google/googlenav/layer/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/s;->c:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    iput-object p2, p0, Lcom/google/googlenav/layer/s;->b:Lcom/google/googlenav/layer/m;

    return-void
.end method

.method private a(Lcom/google/googlenav/W;Z)Law/g;
    .locals 7

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/googlenav/W;->r()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v5, v3, v5

    if-eqz v5, :cond_4

    cmp-long v1, v1, v3

    if-lez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/W;->q()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->c:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/p;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/googlenav/layer/p;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/W;->w()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/layer/q;)V

    iget-object v1, p0, Lcom/google/googlenav/layer/s;->c:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x43

    if-eqz p2, :cond_5

    const-string v1, "p"

    :goto_2
    invoke-static {p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/W;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    :cond_3
    invoke-virtual {v0}, Lcom/google/googlenav/layer/p;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/p;->a(Z)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "LoadingDetailsHelper.potentiallyFetchDetails"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    :try_start_1
    const-string v1, "i"
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private a()Lcom/google/googlenav/T;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Layer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v2}, Lbf/i;->av()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support loading details on-demand."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "LoadingDetailsHelper.getLayerFeatureSet"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :sswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/y;

    invoke-virtual {v0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_1
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/aJ;

    invoke-virtual {v0}, Lbf/aJ;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/ak;

    invoke-virtual {v0}, Lbf/ak;->b()Lcom/google/googlenav/T;

    move-result-object v0

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/bx;

    invoke-virtual {v0}, Lbf/bx;->f()Lcom/google/googlenav/T;

    move-result-object v0

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/bU;

    invoke-virtual {v0}, Lbf/bU;->bN()Lcom/google/googlenav/T;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0xb -> :sswitch_0
        0xf -> :sswitch_3
        0x10 -> :sswitch_4
        0x17 -> :sswitch_5
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method private static a(Lcom/google/googlenav/W;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "l="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ai;)Law/g;
    .locals 2

    :try_start_0
    check-cast p1, Lcom/google/googlenav/W;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/W;Z)Law/g;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "LoadingDetailsHelper.loadDetails"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/W;Z)V
    .locals 6

    const-wide/16 v2, -0x1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_7

    invoke-direct {p0}, Lcom/google/googlenav/layer/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    invoke-virtual {p2, v0}, Lcom/google/googlenav/W;->a(B)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->b:Lcom/google/googlenav/layer/m;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->b:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->j()J

    move-result-wide v0

    :goto_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Lcom/google/googlenav/W;->r()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p2, v0, v1}, Lcom/google/googlenav/W;->a(J)V

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/layer/s;->a()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/T;->a(Ljava/lang/String;Lcom/google/googlenav/W;)V

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0, p2}, Lbf/i;->a(Lcom/google/googlenav/ai;)V

    move-object v0, p2

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/layer/s;->b:Lcom/google/googlenav/layer/m;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/layer/s;->b:Lcom/google/googlenav/layer/m;

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "msid:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/layer/s;->b:Lcom/google/googlenav/layer/m;

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/W;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/googlenav/W;->a(B)V

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    instance-of v0, v0, Lbf/m;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    check-cast v0, Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bq()V

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ao()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->an()Z

    :cond_5
    return-void

    :cond_6
    move-wide v0, v2

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/W;->a(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/layer/s;->a()Lcom/google/googlenav/T;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/googlenav/T;->a(Ljava/lang/String;Lcom/google/googlenav/W;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "LoadingDetailsHelper.complete"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_8
    if-eqz p3, :cond_4

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->e(Z)V

    iget-object v0, p0, Lcom/google/googlenav/layer/s;->a:Lbf/i;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public b(Lcom/google/googlenav/ai;)Law/g;
    .locals 2

    :try_start_0
    check-cast p1, Lcom/google/googlenav/W;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/W;Z)Law/g;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "LoadingDetailsHelper.prefetchDetails"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
