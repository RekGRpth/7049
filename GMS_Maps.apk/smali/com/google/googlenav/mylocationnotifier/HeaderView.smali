.class public Lcom/google/googlenav/mylocationnotifier/HeaderView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/googlenav/mylocationnotifier/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/HeaderView;->a:Z

    return-void
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/HeaderView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/HeaderView;->b:Lcom/google/googlenav/mylocationnotifier/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/HeaderView;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    :cond_0
    return-void
.end method

.method public setMyLocationNotifierManager(Lcom/google/googlenav/mylocationnotifier/k;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/HeaderView;->b:Lcom/google/googlenav/mylocationnotifier/k;

    return-void
.end method

.method public setShouldDeactivateOnDetach(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/mylocationnotifier/HeaderView;->a:Z

    return-void
.end method
