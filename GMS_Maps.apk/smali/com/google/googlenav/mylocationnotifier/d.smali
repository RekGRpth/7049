.class public Lcom/google/googlenav/mylocationnotifier/d;
.super Lcom/google/googlenav/mylocationnotifier/a;
.source "SourceFile"

# interfaces
.implements LaB/p;


# instance fields
.field private f:Lcom/google/googlenav/ai;

.field private g:Lcom/google/googlenav/ui/view/android/J;

.field private h:Lcom/google/googlenav/mylocationnotifier/l;

.field private i:Z

.field private j:Z

.field private final k:LaB/s;

.field private final l:Lcom/google/googlenav/ui/br;

.field private m:Lcom/google/googlenav/ui/bs;

.field private n:Lam/f;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/mylocationnotifier/k;Lcom/google/googlenav/mylocationnotifier/l;Z)V
    .locals 3

    invoke-direct {p0, p2}, Lcom/google/googlenav/mylocationnotifier/a;-><init>(Lcom/google/googlenav/mylocationnotifier/k;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->j:Z

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    iput-object p3, p0, Lcom/google/googlenav/mylocationnotifier/d;->h:Lcom/google/googlenav/mylocationnotifier/l;

    iput-boolean p4, p0, Lcom/google/googlenav/mylocationnotifier/d;->i:Z

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->k:LaB/s;

    new-instance v0, Lcom/google/googlenav/ui/br;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->k:LaB/s;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->l:Lcom/google/googlenav/ui/br;

    return-void
.end method

.method private a(Ljava/lang/CharSequence;II)Lbj/H;
    .locals 1

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/g;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/google/googlenav/mylocationnotifier/g;-><init>(Lcom/google/googlenav/mylocationnotifier/d;ILjava/lang/CharSequence;I)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/mylocationnotifier/d;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 9

    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-static {v1, v4}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v1

    invoke-static {v1}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->k:LaB/s;

    invoke-virtual {v2, v1}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->k:LaB/s;

    invoke-virtual {v2, v1, p0}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ai;->d(Z)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bf()Lcom/google/googlenav/cx;

    move-result-object v0

    :goto_1
    new-instance v7, Lcom/google/googlenav/ui/view/android/bt;

    invoke-direct {v7}, Lcom/google/googlenav/ui/view/android/bt;-><init>()V

    invoke-virtual {v7, v3}, Lcom/google/googlenav/ui/view/android/bt;->d(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v7

    const v8, 0x7f04011d

    invoke-virtual {v7, v8}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/googlenav/ui/view/android/bt;->e(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/view/android/bt;->f(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/view/android/bt;->i(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->n:Lam/f;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Lam/f;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->b(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->l:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/br;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/cx;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bt;->e(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->T()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->e(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/view/android/bt;->d(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bt;->a()Lcom/google/googlenav/ui/view/android/bs;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->k:LaB/s;

    invoke-virtual {v2, v1}, LaB/s;->a(Lcom/google/googlenav/ui/bs;)Lam/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->n:Lam/f;

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_2

    :cond_3
    move-object v1, v0

    move-object v2, v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/mylocationnotifier/d;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/mylocationnotifier/d;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/mylocationnotifier/d;)Lcom/google/googlenav/ui/view/android/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/mylocationnotifier/d;)Lcom/google/googlenav/mylocationnotifier/l;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->h:Lcom/google/googlenav/mylocationnotifier/l;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/mylocationnotifier/d;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->i:Z

    return v0
.end method

.method private m()Ljava/util/List;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v1}, Lcom/google/googlenav/mylocationnotifier/d;->a(Ljava/util/List;)V

    const/16 v0, 0x2ca

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f02026e

    const/4 v3, 0x2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/mylocationnotifier/d;->a(Ljava/lang/CharSequence;II)Lbj/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x70

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f020214

    const/4 v3, 0x3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/mylocationnotifier/d;->a(Ljava/lang/CharSequence;II)Lbj/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x422

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f020275

    const/4 v3, 0x4

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/mylocationnotifier/d;->a(Ljava/lang/CharSequence;II)Lbj/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0x1e5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f02027d

    const/4 v3, 0x5

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/mylocationnotifier/d;->a(Ljava/lang/CharSequence;II)Lbj/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->c(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x31d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f020270

    const/4 v3, 0x6

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/mylocationnotifier/d;->a(Ljava/lang/CharSequence;II)Lbj/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v1

    :cond_3
    const/16 v0, 0x3f8

    goto :goto_0
.end method


# virtual methods
.method public Q_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->m:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->k:LaB/s;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->m:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v1}, LaB/s;->a(Lcom/google/googlenav/ui/bs;)Lam/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->n:Lam/f;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {p0}, Lcom/google/googlenav/mylocationnotifier/d;->m()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->b:Lcom/google/googlenav/mylocationnotifier/k;

    iput-object p1, v0, Lcom/google/googlenav/mylocationnotifier/k;->c:Lcom/google/googlenav/ai;

    iput-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->m:Lcom/google/googlenav/ui/bs;

    iput-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->n:Lam/f;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {p0}, Lcom/google/googlenav/mylocationnotifier/d;->m()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->j:Z

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/l;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/google/googlenav/mylocationnotifier/l;-><init>(Ljava/util/List;Lcom/google/googlenav/mylocationnotifier/k;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->h:Lcom/google/googlenav/mylocationnotifier/l;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const v1, 0x7f10004a

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/mylocationnotifier/d;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x2d3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {p0}, Lcom/google/googlenav/mylocationnotifier/d;->m()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method protected b()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/mylocationnotifier/d;->a:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400f2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->i:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x2c6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const v2, 0x7f10004a

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/mylocationnotifier/d;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const v1, 0x7f10001e

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/mylocationnotifier/d;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->i:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/e;

    invoke-direct {v1, p0}, Lcom/google/googlenav/mylocationnotifier/e;-><init>(Lcom/google/googlenav/mylocationnotifier/d;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/mylocationnotifier/HeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    const/16 v0, 0x2c8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->f:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected c()Landroid/widget/ListAdapter;
    .locals 5

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    sget-object v1, Lcom/google/googlenav/mylocationnotifier/d;->a:Lcom/google/android/maps/MapsActivity;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/mylocationnotifier/d;->m()Ljava/util/List;

    move-result-object v3

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->g:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method protected d()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/f;

    invoke-direct {v0, p0}, Lcom/google/googlenav/mylocationnotifier/f;-><init>(Lcom/google/googlenav/mylocationnotifier/d;)V

    return-object v0
.end method

.method protected f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->i:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/d;->k()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/d;->h:Lcom/google/googlenav/mylocationnotifier/l;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/l;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/d;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;)Z

    :cond_1
    return-void
.end method
