.class public Lcom/google/googlenav/mylocationnotifier/i;
.super Lcom/google/googlenav/mylocationnotifier/a;
.source "SourceFile"


# instance fields
.field private final f:Z

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/mylocationnotifier/k;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/mylocationnotifier/i;-><init>(Lcom/google/googlenav/mylocationnotifier/k;ZLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/mylocationnotifier/k;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/mylocationnotifier/a;-><init>(Lcom/google/googlenav/mylocationnotifier/k;)V

    iput-boolean p2, p0, Lcom/google/googlenav/mylocationnotifier/i;->f:Z

    iput-object p3, p0, Lcom/google/googlenav/mylocationnotifier/i;->g:Ljava/lang/String;

    return-void
.end method

.method private a(LaH/h;)V
    .locals 3

    invoke-virtual {p1}, LaH/h;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/j;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/mylocationnotifier/j;-><init>(Lcom/google/googlenav/mylocationnotifier/i;LaH/h;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/l;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/i;->b:Lcom/google/googlenav/mylocationnotifier/k;

    iget-boolean v2, p0, Lcom/google/googlenav/mylocationnotifier/i;->f:Z

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/google/googlenav/mylocationnotifier/l;-><init>(Ljava/util/List;Lcom/google/googlenav/mylocationnotifier/k;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/i;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(Lcom/google/googlenav/mylocationnotifier/a;)V

    return-void
.end method

.method protected b()V
    .locals 7

    const v6, 0x7f10001e

    const/4 v5, 0x1

    const v4, 0x7f10004a

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/mylocationnotifier/i;->a:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400f3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->g:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {p0, v0, v6}, Lcom/google/googlenav/mylocationnotifier/i;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x292

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {p0, v0, v4}, Lcom/google/googlenav/mylocationnotifier/i;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x4f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/i;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {p0, v1, v6}, Lcom/google/googlenav/mylocationnotifier/i;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v1

    aget-object v2, v0, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {p0, v1, v4}, Lcom/google/googlenav/mylocationnotifier/i;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v1

    aget-object v0, v0, v5

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {p0, v0, v4}, Lcom/google/googlenav/mylocationnotifier/i;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected c()Landroid/widget/ListAdapter;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected d()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->c:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x2c7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    return-void
.end method

.method protected f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/i;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/i;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/mylocationnotifier/i;->a(LaH/h;)V

    :cond_0
    return-void
.end method
