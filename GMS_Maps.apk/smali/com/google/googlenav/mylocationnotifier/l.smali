.class public Lcom/google/googlenav/mylocationnotifier/l;
.super Lcom/google/googlenav/mylocationnotifier/a;
.source "SourceFile"


# instance fields
.field private final f:[Lcom/google/googlenav/ai;

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private i:Lcom/google/googlenav/mylocationnotifier/d;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/googlenav/mylocationnotifier/k;Ljava/lang/String;Z)V
    .locals 5

    const/4 v2, 0x0

    invoke-direct {p0, p2}, Lcom/google/googlenav/mylocationnotifier/a;-><init>(Lcom/google/googlenav/mylocationnotifier/k;)V

    iput-object p3, p0, Lcom/google/googlenav/mylocationnotifier/l;->g:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/googlenav/mylocationnotifier/l;->h:Z

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;B)Lcom/google/googlenav/ai;

    move-result-object v0

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/mylocationnotifier/q;-><init>(Lcom/google/googlenav/mylocationnotifier/l;)V

    aput-object v1, v0, v3

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/mylocationnotifier/l;Lcom/google/googlenav/mylocationnotifier/d;)Lcom/google/googlenav/mylocationnotifier/d;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/l;->i:Lcom/google/googlenav/mylocationnotifier/d;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/mylocationnotifier/l;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/mylocationnotifier/l;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/mylocationnotifier/l;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/o;

    invoke-direct {v0, p0, p2}, Lcom/google/googlenav/mylocationnotifier/o;-><init>(Lcom/google/googlenav/mylocationnotifier/l;I)V

    new-instance v1, Lcom/google/googlenav/f;

    invoke-direct {v1, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/mylocationnotifier/l;)[Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/mylocationnotifier/l;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/mylocationnotifier/l;->m()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/mylocationnotifier/l;)Lcom/google/googlenav/mylocationnotifier/d;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->i:Lcom/google/googlenav/mylocationnotifier/d;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/mylocationnotifier/l;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->h:Z

    return v0
.end method

.method private m()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/l;->l()V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/p;

    invoke-direct {v1, p0}, Lcom/google/googlenav/mylocationnotifier/p;-><init>(Lcom/google/googlenav/mylocationnotifier/l;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/googlenav/actionbar/b;)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget-boolean v1, p0, Lcom/google/googlenav/mylocationnotifier/l;->h:Z

    if-eqz v1, :cond_0

    const/16 v0, 0x2d2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-gt v0, v2, :cond_1

    const/16 v1, 0x2cc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v1, 0x2cb

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ge p1, v1, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v1, -0x1

    if-ge p1, v2, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/l;

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/l;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/google/googlenav/mylocationnotifier/l;-><init>(Ljava/util/List;Lcom/google/googlenav/mylocationnotifier/k;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/mylocationnotifier/k;->a(Lcom/google/googlenav/mylocationnotifier/a;)V

    return-void
.end method

.method public a()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-gt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/mylocationnotifier/l;->a:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400f4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const v1, 0x7f10001e

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/mylocationnotifier/l;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/mylocationnotifier/l;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/l;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const v2, 0x7f10004a

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/mylocationnotifier/l;->a(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/mylocationnotifier/l;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/m;

    invoke-direct {v1, p0}, Lcom/google/googlenav/mylocationnotifier/m;-><init>(Lcom/google/googlenav/mylocationnotifier/l;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/mylocationnotifier/HeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method protected c()Landroid/widget/ListAdapter;
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/view/android/bA;

    sget-object v1, Lcom/google/googlenav/mylocationnotifier/l;->a:Lcom/google/android/maps/MapsActivity;

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/l;->f:[Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/bA;-><init>(Landroid/content/Context;[Lcom/google/googlenav/ai;)V

    return-object v0
.end method

.method protected d()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/n;

    invoke-direct {v0, p0}, Lcom/google/googlenav/mylocationnotifier/n;-><init>(Lcom/google/googlenav/mylocationnotifier/l;)V

    return-object v0
.end method

.method protected f()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/l;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/l;->k()V

    :cond_0
    return-void
.end method
