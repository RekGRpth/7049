.class public Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;
.super Lcom/google/googlenav/android/DelegateIntentProcessorHandler;
.source "SourceFile"


# instance fields
.field private dialog:Lcom/google/googlenav/ui/view/dialog/bk;

.field private intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/bk;Lcom/google/googlenav/android/R;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;-><init>(Lcom/google/googlenav/android/R;)V

    iput-object p1, p0, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;->dialog:Lcom/google/googlenav/ui/view/dialog/bk;

    iput-object p3, p0, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;->intent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public resetForInvocation()V
    .locals 0

    return-void
.end method

.method public startBusinessDetailsLayer(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;->dialog:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Ljava/lang/String;Z)V

    return-void
.end method
