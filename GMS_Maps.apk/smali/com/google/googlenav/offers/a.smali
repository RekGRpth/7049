.class public Lcom/google/googlenav/offers/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/s;

.field private final b:LaN/u;

.field private final c:Lcom/google/googlenav/ui/wizard/jv;

.field private final d:Lbf/am;

.field private e:LaN/B;

.field private f:Lcom/google/googlenav/aZ;

.field private final g:Ljava/lang/Object;

.field private h:I

.field private i:J

.field private final j:Las/d;

.field private final k:Las/d;

.field private final l:LaN/v;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/u;Lcom/google/googlenav/ui/wizard/jv;Lbf/am;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/offers/a;->g:Ljava/lang/Object;

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->a()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/offers/a;->h:I

    new-instance v0, Lcom/google/googlenav/offers/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/offers/b;-><init>(Lcom/google/googlenav/offers/a;)V

    iput-object v0, p0, Lcom/google/googlenav/offers/a;->l:LaN/v;

    iput-object p1, p0, Lcom/google/googlenav/offers/a;->a:Lcom/google/googlenav/ui/s;

    iput-object p2, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->l:LaN/v;

    invoke-virtual {p2, v0}, LaN/u;->a(LaN/v;)V

    iput-object p3, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p4, p0, Lcom/google/googlenav/offers/a;->d:Lbf/am;

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->e()J

    move-result-wide v0

    mul-long/2addr v0, v0

    iput-wide v0, p0, Lcom/google/googlenav/offers/a;->i:J

    invoke-virtual {p3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->A()Lcom/google/googlenav/ui/wizard/fe;

    new-instance v0, Lcom/google/googlenav/offers/c;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/offers/c;-><init>(Lcom/google/googlenav/offers/a;Las/c;)V

    iput-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    new-instance v0, Lcom/google/googlenav/offers/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/offers/d;-><init>(Lcom/google/googlenav/offers/a;Las/c;)V

    iput-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/offers/a;LaN/B;)LaN/B;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/offers/a;->e:LaN/B;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/offers/a;)LaN/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/offers/a;Lcom/google/googlenav/aZ;)Lcom/google/googlenav/aZ;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    return-object p1
.end method

.method private a(LaN/B;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/offers/a;->e:LaN/B;

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p2}, Lcom/google/googlenav/offers/a;->a(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/offers/a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/offers/a;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/offers/a;LaN/B;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/offers/a;->a(LaN/B;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/offers/a;)LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->e:LaN/B;

    return-object v0
.end method

.method private declared-synchronized b(I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->b()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Las/d;->c(J)V

    :cond_0
    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    invoke-virtual {v0}, Las/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/googlenav/offers/a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->g:Ljava/lang/Object;

    return-object v0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    sget v1, LaA/c;->a:I

    if-le p0, v1, :cond_0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    const/16 v0, 0x323

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v0, 0x322

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/googlenav/offers/a;)Lcom/google/googlenav/aZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/offers/a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/offers/a;->i:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/googlenav/offers/a;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private declared-synchronized f()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    invoke-virtual {v0}, Las/d;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/offers/a;->k:Las/d;

    invoke-virtual {v1}, Las/d;->c()I

    const-string v1, "OFFER_CONTENT_HINT_FIRST_REFRESH_PERIOD_PREFERENCE"

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->b()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->A()Lcom/google/googlenav/ui/wizard/fe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fe;->e()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->f()V

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/google/googlenav/offers/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/offers/a;->g()V

    return-void
.end method


# virtual methods
.method protected a(Z)Lcom/google/googlenav/aZ;
    .locals 6

    const/4 v5, 0x5

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/bc;

    const-string v3, ""

    const/4 v4, 0x0

    invoke-direct {v2, v5, v3, v4}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    invoke-virtual {v2}, LaN/u;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(I)Lcom/google/googlenav/bg;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    invoke-virtual {v2}, LaN/u;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->d(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->j(Z)Lcom/google/googlenav/bg;

    const-string v1, "28"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    :goto_0
    if-nez p1, :cond_0

    new-instance v1, Lcom/google/googlenav/offers/f;

    invoke-direct {v1, p0}, Lcom/google/googlenav/offers/f;-><init>(Lcom/google/googlenav/offers/a;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/aZ;

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    return-object v1

    :cond_1
    const-string v1, "27"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    goto :goto_0
.end method

.method public a()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->a()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/offers/a;->h:I

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->e()J

    move-result-wide v0

    mul-long/2addr v0, v0

    iput-wide v0, p0, Lcom/google/googlenav/offers/a;->i:J

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/d;->b()I

    move-result v0

    const-string v1, "OFFER_CONTENT_HINT_FIRST_REFRESH_PERIOD_PREFERENCE"

    new-instance v2, Lcom/google/googlenav/offers/e;

    invoke-direct {v2, p0}, Lcom/google/googlenav/offers/e;-><init>(Lcom/google/googlenav/offers/a;)V

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    return-void
.end method

.method public a(I)V
    .locals 12

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v10

    sget v0, LaA/c;->a:I

    if-le p1, v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/offers/a;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aB()Lav/a;

    move-result-object v0

    const v3, 0x7f1000c1

    const v4, 0x7f050014

    const v5, 0x7f050015

    const/16 v6, 0xfa0

    const/4 v7, 0x0

    new-instance v8, Lcom/google/googlenav/offers/g;

    invoke-direct {v8, p0}, Lcom/google/googlenav/offers/g;-><init>(Lcom/google/googlenav/offers/a;)V

    new-instance v9, Lcom/google/googlenav/offers/h;

    invoke-direct {v9, p0, v10, v11}, Lcom/google/googlenav/offers/h;-><init>(Lcom/google/googlenav/offers/a;J)V

    const/16 v10, 0x58

    move v1, p1

    invoke-virtual/range {v0 .. v10}, Lav/a;->a(ILjava/lang/CharSequence;IIIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/offers/a;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(ZLjava/lang/String;)V
    .locals 12

    const/4 v2, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const-string v1, "f"

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/offers/a;->a(Z)Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v4

    invoke-virtual {v4, v3}, Law/h;->c(Law/g;)V

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v4

    invoke-virtual {v4}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "q="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "t="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->z()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "s="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v10

    const-string v3, "c="

    aput-object v3, v5, v11

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v10, v4, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/16 v3, 0x58

    const-string v4, "s"

    new-array v5, v11, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "t="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ub="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    if-nez v0, :cond_2

    move-object v0, v2

    :goto_1
    aput-object v0, v5, v10

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/googlenav/offers/a;->g:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    const-string v1, "t"

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/googlenav/offers/a;->d:Lbf/am;

    iget-object v5, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    invoke-virtual {v4, v5}, Lbf/am;->d(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/google/googlenav/ui/wizard/jv;->c(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/googlenav/aZ;->b(Z)V

    iget-object v5, p0, Lcom/google/googlenav/offers/a;->d:Lbf/am;

    invoke-virtual {v5, v4}, Lbf/am;->f(Lbf/i;)V

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lbf/aJ;->a(B)V

    iget-object v5, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    invoke-virtual {v5}, Lcom/google/googlenav/aZ;->ap()I

    move-result v5

    invoke-virtual {v4, v5}, Lbf/aJ;->b(I)V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    iget-object v4, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/jC;->R()Lcom/google/googlenav/ui/wizard/fl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/fl;->a()V

    :goto_2
    monitor-exit v3

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    const-string v4, ""

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/jv;->c(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/offers/a;->a(Z)Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/googlenav/offers/a;->a:Lcom/google/googlenav/ui/s;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v4, v6, v7}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method a(LaN/B;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/offers/a;->d()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/clientparam/d;->d()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/googlenav/offers/a;->e:LaN/B;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/googlenav/offers/a;->e:LaN/B;

    invoke-virtual {v3, p1}, LaN/B;->a(LaN/B;)J

    move-result-wide v3

    mul-long/2addr v1, v1

    cmp-long v1, v3, v1

    if-ltz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/offers/a;->f()V

    iget-object v1, p0, Lcom/google/googlenav/offers/a;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/googlenav/offers/a;->f:Lcom/google/googlenav/aZ;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    iget v0, p0, Lcom/google/googlenav/offers/a;->h:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    iget v1, p0, Lcom/google/googlenav/offers/a;->h:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/offers/a;->j:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    :cond_1
    return-void
.end method

.method d()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ag()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->A()Lcom/google/googlenav/ui/wizard/fe;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/fe;->e()I

    move-result v2

    if-ne v2, v1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->d:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->C()Lbf/aJ;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->d:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->v()Lbf/O;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/offers/a;->d:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->w()Lbf/bK;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->e()Lcom/google/googlenav/clientparam/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/d;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/offers/a;->b:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    if-lt v3, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->R()Lcom/google/googlenav/ui/wizard/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fl;->a()V

    iget-object v0, p0, Lcom/google/googlenav/offers/a;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/offers/a;->g()V

    return-void
.end method
