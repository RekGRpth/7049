.class public Lcom/google/googlenav/offers/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bl;


# static fields
.field public static final a:Lcom/google/googlenav/offers/j;

.field public static final b:Lcom/google/googlenav/offers/j;


# instance fields
.field c:Lcom/google/googlenav/ui/view/dialog/bk;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/offers/j;

    const v1, 0x7f040108

    invoke-direct {v0, v1}, Lcom/google/googlenav/offers/j;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/offers/j;->a:Lcom/google/googlenav/offers/j;

    new-instance v0, Lcom/google/googlenav/offers/j;

    const v1, 0x7f040107

    invoke-direct {v0, v1}, Lcom/google/googlenav/offers/j;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/offers/j;->b:Lcom/google/googlenav/offers/j;

    return-void
.end method

.method constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    iput p1, p0, Lcom/google/googlenav/offers/j;->d:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/offers/j;->b(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->show()V

    invoke-virtual {p0}, Lcom/google/googlenav/offers/j;->a()V

    iget-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    return-object v0
.end method

.method public a()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/k;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/k;->b()Z

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/dialog/bk;->m()Landroid/support/v4/app/k;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/k;->a()Landroid/support/v4/app/t;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Landroid/support/v4/app/f;)Landroid/support/v4/app/t;

    invoke-virtual {v0}, Landroid/support/v4/app/t;->b()I

    invoke-virtual {p0}, Lcom/google/googlenav/offers/j;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    if-ne v0, p1, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/offers/j;->c:Lcom/google/googlenav/ui/view/dialog/bk;

    :cond_1
    return-void
.end method

.method protected b(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;
    .locals 2

    const/16 v0, 0x31d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/offers/j;->d:I

    invoke-static {v0, v1, p0, p1}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Ljava/lang/CharSequence;ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    return-object v0
.end method
