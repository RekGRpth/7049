.class public Lcom/google/googlenav/prefetch/android/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;


# instance fields
.field private final a:LaN/B;

.field private final b:LaN/B;

.field private final c:Lo/aq;

.field private final d:Lo/aq;

.field private e:I

.field private f:I

.field private g:Lo/aq;

.field private h:Lo/aq;

.field private i:Lo/aq;


# direct methods
.method public constructor <init>(LaN/B;LaN/B;)V
    .locals 3

    const/16 v2, 0xe

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    iput-object p2, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->a(ILo/T;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {p2}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p2}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->a(ILo/T;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-direct {p0}, Lcom/google/googlenav/prefetch/android/k;->l()V

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/prefetch/android/k;
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/prefetch/android/k;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/prefetch/android/k;-><init>(LaN/B;LaN/B;)V

    return-object v2
.end method

.method private b(I)Landroid/util/Pair;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    sub-int v1, p1, v0

    if-lez v1, :cond_0

    new-instance v0, Lo/aq;

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    shl-int/2addr v2, v1

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    shl-int v1, v3, v1

    invoke-direct {v0, p1, v2, v1}, Lo/aq;-><init>(III)V

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    sub-int v2, p1, v1

    if-lez v2, :cond_1

    new-instance v1, Lo/aq;

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->c()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    shl-int/2addr v3, v2

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    shl-int v2, v4, v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v1, p1, v3, v2}, Lo/aq;-><init>(III)V

    :goto_1
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lo/aq;

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    neg-int v3, v1

    shr-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    neg-int v1, v1

    shr-int v1, v3, v1

    invoke-direct {v0, p1, v2, v1}, Lo/aq;-><init>(III)V

    goto :goto_0

    :cond_1
    new-instance v1, Lo/aq;

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->c()I

    move-result v3

    neg-int v4, v2

    shr-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    neg-int v2, v2

    shr-int v2, v4, v2

    invoke-direct {v1, p1, v3, v2}, Lo/aq;-><init>(III)V

    goto :goto_1
.end method

.method private l()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/prefetch/android/k;->m()V

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    return-void
.end method

.method private m()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x5

    move v2, v1

    move v1, v0

    :goto_0
    const/16 v0, 0xe

    if-gt v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->c()I

    move-result v3

    sub-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    if-gez v0, :cond_0

    const/4 v3, 0x1

    shl-int/2addr v3, v1

    add-int/2addr v0, v3

    :cond_0
    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v0, v3

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    iput v2, p0, Lcom/google/googlenav/prefetch/android/k;->e:I

    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    return-object v0
.end method

.method public a(I)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/googlenav/prefetch/android/k;->b(I)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    new-instance v0, Lo/aq;

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lo/aq;-><init>(III)V

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    return-void
.end method

.method public a(Lo/aq;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/4 v3, 0x5

    if-lt v0, v3, :cond_0

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/16 v3, 0xe

    if-le v0, v3, :cond_2

    :cond_0
    move v2, v1

    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->b(I)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-le v4, v0, :cond_6

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-ge v4, v0, :cond_3

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-gt v4, v0, :cond_5

    :cond_3
    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lo/aq;->d()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    if-lt v4, v0, :cond_4

    invoke-virtual {p1}, Lo/aq;->d()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    if-le v4, v0, :cond_1

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-lt v4, v0, :cond_7

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-gt v4, v0, :cond_7

    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public b()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    return-object v0
.end method

.method public c()Lo/aq;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    if-nez v0, :cond_0

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    new-instance v3, Lo/aq;

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->c()I

    move-result v1

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    invoke-direct {v3, v0, v1, v4}, Lo/aq;-><init>(III)V

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->c()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->c()I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v5}, Lo/aq;->b()I

    move-result v5

    shl-int/2addr v4, v5

    if-lt v1, v4, :cond_2

    const/4 v1, 0x0

    :cond_2
    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    if-le v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    :goto_1
    move-object v0, v3

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    goto :goto_1

    :cond_4
    new-instance v2, Lo/aq;

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->b()I

    move-result v4

    invoke-direct {v2, v4, v1, v0}, Lo/aq;-><init>(III)V

    iput-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    goto :goto_1
.end method

.method public d()V
    .locals 1

    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->e:I

    iput v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/googlenav/prefetch/android/k;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/googlenav/prefetch/android/k;

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    iget-object v2, p1, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v1, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    iget-object v2, p1, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v1, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/r;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-static {v2}, LaN/C;->c(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-static {v2}, LaN/C;->c(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->e:I

    return v0
.end method

.method public i()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public j()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    sub-int/2addr v0, v1

    if-gez v0, :cond_0

    const v1, 0x15752a00

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public k()LaN/B;
    .locals 4

    new-instance v0, LaN/B;

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/googlenav/prefetch/android/k;->j()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method
