.class Lcom/google/googlenav/settings/E;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/googlenav/settings/LegalActivity;

.field private b:[B

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/settings/LegalActivity;Ljava/lang/String;[B)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/settings/E;->a:Lcom/google/googlenav/settings/LegalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/googlenav/settings/E;->b:[B

    iput-object p2, p0, Lcom/google/googlenav/settings/E;->c:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/settings/LegalActivity;Ljava/lang/String;[BLcom/google/googlenav/settings/D;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/settings/E;-><init>(Lcom/google/googlenav/settings/LegalActivity;Ljava/lang/String;[B)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/settings/E;->a:Lcom/google/googlenav/settings/LegalActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LegalActivity;->a(Lcom/google/googlenav/settings/LegalActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/E;->a:Lcom/google/googlenav/settings/LegalActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LegalActivity;->a(Lcom/google/googlenav/settings/LegalActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/E;->a:Lcom/google/googlenav/settings/LegalActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LegalActivity;->a(Lcom/google/googlenav/settings/LegalActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/E;->b:[B

    if-nez v0, :cond_1

    const-string v0, "FileCompleteHandler"

    iget-object v1, p0, Lcom/google/googlenav/settings/E;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/settings/E;->b:[B

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/googlenav/android/WebViewActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    iget-object v3, p0, Lcom/google/googlenav/settings/E;->a:Lcom/google/googlenav/settings/LegalActivity;

    invoke-static {v3}, Lcom/google/googlenav/settings/LegalActivity;->b(Lcom/google/googlenav/settings/LegalActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "html_string"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "base_url"

    iget-object v2, p0, Lcom/google/googlenav/settings/E;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/settings/E;->b:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_1
.end method
