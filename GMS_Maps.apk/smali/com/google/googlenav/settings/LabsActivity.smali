.class public Lcom/google/googlenav/settings/LabsActivity;
.super Lcom/google/googlenav/settings/GmmPreferenceActivity;
.source "SourceFile"


# instance fields
.field private b:LaE/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;-><init>()V

    invoke-static {}, LaE/f;->a()LaE/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LabsActivity;->b:LaE/f;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LabsActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f060005

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LabsActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LabsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const/16 v0, 0x20b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LabsActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LabsActivity;->b:LaE/f;

    invoke-virtual {v0}, LaE/f;->b()[LaE/e;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    new-instance v6, Lcom/google/googlenav/settings/c;

    const/16 v7, 0xf

    invoke-direct {v6, p0, v7}, Lcom/google/googlenav/settings/c;-><init>(Landroid/content/Context;I)V

    invoke-interface {v5}, LaE/e;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/googlenav/settings/c;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "    "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v8, 0x20d

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    invoke-interface {v5}, LaE/e;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v8, v9}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v5}, LaE/e;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Lcom/google/googlenav/settings/c;->setSummary(Ljava/lang/CharSequence;)V

    invoke-interface {v5}, LaE/e;->e()Z

    move-result v5

    invoke-virtual {v6, v5}, Lcom/google/googlenav/settings/c;->setChecked(Z)V

    invoke-virtual {v2, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/settings/LabsActivity;->b:LaE/f;

    invoke-virtual {v2}, LaE/f;->b()[LaE/e;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    invoke-interface {v5}, LaE/e;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, LaE/e;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/settings/LabsActivity;->b:LaE/f;

    invoke-virtual {v2, v5}, LaE/f;->b(LaE/e;)V

    iget-object v2, p0, Lcom/google/googlenav/settings/LabsActivity;->a:Lcom/google/googlenav/ui/s;

    const/16 v3, 0x20e

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/String;

    invoke-interface {v5}, LaE/e;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :goto_1
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/settings/LabsActivity;->b:LaE/f;

    invoke-virtual {v2, v5}, LaE/f;->a(LaE/e;)V

    iget-object v2, p0, Lcom/google/googlenav/settings/LabsActivity;->a:Lcom/google/googlenav/ui/s;

    const/16 v3, 0x20c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/String;

    invoke-interface {v5}, LaE/e;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
