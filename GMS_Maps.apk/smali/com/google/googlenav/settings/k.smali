.class Lcom/google/googlenav/settings/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/settings/k;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/settings/k;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/ListPreference;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p2, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/settings/k;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/ListPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "manual"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/settings/k;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0, p2}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/k;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0, p2}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/k;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/settings/q;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/settings/q;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method
