.class public Lcom/google/googlenav/suggest/android/SuggestContentProvider;
.super Landroid/content/ContentProvider;
.source "SourceFile"


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private b:Lcom/google/googlenav/suggest/android/k;

.field private final c:Landroid/content/BroadcastReceiver;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.google.android.maps.SuggestionProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Lcom/google/googlenav/suggest/android/j;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/j;-><init>(Lcom/google/googlenav/suggest/android/SuggestContentProvider;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    return-void
.end method

.method private a()V
    .locals 4

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->f()LaR/n;

    move-result-object v2

    const/4 v3, 0x4

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    invoke-virtual {v1}, LaR/l;->g()LaR/n;

    move-result-object v2

    const/4 v3, 0x6

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    invoke-virtual {v1}, LaR/l;->h()LaR/n;

    move-result-object v2

    const/16 v3, 0x9

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    invoke-virtual {v1}, LaR/l;->i()LaR/n;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    new-instance v1, Lbb/j;

    const/4 v2, 0x1

    invoke-static {v2}, Lbb/i;->a(I)Lbb/w;

    move-result-object v2

    invoke-direct {v1, v2}, Lbb/j;-><init>(Lbb/w;)V

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(Lbb/r;I)V

    return-void
.end method

.method private a(LaR/n;I)V
    .locals 3

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    new-instance v1, Lbb/h;

    invoke-static {p2}, Lbb/i;->a(I)Lbb/w;

    move-result-object v2

    invoke-direct {v1, p1, v2, p2}, Lbb/h;-><init>(LaR/n;Lbb/w;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(Lbb/r;I)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;

    if-eqz v0, :cond_0

    invoke-direct {v0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/SuggestContentProvider;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b()V

    return-void
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    invoke-direct {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c()V

    invoke-static {v2, v2, v2}, Lbb/o;->a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V

    invoke-direct {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private c()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, LaM/a;->a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;

    :cond_0
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {v1}, LaR/l;->a(Lcom/google/googlenav/ui/wizard/jv;)LaR/l;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->r()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->C_()V

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->C_()V

    :cond_1
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.googlenav.suggest.android.SuggestContentProvider.INIT_SUGGEST_PROVIDER"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v5, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    invoke-static {v4, v4, v4}, Lbb/o;->a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V

    new-instance v0, Lcom/google/googlenav/suggest/android/k;

    invoke-direct {v0}, Lcom/google/googlenav/suggest/android/k;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/suggest/android/a;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/googlenav/suggest/android/a;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(Lbb/r;I)V

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.googlenav.suggest.android.SuggestContentProvider.SUGGEST_PROVIDER_CREATED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return v5
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3

    new-instance v0, Lbb/u;

    invoke-direct {v0}, Lbb/u;-><init>()V

    :try_start_0
    invoke-virtual {v0, p4}, Lbb/u;->a([Ljava/lang/String;)Lbb/u;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbb/u;->a(Z)Lbb/u;

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    invoke-virtual {v0}, Lbb/u;->a()Lbb/s;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/suggest/android/k;->a(Lbb/s;)V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/k;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "in_progress"

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
