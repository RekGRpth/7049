.class public abstract Lcom/google/googlenav/suggest/android/i;
.super Landroid/widget/CursorAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/suggest/android/BaseSuggestView;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    invoke-direct {p0, p2, p3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/i;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "in_progress"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    iget-object v1, v1, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    invoke-static {v1}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public synthetic convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/suggest/android/i;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/i;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/i;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/i;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p3}, Lcom/google/googlenav/suggest/android/i;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public abstract newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public final notifyDataSetChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    iget-boolean v0, v0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/CursorAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/i;->a:Lcom/google/googlenav/suggest/android/BaseSuggestView;

    invoke-static {v1}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/i;->a()V

    :cond_0
    return-void
.end method

.method public final notifyDataSetInvalidated()V
    .locals 0

    invoke-super {p0}, Landroid/widget/CursorAdapter;->notifyDataSetInvalidated()V

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/i;->a()V

    return-void
.end method
