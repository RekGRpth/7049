.class public Lcom/google/googlenav/suggest/android/k;
.super Lcom/google/googlenav/provider/e;
.source "SourceFile"

# interfaces
.implements Lbb/v;


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private volatile b:Lbb/z;

.field private c:Landroid/os/Bundle;

.field private d:Lbb/z;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_completion"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/suggest/android/k;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/provider/e;-><init>()V

    new-instance v0, Lbb/z;

    invoke-direct {v0}, Lbb/z;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/k;->e:Z

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbb/o;->a(Lbb/v;)V

    return-void
.end method

.method private a(Lbb/z;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    return-void
.end method


# virtual methods
.method public a(I)Lbb/w;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v0, p1}, Lbb/z;->a(I)Lbb/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbb/s;)V
    .locals 1

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbb/o;->a(Lbb/s;)V

    return-void
.end method

.method public declared-synchronized a(Lbb/z;Z)V
    .locals 1

    const/4 v0, 0x0

    monitor-enter p0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/k;->e:Z

    iput-object p1, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/k;->onChange(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/suggest/android/k;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getCount()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v0}, Lbb/z;->d()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->c:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/k;->c:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public getInt(I)I
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    :cond_0
    return v1
.end method

.method public getLong(I)J
    .locals 2

    if-nez p1, :cond_0

    iget v0, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    int-to-long v0, v0

    return-wide v0

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized getString(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    iget-object v2, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v2}, Lbb/z;->d()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    invoke-virtual {p0, v1}, Lcom/google/googlenav/suggest/android/k;->a(I)Lbb/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-object v0

    :pswitch_1
    :try_start_1
    invoke-virtual {v1}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1}, Lbb/w;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {v1}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    invoke-virtual {v1, v0}, Lbb/w;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-virtual {v1}, Lbb/w;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    invoke-virtual {v1}, Lbb/w;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    invoke-virtual {v1}, Lbb/w;->e()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public declared-synchronized requery()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v0, v1}, Lbb/z;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/k;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "in_progress"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/googlenav/suggest/android/k;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    invoke-direct {p0, v0}, Lcom/google/googlenav/suggest/android/k;->a(Lbb/z;)V

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/k;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "in_progress"

    iget-boolean v2, p0, Lcom/google/googlenav/suggest/android/k;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0}, Lcom/google/googlenav/provider/e;->requery()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
