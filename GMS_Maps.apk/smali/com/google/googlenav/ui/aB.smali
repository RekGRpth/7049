.class public Lcom/google/googlenav/ui/aB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaB/p;


# instance fields
.field private final a:Lcom/google/googlenav/ui/aA;

.field private final b:Lcom/google/googlenav/ui/bs;

.field private final c:Lbf/m;

.field private final d:Lcom/google/googlenav/ui/ay;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;Lbf/m;Lcom/google/googlenav/ui/ay;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/aB;->a:Lcom/google/googlenav/ui/aA;

    iput-object p2, p0, Lcom/google/googlenav/ui/aB;->b:Lcom/google/googlenav/ui/bs;

    iput-object p3, p0, Lcom/google/googlenav/ui/aB;->c:Lbf/m;

    iput-object p4, p0, Lcom/google/googlenav/ui/aB;->d:Lcom/google/googlenav/ui/ay;

    return-void
.end method

.method private b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aB;->c:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ak()Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public Q_()V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/aB;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/aB;->d:Lcom/google/googlenav/ui/ay;

    iget-object v1, p0, Lcom/google/googlenav/ui/aB;->a:Lcom/google/googlenav/ui/aA;

    iget-object v2, p0, Lcom/google/googlenav/ui/aB;->b:Lcom/google/googlenav/ui/bs;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/ay;->a(Lcom/google/googlenav/ui/ay;Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/aB;->c:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bd()V

    :cond_0
    return-void
.end method
