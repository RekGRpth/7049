.class public Lcom/google/googlenav/ui/aF;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/aH;


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private final c:Lax/b;

.field private final d:I

.field private final e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lax/b;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;II)V

    return-void
.end method

.method public constructor <init>(Lax/b;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/aF;->f:I

    const v0, -0x57ffaf01

    iput v0, p0, Lcom/google/googlenav/ui/aF;->g:I

    iput-object p1, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    iput p2, p0, Lcom/google/googlenav/ui/aF;->d:I

    iput p3, p0, Lcom/google/googlenav/ui/aF;->e:I

    return-void
.end method

.method private static b()I
    .locals 2

    sget v0, Lcom/google/googlenav/ui/aF;->a:I

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/aF;->a:I

    :cond_0
    sget v0, Lcom/google/googlenav/ui/aF;->a:I

    return v0
.end method

.method public static b(LaN/Y;)I
    .locals 2

    invoke-virtual {p0}, LaN/Y;->a()I

    move-result v0

    const/16 v1, 0xb

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/aF;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/aF;->e()I

    move-result v0

    goto :goto_0
.end method

.method private static e()I
    .locals 2

    sget v0, Lcom/google/googlenav/ui/aF;->b:I

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/aF;->b:I

    :cond_0
    sget v0, Lcom/google/googlenav/ui/aF;->b:I

    return v0
.end method


# virtual methods
.method public a(LaN/Y;)I
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/aF;->b(LaN/Y;)I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/aF;->f:I

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->v()Z

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/aF;->g:I

    return-void
.end method

.method public c()Lo/D;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->ae()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/aF;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lax/t;->h()Lo/D;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    iget v1, p0, Lcom/google/googlenav/ui/aF;->d:I

    invoke-virtual {v0, v1}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    goto :goto_1
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/aF;->f:I

    return v0
.end method

.method public h()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aF;->j()[LaN/B;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->K()I

    move-result v0

    return v0
.end method

.method public j()[LaN/B;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/aF;->c:Lax/b;

    iget v1, p0, Lcom/google/googlenav/ui/aF;->d:I

    iget v2, p0, Lcom/google/googlenav/ui/aF;->e:I

    invoke-virtual {v0, v1, v2}, Lax/b;->a(II)[LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/aF;->g:I

    return v0
.end method

.method public l()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public m()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    const-string v0, "directions"

    return-object v0
.end method

.method public o()[[LaN/B;
    .locals 1

    const/4 v0, 0x0

    check-cast v0, [[LaN/B;

    return-object v0
.end method
