.class public Lcom/google/googlenav/ui/aK;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/aH;


# instance fields
.field private a:I

.field private b:[Lcom/google/googlenav/cp;

.field private c:Lcom/google/googlenav/bV;

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>([Lcom/google/googlenav/cp;IILcom/google/googlenav/bV;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/aK;->a:I

    iput-object p1, p0, Lcom/google/googlenav/ui/aK;->b:[Lcom/google/googlenav/cp;

    iput-object p4, p0, Lcom/google/googlenav/ui/aK;->c:Lcom/google/googlenav/bV;

    iput p2, p0, Lcom/google/googlenav/ui/aK;->e:I

    iput p3, p0, Lcom/google/googlenav/ui/aK;->f:I

    iput p5, p0, Lcom/google/googlenav/ui/aK;->d:I

    iput p6, p0, Lcom/google/googlenav/ui/aK;->a:I

    return-void
.end method

.method public static a(Z)I
    .locals 1

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaN/Y;)I
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/aF;->b(LaN/Y;)I

    move-result v0

    return v0
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()Lo/D;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/aK;->a:I

    return v0
.end method

.method public h()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aK;->a()Z

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public j()[LaN/B;
    .locals 5

    iget v0, p0, Lcom/google/googlenav/ui/aK;->f:I

    iget v1, p0, Lcom/google/googlenav/ui/aK;->e:I

    sub-int/2addr v0, v1

    new-array v1, v0, [LaN/B;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/aK;->b:[Lcom/google/googlenav/cp;

    iget-object v3, p0, Lcom/google/googlenav/ui/aK;->c:Lcom/google/googlenav/bV;

    iget v4, p0, Lcom/google/googlenav/ui/aK;->e:I

    add-int/2addr v4, v0

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bV;->a(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bZ;->j()LaN/B;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/aK;->d:I

    return v0
.end method

.method public l()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public m()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    const-string v0, "transit line route"

    return-object v0
.end method

.method public o()[[LaN/B;
    .locals 1

    const/4 v0, 0x0

    check-cast v0, [[LaN/B;

    return-object v0
.end method
