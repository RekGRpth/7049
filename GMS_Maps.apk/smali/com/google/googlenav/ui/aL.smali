.class public Lcom/google/googlenav/ui/aL;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final o:Lam/d;

.field private static final p:Lam/d;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/googlenav/ui/bi;

.field private final l:Landroid/graphics/Point;

.field private m:I

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const/high16 v0, -0x1000000

    invoke-static {v1, v1, v0}, Lcom/google/googlenav/G;->a(IZI)Lam/d;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/aL;->o:Lam/d;

    const/4 v0, -0x1

    invoke-static {v1, v1, v0}, Lcom/google/googlenav/G;->a(IZI)Lam/d;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/aL;->p:Lam/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/googlenav/ui/aL;->a:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->b:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->c:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->d:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->e:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->f:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->g:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->h:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->i:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->j:Ljava/lang/String;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->l:Landroid/graphics/Point;

    iput v2, p0, Lcom/google/googlenav/ui/aL;->m:I

    iput v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->k:Lcom/google/googlenav/ui/bi;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->a:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->b:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->c:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->d:I

    iget v0, p0, Lcom/google/googlenav/ui/aL;->b:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/aL;->e:I

    iget v0, p0, Lcom/google/googlenav/ui/aL;->d:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/aL;->f:I

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/aL;->a(Z)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/aM;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/aL;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/googlenav/ui/aL;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/aN;->a:Lcom/google/googlenav/ui/aL;

    return-object v0
.end method

.method private static a(Lam/e;Ljava/lang/String;ZII)V
    .locals 1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aL;->o:Lam/d;

    :goto_0
    invoke-interface {p0, v0}, Lam/e;->a(Lam/d;)V

    add-int/lit8 v0, p3, -0x1

    invoke-interface {p0, p1, v0, p4}, Lam/e;->a(Ljava/lang/String;II)V

    add-int/lit8 v0, p3, 0x1

    invoke-interface {p0, p1, v0, p4}, Lam/e;->a(Ljava/lang/String;II)V

    add-int/lit8 v0, p4, -0x1

    invoke-interface {p0, p1, p3, v0}, Lam/e;->a(Ljava/lang/String;II)V

    add-int/lit8 v0, p4, 0x1

    invoke-interface {p0, p1, p3, v0}, Lam/e;->a(Ljava/lang/String;II)V

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/aL;->p:Lam/d;

    :goto_1
    invoke-interface {p0, v0}, Lam/e;->a(Lam/d;)V

    invoke-interface {p0, p1, p3, p4}, Lam/e;->a(Ljava/lang/String;II)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/aL;->p:Lam/d;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/aL;->o:Lam/d;

    goto :goto_1
.end method

.method private a(Z)V
    .locals 3

    iget v0, p0, Lcom/google/googlenav/ui/aL;->a:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget-object v0, p0, Lcom/google/googlenav/ui/aL;->k:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/aL;->a:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/googlenav/ui/aL;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/aL;->n:I

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/aL;->n:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/aL;->n:I

    :cond_0
    return-void
.end method


# virtual methods
.method public a(LaN/p;Z)V
    .locals 10

    if-nez p1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->h:I

    iput v0, p0, Lcom/google/googlenav/ui/aL;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->i:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/aL;->a(Z)V

    iget v0, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v1, p0, Lcom/google/googlenav/ui/aL;->n:I

    invoke-virtual {p1, v0, v1}, LaN/p;->b(II)LaN/B;

    move-result-object v1

    iget v0, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->c:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    invoke-virtual {p1, v0, v2}, LaN/p;->b(II)LaN/B;

    move-result-object v0

    invoke-virtual {v1, v0}, LaN/B;->b(LaN/B;)J

    move-result-wide v2

    long-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide v4, 0x400a3f28fd4f4b98L

    mul-double/2addr v4, v2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->i:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/common/util/u;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_2

    sget-object v6, Lcom/google/googlenav/common/util/u;->a:[I

    aget v6, v6, v0

    int-to-double v6, v6

    sub-double v6, v2, v6

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_4

    sget-object v2, Lcom/google/googlenav/common/util/u;->a:[I

    aget v0, v2, v0

    const/16 v2, 0x3e8

    if-ge v0, v2, :cond_3

    const/16 v2, 0x102

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/aL;->i:Ljava/lang/String;

    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, LaN/B;->c(II)LaN/B;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/aL;->l:Landroid/graphics/Point;

    invoke-virtual {p1, v0, v2}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/aL;->l:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->m:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/googlenav/ui/aL;->g:I

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/aL;->h:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/aL;->j:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/common/util/u;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-ltz v0, :cond_0

    sget-object v2, Lcom/google/googlenav/common/util/u;->b:[I

    aget v2, v2, v0

    int-to-double v2, v2

    sub-double v2, v4, v2

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_6

    sget-object v2, Lcom/google/googlenav/common/util/u;->b:[I

    aget v0, v2, v0

    const/16 v2, 0x14a0

    if-ge v0, v2, :cond_5

    const/16 v2, 0x100

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/aL;->j:Ljava/lang/String;

    :goto_4
    const/4 v2, 0x0

    int-to-double v3, v0

    const-wide v5, 0x400a3f28fd4f4b98L

    div-double/2addr v3, v5

    double-to-int v0, v3

    invoke-virtual {v1, v2, v0}, LaN/B;->c(II)LaN/B;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/aL;->l:Landroid/graphics/Point;

    invoke-virtual {p1, v0, v1}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/aL;->l:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/aL;->h:I

    goto/16 :goto_0

    :cond_3
    const/16 v2, 0x101

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    div-int/lit16 v7, v0, 0x3e8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/aL;->i:Ljava/lang/String;

    goto/16 :goto_2

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_1

    :cond_5
    const/16 v2, 0x103

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    div-int/lit16 v5, v0, 0x14a0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/aL;->j:Ljava/lang/String;

    goto :goto_4

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_3
.end method

.method public a(Lam/e;Z)V
    .locals 5

    iget v0, p0, Lcom/google/googlenav/ui/aL;->g:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/aL;->h:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/aL;->g:I

    iget v1, p0, Lcom/google/googlenav/ui/aL;->h:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/2addr v0, v1

    const/4 v1, -0x1

    invoke-interface {p1, v1}, Lam/e;->a(I)V

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->e:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/google/googlenav/ui/aL;->b:I

    add-int/lit8 v4, v4, 0x2

    invoke-interface {p1, v1, v2, v3, v4}, Lam/e;->b(IIII)V

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->f:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v3, v0, 0x2

    iget v4, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/lit8 v4, v4, 0x2

    invoke-interface {p1, v1, v2, v3, v4}, Lam/e;->b(IIII)V

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->h:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->e:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/google/googlenav/ui/aL;->e:I

    add-int/lit8 v4, v4, 0x1

    invoke-interface {p1, v1, v2, v3, v4}, Lam/e;->b(IIII)V

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->g:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/google/googlenav/ui/aL;->e:I

    add-int/lit8 v4, v4, 0x1

    invoke-interface {p1, v1, v2, v3, v4}, Lam/e;->b(IIII)V

    const/high16 v1, -0x1000000

    invoke-interface {p1, v1}, Lam/e;->a(I)V

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->e:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/googlenav/ui/aL;->d:I

    iget v4, p0, Lcom/google/googlenav/ui/aL;->b:I

    invoke-interface {p1, v1, v2, v3, v4}, Lam/e;->b(IIII)V

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->f:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/googlenav/ui/aL;->d:I

    invoke-interface {p1, v1, v2, v0, v3}, Lam/e;->b(IIII)V

    iget v0, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v1, p0, Lcom/google/googlenav/ui/aL;->h:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->e:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/googlenav/ui/aL;->d:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->e:I

    invoke-interface {p1, v0, v1, v2, v3}, Lam/e;->b(IIII)V

    iget v0, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v1, p0, Lcom/google/googlenav/ui/aL;->g:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->d:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->e:I

    invoke-interface {p1, v0, v1, v2, v3}, Lam/e;->b(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/aL;->j:Ljava/lang/String;

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/googlenav/ui/aL;->a:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    sget-object v3, Lcom/google/googlenav/ui/aL;->o:Lam/d;

    invoke-interface {v3}, Lam/d;->a()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/googlenav/ui/aL;->a:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-static {p1, v0, p2, v1, v2}, Lcom/google/googlenav/ui/aL;->a(Lam/e;Ljava/lang/String;ZII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/aL;->i:Ljava/lang/String;

    iget v1, p0, Lcom/google/googlenav/ui/aL;->m:I

    iget v2, p0, Lcom/google/googlenav/ui/aL;->d:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/googlenav/ui/aL;->a:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/googlenav/ui/aL;->n:I

    iget v3, p0, Lcom/google/googlenav/ui/aL;->a:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-static {p1, v0, p2, v1, v2}, Lcom/google/googlenav/ui/aL;->a(Lam/e;Ljava/lang/String;ZII)V

    goto/16 :goto_0
.end method
